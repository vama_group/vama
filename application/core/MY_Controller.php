<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Controller extends CI_Controller {

	public function __construct() {
		parent::__construct();
		// DEFAULT TIME ZONE
		date_default_timezone_set('Asia/Jakarta');
		// Define Table Name Here

		// Load Model Here        
		$this->load->model('pegawai_pusat_model');
		$this->load->model('usergroup_model');
		$this->load->model('supplier_model');
		$this->load->model('pegawai_pusat_model');
		$this->load->model('jenis_barang_model');
		$this->load->model('kategori_barang_model');
		$this->load->model('customer_pusat_model');				
	}

	function returnJson($data) {
		echo json_encode($data);
	}

	function clean_tag_input($str)
	{
		$t = preg_replace('/<[^<|>]+?>/', '', htmlspecialchars_decode($str));
		$t = htmlentities($t, ENT_QUOTES, "UTF-8");
		$t = trim($t);
		return $t;
	}
}
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Barang_pusat extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->CI =& get_instance();
		$this->load->model('pegawai_pusat_model','pegawai_pusat');
        $this->load->model('useraccess_pusat_model','useraccess_pusat');
        
        $this->load->model('periode_stok_pusat_model','periode_stok_pusat');
		$this->load->model('detail_transaksi_model','detail_transaksi');
		$this->load->model('barang_pusat_model','barang_pusat');
	    $this->load->model('barang_toko_model','barang_toko');
	    $this->load->model('supplier_model','supplier');
	    $this->load->model('jenis_barang_model','jenis_barang');
	    $this->load->model('kategori_barang_model','kategori_barang');
	    $this->load->model('customer_pusat_model','customer_pusat');
	    $this->load->model('penjualan_master_model','penjualan_master');	    
	    $this->load->model('penjualan_detail_model','penjualan_detail');
	    $this->load->helper(array('url','file'));
	}

	public function index()
	{
		// Cek user acces menu
		$id_pegawai = $this->session->userdata('id_pegawai');
		if($id_pegawai == ''){
			redirect(base_url().'login');
			exit();
		}

		$data_pegawai = $this->pegawai_pusat->get_by_id($id_pegawai);
		if($data_pegawai){
			$cek_useraccess             = $this->useraccess_pusat->cek_access($id_pegawai, '19');
			$cek_useraccess_barang_toko = $this->useraccess_pusat->cek_access($id_pegawai, '20');

			if($cek_useraccess){
				if($cek_useraccess->act_read == '0' or $cek_useraccess->act_read == '-'){
					redirect(base_url().'dashboard');
					exit();
				}					
			}else{
				redirect(base_url().'dashboard');
				exit();
			}

			// Ambil kode dan id periode stok terbaru
			$periode_sekarang = $this->periode_stok_pusat->akhir();
			$data_periode     = $this->periode_stok_pusat->listing();
			
			$data['access_create']             = $cek_useraccess->act_create;
			$data['data_pegawai']              = $data_pegawai;
			$data['data_induk_menu']           = $this->useraccess_pusat->get_induk_menu($id_pegawai);
			$data['access_update_barang_toko'] = $cek_useraccess_barang_toko->act_update;
			
			$data['data_periode_sekarang']     = $periode_sekarang;
			$data['data_periode']              = $data_periode;

			$daftar_toko                       = $this->customer_pusat->listing_toko();
			$data['data_toko']                 = $daftar_toko;
			
			$data['atribut_halaman']           = 'Barang Pusat';
			$data['halaman_list']              = $this->load->view('admin/master_data/barang_pusat/list',$data,true);
			$data['halaman_form']              = $this->load->view('admin/master_data/barang_pusat/form',$data,true);
			$data['halaman_plugin']            = $this->load->view('admin/master_data/barang_pusat/plugin',$data,true);
			$data['isi_halaman']               = $this->load->view('admin/master_data/barang_pusat/index',$data,true);
			$this->load->view('admin/layout',$data);
		}else{
			redirect(base_url().'login');
			exit();
		}
	}

	public function ajax_list_master_barang()
	{
		if($this->input->is_ajax_request()){
			// Cek user acces menu
			$id_pegawai 	= $this->session->userdata('id_pegawai');
			$cek_useraccess = $this->useraccess_pusat->cek_access($id_pegawai, '19');
			if($cek_useraccess->act_read == '0' or $cek_useraccess->act_read == '0'){
				redirect(base_url().'dashboard');
				exit();
			}

			$id_periode_stok_pusat = $this->input->post('id_periode_stok_pusat');
			if($id_periode_stok_pusat == ''){
				redirect(base_url().'barang_pusat');
				exit();
			}

			$status_tampil = $this->input->post('status_tampil');
			if($status_tampil == ''){
				redirect(base_url().'barang_pusat');
				exit();
			}

			$dp   = $this->periode_stok_pusat->get_by_id($id_periode_stok_pusat);
			$list = $this->barang_pusat->get_datatables(
				'_get_datatables_query', $dp->tanggal_periode_awal, $dp->tanggal_periode_akhir
			);

			// $list 	= $this->barang_pusat->get_datatables('_get_datatables_query', '', '');
			$data 	= array();
			$no 	= $_POST['start'];
			foreach ($list as $barang_pusat) {
				$no++;
				$row 	= array();
				
				$row[] 	= $no;

				//add html for action
				if($cek_useraccess->act_update == 1){
					$tombol_edit 	 = '<a class="btn btn-rounded btn-default btn-xs" href="javascript:void(0)" title="Edit Barang" 
											onclick="edit_barang_pusat('."'".$barang_pusat->id_barang_pusat."'".')">
											<i class="fa fa-pencil" style="color: blue;"></i>
									   </a>';
				}else{$tombol_edit 	 = '';}

				if($cek_useraccess->act_delete == 1){
					$tombol_hapus 	 = '<a class="btn btn-rounded btn-default btn-xs" href="javascript:void(0)" title="Hapus Barang" 
											onclick="verifikasi_delete('."'".$barang_pusat->id_barang_pusat."'".')">
											<i class="fa fa-remove" style="color: red;"></i>
						  			   </a>';
				}else{$tombol_hapus  = '';}

				$row[] = '	<a class="btn btn-rounded btn-default btn-xs" href="javascript:void(0)" title="Cetak Barcode" 
								onclick="cetak_barcode('."'".$barang_pusat->id_barang_pusat."'".')">
								<i class="fa fa-barcode"></i>
							</a>

							<a class="btn btn-rounded btn-default btn-xs" href="javascript:void(0)" title="Detail Transaksi" 
								onclick="detail_transaksi('."'".$barang_pusat->id_barang_pusat."'".')">
								<i class="fa fa-folder-open-o" style="color: #188ae2 ;"></i>
							</a>

						  	'.$tombol_edit.'
						  	'.$tombol_hapus.'
					  	';

				if($status_tampil == '1' AND  $this->session->userdata('usergroup_name') == 'Super Admin'){
					$row[] = $barang_pusat->nama_supplier;
				}

				if($barang_pusat->status == 'AKTIF'){
					$warna_text = 'text-dark';
				}else{
					$warna_text = 'text-danger';
				}

				$row[] = $barang_pusat->sku;
				$row[] = '<span class="'.$warna_text.'"><b>'.$barang_pusat->nama_barang.'</b></span>';

				$stok       = $barang_pusat->total_stok;
				$stok_rusak = $barang_pusat->total_stok_rusak;

				// Awal stok jual
				if($stok<="0"){
					$row[] = '
								<div class="btn btn-default btn-rounded btn-block btn-xs">
								<i class="fa fa-ban text-danger"></i>
								<span class="text-danger text-center">'."".$stok."".'</span>
								</div>
							 ';
				}elseif($stok<="4"){
					$row[] = '
								<div class="btn btn-default btn-rounded btn-block btn-xs">
								<i class="fa fa-warning text-warning"></i>
								<span class="text-warning text-center">'."".$stok."".'</span>
								</div>
							 ';
				}elseif($stok<="9"){
					$row[] = '
								<div class="btn btn-default btn-rounded btn-block btn-xs">
								<i class="fa fa-check text-primary"></i>
								<span class="text-primary text-center">'."".$stok."".'</span>
								</div>
							 ';
				}elseif($stok>="10"){
					$row[] = '
								<div class="btn btn-default btn-rounded btn-block btn-xs">
								<i class="fa fa-check text-success"></i>
								<span class="text-success text-center">'."".$stok."".'</span>
								</div>
							 ';
				}
				// Akhir stok jual

				// Awal stok rusak
				if($stok_rusak<="0"){
					$row[] = '
								<div class="btn btn-default btn-rounded btn-block btn-xs">
								<i class="fa fa-ban text-danger"></i>
								<span class="text-danger text-center">'."".$stok_rusak."".'</span>
								</div>
							 ';
				}elseif($stok_rusak<="4"){
					$row[] = '
								<div class="btn btn-default btn-rounded btn-block btn-xs">
								<i class="fa fa-warning text-warning"></i>
								<span class="text-warning text-center">'."".$stok_rusak."".'</span>
								</div>
							 ';
				}elseif($stok_rusak<="9"){
					$row[] = '
								<div class="btn btn-default btn-rounded btn-block btn-xs">
								<i class="fa fa-check text-primary"></i>
								<span class="text-primary text-center">'."".$stok_rusak."".'</span>
								</div>
							 ';
				}elseif($stok_rusak>="10"){
					$row[] = '
								<div class="btn btn-default btn-rounded btn-block btn-xs">
								<i class="fa fa-check text-success"></i>
								<span class="text-success text-center">'."".$stok_rusak."".'</span>
								</div>
							 ';
				} 
				// Akhir stok rusak 

				if($status_tampil == '1' AND  $this->session->userdata('usergroup_name') == 'Super Admin'){
					$row[] 	= 'Rp. <span class="text-right">'."".number_format($barang_pusat->modal,'0',',','.')."".'</span>';
					$row[] 	= $barang_pusat->resistensi_modal;
					$row[] 	= 'Rp. <span class="text-right">'.number_format($barang_pusat->modal_bersih,'0',',','.').'</span>';							
				}

				$row[] 	= 'Rp. <span class="text-right">'."".number_format($barang_pusat->harga_eceran,'0',',','.')."".'</span>';
				$row[] 	= 'Rp. <span class="text-right">'.number_format($barang_pusat->harga_grosir1,'0',',','.').'</span>';
				$row[] 	= 'Rp. <span class="text-right">'.number_format($barang_pusat->harga_grosir2,'0',',','.').'</span>';
				
				$row[] 	= '<span class="'.$warna_text.'">'.$barang_pusat->status.'</span>';

				$row[]	= $barang_pusat->pegawai_save;
				$row[] 	= $barang_pusat->tanggal_pembuatan;
				$row[] 	= $barang_pusat->pegawai_edit;
				$row[] 	= $barang_pusat->tanggal_pembaharuan;
				$data[] = $row;
			}

			$output = array(
				"query"           => $this->db->last_query(),	
				"draw"            => $_POST['draw'],
				"recordsTotal"    => $this->barang_pusat->count_all(),
				"recordsFiltered" => $this->barang_pusat->count_filtered('_get_datatables_query', '', ''),
				"data"            => $data,
				"status"          => $status_tampil
			);
			echo json_encode($output);
		}else{
			redirect(base_url().'barang_pusat');
		}
	}

	public function ajax_list_daftar_transaksi()
	{
		if($this->input->is_ajax_request()){
			// Cek user acces menu
			$id_pegawai     = $this->session->userdata('id_pegawai');
			$cek_useraccess = $this->useraccess_pusat->cek_access($id_pegawai, '19');
			if($cek_useraccess->act_read == '0' or $cek_useraccess->act_read == '0'){
				redirect(base_url().'dashboard');
				exit();
			}

			$id_periode_stok_pusat = $this->input->post('id_periode_stok_pusat');
			if($id_periode_stok_pusat == ''){
				redirect(base_url().'barang_pusat');
				exit();
			}

			$dp   = $this->periode_stok_pusat->get_by_id($id_periode_stok_pusat);
			$list = $this->barang_pusat->get_datatables(
				'_get_datatables_query_transaksi', $dp->tanggal_periode_awal, $dp->tanggal_periode_akhir
			);
			
			$data = array();
			$no = $_POST['start'];
			foreach ($list as $barang_pusat) {
				$no++;
				$row   = array();
				$row[] = $no;
				$row[] = $barang_pusat->sku;
				$row[] = '<span class="text-dark"><b>'.$barang_pusat->nama_barang.'</b></span>';
				$row[] = 'Rp. <span class="text-right">'."".number_format($barang_pusat->harga_eceran,'0',',','.')."".'</span>';

				$row[] = '<span class="text-primary pull-right">'.$barang_pusat->so.'</span>';
				$row[] = '<span class="text-primary pull-right">'.$barang_pusat->pb.'</span>';
				$row[] = '<span class="text-primary pull-right">'.$barang_pusat->dp.'</span>';
				$row[] = '<span class="text-primary pull-right">'.$barang_pusat->rpjt.'</span>';
				$row[] = '<span class="text-primary pull-right">'.$barang_pusat->rbdt.'</span>';
				$row[] = '<span class="text-danger pull-right">'.$barang_pusat->pj.'</span>';
				$row[] = '<span class="text-danger pull-right">'.$barang_pusat->hd.'</span>';
				$row[] = '<span class="text-danger pull-right">'.$barang_pusat->up.'</span>';
				$row[] = '<span class="text-danger pull-right">'.$barang_pusat->rpbt.'</span>';

				// Awal stok jual
				$stok  = $barang_pusat->total_stok_fix;			
				if($stok<="0"){
					$row[] = '
								<div class="btn btn-default btn-rounded btn-block btn-xs">
								<i class="fa fa-ban text-danger"></i>
								<span class="text-danger text-center">'."".$stok."".'</span>
								</div>
							 ';
				}elseif($stok<="4"){
					$row[] = '
								<div class="btn btn-default btn-rounded btn-block btn-xs">
								<i class="fa fa-warning text-warning"></i>
								<span class="text-warning text-center">'."".$stok."".'</span>
								</div>
							 ';
				}elseif($stok<="9"){
					$row[] = '
								<div class="btn btn-default btn-rounded btn-block btn-xs">
								<i class="fa fa-check text-primary"></i>
								<span class="text-primary text-center">'."".$stok."".'</span>
								</div>
							 ';
				}elseif($stok>="10"){
					$row[] = '
								<div class="btn btn-default btn-rounded btn-block btn-xs">
								<i class="fa fa-check text-success"></i>
								<span class="text-success text-center">'."".$stok."".'</span>
								</div>
							 ';
				}
				// Akhir stok jual
				
				$data[] = $row;
			}

			$output = array(
				"draw"            => $_POST['draw'],
				"recordsTotal"    => $this->barang_pusat->count_all(),
				"recordsFiltered" => $this->barang_pusat->count_filtered('_get_datatables_query_transaksi', 
									 $dp->tanggal_periode_awal, $dp->tanggal_periode_akhir),
				"data"            => $data
			);
			echo json_encode($output);
		}else{
			redirect(base_url(),'barang_pusat');
		}
	}

	public function ajax_list_daftar_transaksi_rusak()
	{
		if($this->input->is_ajax_request()){
			// Cek user acces menu
			$id_pegawai 	= $this->session->userdata('id_pegawai');
			$cek_useraccess = $this->useraccess_pusat->cek_access($id_pegawai, '19');
			if($cek_useraccess->act_read == '0' or $cek_useraccess->act_read == '0'){
				redirect(base_url().'dashboard');
				exit();
			}

			$id_periode_stok_pusat = $this->input->post('id_periode_stok_pusat');
			if($id_periode_stok_pusat == ''){
				redirect(base_url().'barang_pusat');
				exit();
			}

			$dp 					= $this->periode_stok_pusat->get_by_id($id_periode_stok_pusat);
			$list 					= $this->barang_pusat->get_datatables('_get_datatables_query_transaksi_rusak', 
									  $dp->tanggal_periode_awal, $dp->tanggal_periode_akhir);
			
			$data = array();
			$no = $_POST['start'];
			foreach ($list as $barang_pusat) {
				$no++;
				$row 	= array();
				$row[] 	= $no;
				$row[] 	= $barang_pusat->sku;
				$row[] 	= '<span class="text-dark"><b>'.$barang_pusat->nama_barang.'</b></span>';
				$row[] 	= 'Rp. <span class="text-right">'."".number_format($barang_pusat->harga_eceran,'0',',','.')."".'</span>';
				$row[] 	= '<span class="text-primary">'.$barang_pusat->rpr.'</span>';
				$row[] 	= '<span class="text-primary">'.$barang_pusat->rbdtr.'</span>';
				$row[] 	= '<span class="text-danger">'.$barang_pusat->rpbr.'</span>';
				
				// Awal stok rusak
				$stok  = $barang_pusat->total_stok_rusak_fix;
				if($stok<="0"){
					$row[] = '
								<div class="btn btn-default btn-rounded btn-block btn-xs">
								<i class="fa fa-ban text-danger"></i>
								<span class="text-danger text-center">'."".$stok."".'</span>
								</div>
							 ';
				}elseif($stok<="4"){
					$row[] = '
								<div class="btn btn-default btn-rounded btn-block btn-xs">
								<i class="fa fa-warning text-warning"></i>
								<span class="text-warning text-center">'."".$stok."".'</span>
								</div>
							 ';
				}elseif($stok<="9"){
					$row[] = '
								<div class="btn btn-default btn-rounded btn-block btn-xs">
								<i class="fa fa-check text-primary"></i>
								<span class="text-primary text-center">'."".$stok."".'</span>
								</div>
							 ';
				}elseif($stok>="10"){
					$row[] = '
								<div class="btn btn-default btn-rounded btn-block btn-xs">
								<i class="fa fa-check text-success"></i>
								<span class="text-success text-center">'."".$stok."".'</span>
								</div>
							 ';
				}
				// Akhir stok rusak

				$data[] = $row;
			}

			$output = array(
				"draw"            => $_POST['draw'],
				"recordsTotal"    => $this->barang_pusat->count_all(),
				"recordsFiltered" => $this->barang_pusat->count_filtered('_get_datatables_query_transaksi_rusak', 
									 $dp->tanggal_periode_awal, $dp->tanggal_periode_akhir),
				"data"            => $data
			);
			echo json_encode($output);
		}else{
			redirect(base_url,'barang_pusat');
		}
	}

	public function ajax_list_pembaharuan_barang()
	{
		if($this->input->is_ajax_request()){
			// Cek user acces menu
			$id_pegawai 	= $this->session->userdata('id_pegawai');
			$cek_useraccess = $this->useraccess_pusat->cek_access($id_pegawai, '19');
			if($cek_useraccess->act_read == '0' or $cek_useraccess->act_read == '0'){
				redirect(base_url().'dashboard');
				exit();
			}

			$id_periode_stok_pusat = $this->input->post('id_periode_stok_pusat');
			if($id_periode_stok_pusat == ''){
				redirect(base_url().'barang_pusat');
				exit();
			}

			$tanggal_awal 	= date('Y-m-01');
			$tanggal_akhir 	= date('Y-m-d');;
			$list 			= $this->barang_pusat->get_datatables('_get_datatables_query_pembaharuan_barang', $tanggal_awal, $tanggal_akhir);

			$data 	= array();
			$no 	= $_POST['start'];
			foreach ($list as $barang_pusat) {
				$no++;
				$row 	= array();
				$row[] 	= $no;

				//add html for action
				if($cek_useraccess->act_update == 1){
					$tombol_edit 	 = '<a class="btn btn-rounded btn-default btn-xs" href="javascript:void(0)" title="Edit" 
										onclick="edit_barang_pusat('."'".$barang_pusat->id_barang_pusat."'".')">
										<i class="fa fa-pencil" style="color: blue;"></i>
									   </a>';
				}else{$tombol_edit 	 = '';}

				$row[] = '	<a class="btn btn-rounded btn-default btn-xs" href="javascript:void(0)" title="Cetak Barcode" 
								onclick="cetak_barcode('."'".$barang_pusat->id_barang_pusat."'".')">
								<i class="fa fa-barcode"></i>
							</a>

							<a class="btn btn-rounded btn-default btn-xs" href="javascript:void(0)" title="Detail Barang" 
								onclick="cetak_barcode('."'".$barang_pusat->id_barang_pusat."'".')">
								<i class="fa fa-folder-open-o" style="color: #188ae2 ;"></i>
							</a>
							
						  	'.$tombol_edit.'
					  	';

				$row[] 	= $barang_pusat->sku;
				$row[] 	= '<span class="text-dark"><b>'.$barang_pusat->nama_barang.'</b></span>';
				$row[] 	= 'Rp. <span class="text-right">'."".number_format($barang_pusat->harga_eceran,'0',',','.')."".'</span>';
				$row[] 	= 'Rp. <span class="text-right">'.number_format($barang_pusat->harga_grosir1,'0',',','.').'</span>';
				$row[] 	= 'Rp. <span class="text-right">'.number_format($barang_pusat->harga_grosir2,'0',',','.').'</span>';

				// Awal stok jual
				$stok  = $barang_pusat->total_stok;
				if($stok<="0"){
					$row[] = '
								<div class="btn btn-default btn-rounded btn-block btn-xs">
								<i class="fa fa-ban text-danger"></i>
								<span class="text-danger text-center">'."".$stok."".'</span>
								</div>
							 ';
				}elseif($stok<="4"){
					$row[] = '
								<div class="btn btn-default btn-rounded btn-block btn-xs">
								<i class="fa fa-warning text-warning"></i>
								<span class="text-warning text-center">'."".$stok."".'</span>
								</div>
							 ';
				}elseif($stok<="9"){
					$row[] = '
								<div class="btn btn-default btn-rounded btn-block btn-xs">
								<i class="fa fa-check text-primary"></i>
								<span class="text-primary text-center">'."".$stok."".'</span>
								</div>
							 ';
				}elseif($stok>="10"){
					$row[] = '
								<div class="btn btn-default btn-rounded btn-block btn-xs">
								<i class="fa fa-check text-success"></i>
								<span class="text-success text-center">'."".$stok."".'</span>
								</div>
							 ';
				}
				// Akhir stok jual

				$row[]	= $barang_pusat->pegawai_save;
				$row[] 	= $barang_pusat->tanggal_pembuatan;
				$row[] 	= $barang_pusat->pegawai_edit;
				$row[] 	= $barang_pusat->tanggal_pembaharuan;
				$data[] = $row;
			}

			$output = array(	
				"draw"            => $_POST['draw'],
				"recordsTotal"    => $this->barang_pusat->count_all(),
				"recordsFiltered" => $this->barang_pusat->count_filtered('_get_datatables_query_pembaharuan_barang', 
									 $tanggal_awal, $tanggal_akhir),
				"data"            => $data
			);
			echo json_encode($output);
		}else{
			redirect(base_url(),'barang_pusat');
		}
	}

	public function ajax_list_periksa_stok()
	{
		if($this->input->is_ajax_request()){
			// Cek user acces menu
			$id_pegawai 	= $this->session->userdata('id_pegawai');
			$cek_useraccess = $this->useraccess_pusat->cek_access($id_pegawai, '19');
			if($cek_useraccess->act_read == '0' or $cek_useraccess->act_read == '0'){
				redirect(base_url().'dashboard');
				exit();
			}

			$id_periode_stok_pusat = $this->input->post('id_periode_stok_pusat');
			if($id_periode_stok_pusat == ''){
				redirect(base_url().'barang_pusat');
				exit();
			}

			$dp   = $this->periode_stok_pusat->get_by_id($id_periode_stok_pusat);
			$list = $this->barang_pusat->get_datatables(
				'_get_datatables_query_periksa', $dp->tanggal_periode_awal, $dp->tanggal_periode_akhir
			);

			$data = array();
			$no   = $_POST['start'];
			foreach ($list as $barang_pusat) {
				$no++;
				$row   = array();
				$row[] = $no;

				//add html for action
				if($cek_useraccess->act_update == 1){
					$tombol_edit 	 = '<a class="btn btn-rounded btn-default btn-xs" href="javascript:void(0)" title="Edit Barang" 
											onclick="edit_barang_pusat('."'".$barang_pusat->id_barang_pusat."'".')">
											<i class="fa fa-pencil" style="color: blue;"></i>
									   </a>';
				}else{$tombol_edit 	 = '';}

				if($cek_useraccess->act_delete == 1){
					$tombol_hapus 	 = '<a class="btn btn-rounded btn-default btn-xs" href="javascript:void(0)" title="Hapus Barang" 
											onclick="verifikasi_delete('."'".$barang_pusat->id_barang_pusat."'".')">
											<i class="fa fa-remove" style="color: red;"></i>
						  			   </a>';
				}else{$tombol_hapus  = '';}

				$row[] = '	<a class="btn btn-rounded btn-default btn-xs" href="javascript:void(0)" title="Cetak Barcode" 
								onclick="cetak_barcode('."'".$barang_pusat->id_barang_pusat."'".')">
								<i class="fa fa-barcode"></i>
							</a>

							<a class="btn btn-rounded btn-default btn-xs" href="javascript:void(0)" title="Detail Transaksi" 
								onclick="detail_transaksi('."'".$barang_pusat->id_barang_pusat."'".')">
								<i class="fa fa-folder-open-o" style="color: #188ae2 ;"></i>
							</a>

						  	'.$tombol_edit.'
						  	'.$tombol_hapus.'
					  	';

				$row[] = $barang_pusat->sku;
				$row[] = '<span class="text-dark"><b>'.$barang_pusat->nama_barang.'</b></span>';
				$row[] = 'Rp. <span class="text-right">'."".number_format($barang_pusat->harga_eceran,'0',',','.')."".'</span>';

				$row[] = '<span class="text-right">'."".number_format($barang_pusat->total_stok_fix,'0',',','.')."".'</span>';
				$row[] = '<span class="text-right">'."".number_format($barang_pusat->total_stok,'0',',','.')."".'</span>';
				$koreksi    = $barang_pusat->koreksi;

				// Awal koreksi stok
				if($koreksi<="0"){
					$row[] = '
								<div class="btn btn-default btn-rounded btn-block btn-xs">
								<i class="fa fa-ban text-danger"></i>
								<span class="text-danger text-center">'."".$koreksi."".'</span>
								</div>
							 ';
				}elseif($koreksi<="4"){
					$row[] = '
								<div class="btn btn-default btn-rounded btn-block btn-xs">
								<i class="fa fa-warning text-warning"></i>
								<span class="text-warning text-center">'."".$koreksi."".'</span>
								</div>
							 ';
				}elseif($koreksi<="9"){
					$row[] = '
								<div class="btn btn-default btn-rounded btn-block btn-xs">
								<i class="fa fa-check text-primary"></i>
								<span class="text-primary text-center">'."".$koreksi."".'</span>
								</div>
							 ';
				}elseif($koreksi>="10"){
					$row[] = '
								<div class="btn btn-default btn-rounded btn-block btn-xs">
								<i class="fa fa-check text-success"></i>
								<span class="text-success text-center">'."".$koreksi."".'</span>
								</div>
							 ';
				}
				// Akhir koreksi stok

				$row[]	= $barang_pusat->status;
				$row[]	= $barang_pusat->pegawai_save;
				$row[] 	= $barang_pusat->tanggal_pembuatan;
				$row[] 	= $barang_pusat->pegawai_edit;
				$row[] 	= $barang_pusat->tanggal_pembaharuan;
				$data[] = $row;
			}

			$output = array(	
				"draw"            => $_POST['draw'],
				"recordsTotal"    => $this->barang_pusat->count_all(),
				"recordsFiltered" => $this->barang_pusat->count_filtered(
					'_get_datatables_query_periksa', $dp->tanggal_periode_awal, $dp->tanggal_periode_akhir
				),
				"data"            => $data,
			);
			echo json_encode($output);
		}else{
			redirect(base_url().'barang_pusat');
		}
	}

	public function ajax_fix_stok()
	{
		if($this->input->is_ajax_request()){
			// Cek user acces menu
			$id_pegawai 	= $this->session->userdata('id_pegawai');
			$cek_useraccess = $this->useraccess_pusat->cek_access($id_pegawai, '19');
			if($cek_useraccess->act_read == '0' or $cek_useraccess->act_read == '0'){
				redirect(base_url().'dashboard');
				exit();
			}

			$id_periode_stok_pusat = $this->input->post('id_periode_stok_pusat');
			if($id_periode_stok_pusat == ''){
				redirect(base_url().'barang_pusat');
				exit();
			}

			$dp   = $this->periode_stok_pusat->get_by_id($id_periode_stok_pusat);
			$list = $this->barang_pusat->periksa_stok($dp->tanggal_periode_awal, $dp->tanggal_periode_akhir);

			if($list){
				ini_set('max_execution_time', 0); 
				ini_set('memory_limit','2048M');
				foreach ($list as $barang_pusat){
					$koreksi_stok = $barang_pusat->total_stok_fix - $barang_pusat->total_stok;
					if($koreksi_stok == '' or $koreksi_stok == '0'){
						$koreksi_stok = '0';
					}else{
						$this->barang_pusat->update_stok_pusat(
							$barang_pusat->id_barang_pusat, 
							$barang_pusat->total_stok_fix, 
							$id_pegawai
						);
					}
				}

				echo json_encode(array(
					'status' => 1
				));
			}else{
				echo json_encode(array(
					'status' => 0
				));
			}
		}else{
			redirect(base_url().'barang_pusat');
		}
	}

	public function ajax_list_detail_transaksi()
	{
		if($this->input->is_ajax_request()){
			// Cek user acces menu
			$id_pegawai     = $this->session->userdata('id_pegawai');
			$cek_useraccess = $this->useraccess_pusat->cek_access($id_pegawai, '19');
			if($cek_useraccess->act_read == '0' or $cek_useraccess->act_read == '-'){
				redirect(base_url().'dashboard');
			}

			$id_barang_pusat       = $this->clean_tag_input($this->input->post('id_barang_pusat'));
			$id_periode_stok_pusat = $this->input->post('id_periode_stok_pusat');
			$jenis_transaksi       = $this->clean_tag_input($this->input->post('jenis_transaksi'));
			if($id_barang_pusat == '' or $id_periode_stok_pusat == '' or $jenis_transaksi == ''){
				exit();
			}

			// Validasi id barang pusat
			$data_barang = $this->barang_pusat->get_id($id_barang_pusat);
			if($data_barang == ''){
				exit();
			}

			// Validasi id periode stok pusat dan ambil tanggal awal dan akhir periode
			$data_periode = $this->periode_stok_pusat->get_by_id($id_periode_stok_pusat);
			if($data_periode){
				$tanggal_periode_awal  = $data_periode->tanggal_periode_awal;
				$tanggal_periode_akhir = $data_periode->tanggal_periode_akhir;
			}else{
				exit();
			}
			
			if($jenis_transaksi == 'pju'){
				$jenis_perintah = '!=';
			}else if($jenis_transaksi == 'pjt'){
				$jenis_perintah = '=';
			}else{
				$jenis_perintah = '';
			}

			if($jenis_perintah == ''){
				$jenis_query = '_get_datatables_query_'.$jenis_transaksi;
				$list        = $this->detail_transaksi->get_datatables(
					$jenis_query, $id_barang_pusat, $id_periode_stok_pusat, $tanggal_periode_awal, $tanggal_periode_akhir, $jenis_perintah
				);
			}else{
				$jenis_query = '_get_datatables_query_pj';
				$list        = $this->detail_transaksi->get_datatables(
					$jenis_query, $id_barang_pusat, $id_periode_stok_pusat, $tanggal_periode_awal, $tanggal_periode_akhir, $jenis_perintah
				);
			}

			$data       = array();
			$no         = $_POST['start'];
			$nama_table = '';

			// Awal Stok Jual
			if($jenis_transaksi == 'so'){
				$nama_table = "vamr4846_vama.stok_opname_pusat";
			}else if($jenis_transaksi == 'pb'){
				$nama_table = 'vamr4846_vama.pembelian_detail';
			}else if($jenis_transaksi == 'dp'){
				$nama_table = "vamr4846_vama.perakitan_master";
			}else if($jenis_transaksi == 'rpt'){
				$nama_table ="vamr4846_vama.retur_penjualan_detail";
			}else if($jenis_transaksi == 'rbdtt'){
				$nama_table = "vamr4846_vama.retur_dari_toko";
			}else if($jenis_transaksi == 'up'){
				$nama_table = "vamr4846_vama.perakitan_detail";
			}else if($jenis_transaksi == 'pju'){
				$nama_table = "vamr4846_vama.penjualan_detail AS pd";
			}else if($jenis_transaksi == 'pjt'){
				$nama_table = "vamr4846_vama.penjualan_detail AS pd";
			}else if($jenis_transaksi == 'rpbt'){
				$nama_table = "vamr4846_vama.retur_pembelian_detail";
			}else if($jenis_transaksi == 'pp'){
				$nama_table = "vamr4846_vama.penukaran_poin_detail";
			}else if($jenis_transaksi == 'hd'){
				$nama_table = "vamr4846_vama.hadiah_detail";
			}else if($jenis_transaksi == 'ps'){
				$nama_table = "vamr4846_vama.perpindahan_stok";
			}
			// Akhir Stok Jual

			// Awal Stok Rusak
			if($jenis_transaksi == 'rpr'){
				$nama_table = "vamr4846_vama.retur_penjualan_detail";
			}else if($jenis_transaksi == 'rbdttr'){
				$nama_table = "vamr4846_vama.retur_dari_toko";
			}else if($jenis_transaksi == 'rpbr'){
				$nama_table = "vamr4846_vama.retur_pembelian_detail";
			}
			// Akhir Stok Rusak

			foreach ($list as $dt){
				$no++;
				$row 	= array();
				$row[] 	= $no;

				// Awal Stok Jual
				if($jenis_transaksi == 'so'){
					$row[]      = '<span class="text-right">'.number_format($dt->jumlah_so_sebelumnya,'0',',','.').'</span>';
					$row[]      = '<span class="text-right">'.number_format($dt->jumlah_so,'0',',','.').'</span>';
				}else if($jenis_transaksi == 'pb'){
					$row[]      = $dt->no_pembelian;
					$row[]      = $dt->no_faktur;
					$row[]      = '<span class="text-right">'.number_format($dt->jumlah_beli,'0',',','.').'</span>';
				}else if($jenis_transaksi == 'dp'){
					$row[]      = $dt->no_perakitan;
					$row[]      = '<span class="text-right">'.number_format($dt->jumlah_perakitan,'0',',','.').'</span>';					
				}else if($jenis_transaksi == 'rpt'){
					$row[]      = $dt->no_retur_penjualan;
					$row[]      = $dt->no_penjualan;
					$row[]      = '<span class="text-right">'.number_format($dt->jumlah_beli,'0',',','.').'</span>';					
					$row[]      = '<span class="text-right">'.number_format($dt->jumlah_retur,'0',',','.').'</span>';					
				}else if($jenis_transaksi == 'rbdtt'){
					$row[]      = $dt->no_retur_pembelian;
					$row[]      = $dt->nama_toko;
					$row[]      = '<span class="text-right">'.number_format($dt->jumlah_masuk,'0',',','.').'</span>';					
				}else if($jenis_transaksi == 'up'){
					$row[]      = $dt->no_perakitan;
					$row[]      = '<span class="text-right">'.number_format($dt->jumlah_komponen,'0',',','.').'</span>';
				}else if($jenis_transaksi == 'pju'){
					$row[]      = $dt->no_penjualan;
					$row[]      = $dt->nama_customer_pusat;
					$row[]      = '<span class="text-right">'.number_format($dt->jumlah_beli,'0',',','.').'</span>';
					$row[]      = $dt->tipe_penjualan;
				}else if($jenis_transaksi == 'pjt'){
					$row[]      = $dt->no_penjualan;
					$row[]      = $dt->nama_customer_pusat;
					$row[]      = '<span class="text-right">'.number_format($dt->jumlah_beli,'0',',','.').'</span>';
				}else if($jenis_transaksi == 'rpbt'){
					$row[]      = $dt->no_retur_pembelian;
					$row[]      = $dt->no_pembelian;
					$row[]      = '<span class="text-right">'.number_format($dt->jumlah_retur,'0',',','.').'</span>';
				}else if($jenis_transaksi == 'pp'){
					$row[]      = $dt->no_penukaran_poin;
					$row[]      = $dt->nama_customer_pusat;
					$row[]      = '<span class="text-right">'.number_format($dt->jumlah_penukaran,'0',',','.').'</span>';
				}else if($jenis_transaksi == 'hd'){
					$row[]      = $dt->no_hadiah;
					$row[]      = $dt->nama_hadiah;
					$row[]      = $dt->nama_customer_pusat;
					$row[]      = '<span class="text-right">'.number_format($dt->jumlah_hadiah,'0',',','.').'</span>';
				}else if($jenis_transaksi == 'ps'){
					$row[]      = $dt->no_pindah_stok;
					$row[]      = '<span class="text-right">'.number_format($dt->jumlah_pindah,'0',',','.').'</span>';
				}
				// Akhir Stok Jual

				// Awal Stok Rusak
				if($jenis_transaksi == 'rpr'){
					$row[]      = $dt->no_retur_penjualan;
					$row[]      = $dt->no_penjualan;
					$row[]      = '<span class="text-right">'.number_format($dt->jumlah_beli,'0',',','.').'</span>';
					$row[]      = '<span class="text-right">'.number_format($dt->jumlah_retur,'0',',','.').'</span>';
					$row[]      = $dt->nama_customer_pusat;					
				}else if($jenis_transaksi == 'rbdttr'){
					$row[]      = $dt->no_retur_pembelian;
					$row[]      = $dt->nama_toko;
					$row[]      = '<span class="text-right">'.number_format($dt->jumlah_masuk,'0',',','.').'</span>';
				}else if($jenis_transaksi == 'rpbr'){
					$row[]      = $dt->no_retur_pembelian;
					$row[]      = $dt->no_pembelian;
					$row[]      = '<span class="text-right">'.number_format($dt->jumlah_retur,'0',',','.').'</span>';
				}
				// Akhir Stok Rusak

				$row[]  = $dt->catatan;
				$row[]  = $dt->pegawai_save;
				$row[]  = $dt->tanggal_pembuatan;
				
				if($jenis_transaksi == 'pjt'){
					$row[]  = $dt->pegawai_edit;
					$row[]  = $dt->tanggal_pembaharuan;
					$row[]  = $dt->pegawai_masuk;
					$row[]  = $dt->tanggal_masuk;
				}else{
					$row[]  = $dt->pegawai_edit;
					$row[]  = $dt->tanggal_pembaharuan;
				}
				
				$data[] = $row;
			}

			$output = array(	
				"draw"         => $_POST['draw'],
				"recordsTotal" => $this->detail_transaksi->count_all(
					$nama_table, $id_barang_pusat, $id_periode_stok_pusat, 
					$tanggal_periode_awal, $tanggal_periode_akhir, $jenis_perintah
				),
				"recordsFiltered" => $this->detail_transaksi->count_filtered(
					$jenis_query, $id_barang_pusat, $id_periode_stok_pusat, 
					$tanggal_periode_awal, $tanggal_periode_akhir, $jenis_perintah
				),
				"data"            => $data
				// "id_barang_pusat" => $id_barang_pusat, 
				// "data_barang"     => $data_barang,
				// "nama_table"      => $nama_table,
				// "jenis_transaksi" => $jenis_transaksi
			);
			echo json_encode($output);
		}else{
			redirect(base_url(),'barang_pusat');
		}
	}

	public function cek_stok()
	{
		if($this->input->is_ajax_request()){
			$sku         = $this->input->post('sku');
			$jumlah_beli = $this->input->post('jumlah_beli');
			$nomor_nota  = $this->clean_tag_input($this->input->post('no_penjualan'));

			$data_penjualan_master = $this->penjualan_master->get_id($nomor_nota);
			if($data_penjualan_master){
				$id_master = $data_penjualan_master->id_penjualan_m;
			}else{
				$id_master = '';
			}
			
			$data_barang_pusat = $this->barang_pusat->get_sku($sku);
			if($data_barang_pusat){
				$id_barang_pusat = $data_barang_pusat->id_barang_pusat;
			}else{
				$id_barang_pusat = '';
			}

			if($id_master){
				$data_penjualan_detail = $this->penjualan_detail->get_id($id_master,$id_barang_pusat);
				if ($data_penjualan_detail){
					$jumlah_beli_lama = $data_penjualan_detail->jumlah_beli;	
				}else{
					$jumlah_beli_lama = '0';
				}
			}else{
				$jumlah_beli_lama = '0';
			}

			$get_stok 			= $this->barang_pusat->get_stok($sku, $jumlah_beli_lama);
			$stok_sekarang		= $get_stok->total_stok;

			if($jumlah_beli > $stok_sekarang){
				echo json_encode(array(	'status' 			=> 0,
										'nomor_nota' 		=> $nomor_nota,
										'sku'				=> $sku,
										'id_barang_pusat'	=> $id_barang_pusat,
										'id_master'			=> $id_master,
										'stok_skrng' 		=> $get_stok->total_stok,
										'jumlah_lama' 		=> $jumlah_beli_lama,
										'pesan' 			=> "Stok untuk : </br></br>
																SKU</br>
																<b class='text-dark'>".$get_stok->sku."</b></br></br>

																Nama Barang</br>
																<b class='text-dark'>".$get_stok->nama_barang."</b></br></br> 
																
																Saat ini hanya tersisa  : </br> 
																<h2 class='text-danger'>".$stok_sekarang."</h2>"
									   )
								);
			}else{
				echo json_encode(array(	'nomor_nota'		=> $nomor_nota,
										'stok'				=> $stok_sekarang,
										'id_master'			=> $id_master,
										'id_barang_pusat' 	=> $id_barang_pusat,
										'stok_skrng'  		=> $get_stok->total_stok,
										'jumlah_beli_lama' 	=> $jumlah_beli_lama,
										'status' => 1
									   )
								);
			}
		}
	}

	public function ajax_add()
	{
		// Cek user acces menu
		$id_pegawai 	= $this->session->userdata('id_pegawai');
		$cek_useraccess = $this->useraccess_pusat->cek_access($id_pegawai, '19');
		if($cek_useraccess->act_create == '0' or $cek_useraccess->act_create == '-'){
			redirect(base_url().'barang_pusat');
			exit();
		}

		$this->_validate('0');

		// Buat kode barang  baru
		$i             = $this->input;
		$kode_supplier = $i->post('pilih_supplier');
		$kode_jenis    = $i->post('pilih_jenis_barang');
		$kode_kategori = $i->post('pilih_kategori_barang');

		$kode_barang_new  = $this->barang_pusat->ambil_kode_barang_baru($kode_supplier, $kode_jenis, $kode_kategori);
		if($kode_barang_new){
			// $nomor_urut_baru  = $kode_barang_new->jumlah_barang + 1;
			$nomor_urut_baru = $kode_barang_new->kode_barang_baru;
			if($nomor_urut_baru < 10) {
				$pola        = "000".$nomor_urut_baru;
				$kode_barang = $kode_supplier.$kode_jenis.$kode_kategori.$pola;
			}elseif($nomor_urut_baru < 100) {
				$pola        = "00".$nomor_urut_baru;
				$kode_barang = $kode_supplier.$kode_jenis.$kode_kategori.$pola;
			}elseif($nomor_urut_baru < 1000) {
				$pola        = "0".$nomor_urut_baru;
				$kode_barang = $kode_supplier.$kode_jenis.$kode_kategori.$pola;
			}else{
				$pola        = $nomor_urut_baru;
				$kode_barang = $kode_supplier.$kode_jenis.$kode_kategori.$pola;
			}
		}else{
			$kode_barang = $kode_supplier.$kode_jenis.$kode_kategori."0001";
		}
		// End buat kode

        // Start Buat sku  baru
        $sku_new  = $this->barang_pusat->ambil_sku_baru($kode_jenis);
        if($sku_new->sku_baru > 0) {
			// $nomor_urut_baru  = $sku_new->jumlah_barang + 1;
			$nomor_urut_baru = $sku_new->sku_baru;
			if($nomor_urut_baru < 9) {
				$pola = "000".$nomor_urut_baru;
			}elseif($nomor_urut_baru < 99) {
				$pola = "00".$nomor_urut_baru;
			}elseif($nomor_urut_baru < 999) {
				$pola = "0".$nomor_urut_baru;
			}else{
				$pola = $nomor_urut_baru;
			}
			$sku = "1".$kode_jenis.$pola;
        }else{
			$nomor_urut_baru = 1;
			$sku             = "1".$kode_jenis."0001";
        }
        // End sku baru 

		$data = array(	
			'kode_barang'             => $kode_barang,
			'sku'                     => $sku,
			'total_stok'              => '0',
			'nama_barang'             => $i->post('nama_barang'),
			'kode_supplier'           => $i->post('pilih_supplier'),
			'kode_jenis'              => $i->post('pilih_jenis_barang'),
			'kode_kategori'           => $i->post('pilih_kategori_barang'),
			'modal'                   => str_replace('.','',$i->post('harga_modal_hidden')),
			'resistensi_modal'        => $i->post('resistensi_harga_modal_persen'),
			'modal_bersih'            => str_replace('.','',$i->post('harga_modal_bersih_hidden')),
			'resistensi_harga_eceran' => $i->post('resistensi_harga_eceran_persen'),
			'harga_eceran'            => str_replace('.','',$i->post('harga_eceran_pusat_hidden')),
			'harga_grosir1'           => str_replace('.','',$i->post('harga_grosir1_pusat_hidden')),
			'harga_grosir2'           => str_replace('.','',$i->post('harga_grosir2_pusat_hidden')),
			'foto'                    => 'avatar.jpg',
			'status'                  => $i->post('status'),
			'status2'                 => $i->post('status2'),						
			'id_pegawai_pembuatan'    => $this->session->userdata('id_pegawai'),
			'tanggal_pembuatan'       => date('Y-m-d H:i:s')
		);
		$insert = $this->barang_pusat->save($data);

		// Ambil id barang pusat dengan sku
		$barang_pusat = $this->barang_pusat->get_sku($sku);
		if($barang_pusat){
			$id_barang_pusat = $barang_pusat->id_barang_pusat;
		}else{
			$id_barang_pusat = '0';
		}

		// Awal hitung jumlah toko untuk looping simpan barang toko
        $daftar_toko  = $this->customer_pusat->listing_toko();
        if(count($daftar_toko) > 0){
        	foreach ($daftar_toko as $daftar_toko) { 
				$data2=array(  
					'kode_toko'            => $daftar_toko->kode_customer_pusat,
					'id_barang_pusat'      => $id_barang_pusat,
					'kode_barang'          => $kode_barang,
					'sku'                  => $sku,
					'total_stok'           => '0',
					// 'modal_bersih'         => str_replace('.', '', $i->post('harga_modal_bersih_hidden')),
					'harga_eceran'         => str_replace('.', '', $i->post('harga_eceran_pusat_hidden')),
					'status'               => $i->post('status'),
					'status2'              => $i->post('status2'),
					'id_pegawai_pembuatan' => $this->session->userdata('id_pegawai'),
					'tanggal_pembuatan'    => date('Y-m-d H:i:s')
	        	);
	        	$table = 'vamr4846_toko_mrc.barang_toko_'.$daftar_toko->kode_customer_pusat;
				$this->barang_toko->tambah($data2, $table);
			}
        }
        // Akhir hitung jumlah toko untuk looping simpan barang toko

		echo json_encode(array(
			"status"          => TRUE,
			"id_barang_pusat" => $id_barang_pusat
		));
	}

	public function ajax_edit()
	{
		// Cek user acces menu
		$id_pegawai 	= $this->session->userdata('id_pegawai');
		$cek_useraccess = $this->useraccess_pusat->cek_access($id_pegawai, '19');
		if($cek_useraccess->act_update == '0' or $cek_useraccess->act_update == '-'){
			redirect(base_url().'barang_pusat');
			exit();
		}

		$id_barang_pusat = $this->input->post('id_barang_pusat');
		if($id_barang_pusat == ''){
			echo json_encode(array(
				'id_barang_pusat' => $id_barang_pusat
			));
			// redirect(base_url().'barang_pusat');
			exit();
		}else{
			$data = $this->barang_pusat->get_by_id($id_barang_pusat);
			echo json_encode(array(
									'data' 			=> $data,
									'foto'			=> "<div class='thumb'>
															<img id='foto' name='foto' src='assets/upload/image/barang/thumbs/$data->foto' 
															class='thumb-img'>
														</div>"
								  )
							);			
		}
	}

	public function ajax_harga_eceran_toko()
	{
		// Cek user acces menu
		$id_pegawai 	= $this->session->userdata('id_pegawai');
		$cek_useraccess = $this->useraccess_pusat->cek_access($id_pegawai, '19');
		if($cek_useraccess->act_update == '0' or $cek_useraccess->act_update == '-'){
			redirect(base_url().'barang_pusat');
			exit();
		}

		$id_barang_pusat = $this->input->post('id_barang_pusat');
		if($id_barang_pusat == ''){
			echo json_encode(array(
				'status' => '0',
				'pesan'  => 'Data barang tidak terdaftar'
			));
			exit();
		}

		$kode_toko = $this->input->post('kode_toko');
		if($kode_toko == ''){
			echo json_encode(array(
				'status' => '0',
				'pesan'  => 'Data toko tidak terdaftar!'
			));
			exit();
		}

		$data = $this->barang_toko->get_by_id_barang_pusat($id_barang_pusat, $kode_toko);
		if($data){
			echo json_encode(array(
				'status'      => '1',
				'eceran_toko' => $data->harga_eceran,
			));
		}else{
			echo json_encode(array(
				'status' => '0',
				'pesan'  => 'Data barang tidak di temukan!'
			));
			exit();
		}
	}

	public function ajax_update_harga_eceran_toko()
	{
		// Cek user acces menu
		$id_pegawai 	= $this->session->userdata('id_pegawai');
		$cek_useraccess = $this->useraccess_pusat->cek_access($id_pegawai, '19');
		if($cek_useraccess->act_update == '0' or $cek_useraccess->act_update == '-'){
			redirect(base_url().'barang_pusat');
			exit();
		}

		$id_barang_pusat = $this->input->post('id_barang_pusat');
		if($id_barang_pusat == ''){
			echo json_encode(array(
				'status' => '0',
				'pesan'  => 'Data barang tidak terdaftar'
			));
			exit();
		}

		$kode_toko = $this->input->post('kode_toko');
		if($kode_toko == ''){
			echo json_encode(array(
				'status' => '0',
				'pesan'  => 'Data toko tidak terdaftar!'
			));
			exit();
		}

		$harga_eceran = $this->input->post('harga_eceran');
		if($harga_eceran == ''){
			echo json_encode(array(
				'status' => '0',
				'pesan'  => 'Harap isikan harga eceran!'
			));
			exit();
		}

		$data = $this->barang_toko->get_by_id_barang_pusat($id_barang_pusat, $kode_toko);
		if($data){
			$simpan = $this->barang_toko->update_harga_by_id(
				$id_barang_pusat, $kode_toko, $harga_eceran, $this->session->userdata('id_pegawai')
			);
			if($simpan){
				echo json_encode(array(
					'status' => '1'
				));
			}else{
				echo json_encode(array(
					'status' => '0',
					'pesan'  => 'Gagal menyimpan eceran toko!'
				));
				exit();
			}
		}else{
			echo json_encode(array(
				'status' => '0',
				'pesan'  => 'Data barang toko tidak ditemukan'
			));
		}
	}

	public function ajax_import_barang_pusat(){
		// Cek user acces menu
		$id_pegawai 	= $this->session->userdata('id_pegawai');
		$cek_useraccess = $this->useraccess_pusat->cek_access($id_pegawai, '19');
		if($cek_useraccess->act_update == '0' or $cek_useraccess->act_update == '-'){
			redirect(base_url().'barang_pusat');
			exit();
		}

		if($id_pegawai == '1'){
			$data_barang = $this->barang_pusat->ambil_barang_pusat_lama();
			if($data_barang){
				$no = 1;
				ini_set('max_execution_time', 0); 
				ini_set('memory_limit','2048M');
				foreach($data_barang->result() as $b){
					// Validasi barang
					$validasi_barang = $this->barang_pusat->validasi_barang($b->SKU, $b->Kode_Barang);
					if($validasi_barang){
					}else{
						$sql_insert_barang = "
							INSERT INTO vamr4846_vama.barang_pusat (
								kode_barang, sku, nomor_urut, 
								total_stok, total_stok_rusak, nama_barang, 
								kode_supplier, kode_jenis, kode_kategori, 
								modal, resistensi_modal, modal_bersih, 
								resistensi_harga_eceran, harga_eceran, harga_grosir1, harga_grosir2,
								foto, status, status2, status_hapus,
								tanggal_pembuatan, tanggal_pembaharuan
							)  
							VALUES (
								'".$b->Kode_Barang."', '".$b->SKU."', '".$no."', 
								'0', '0', '".$b->Nama_Barang."',
								'".$b->Kode_Supplier."', '".$b->Kode_Jenis."', '".$b->Kode_Kategori."', 
								'".$b->Modal."', '".$b->Resistensi_Modal."', '".$b->Modal_Bersih."', 
								'".$b->Resistensi_Harga_Eceran."', '".$b->Harga_Eceran."', 
								'".$b->Harga_Grosir."', '".$b->Harga_Bengkel."',
								'avatar.jpg', '".$b->Status."', '".$b->Status2."', '".$b->Status_Hapus."',
								'".$b->Tanggal_Pembuatan."', '".$b->Tanggal_Pembaharuan."'
							)
						";
						$this->db->query($sql_insert_barang);

						// Validasi jenis barang
						$validasi_jenis = $this->barang_pusat->validasi_jenis($b->Kode_Jenis);
						if($validasi_jenis){
						}else{
							$j = $this->barang_pusat->ambil_jenis_barang($b->Kode_Jenis);
							$sql_insert_jenis_barang = "
								INSERT INTO vamr4846_vama.jenis_barang (
									kode_jenis, nama_jenis, urutan, status_hapus, 
									id_pegawai_pembuatan, tanggal_pembuatan, tanggal_pembaharuan
								)  
								VALUES (
									'".$j->Kode_Jenis."', '".$j->Nama_Jenis."', '0', 'TIDAK', 
									'1', '".$j->Tanggal_Pembuatan."', '".$j->Tanggal_Pembaharuan."'
								)
							";
							$this->db->query($sql_insert_jenis_barang);
						}


						// Validasi kategori barang
						$validasi_kategori = $this->barang_pusat->validasi_kategori($b->Kode_Jenis, $b->Kode_Kategori);
						if($validasi_kategori){
						}else{
							$k = $this->barang_pusat->ambil_kategori_barang($b->Kode_Jenis, $b->Kode_Kategori);
							$sql_insert_kategori_barang = "
								INSERT INTO vamr4846_vama.kategori_barang (
									kode_jenis, kode_kategori, nama_kategori, urutan, status_hapus, 
									id_pegawai_pembuatan, tanggal_pembuatan, tanggal_pembaharuan
								)  
								VALUES (
									'".$k->Kode_Jenis."', '".$k->Kode_Kategori."', '".$k->Nama_Kategori."', '0', 'TIDAK',
									'1', '".$k->Tanggal_Pembuatan."', '".$k->Tanggal_Pembaharuan."'
								)
							";
							$this->db->query($sql_insert_kategori_barang);
						}

						// Validasi supplier
						$validasi_supplier = $this->barang_pusat->validasi_supplier($b->Kode_Supplier);
						if($validasi_supplier){
						}else{
							$s = $this->barang_pusat->ambil_supplier($b->Kode_Supplier);
							$sql_insert_supplier = "
								INSERT INTO vamr4846_vama.supplier (
									kode_supplier, nama_supplier, no_siup, alamat_supplier, asal_supplier,
									tipe_bisnis, tipe_supplier, kontak_pribadi, jabatan, 
									telephone1, telephone2, handphone1, handphone2, fax, email, foto, status_hapus, 
									id_pegawai_pembuatan, tanggal_pembuatan, tanggal_pembaharuan
								)  
								VALUES (
									'".$s->Kode_Supplier."', '".$s->Nama_Supplier."', '".$s->No_SIUP."', '".$s->Alamat_Supplier."', '
									".$s->Asal_Supplier."', '".$s->Tipe_Bisnis."', '".$s->Tipe_Supplier."', '".$s->Contact_Person."', 
									'".$s->Jabatan."',  '".$s->Telephone1."', '".$s->Telephone2."', '".$s->Handphone1."', 
									'".$s->Handphone2."', '".$s->Fax."', '".$s->Email."', '', 'TIDAK', 
									'1', '".$s->Tanggal_Pembuatan."', '".$s->Tanggal_Pembaharuan."'
								)
							";
							$this->db->query($sql_insert_supplier);
						}

						$no = $no + 1;
					}
				}
			}
			if($no > 1){
				echo json_encode(array(
					'status' => 1,
					'pesan'  => 'Data barang pusat berhasil di import.'
				));
			}else{
				echo json_encode(array(
					'status' => 0,
					'pesan'  => 'Tidak ada data barang pusat yg di import !'
				));
			}
		}else{
			echo json_encode(array(
				'status' => 0,
				'pesan'  => 'anda tidak dizinkan untuk import barang'
			));
		}
	} 

	public function detail_transaksi(){
		if($this->input->is_ajax_request()){
			// Cek user acces menu
			$id_pegawai 	= $this->session->userdata('id_pegawai');
			$cek_useraccess = $this->useraccess_pusat->cek_access($id_pegawai, '19');
			if($cek_useraccess->act_read == '0' or $cek_useraccess->act_read == '-'){
				echo json_encode(array(
					'status' => 0,
					'pesan'  => 'Maaf anda tidak di izinkan untuk melihat detail transaksi'
				));
				exit();
			}

			// Validasi master barang pusat by id
			$id_barang_pusat       = $this->input->post('id_barang_pusat');
			$id_periode_stok_pusat = $this->input->post('id_periode_stok_pusat');
			$data_periode          = $this->periode_stok_pusat->get_by_id($id_periode_stok_pusat);
			
			// Detail barang dan transaksi stok jual 
			$data_barang = $this->barang_pusat->get_detail_transaksi(
				$id_barang_pusat, $id_periode_stok_pusat, $data_periode->tanggal_periode_awal, $data_periode->tanggal_periode_akhir
			);

			// Detail transaksi stok rusak
			$data_barang_rusak = $this->barang_pusat->get_detail_transaksi_rusak(
				$id_barang_pusat, $id_periode_stok_pusat, $data_periode->tanggal_periode_awal, $data_periode->tanggal_periode_akhir
			);

			echo json_encode(array(
				'status'                  => 1,
				'data_stok_jual'          => $data_barang,
				'data_stok_rusak'         => $data_barang_rusak,
				'kode_periode_stok_pusat' => $data_periode->kode_periode_stok_pusat,
				'tanggal_periode_awal'    => $data_periode->tanggal_periode_awal,
				'tanggal_periode_akhir'   => $data_periode->tanggal_periode_akhir,
				'foto'                    => "<div class='inbox-item-img'>
												<img id='foto' name='foto' src='assets/upload/image/barang/thumbs/$data_barang->foto' 
												class='thumb-md'>
											  </div>"
			));			
		}else{
			redirect(base_url().'barang_pusat');
		}
	}

	public function ajax_update(){
		// Cek user acces menu
		$id_pegawai 	= $this->session->userdata('id_pegawai');
		$cek_useraccess = $this->useraccess_pusat->cek_access($id_pegawai, '19');
		if($cek_useraccess->act_update == '0' or $cek_useraccess->act_update == '-'){
			redirect(base_url().'barang_pusat');
			exit();
		}	
		
		$i 					= $this->input;
		$id_barang_pusat 	= $i->post('id_barang_pusat');
		if($id_barang_pusat == ''){
			redirect(base_url().'barang_pusat');
		}

		$this->_validate_update($id_barang_pusat);
		$barang = $this->barang_pusat->get_by_id($id_barang_pusat);
		if($barang){
			// Validasi perubahan harga eceran, jika iya rubah harga eceran toko
			if(str_replace('.','',$i->post('harga_eceran_pusat_hidden')) <> $barang->harga_eceran){
				// Awal hitung jumlah toko untuk looping update eceran barang toko
		        $daftar_toko  = $this->customer_pusat->listing_toko();
		        if(count($daftar_toko) > 0){
		        	foreach ($daftar_toko as $daftar_toko) { 
						$this->barang_toko->update_harga_by_id(
							$id_barang_pusat, 
							$daftar_toko->kode_customer_pusat, 
							str_replace('.', '', $i->post('harga_eceran_pusat_hidden')),
							$this->session->userdata('id_pegawai')
						);
					}
		        }
		        // Akhir hitung jumlah toko untuk looping update eceran barang toko
			}

			$data = array(
				'nama_barang'             => $i->post('nama_barang'),
				// 'kode_supplier'           => $i->post('pilih_supplier'),
				// 'kode_jenis'              => $i->post('pilih_jenis_barang'),
				// 'kode_kategori'           => $i->post('pilih_kategori_barang'),
				'modal'                   => str_replace('.','',$i->post('harga_modal_hidden')),
				'resistensi_modal'        => $i->post('resistensi_harga_modal_persen'),
				'modal_bersih'            => str_replace('.','',$i->post('harga_modal_bersih_hidden')),
				'resistensi_harga_eceran' => $i->post('resistensi_harga_eceran_persen'),
				'harga_eceran'            => str_replace('.','',$i->post('harga_eceran_pusat_hidden')),
				'harga_grosir1'           => str_replace('.','',$i->post('harga_grosir1_pusat_hidden')),
				'harga_grosir2'           => str_replace('.','',$i->post('harga_grosir2_pusat_hidden')),
				'status'                  => $i->post('status'),
				'status2'                 => $i->post('status2'),						
				'id_pegawai_pembaharuan'  => $this->session->userdata('id_pegawai')
			);
			$this->barang_pusat->update(array('id_barang_pusat' => $id_barang_pusat), $data);
			
			echo json_encode(array(
				"status"          => TRUE,
				"id_barang_pusat" => $id_barang_pusat
			));
		}else{
			exit();
		}
	}

	public function ajax_verifikasi_delete()
	{
		// Cek user acces menu
		$id_pegawai 	= $this->session->userdata('id_pegawai');
		$cek_useraccess = $this->useraccess_pusat->cek_access($id_pegawai, '19');
		if($cek_useraccess->act_delete == '0' or $cek_useraccess->act_delete == '-'){
			redirect(base_url().'barang_pusat');
			exit();
		}

		$id_barang_pusat 			= $this->input->post('id_barang_pusat');
		if($id_barang_pusat == ''){
			redirect(base_url().'barang_pusat');
			exit();
		}

		$data_barang_pusat 			= $this->barang_pusat->get_id($id_barang_pusat);
		$sku 						= $data_barang_pusat->sku;
		$nama_barang 				= $data_barang_pusat->nama_barang;
		$get_stok 					= $this->barang_pusat->lihat_stok($id_barang_pusat);
		$stok 						= $get_stok->total_stok;
		
		if($stok > 0){
			// Tampilkan informasi stok
			echo json_encode(array(
									'status' 	=> 0,
									'pesan' 	=> "<b class='text-danger'>Maaf barang ini tidak bisa dihapus !</b></br></br>

													SKU : </br>
													<b class='text-dark'>".$sku."</b></br></br>

													Nama Barang : </br>
													<b class='text-dark'>".$nama_barang."</b></br></br>
													
													Karna masih memiliki stok sebanyak : </br>
													<h2 class='text-dark'>".$stok."</h2>",
									'footer'	=> 	"<button type='button' class='btn btn-primary waves-effect' 
													data-dismiss='modal'>Ok, Saya Mengerti</button>"
									)
							);
		}else{
			echo json_encode(array(
									'pesan' 	=> "<b class='text-danger'>Yakin ingin menghapus barang ini ? </b></br></br>

													SKU : </br>
													<b class='text-dark'>".$sku."</b></br></br>

													Nama Barang : </br>
													<b class='text-dark'>".$nama_barang."</b>",
									'footer'	=> 	"<button onclick='delete_barang_pusat($id_barang_pusat)' 
														type='button' class='btn btn-primary waves-effect waves-light' 
														data-dismiss='modal' autofocus>Iya, Hapus</button> 
													<button type='button' class='btn btn-default waves-effect' 
														data-dismiss='modal'>Batal</button>"
								  	)
							);
		}
	}

	public function ajax_delete()
	{
		// Cek user acces menu
		$id_pegawai 	= $this->session->userdata('id_pegawai');
		$cek_useraccess = $this->useraccess_pusat->cek_access($id_pegawai, '19');
		if($cek_useraccess->act_delete == '0' or $cek_useraccess->act_delete == '-'){
			redirect(base_url().'barang_pusat');
			exit();
		}

		if($this->input->is_ajax_request()){
			$id_barang_pusat			= $this->input->post('id_barang_pusat');
			if($id_barang_pusat == ''){
				redirect(base_url().'barang_pusat');
				exit();
			}

			// Validasi stok sebelum hapus
			$data_barang_pusat 			= $this->barang_pusat->get_id($id_barang_pusat);
			$sku 						= $data_barang_pusat->sku;
			$nama_barang 				= $data_barang_pusat->nama_barang;
			$get_stok 					= $this->barang_pusat->lihat_stok($id_barang_pusat);
			$stok 						= $get_stok->total_stok;
			
			if($stok > 0){
				// Tampilkan informasi stok
				echo json_encode(array(
										'status' 	=> 0,
										'pesan' 	=> "
														Maaf barang tidak bisa dihapus !</br></br>

														SKU : </br>
														<b class='text-dark'>".$sku."</b></br></br>

														Nama Barang : </br>
														<b class='text-dark'>".$nama_barang."</b></br></br>
														
														Karna masih memiliki stok sebanyak : </br>
														<h2 class='text-dark'>".$stok."</h2> 
														"));
			}else{
				// Hapus data hanya mengupdate status hapus menjadi = iya 
				$this->barang_pusat->update_status_hapus($id_barang_pusat, $id_pegawai);
				echo json_encode(array('status' => 1));
			}		
		}
	}

	private function _validate()
	{
		$data                 = array();
		$data['error_string'] = array();
		$data['inputerror']   = array();
		$data['status']       = TRUE;
		$id_barang_pusat      = $this->input->post('id_barang_pusat');
		$nama_barang          = $this->input->post('nama_barang');

		if($this->input->post('pilih_supplier') == ''){
			$data['inputerror'][] 	= 'pilih_supplier';
			$data['error_string'][] = 'Supplier wajib dipilih';
			$data['status'] 		= FALSE;
		}

		if($this->input->post('pilih_jenis_barang') == ''){
			$data['inputerror'][]	= 'pilih_jenis_barang';
			$data['error_string'][] = 'Jenis barang wajib dipilih';
			$data['status'] 		= FALSE;
		}

		if($this->input->post('pilih_kategori_barang') == ''){
			$data['inputerror'][] 	= 'pilih_kategori_barang';
			$data['error_string'][] = 'Kategori barang wajib dipilih';
			$data['status'] 		= FALSE;
		}


		if($nama_barang == ''){
			$data['inputerror'][] 	= 'nama_barang';
			$data['error_string'][] = 'Nama barang pusat wajib diisi';
			$data['status'] 		= FALSE;
		}	

		// Cek nama barang
		$validisi_nama = $this->barang_pusat->get_by_nama($nama_barang);
		if($validisi_nama){
			if($id_barang_pusat <> $validisi_nama->id_barang_pusat){
				$data['inputerror'][] 	= 'nama_barang';
				$data['error_string'][] = 'Nama barang sudah digunakan, harap ganti nama barang';
				$data['status'] 		= FALSE;
			}
		}

		if($data['status'] === FALSE){
			echo json_encode($data);
			exit();
		}
	}

	private function _validate_update()
	{
		$data                 = array();
		$data['error_string'] = array();
		$data['inputerror']   = array();
		$data['status']       = TRUE;
		$id_barang_pusat      = $this->input->post('id_barang_pusat');
		$nama_barang          = $this->input->post('nama_barang');

		if($nama_barang == ''){
			$data['inputerror'][] 	= 'nama_barang';
			$data['error_string'][] = 'Nama barang pusat wajib diisi';
			$data['status'] 		= FALSE;
		}	

		// Cek nama barang
		$validisi_nama = $this->barang_pusat->get_by_nama($nama_barang);
		if($validisi_nama){
			if($id_barang_pusat <> $validisi_nama->id_barang_pusat){
				$data['inputerror'][] 	= 'nama_barang';
				$data['error_string'][] = 'Nama barang sudah digunakan, harap ganti nama barang';
				$data['status'] 		= FALSE;
			}
		}

		if($data['status'] === FALSE){
			echo json_encode($data);
			exit();
		}
	}

	// Filter kategori barang per jenis barang yang di pilih
	public function filter_kategori_barang(){
		$kode_jenis = $this->input->post('pilih_jenis_barang');
		$data 	 	= $this->kategori_barang->filter_kategori($kode_jenis);
		if($data){
		 $hasil = array('status' => 'success', 'data' => $data, 'message' => 'data ditemukan');     
		}else{
		 $hasil = array('status' => 'error', 'data' => $data, 'message' => 'data tidak ditemukan'); 
		}
		echo json_encode($hasil);
	}

	//Untuk proses upload foto
	public function proses_upload(){
    	$sku 					 	= $this->input->post('sku');
    	// $nama_barang 			 = $this->input->post('nama_barang');

        $config['upload_path']   	= './assets/upload/image/barang/'; 
        $config['allowed_types'] 	= 'gif|jpg|png|bmp'; 
        $config['max_size']         = '2000'; //max 2mb
        // $config['file_name'] 	 	= $sku.'-'.$nama_barang; nama foto : sku - nama
        $config['file_name'] 	 	= $sku;
        
        $this->load->library('upload',$config);
        if($this->upload->do_upload('userfile')){
        	$nama 	= $this->upload->data('file_name');
	    	$token 	= $this->input->post('token_foto');

        	// $data 	= $this->upload->data('userfile');
	    	$upload_data 	= array('uploads' =>$this->upload->data());
	        $gambar 		= $upload_data['uploads']['file_name'];
	        
	        $this->db->insert('foto',array('sku'=>$sku,'nama_foto'=>$nama,'token'=>$token)); //Simpan ke database
			$this->thumbnail_produk($gambar); //thumbnail
		}
	}

	public function ajax_daftar_foto()
	{
		if($this->input->is_ajax_request()){
			$sku 			= $this->input->post('sku');
			$daftar_foto 	= $this->barang_pusat->ambil_daftar_foto($sku);
			$foto_profile 	= $this->barang_pusat->get_sku($sku);

			if($foto_profile){
				$alamat_foto=$foto_profile->foto;
			}

			if($daftar_foto->num_rows() > 0){
				$json['status'] 	= 1;
				$json['datanya'] 	= "";
				$json['datanya'] 	.= "<div class='row port m-b-20'>
			                                <div class='portfolioContainer'>";
				$no = 0;	
				foreach($daftar_foto->result() as $df){
					$no++;
					$json['datanya'] .= "
	                                        <div class='col-sm-3 col-md-3'>
	                                            <div class='thumb'>
	                                                <a name='foto' href='assets/upload/image/barang/$df->nama_foto' class='image-popup' title='Foto $no'>
	                                                    <img src='assets/upload/image/barang/thumbs/$df->nama_foto' class='thumb-img' alt='work-thumbnail'>
	                                                </a>
	                                                <div class='gal-detail'>
	                                                    <p class='text-muted text-left'>Foto - $no</p>
	                                                    <span>
									                    	<div class='row'>
								              	      			<div class='col-md-6 pull-left'>
										";
					
					// Bukan foto profile
					if($alamat_foto<>$df->nama_foto){
						$json['datanya'] .= "
									                                <a class='btn btn-rounded btn-default btn-block' href='javascript:void(0)' title='Jadikan Profile' onclick='jadikan_profile_foto(".$df->id_foto.")'><i class='fa fa-camera'></i> 
																	</a>
																</div>
						";
					}
					// Tandai sebagai foto profile
					else{
						$json['datanya'] .= "
									                                <a class='btn btn-rounded btn-primary btn-block' href='javascript:void(0)' title='Jadikan Profile' onclick='jadikan_profile_foto(".$df->id_foto.")'><i class='fa fa-camera'></i> 
																	</a>
																</div>
						";
					}

					$json['datanya'] 	.= "
																<div class='col-md-6 pull-right'>
																	<a class='btn btn-rounded btn-danger btn-block' href='javascript:void(0)' title='Hapus' onclick='delete_foto(".$df->id_foto.")'><i class='fa fa-remove'></i>
																	</a>
																</div>
															</div>
			                    						</span>
			                    					</div>
			                    				</div>
			                    			</div>
					";
				}
				$json['datanya'] .= "</div></div>";
			}else{
				$json['status'] 		= 0;
			}

			echo json_encode($json);
		}
	}	

	//function untuk thumbnail
	public function thumbnail_produk($gambar) {
	    $source_path = './assets/upload/image/barang/'.$gambar;
	    $target_path = './assets/upload/image/barang/thumbs';
	    $config_manip = array(
						        'image_library' 	=> 'gd2',
						        'source_image' 		=> $source_path,
						        'new_image' 		=> $target_path,
						        'create_thumb'      => TRUE,
					            'quality'           => "100%",
					            'maintain_ratio'    => TRUE,
					            'width'             => 360, // Pixel
					            'height'            => 360, // Pixel
					            'x_axis'            => 0,
					            'y_axis'            => 0,
					            'thumb_marker'      => ''
	    					);
	    $this->load->library('image_lib', $config_manip);
	    if (!$this->image_lib->resize()) {
	        echo $this->image_lib->display_errors($gambar);
	    }
	    // clear //
	    $this->image_lib->clear();
	}

	//Untuk menghapus foto
	public function remove_foto(){
		//Ambil token foto
		$token=$this->input->post('token');
		$foto=$this->db->get_where('foto',array('token'=>$token));

		if($foto->num_rows() > 0){
			$hasil=$foto->row();
			$nama_foto=$hasil->nama_foto;
			if(file_exists($file='./assets/upload/image/barang/'.$nama_foto)){
				unlink($file);
			}
			$this->db->delete('foto',array('token'=>$token));
		}


		echo "{}";
	}

	// Awal delete foto
	public function ajax_delete_foto()
	{
		if($this->input->is_ajax_request()){
			$id_foto	= $this->input->post('id_foto');
			$foto 		= $this->db->get_where('foto',array('id_foto'=>$id_foto));

			if($foto->num_rows()>0){
				$hasil=$foto->row();
				$nama_foto=$hasil->nama_foto;
				if(file_exists($file='./assets/upload/image/barang/'.$nama_foto)){
					unlink($file);
				}
				if(file_exists($file='./assets/upload/image/barang/thumbs/'.$nama_foto)){
					unlink($file);
				}
				$this->db->delete('foto',array('id_foto'=>$id_foto));
				echo json_encode(array('status' => 1));
			}
		}
	}

	// Awal jadikan foto profile barang
	public function ajax_jadikan_foto_profile()
	{
		if($this->input->is_ajax_request()){
			$id_foto	= $this->input->post('id_foto');
			$ambil_foto	= $this->barang_pusat->ambil_nama_foto($id_foto);
			if ($ambil_foto){
				$sku 		= $ambil_foto->sku;
				$nama_foto 	= $ambil_foto->nama_foto;
			}
			$this->barang_pusat->update_foto_profile($sku,$nama_foto);
			echo json_encode(array('status' => 1));
		}
	}

	// Stok opname
	public function ajax_cari_barang_untuk_transaksi()
	{
		if($this->input->is_ajax_request()){
			$sku 				= $this->input->post('sku');			
			$data_barang 		= $this->barang_pusat->ambil_barang_untuk_transaksi($sku);				

			if($data_barang->num_rows() > 0){
				$json['status'] 		= 1;
				$json['data'] 			= $data_barang->row();
			}else{
				$json['status'] 		= 0;
				$json['pesan'] 			= "SKU : </br>
										   <b class='text-dark'>$sku</b></br></br>  
										   <b class='text-danger'>Tidak terdaftar</b>";
			}

			echo json_encode($json);
		}else{
			redirect(base_url().'barang_pusat');
			exit();
		}
	}

	// Pembelian
	public function ajax_cari_barang_untuk_pembelian()
	{
		if($this->input->is_ajax_request()){
			$id_pegawai     = $this->session->userdata('id_pegawai');
			$cek_useraccess = $this->useraccess_pusat->cek_access($id_pegawai, '26');
			if($cek_useraccess->act_create == '1'){
				$sku 			= $this->input->post('sku');
				$kode_supplier	= $this->input->post('kode_supplier');
				$nama_supplier	= $this->input->post('nama_supplier');
				$data_barang 	= $this->barang_pusat->ambil_barang_untuk_pembelian($sku, $kode_supplier);				

				if($data_barang){
					$json['status'] = 1;
					$json['data']   = $data_barang;
				}else{
					$json['status'] = 0;
					$json['pesan']  = "	SKU : </br>
										<b class='text-dark'>".$sku."</b></br></br>  
										<b class='text-danger'>Tidak terdaftar di supplier ini!</b></br></br>
										Kode Supplier : <b class='text-dark'>".$kode_supplier."</b></br>
										Nama Supplier : <b class='text-dark'>".$nama_supplier."</b>
									";
				}
			}else{
				$json['status'] = 2;
				$json['pesan']  = "	Maaf anda tidak di izinkan 
									untuk menambahkan barang pembelian.";
			}
			echo json_encode($json);
		}else{
			redirect(base_url().'pembelian');
			exit();
		}
	}
	// Akhir pencarian barang dengan sku untuk transaksi
}

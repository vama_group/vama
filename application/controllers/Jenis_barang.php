<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Jenis_barang extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('pegawai_pusat_model','pegawai_pusat');
        $this->load->model('useraccess_pusat_model','useraccess_pusat');
		$this->load->model('jenis_barang_model','jenis_barang');
	}

	public function index()
	{
		// Cek user acces menu
		$id_pegawai = $this->session->userdata('id_pegawai');
		if($id_pegawai == ''){
			redirect(base_url().'login');
		}
		
		$cek_useraccess = $this->useraccess_pusat->cek_access($id_pegawai, '17');
		if($cek_useraccess){
			if($cek_useraccess->act_read == '0' or $cek_useraccess->act_read == '-'){
				redirect(base_url().'dashboard');
			}					
		}else{
			redirect(base_url().'dashboard');
		}

		// cari data pegawai
		$data_pegawai = $this->pegawai_pusat->get_by_id($id_pegawai);
		if($data_pegawai){
			$data['access_create']   = $cek_useraccess->act_create;
			$data['access_update']   = $cek_useraccess->act_update;
			$data['access_delete']   = $cek_useraccess->act_delete;
			$data['data_pegawai']    = $data_pegawai;
			$data['data_induk_menu'] = $this->useraccess_pusat->get_induk_menu($id_pegawai);
			
			$data['atribut_halaman'] = 'Jenis Barang';
			$data['halaman_list']    = $this->load->view('admin/master_data/jenis_barang/list',$data,true);
			$data['halaman_form']    = $this->load->view('admin/master_data/jenis_barang/form',$data,true);
			$data['halaman_plugin']  = $this->load->view('admin/master_data/jenis_barang/plugin',$data,true);
			$data['isi_halaman']     = $this->load->view('admin/master_data/jenis_barang/index',$data,true);
			$this->load->view('admin/layout',$data);
		}else{
			redirect(base_url().'login');
		}
	}

	public function ajax_list()
	{
		// Cek user acces menu
		$id_pegawai 	= $this->session->userdata('id_pegawai');
		$cek_useraccess = $this->useraccess_pusat->cek_access($id_pegawai, '17');
		if($cek_useraccess->act_read == '0' or $cek_useraccess->act_read == '0'){
			redirect(base_url().'dashboard');
			exit();
		}

		$id_periode_stok_pusat = $this->input->post('id_periode_stok_pusat');
		if($id_periode_stok_pusat == ''){
			redirect(base_url().'jenis_barang');
			exit();
		}

		$list 	= $this->jenis_barang->get_datatables();
		$data 	= array();
		$no 	= $_POST['start'];
		foreach ($list as $jenis_barang) {
			$no++;
			$row 	= array();
			$row[] 	= $no;
			
			//add html for action
			if($cek_useraccess->act_update == 1){
				$tombol_edit 	 = '<a class="btn btn-rounded btn-xs btn-default" href="javascript:void(0)" 
										title="Edit" onclick="edit_jenis_barang('."'".$jenis_barang->id_jenis_barang."'".')">
										<i class="fa fa-pencil" style="color: blue;"></i>
									</a>';
			}else{$tombol_edit 	 = '';}

			if($cek_useraccess->act_delete == 1){
				$tombol_hapus 	 = '<a class="btn btn-rounded btn-xs btn-default" href="javascript:void(0)" 
										title="Hapus" onclick="verifikasi_delete('."'".$jenis_barang->id_jenis_barang."'".')">
										<i class="fa fa-remove" style="color: red;"></i>
				  					</a>';
			}else{$tombol_hapus  = '';}

			if($cek_useraccess->act_update == 1 or $cek_useraccess->act_delete == 1){
				$row[] = '					
						  	'.$tombol_edit.'
						  	'.$tombol_hapus.'
					  	 ';
			}

			$row[] 	= $jenis_barang->kode_jenis;
			$row[] 	= $jenis_barang->nama_jenis;
			
			$row[]	= $jenis_barang->pegawai_save;
			$row[] 	= $jenis_barang->tanggal_pembuatan;
			$row[] 	= $jenis_barang->pegawai_edit;
			$row[] 	= $jenis_barang->tanggal_pembaharuan;
			$data[] = $row;
		}

		$output = array(	"draw" 				=> $_POST['draw'],
							"recordsTotal" 		=> $this->jenis_barang->count_all(),
							"recordsFiltered" 	=> $this->jenis_barang->count_filtered(),
							"data" 				=> $data,
						);
		echo json_encode($output);
	}

	public function ajax_add()
	{
		// Cek user acces menu
		$id_pegawai 	= $this->session->userdata('id_pegawai');
		$cek_useraccess = $this->useraccess_pusat->cek_access($id_pegawai, '17');
		if($cek_useraccess->act_create == '0' or $cek_useraccess->act_create == '-'){
			redirect(base_url().'jenis_barang');
			exit();
		}

		$this->_validate('0');

		// Buat kode baru
		$akhir = $this->jenis_barang->akhir();
		if($akhir){
			$id_jenis_barang = $akhir->id_jenis_barang;
		}else{
			$id_jenis_barang = 0;
		}

		
		if ($id_jenis_barang < 9){
			$depan 		= '00';
		}elseif($id_jenis_barang< 100) {
			$depan		= '0';
		}else{
			$depan		= '';
		}

		$jumlah			= $id_jenis_barang+1;
		$kode_jenis		= $depan.$jumlah;
		// Akhir buat kode baru

		$data = array(
						'kode_jenis' 			=> $kode_jenis,
						'nama_jenis' 			=> $this->input->post('nama_jenis_barang'),
						'id_pegawai_pembuatan'	=> $this->session->userdata('id_pegawai'),
						'tanggal_pembuatan'		=> date('Y-m-d H:i:s')
					);
		$insert = $this->jenis_barang->save($data);
		echo json_encode(array("status" => TRUE));
	}
	
	public function ajax_edit()
	{
		// Cek user acces menu
		$id_pegawai 	= $this->session->userdata('id_pegawai');
		$cek_useraccess = $this->useraccess_pusat->cek_access($id_pegawai, '17');
		if($cek_useraccess->act_update == '0' or $cek_useraccess->act_update == '-'){
			redirect(base_url().'jenis_barang');
			exit();
		}

		$id_jenis_barang = $this->input->post('id_jenis_barang');
		if($id_jenis_barang == ''){
			redirect(base_url().'jenis_barang');
			exit();
		}else{
			$data = $this->jenis_barang->get_by_id($id_jenis_barang);
			echo json_encode($data);			
		}
	}

	public function ajax_update()
	{
		$id_pegawai 	= $this->session->userdata('id_pegawai');
		$cek_useraccess = $this->useraccess_pusat->cek_access($id_pegawai, '17');
		if($cek_useraccess->act_update == '0' or $cek_useraccess->act_update == '0'){
			redirect(base_url().'jenis_barang');
			exit();
		}

		$id_jenis_barang = $this->input->post('id_jenis_barang');
		if($id_jenis_barang == ''){
			redirect(base_url().'jenis_barang');
			exit();
		}

		$this->_validate($id_jenis_barang);
		$data = array(
						'nama_jenis' 				=> $this->input->post('nama_jenis_barang'),
						'id_pegawai_pembaharuan'	=> $this->session->userdata('id_pegawai')
					);
		$this->jenis_barang->update(array('id_jenis_barang' => $this->input->post('id_jenis_barang')), $data);
		echo json_encode(array("status" => TRUE));
	}

	public function ajax_verifikasi_delete()
	{
		// Cek user acces menu
		$id_pegawai 	= $this->session->userdata('id_pegawai');
		$cek_useraccess = $this->useraccess_pusat->cek_access($id_pegawai, '17');
		if($cek_useraccess->act_delete == '0' or $cek_useraccess->act_delete == '-'){
			redirect(base_url().'jenis_barang');
			exit();
		}

		$id_jenis_barang = $this->input->post('id_jenis_barang');
		if($id_jenis_barang == ''){
			redirect(base_url().'jenis_barang');
			exit();
		}
		
		$data = $this->jenis_barang->get_by_id($id_jenis_barang);		
		echo json_encode(array(
								'pesan' 	=> "<b class='text-danger'>Yakin ingin menghapus jenis barang ini ? </b></br></br>
												Jenis Barang : </br>
												<h2 class='text-dark'>".$data->nama_jenis."</h2>",
								'footer'	=> 	"<button onclick='delete_jenis_barang($id_jenis_barang)' 
													type='button' class='btn btn-primary waves-effect waves-light' 
													data-dismiss='modal' autofocus>Iya, Hapus</button> 
												<button type='button' class='btn btn-default waves-effect' 
													data-dismiss='modal'>Batal</button>"
							  	)
						);
	}

	public function ajax_delete()
	{
		$id_pegawai 	= $this->session->userdata('id_pegawai');
		$cek_useraccess = $this->useraccess_pusat->cek_access($id_pegawai, '17');
		if($cek_useraccess->act_delete == '0' or $cek_useraccess->act_delete == '-'){
			redirect(base_url().'jenis_barang');
			exit();
		}

		$id_jenis_barang = $this->input->post('id_jenis_barang');
		if($id_jenis_barang == ''){
			redirect(base_url().'jenis_barang');
			exit();
		}

		$this->jenis_barang->update_status_hapus($id_jenis_barang, $id_pegawai);
		echo json_encode(array("status" => TRUE));
	}

	private function _validate($id_jenis_barang)
	{
		$data 					= array();
		$data['error_string'] 	= array();
		$data['inputerror'] 	= array();
		$data['status']	 		= TRUE;
		$nama_jenis_barang 		= $this->input->post('nama_jenis_barang');

		if($nama_jenis_barang == ''){
			$data['inputerror'][] 	= 'nama_jenis_barang';
			$data['error_string'][] = 'Nama jenis barang wajib diisi';
			$data['status'] 		= FALSE;
		}

		$validasi_nama = $this->jenis_barang->get_by_nama($nama_jenis_barang);
		if($validasi_nama){
			if($id_jenis_barang <> $validasi_nama->id_jenis_barang){
				$data['inputerror'][] 	= 'nama_jenis_barang';
				$data['error_string'][] = 'Nama jenis barang sudah digunakan, harap ganti nama jenis barang';
				$data['status'] 		= FALSE;
			}
		}

		if($data['status'] === FALSE){
			echo json_encode($data);
			exit();
		}
	}

	public function ajax_pencarian_jenis_barang()
	{
		if($this->input->is_ajax_request())
		{
			$keyword 		= $this->input->post('keyword');
			$jenis_barang 	= $this->jenis_barang->ambil_jenis_barang($keyword);

			if($jenis_barang->num_rows() > 0)
			{
				$json['status'] 	= 1;
				$json['datanya'] 	= "<ul id='daftar_autocompleted_jenis_barang' class='list-group'>";
				foreach($jenis_barang->result() as $j)
				{
					$json['datanya'] .= "
						                <li class='list-group-item'>
						                	".$j->kode_jenis." - 
											<span id='data_kode_jenis' style='display:none;'>".$j->kode_jenis."</span>
											<span id='data_nama_jenis'>".$j->nama_jenis."</span>
										</li>
										";
				}
				$json['datanya'] .= "</ul>";
			}
			else
			{
				$json['status'] 		= 0;
				// $json['perintah_sql'] 	= $this->db->last_query();				
			}

			echo json_encode($json);
		}
	}

	public function list_jenis_barang(){
		$id_pegawai = $this->session->userdata('id_pegawai');
		if($id_pegawai){		
			$nj         = $this->input->get('nj');
			$data_jenis = $this->jenis_barang->cari_jenis_barang($nj);
			if($data_jenis){
				echo json_encode($data_jenis);
			}else{
				echo json_encode('');
			}
		}else{
			redirect(base_url().'dashboard');
			exit();
		}
	}
}


<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require('./assets/zebra/vendor/autoload.php');
use Zebra\Client;
use Zebra\Zpl\Image;
use Zebra\Zpl\Builder;
use Zebra\Zpl\GdDecoder;
class Cetak_barcode extends CI_Controller {

	public function index()
	{
		$decoder = GdDecoder::fromPath('happy.png');
		$image = new Image($decoder);

		$zpl = new Builder();
		$zpl->fo(50, 50)->gf($image)->fs();

		$client = new Client('10.0.0.50');
		$client->send($zpl);
	}

}
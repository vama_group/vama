<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Laporan_retur_pembelian extends MY_Controller 
{
	function __construct()
	{
		parent::__construct();
		$this->CI =& get_instance();
		$this->load->model('pegawai_pusat_model','pegawai_pusat');
		$this->load->model('useraccess_pusat_model','useraccess_pusat');
		$this->load->model('supplier_model','supplier');
		$this->load->model('barang_pusat_model','barang_pusat');
		$this->load->model('retur_pembelian_master_model','retur_pembelian_master');
		$this->load->model('retur_pembelian_detail_model','retur_pembelian_detail');
	}

	public function index()
	{
		// Cek user acces menu
		$id_pegawai = $this->session->userdata('id_pegawai');
		if($id_pegawai == ''){
			redirect(base_url().'dashboard');
		}
		
		$cek_useraccess = $this->useraccess_pusat->cek_access($id_pegawai, '37');
		if($cek_useraccess){
			if($cek_useraccess->act_read == '0' or $cek_useraccess->act_read == '-'){
				redirect(base_url().'dashboard');
			}					
		}else{
			redirect(base_url().'dashboard');
		}

		$data_pegawai = $this->pegawai_pusat->get_by_id($id_pegawai);
		if($data_pegawai){
			$data['data_pegawai']    	= $data_pegawai;
			$data['data_induk_menu'] 	= $this->useraccess_pusat->get_induk_menu($id_pegawai);
			$data['access_create'] 		= $cek_useraccess->act_create;

			$data['atribut_halaman'] 	= 'Laporan Retur Pembelian';
			$data['halaman_laporan'] 	= $this->load->view('admin/laporan/retur_pembelian/laporan',$data,true);
			$data['halaman_plugin'] 	= $this->load->view('admin/laporan/retur_pembelian/plugin',$data,true);
			$data['isi_halaman'] 		= $this->load->view('admin/laporan/retur_pembelian/index',$data,true);
			$this->load->view('admin/layout',$data);
		}else{
			redirect(base_url().'login');
		}
	}

	public function ajax_list()
	{
		// Cek user acces menu
		$id_pegawai 					= $this->session->userdata('id_pegawai');
		$cek_useraccess_transaksi 		= $this->useraccess_pusat->cek_access($id_pegawai, '26');
		$cek_useraccess_laporan 		= $this->useraccess_pusat->cek_access($id_pegawai, '37');
		if($cek_useraccess_laporan->act_read == '0' or $cek_useraccess_laporan->act_read == '-'){
			redirect(base_url().'dashboard');
		}

		$tanggal_filter 				= $this->input->post('tanggal_filter');
		if($tanggal_filter == ''){
			redirect(base_url().'laporan_retur_pembelian');
			exit();
		}

		$tanggal_awal 					= substr($tanggal_filter, 0, 10);
		$tanggal_akhir 					= substr($tanggal_filter, 18, 27);
		$data_laporan_retur_pembelian 	= $this->retur_pembelian_master->get_laporan('_get_laporan_query', $tanggal_awal, $tanggal_akhir);
			
		$data 							= array();
		$no 							= $_POST['start'];

		foreach ($data_laporan_retur_pembelian as $dlp){
			$no++;
			$row 	= array();
			$row[] 	= $no;
			$status = $dlp->status_retur; 

			//Awal tombol
			if($cek_useraccess_laporan->act_update == 1){
				$tombol_edit 	 = '<a class="btn btn-default btn-rounded btn-xs" 
										onclick="edit_transaksi('."'".$dlp->id_retur_pembelian_m."'".')">
										<i class="fa fa-pencil" style="color:blue;"></i>
									</a>';
			}else{
				if($cek_useraccess_transaksi->act_update == 1 AND $status=="MENUNGGU" 
					OR $cek_useraccess_transaksi->act_create == 1 AND $status=="MENUNGGU"){
					$tombol_edit = '<a class="btn btn-default btn-rounded btn-xs" 
										onclick="edit_transaksi('."'".$dlp->id_retur_pembelian_m."'".')">
										<i class="fa fa-pencil" style="color:blue;"></i>
									</a>';
				}else{
					$tombol_edit = '';
				}
			}

			if($cek_useraccess_laporan->act_delete == 1){
				$tombol_hapus = '<a class="btn btn-default btn-rounded btn-xs" 
				  					onclick="verifikasi_hapus_transaksi('."'".$dlp->id_retur_pembelian_m."'".')">
				  					<i class="fa fa-times" style="color:red;"></i>
				  				</a>';
			}else{
				$tombol_hapus = '';
				if($cek_useraccess_transaksi->act_delete == 1 AND $status=="MENUNGGU"){
					$tombol_hapus = '<a class="btn btn-default btn-rounded btn-xs" 
					  					onclick="verifikasi_hapus_transaksi('."'".$dlp->id_retur_pembelian_m."'".')">
					  					<i class="fa fa-times" style="color:red;"></i>
					  				</a>';
				}else{
					$tombol_hapus = '';
				}
			}

			$row[] =	'
						<a class="btn btn-default btn-rounded btn-xs" onclick="cetak_faktur('."'".$dlp->id_retur_pembelian_m."'".')">
							<i class="fa fa-print"></i>
						</a>
						'.$tombol_edit.'
						'.$tombol_hapus.'
				  		';
			//Akhir tombol

			$row[] 	= $dlp->no_retur_pembelian;
			$row[] 	= $dlp->no_pembelian;
			if($status=="SELESAI"){
				$row[] 	= '<span class="btn btn-success btn-xs btn-block btn-rounded text-center">'.$status.'</span>';
			}elseif($status=="MENUNGGU"){
				$row[] 	= '<span class="btn btn-danger btn-xs btn-block btn-rounded text-center">'.$status.'</span>';
			}
			$row[] 	= date('d-m-Y', strtotime($dlp->tanggal_retur));
			$row[] 	= $dlp->nama_supplier;
			$row[] 	= '<span class="pull-right">'.number_format($dlp->jml_barang,'0', ',', '.').'</span>';
			
			if($this->session->userdata('usergroup_name') == 'Super Admin'){
				$row[] 	= '<span class="pull-right">'.number_format($dlp->total,'0', ',', '.').'</span>';
				$row[] 	= '<span class="pull-right">'.number_format($dlp->biaya_lain,'0', ',', '.').'</span>';
				$row[] 	= '<span class="pull-right">'.number_format($dlp->ppn,'0', ',', '.').'</span>';
				$row[] 	= '<span class="pull-right">'.number_format($dlp->grand_total,'0', ',', '.').'</span>';
			}

			$row[] 	= $dlp->keterangan_lain;
			$row[] 	= $dlp->pegawai_save;
			$row[] 	= $dlp->tanggal_pembuatan;
			$row[] 	= $dlp->pegawai_edit;
			$row[] 	= $dlp->tanggal_pembaharuan;
			$data[]	= $row;
		}

		$output = array(	
			"draw"            => $_POST['draw'],
			"recordsTotal"    => $this->retur_pembelian_master->count_all('vamr4846_vama.retur_pembelian_master', $tanggal_awal, $tanggal_akhir),
			"recordsFiltered" => $this->retur_pembelian_master->count_filtered('_get_laporan_query', $tanggal_awal, $tanggal_akhir),
			"data"            => $data
						);		
		echo json_encode($output);
	}

	public function ajax_list_detail()
	{
		// Cek user acces menu
		$id_pegawai               = $this->session->userdata('id_pegawai');
		$cek_useraccess_transaksi = $this->useraccess_pusat->cek_access($id_pegawai, '26');
		$cek_useraccess_laporan   = $this->useraccess_pusat->cek_access($id_pegawai, '37');
		if($cek_useraccess_laporan->act_read == '0' or $cek_useraccess_laporan->act_read == '-'){
			redirect(base_url().'dashboard');
		}

		$tanggal_filter 				= $this->input->post('tanggal_filter');
		if($tanggal_filter == ''){
			redirect(base_url().'laporan_retur_pembelian');
			exit();
		}

		$tanggal_awal                 = substr($tanggal_filter, 0, 10);
		$tanggal_akhir                = substr($tanggal_filter, 18, 27);
		$data_laporan_retur_pembelian = $this->retur_pembelian_master->get_laporan('_get_laporan_query_detail', $tanggal_awal, $tanggal_akhir);
			
		$data               = array();
		$no                 = $_POST['start'];
		$no_retur_pembelian = '0';

		foreach ($data_laporan_retur_pembelian as $dlp) 
		{
			$no++;
			$row       = array();
			$row[]     = $no;
			$status    = $dlp->status_retur; 
			$dari_stok = $dlp->dari_stok; 

			//Awal tombol
			if($cek_useraccess_laporan->act_update == 1){
				$tombol_edit 	 = '<a class="btn btn-default btn-rounded btn-xs" 
										onclick="edit_transaksi('."'".$dlp->id_retur_pembelian_m."'".')">
										<i class="fa fa-pencil" style="color:blue;"></i>
									</a>';
			}else{
				if($cek_useraccess_transaksi->act_update == 1 AND $status=="MENUNGGU" 
					OR $cek_useraccess_transaksi->act_create == 1 AND $status=="MENUNGGU"){
					$tombol_edit = '<a class="btn btn-default btn-rounded btn-xs" 
										onclick="edit_transaksi('."'".$dlp->id_retur_pembelian_m."'".')">
										<i class="fa fa-pencil" style="color:blue;"></i>
									</a>';
				}else{
					$tombol_edit = '';
				}
			}

			if($cek_useraccess_laporan->act_delete == 1){
				$tombol_hapus = '<a class="btn btn-default btn-rounded btn-xs" 
				  					onclick="verifikasi_hapus_transaksi('."'".$dlp->id_retur_pembelian_m."'".')">
				  					<i class="fa fa-times" style="color:red;"></i>
				  				</a>';
			}else{
				$tombol_hapus = '';
				if($cek_useraccess_transaksi->act_delete == 1 AND $status=="MENUNGGU"){
					$tombol_hapus = '<a class="btn btn-default btn-rounded btn-xs" 
					  					onclick="verifikasi_hapus_transaksi('."'".$dlp->id_retur_pembelian_m."'".')">
					  					<i class="fa fa-times" style="color:red;"></i>
					  				</a>';
				}else{
					$tombol_hapus = '';
				}
			}

			$row[] =	'
						<a class="btn btn-default btn-rounded btn-xs" onclick="cetak_faktur('."'".$dlp->id_retur_pembelian_m."'".')">
							<i class="fa fa-print"></i>
						</a>
						'.$tombol_edit.'
						'.$tombol_hapus.'
				  		';
			//Akhir tombol

			if($no_retur_pembelian <> $dlp->no_retur_pembelian){
				$row[] 	= '<span class="btn btn-primary btn-block btn-rounded btn-xs">'.$dlp->no_retur_pembelian.'</span>';
			}else{
				$row[] 	= '<span class="btn btn-default btn-block btn-rounded btn-xs">'.$dlp->no_retur_pembelian.'</span>';
			}

			if($status=="SELESAI"){
				$row[] 	= '<span class="btn btn-success btn-xs btn-block btn-rounded text-center">'.$status.'</span>';
			}elseif($status=="MENUNGGU"){
				$row[] 	= '<span class="btn btn-danger btn-xs btn-block btn-rounded text-center">'.$status.'</span>';
			}
			$row[] 	= date('d-m-Y', strtotime($dlp->tanggal_retur));
			$row[] 	= $dlp->nama_supplier;
			$row[] 	= $dlp->sku;
			$row[] 	= $dlp->nama_barang;
			
			if($dari_stok =="JUAL"){
				$row[] 	= '<span class="btn btn-success btn-xs btn-block btn-rounded text-center">'.$dari_stok.'</span>';
			}elseif($dari_stok=="RUSAK"){
				$row[] 	= '<span class="btn btn-danger btn-xs btn-block btn-rounded text-center">'.$dari_stok.'</span>';
			}

			$row[] 	= '<span class="pull-right">'.$dlp->jumlah_retur.'</span>';
			if($this->session->userdata('usergroup_name') == 'Super Admin'){
				$row[] 	= '<span class="pull-right">'.number_format($dlp->harga_satuan,'0', ',', '.').'</span>';
				$row[] 	= '<span class="pull-right">'.number_format($dlp->subtotal,'0', ',', '.').'</span>';
			}

			$row[] 	= $dlp->keterangan;
			$row[] 	= $dlp->pegawai_save;
			$row[] 	= $dlp->tanggal_pembuatan;
			$row[] 	= $dlp->pegawai_edit;
			$row[] 	= $dlp->tanggal_pembaharuan;
			$data[]	= $row;
			$no_retur_pembelian = $dlp->no_retur_pembelian;
		}

		$output = array(
			"draw"            => $_POST['draw'],
			"recordsTotal"    => $this->retur_pembelian_master->count_all('vamr4846_vama.retur_pembelian_detail', $tanggal_awal, $tanggal_akhir),
			"recordsFiltered" => $this->retur_pembelian_master->count_filtered('_get_laporan_query_detail', $tanggal_awal, $tanggal_akhir),
			"data"            => $data
		);
		echo json_encode($output);
	}

	public function ajax_list_batal()
	{
		// Cek user acces menu
		$id_pegawai               = $this->session->userdata('id_pegawai');
		$cek_useraccess_transaksi = $this->useraccess_pusat->cek_access($id_pegawai, '26');
		$cek_useraccess_laporan   = $this->useraccess_pusat->cek_access($id_pegawai, '37');
		if($cek_useraccess_laporan->act_read == '0' or $cek_useraccess_laporan->act_read == '-'){
			redirect(base_url().'dashboard');
		}

		$tanggal_filter = $this->input->post('tanggal_filter');
		if($tanggal_filter == ''){
			redirect(base_url().'laporan_retur_pembelian');
			exit();
		}

		$tanggal_awal                 = substr($tanggal_filter, 0, 10);
		$tanggal_akhir                = substr($tanggal_filter, 18, 27);
		$data_laporan_retur_pembelian = $this->retur_pembelian_master->get_laporan('_get_laporan_query_batal', $tanggal_awal, $tanggal_akhir);
			
		$data = array();
		$no   = $_POST['start'];

		foreach ($data_laporan_retur_pembelian as $dlp) 
		{
			$no++;
			$row       = array();
			$row[]     = $no;
			$dari_stok = $dlp->dari_stok; 

			$row[] 	= $dlp->no_retur_pembelian;
			$row[] 	= $dlp->no_pembelian;
			$row[] 	= date('d-m-Y', strtotime($dlp->tanggal_retur));
			$row[] 	= $dlp->nama_supplier;
			$row[] 	= $dlp->sku;
			$row[] 	= $dlp->nama_barang;
			if($dari_stok =="JUAL"){
				$row[] 	= '<span class="btn btn-success btn-xs btn-block btn-rounded text-center">'.$dari_stok.'</span>';
			}elseif($dari_stok=="RUSAK"){
				$row[] 	= '<span class="btn btn-danger btn-xs btn-block btn-rounded text-center">'.$dari_stok.'</span>';
			}
			$row[] 	= $dlp->jumlah_retur;
			
			if($this->session->userdata('usergroup_name') == 'Super Admin'){
				$row[] 	= '<span class="pull-right">'.number_format($dlp->harga_satuan,'0', ',', '.').'</span>';
				$row[] 	= '<span class="pull-right">'.number_format($dlp->subtotal,'0', ',', '.').'</span>';
			}

			$row[] 	= $dlp->keterangan;
			$row[] 	= $dlp->keterangan_batal;
			$row[] 	= $dlp->pegawai_pembatalan;
			$row[] 	= $dlp->tanggal_pembatalan;
			$row[] 	= $dlp->pegawai_save;
			$row[] 	= $dlp->tanggal_pembuatan;
			$row[] 	= $dlp->pegawai_edit;
			$row[] 	= $dlp->tanggal_pembaharuan;
			$data[]	= $row;
		}

		$output = array(
			"draw"            => $_POST['draw'],
			"recordsTotal"    => $this->retur_pembelian_master->count_all('vamr4846_vama.retur_pembelian_detail_batal', $tanggal_awal, $tanggal_akhir),
			"recordsFiltered" => $this->retur_pembelian_master->count_filtered('_get_laporan_query_batal', $tanggal_awal, $tanggal_akhir),
			"data"            => $data
		);
		
		//output to json format
		echo json_encode($output);
	}

	public function ambil_total()
	{
		// Cek user acces menu
		$id_pegawai     = $this->session->userdata('id_pegawai');
		$cek_useraccess = $this->useraccess_pusat->cek_access($id_pegawai, '37');
		if($cek_useraccess->act_read == '0' or $cek_useraccess->act_read == '-'){
			redirect(base_url().'dashboard');
		}

		if($this->session->userdata('usergroup_name') !== 'Super Admin'){
			redirect(base_url().'laporan_retur_pembelian');
			exit();
		}

		$tanggal_filter 			= $this->input->post('tanggal_filter');
		if($tanggal_filter == ''){
			redirect(base_url().'laporan_retur_pembelian');
			exit();
		}

		$tanggal_awal  = substr($tanggal_filter, 0, 10);
		$tanggal_akhir = substr($tanggal_filter, 18, 27);

		// Ambil jumlah transaksi, total tunai, debit dan total uang masuk
		$data_uang_masuk = $this->retur_pembelian_master->get_grand_total($tanggal_awal, $tanggal_akhir);
		if ($data_uang_masuk)
		{
			$jml_transaksi               = number_format($data_uang_masuk->jml_transaksi,'0', ',', '.');
			$grand_total_retur_pembelian = '<span>Rp. <p class="pull-right">'.number_format($data_uang_masuk->grand_total_retur_pembelian,'0', ',', '.').'</p></span>';
		}
		else
		{
			$jml_transaksi               = '0';
			$grand_total_retur_pembelian = '0';
		}
		
		echo json_encode(array(
			'status'                      => 1,
			"jml_transaksi"               => $jml_transaksi,
			"grand_total_retur_pembelian" => $grand_total_retur_pembelian
		));
	}

	public function ajax_verifikasi_hapus_transaksi()
	{
		$id_retur_pembelian_m 	= $this->input->post('id_retur_pembelian_m');
		if($id_retur_pembelian_m == ''){
			redirect(base_url().'laporan_retur_pembelian');
			exit();
		}

		// Cek user acces menu
		$id_pegawai     = $this->session->userdata('id_pegawai');
		$cek_useraccess = $this->useraccess_pusat->cek_access($id_pegawai, '37');
		$data           = $this->retur_pembelian_master->get_master2($id_retur_pembelian_m);

		if($data){
			if($data->status_retur == 'SELESAI'){
				if($cek_useraccess->act_delete == '0' or $cek_useraccess->act_delete == '-'){
					echo json_encode(array(
						"status" 		=> 0,
					  	"info_pesan" 	=> "Maaf anda tidak diberikan izin untuk menghapus transaksi retur pembelian,
											dengan status retur : SELESAI !",
						"pesan" 		=> "
												No. Retur Pembelian : ".$data->no_retur_pembelian."
												No. Pembelian : ".$data->no_pembelian."
												Status Retur : ".$data->status_retur."
											",
						"tipe_pesan" 	=> "error"
					));
					exit();
				}
			}
		}
		
		if($data){
			echo json_encode(array(
			'status' 	=> 1,
			'pesan' 	=>	"<table class='table' id='detail_hapus_retur_pembelian'>
								<thead>
								</thead>
								<tbody>
									<tr>
										<td>
											<small>No. Retur Pembelian : </small>
											<b class='text-dark'>$data->no_retur_pembelian </b><br/>
											
											<small>No. Pembelian : </small>
											<b class='text-dark'>$data->no_pembelian </b><br/><br/>

											<small>Nama Supplier : </small> <br/>	
											<b class='text-dark'>$data->nama_supplier </b> </br><br/>

											<small>Jumlah Barang : </small> <br/>	
											<h2 class=text-primary>$data->jumlah_barang </h2>

											<textarea name='keterangan_batal' id='keterangan_batal' class='form-control input-md' placeholder='Keterangan batal' style='resize: vertical;'></textarea>
										</td>
									</tr>
								</tbody>
							</table>",

			'footer'	=> 	"<button onclick='hapus_transaksi($data->id_retur_pembelian_m)' type='button' class='btn btn-primary 
							 waves-effect waves-light' data-dismiss='modal' autofocus>Iya, Hapus</button> 
							<button type='button' class='btn btn-default waves-effect' data-dismiss='modal'>Batal</button>"
			));
		}else{
			redirect(base_url().'laporan_retur_pembelian');
		}
	}

	public function ajax_hapus_transaksi()
	{
		if($this->input->is_ajax_request()){
			$id_retur_pembelian_m 	= $this->input->post('id_retur_pembelian_m');
			if($id_retur_pembelian_m == ''){
				redirect(base_url().'laporan_retur_pembelian');
				exit();
			}

			// Cek user acces menu
			$id_pegawai         = $this->session->userdata('id_pegawai');
			$cek_useraccess     = $this->useraccess_pusat->cek_access($id_pegawai, '37');
			$master             = $this->retur_pembelian_master->get_master2($id_retur_pembelian_m);
			$no_retur_pembelian = $master->no_retur_pembelian;
			$no_pembelian       = $master->no_pembelian;
			$id_supplier        = $master->id_supplier;

			if($master){
				if($master->status_retur == 'SELESAI'){
					if($cek_useraccess->act_delete == '0' or $cek_useraccess->act_delete == '-'){
						echo json_encode(array(
							"status" 		=> 0,
						  	"info_pesan" 	=> "Maaf anda tidak diberikan izin untuk menghapus transaksi retur pembelian,
												dengan status retur : SELESAI !",
							"pesan" 		=> "
													No. Retur Pembelian : ".$master->no_retur_pembelian."
													No. Pembelian : ".$master->no_pembelian."
													Status Retur : ".$master->status_retur."
												",
							"tipe_pesan" 	=> "error",
							"gaya_tombol"	=> "btn-danger btn-md waves-effect waves-light"
						));
						exit();
					}
				}
			}
			
			$id_pegawai_pembatalan = $this->session->userdata('id_pegawai');
			$keterangan_batal      = $this->input->post('keterangan_batal');
			$hapus                 = $this->retur_pembelian_master->hapus_transaksi($id_retur_pembelian_m, $id_supplier, 
																					 $no_retur_pembelian, $no_pembelian, 
																					 $keterangan_batal, $id_pegawai_pembatalan);
			
			if($hapus){
				echo json_encode(array(
					"status" 		=> 1,
					"info_pesan" 	=> "Berhasil!",
					"pesan"			=> "Transaksi retur pembelian berhasil dihapus,
									    Dengan no. pembelian ".$no_retur_pembelian."",
					"tipe_pesan" 	=> 'success',
					"gaya_tombol"	=> "btn-success btn-md waves-effect waves-light"
				));
			}else{
				echo json_encode(array(
					"status"		=> 0,
					"info_pesan" 	=> "Gagal!",
					"pesan" 		=> "Terjadi kesalahan, coba lagi !",
					"tipe_pesan" 	=> "error",
					"gaya_tombol"	=> "btn-danger btn-md waves-effect waves-light"
				));
			}
		}else{
			redirect(base_url().'laporan_retur_pembelian');
		}
	}
}
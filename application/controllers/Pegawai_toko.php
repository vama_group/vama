<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pegawai_toko extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('usergroup_toko_model','usergroup_toko');
		$this->load->model('pegawai_pusat_model','pegawai_pusat');
		$this->load->model('useraccess_pusat_model','useraccess_pusat');
		$this->load->model('useraccess_toko_model','useraccess_toko');

		$this->load->model('pegawai_toko_model','pegawai_toko');
		$this->load->model('customer_pusat_model','customer_pusat');
	}

	public function index()
	{
		// Cek user acces menu
		$id_pegawai = $this->session->userdata('id_pegawai');
		if($id_pegawai == ''){
			redirect(base_url().'login');
		}

		$cek_useraccess = $this->useraccess_pusat->cek_access($id_pegawai, '5');
		if($cek_useraccess){
			if($cek_useraccess->act_read == '0' or $cek_useraccess->act_read == '-'){
				redirect(base_url().'dashboard');
			}					
		}else{
			redirect(base_url().'dashboard');
		}

		// cari data pegawai
		$id_pegawai              = $this->session->userdata('id_pegawai');
		$data_pegawai            = $this->pegawai_pusat->get_by_id($id_pegawai);
		$data['access_create']   = $cek_useraccess->act_create;
		$data['access_update']   = $cek_useraccess->act_update;
		$data['access_delete']   = $cek_useraccess->act_delete;
		$data['data_pegawai']    = $data_pegawai;
		$data['data_induk_menu'] = $this->useraccess_pusat->get_induk_menu($id_pegawai);
		
		$data['atribut_halaman'] = 'Pegawai Toko';
		$data['list_usergroup']  = $this->usergroup_toko->listing();
		$data['list_toko']       = $this->customer_pusat->listing_toko();
		$data['halaman_list']    = $this->load->view('admin/master_data/pegawai_toko/list',$data,true);
		$data['halaman_form']    = $this->load->view('admin/master_data/pegawai_toko/form',$data,true);
		$data['halaman_plugin']  = $this->load->view('admin/master_data/pegawai_toko/plugin',$data,true);
		$data['isi_halaman']     = $this->load->view('admin/master_data/pegawai_toko/index',$data,true);
		$this->load->view('admin/layout',$data);
	}

	public function ajax_list()
	{
		// Cek user acces menu
		$id_pegawai 				= $this->session->userdata('id_pegawai');
		$cek_useraccess 			= $this->useraccess_pusat->cek_access($id_pegawai, '5');
		if($cek_useraccess->act_read == '0' or $cek_useraccess->act_read == '-'){
			redirect(base_url().'dashboard');
		}

		$id_periode_stok_pusat = $this->input->post('id_periode_stok_pusat');
		if($id_periode_stok_pusat == ''){
			redirect(base_url().'pegawai_toko');
			exit();
		}

		$list = $this->pegawai_toko->get_datatables();
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $pegawai_toko) {
			$no++;
			$row 	= array();

			$row[] 	= $no;

			//add html for action
			if($cek_useraccess->act_update == 1){
				$tombol_edit 	 = '<a class="btn btn-rounded btn-xs btn-default" href="javascript:void(0)" title="Edit" 
										onclick="edit_pegawai('."'".$pegawai_toko->id_pegawai_toko."'".')">
										<i class="fa fa-pencil" style="color:blue;"></i>
									</a>';
			}else{$tombol_edit 	 = '';}

			if($cek_useraccess->act_delete == 1){
				$tombol_hapus 	 = '<a class="btn btn-rounded btn-xs btn-default" href="javascript:void(0)" title="Hapus" 
										onclick="verifikasi_delete('."'".$pegawai_toko->id_pegawai_toko."'".')">
										<i class="fa fa-remove" style="color:red;"></i>
				  					</a>';
			}else{$tombol_hapus  = '';}

			if($cek_useraccess->act_update == 0 AND $cek_useraccess->act_delete == 0){
				$keterangan_tombol = '<b class="btn btn-rounded btn-xs btn-default text-muted text-center">Tidak ada akses</b>';
			}else{
				$keterangan_tombol = '';
			}
			
			$row[] 	= '	
						'.$keterangan_tombol.'
						'.$tombol_edit.'
						'.$tombol_hapus.'
				  	';

			$row[]	= $pegawai_toko->kode_pegawai_toko;
			$row[] 	= $pegawai_toko->nama_pegawai_toko;
			$row[]	= $pegawai_toko->email;
			$row[] 	= $pegawai_toko->handphone;
			$row[] 	= $pegawai_toko->usergroup_name;
			$row[] 	= $pegawai_toko->nama_toko;
			$row[] 	= $pegawai_toko->status_blokir;
			
			$row[] 	= $pegawai_toko->pegawai_save;
			$row[] 	= $pegawai_toko->tanggal_pembuatan;
			$row[] 	= $pegawai_toko->pegawai_edit;
			$row[] 	= $pegawai_toko->tanggal_pembaharuan;
			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->pegawai_toko->count_all(),
						"recordsFiltered" => $this->pegawai_toko->count_filtered(),
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}

	public function ajax_list_otoritas()
	{
		// Cek user acces menu
		$id_pegawai     = $this->session->userdata('id_pegawai');
		$cek_useraccess = $this->useraccess_pusat->cek_access($id_pegawai, '5');
		if($cek_useraccess->act_update == '0' or $cek_useraccess->act_update == '-'){
			redirect(base_url().'dashboard');
		}

		if($id_pegawai == ''){
			redirect(base_url().'pegawai_toko');
			exit();
		}

		$id_pegawai_otoritas = $this->input->post('id_pegawai_otoritas');
		$list                = $this->useraccess_toko->get_datatables($id_pegawai_otoritas);
		$data                = array();
		$no                  = $_POST['start'];
		foreach ($list as $useraccess) {			
			$row = array();

			if($useraccess->id_menu_induk == 0){
				$row[] = '';
				$row[] = '<h4>'.$useraccess->nama_menu.'</h4>';
				$row[] = '';
				$row[] = '';
				$row[] = '';
				$row[] = '';
			}else{
				$no++;
				$row[] = $no;
				$row[] = $useraccess->nama_lengkap_menu;

				//add html for action
				if($useraccess->act_read == '1'){$status = 'checked';}
				else{$status = '';}
				$row[] = '<input type="checkbox" id="read_'.$useraccess->id_menu.'" switch="info" value='.$useraccess->act_read.'
						  onclick="update_useraccess('.$useraccess->id_menu.', '.$useraccess->id_pegawai.', 1, '.$useraccess->act_read.')" '.$status.' />
		                  <label for="read_'.$useraccess->id_menu.'" data-on-label="Yes" data-off-label="No"></label>';
				
		        if($useraccess->act_create == '1'){$status = 'checked';
				}else{$status = '';}
				$row[] = '<input type="checkbox" id="create_'.$useraccess->id_menu.'" switch="success" value='.$useraccess->act_create.'
						  onclick="update_useraccess('.$useraccess->id_menu.', '.$useraccess->id_pegawai.', 2, '.$useraccess->act_create.')" '.$status.' />
		                  <label for="create_'.$useraccess->id_menu.'" data-on-label="Yes" data-off-label="No"></label>';

				if($useraccess->act_update == '1'){$status = 'checked';
				}else{$status = '';}
				$row[] = '<input type="checkbox" id="update_'.$useraccess->id_menu.'" switch="primary" value='.$useraccess->act_update.'
						  onclick="update_useraccess('.$useraccess->id_menu.', '.$useraccess->id_pegawai.', 3, '.$useraccess->act_update.')" '.$status.' />
		                  <label for="update_'.$useraccess->id_menu.'" data-on-label="Yes" data-off-label="No"></label>';

		        if($useraccess->act_delete == '1'){$status = 'checked';
				}else{$status = '';}
				$row[] = '<input type="checkbox" id="delete_'.$useraccess->id_menu.'" switch="warning" value='.$useraccess->act_delete.'
						  onclick="update_useraccess('.$useraccess->id_menu.', '.$useraccess->id_pegawai.', 4, '.$useraccess->act_delete.')" '.$status.' />
		                  <label for="delete_'.$useraccess->id_menu.'" data-on-label="Yes" data-off-label="No"></label>';				
			}
		
			$data[] = $row;
		}

		$output = array(
			"draw"            => $_POST['draw'],
			"recordsTotal"    => $this->useraccess_toko->count_all($id_pegawai),
			"recordsFiltered" => $this->useraccess_toko->count_filtered($id_pegawai),
			"data"            => $data,
			"id_pegawai"      => $id_pegawai
		);
		echo json_encode($output);
	}

	public function ajax_edit()
	{
		// Cek user acces menu
		$id_pegawai 	= $this->session->userdata('id_pegawai');
		$cek_useraccess = $this->useraccess_pusat->cek_access($id_pegawai, '5');
		if($cek_useraccess->act_update == '0' or $cek_useraccess->act_update == '-'){
			redirect(base_url().'pegawai_toko');
			exit();
		}

		$id_pegawai_toko = $this->input->post('id_pegawai_toko');
		if($id_pegawai_toko == ''){
			redirect(base_url().'pegawai_toko');
			exit();
		}else{
			$data = $this->pegawai_toko->get_by_id($id_pegawai_toko);
			echo json_encode($data);
		}
	}

	public function ajax_add()
	{
		// Cek user acces menu
		$id_pegawai 	= $this->session->userdata('id_pegawai');
		$cek_useraccess = $this->useraccess_pusat->cek_access($id_pegawai, '5');
		if($cek_useraccess->act_create == '0' or $cek_useraccess->act_create == '-'){
			redirect(base_url().'pegawai_toko');
			exit();
		}

		$tanggal_pembuatan = date('Y-m-d H:i:s');
		$kode_toko         = $this->input->post('kode_toko');
		$akhir             = $this->pegawai_toko->akhir($kode_toko);
		$id_pegawai_toko   = $akhir->id_pegawai_toko_baru;
		
		if($id_pegawai_toko < 10){
			$depan = '00';
		}elseif($id_pegawai_toko < 100) {
			$depan = '0';
		}else{
			$depan = '';
		}

		$kode_pegawai_toko		= $kode_toko.$depan.$id_pegawai_toko;
		// Akhir buat kode baru

		$this->_validate('save', '0');
		$data = array(
			'kode_pegawai_toko'    => $kode_pegawai_toko,
			'nama_pegawai_toko'    => $this->input->post('nama_pegawai_toko'),
			'alamat_pegawai_toko'  => $this->input->post('alamat_pegawai_toko'),
			'email'                => $this->input->post('email'),
			'password'             => sha1($this->input->post('password')),
			'handphone'            => $this->input->post('handphone'),
			'foto'                 => 'avatar.jpg',
			'id_usergroup'         => $this->input->post('usergroup'),
			'kode_toko'            => $this->input->post('kode_toko'),
			'status_blokir'        => $this->input->post('status_blokir'),
			'id_pegawai_pembuatan' => $this->session->userdata('id_pegawai'),
			'tanggal_pembuatan'    => date('Y-m-d H:i:s')
		);
		$insert = $this->pegawai_toko->save($data);

		// Ambil id pegawai baru untuk dibuatkan default user access
		$data_pegawai = $this->pegawai_toko->get_by_email($this->input->post('email'));
		if($data_pegawai){
			$id_pegawai_baru  = $data_pegawai->id_pegawai_toko;
			$this->pegawai_toko->create_useraccess_toko($id_pegawai_baru, $id_pegawai, $tanggal_pembuatan);
		}
		echo json_encode(array("status" => TRUE));
	}

	public function ajax_update()
	{
		// Cek user acces menu
		$id_pegawai 		= $this->session->userdata('id_pegawai');
		$cek_useraccess 	= $this->useraccess_pusat->cek_access($id_pegawai, '5');
		if($cek_useraccess->act_update == '0' or $cek_useraccess->act_update == '-'){
			redirect(base_url().'pegawai_toko');
			exit();
		}

		$id_pegawai_save 	= $this->input->post('id_pegawai_toko');
		if($id_pegawai_save == ''){
			redirect(base_url().'pegawai_toko');
			exit();
		}

		$this->_validate('update', $id_pegawai_save);
		$data = array(
				'nama_pegawai_toko'			=> $this->input->post('nama_pegawai_toko'),
				'alamat_pegawai_toko'		=> $this->input->post('alamat_pegawai_toko'),
				'email' 					=> $this->input->post('email'),
				'handphone' 				=> $this->input->post('handphone'),
				// 'password' 					=> sha1($this->input->post('password')),
				'id_usergroup' 				=> $this->input->post('usergroup'),
				'kode_toko'					=> $this->input->post('kode_toko'),
				'status_blokir'				=> $this->input->post('status_blokir'),
				'id_pegawai_pembaharuan'	=> $this->session->userdata('id_pegawai')
			);
		$this->pegawai_toko->update(array('id_pegawai_toko' => $this->input->post('id_pegawai_toko')), $data);
		echo json_encode(array("status" => TRUE));
	}

	public function ajax_verifikasi_delete()
	{
		// Cek user acces menu
		$id_pegawai 	= $this->session->userdata('id_pegawai');
		$cek_useraccess = $this->useraccess_pusat->cek_access($id_pegawai, '5');
		if($cek_useraccess->act_delete == '0' or $cek_useraccess->act_delete == '-'){
			redirect(base_url().'pegawai_toko');
			exit();
		}

		$id_pegawai_toko = $this->input->post('id_pegawai_toko');
		if($id_pegawai_toko == ''){
			redirect(base_url().'pegawai_toko');
			exit();
		}
		
		$data = $this->pegawai_toko->get_by_id($id_pegawai_toko);		
		echo json_encode(array(
								'pesan' 	=> "<b class='text-danger'>Yakin ingin menghapus pegawai toko ini ? </b></br></br>
												Nama Pegawai : </br>
												<h2 class='text-dark'>".$data->nama_pegawai_toko."</h2></br>
												
												Pegawai Toko : </br>
												<b class='text-dark'>".$data->nama_toko."</b></br></br>

												Email : </br>
												<b class='text-dark'>".$data->email."</b></br></br>

												Handphone : </br>
												<b class='text-dark'>".$data->handphone."</b></br></br>

												Usergroup : </br>
												<b class='text-dark'>".$data->usergroup_name."</b></br></br>",
								'footer'	=> 	"<button onclick='delete_pegawai_toko($id_pegawai_toko)' 
													type='button' class='btn btn-primary waves-effect waves-light' 
													data-dismiss='modal' autofocus>Iya, Hapus</button> 
												<button type='button' class='btn btn-default waves-effect' 
													data-dismiss='modal'>Batal</button>"
							  	)
						);
	}

	public function ajax_delete()
	{
		// Cek user acces menu
		$id_pegawai 	= $this->session->userdata('id_pegawai');
		$cek_useraccess = $this->useraccess_pusat->cek_access($id_pegawai, '5');
		if($cek_useraccess->act_delete == '0' or $cek_useraccess->act_delete == '-'){
			redirect(base_url().'pegawai_toko');
			exit();
		}

		$id_pegawai_save = $this->input->post('id_pegawai_toko');
		if($id_pegawai_save == ''){
			redirect(base_url().'pegawai_toko');
			exit();
		}

		$this->pegawai_toko->update_status_hapus($id_pegawai_save, $id_pegawai);
		echo json_encode(array("status" => TRUE));
	}

	public function ajax_update_password()
	{
		// Cek user acces menu
		$id_pegawai     = $this->session->userdata('id_pegawai');
		$cek_useraccess = $this->useraccess_pusat->cek_access($id_pegawai, '5');
		if($cek_useraccess->act_update == '0' or $cek_useraccess->act_update == '-'){
			redirect(base_url().'pegawai_toko');
			exit();
		}

		$this->_validate_password();

		$i             = $this->input;
		$id_pegawai    = $i->post('id_pegawai_password');
		$password_baru = sha1($i->post('password_baru'));

		$data = array(
			'password'               => $password_baru,
			'id_pegawai_pembaharuan' => $this->session->userdata('id_pegawai')
		);

		$this->pegawai_toko->update(
			array('id_pegawai_toko' => $id_pegawai), 
			$data
		);
		echo json_encode(array("status" => TRUE));
	}

	public function ajax_update_usseraccess()
	{
		$field = $this->input->post('field');
		if($field == 1){
			$nama_field = 'act_read';
		}else if($field == 2){
			$nama_field = 'act_create';
		}else if($field == 3){
			$nama_field = 'act_update';
		}else if($field == 4){
			$nama_field = 'act_delete';
		}

		$status_sekarang = $this->input->post('status');
		$id_menu         = $this->input->post('id_menu');
		$id_pegawai_save = $this->input->post('id_pegawai');

		$data = array(
			$nama_field              => $status_sekarang,
			'id_pegawai_pembaharuan' => $this->session->userdata('id_pegawai')
		);

		$this->useraccess_toko->update(array('id_pegawai' 	=> $id_pegawai_save,
											  'id_menu' 	=> $id_menu),
										$data);

		// Update menu induk 1
		$jumlah_access1 		= '0';
		$cek_menu_induk 		= $this->useraccess_toko->cek_menu_induk($this->input->post('id_menu'));
		if($cek_menu_induk){
			$id_menu_induk_1  	= $cek_menu_induk->id_menu_induk;
			if($status_sekarang == '1'){
				$data = array($nama_field 				=> '1',
							  'id_pegawai_pembaharuan'	=> $this->session->userdata('id_pegawai')
							 );

				$this->useraccess_toko->update(array('id_pegawai' 	=> $id_pegawai_save,
													  'id_menu' 	=> $id_menu_induk_1),
												$data);
			}else if($status_sekarang == '0'){
				// Cek jumlah useracces
				$cek_jumlah 	= $this->useraccess_toko->cek_jumlah_useraccess($id_pegawai_save, $id_menu_induk_1);
				$jumlah_access1 = $cek_jumlah->jumlah_access;
				if ($jumlah_access1 == 0){
					$data = array($nama_field 				=> '0',
							  	  'id_pegawai_pembaharuan'	=> $this->session->userdata('id_pegawai')
							 );

					$this->useraccess_toko->update(array('id_pegawai' 	=> $id_pegawai_save,
														  'id_menu' 	=> $id_menu_induk_1),
													$data);
				}
			}			
		}

		// Update menu induk 2
		$jumlah_access2 		= '0';
		$cek_menu_induk 		= $this->useraccess_toko->cek_menu_induk($id_menu_induk_1);
		if($cek_menu_induk){
			$id_menu_induk_2  	= $cek_menu_induk->id_menu_induk;
			if($status_sekarang == '1'){
				$data = array($nama_field 				=> '1 ',
							  'id_pegawai_pembaharuan'	=> $this->session->userdata('id_pegawai')
							 );

				$this->useraccess_toko->update(array('id_pegawai' 	=> $id_pegawai_save,
													  'id_menu' 	=> $id_menu_induk_2),
												$data);
			}else if($status_sekarang == '0'){
				// Cek jumlah useracces
				$cek_jumlah 	= $this->useraccess_toko->cek_jumlah_useraccess($id_pegawai_save, $id_menu_induk_2);
				$jumlah_access2 = $cek_jumlah->jumlah_access;
				if ($jumlah_access2 == 0){
					$data = array($nama_field 				=> '0',
							  	  'id_pegawai_pembaharuan'	=> $this->session->userdata('id_pegawai')
							 );

					$this->useraccess_toko->update(array('id_pegawai' 	=> $id_pegawai_save,
														  'id_menu' 	=> $id_menu_induk_2),
													$data);
				}
			}
		}

		echo json_encode(array(
			"status"          => TRUE,
			"id_menu_induk_1" => $id_menu_induk_1,
			"id_menu_induk_2" => $id_menu_induk_2,
			"jumlah_access1"  => $jumlah_access1,
			"jumlah_access2"  => $jumlah_access2
		));
	}

	private function _validate_password(){
		$data                 = array();
		$data['error_string'] = array();
		$data['inputerror']   = array();
		$data['status']       = TRUE;
		
		$i                    = $this->input;
		$id_pegawai           = $i->post('id_pegawai_password');
		$password_baru        = $i->post('password_baru');
		$password             = $i->post('konfirmasi_password_baru');

		// Validasi isian password tidak boleh kosong
		if($password_baru == ''){
			$data['inputerror'][]   = 'password_baru';
			$data['error_string'][] = 'Password baru harus di isi';
			$data['status']         = FALSE;
		}

		if($password == ''){
			$data['inputerror'][]   = 'konfirmasi_password_baru';
			$data['error_string'][] = 'Konfirmasi assword baru harus di isi';
			$data['status']         = FALSE;
		}

		// Validasi password baru dengan konfirmasi password baru
		if($password_baru != $password){
			$data['inputerror'][]   = 'konfirmasi_password_baru';
			$data['error_string'][] = 'Password baru dan konfirmasi password baru tidak sama';
			$data['status']         = FALSE;
		}

		if($data['status'] === FALSE){
			echo json_encode($data);
			exit();
		}
	}

	private function _validate($keterangan, $id_pegawai_toko)
	{
		$data = array();
		$data['error_string'] 	= array();
		$data['inputerror'] 	= array();
		$data['status'] 		= TRUE;
		$nama_pegawai_toko 		= $this->input->post('nama_pegawai_toko');
		$email 					= $this->input->post('email');

		if($this->input->post('nama_pegawai_toko') == ''){
			$data['inputerror'][] 	= 'nama_pegawai_toko';
			$data['error_string'][] = 'Nama pegawai wajib diisi';
			$data['status']	 		= FALSE;
		}

		if($this->input->post('alamat_pegawai_toko') == ''){
			$data['inputerror'][] 	= 'alamat_pegawai_toko';
			$data['error_string'][] = 'Alamat pegawai wajib diisi';
			$data['status'] 		= FALSE;
		}

		if($this->input->post('email') == ''){
			$data['inputerror'][] 	= 'email';
			$data['error_string'][] = 'Email wajib diisi';
			$data['status'] 		= FALSE;
		}

		if($this->input->post('handphone') == ''){
			$data['inputerror'][] 	= 'handphone';
			$data['error_string'][] = 'Handphone wajib diisi';
			$data['status'] 		= FALSE;
		}

		if($keterangan == 'save'){
			if($this->input->post('password') == ''){
				$data['inputerror'][] 	= 'password';
				$data['error_string'][] = 'Password wajib diisi';
				$data['status'] 		= FALSE;
			}
		}

		if($this->input->post('usergroup') == ''){
			$data['inputerror'][] 	= 'usergroup';
			$data['error_string'][] = 'Usergroup wajib dipilih';
			$data['status']			= FALSE;
		}

		if($this->input->post('kode_toko') == ''){
			$data['inputerror'][] 	= 'kode_toko';
			$data['error_string'][] = 'Toko wajib dipilih';
			$data['status'] 		= FALSE;
		}

		if($this->input->post('status_blokir') == ''){
			$data['inputerror'][] 	= 'status_blokir';
			$data['error_string'][] = 'Status blokir wajib dipilih';
			$data['status'] 		= FALSE;
		}

		$validasi_nama = $this->pegawai_toko->get_by_nama($nama_pegawai_toko);
		if($validasi_nama){
			if($id_pegawai_toko <> $validasi_nama->id_pegawai_toko){
				$data['inputerror'][] 	= 'nama_pegawai_toko';
				$data['error_string'][] = 'Nama pegawai sudah digunakan, harap ganti nama pegawai';
				$data['status'] 		= FALSE;
			}
		}

		$validasi_email = $this->pegawai_toko->get_by_email($email);
		if($validasi_email){
			if($id_pegawai_toko <> $validasi_email->id_pegawai_toko){
				$data['inputerror'][] 	= 'email';
				$data['error_string'][] = 'Email pegawai sudah digunakan, harap ganti email pegawai';
				$data['status'] 		= FALSE;
			}
		}

		if($data['status'] === FALSE){
			echo json_encode($data);
			exit();
		}
	}

	// Awal contoroller foto
	//Untuk proses upload foto
	public function proses_upload(){
		$id_pegawai_toko         = $this->input->post('id_pegawai_toko');
		$config['upload_path']   = './assets/upload/image/pegawai_toko/'; 
		$config['allowed_types'] = 'gif|jpg|png|bmp'; 
		$config['max_size']      = '2000'; //max 2mb
		$config['file_name']     = $id_pegawai;
        // $config['file_name'] 	 	= $sku.'-'.$nama_barang; nama foto : sku - nama
        
        $this->load->library('upload',$config);
        if($this->upload->do_upload('userfile')){
			$nama        = $this->upload->data('file_name');
			$token       = $this->input->post('token_foto');
			$upload_data = array('uploads' =>$this->upload->data());
			$gambar      = $upload_data['uploads']['file_name'];
	        
	        //Simpan ke database
	        $this->db->insert(
				'toko_mrc.foto_pegawai_toko',
			  	array(
					'id_pegawai' => $id_pegawai_toko,
					'nama_foto'  => $nama,
					'token'      => $token
				)
			);

	        //thumbnail
			$this->thumbnail_produk($gambar); 
		}
	}

	//function untuk thumbnail
	public function thumbnail_produk($gambar) {
	    $source_path = './assets/upload/image/pegawai_toko/'.$gambar;
	    $target_path = './assets/upload/image/pegawai_toko/thumbs';
	    $config_manip = array(
			'image_library' => 'gd2',
			'source_image'  => $source_path,
			'new_image'     => $target_path,
			'create_thumb'  => TRUE,
			'quality'       => "100%",
			'width'         => 360,
			'height'        => 360,
			'x_axis'        => 0,
			'y_axis'        => 0,
			'thumb_marker'  => ''
		);

	    $this->load->library('image_lib', $config_manip);
	    if (!$this->image_lib->resize()) {
	        echo $this->image_lib->display_errors($gambar);
	    }

	    // clear
	    $this->image_lib->clear();
	}

	public function ajax_daftar_foto()
	{
		if($this->input->is_ajax_request()){
			$id_pegawai   = $this->input->post('id_pegawai');
			$daftar_foto  = $this->pegawai_toko->ambil_daftar_foto($id_pegawai);
			$foto_profile = $this->pegawai_toko->get_by_id($id_pegawai);

			if($foto_profile){
				$alamat_foto = $foto_profile->foto;
			}

			if($daftar_foto->num_rows() > 0){
				$json['status'] 	= 1;
				$json['datanya'] 	= "";
				$json['datanya'] 	.= "<div class='row port m-b-20'>
			                                <div class='portfolioContainer'>";
				$no = 0;	
				foreach($daftar_foto->result() as $df){
					$no++;
					$json['datanya'] .= "
	                                        <div class='col-sm-3 col-md-4'>
	                                            <div class='thumb'>
	                                                <a name='foto' href='assets/upload/image/pegawai_toko/$df->nama_foto' class='image-popup' title='Foto $no'>
	                                                    <img src='assets/upload/image/pegawai_toko/thumbs/$df->nama_foto' class='thumb-img' alt='work-thumbnail'>
	                                                </a>
	                                                <div class='gal-detail'>
	                                                    <p class='text-muted text-left'>Foto - $no</p>
	                                                    <span>
									                    	<div class='row'>
								              	      			<div class='col-md-6 pull-left'>
										";
					
					if ($alamat_foto<>$df->nama_foto){ // Bukan foto profile
						$json['datanya'] .= "
									                                <a class='btn btn-rounded btn-default btn-block' href='javascript:void(0)' title='Jadikan Profile' onclick='jadikan_profile_foto(".$df->id_foto.")'><i class='fa fa-camera'></i> 
																	</a>
																</div>
						";
					}
					else { // Tandai sebagai foto profile
						$json['datanya'] .= "
									                                <a class='btn btn-rounded btn-primary btn-block' href='javascript:void(0)' title='Jadikan Profile' onclick='jadikan_profile_foto(".$df->id_foto.")'><i class='fa fa-camera'></i> 
																	</a>
																</div>
						";
					}

					$json['datanya'] 	.= "
																<div class='col-md-6 pull-right'>
																	<a class='btn btn-rounded btn-danger btn-block' href='javascript:void(0)' title='Hapus' onclick='delete_foto(".$df->id_foto.")'><i class='fa fa-remove'></i>
																	</a>
																</div>
															</div>
			                    						</span>
			                    					</div>
			                    				</div>
			                    			</div>
					";
				}
				$json['datanya'] .= "</div></div>";
			}else{
				$json['status']     = 0;
				$json['id_pegawai'] = $id_pegawai;
			}

			echo json_encode($json);
		}
	}

	//Untuk menghapus foto
	public function remove_foto(){
	//Ambil token foto
	$token 	= $this->input->post('token');
	$foto 	= $this->db->get_where(
		'toko_mrc.foto_pegawai_toko',
		array('token' => $token)
	);

	if($foto->num_rows()>0){
		$hasil 		= $foto->row();
		$nama_foto 	= $hasil->nama_foto;
		if(file_exists($file='./assets/upload/image/pegawai_toko/'.$nama_foto)){
			unlink($file);
		}
		$this->db->delete(
			'toko_mrc.foto_pegawai_toko',
			array('token' => $token)
		);
	}


		// echo "{}";
	}

	// Awal delete foto
	public function ajax_delete_foto()
	{
		if($this->input->is_ajax_request()){
			$id_foto = $this->input->post('id_foto');
			$foto    = $this->db->get_where('toko_mrc.foto_pegawai_toko',array('id_foto' => $id_foto));

			if($foto->num_rows()>0){
				$hasil     = $foto->row();
				$nama_foto =$hasil->nama_foto;
				if(file_exists($file='./assets/upload/image/pegawai_toko/'.$nama_foto)){
					unlink($file);
				}
				if(file_exists($file='./assets/upload/image/pegawai_toko/thumbs/'.$nama_foto)){
					unlink($file);
				}
				$this->db->delete('toko_mrc.foto_pegawai_toko',array('id_foto'=>$id_foto));
				echo json_encode(array('status' => 1));
			}
		}
	}

	// Awal jadikan foto profile barang
	public function ajax_jadikan_foto_profile()
	{
		if($this->input->is_ajax_request()){
			$id_foto    = $this->input->post('id_foto');
			$ambil_foto = $this->pegawai_toko->ambil_nama_foto($id_foto);
			if ($ambil_foto){
				$id_pegawai = $ambil_foto->id_pegawai;
				$nama_foto  = $ambil_foto->nama_foto;
			}

			$this->pegawai_toko->update_foto_profile($id_pegawai, $nama_foto);
			echo json_encode(array('status' => 1));
		}
	}
	// Akhir controller foto
}

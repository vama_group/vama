<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Supplier extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('pegawai_pusat_model','pegawai_pusat');
		$this->load->model('useraccess_pusat_model','useraccess_pusat');
		$this->load->model('supplier_model','supplier');
	}

	public function index()
	{
		// Cek user acces menu
		$id_pegawai = $this->session->userdata('id_pegawai');
		if($id_pegawai == ''){
			redirect(base_url().'login');
		}
		
		$cek_useraccess = $this->useraccess_pusat->cek_access($id_pegawai, '9');
		if($cek_useraccess){
			if($cek_useraccess->act_read == '0' or $cek_useraccess->act_read == '-'){
				redirect(base_url().'dashboard');
			}					
		}else{
			redirect(base_url().'dashboard');
		}

		$data_pegawai = $this->pegawai_pusat->get_by_id($id_pegawai);
		if($data_pegawai){
			$data['access_create']   = $cek_useraccess->act_create;
			$data['access_update']   = $cek_useraccess->act_update;
			$data['access_delete']   = $cek_useraccess->act_delete;
			$data['data_pegawai']    = $data_pegawai;
			$data['data_induk_menu'] = $this->useraccess_pusat->get_induk_menu($id_pegawai);
			
			$data['atribut_halaman'] = 'Supplier';
			$data['list_supplier']   = $this->supplier->listing();
			$data['halaman_list']    = $this->load->view('admin/master_data/supplier/list',$data,true);
			$data['halaman_form']    = $this->load->view('admin/master_data/supplier/form',$data,true);
			$data['halaman_plugin']  = $this->load->view('admin/master_data/supplier/plugin',$data,true);
			$data['isi_halaman']     = $this->load->view('admin/master_data/supplier/index',$data,true);
			$this->load->view('admin/layout',$data);
		}else{
			redirect(base_url().'login');
		}
	}

	public function ajax_list()
	{
		// Cek user acces menu
		$id_pegawai     = $this->session->userdata('id_pegawai');
		$cek_useraccess = $this->useraccess_pusat->cek_access($id_pegawai, '9');
		if($cek_useraccess->act_read == '0' or $cek_useraccess->act_read == '0'){
			redirect(base_url().'dashboard');
			exit();
		}

		$id_periode_stok_pusat = $this->input->post('id_periode_stok_pusat');
		if($id_periode_stok_pusat == ''){
			redirect(base_url().'supplier');
			exit();
		}

		$list = $this->supplier->get_datatables();
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $supplier) {
			$no++;
			$row 	= array();
			$row[] 	= $no;

			//add html for action
			if($cek_useraccess->act_update == 1){
				$tombol_edit 	 = '<a class="btn btn-rounded btn-xs btn-default" href="javascript:void(0)" title="Edit" 
										onclick="edit_supplier('."'".$supplier->id_supplier."'".')">
										<i class="fa fa-pencil" style="color: blue;"></i>
									</a>';
			}else{$tombol_edit 	 = '';}

			if($cek_useraccess->act_delete == 1){
				$tombol_hapus 	 = '<a class="btn btn-rounded btn-xs btn-default" href="javascript:void(0)" title="Hapus" 
										onclick="verifikasi_delete('."'".$supplier->id_supplier."'".')">
										<i class="fa fa-remove" style="color: red;"></i>
				  					</a>';
			}else{$tombol_hapus  = '';}

			if($cek_useraccess->act_update == 1 or $cek_useraccess->act_delete == 1){
				$row[] = '						
							'.$tombol_edit.'
						  	'.$tombol_hapus.'
					  	';
			}

			$row[] = $supplier->kode_supplier;
			$row[] = '<span class="text-dark">'.$supplier->nama_supplier.'</span>';
			$row[] = $supplier->tipe_supplier;
			$row[] = $supplier->tipe_bisnis;
			
			$row[]	= $supplier->pegawai_save;
			$row[] 	= $supplier->tanggal_pembuatan;
			$row[] 	= $supplier->pegawai_edit;
			$row[] 	= $supplier->tanggal_pembaharuan;
			$data[] = $row;
		}

		$output = array(
						"draw" 				=> $_POST['draw'],
						"recordsTotal" 		=> $this->supplier->count_all(),
						"recordsFiltered" 	=> $this->supplier->count_filtered(),
						"data" 				=> $data,
				);
		//output to json format
		echo json_encode($output);
	}

	public function ajax_add()
	{
		// Cek user acces menu
		$id_pegawai 		= $this->session->userdata('id_pegawai');
		$cek_useraccess 	= $this->useraccess_pusat->cek_access($id_pegawai, '9');
		if($cek_useraccess->act_create == '0' or $cek_useraccess->act_create == '-'){
			redirect(base_url().'supplier');
			exit();
		}

		$this->_validate('0');

		// Awal buat kode baru
		$akhir			= $this->supplier->akhir();
		$inisial		= "S";
		$id_supplier	= $akhir->id_supplier_baru;
		
		if($id_supplier < 10){
			$depan 		= '00';
		}elseif($id_supplier< 100){
			$depan		= '0';
		}else{
			$depan		= '';
		}

		$kode_supplier	= $inisial.$depan.$id_supplier;
		// Akhir buat kode baru

		$data = array(
			'kode_supplier'        => $kode_supplier,
			'nama_supplier'        => $this->input->post('nama_supplier'),
			'no_siup'              => $this->input->post('no_siup'),
			'alamat_supplier'      => $this->input->post('alamat_supplier'),
			'asal_supplier'        => $this->input->post('asal_supplier'),
			'tipe_bisnis'          => $this->input->post('tipe_bisnis'),
			'tipe_supplier'        => $this->input->post('tipe_supplier'),
			'kontak_pribadi'       => $this->input->post('kontak_pribadi'),
			'jabatan'              => $this->input->post('jabatan'),
			'telephone1'           => $this->input->post('telephone1'),
			'telephone2'           => $this->input->post('telephone2'),
			'handphone1'           => $this->input->post('handphone1'),
			'handphone2'           => $this->input->post('handphone2'),
			'fax'                  => $this->input->post('fax'),
			'email'                => $this->input->post('email'),
			'id_pegawai_pembuatan' => $this->session->userdata('id_pegawai'),
			'tanggal_pembuatan'    => date('Y-m-d H:i:s')
		);
		$insert = $this->supplier->save($data);
		echo json_encode(array("status" => TRUE));
	}

	public function ajax_edit()
	{
		// Cek user acces menu
		$id_pegawai 	= $this->session->userdata('id_pegawai');
		$cek_useraccess = $this->useraccess_pusat->cek_access($id_pegawai, '9');
		if($cek_useraccess->act_update == '0' or $cek_useraccess->act_update == '-'){
			redirect(base_url().'supplier');
			exit();
		}

		$id_supplier 	= $this->input->post('id_supplier');
		if($id_supplier == ''){
			redirect(base_url().'supplier');
			exit();
		}else{
			$data = $this->supplier->get_by_id($id_supplier);
			echo json_encode($data);
		}
	}

	public function ajax_update()
	{
		$id_pegawai 	= $this->session->userdata('id_pegawai');
		$cek_useraccess = $this->useraccess_pusat->cek_access($id_pegawai, '9');
		if($cek_useraccess->act_update == '0' or $cek_useraccess->act_update == '0'){
			redirect(base_url().'supplier');
			exit();
		}

		$id_supplier 	= $this->input->post('id_supplier');
		if($id_supplier == ''){
			redirect(base_url().'supplier');
			exit();
		}

		$this->_validate($id_supplier);
		$data = array(
						'nama_supplier' 			=> $this->input->post('nama_supplier'),
						'no_siup'		 			=> $this->input->post('no_siup'),
						'alamat_supplier' 			=> $this->input->post('alamat_supplier'),
						'asal_supplier' 			=> $this->input->post('asal_supplier'),
						'tipe_bisnis' 				=> $this->input->post('tipe_bisnis'),
						'tipe_supplier' 			=> $this->input->post('tipe_supplier'),
						'kontak_pribadi' 			=> $this->input->post('kontak_pribadi'),
						'jabatan' 					=> $this->input->post('jabatan'),
						'telephone1' 				=> $this->input->post('telephone1'),
						'telephone2' 				=> $this->input->post('telephone2'),
						'handphone1' 				=> $this->input->post('handphone1'),
						'handphone2' 				=> $this->input->post('handphone2'),
						'fax' 						=> $this->input->post('fax'),
						'email' 					=> $this->input->post('email'),
						'id_pegawai_pembaharuan'	=> $this->session->userdata('id_pegawai')
					);
		$this->supplier->update(array('id_supplier' => $this->input->post('id_supplier')), $data);
		echo json_encode(array("status" => TRUE));
	}

	public function ajax_verifikasi_delete()
	{
		// Cek user acces menu
		$id_pegawai 	= $this->session->userdata('id_pegawai');
		$cek_useraccess = $this->useraccess_pusat->cek_access($id_pegawai, '9');
		if($cek_useraccess->act_delete == '0' or $cek_useraccess->act_delete == '-'){
			redirect(base_url().'supplier');
			exit();
		}

		$id_supplier = $this->input->post('id_supplier');
		if($id_supplier == ''){
			redirect(base_url().'supplier');
			exit();
		}
		
		$data = $this->supplier->get_by_id($id_supplier);		
		echo json_encode(array(
								'pesan' 	=> "<b class='text-danger'>Yakin ingin menghapus supplier ini ? </b></br></br>
												Nama Supplier : </br>
												<h2 class='text-dark'>".$data->nama_supplier."</h2></br>

												Asal Supplier : </br>
												<b class='text-dark'>".$data->asal_supplier."</b></br></br>
												
												Tipe Supplier : </br>
												<b class='text-dark'>".$data->tipe_supplier."</b></br></br>

												Tipe Bisnis : </br>
												<b class='text-dark'>".$data->tipe_bisnis."</b>",

								'footer'	=> 	"<button onclick='delete_supplier($id_supplier)' 
													type='button' class='btn btn-primary waves-effect waves-light' 
													data-dismiss='modal' autofocus>Iya, Hapus</button> 
												<button type='button' class='btn btn-default waves-effect' 
													data-dismiss='modal'>Batal</button>"
							  	)
						);
	}

	public function ajax_delete()
	{
		$id_pegawai 	= $this->session->userdata('id_pegawai');
		$cek_useraccess = $this->useraccess_pusat->cek_access($id_pegawai, '9');
		if($cek_useraccess->act_delete == '0' or $cek_useraccess->act_delete == '-'){
			redirect(base_url().'supplier');
			exit();
		}

		$id_supplier = $this->input->post('id_supplier');
		if($id_supplier == ''){
			redirect(base_url().'supplier');
			exit();
		}

		$this->supplier->update_status_hapus($id_supplier, $id_pegawai);
		echo json_encode(array("status" => TRUE));
	}

	private function _validate($id_supplier)
	{
		$data = array();
		$data['error_string'] 	= array();
		$data['inputerror'] 	= array();
		$data['status'] 		= TRUE;
		$nama_supplier 			= $this->input->post('nama_supplier');

		if($nama_supplier == ''){
			$data['inputerror'][] = 'nama_supplier';
			$data['error_string'][] = 'Nama supplier wajib diisi';
			$data['status'] = FALSE;
		}

		$validasi_nama = $this->supplier->get_by_nama($nama_supplier);
		if($validasi_nama){
			if($id_supplier <> $validasi_nama->id_supplier){
				$data['inputerror'][] 	= 'nama_supplier';
				$data['error_string'][] = 'Nama supplier sudah digunakan, harap ganti nama supplier';
				$data['status'] 		= FALSE;
			}
		}

		if($data['status'] === FALSE){
			echo json_encode($data);
			exit();
		}
	}

	public function ajax_pencarian_supplier()
	{
		// $url = $this->input->post('url');
		if($this->input->is_ajax_request()){
			$keyword 	= $this->input->post('keyword');
			$supplier 	= $this->supplier->cari_supplier_1($keyword);

			if($supplier->num_rows() > 0){
				$json['status'] 	= 1;
				$json['datanya'] 	= "<ul id='daftar_autocompleted_supplier' class='user-list' style='width : 350px;'>";
				foreach($supplier->result() as $s){
					if($s->foto!=""){
						$json['datanya'] .= "
							                <li class='user-list-item'>
							                	<div class='avatar avatar-sm-box'>
					                                <img src='".base_url()."assets/upload/image/supplier/thumbs/$s->foto'>
							                	</div>";
			        }else{
						$nama_supplier 	  = $s->nama_supplier;
						$inisial 		  = substr($nama_supplier, 0, 1);
						$json['datanya'] .= "
							                <li class='user-list-item'>
							                	<div class='avatar avatar-sm-box'>
				                                    <span class='avatar-sm-box bg-info'>".$inisial."</span>
							                	</div>";
					}

					$json['datanya'] .= "
						                	<div class='user-desc'>
												<span id='id_supplier' style='display:none;'>".$s->id_supplier."</span>
												<span id='nama_supplier' class='name'>".$s->nama_supplier."</span>
												<span id='kode_supplier' class='desc'>".$s->kode_supplier."</span>
												<span id='tipe_supplier' class='name' style='display:none;'>".$s->tipe_supplier."</span>
											</div>
										</li>
										";
				}
				$json['datanya'] .= "</ul>";
			}else{
				$json['status'] = 0;
				// $json['pesan'] 	= $this->db->last_query();
			}
			echo json_encode($json);
		}else{
			redirect(base_url().$url);
			exit();
		}
	}

	public function list_supplier(){
		$id_pegawai = $this->session->userdata('id_pegawai');
		if($id_pegawai){		
			$ns            = $this->input->get('ns');
			$data_supplier = $this->supplier->cari_supplier_2($ns);
			if($data_supplier){
				echo json_encode($data_supplier);
			}else{
				echo json_encode('');
			}
		}else{
			redirect(base_url().'dashboard');
			exit();
		}
	}

	public function ajax_lihat_supplier()
	{
		// Cek user acces menu
		$id_pegawai = $this->session->userdata('id_pegawai');
		if($id_pegawai){
			$id_supplier = $this->input->post('id_supplier');
			if($id_supplier == ''){
				redirect(base_url().'dashboard');
				exit();
			}else{
				$data = $this->supplier->get_by_id($id_supplier);
				if($data){
					$data_supplier = array(
						'id_supplier'   => $data->id_supplier,
						'kode_supplier' => $data->kode_supplier,
						'nama_supplier' => $data->nama_supplier,
						'tipe_supplier' => $data->tipe_supplier
					);
				}else{
					$data_supplier = '';
				}
				echo json_encode($data_supplier);
			}
		}else{
			redirect(base_url().'dashboard');
			exit();
		}
	}
}

<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Laporan_perakitan extends MY_Controller 
{
	function __construct()
	{
		parent::__construct();
		$this->CI =& get_instance();
		$this->load->model('pegawai_pusat_model','pegawai_pusat');
        $this->load->model('useraccess_pusat_model','useraccess_pusat');
		$this->load->model('barang_pusat_model','barang_pusat');
		$this->load->model('pegawai_pusat_model','pegawai_pusat');
		$this->load->model('perakitan_master_model','perakitan_master');
		$this->load->model('perakitan_detail_model','perakitan_detail');
	}

	public function index()
	{
		// Cek user acces menu
		$id_pegawai = $this->session->userdata('id_pegawai');
		if($id_pegawai == ''){
			redirect(base_url().'dashboard');
		}
		
		$cek_useraccess = $this->useraccess_pusat->cek_access($id_pegawai, '44');
		if($cek_useraccess){
			if($cek_useraccess->act_read == '0' or $cek_useraccess->act_read == '-'){
				redirect(base_url().'dashboard');
			}					
		}else{
			redirect(base_url().'dashboard');
		}

		$data_pegawai = $this->pegawai_pusat->get_by_id($id_pegawai);
		if($data_pegawai){		
			$data['access_create']       = $cek_useraccess->act_create;
			$data['data_pegawai']        = $data_pegawai;
			$data['data_induk_menu']     = $this->useraccess_pusat->get_induk_menu($id_pegawai);
			
			$data['atribut_halaman']     = 'Laporan Perakitan';
			$data['halaman_laporan']     = $this->load->view('admin/laporan/perakitan/laporan',$data,true);
			$data['halaman_plugin']      = $this->load->view('admin/laporan/perakitan/plugin',$data,true);
			$data['isi_halaman']         = $this->load->view('admin/laporan/perakitan/index',$data,true);
			$this->load->view('admin/layout',$data);
		}else{
			redirect(base_url().'login');
		}
	}

	public function ajax_list()
	{
		// Cek user acces menu
		$id_pegawai 				= $this->session->userdata('id_pegawai');
		$cek_useraccess_transaksi 	= $this->useraccess_pusat->cek_access($id_pegawai, '29');
		$cek_useraccess_laporan 	= $this->useraccess_pusat->cek_access($id_pegawai, '44');
		if($cek_useraccess_laporan->act_read == '0' or $cek_useraccess_laporan->act_read == '-'){
			redirect(base_url().'dashboard');
		}

		$tanggal_filter = $this->input->post('tanggal_filter');
		if($tanggal_filter == ''){
			redirect(base_url().'laporan_perakitan');
			exit();
		}

		$tanggal_awal           = substr($tanggal_filter, 0, 10);
		$tanggal_akhir          = substr($tanggal_filter, 18, 27);
		$data_laporan_perakitan = $this->perakitan_master->get_laporan('_get_laporan_query', $tanggal_awal, $tanggal_akhir);
		
		$data                   = array();
		$no                     = $_POST['start'];

		foreach ($data_laporan_perakitan as $dlp) {
			$no++;
			$row    = array();
			$row[]  = $no;
			$status = $dlp->status_perakitan;

			//add html for action
			if($cek_useraccess_laporan->act_update == 1){
				$tombol_edit 	 = '<a class="btn btn-rounded btn-default btn-xs" 
										onclick="edit_perakitan('."'".$dlp->id_perakitan_m."'".')">
										<i class="fa fa-pencil" style="color:blue;"></i>
									</a>';
			}else{
				if($cek_useraccess_transaksi->act_update == 1 AND $status=="MENUNGGU" 
					OR $cek_useraccess_transaksi->act_create == 1 AND $status=="MENUNGGU"){
					$tombol_edit = '<a class="btn btn-rounded btn-default btn-xs" 
										onclick="edit_perakitan('."'".$dlp->id_perakitan_m."'".')">
										<i class="fa fa-pencil" style="color:blue;"></i>
									</a>';
				}else{
					$tombol_edit = '';
				}
			}

			if($cek_useraccess_laporan->act_delete == 1){
				$tombol_hapus = '<a class="btn btn-rounded btn-default btn-xs" 
									onclick="verifikasi_hapus_perakitan('."'".$dlp->id_perakitan_m."'".')">
									<i class="fa fa-times" style="color:red;"></i>
								</a>';
			}else{
				$tombol_hapus = '';
				if($cek_useraccess_transaksi->act_delete == 1 AND $status=="MENUNGGU"){
					$tombol_hapus = '<a class="btn btn-rounded btn-default btn-xs" 
										onclick="verifikasi_hapus_perakitan('."'".$dlp->id_perakitan_m."'".')">
										<i class="fa fa-times" style="color:red;"></i>
									</a>';
				}else{
					$tombol_hapus = '';
				}
			}

			$row[] =	'	<a class="btn btn-rounded btn-default btn-xs" 
							onclick="cetak_faktur('."'".$dlp->id_perakitan_m."'".')"> <i class="fa fa-print"></i>
							</a>
							'.$tombol_edit.'
							'.$tombol_hapus.'
				  		';

			$row[] 	= '<span class="text-dark">'.$dlp->no_perakitan.'</span><br/>';
			if($status =="SELESAI"){
				$row[] 	= '<span class="badge badge-success text-center">'.$status.'</span>';
			}elseif($status =="MENUNGGU"){
				$row[] 	= '<span class="badge badge-danger text-center">'.$status.'</span>';
			}
			$row[] 	= date('d-m-Y', strtotime($dlp->tanggal));

			if($dlp->foto !== "avatar.JPG"){
				$row[] = '<img src="'."".base_url()."".'assets/upload/image/barang/thumbs/'."".$dlp->foto."".'" class="thumb-sm img-circle">';
			}else{
				$nama_barang = $dlp->nama_barang;
				$inisial     = substr($nama_barang, 0, 1);			
				$row[]       = '<span class="avatar-sm-box badge-default text-dark">'.$inisial.'</span>';
			}

			$row[] 	= '<span class="text-dark">'.$dlp->sku.'</span>';
			$row[] 	= '<span class="text-dark">'.$dlp->nama_barang.'</span>';
			$row[] 	= '<span class="pull-right">'.number_format($dlp->jumlah_perakitan,'0','.',',').'</span>';
			$row[] 	= '<span>'.number_format($dlp->jumlah_komponen,'0','.',',').' item</span>';

			$row[] 	= $dlp->keterangan_lain;
			$row[] 	= $dlp->pegawai_save;
			$row[] 	= $dlp->tanggal_pembuatan;
			$row[] 	= $dlp->pegawai_edit;
			$row[] 	= $dlp->tanggal_pembaharuan;
			$data[]	= $row;
		}

		$output = array(
			"draw"            => $_POST['draw'],
			"recordsTotal"    => $this->perakitan_master->count_all('vamr4846_vama.perakitan_master', $tanggal_awal, $tanggal_akhir),
			"recordsFiltered" => $this->perakitan_master->count_filtered('_get_laporan_query', $tanggal_awal, $tanggal_akhir),
			"data"            => $data
		);
		echo json_encode($output);
	}

	public function ajax_list_detail()
	{
		// Cek user acces menu
		$id_pegawai               = $this->session->userdata('id_pegawai');
		$cek_useraccess_transaksi = $this->useraccess_pusat->cek_access($id_pegawai, '29');
		$cek_useraccess_laporan   = $this->useraccess_pusat->cek_access($id_pegawai, '44');
		if($cek_useraccess_laporan->act_read == '0' or $cek_useraccess_laporan->act_read == '-'){
			redirect(base_url().'dashboard');
		}

		$tanggal_filter = $this->input->post('tanggal_filter');
		if($tanggal_filter == ''){
			redirect(base_url().'laporan_perakitan');
			exit();
		}

		$tanggal_awal           = substr($tanggal_filter, 0, 10);
		$tanggal_akhir          = substr($tanggal_filter, 18, 27);
		$data_laporan_perakitan = $this->perakitan_master->get_laporan('_get_laporan_query_detail', $tanggal_awal, $tanggal_akhir);
		
		$data                   = array();
		$no                     = $_POST['start'];
		$no_perakitan           = '0';

		foreach ($data_laporan_perakitan as $dlp) {
			$no++;
			$row    = array();
			$row[]  = $no;
			$status = $dlp->status_perakitan;

			//add html for action
			if($cek_useraccess_laporan->act_update == 1){
				$tombol_edit 	 = '<a class="btn btn-rounded btn-default btn-xs" 
										onclick="edit_perakitan('."'".$dlp->id_perakitan_m."'".')">
										<i class="fa fa-pencil" style="color:blue;"></i>
									</a>';
			}else{
				if($cek_useraccess_transaksi->act_update == 1 AND $status=="MENUNGGU" 
					OR $cek_useraccess_transaksi->act_create == 1 AND $status=="MENUNGGU"){
					$tombol_edit = '<a class="btn btn-rounded btn-default btn-xs" 
										onclick="edit_perakitan('."'".$dlp->id_perakitan_m."'".')">
										<i class="fa fa-pencil" style="color:blue;"></i>
									</a>';
				}else{
					$tombol_edit = '';
				}
			}

			if($cek_useraccess_laporan->act_delete == 1){
				$tombol_hapus = '<a class="btn btn-rounded btn-default btn-xs" 
									onclick="verifikasi_hapus_perakitan('."'".$dlp->id_perakitan_m."'".')">
									<i class="fa fa-times" style="color:red;"></i>
								</a>';
			}else{
				$tombol_hapus = '';
				if($cek_useraccess_transaksi->act_delete == 1 AND $status=="MENUNGGU"){
					$tombol_hapus = '<a class="btn btn-rounded btn-default btn-xs" 
										onclick="verifikasi_hapus_perakitan('."'".$dlp->id_perakitan_m."'".')">
										<i class="fa fa-times" style="color:red;"></i>
									</a>';
				}else{
					$tombol_hapus = '';
				}
			}

			$row[] =	'	<a class="btn btn-rounded btn-default btn-xs" 
							onclick="cetak_faktur('."'".$dlp->id_perakitan_m."'".')"> <i class="fa fa-print"></i>
							</a>
							'.$tombol_edit.'
							'.$tombol_hapus.'
				  		';

			if($no_perakitan <> $dlp->no_perakitan){
				$row[] 	= '<span class="btn btn-purple btn-block btn-rounded btn-xs">'.$dlp->no_perakitan.'</span>';
			}else{
				$row[] 	= '<span class="btn btn-default btn-block btn-rounded btn-xs">'.$dlp->no_perakitan.'</span>';
			}

			if($status =="SELESAI"){
				$row[] 	= '<span class="badge badge-success text-center">'.$status.'</span>';
			}elseif($status =="MENUNGGU"){
				$row[] 	= '<span class="badge badge-danger text-center">'.$status.'</span>';
			}
			
			$row[] 	= date('d-m-Y', strtotime($dlp->tanggal));
			$row[] 	= '<span class="text-dark">'.$dlp->sku_perakitan.' - '.$dlp->nama_barang_perakitan.'</span>';
			
			if($dlp->foto_komponen !== "avatar.JPG"){
				$row[] = '<img src="'."".base_url()."".'assets/upload/image/barang/thumbs/'."".$dlp->foto_komponen."".'" 
						  class="thumb-sm img-circle">';
			}else{
				$nama_barang = $dlp->nama_barang_komponen;
				$inisial     = substr($nama_barang, 0, 1);			
				$row[]       = '<span class="avatar-sm-box badge-default text-dark">'.$inisial.'</span>';
			}

			$row[] 	= '<span class="text-dark">'.$dlp->sku_komponen.'</span>';
			$row[] 	= '<span class="text-dark">'.$dlp->nama_barang_komponen.'</span>';
			$row[] 	= '<span class="pull-right">'.number_format($dlp->jumlah_komponen,'0','.',',').'</span>';
			$row[] 	= $dlp->keterangan_lain;
			$row[] 	= $dlp->pegawai_save;
			$row[] 	= $dlp->tanggal_pembuatan;
			$row[] 	= $dlp->pegawai_edit;
			$row[] 	= $dlp->tanggal_pembaharuan;
			$data[]	= $row;
			$no_perakitan = $dlp->no_perakitan;
		}

		$output = array(
			"draw"            => $_POST['draw'],
			"recordsTotal"    => $this->perakitan_master->count_all('vamr4846_vama.perakitan_detail', $tanggal_awal, $tanggal_akhir),
			"recordsFiltered" => $this->perakitan_master->count_filtered('_get_laporan_query_detail', $tanggal_awal, $tanggal_akhir),
			"data"            => $data
		);
		echo json_encode($output);
	}

	public function ajax_list_batal()
	{
		// Cek user acces menu
		$id_pegawai               = $this->session->userdata('id_pegawai');
		$cek_useraccess_transaksi = $this->useraccess_pusat->cek_access($id_pegawai, '29');
		$cek_useraccess_laporan   = $this->useraccess_pusat->cek_access($id_pegawai, '44');
		if($cek_useraccess_laporan->act_read == '0' or $cek_useraccess_laporan->act_read == '-'){
			redirect(base_url().'dashboard');
		}

		$tanggal_filter = $this->input->post('tanggal_filter');
		if($tanggal_filter == ''){
			redirect(base_url().'laporan_perakitan');
			exit();
		}

		$tanggal_awal           = substr($tanggal_filter, 0, 10);
		$tanggal_akhir          = substr($tanggal_filter, 18, 27);
		$data_laporan_perakitan = $this->perakitan_master->get_laporan('_get_laporan_query_batal', $tanggal_awal, $tanggal_akhir);
		
		$data                   = array();
		$no                     = $_POST['start'];
		$no_perakitan           = '0';

		foreach ($data_laporan_perakitan as $dlp) {
			$no++;
			$row    = array();
			$row[]  = $no;

			if($no_perakitan <> $dlp->no_perakitan){
				$row[] 	= '<span class="btn btn-purple btn-block btn-rounded btn-xs">'.$dlp->no_perakitan.'</span>';
			}else{
				$row[] 	= '<span class="btn btn-default btn-block btn-rounded btn-xs">'.$dlp->no_perakitan.'</span>';
			}
			
			$row[] 	= '<span class="text-dark">'.$dlp->sku_perakitan.' - '.$dlp->nama_barang_perakitan.'</span>';			
			if($dlp->foto_komponen !== "avatar.JPG"){
				$row[] = '<img src="'."".base_url()."".'assets/upload/image/barang/thumbs/'."".$dlp->foto_komponen."".'" 
						  class="thumb-sm img-circle">';
			}else{
				$nama_barang = $dlp->nama_barang_komponen;
				$inisial     = substr($nama_barang, 0, 1);			
				$row[]       = '<span class="avatar-sm-box badge-default text-dark">'.$inisial.'</span>';
			}

			$row[] 	= '<span class="text-dark">'.$dlp->sku_komponen.'</span>';
			$row[] 	= '<span class="text-dark">'.$dlp->nama_barang_komponen.'</span>';
			$row[] 	= '<span class="pull-right">'.number_format($dlp->jumlah_komponen,'0','.',',').'</span>';

			$row[] 	= $dlp->keterangan_batal;
			$row[] 	= $dlp->pegawai_batal;
			$row[] 	= $dlp->tanggal_pembatalan;
			$row[] 	= $dlp->pegawai_save;
			$row[] 	= $dlp->tanggal_pembuatan;
			$row[] 	= $dlp->pegawai_edit;
			$row[] 	= $dlp->tanggal_pembaharuan;
			$data[]	= $row;
			$no_perakitan = $dlp->no_perakitan;
		}

		$output = array(
			"draw"            => $_POST['draw'],
			"recordsTotal"    => $this->perakitan_master->count_all('vamr4846_vama.perakitan_detail_batal', $tanggal_awal, $tanggal_akhir),
			"recordsFiltered" => $this->perakitan_master->count_filtered('_get_laporan_query_batal', $tanggal_awal, $tanggal_akhir),
			"data"            => $data
		);
		echo json_encode($output);
	}

	public function ambil_total()
	{
		// Cek user acces menu
		$id_pegawai 				= $this->session->userdata('id_pegawai');
		$cek_useraccess 			= $this->useraccess_pusat->cek_access($id_pegawai, '44');
		if($cek_useraccess->act_read == '0' or $cek_useraccess->act_read == '-'){
			redirect(base_url().'dashboard');
		}

		$tanggal_filter = $this->input->post('tanggal_filter');
		if($tanggal_filter == ''){
			redirect(base_url().'laporan_perakitan');
			exit();
		}

		$tanggal_awal 				= substr($tanggal_filter, 0, 10);
		$tanggal_akhir 				= substr($tanggal_filter, 18, 27);

		// Ambil jumlah transaksi, total tunai, debit dan total uang masuk
		$data_uang_masuk 			= $this->perakitan_master->get_grand_total($tanggal_awal, $tanggal_akhir);
		if ($data_uang_masuk){
			$jml_transaksi			= number_format($data_uang_masuk->jml_transaksi,'0','.',',');
		}else{
			$jml_transaksi			= '0';
			$grand_total_perakitan 	= '0';
		}
		
		echo json_encode(array(
								'status' 				=> 1,
								"jml_transaksi"			=> $jml_transaksi
						));
	}

	public function ajax_verifikasi_hapus_perakitan()
	{
		if($this->input->is_ajax_request()){
			$id_perakitan_m = $this->input->post('id_perakitan_m');
			if($id_perakitan_m == ''){
				redirect(base_url().'laporan_perakitan');
				exit();
			} 

			// Cek user acces menu
			$id_pegawai     = $this->session->userdata('id_pegawai');
			$cek_useraccess = $this->useraccess_pusat->cek_access($id_pegawai, '44');
			$data_master    = $this->perakitan_master->get_master($id_perakitan_m);
			if($data_master){
				if($data_master->status_perakitan == 'SELESAI'){
					if($cek_useraccess->act_delete == '0' or $cek_useraccess->act_delete == '-'){
						echo json_encode(array(
												"status" 		=> 0,
											  	"info_pesan" 	=> "Maaf anda tidak diberikan izin untuk menghapus transaksi perakitan,
																	dengan status perakitan : SELESAI !",
												"pesan" 		=> "
																		No. Perakitan : ".$data_master->no_perakitan."
																		SKU : ".$data_master->sku."
																		Nama Barang : ".$data_master->nama_barang."
																		Status Perakitan : ".$data_master->status_perakitan."
																	",
												"tipe_pesan" 	=> "error"
											 )
									   );
						exit();
					}
				}

				// Verifikasi stok, jika minus maka tampilkan pesan
				// Jika tidak menyebabkan minus, tampilkan informasi hapus perakitan
				$id_barang_pusat 		= $data_master->id_barang_pusat;
				$jumlah_perakitan 		= $data_master->jumlah_perakitan;
				$data_barang 			= $this->barang_pusat->get_id($id_barang_pusat);
				if($data_barang){
					$sku 				= $data_barang->sku;
					$nama_barang 		= $data_barang->nama_barang;
					$stok_sekarang 		= $data_barang->total_stok;
					$validasi_stok 		= $stok_sekarang - $jumlah_perakitan + 0;

					if($validasi_stok < 0){
						echo json_encode(array(
												"status" 		=> 2,
											  	"info_pesan" 	=> "Oops!",
												"pesan" 		=> "
																		Maaf anda tidak bisa menghapus transaksi perakitan ini  !
																		Karna akan menyebabkan stok barang ini menjadi minus : 
																		No. Perakitan : ".$data_master->no_perakitan."
																		SKU : ".$sku."
																		Nama Barang : ".$nama_barang."
																		Total Minus : ".$validasi_stok."
																	"
											 )
									   );
						exit();
					}else{
						echo json_encode(array(
							'status' 	=> 1,
							'pesan' 	=> "
												<b>DATA PERAKITAN</b><br/>
												<small>SKU : </small>
												<b>$data_master->sku</b><br/>
												
												<small>Nama Barang : </small>	
												<b>$data_master->nama_barang </b> </br>

												<small>Jumlah Perakitan : </small></br>
												<h2 class='text-danger'>$data_master->jumlah_perakitan </h2>
												<hr>

												<small>Total Komponen : </small>
												<b>$data_master->total_komponen - dari ($data_master->jumlah_komponen item) </b>
												<hr>

												<textarea name='keterangan_batal' id='keterangan_batal' class='form-control' placeholder='Keterangan batal' style='resize: vertical;'></textarea>
											",

							'footer'	=> 	"<button onclick='hapus_perakitan($data_master->id_perakitan_m)' type='button' class='btn btn-primary 
											 waves-effect waves-light' data-dismiss='modal' autofocus>Iya, Hapus</button> 
											 <button type='button' class='btn btn-default waves-effect' data-dismiss='modal'>Batal</button>"
						));
						exit();
					}
				}else{
					echo json_encode(array(
											'status'     =>	0,
											'info_pesan' => 'Oops!',
											'pesan'      =>	"Komponen perakitan tidak ditemukan."
									));
					exit();
				}
			}else{
				echo json_encode(array(
										'status'     =>	0,
										'info_pesan' => 'Oops!',
										'pesan'      =>	"Transaksi perakitan tidak ditemukan."
								));
				exit();
			}
		}else{
			redirect(base_url().'laporan_perakitan');
		}
	}

	public function ajax_hapus_perakitan()
	{
		if($this->input->is_ajax_request()){
			$id_perakitan_m = $this->input->post('id_perakitan_m');
			if($id_perakitan_m == ''){
				redirect(base_url().'laporan_perakitan');
				exit();
			} 

			// Cek user acces menu
			$id_pegawai     = $this->session->userdata('id_pegawai');
			$cek_useraccess = $this->useraccess_pusat->cek_access($id_pegawai, '44');
			$master         = $this->perakitan_master->get_master($id_perakitan_m);
			if($master){
				$no_perakitan          = $master->no_perakitan;
				$id_barang_perakitan   = $master->id_barang_pusat;
				$jumlah_perakitan      = $master->jumlah_perakitan;
				
				$id_pegawai_pembatalan = $this->session->userdata('id_pegawai');
				$keterangan_batal      = $this->input->post('keterangan_batal');
				
				if($master->status_perakitan == 'SELESAI'){
					if($cek_useraccess->act_delete == '0' or $cek_useraccess->act_delete == '-'){
						echo json_encode(array(
												"status" 		=> 0,
											  	"info_pesan" 	=> "Maaf anda tidak diberikan izin untuk menghapus transaksi perakitan,
																	dengan status perakitan : SELESAI !",
												"pesan" 		=> "
																		No. Perakitan : ".$master->no_perakitan."
																		SKU : ".$master->sku."
																		Nama Barang : ".$master->nama_barang."
																		Jumlah Perakitan : ".$master->jumlah_perakitan."
																	",
												"tipe_pesan" 	=> "error",
												"gaya_tombol" 	=> "btn-danger btn-md waves-effect waves-light"
											 )
									   );
						exit();
					}
				}

				// Verifikasi stok, jika minus maka tampilkan pesan
				// Jika tidak menyebabkan minus, tampilkan informasi hapus perakitan
				$data_barang 			= $this->barang_pusat->get_id($id_barang_perakitan);
				if($data_barang){
					$sku 				= $data_barang->sku;
					$nama_barang 		= $data_barang->nama_barang;
					$stok_sekarang 		= $data_barang->total_stok;
					$validasi_stok 		= $stok_sekarang - $jumlah_perakitan + 0;

					if($validasi_stok < 0){
						echo json_encode(array(
												"status" 		=> 0,
											  	"info_pesan" 	=> "Oops!",
												"pesan" 		=> "
																		Maaf anda tidak bisa menghapus transaksi perakitan ini  !
																		Karna akan menyebabkan stok barang ini menjadi minus : 
																		No. Perakitan : ".$master->no_perakitan."
																		SKU : ".$sku."
																		Nama Barang : ".$nama_barang."
																		Total Minus : ".$validasi_stok."
																	",
												"tipe_pesan" 	=> "error",
												"gaya_tombol" 	=> "btn-danger btn-md waves-effect waves-light"
											 )
									   );
						exit();
					}

					$hapus = $this->perakitan_master->hapus_transaksi(
						$id_perakitan_m, $no_perakitan, $id_barang_perakitan, $jumlah_perakitan, $keterangan_batal, $id_pegawai_pembatalan
					);

					if($hapus){
						echo json_encode(array(
							"status" 		=> 1,
							"no_perakitan"	=> $no_perakitan,
							"info_pesan" 	=> "Berhasil!",
							"pesan" 		=> "Transaksi perakitan berhasil dihapus,
												No. Perakitan : ".$no_perakitan.",
												Jumlah Perakitan : ".$jumlah_perakitan."
											   ",
							"tipe_pesan" 	=> "success",
							"gaya_tombol" 	=> "btn-success btn-md waves-effect waves-light"
						));
					}else{
						echo json_encode(array(
							"status"      => 0,
							"info_pesan"  => "Oops!",
							"pesan"       => "Terjadi kesalahan, coba lagi !",
							"tipe_pesan"  => "error",
							"gaya_tombol" => "btn-danger btn-md waves-effect waves-light"
						));
					}
				}else{
					echo json_encode(array(
											'status'      => 0,
											'info_pesan'  => 'Oops!',
											'pesan'       =>"Master barang perakitan tidak ditemukan.",
											"tipe_pesan"  => "error",
											"gaya_tombol" => "btn-danger btn-md waves-effect waves-light"
									));
				}
			}else{
				echo json_encode(array(
										'status'      => 0,
										'info_pesan'  => 'Oops!',
										'pesan'       =>"Transaksi perakitan tidak ditemukan.",
										"tipe_pesan"  => "error",
										"gaya_tombol" => "btn-danger btn-md waves-effect waves-light"
								));
			}
		}else{
			redirect(base_url().'laporan_perakitan');
		}
	}
}
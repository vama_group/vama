<?php
// ob_start();
defined('BASEPATH') OR exit('No direct script access allowed');

class Penjualan extends MY_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->CI =& get_instance();
		$this->load->model('pegawai_pusat_model','pegawai_pusat');
        $this->load->model('useraccess_pusat_model','useraccess_pusat');
		$this->load->model('customer_pusat_model','customer_pusat');
		$this->load->model('barang_pusat_model','barang_pusat');
		$this->load->model('barang_toko_model','barang_toko');
		$this->load->model('pegawai_pusat_model','pegawai_pusat');
		$this->load->model('penjualan_master_model','penjualan_master');
		$this->load->model('penjualan_detail_model','penjualan_detail');
		$this->load->model('penjualan_detail_kartu_model','penjualan_detail_kartu');
		$this->load->model('kartu_edc_pusat_model','kartu_edc_pusat');
		$this->load->model('referensi_sumber_online_model', 'referensi_sumber_online');
		$this->load->model('periode_stok_pusat_model','periode_stok_pusat');
		$this->load->model('periode_stok_toko_model','periode_stok_toko');
	}

	public function index()
	{
		// Cek user acces menu
		$id_pegawai = $this->session->userdata('id_pegawai');
		if($id_pegawai == ''){
			redirect(base_url().'login');
			exit();
		}
		
		$cek_useraccess = $this->useraccess_pusat->cek_access($id_pegawai, '27');
		if($cek_useraccess){
			if($cek_useraccess->act_read == '0' or $cek_useraccess->act_read == '-'){
				redirect(base_url().'dashboard');
				exit();
			}					
		}else{
			redirect(base_url().'dashboard');
			exit();
		}

		$data_pegawai = $this->pegawai_pusat->get_by_id($id_pegawai);
		if($data_pegawai){
			$cek_useraccess            = $this->useraccess_pusat->cek_access($id_pegawai, '27');
			$cek_useraccess_laporan    = $this->useraccess_pusat->cek_access($id_pegawai, '39');
			$cek_useraccess_customer   = $this->useraccess_pusat->cek_access($id_pegawai, '11');
			
			$data['data_pegawai']      = $data_pegawai;
			$data['data_induk_menu']   = $this->useraccess_pusat->get_induk_menu($id_pegawai);
			$data['access_create']     = $cek_useraccess->act_create;
			$data['access_update']     = $cek_useraccess->act_update;
			$data['access_laporan']    = $cek_useraccess_laporan->act_read;
			$data['access_customer']   = $cek_useraccess_customer->act_create;
			
			$data['atribut_halaman']   = 'Penjualan Pusat';
			$data['id_penjualan_m']    = '0';
			$data['halaman_transaksi'] = $this->load->view('admin/transaksi/penjualan/transaksi', $data, true);
			$data['halaman_plugin']    = $this->load->view('admin/transaksi/penjualan/plugin', $data, true);
			$data['isi_halaman']       = $this->load->view('admin/transaksi/penjualan/index', $data, true);
			$this->load->view('admin/layout',$data);
		}else{
			redirect(base_url().'login');
		}
	}

	public function transaksi()
	{
		$id_penjualan_m = $this->input->get('id');
		$data_penjualan = $this->penjualan_master->get_master($id_penjualan_m);
		if($data_penjualan->id_penjualan_m == ''){
			redirect(base_url().'penjualan');
			exit();
		}

		// Cek user acces menu
		$id_pegawai              = $this->session->userdata('id_pegawai');
		$cek_useraccess_laporan  = $this->useraccess_pusat->cek_access($id_pegawai, '39');
		$cek_useraccess_customer = $this->useraccess_pusat->cek_access($id_pegawai, '11');
		if($cek_useraccess_laporan->act_update == '1' or $data_penjualan->status_penjualan == 'MENUNGGU'
			or $data_penjualan->status_penjualan == 'DISIAPKAN' or $data_penjualan->status_penjualan == 'DIKIRIM'){
			$cek_useraccess            = $this->useraccess_pusat->cek_access($id_pegawai, '27');
			$data['access_laporan']    = $cek_useraccess_laporan->act_read;
			$data['access_customer']   = $cek_useraccess_customer->act_create;
			$data['access_create']     = $cek_useraccess->act_create;
			$data['access_update']     = $cek_useraccess->act_update;
			
			// cari data pegawai
			$id_pegawai                = $this->session->userdata('id_pegawai');
			$data_pegawai              = $this->pegawai_pusat->get_by_id($id_pegawai);
			$this->CI->session->set_userdata('usergroup_name', $data_pegawai->usergroup_name);
			$data['data_pegawai']      = $data_pegawai;
			$data['data_induk_menu']   = $this->useraccess_pusat->get_induk_menu($id_pegawai);
			
			$data['atribut_halaman']   = 'Penjualan';
			$data['id_penjualan_m']    = $id_penjualan_m;
			$data['no_penjualan']      = $data_penjualan->no_penjualan;
			$data['halaman_transaksi'] = $this->load->view('admin/transaksi/penjualan/transaksi',$data,true);
			$data['halaman_plugin']    = $this->load->view('admin/transaksi/penjualan/plugin',$data,true);
			$data['isi_halaman']       = $this->load->view('admin/transaksi/penjualan/index',$data,true);
			$this->load->view('admin/layout',$data);
		}else{
			redirect(base_url().'penjualan');
			exit();
		}
	}

	public function simpan_transaksi()
	{
		if(!empty($_POST['id_penjualan_m'])){
			// Cek user acces menu
			$id_pegawai     = $this->session->userdata('id_pegawai');
			$cek_useraccess = $this->useraccess_pusat->cek_access($id_pegawai, '27');
			$id_penjualan_m = $this->input->post('id_penjualan_m');

			if($cek_useraccess->act_create == '1' or $cek_useraccess->act_update == '1'){
				$no_penjualan          = $this->input->post('no_penjualan');
				$id_penjualan_m        = $this->input->post('id_penjualan_m');
				$data_penjualan_master = $this->penjualan_master->get_by_id($id_penjualan_m);
				if($data_penjualan_master){
					$status_penjualan  = $data_penjualan_master->status_penjualan;
					$tanggal           = date('Y-m-d H:i:s');
					$id_pegawai        = $this->session->userdata('id_pegawai');
					$id_customer_pusat = $this->input->post('id_customer_pusat');
					$data_customer     = $this->customer_pusat->get_by_id($id_customer_pusat);
					if($data_customer){
						$tipe_customer_pusat = $data_customer->tipe_customer_pusat;
						$asal_customer_pusat = $data_customer->asal_customer_pusat;
					}else{
						echo json_encode(array(	
							'status' => 0,
							'pesan'  => 'Sebelum menyimpan transaksi penjualan,
										 Anda diwajibkan untuk memilih customer,
										 Jika customer belum terdaftar klik tombol tambah customer,
										 Disebelah kanan kotak pencarian nama customer',
							'url'    => 'penjualan/transaksi/?&id='.$id_penjualan_m
						));
						exit();
					}

					$catatan     = $this->clean_tag_input($this->input->post('catatan'));
					$total       = $this->input->post('total');
					$ppn         = $this->input->post('ppn');
					$biaya_lain  = $this->input->post('biaya_lain');
					$grand_total = $this->input->post('grand_total');
					$tunai       = $this->input->post('tunai');
					$debit       = $this->input->post('debit');

					if($status_penjualan == 'MENUNGGU'){
						$status_sekarang = 'DISIAPKAN';
					}else{
						$status_sekarang = $status_penjualan;
					}

					// Update penjualan master
					$master = $this->penjualan_master->update_master(	
						$id_penjualan_m, $id_pegawai,
						$id_customer_pusat, $tipe_customer_pusat,
						$total, $biaya_lain, $ppn, $grand_total, $debit, $tunai,
						$tanggal, $catatan, $status_sekarang
					);

					if($asal_customer_pusat == 'ONLINE'){
						// Validasi referensi jumlah poin jika jika kosong maka tidak menghitung poin
						$referensi_jumlah_poin = $this->penjualan_master->cek_referensi_poin();
						if($referensi_jumlah_poin){
							$nominal         = $referensi_jumlah_poin->nominal;
							$jumlah_poin     = $referensi_jumlah_poin->jumlah_poin;
							$jumlah_poin_fix = round(($grand_total/$nominal) * $jumlah_poin);

							if($jumlah_poin_fix > 0){
								// Validasi poin
								$cek_poin = $this->penjualan_master->cek_poin($id_penjualan_m, $no_penjualan);
								if($cek_poin){
									$this->penjualan_master->update_poin($id_penjualan_m, $no_penjualan, $id_customer_pusat, $grand_total, $jumlah_poin_fix, $id_pegawai);
								}else{
									$this->penjualan_master->insert_poin($id_penjualan_m, $no_penjualan, $id_customer_pusat, $grand_total, $jumlah_poin_fix, $id_pegawai);
								}
							}
						}
					}else{
						$cek_poin = $this->penjualan_master->cek_poin($id_penjualan_m, $no_penjualan);
						if($cek_poin){
							$this->penjualan_master->delete_poin($id_penjualan_m, $no_penjualan);
						}
					}

					if($master){
						echo json_encode(array( 
							'status' => 1,
							'pesan'  => "Transaksi berhasil disimpan,
										No. Penjualan : ".$no_penjualan.",
										Apakah anda ingin mencetak faktur penyiapan barang ?"
							));
					}else{
						echo json_encode(array(	
							'status' => 0,
							'pesan'  => 'Terjadi kesalah saat menyimpan, harap coba kembali.',
							'url'    => 'penjualan/transaksi/?&id='.$id_penjualan_m
						));
					}
				}else{
					echo json_encode(array(	
						'status' => 0,
						'pesan'  => 'No. penjualan tidak terdaftar, harap hubungi administrator.',
						'url'    => 'penjualan/transaksi/?&id='.$id_penjualan_m
					));
				}
			}else{
				echo json_encode(array(	
					'status' => 0,
					'pesan'  => 'Maaf anda tidak diizinkan untuk menyimpan transaksi penjualan.',
					'url'    => 'penjualan/transaksi/?&id='.$id_penjualan_m
				));
			}
		}else{
			redirect(base_url().'penjualan');
		}
	}

	public function tahan_transaksi()
	{
		if( ! empty($_POST['id_penjualan_m'])){
			// Cek user acces menu
			$url            = $this->input->post('url');
			$id_pegawai     = $this->session->userdata('id_pegawai');
			$cek_useraccess = $this->useraccess_pusat->cek_access($id_pegawai, '27');
			$id_penjualan_m = $this->input->post('id_penjualan_m');

			if($cek_useraccess->act_create == '1' or $cek_useraccess->act_update == '1'){
				$no_penjualan          = $this->input->post('no_penjualan');
				$id_penjualan_m        = $this->input->post('id_penjualan_m');
				$data_penjualan_master = $this->penjualan_master->get_by_id($id_penjualan_m);
				if($data_penjualan_master){
					$status_penjualan = $data_penjualan_master->status_penjualan;
					if($status_penjualan == 'DISIAPKAN' or $status_penjualan == 'DIKIRIM' or $status_penjualan == 'SELESAI'){
						echo json_encode(array(	
							'status'	=> 0,
							'pesan' 	=> 'Maaf transaksi dengan,
											No. Penjualan : '.$no_penjualan.',
											sudah tidak bisa ditahan,
											Karna sudah '.$status_penjualan.'',
							'url'   	=> 'penjualan/transaksi/?&id='.$id_penjualan_m
						));
						exit();
					}else{
						$id_customer_pusat = $this->input->post('id_customer_pusat');
						$data_customer     = $this->customer_pusat->get_by_id($id_customer_pusat);
						if($data_customer){
							$tipe_customer_pusat = $data_customer->tipe_customer_pusat;
						}else{
							echo json_encode(array(	
								'status' => 0,
								'pesan'  => 'Maaf, sebelum menyimpan transaksi penjualan,
											 Anda diwajibkan untuk memilih customer,
											 Jika customer belum terdaftar klik tombol tambah customer,
											 Disebelah kanan kotak pencarian nama customer',
								'url'    => 'penjualan/transaksi/?&id='.$id_penjualan_m
							));
							exit();
						}

						if($data_penjualan_master->tipe_customer_pusat == 'MRC'){
							$id_customer_pusat   = $data_penjualan_master->id_customer_pusat;
							$tipe_customer_pusat = $data_penjualan_master->tipe_customer_pusat;
						}

						$status_penjualan = $data_penjualan_master->status_penjualan;
						$tanggal          = date('Y-m-d H:i:s');
						$id_pegawai       = $this->session->userdata('id_pegawai');
						$catatan          = $this->clean_tag_input($this->input->post('catatan'));
						
						$total            = $this->input->post('total');
						$ppn              = $this->input->post('ppn');
						$biaya_lain       = $this->input->post('biaya_lain');
						$grand_total      = $this->input->post('grand_total');
						$tunai            = $this->input->post('tunai');
						$debit            = $this->input->post('debit');

						// Update penjualan master
						$master = $this->penjualan_master->update_master(	
							$id_penjualan_m, $id_pegawai,
							$id_customer_pusat, $tipe_customer_pusat,
							$total, $biaya_lain, $ppn, $grand_total, $debit, $tunai,
							$tanggal, $catatan, 'MENUNGGU'
						);
						if($master){
							echo json_encode(array(
								'status' => 1,
								'pesan'  => "Transaksi berhasil ditahan,
											No. Penjualan : ".$no_penjualan."."
							));
						}else{
							echo json_encode(array(	
								'status' => 0,
								'pesan'  => 'Terjadi kesalah saat menyimpan, harap coba kembali.',
								'url'    => 'penjualan/transaksi/?&id='.$id_penjualan_m
							));
						}
					}
				}else{
					echo json_encode(array(	
						'status' => 0,
						'pesan'  => 'No. penjualan tidak terdaftar, harap hubungi administrator.',
						'url'    => 'penjualan/transaksi/?&id='.$id_penjualan_m
					));
				}
			}else{
				echo json_encode(array(	
					'status' => 0,
					'pesan'  => 'Maaf anda tidak diizinkan untuk menahan transaksi penjualan.',
					'url'    => 'penjualan/transaksi/?&id='.$id_penjualan_m
				));
			}
		}else{
			redirect(base_url().'penjualan');
		}
	}

	public function ambil_data()
	{
		// Cek user acces menu
		$id_pegawai     = $this->session->userdata('id_pegawai');
		$cek_useraccess = $this->useraccess_pusat->cek_access($id_pegawai, '27');
		if($cek_useraccess->act_read == '0' or $cek_useraccess->act_read == '-'){
			redirect(base_url().'penjualan');
			exit();
		}

		$id_penjualan_m = $this->input->post('id_penjualan_m');
		if($id_penjualan_m == ''){
			redirect(base_url().'penjualan');
			exit();
		}

		$data_induk = $this->penjualan_master->get_master($id_penjualan_m);
		$data_batal = $this->penjualan_master->get_jumlah_batal($id_penjualan_m);
		echo json_encode(array(
			"data_induk" => $data_induk,
			"data_batal" => $data_batal
		));
	}

	private function no_penjualan_baru(){
		$id_pegawai = $this->session->userdata('id_pegawai');
		if($id_pegawai == ''){
			redirect(base_url().'penjualan');
			exit();
		}
		
		$tanggal_sekarang = date('Y-m-d');
		$bulan_sekarang   = date('m');
		$tahun_sekarang   = date('Y');
		$tahun_sekarang_2 = date('y');
		
		$akhir            = $this->penjualan_master->no_penjualan_baru($tahun_sekarang, $bulan_sekarang, $id_pegawai);
		$inisial          = "PJ";
		if($akhir){
			$no_baru = $akhir->no_baru;
		}else{
			$no_baru = 1;
		}
		
		if($no_baru < 10){
			$depan = '000';
		}elseif($no_baru < 100){
			$depan = '00';
		}elseif($no_baru < 1000){
			$depan = '0';
		}else{
			$depan = '';
		}
		
		$jumlah            = $no_baru;
		$no_penjualan_baru = $inisial.'-'.$bulan_sekarang.$tahun_sekarang_2.'-'.$id_pegawai.'-'.$depan.$jumlah;
		return $no_penjualan_baru;
	}

	public function simpan_detail()
	{
		// Cek user acces menu
		$id_pegawai      = $this->session->userdata('id_pegawai');
		$cek_useraccess  = $this->useraccess_pusat->cek_access($id_pegawai, '27');
		$id_barang_pusat = $this->input->post('id_barang_pusat');
		if($id_barang_pusat == ''){
			redirect(base_url().'penjualan');
			exit();
		}

		// Ambil data penjualan master
		$status_simpann    = "0";
		$id_master         = "0";
		$id_penjualan_m    = $this->input->post('id_penjualan_m');
		$no_penjualan      = $this->input->post('no_penjualan');
		$tanggal           = date('Y-m-d H:i:s');
		$id_pegawai        = $this->session->userdata('id_pegawai');
		$id_customer_pusat = $this->input->post('id_customer_pusat');
		$catatan           = $this->clean_tag_input($this->input->post('catatan'));
		$biaya_lain        = $this->input->post('biaya_lain');
		$tunai             = $this->input->post('tunai');
		$debit             = $this->input->post('debit');
		// $tipe_customer_pusat = $this->input->post('tipe_customer_pusat');

		// Validasi customer
		$data_customer = $this->customer_pusat->get_by_id($id_customer_pusat);
		if($data_customer){
			$tipe_customer_pusat = $data_customer->tipe_customer_pusat;
		}else{
			echo json_encode(array(
				'status' => 2,
				'pesan'  => 'Maaf, sebelum menyimpan data penjualan barang,
							 Anda diwajibkan untuk memilih customer,
							 Jika customer belum terdaftar klik tombol tambah customer,
							 Disebelah kanan kotak pencarian nama customer',
				'url'    => ''
			));
			exit();
		}

		// Ambil kode toko, jika tipe customer "MRC"
		if($tipe_customer_pusat === 'MRC'){
			$dt        = $this->customer_pusat->get_by_id($id_customer_pusat);
			$kode_toko = $dt->kode_customer_pusat;
		}else{
			$kode_toko = '';
		}

		// Validasi penjualan master sudah ada atau belum
		$data_penjualan_master	= $this->penjualan_master->get_by_id($id_penjualan_m);
		if($data_penjualan_master){
			if($cek_useraccess->act_create == '1' or $cek_useraccess->act_update == '1'){
				if($data_penjualan_master->tipe_customer_pusat == 'MRC'){
					$kode_toko           = $data_penjualan_master->kode_customer_pusat;
					$id_customer_pusat   = $data_penjualan_master->id_customer_pusat;
					$tipe_customer_pusat = $data_penjualan_master->tipe_customer_pusat;
				}

				$id_master        = $data_penjualan_master->id_penjualan_m;
				$status_penjualan = $data_penjualan_master->status_penjualan;
				$status_simpan    = "1";
			}else{
				echo json_encode(array(
					'status' => 2,
					'pesan'  => 'Maaf anda tidak diizinkan untuk menyimpan data penjualan barang.',
					'url'    => 'penjualan/transaksi/?&id='.$id_penjualan_m
				));
				exit();
			}
		}else{
			if($cek_useraccess->act_create == '1' or $cek_useraccess->act_update == '1'){
				$status_simpan = "1";
			}else{
				echo json_encode(array(
					'status' => 2,
					'pesan'  => 'Maaf anda tidak diizinkan untuk menyimpan data penjualan barang.',
					'url'    => 'penjualan/transaksi/?&id='.$id_penjualan_m
				));
				exit();
			}
		}

		if($status_simpan == "1"){
			// Ambil data penjualan detail
			$sku               = $this->input->post('sku');
			$jumlah_beli       = $this->input->post('jumlah_beli');
			$jenis_harga       = $this->input->post('jenis_harga');
			$harga_satuan      = $this->input->post('harga');
			$discount          = $this->input->post('discount');
			$subtotal          = $this->input->post('subtotal');
			
			$data_barang_pusat = $this->barang_pusat->get_id($id_barang_pusat);
			$kode_barang       = $data_barang_pusat->kode_barang;
			$nama_barang       = $data_barang_pusat->nama_barang;
			$total_stok        = $data_barang_pusat->total_stok;
			$modal             = $data_barang_pusat->modal_bersih;
			$status2           = $data_barang_pusat->status2;

			// Validasi insert atau update di penjualan detail
			$data_penjualan_detail 	= $this->penjualan_detail->get_id($id_master, $id_barang_pusat);
			if($data_penjualan_detail){
				if($cek_useraccess->act_update == '1'){
					if($data_penjualan_detail->jumlah_beli == $data_penjualan_detail->jumlah_masuk){
						echo json_encode(array(
							"status" 	=> 3,
						  	"pesan"   	=> "<b class='text-danger'>
						  						Maaf anda tidak bisa memperbaharui penjualan barang dengan  jumlah : ".$jumlah_beli."
						  					</b></br></br>
						  					SKU : </br>
						  					<b class='text-dark'>".$sku."</b></br></br>

						  					Nama barang : </br>
						  					<b class='text-dark'>".$nama_barang."</b></br></br>

											<b class='text-danger'>
												Karna barang ini sudah di approve oleh toko!
											</b></br>
						  					"
						));
						exit();
					}

					// Cari jumlah beli sebelumnya
					$id_detail        = $data_penjualan_detail->id_penjualan_d;
					$jumlah_beli_lama = $data_penjualan_detail->jumlah_beli;
					$validasi_stok1   = $total_stok + $jumlah_beli_lama;
					$validasi_stok2   = $validasi_stok1 - $jumlah_beli;
					$stok_sekarang    = $jumlah_beli_lama + $total_stok;

					// Validasi stok
					if($validasi_stok2 < 0){
						echo json_encode(array(
							"status" 	=> 3,
						  	"pesan"   	=> "<b class='text-danger'>
						  						Maaf anda tidak bisa memperbaharui penjualan barang dengan  jumlah : ".$jumlah_beli."
						  					</b></br></br>
						  					SKU : </br>
						  					<b class='text-dark'>".$sku."</b></br></br>

						  					Nama barang : </br>
						  					<b class='text-dark'>".$nama_barang."</b></br></br>

						  					<b class='text-dark'>Karna bisa menyebabkan minus sebanyak : </b></br>
						  					<h2 class='text-danger'>".$validasi_stok2."</h2></br>

						  					<b class='text-primary'>Sisa Stok Sekarang :</b>
						  					<h2 class='text-primary'>".$stok_sekarang."</h2>
						  					"
						));
						exit();
					}else{
						if($tipe_customer_pusat === 'MRC'){
							// Update harga eceran toko
							$table = 'vamr4846_toko_mrc.barang_toko_'.$kode_toko;
							$data  = array(
								'harga_eceran'           => $harga_satuan,
								'status'                 => 'AKTIF',
								'status_hapus'           => 'TIDAK',
								'id_pegawai_pembaharuan' => $this->session->userdata('id_pegawai')
							);
							$this->barang_toko->update('id_barang_pusat='.$id_barang_pusat.'', $data, $table);
						}

						// update dan kembalikan stok sebelumnya
						$simpan_data = $this->penjualan_detail->update_detail(
						   $id_master, $id_barang_pusat, $jumlah_beli, $modal,
						   $jenis_harga, $harga_satuan, $discount, $subtotal, $id_pegawai, $jumlah_beli_lama,
						   $tipe_customer_pusat, $kode_toko
						);
					}
				}else{
					echo json_encode(array(
						'status' => 2,
						'pesan'  => 'Maaf anda tidak diizinkan untuk mengedit data penjualan barang.',
						'url'    => 'penjualan/transaksi/?&id='.$id_penjualan_m
					));
					exit();
				}
			}else{
				if($cek_useraccess->act_create == '1'){
					$validasi_stok 		= $total_stok - $jumlah_beli;

					// Validasi stok
					if($validasi_stok < 0){
						echo json_encode(array(
							"status" 	=> 3,
						  	"pesan"   	=> "<b class='text-danger'>
						  						Maaf anda tidak bisa menambahkan penjualan barang dengan  jumlah : ".$jumlah_beli."
						  					</b></br></br>
						  					SKU : </br>
						  					<b class='text-dark'>".$sku."</b></br></br>

						  					Nama barang : </br>
						  					<b class='text-dark'>".$nama_barang."</b></br></br>

						  					<b class='text-dark'>Karna bisa menyebabkan minus sebanyak : </b></br>
						  					<h2 class='text-danger'>".$validasi_stok."</h2></br>

						  					<b class='text-primary'>Sisa Stok Sekarang :</b>
						  					<h2 class='text-primary'>".$total_stok."</h2>
											  					"
						));
						exit();
					}else{
						if($id_master == "0"){
							// Insert penjualan master
							$no_penjualan = $this->no_penjualan_baru();
							$master 	  = $this->penjualan_master->insert_master( 
								$no_penjualan, $tanggal, $id_pegawai,
								$id_customer_pusat, $tipe_customer_pusat,
								'0', $biaya_lain, '0', '0', '0', '0', $catatan
							);

							$data_penjualan_master 	= $this->penjualan_master->get_id($no_penjualan);
							$id_master 				= $data_penjualan_master->id_penjualan_m;
							$status_penjualan 		= $data_penjualan_master->status_penjualan;
						}

						// insert
						$simpan_data = $this->penjualan_detail->insert_detail( 
							$id_master, $id_barang_pusat, $jumlah_beli, $modal,
							$jenis_harga, $harga_satuan, $discount, $subtotal, $id_pegawai
						);

						if($tipe_customer_pusat === 'MRC'){
							$table = 'vamr4846_toko_mrc.barang_toko_'.$kode_toko;

							//Cek barang toko
							$data_barang_toko = $this->barang_toko->get_barang_toko($id_barang_pusat, $kode_toko); 
							if($data_barang_toko){
								// Update harga eceran toko
								$data = array(
									'harga_eceran'           => $harga_satuan,
									'status'                 => 'AKTIF',
									'status_hapus'           => 'TIDAK',
									'id_pegawai_pembaharuan' => $this->session->userdata('id_pegawai')
								);
								$this->barang_toko->update('id_barang_pusat='.$id_barang_pusat.'', $data, $table);
							}else{
								// Insert master barang toko baru, jika di toko tidak ada atau barang baru
								$data2 = array(  
									'kode_toko'            => $kode_toko,
									'id_barang_pusat'      => $id_barang_pusat,
									'kode_barang'          => $kode_barang,
									'sku'                  => $sku,
									'total_stok'           => '0',
									'modal_bersih'         => $modal,
									'harga_eceran'         => $harga_satuan,
									'status'               => 'AKTIF',
									'status2'              => $status2,
									'id_pegawai_pembuatan' => $this->session->userdata('id_pegawai'),
									'tanggal_pembuatan'    => date('Y-m-d H:i:s')
					        	);
								$this->barang_toko->tambah($data2, $table);
							}
						}
					}
				}else{
					echo json_encode(array(
						'status' => 2,
						'pesan'  => 'Maaf anda tidak diizinkan untuk menambahkan data penjualan barang.',
						'url'    => 'penjualan/transaksi/?&id='.$id_penjualan_m
					));
					exit();
				}
			}

			if($simpan_data){
				$data_total_penjualan = $this->penjualan_detail->get_total($id_master);
				if ($data_total_penjualan){
					$ttl           = $data_total_penjualan->total;
					// $ppn           = $ttl*(10/100);
					$ppn           = '0';
					$grand_total   = $ttl+$ppn+$biaya_lain;
					$jumlah_barang = $data_total_penjualan->jumlah_barang;

					// Update total dan grandtotal di penjualan master
					$master = $this->penjualan_master->update_master(	
						$id_master, $id_pegawai,
						$id_customer_pusat, $tipe_customer_pusat,
						$ttl, $biaya_lain, $ppn, $grand_total, '0', $grand_total,
						$tanggal, $catatan, $status_penjualan
					);
				}else{
					$ttl           = '0';
					$grand_total   = '0';
					$jumlah_barang = '0';
				}

				$data_batal = $this->penjualan_master->get_jumlah_batal($id_penjualan_m);
				if($data_batal){
					$jumlah_barang_batal = $data_batal->jumlah_barang;
				}else{
					$jumlah_barang_batal = '0';
				}

				// Update stok barang pusat
				$this->barang_pusat->update_stok($id_barang_pusat, $jumlah_beli);

				// Update harga barang toko
				if($tipe_customer_pusat === 'MRC'){
					$data = array( 
						'harga_eceran' => $harga_satuan,
						'status'       => 'AKTIF',
						'status_hapus' => 'TIDAK'
					);
					$this->barang_toko->update(array(	
						'sku'       => $sku,
						'kode_toko' => $kode_toko,
			        ), $data, 'vamr4846_toko_mrc.barang_toko_'.$kode_toko
					);
				}

				echo json_encode(array(
					'status'              => 1,
					'pesan'               => "Data penjualan barang berhasil disimpan",
					'url'                 => 'penjualan/transaksi/?&id='.$id_master,
					'no_penjualan'        => $no_penjualan,
					'id_pm'               => $id_master,
					'id_customer_pusat'   => $id_customer_pusat,
					'tipe_customer_pusat' => $tipe_customer_pusat,
					'jumlah_barang'       => $jumlah_barang,
					'jumlah_barang_batal' => $jumlah_barang_batal,
					'total'               => $ttl,
					'ppn'                 => $ppn,
					'grand_total'         => $grand_total
				));
			}else{
				echo json_encode(array(
					'status' => 0,
					'pesan'  => 'Data barang gagal disimpan'
				));
			}
		}
	}

	public function ajax_list()
	{
		$id_penjualan_m = $this->clean_tag_input($this->input->post('id_penjualan_m'));
		if($id_penjualan_m == ''){
			redirect(base_url().'penjualan');
		}

		// Cek user acces menu
		$id_pegawai             = $this->session->userdata('id_pegawai');
		$cek_useraccess         = $this->useraccess_pusat->cek_access($id_pegawai, '27');
		$cek_useraccess_laporan = $this->useraccess_pusat->cek_access($id_pegawai, '39');
		if($cek_useraccess->act_read == '0' or $cek_useraccess->act_read == '-'){
			redirect(base_url().'dashboard');
		}

		$id_master             = '0';
		$data_penjualan_master = $this->penjualan_master->get_by_id($id_penjualan_m);
		if($data_penjualan_master){
			$id_master        = $data_penjualan_master->id_penjualan_m;
			$status_penjualan = $data_penjualan_master->status_penjualan;
		}

		$list = $this->penjualan_detail->get_datatables('_get_datatables_query', $id_master, '');
		$data = array();
		$no   = $_POST['start'];

		foreach ($list as $pd){
			$no++;
			$row   = array();
			$row[] = $no;

			//add html for action
			if($cek_useraccess->act_update == 1){
				if($status_penjualan == "SELESAI"){
					if($cek_useraccess_laporan->act_update == 1){
						$tombol_edit = '<a class="btn btn-rounded btn-xs btn-default"
											onclick="edit_penjualan_detail('."'".$pd->id_penjualan_d."'".')">
											<i class="fa fa-pencil" style="color:blue;" title="Edit"></i>
										</a>';
					}else{
						$tombol_edit = '';
					}
				}else{
					$tombol_edit = '<a class="btn btn-rounded btn-xs btn-default"
										onclick="edit_penjualan_detail('."'".$pd->id_penjualan_d."'".')">
										<i class="fa fa-pencil" style="color:blue;" title="Edit"></i>
									</a>';
				}
			}else{
				$tombol_edit = '';
			}

			if($cek_useraccess->act_delete == 1){
				if($status_penjualan == "SELESAI"){
					if($cek_useraccess_laporan->act_delete == 1){
						$tombol_hapus = '<a class="btn btn-rounded btn-xs btn-default"
											onclick="verifikasi_hapus_penjualan_detail('."'".$pd->id_penjualan_d."'".')">
										 	<i class="fa fa-times" style="color:red;" title="Hapus"></i>
										 </a>';
					}else{
						$tombol_hapus = '';
					}
				}else{
					$tombol_hapus = '<a class="btn btn-rounded btn-xs btn-default"
										onclick="verifikasi_hapus_penjualan_detail('."'".$pd->id_penjualan_d."'".')">
									 	<i class="fa fa-times" style="color:red;" title="Hapus"></i>
									 </a>';
				}
			}else{
				$tombol_hapus = '';
			}

			if($tombol_edit == '' AND $tombol_hapus == ''){
				$tombol_keterangan = '<b class="btn btn-rounded btn-xs btn-default text-muted text-center">Tidak ada akses</b>';
			}else{
				$tombol_keterangan = '';
			}
			
			if($pd->jumlah_masuk == $pd->jumlah_beli){
				$row[] 	= 	'<span class="btn btn-xs btn-rounded btn-default text-primary">Qty Masuk : '.$pd->jumlah_masuk.'</span>';
			}elseif($pd->jumlah_retur > 0){
				$row[] 	= 	'<span class="btn btn-xs btn-rounded btn-default text-primary">Qty Retur : '.$pd->jumlah_retur.'</span>';
			}else{
				$row[] 	=	' 	'.$tombol_edit.'
								'.$tombol_hapus.'
								'.$tombol_keterangan.'
					  		';
			}

			if($pd->jumlah_retur > 0){
				$warna_tulisan = "text-danger";
			}else{
				$warna_tulisan = "text-dark";
			}

			$row[] 	= '<span class="'.$warna_tulisan.'">'.$pd->sku.'</span>';
			$row[] 	= '<span class="'.$warna_tulisan.'">'.$pd->nama_barang.'</span>';
			$row[] 	= '<span class="'.$warna_tulisan.'">'.$pd->jenis_harga.'</span>';
			$row[] 	= '<span class="pull-right '.$warna_tulisan.'">'.number_format($pd->harga_satuan,'0',',','.').'</span>';
			$row[] 	= '<span class="pull-right '.$warna_tulisan.'">'.number_format($pd->discount,'0',',','.').'</span>';
			$row[] 	= '<span class="pull-right '.$warna_tulisan.'">'.number_format($pd->jumlah_beli,'0',',','.').'</span>';
			$row[] 	= '<span class="pull-right '.$warna_tulisan.'">'.number_format($pd->subtotal,'0',',','.').'</span>';
			
			$row[] 	= $pd->pegawai_save;
			$row[] 	= $pd->tanggal_pembuatan;
			$row[] 	= $pd->pegawai_edit;
			$row[] 	= $pd->tanggal_pembaharuan;
			$data[]	= $row;
		}

		$output = array(	
			"draw"            => $_POST['draw'],
			"recordsTotal"    => $this->penjualan_detail->count_all('vamr4846_vama.penjualan_detail', $id_master, ''),
			"recordsFiltered" => $this->penjualan_detail->count_filtered('_get_datatables_query', $id_master, ''),
			"data"            => $data
						);
		echo json_encode($output);
	}

	public function ajax_list_batal()
	{
		// Cek user acces menu
		$id_pegawai     = $this->session->userdata('id_pegawai');
		$cek_useraccess = $this->useraccess_pusat->cek_access($id_pegawai, '27');
		if($cek_useraccess->act_read == '0' or $cek_useraccess->act_read == '-'){
			redirect(base_url().'dashboard');
		}

		$id_penjualan_m = $this->clean_tag_input($this->input->post('id_penjualan_m'));
		if($id_penjualan_m == ''){
			redirect(base_url().'penjualan');
		}

		$data_penjualan_master = $this->penjualan_master->get_by_id($id_penjualan_m);
		if($data_penjualan_master){
			$id_master = $data_penjualan_master->id_penjualan_m;
		}else{
			$id_master = '0';
		}

		$list = $this->penjualan_detail->get_datatables('_get_datatables_query_batal', $id_master, '');
		$data = array();
		$no   = $_POST['start'];

		foreach ($list as $pd){
			$no++;
			$row = array();
			$row[] = $no;

			//add html for action
			if($cek_useraccess->act_create == 1 or $cek_useraccess->act_update == 1){
				$tombol_pulihkan = '<a class="btn btn-rounded btn-default btn-xs" onclick="pulihkan_data_detail('."'".$pd->id_penjualan_b."'".')">
										<i class="fa fa-undo" style="color:blue;" title="Pulihkan"></i>
									</a>';
			}else{
				$tombol_pulihkan = '<b class="btn btn-rounded btn-default btn-xs text-muted text-center">Tidak ada akses</b>';
			}

			$row[] 	= ''.$tombol_pulihkan.'';
			$row[] 	= $pd->sku;
			$row[] 	= $pd->nama_barang;
			$row[] 	= $pd->jenis_harga;

			$row[] 	= '<span class="pull-right">'.number_format($pd->harga_satuan,'0',',','.').'</span>';
			$row[] 	= '<span class="pull-right">'.number_format($pd->discount,'0',',','.').'</span>';
			$row[] 	= '<span class="pull-right">'.number_format($pd->jumlah_beli,'0',',','.').'</span>';
			$row[] 	= '<span class="pull-right">'.number_format($pd->subtotal,'0',',','.').'</span>';

			$row[] 	= $pd->keterangan_batal;
			$row[] 	= $pd->nama_pegawai;
			$row[] 	= $pd->tanggal_pembatalan;
			$data[]	= $row;
		}

		$output = array(	
			"draw"            => $_POST['draw'],
			"recordsTotal"    => $this->penjualan_detail->count_all('vamr4846_vama.penjualan_detail_batal', $id_master, ''),
			"recordsFiltered" => $this->penjualan_detail->count_filtered('_get_datatables_query_batal', $id_master, ''),
			"data"            => $data
		);
		echo json_encode($output);
	}

	public function ajax_list_barang()
	{
		// Cek user acces menu
		$id_pegawai     = $this->session->userdata('id_pegawai');
		$cek_useraccess = $this->useraccess_pusat->cek_access($id_pegawai, '27');
		if($cek_useraccess->act_read == '0' or $cek_useraccess->act_read == '-'){
			redirect(base_url().'dashboard');
		}

		$id_penjualan_m = $this->clean_tag_input($this->input->post('id_penjualan_m'));
		if($id_penjualan_m == ''){
			redirect(base_url().'penjualan');
		}

		$id_customer   = $this->clean_tag_input($this->input->post('id_customer'));
		$data_customer = $this->customer_pusat->get_by_id($id_customer);
		if($data_customer){
			$kode_customer = $data_customer->kode_customer_pusat;
			$tipe_customer = $data_customer->tipe_customer_pusat;
		}else{
			$kode_customer = '0';
			$tipe_customer = '-';
		}

		$data_penjualan_master = $this->penjualan_master->get_by_id($id_penjualan_m);
		if($data_penjualan_master){
			$id_master = $data_penjualan_master->id_penjualan_m;
		}else{
			$id_master = '0';
		}

		$data_penjualan = $this->penjualan_master->get_by_id($id_master);
		if($data_penjualan){
			if($data_penjualan->tipe_customer_pusat === 'MRC'){
				$kode_customer = $data_penjualan->kode_customer_pusat;
				$tipe_customer = $data_penjualan->tipe_customer_pusat;
			}
		}

		if($tipe_customer == 'MRC'){
			$list = $this->penjualan_detail->get_datatables('_get_datatables_query_barang_toko', $id_master, $kode_customer);
		}else{
			$list = $this->penjualan_detail->get_datatables('_get_datatables_query_barang', $id_master, '');
		}

		$data = array();
		$no   = $_POST['start'];

		foreach ($list as $bp){
			$no++;
			$row 	= array();
			$row[] 	= $no;

			$row[] 	= $bp->sku;
			$row[] 	= $bp->nama_barang;
			if($tipe_customer == 'MRC'){
				$row[] 	= '<span class="pull-right">'.number_format($bp->total_stok_toko,'0',',','.').'</span>';
				$row[] 	= '<span class="pull-right">'.number_format($bp->total_stok_pusat,'0',',','.').'</span>';
				$row[] 	= '<span class="pull-right">'.number_format($bp->harga_eceran_toko,'0',',','.').'</span>';
				$row[] 	= '<span class="pull-right">'.number_format($bp->harga_eceran_pusat,'0',',','.').'</span>';
			}else{
				$row[] 	= '<span class="pull-right">'.number_format($bp->total_stok_pusat,'0',',','.').'</span>';
				$row[] 	= '<span class="pull-right">'.number_format($bp->harga_eceran_pusat,'0',',','.').'</span>';
				$row[] 	= '<span class="pull-right">'.number_format($bp->harga_grosir1,'0',',','.').'</span>';
				$row[] 	= '<span class="pull-right">'.number_format($bp->harga_grosir2,'0',',','.').'</span>';
			}

			$row[] 	=	'<a class="btn btn-primary btn-xs" onclick="proses_barang('."'".$bp->sku."'".')">
							<i class="fa fa-check"></i> PILIH
						</a>';
			$data[]	= $row;
		}

		if($tipe_customer == 'MRC'){
			$output = array(	
				"draw"            => $_POST['draw'],
				"recordsTotal"    => $this->penjualan_detail->count_all('vamr4846_toko_mrc.barang_toko_'.$kode_customer.'', $id_master, $kode_customer),
				"recordsFiltered" => $this->penjualan_detail->count_filtered('_get_datatables_query_barang_toko', $id_master, $kode_customer),
				"data"            => $data
			);
		}else{
			$output = array(	
				"draw"            => $_POST['draw'],
				"recordsTotal"    => $this->penjualan_detail->count_all('vamr4846_vama.barang_pusat', $id_master, $kode_customer),
				"recordsFiltered" => $this->penjualan_detail->count_filtered('_get_datatables_query_barang',$id_master, ''),
				"data"            => $data
			);
		}
		echo json_encode($output);
	}

	public function ajax_edit()
	{
		// Cek user acces menu
		$id_pegawai 		= $this->session->userdata('id_pegawai');
		$cek_useraccess 	= $this->useraccess_pusat->cek_access($id_pegawai, '27');
		if($cek_useraccess->act_update == '0' or $cek_useraccess->act_update == '-'){
			redirect(base_url()."penjualan");
			exit();
		}

		$id_penjualan_d 	= $this->input->post('id_penjualan_d');
		$url 				= $this->input->post('url');
		if($id_penjualan_d == ''){
			if($url == ''){
				redirect(base_url()."penjualan");
			}else{
				redirect(base_url().$url);
			}
		}else{
			$data = $this->penjualan_detail->get_penjualan_detail($id_penjualan_d);
			echo json_encode($data);
		}
	}

	public function ajax_verifikasi_hapus_detail()
	{
		$id_penjualan_d = $this->input->post('id_penjualan_d');
		$url            = $this->input->post('url');
		if($id_penjualan_d == ''){
			redirect(base_url().'penjualan');
			exit();
		}

		// Cek user acces menu
		$id_pegawai     = $this->session->userdata('id_pegawai');
		$cek_useraccess = $this->useraccess_pusat->cek_access($id_pegawai, '27');
		if($cek_useraccess->act_delete == '0' or $cek_useraccess->act_delete == '-'){
			echo json_encode(array(		
									'status' => 0,
									'pesan'  =>	"Maaf anda tidak di izinkan, 
												 Untuk menghapus barang penjualan."
							));
			exit();
		}

		$data = $this->penjualan_detail->get_penjualan_detail($id_penjualan_d);
		if($data){
			echo json_encode(array(
				'status' 	=> 1,
				'pesan' 	=>	"	<small>SKU : </small></br>
									<b class='text-dark'>$data->sku </b><br/></br>

									<small>Nama Barang : </small> <br/>
									<b class='text-dark'>$data->nama_barang </b> </br><br/>

									<small>Jumlah Beli : </small></br>
									<h1 class='text-danger'>$data->jumlah_beli </h1>

									<b class='text-danger'>Keterangan batal : </b>
									<textarea name='keterangan_batal' id='keterangan_batal' class='form-control input-md' placeholder='Masukan keterangan batal disini' style='resize: vertical;'></textarea>
							 	",

				'footer'	=> 	"	<button onclick='hapus_penjualan_detail($data->id_penjualan_d)' type='button' 
										class='btn btn-danger waves-effect waves-light' data-dismiss='modal' autofocus>
										Iya, Hapus
									</button>
									<button type='button' class='btn btn-default waves-effect' data-dismiss='modal'>
										Batal
									</button>

							   	"
			));
		}else{
			echo json_encode(array(		
				'status' => 0,
				'pesan'  =>	"Barang tidak ditemukan"
			));
		}
	}

	public function ajax_hapus_detail()
	{
		if($this->input->is_ajax_request()){
			// Cek user acces menu
			$id_pegawai 		= $this->session->userdata('id_pegawai');
			$cek_useraccess 	= $this->useraccess_pusat->cek_access($id_pegawai, '27');
			if($cek_useraccess->act_delete == '0' or $cek_useraccess->act_delete == '-'){
				echo json_encode(array(
									'status'      => 0,
									'judul'       => 'Oops!',
									'pesan'       => 'Maaf anda tidak di izinkan, 
													  Untuk menghapus barang penjualan.',
									'tipe_pesan'  => 'error',
									'gaya_tombol' => 'btn-danger btn-md waves-effect waves-light'
								));
				exit();
			}

			$url                 = $this->input->post('url');
			$id_penjualan_d      = $this->input->post('id_penjualan_d');
			$keterangan_batal    = $this->input->post('keterangan_batal');
			$tanggal             = date('Y-m-d H:i:s');
			$catatan             = $this->clean_tag_input($this->input->post('catatan'));
			$biaya_lain          = $this->input->post('biaya_lain');
			$id_customer_pusat   = $this->input->post('id_customer_pusat');
			$tipe_customer_pusat = $this->input->post('tipe_customer_pusat');

			// Ambil kode toko
			if($tipe_customer_pusat === 'MRC'){
				$data_customer = $this->customer_pusat->get_by_id($id_customer_pusat);
				$kode_toko     = $data_customer->kode_customer_pusat;
			}else{
				$kode_toko = '';
			}

			if($biaya_lain == ''){
				$biaya_lain = '0';
			}

			// Ambil data penjualan detail
			$dt	= $this->penjualan_detail->get_penjualan_detail($id_penjualan_d);
			if($dt){
				$data_penjualan = $this->penjualan_master->get_by_id($dt->id_penjualan_m);
				if($data_penjualan){
					$status_penjualan = $data_penjualan->status_penjualan;
					if($data_penjualan->tipe_customer_pusat === 'MRC'){
						$id_customer_pusat   = $data_penjualan->id_customer_pusat;
						$kode_toko           = $data_penjualan->kode_customer_pusat;
						$tipe_customer_pusat = $data_penjualan->tipe_customer_pusat;
					}
				}else{
					echo json_encode(array(
						'status'      => 0,
						'judul'       => 'Gagal!',
						'pesan'       => 'Data penjualan master tidak ditemukan.',
						'tipe_pesan'  => 'error',
						'gaya_tombol' => 'btn-danger btn-md waves-effect waves-light',
						'url'         => $url
					));
					exit();
				}

				// Validasi stok barang toko, jika tipe customer mrc
				if($tipe_customer_pusat == 'MRC'){
					$barang_toko = $this->barang_toko->get_barang_toko($dt->id_barang_pusat, $kode_toko);
					if($barang_toko){
						$total_stok 	= $barang_toko->total_stok;
						$validasi_stok 	= $total_stok - $dt->jumlah_beli;
						if($validasi_stok < 0){
							if($status_penjualan == 'SELESAI'){
								echo json_encode(array(
									'status'      => 0,
									'judul'       => "Oops!",
									'pesan'       => "".$barang_toko->nama_barang." tidak bisa dihapus,
													 Karna bisa menyebabkan minus sebanyak : ".$validasi_stok."",
									'tipe_pesan'  => 'error',
									'gaya_tombol' => 'btn-danger btn-md waves-effect waves-light',
									'url'         => $url
								));
								exit();
							}
						}
					}else{
						echo json_encode(array(
							'status'      => 0,
							'judul'       => 'Gagal!',
							'pesan'       => 'Barang toko tidak ditemukan.',
							'tipe_pesan'  => 'error',
							'gaya_tombol' => 'btn-danger btn-md waves-effect waves-light',
							'url'         => $url
						));
						exit();
					}
				}

				$id_master 			= $dt->id_penjualan_m;
				$status_penjualan 	= $dt->status_penjualan;
				$hapus 				= $this->penjualan_detail->hapus_penjualan_detail(
										$dt->id_penjualan_d, $dt->id_penjualan_m, $dt->no_penjualan,
										$dt->id_customer_pusat, $dt->tipe_customer_pusat,
										$dt->id_barang_pusat, $dt->modal, $dt->jenis_harga, $dt->harga_satuan,
										$dt->discount, $dt->jumlah_beli, $dt->subtotal,

										$dt->id_pegawai_pembuatan, $dt->id_pegawai_pembaharuan, $id_pegawai,
										$dt->tanggal_pembuatan, $dt->tanggal_pembaharuan, $keterangan_batal,
										$kode_toko, $status_penjualan
									   );
			}else{
				echo json_encode(array(
					'status'      => 0,
					'judul'       => 'Gagal!',
					'pesan'       => 'Barang tidak ditemukan.',
					'tipe_pesan'  => 'error',
					'gaya_tombol' => 'btn-danger btn-md waves-effect waves-light',
					'url'         => $url
				));
				exit();
			}

			if($hapus){
				$data_total_penjualan = $this->penjualan_detail->get_total($id_master);
				if($data_total_penjualan){
					$ttl 			= $data_total_penjualan->total;
					// $ppn 			= $ttl*(10/100);
					$ppn 			= '0';
					$grand_total	= $ttl+$ppn+$biaya_lain;
					$jumlah_barang 	= $data_total_penjualan->jumlah_barang;

					// Update total dan grandtotal di penjualan master
					$master = $this->penjualan_master->update_master(	
						$id_master, $id_pegawai,
						$id_customer_pusat, $tipe_customer_pusat,
						$ttl, $biaya_lain, $ppn, $grand_total, '0', $grand_total,
						$tanggal, $catatan, $status_penjualan
					);
					
					if($jumlah_barang == 0){
						$this->penjualan_master->hapus_penjualan_master($id_master);
						echo json_encode(array(
							'status'      => 0,
							'judul'       => 'Berhasil!',
							'pesan'       => 'Data penjualan barang berhasil dihapus.',
							'tipe_pesan'  => 'success',
							'gaya_tombol' => 'btn-success btn-md waves-effect waves-light',
							'url'         => 'penjualan'
						));
						exit();
					}
				}

				$data_batal = $this->penjualan_master->get_jumlah_batal($id_master);
				if($data_batal){
					$jumlah_barang_batal = $data_batal->jumlah_barang;
				}else{
					$jumlah_barang_batal = '0';
				}

				// Tampilkan informasi hapus
				echo json_encode(array( 
					'status'              => 1,
					'id_pm'               => $id_master,
					'jumlah_barang'       => $jumlah_barang,
					'jumlah_barang_batal' => $jumlah_barang_batal,
					'total'               => $ttl,
					'ppn'                 => $ppn,
					'grand_total'         => $grand_total,
					'pesan'               => "Data penjualan barang berhasil dihapus."
				));
			}else{
				echo json_encode(array(
					'status'      => 0,
					'judul'       => 'Gagal!',
					'pesan'       => 'Barang penjualan gagal dihapus.',
					'tipe_pesan'  => 'error',
					'gaya_tombol' => 'btn-success btn-md waves-effect waves-light',
					'url'         => $url
				));
			}
		}else{
			redirect(base_url().'penjualan');
		}
	}

	public function ajax_cari_barang_untuk_penjualan()
	{
		if($this->input->is_ajax_request()){
			// Cek user acces menu
			$id_pegawai     = $this->session->userdata('id_pegawai');
			$cek_useraccess = $this->useraccess_pusat->cek_access($id_pegawai, '27');
			if($cek_useraccess->act_create == '1'){
				$id_pm         = $this->input->post('id_penjualan_m');
				$sku           = $this->input->post('sku');
				$tipe_customer = $this->input->post('tipe_customer');
				$kode_toko     = $this->input->post('kode_toko');

				$data_penjualan = $this->penjualan_master->get_by_id($id_pm);
				if($data_penjualan){
					if($data_penjualan->tipe_customer_pusat === 'MRC'){
						$kode_toko     = $data_penjualan->kode_customer_pusat;
						$tipe_customer = $data_penjualan->tipe_customer_pusat;
					}
				}

				if($tipe_customer == 'MRC'){
					$data_barang = $this->barang_toko->cari_barang_toko($sku, '', $kode_toko);
					if($data_barang->num_rows() <= 0){
						$data_barang = $this->barang_pusat->ambil_barang_untuk_penjualan($sku, 'eceran');
					}
				}else{
					if($tipe_customer == '' OR $tipe_customer =='-' OR $tipe_customer =='ECERAN'){
						$jenis_harga = 'eceran';
					}else if($tipe_customer == 'BENGKEL'){
						$jenis_harga = 'grosir1';
					}else if($tipe_customer == 'GROSIR'){
						$jenis_harga = 'grosir2';
					}
					$data_barang = $this->barang_pusat->ambil_barang_untuk_penjualan($sku, $jenis_harga);
				}

				if($data_barang->num_rows() > 0){
					$json['status'] = 1;
					$json['data']   = $data_barang->row();
				}else{
					$json['status'] = 0;
					$json['pesan']  = "SKU : $sku
									   Tidak terdaftar.";
				}
			}else{
				$json['status'] = 0;
				$json['pesan']  = "Maaf anda tidak diizinkan untuk menambahkan barang penjualan.";
			}
			echo json_encode($json);
		}else{
			redirect(base_url().'penjualan');
			exit();
		}
	}

	public function ajax_pulihkan_data_detail()
	{
		if($this->input->is_ajax_request()){
			// Cek user acces menu
			$id_pegawai     = $this->session->userdata('id_pegawai');
			$cek_useraccess = $this->useraccess_pusat->cek_access($id_pegawai, '27');
			if($cek_useraccess->act_create == '1' or $cek_useraccess->act_update == '1'){
				$id_penjualan_b = $this->input->post('id_penjualan_b');
				$data_barang    = $this->penjualan_detail->get_penjualan_batal($id_penjualan_b);

				if($data_barang){
					$json['status'] = 1;
					$json['data']   = $data_barang;
				}else{
					$json['status'] = 0;
					$json['pesan']  = "Data penjualan barang tidak ditemukan.";
				}
				echo json_encode($json);
			}else{
				$json['status'] = 0;
				$json['pesan']  = "Anda tidak dizinkan untuk memulihkan data";
				echo json_encode($json);
			}
		}else{
			redirect(base_url().'penjualan');
			exit();
		}
	}

	public function ajax_kode()
	{
		if($this->input->is_ajax_request()){
			// Cek user acces menu
			$id_pegawai     = $this->session->userdata('id_pegawai');
			$cek_useraccess = $this->useraccess_pusat->cek_access($id_pegawai, '27');
			if($cek_useraccess->act_create == '1' or $cek_useraccess->act_update == '1'){
				$keyword       = $this->input->post('keyword');
				$id_pm         = $this->input->post('id_pm');
				$kode_toko     = $this->input->post('kode_toko');
				$tipe_customer = $this->input->post('tipe_customer');

				$data_penjualan = $this->penjualan_master->get_by_id($id_pm);
				if($data_penjualan){
					if($data_penjualan->tipe_customer_pusat === 'MRC'){
						$kode_toko     = $data_penjualan->kode_customer_pusat;
						$tipe_customer = $data_penjualan->tipe_customer_pusat;
					}
				}

				if($tipe_customer === 'MRC'){
					$barang = $this->barang_toko->cari_barang_toko($keyword, $id_pm, $kode_toko);
				}else{
					$barang = $this->barang_pusat->cari_kode($keyword, $id_pm);
				}

				if($barang->num_rows() > 0){
					$json['status'] 	= 1;
					$json['datanya'] 	= "<ul id='daftar-autocomplete' class='list-group user-list'>";

					foreach($barang->result() as $b){
						if($b->foto!=""){
							$json['datanya'] .= "
				                <li class='user-list-item'>
				                	<div class='avatar avatar-sm-box'>
	                                    <img src='assets/upload/image/barang/thumbs/$b->foto'>
				                	</div>";
				        }else{
							$nama_barang 	  = $b->nama_barang;
							$inisial 		  = substr($nama_barang, 0, 1);
							$json['datanya'] .= "
				                <li class='user-list-item'>
				                	<div class='avatar avatar-sm-box'>
	                                    <span class='avatar-sm-box bg-info'>".$inisial."</span>
				                	</div>";
						}

						if($tipe_customer === 'MRC'){
							$json['datanya'] .= "
				                	<div class='user-desc'>
				                		<span id='id_barangnya' style='display: none;'>".$b->id_barang_pusat."</span>
										<span id='barangnya' class='name'><b>".$b->nama_barang."</b></span>
										<span id='skunya' class='name'>".$b->sku."</span>
										<span id='total_stok' class='name'>Stok Toko : ".$b->total_stok_toko."</span>
										<span id='total_stok' class='name'>Stok Pusat : ".$b->total_stok_pusat."</span>
										<span id='harga_eceran_toko' class='name'>Eceran Toko : Rp. ".$b->eceran_toko."</span>
										<span id='eceran_toko' style='display: none;'>".$b->harga_eceran_toko."</span>
										<span id='harga_eceran_pusat' class='name'>Eceran Pusat : Rp. ".$b->eceran_pusat."</span>
									</div>
								</li>
							";
						}else{
							$json['datanya'] .= "
				                	<div class='user-desc'>
				                		<span id='id_barangnya' style='display: none;'>".$b->id_barang_pusat."</span>
										<span id='barangnya' class='name'><b>".$b->nama_barang."</b></span>
										<span id='skunya' class='name'>".$b->sku."</span>
										<span id='total_stok' class='name'>Stok : ".$b->total_stok."</span>
										<span id='harga_eceran' class='name'>EC : Rp. ".$b->eceran."</span>
										<span id='harga_grosir1' class='name'>G1 : Rp. ".$b->grosir1."</span>
										<span id='harga_grosir2' class='name'>G2 : Rp. ".$b->grosir2."</span>
										<span id='eceran' style='display:none;'>".$b->harga_eceran."</span>
										<span id='grosir1' style='display:none;'>".$b->harga_grosir1."</span>
										<span id='grosir2' style='display:none;'>".$b->harga_grosir2."</span>
									</div>
								</li>
							";
						}
					}

					$json['datanya'] .= "</ul>";
				}else{
					$json['status'] 	= 0;
					$json['pesan'] 		= "Tidak ada barang yang ditemukan";
				}
				echo json_encode($json);
			}else{
				$json['status'] 	= 0;
				$json['pesan'] 		= "Anda tidak dizinkan untuk mencari data";
				echo json_encode($json);
			}
		}else{
			redirect(base_url().'penjualan');
			exit();
		}
	}

	public function ajax_pencarian_customer()
	{
		if($this->input->is_ajax_request()){
			// Cek user acces menu
			$id_pegawai 			= $this->session->userdata('id_pegawai');
			$cek_useraccess 		= $this->useraccess_pusat->cek_access($id_pegawai, '27');
			if($cek_useraccess->act_create == '1' or $cek_useraccess->act_update == '1'){
				$id_penjualan_m 	= $this->input->post('id_penjualan_m');
				$data_penjualan 	= $this->penjualan_master->get_by_id($id_penjualan_m);
				if($data_penjualan){
					if($data_penjualan->tipe_customer_pusat === 'MRC'){
						$json['status']                   = 1;
						$json['id_customer_pusat']        = $data_penjualan->id_customer_pusat;
						$json['nama_customer_pusat']      = $data_penjualan->nama_customer_pusat;
						$json['kode_customer_pusat']      = $data_penjualan->kode_customer_pusat;
						$json['tipe_customer_pusat']      = $data_penjualan->tipe_customer_pusat;
						$json['handphone_customer_pusat'] = $data_penjualan->handphone1;
						echo json_encode($json);
						exit();
					}
				}

				if($data_penjualan){
					$tipe_customer_pusat = 'MRC';
				}else{
					$tipe_customer_pusat = '';
				}

				$keyword 			= $this->input->post('keyword');
				$customer 			= $this->customer_pusat->cari_customer($keyword, $tipe_customer_pusat);

				if($customer->num_rows() > 0){
					$json['status'] 	= 1;
					$json['datanya'] 	= "<ul id='daftar_autocompleted_customer' class='user-list'>";
					foreach($customer->result() as $c){
						if($c->foto!=""){
						$json['datanya'] .= "
							                <li class='user-list-item'>
							                	<div class='avatar avatar-sm-box'>
					                                <img src='assets/upload/image/customer_pusat/thumbs/$c->foto'>
							                	</div>";
				        }else{
						$nama_customer 	  = $c->nama_customer_pusat;
						$inisial 		  = substr($nama_customer, 0, 1);
						$json['datanya'] .= "
							                <li class='user-list-item'>
							                	<div class='avatar avatar-sm-box'>
				                                    <span class='avatar-sm-box bg-info'>".$inisial."</span>
							                	</div>";
						}

						$json['datanya'] .= "
							                	<div class='user-desc'>
													<span id='id_customer' style='display:none;'>".$c->id_customer_pusat."</span>
													<span id='kode_customer' style='display:none;'>".$c->kode_customer_pusat."</span>
													<span id='nama_customer' class='name'>".$c->nama_customer_pusat."</span>
													<span id='email_customer' class='name'>".$c->email."</span>
													<span id='hp_customer' class='name'>".$c->handphone1."</span>
													<span id='tipe_customer' class='desc' style='display:none;'>".$c->tipe_customer_pusat."</span>
												</div>
											</li>
											";
					}
					$json['datanya'] .= "</ul>";
				}else{
					$json['status'] 	= 0;
					$json['pesan'] 		= "Anda tidak dizinkan untuk mencari data";
				}
				echo json_encode($json);
			}else{
				$json['status'] 	= 0;
				$json['pesan'] 		= "Tidak ada customer yang ditemukan";
				echo json_encode($json);
			}
		}else{
			redirect(base_url().'penjualan');
			exit();
		}
	}

	public function ajax_pencarian_harga()
	{
		if($this->input->is_ajax_request()){
			$id_pegawai 	= $this->session->userdata('id_pegawai');
			$sku 			= $this->input->post('sku');
			$barang_pusat 	= $this->barang_pusat->cari_harga_barang_pusat($sku);
			$b 				= $barang_pusat;
			if($b){
				$json['status'] 	= 1;
				$json['datanya'] 	= "<ul id='daftar_autocompleted_harga' class='list-group'>";
				$json['datanya'] .= "
					                <li class='list-group-item'>
					                	EC : Rp.
										<span id='harga' class='text-dark text-right'>".$b->eceran."</span>
										<span id='harga_hidden' style='display:none;'>".$b->harga_eceran."</span>
										<span id='jenis' style='display:none;'>ECERAN</span>
									</li>
					                <li class='list-group-item'>
					                	G1 : Rp.
										<span id='harga' class='text-dark text-right'>".$b->grosir1."</span>
										<span id='harga_hidden' style='display:none;'>".$b->harga_grosir1."</span>
										<span id='jenis' style='display:none;'>GROSIR 1</span>
									</li>
					                <li class='list-group-item'>
					                	G2 : Rp.
										<span id='harga' class='text-dark text-right'>".$b->grosir2."</span>
										<span id='harga_hidden' style='display:none;'>".$b->harga_grosir2."</span>
										<span id='jenis' style='display:none;'>GROSIR 2</span>
									</li>
									";

				$data_pegawai = $this->pegawai_pusat->get_by_id($id_pegawai);
				if($data_pegawai->usergroup_name == 'Super Admin'){
					$json['datanya'] .= "<li class='list-group-item'>
						                	<a class='btn btn-rounded btn-default btn-xs'>
						                		<span id='jenis' style='display:none;'>KHUSUS</span>
						                		<i class='fa fa-pencil' style='color:blue;'></i>
						                		Harga Khusus
						                	</a>
										</li>";
				}

				$json['datanya'] .= "</ul>";
			}else{
				$json['status'] = 0;
			}

			echo json_encode($json);
		}else{
			redirect(base_url().'penjualan');
			exit();
		}
	}

	public function form_customer()
	{
		$id_pegawai     = $this->session->userdata('id_pegawai');
		$cek_useraccess = $this->useraccess_pusat->cek_access($id_pegawai, '11');
		if($cek_useraccess->act_create == '0' or $cek_useraccess->act_create == '-'){
			echo json_encode(array(	
				"status" => 0,
				"pesan"  =>	"Anda tidak di izinkan untuk menambahkan data customer"
			));
			// redirect(base_url().'penjualan');
			exit();
		}

		$id_penjualan_m = $this->input->post('id_penjualan_m');
		if($id_penjualan_m == ''){
			redirect(base_url().'penjualan');
			exit();
		}

		$data_penjualan = $this->penjualan_master->get_by_id($id_penjualan_m);
		if($data_penjualan){
			if($data_penjualan->tipe_customer_pusat === 'MRC'){
				echo json_encode(array(	
					"status"	=> 0,
					"pesan"		=> "Anda tidak bisa menambahkan data customer baru,
									Disaat sedang membuka transaksi penjualan ke MRC"
				));
				exit();
			}
		}
		echo json_encode(array("status" => 1));
	}

	public function inputan_customer(){
		$id_pegawai     = $this->session->userdata('id_pegawai');
		$cek_useraccess = $this->useraccess_pusat->cek_access($id_pegawai, '11');
		if($cek_useraccess->act_create == '0' or $cek_useraccess->act_create == '-'){
			echo json_encode(array(	
				"status" => 0,
				"pesan"  =>	"Anda tidak di izinkan untuk menambahkan data customer"
			));
			redirect(base_url().'penjualan');
			exit();
		}

		$data['list_sumber']     = $this->referensi_sumber_online->listing();
		$data['access_customer'] = $cek_useraccess->act_create;
		$this->load->view('admin/transaksi/penjualan/form_customer', $data);
	}

	public function tambah_customer()
	{
		// Cek user acces menu
		$id_pegawai 	= $this->session->userdata('id_pegawai');
		$cek_useraccess = $this->useraccess_pusat->cek_access($id_pegawai, '11');
		if($cek_useraccess->act_create == '0' or $cek_useraccess->act_create == '-'){
			echo json_encode(array("status" => 1));
			exit();
		}

		$tipe_customer_pusat = $this->input->post('tipe_customer_pusat');
		if($tipe_customer_pusat == ''){
			echo json_encode(array("status" => 1));
			exit();
		}else if($tipe_customer_pusat == 'MRC'){
			$akhir   = $this->customer_pusat->akhir_toko();
			$inisial = "MRC";
		}else{
			$akhir   = $this->customer_pusat->akhir_umum();
			$inisial = "CSP";
		}

		$this->_validate();

		// Buat kode customer
		$jumlah_customer	= $akhir->jumlah_customer;
		if($jumlah_customer < 10){
			if($tipe_customer_pusat == 'MRC'){
				$depan = '00';
			}else{
				$depan = '000';
			}
		}elseif($jumlah_customer < 100){
			if($tipe_customer_pusat == 'MRC'){
				$depan = '0';
			}else{
				$depan = '00';
			}
		}elseif($jumlah_customer < 1000){
			if($tipe_customer_pusat == 'MRC'){
				$depan = '';
			}else{
				$depan = '0';
			}
		}elseif($jumlah_customer < 10000){
			if($tipe_customer_pusat == 'MRC'){
				$depan = '';
			}else{
				$depan = '';
			}
		}

		$kode_customer_pusat 	= $inisial.$depan.$jumlah_customer;
		// // Akhir buat kode baru

		$this->_validate();
		$data = array(	
			'kode_customer_pusat'   => $kode_customer_pusat,
			'nama_customer_pusat'   => $this->input->post('nama_customer_pusat'),
			'no_siup'               => $this->input->post('no_siup'),
			'alamat_customer_pusat' => $this->input->post('alamat_customer_pusat'),
			'asal_customer_pusat'   => $this->input->post('asal_customer_pusat'),
			'id_referensi'          => $this->input->post('sumber_online'),
			'tipe_customer_pusat'   => $tipe_customer_pusat,
			'kontak_pribadi'        => $this->input->post('kontak_pribadi'),
			'jabatan'               => $this->input->post('jabatan'),
			'pin_bb1'               => $this->input->post('pin_bb1'),
			'pin_bb2'               => $this->input->post('pin_bb2'),
			'telephone1'            => $this->input->post('telephone1'),
			'telephone2'            => $this->input->post('telephone2'),
			'handphone1'            => $this->input->post('handphone1'),
			'handphone2'            => $this->input->post('handphone2'),
			'fax'                   => $this->input->post('fax'),
			'email'                 => $this->input->post('email'),
			'id_pegawai_pembuatan'  => $this->session->userdata('id_pegawai'),
			'tanggal_pembuatan'     => date('Y-m-d H:i:s')
		);

		$insert = $this->customer_pusat->save($data);
		if($insert){
			$email                 = $this->input->post('email');
			$nama_customer_pusat   = $this->input->post('nama_customer_pusat');
			$tipe_customer_pusat   = $this->input->post('tipe_customer_pusat');
			$handphone1            = $this->input->post('handphone1');
			$alamat_customer_pusat = $this->input->post('alamat_customer_pusat');
			$id_customer_pusat     = $this->customer_pusat->get_customer_by_email($email)->row()->id_customer_pusat;
			echo json_encode(array(
				'status'                => 2,
				'pesan'                 => "".$nama_customer_pusat." berhasil ditambahkan sebagai customer baru.",
				'id_customer_pusat'     => $id_customer_pusat,
				'nama_customer_pusat'   => $nama_customer_pusat,
				'tipe_customer_pusat'   => $tipe_customer_pusat,
				'alamat_customer_pusat' => preg_replace("/\r\n|\r|\n/",'<br />', $alamat_customer_pusat),
				'handphone1'            => $handphone1,
				'email'                 => $email
			));
		}else{
			$this->query_error();
		}
	}

	public function form_input_debit()
	{
		$level = $this->session->userdata('usergroup_name');
		if($level == 'Admin' OR $level == 'User' OR $level == 'Super Admin'){
			$dt['list_edc']		= $this->kartu_edc_pusat->listing();
			$this->load->view('admin/transaksi/penjualan/form_input_debit',$dt);
		}
	}

	public function ajax_list_debit()
	{
		$no_penjualan          = $this->clean_tag_input($this->input->post('no_penjualan'));
		$data_penjualan_master = $this->penjualan_master->get_id($no_penjualan);

		if($data_penjualan_master){
			$id_master = $data_penjualan_master->id_penjualan_m;
		}else{
			$id_master = '0';
		}

		$list = $this->penjualan_detail_kartu->get_datatables($id_master);
		$data = array();
		$no   = $_POST['start'];

		foreach ($list as $pdk){
			$no++;
			$row = array();
			$row[] = $no;

			//add html for action
			$row[] =	'<a class="btn btn-rounded btn-default btn-xs"
							onclick="edit_penjualan_debit('."'".$pdk->id_penjualan_debit."'".')">
							<i class="fa fa-pencil" style="color:blue;"></i>
						</a>
				  		<a class="btn btn-rounded btn-default btn-xs"
				  			onclick="hapus_penjualan_detail_debit('."'".$pdk->id_penjualan_debit."'".')">
				  			<i class="fa fa-times" style="color:red;"></i>
				  		</a>';

			$row[] 	= $pdk->nama_bank;
			$row[] 	= $pdk->no_kartu_customer;

			$row[] 	= '<span class="pull-right">'.number_format($pdk->jumlah_pembayaran,'0',',','.').'</span>';
			$row[] 	= $pdk->tanggal_pembaharuan;
			$data[]	= $row;
		}

		$output = array( 	
			'no_penjualan'    => $no_penjualan,
			'id_master'       => $id_master,
			'draw'            => $_POST['draw'],
			'recordsTotal'    => $this->penjualan_detail_kartu->count_all($id_master),
			'recordsFiltered' => $this->penjualan_detail_kartu->count_filtered($id_master),
			'data'            => $data
		);
		echo json_encode($output);
	}

	public function simpan_penjualan_debit()
	{
		$this->_validate_debit();
		$id_penjualan_m       = $this->input->post('id_penjualan_m');
		$id_kartu_edc_pusat   = $this->input->post('id_kartu_edc_pusat');
		$nama_kartu_edc_pusat = $this->input->post('nama_kartu_edc_pusat');
		$no_kartu_customer    = $this->input->post('no_kartu_customer');
		$jumlah_pembayaran    = $this->input->post('jumlah_pembayaran');
		$grand_total          = $this->input->post('grandtotal');
		$id_pegawai           = $this->session->userdata('id_pegawai');

		// Validasi penjualan debit sudah ada atau belum
		$data_penjualan_debit = $this->penjualan_detail_kartu->get_id($id_penjualan_m, $id_kartu_edc_pusat);
		if ($data_penjualan_debit){
			$id_penjualan_debit = $data_penjualan_debit->id_penjualan_debit;
		}else{
			$id_penjualan_debit	= '';
		}

		if($id_penjualan_debit){
			$master = $this->penjualan_detail_kartu->update_detail_debit(
				$id_penjualan_m, $id_penjualan_debit,
				$id_kartu_edc_pusat, $no_kartu_customer,
				$jumlah_pembayaran,$id_pegawai
	   		);
		}else{
			// Insert penjualan detail debit
			$master = $this->penjualan_detail_kartu->insert_detail_debit(
				$id_penjualan_m, $id_kartu_edc_pusat,
				$no_kartu_customer, $jumlah_pembayaran,$id_pegawai
	  		);
		}

		// Ambil total pembayaran debit
		$data_total_debit 	= $this->penjualan_detail_kartu->get_total($id_penjualan_m);
		if($data_total_debit){
			$total_debit 	= $data_total_debit->total_pembayaran;
			$total_tunai 	= $grand_total-$total_debit;

			if($total_tunai < 0){
				$total_tunai = '0';
			}

			// update total debit di penjualan master
			$master = $this->penjualan_master->update_debit( 	
				$id_penjualan_m, $id_pegawai,
				$total_debit, $total_tunai
			);
		}else{
			$total_debit 	= '0';
			$total_tunai 	= $grand_total;

			// update total debit di penjualan master
			$master = $this->penjualan_master->update_debit( 	
				$id_penjualan_m, $id_pegawai,
				$total_debit, $total_tunai
			);
			$this->query_error();
		}
		echo json_encode(array(		
			'status'      => TRUE,
			'total_debit' => $total_debit,
			'total_tunai' => $total_tunai
		));
	}

	public function ajax_edit_debit($id_penjualan_debit)
	{
		$data = $this->penjualan_detail_kartu->get_penjualan_detail_debit($id_penjualan_debit);
		echo json_encode($data);
	}

	public function ajax_hapus_detail_debit($id_penjualan_debit)
	{
		if($this->input->is_ajax_request()){
			$id_penjualan_m = $this->input->post('id_penjualan_m');
			$grand_total    = $this->input->post('grandtotal');
			$id_pegawai     = $this->session->userdata('id_pegawai');

			// Ambil data penjualan detail
			$hapus = $this->penjualan_detail_kartu->hapus_debit($id_penjualan_debit);
			if($hapus){
				// Ambil total pembayaran debit
				$data_total_debit = $this->penjualan_detail_kartu->get_total($id_penjualan_m);
				if($data_total_debit){
					$total_debit = $data_total_debit->total_pembayaran;
					$total_tunai = $grand_total-$total_debit;

					if ($total_tunai < 0){
						$total_tunai = '0';
					}

					// update total debit di penjualan master
					$master = $this->penjualan_master->update_debit(
						$id_penjualan_m, $id_pegawai,
						$total_debit, $total_tunai
					);
				}else{
					$total_debit = '0';
					$total_tunai = $grand_total;

					// update total debit di penjualan master
					$master = $this->penjualan_master->update_debit(
								$id_penjualan_m, $id_pegawai,
								$total_debit, $total_tunai
							);
					$this->query_error();
				}

				echo json_encode(array(
					'status'      => 1,
					'total_debit' => $total_debit,
					'total_tunai' => $total_tunai
				));
			}else{
				echo json_encode(array('status' => 0));
			}
		}else{
			redirect(base_url().'penjualan');
			exit();
		}
	}

	private function _validate()
	{
		$data = array();
		$data['error_string'] = array();
		$data['inputerror']   = array();
		$data['status']       = TRUE;
		$nama_customer_pusat  = $this->input->post('nama_customer_pusat');
		$email                = $this->input->post('email');
		$handphone1           = $this->input->post('handphone1');
		$handphone2           = $this->input->post('handphone2');

		if($this->input->post('alamat_customer_pusat') == ''){
			$data['inputerror'][] 	= 'alamat_customer_pusat';
			$data['error_string'][] = 'Alamat customer pusat wajib diisi';
			$data['status'] 		= 0;
		}

		// if($handphone1 == ''){
		// 	$data['inputerror'][] 	= 'handphone1';
		// 	$data['error_string'][] = 'Handphone-1 customer pusat wajib diisi';
		// 	$data['status'] 		= 0;
		// }

		// $validasi_handphone1 = $this->customer_pusat->get_by_handphone1($handphone1);
		// if($validasi_handphone1){
		// 	$data['inputerror'][] 	= 'handphone1';
		// 	$data['error_string'][] = 'No. handphone customer pusat sudah digunakan, harap ganti no. handphone customer';
		// 	$data['status'] 		= 0;
		// }

		// $validasi_handphone2 = $this->customer_pusat->get_by_handphone1($handphone2);
		// if($validasi_handphone2){
		// 	$data['inputerror'][] 	= 'handphone2';
		// 	$data['error_string'][] = 'No. handphone customer pusat sudah digunakan, harap ganti no. handphone customer';
		// 	$data['status'] 		= 0;
		// }

	    if($nama_customer_pusat == ''){
			$data['inputerror'][] 	= 'nama_customer_pusat';
			$data['error_string'][] = 'Nama customer pusat wajib diisi';
			$data['status'] 		= 0;
		}

		$validasi_nama = $this->customer_pusat->get_by_nama($nama_customer_pusat);
		if($validasi_nama){
			$data['inputerror'][] 	= 'nama_customer_pusat';
			$data['error_string'][] = 'Nama customer pusat sudah digunakan, harap ganti nama customer';
			$data['status'] 		= 0;
		}

		// if($email == ''){
		// 	$data['inputerror'][] 	= 'email';
		// 	$data['error_string'][] = 'Email customer pusat wajib diisi';
		// 	$data['status'] 		= 0;
		// }

		// $validasi_email = $this->customer_pusat->get_by_email($email);
		// if($validasi_email){
		// 	$data['inputerror'][] 	= 'email';
		// 	$data['error_string'][] = 'Email customer pusat sudah digunakan, harap ganti email customer';
		// 	$data['status'] 		= 0;
		// }

		if($data['status'] === 0){
			echo json_encode($data);
			exit();
		}
	}

	private function _validate_debit()
	{
		$data                 = array();
		$data['error_string'] = array();
		$data['inputerror']   = array();
		$data['status']       = TRUE;

		if($this->input->post('id_kartu_edc_pusat') == ''){
			$data['inputerror'][] = 'kartu_edc_pusat';
			$data['error_string'][] = 'Harap pilih kartu edc yang di ingin digunakan';
			$data['status'] = FALSE;
		}

		if($this->input->post('no_kartu_customer') == ''){
			$data['inputerror'][] = 'no_kartu_customer';
			$data['error_string'][] = 'No. kartu customer wajib diisi';
			$data['status'] = FALSE;
		}

		if($this->input->post('jumlah_pembayaran') == '' or $this->input->post('jumlah_pembayaran') == '0'){
			$data['inputerror'][] = 'pembayaran_debit';
			$data['error_string'][] = 'Jumlah pembayaran wajib diisi';
			$data['status'] = FALSE;
		}

		if($data['status'] === FALSE){
			echo json_encode($data);
			exit();
		}
	}
}
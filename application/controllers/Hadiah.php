<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Hadiah extends MY_Controller 
{
	function __construct()
	{
		parent::__construct();
		$this->CI =& get_instance();
		$this->load->model('pegawai_pusat_model','pegawai_pusat');
        $this->load->model('useraccess_pusat_model','useraccess_pusat');
		$this->load->model('customer_pusat_model','customer_pusat');
		$this->load->model('barang_pusat_model','barang_pusat');
		$this->load->model('pegawai_pusat_model','pegawai_pusat');
		$this->load->model('hadiah_master_model','hadiah_master');
		$this->load->model('hadiah_detail_model','hadiah_detail');
	}

	public function index()
	{
		// Cek user acces menu
		$id_pegawai   = $this->session->userdata('id_pegawai');
		if($id_pegawai == ''){
			redirect(base_url().'dashboard');
			exit();
		}

		$data_pegawai = $this->pegawai_pusat->get_by_id($id_pegawai);
		if($data_pegawai){
			$cek_useraccess              = $this->useraccess_pusat->cek_access($id_pegawai, '31');
			$cek_useraccess_laporan      = $this->useraccess_pusat->cek_access($id_pegawai, '46');
			$cek_useraccess_customer     = $this->useraccess_pusat->cek_access($id_pegawai, '11');
			if($cek_useraccess){
				if($cek_useraccess->act_read == '0' or $cek_useraccess->act_read == '-'){
					redirect(base_url().'dashboard');
				}					
			}else{
				redirect(base_url().'dashboard');
			}
			
			$data['data_pegawai']        = $data_pegawai;
			$data['data_induk_menu']     = $this->useraccess_pusat->get_induk_menu($id_pegawai);
			$data['access_create']       = $cek_useraccess->act_create;
			$data['access_update']       = $cek_useraccess->act_update;
			$data['access_laporan']      = $cek_useraccess_laporan->act_read;
			$data['access_customer']     = $cek_useraccess_customer->act_create;

			$data['atribut_halaman']     = 'Hadiah';
			$data['list_customer_pusat'] = $this->customer_pusat->listing();
			$data['id_hadiah_m']         = '0';
			$data['halaman_transaksi']   = $this->load->view('admin/transaksi/hadiah/transaksi',$data,true);
			$data['halaman_plugin']      = $this->load->view('admin/transaksi/hadiah/plugin',$data,true);
			$data['isi_halaman']         = $this->load->view('admin/transaksi/hadiah/index',$data,true);
			$this->load->view('admin/layout',$data);		
		}else{
			redirect(base_url().'dashboard');
		}
	}

	function no_hadiah_baru(){
		// Rancang no. hadiah baru
		$id_pegawai       = $this->session->userdata('id_pegawai');
		$tanggal_sekarang = date('Y-m-d');
		$bulan_sekarang   = date('m');
		$tahun_sekarang   = date('Y');
		$tahun_sekarang_2 = date('y');
		$akhir            = $this->hadiah_master->no_hadiah_baru($tahun_sekarang, $bulan_sekarang, $id_pegawai);
		$inisial          = "HD";
	    
	    if($akhir) {
			$no_baru = $akhir->no_baru;
	    }else{
			$no_baru = 1;
	    }

	    if($no_baru < 10){
			$depan = '000';
	    }elseif($no_baru < 100) {
			$depan = '00';
	    }elseif($no_baru < 1000) {
			$depan = '0';
	    }else{
			$depan = '';
	    }

		$no_hadiah_baru = $inisial.'-'.$bulan_sekarang.$tahun_sekarang_2.'-'.$depan.$no_baru;
		return $no_hadiah_baru;
	}

	public function transaksi()
	{
		$id_hadiah_m = $this->input->get('id');
		$data_hadiah = $this->hadiah_master->get_master($id_hadiah_m);
		if($data_hadiah->id_hadiah_m == ''){
			redirect(base_url().'hadiah');
			exit();
		}

		// Cek user acces menu
		$id_pegawai              = $this->session->userdata('id_pegawai');
		$cek_useraccess_laporan  = $this->useraccess_pusat->cek_access($id_pegawai, '46');
		$cek_useraccess_customer = $this->useraccess_pusat->cek_access($id_pegawai, '11');
		if($cek_useraccess_laporan->act_update == '1' or $data_hadiah->status_hadiah == 'MENUNGGU'
			or $data_hadiah->status_hadiah == 'DISIAPKAN' or $data_hadiah->status_hadiah == 'DIKIRIM'){
			$cek_useraccess              = $this->useraccess_pusat->cek_access($id_pegawai, '31');
			$data['access_laporan']      = $cek_useraccess_laporan->act_read;
			$data['access_customer']     = $cek_useraccess_customer->act_create;
			$data['access_create']       = $cek_useraccess->act_create;
			$data['access_update']       = $cek_useraccess->act_update;
									
			// cari data pegawai
			$data_pegawai                = $this->pegawai_pusat->get_by_id($id_pegawai);
			$this->CI->session->set_userdata('usergroup_name', $data_pegawai->usergroup_name);
			$data['data_pegawai']        = $data_pegawai;
			$data['data_induk_menu']     = $this->useraccess_pusat->get_induk_menu($id_pegawai);
			
			$data['atribut_halaman']     = 'Penjualan Pusat';
			$data['id_hadiah_m']         = $id_hadiah_m;
			$data['no_hadiah']           = $data_hadiah->no_hadiah;
			$data['list_customer_pusat'] = $this->customer_pusat->listing();	
			$data['halaman_transaksi']   = $this->load->view('admin/transaksi/hadiah/transaksi',$data,true);
			$data['halaman_plugin']      = $this->load->view('admin/transaksi/hadiah/plugin',$data,true);
			$data['isi_halaman']         = $this->load->view('admin/transaksi/hadiah/index',$data,true);
			$this->load->view('admin/layout',$data);
		}else{
			redirect(base_url().'hadiah');
			exit();
		}
	}

	public function simpan_transaksi()
	{
		if(!empty($_POST['id_hadiah_m'])){
			$id_pegawai     = $this->session->userdata('id_pegawai');
			$cek_useraccess = $this->useraccess_pusat->cek_access($id_pegawai, '31');
			if($cek_useraccess->act_create == '1' or $cek_useraccess->act_update == '1'){
				$id_hadiah_m        = $this->input->post('id_hadiah_m');
				$data_hadiah_master = $this->hadiah_master->get_by_id($id_hadiah_m);
				if($data_hadiah_master){
					$no_hadiah         = $data_hadiah_master->no_hadiah;
					$nama_hadiah       = $this->input->post('nama_hadiah');
					$tanggal           = date('Y-m-d H:i:s');
					$id_pegawai        = $this->session->userdata('id_pegawai');
					$id_customer_pusat = $this->input->post('id_customer_pusat');
					$catatan           = $this->clean_tag_input($this->input->post('catatan'));
					$total             = $this->input->post('total');

					// Update hadiah master
					$data_total = $this->hadiah_detail->get_total($id_hadiah_m);
					if($data_total){
						$total = $data_total->total;
					}else{
						$total = '0';
					}

					$master = $this->hadiah_master->update_master( 
						$id_hadiah_m, $nama_hadiah, $id_pegawai, $id_customer_pusat, 
						$total, $tanggal, $catatan, 'SELESAI'
					);

					if($master){
						echo json_encode(array(
							'status' => 1, 
							'pesan'  => "Transaksi berhasil disimpan,
							   			 No. Hadiah : ".$no_hadiah.",
							   			 Nama Hadiah : ".$nama_hadiah.",
							   			 Apakah anda ingin mencetak fakturnya ?"
						));
						exit();
					}else{
						echo json_encode(array(
							'status' => 0, 
							'pesan'  => "Gagal menyimpan transaksi hadiah,
										 Harap coba lagi ?"
						));
						exit();
					}
				}else{
					echo json_encode(array(
						'status' => 0, 
						'pesan'  => "Gagal menyimpan transaksi hadiah,
									 Harap coba lagi ?"
					));
					exit();
				}
			}else{
				echo json_encode(array(	
					'status' => 0,
					'pesan'  => 'Maaf anda tidak diizinkan untuk menyimpan transaksi hadiah.'
				));
				exit();
			}
		}else{
			redirect(base_url().'hadiah');
		}
	}

	public function tahan_transaksi()
	{
		if(!empty($_POST['id_hadiah_m'])){
			$id_pegawai     = $this->session->userdata('id_pegawai');
			$cek_useraccess = $this->useraccess_pusat->cek_access($id_pegawai, '31');
			if($cek_useraccess->act_create == '1' or $cek_useraccess->act_update == '1'){
				$id_hadiah_m        = $this->input->post('id_hadiah_m');
				$data_hadiah_master = $this->hadiah_master->get_by_id($id_hadiah_m);
				if($data_hadiah_master){
					$no_hadiah         = $data_hadiah_master->no_hadiah;
					$nama_hadiah       = $this->input->post('nama_hadiah');
					$tanggal           = date('Y-m-d H:i:s');
					$id_pegawai        = $this->session->userdata('id_pegawai');
					$id_customer_pusat = $this->input->post('id_customer_pusat');
					$catatan           = $this->clean_tag_input($this->input->post('catatan'));
					$total             = $this->input->post('total');

					// Update hadiah master
					$master = $this->hadiah_master->update_master( 
						$id_hadiah_m, $nama_hadiah, $id_pegawai, $id_customer_pusat, 
						$total, $tanggal, $catatan, 'MENUNGGU'
					);

					if($master){
						echo json_encode(array(
							'status' => 1, 
							'pesan'  => "Transaksi berhasil ditahan,
							   			 No. Hadiah : ".$no_hadiah.",
							   			 Nama Hadiah : ".$nama_hadiah.""
						));
						exit();
					}else{
						echo json_encode(array(
							'status' => 0, 
							'pesan'  => "Gagal menahan transaksi hadiah,
										 Harap coba lagi ?"
						));
						exit();
					}
				}else{
					echo json_encode(array(
						'status' => 0, 
						'pesan'  => "Gagal menahan transaksi hadiah,
									 Harap coba lagi ?"
					));
					exit();
				}
			}else{
				echo json_encode(array(	
					'status' => 0,
					'pesan'  => 'Maaf anda tidak diizinkan untuk menahan transaksi hadiah.'
				));
				exit();
			}
		}else{
			redirect(base_url().'hadiah');
		}
	}

	public function ambil_data()
	{
		if($this->input->is_ajax_request()){
			// Cek user acces menu
			$id_pegawai     = $this->session->userdata('id_pegawai');
			$cek_useraccess = $this->useraccess_pusat->cek_access($id_pegawai, '31');
			if($cek_useraccess->act_read == '0' or $cek_useraccess->act_read == '-'){
				redirect(base_url().'hadiah');
				exit();
			}

			$id_hadiah_m = $this->input->post('id_hadiah_m');
			$data        = $this->hadiah_master->get_master($id_hadiah_m);
			$data_batal  = $this->hadiah_master->get_jumlah_batal($id_hadiah_m);

			echo json_encode(array(
				'id_hadiah_m'         => $data->id_hadiah_m,
				'no_hadiah'           => $data->no_hadiah,
				'nama_hadiah'         => $data->nama_hadiah,
				'id_customer_pusat'   => $data->id_customer_pusat,
				'nama_customer_pusat' => $data->nama_customer_pusat,
				'handphone1'          => $data->handphone1,
				'tipe_customer_pusat' => $data->tipe_customer_pusat,									
				'keterangan_lain'     => $data->keterangan_lain,
				'jumlah_barang'       => $data->jumlah_barang,
				'jumlah_barang_batal' => $data_batal->jumlah_barang,
				'total'               => $data->total
			));
		}else{
			redirect(base_url().'hadiah');
		}
	}

	public function cek_stok()
	{
		if($this->input->is_ajax_request()){
			// Cek user acces menu
			$id_pegawai     = $this->session->userdata('id_pegawai');
			$cek_useraccess = $this->useraccess_pusat->cek_access($id_pegawai, '31');
			if($cek_useraccess->act_create == '0' AND $cek_useraccess->act_update == '0'){
				echo json_encode(array(
					'status'  => 0,
					'pesan'   => "Maaf anda tidak diizinkan 
								  untuk menambahkan atau memperbaharui transaksi hadiah",
					'arahkan' => "halaman_utama"
				));
				exit();
			}

			$id_barang_pusat = $this->input->post('id_barang_pusat');
			if($id_barang_pusat == '' or $id_barang_pusat == '0'){
				echo json_encode(array(	
					'status' => 0,
					'pesan'  => 'Harap pilih barang hadiah terlebih dahulu.',
					'arahkan' => "halaman_utama"
				));
				exit();
			}

			$jumlah_hadiah = $this->input->post('jumlah_hadiah');
			$id_hadiah_m   = $this->input->post('id_hadiah_m');

			$data_hadiah_master = $this->hadiah_master->get_by_id($id_hadiah_m);
			if($data_hadiah_master){
				$id_master = $data_hadiah_master->id_hadiah_m;
			}else{
				$id_master = '';
			}
			
			$data_barang_pusat = $this->barang_pusat->get_by_id($id_barang_pusat);
			if($data_barang_pusat){
				$id_barang_pusat = $data_barang_pusat->id_barang_pusat;
			}else{
				$id_barang_pusat = '';
			}

			if($id_master){
				$data_hadiah_detail = $this->hadiah_detail->get_id($id_master, $id_barang_pusat);
				if ($data_hadiah_detail){
					$jumlah_hadiah_lama = $data_hadiah_detail->jumlah_hadiah;	
				}else{
					$jumlah_hadiah_lama = '0';
				}
			}else{
				$jumlah_hadiah_lama = '0';
			}
			
			$get_stok      = $this->barang_pusat->get_stok_id($id_barang_pusat, $jumlah_hadiah_lama);
			$stok_sekarang = $get_stok->total_stok;

			if($jumlah_hadiah > $stok_sekarang){
				echo json_encode(array(
										'status'          => 0,
										'stok_skrng'      => $get_stok->total_stok,
										'jumlah_lama'     => $jumlah_hadiah_lama,
										'pesan'           => "Stok untuk :
															 ".$get_stok->nama_barang."
															 Saat ini hanya tersisa  : 
															 ".$stok_sekarang.""
								));
			}else{
				echo json_encode(array(
										'stok'               => $stok_sekarang,
										'id_master'          => $id_master,
										'id_barang_pusat'    => $id_barang_pusat,
										'stok_skrng'         => $get_stok->total_stok,
										'jumlah_hadiah_lama' => $jumlah_hadiah_lama,
										'status'             => 1
								));
			}
		}
	}

	public function simpan_hadiah_detail()
	{
		// Cek user acces menu
		$id_pegawai        = $this->session->userdata('id_pegawai');
		$cek_useraccess    = $this->useraccess_pusat->cek_access($id_pegawai, '31');
		
		// Ambil data hadiah master
		$id_hadiah_m       = $this->clean_tag_input($this->input->post('id_hadiah_m'));
		$nama_hadiah       = $this->clean_tag_input($this->input->post('nama_hadiah'));
		$id_barang_pusat   = $this->clean_tag_input($this->input->post('id_barang_pusat'));
		$id_customer_pusat = $this->input->post('id_customer_pusat');
		$tanggal           = date('Y-m-d H:i:s');
		$catatan           = $this->clean_tag_input($this->input->post('catatan'));

		// Validasi hadiah master sudah ada atau belum
		$data_hadiah_master	= $this->hadiah_master->get_by_id($id_hadiah_m);
		if($data_hadiah_master){
			if($cek_useraccess->act_create == '1' or $cek_useraccess->act_update == '1'){
				$id_master = $data_hadiah_master->id_hadiah_m;
			}else{
				echo json_encode(array(	
					'status'  => 0,
					'pesan'   => 'Maaf anda tidak diizinkan untuk menyimpan transakasi hadiah barang.',
					'arahkan' => "halaman_utama"
				));
				exit();
			}
		}else{
			$id_master         = '';
			$data_barang_pusat = $this->barang_pusat->get_id($id_barang_pusat);
			if($data_barang_pusat){
				$sku          = $data_barang_pusat->sku;
				$nama_barang  = $data_barang_pusat->nama_barang;
				$harga_eceran = $data_barang_pusat->harga_eceran;
			}else{
				echo json_encode(array(
					'status'  => 0,
					'pesan'   => "Barang hadiah tidak dikenal !
								  harap periksa kembali sku barang hadiah",
					'arahkan' => "halaman_utama"
				));
				exit();
			}
		}

		if($id_master){
			if($cek_useraccess->act_create == '0' AND $cek_useraccess->act_update == '0'){
				echo json_encode(array(	
					'status'  => 0,
					'pesan'   => 'Maaf anda tidak diizinkan untuk memperbaharui transaksi hadiah.',
					'arahkan' => "halaman_utama"
				));
				exit();
			}
		}else{
			if($cek_useraccess->act_create == '1'){	
				// Insert hadiah master
				$no_hadiah = $this->no_hadiah_baru($id_pegawai);
				$master    = $this->hadiah_master->insert_master(
					$no_hadiah, $nama_hadiah, $tanggal, $id_pegawai, $id_customer_pusat, '0', $catatan
				);

				$data_hadiah_master = $this->hadiah_master->get_id($no_hadiah);
				$id_master          = $data_hadiah_master->id_hadiah_m;
			}else{
				echo json_encode(array(	
					'status'  => 0,
					'pesan'   => 'Maaf anda tidak diizinkan untuk menambahkan barang transaksi hadiah.',
					'arahkan' => "halaman_utama"
				));
				exit();
			}
		}

		if($id_master){
			// Ambil data hadiah detail
			$sku               = $this->input->post('sku');
			$jumlah_hadiah     = $this->input->post('jumlah_hadiah');
			
			$data_barang_pusat = $this->barang_pusat->get_sku($sku);
			$id_barang_pusat   = $data_barang_pusat->id_barang_pusat;
			$modal             = $data_barang_pusat->modal_bersih;
			$harga_satuan      = $data_barang_pusat->harga_eceran;
			$subtotal          = $jumlah_hadiah * $harga_satuan + 0;

			// Validasi insert atau update di hadiah detail
			$data_hadiah_detail = $this->hadiah_detail->get_id($id_master, $id_barang_pusat);
			if($data_hadiah_detail){
				$id_detail          = $data_hadiah_detail->id_hadiah_d;
				$jumlah_hadiah_lama = $data_hadiah_detail->jumlah_hadiah;
			}else{
				$id_detail          = '';
				$jumlah_hadiah_lama = '0';
			}

			if($id_detail){
				if($cek_useraccess->act_update == '1'){
					// update dan kembalikan stok sebelumnya
					$simpan_data = $this->hadiah_detail->update_detail( 
						$id_master, $id_barang_pusat, $jumlah_hadiah, 
						$modal, $harga_satuan, $subtotal, $jumlah_hadiah_lama, $id_pegawai
					);
				}else{
					echo json_encode(array(	
						'status'  => 0,
						'pesan'   => 'Maaf anda tidak diizinkan untuk mengedit barang transaksi hadiah.',
						'arahkan' => "halaman_utama"
					));
					exit();
				}
			}else{
				if($cek_useraccess->act_create == '1'){
					// insert
					$simpan_data = $this->hadiah_detail->insert_detail( 
						$id_master, $id_barang_pusat, $jumlah_hadiah, 
						$modal, $harga_satuan, $subtotal, $id_pegawai
					);
				}else{
					echo json_encode(array(	
						'status'  => 0,
						'pesan'   => 'Maaf anda tidak diizinkan untuk menambahkan barang transaksi hadiah.',
						'arahkan' => "halaman_utama"
					));
					exit();
				}			
			}

			if($simpan_data){
				$data_total_hadiah = $this->hadiah_detail->get_total($id_master);
				if($data_total_hadiah){
					$ttl           = $data_total_hadiah->total;
					$jumlah_barang = $data_total_hadiah->jumlah_barang;

					// Update total dan grandtotal di hadiah master
					$master = $this->hadiah_master->update_master( 
						$id_master, $nama_hadiah, $id_pegawai, $id_customer_pusat, 
						$ttl, $tanggal, $catatan, 'MENUNGGU'
					);
				}else{
					$ttl                 = '0';
					$jumlah_barang       = '0';
					$jumlah_barang_batal = '0';
				}

				$data_batal = $this->hadiah_master->get_jumlah_batal($id_hadiah_m);
				if($data_batal){
					$jumlah_barang_batal = $data_batal->jumlah_barang;
				}else{
					$jumlah_barang_batal = '0';
				}

				// Update stok barang pusat
				$this->barang_pusat->update_stok($id_barang_pusat, $jumlah_hadiah);

				echo json_encode(array(
										'status'              => 1,
										'pesan'               => "Data hadiah barang berhasil disimpan",
										'url'                 => 'hadiah/transaksi/?&id='.$id_master,
										'id_hm'               => $id_master,
										'jumlah_barang'       => $jumlah_barang,
										'jumlah_barang_batal' => $jumlah_barang_batal,
										'total'               => $ttl
								));
			}else{
				echo json_encode(array(
										'status' => 0,
										'pesan'  => 'Data barang hadiah gagal disimpan'
								));
			}
		}
	}

	public function ajax_list()
	{
		if($this->input->is_ajax_request()){
			// Cek user acces menu
			$id_pegawai     = $this->session->userdata('id_pegawai');
			$cek_useraccess = $this->useraccess_pusat->cek_access($id_pegawai, '31');
			if($cek_useraccess->act_read == '0' or $cek_useraccess->act_read == '-'){
				redirect(base_url().'dashboard');
			}

			$id_hadiah_m = $this->clean_tag_input($this->input->post('id_hadiah_m'));
			if($id_hadiah_m == ''){
				exit();
			}

			$data_hadiah_master = $this->hadiah_master->get_by_id($id_hadiah_m);
			if($data_hadiah_master){
				$id_master = $data_hadiah_master->id_hadiah_m;
			}else{
				$id_master = '0';
			}

			$list = $this->hadiah_detail->get_datatables('_get_datatables_query', $id_master);
			$data = array();
			$no   = $_POST['start'];

			foreach ($list as $hd){
				$no++;
				$row = array();
				$row[] = $no;

				//add html for action
				if($cek_useraccess->act_update == 1){
					$tombol_edit = '<a class="btn btn-rounded btn-default btn-xs" 
										onclick="edit_hadiah_detail('."'".$hd->id_hadiah_d."'".')">
									<i class="fa fa-pencil" style="color:blue;"></i></a>';
				}else{
					$tombol_edit = '';
				}

				if($cek_useraccess->act_delete == 1){
					$tombol_hapus = '<a class="btn btn-rounded btn-default btn-xs" 
										onclick="verifikasi_hapus_hadiah_detail('."'".$hd->id_hadiah_d."'".')">
									<i class="fa fa-times" style="color:red;"></i></a>';
				}else{
					$tombol_hapus = '';
				}

				if($tombol_edit == '' AND $tombol_hapus == ''){
					$tombol_keterangan = '<b class="btn btn-rounded btn-xs btn-default text-muted text-center">Tidak ada akses</b>';
				}else{
					$tombol_keterangan = '';
				}

				$row[] =	'
								'.$tombol_edit.'
								'.$tombol_hapus.'
								'.$tombol_keterangan.'
					  		';

				$row[] 	= $hd->sku;
				$row[] 	= $hd->nama_barang;

				$row[] 	= '<span class="pull-right">'.number_format($hd->harga_satuan,'0',',','.').'</span>';
				$row[] 	= '<span class="pull-right">'.number_format($hd->jumlah_hadiah,'0',',','.').'</span>';
				$row[] 	= '<span class="pull-right">'.number_format($hd->subtotal,'0',',','.').'</span>';

				$row[]  = $hd->pegawai_save;
				$row[]  = $hd->tanggal_pembuatan;
				$row[]  = $hd->pegawai_edit;
				$row[]  = $hd->tanggal_pembaharuan;	
				$data[]	= $row;
			}

			$output = array(
				"draw"            => $_POST['draw'],
				"recordsTotal"    => $this->hadiah_detail->count_all('vamr4846_vama.hadiah_detail', $id_master),
				"recordsFiltered" => $this->hadiah_detail->count_filtered('_get_datatables_query', $id_master),
				"data"            => $data
			);
			echo json_encode($output);
		}else{
			redirect(base_url().'hadiah');
		}
	}

	public function ajax_list_batal()
	{
		if($this->input->is_ajax_request()){
			// Cek user acces menu
			$id_pegawai     = $this->session->userdata('id_pegawai');
			$cek_useraccess = $this->useraccess_pusat->cek_access($id_pegawai, '31');
			if($cek_useraccess->act_read == '0' or $cek_useraccess->act_read == '-'){
				redirect(base_url().'dashboard');
			}

			$id_hadiah_m = $this->clean_tag_input($this->input->post('id_hadiah_m'));
			if($id_hadiah_m == ''){
				exit();
			}

			$data_hadiah_master = $this->hadiah_master->get_by_id($id_hadiah_m);
			if($data_hadiah_master){
				$id_master = $data_hadiah_master->id_hadiah_m;
			}else{
				$id_master = '0';
			}

			$list = $this->hadiah_detail->get_datatables('_get_datatables_query_batal', $id_master);
			$data = array();
			$no   = $_POST['start'];

			foreach ($list as $hd){
				$no++;
				$row = array();
				$row[] = $no;

				$row[] 	= $hd->sku;
				$row[] 	= $hd->nama_barang;

				$row[] 	= '<span class="pull-right">'.number_format($hd->harga_satuan,'0',',','.').'</span>';
				$row[] 	= '<span class="pull-right">'.number_format($hd->jumlah_hadiah,'0',',','.').'</span>';
				$row[] 	= '<span class="pull-right">'.number_format($hd->subtotal,'0',',','.').'</span>';
				$row[]  = $hd->keterangan_batal;				

				$row[]  = $hd->pegawai_batal;
				$row[]  = $hd->tanggal_pembatalan;
				$row[]  = $hd->pegawai_save;
				$row[]  = $hd->tanggal_pembuatan;
				$row[]  = $hd->pegawai_edit;
				$row[]  = $hd->tanggal_pembaharuan;	
				$data[]	= $row;
			}

			$output = array(
				"draw"            => $_POST['draw'],
				"recordsTotal"    => $this->hadiah_detail->count_all('vamr4846_vama.hadiah_detail_batal', $id_master),
				"recordsFiltered" => $this->hadiah_detail->count_filtered('_get_datatables_query_batal', $id_master),
				"data"            => $data
			);
			echo json_encode($output);
		}else{
			redirect(base_url().'hadiah');
		}
	}

	public function ajax_list_barang()
	{
		if($this->input->is_ajax_request()){
			// Cek user acces menu
			$id_pegawai     = $this->session->userdata('id_pegawai');
			$cek_useraccess = $this->useraccess_pusat->cek_access($id_pegawai, '31');
			if($cek_useraccess->act_read == '0' or $cek_useraccess->act_read == '-'){
				$id_hadiah_m = '0';
				$output      = array(	
					"draw"            => $_POST['draw'],
					"recordsTotal"    => $this->hadiah_detail->count_all('barang_pusat', $id_hadiah_m),
					"recordsFiltered" => $this->hadiah_detail->count_filtered('_get_datatables_query_barang', $id_hadiah_m),
					"data"            => $data
				);
				echo json_encode($output);
				exit();
			}

			$id_hadiah_m = $this->clean_tag_input($this->input->post('id_hadiah_m'));
			if($id_hadiah_m == ''){
				$id_hadiah_m = '0';
				$output      = array(	
					"draw"            => $_POST['draw'],
					"recordsTotal"    => $this->hadiah_detail->count_all('barang_pusat', $id_hadiah_m),
					"recordsFiltered" => $this->hadiah_detail->count_filtered('_get_datatables_query_barang', $id_hadiah_m),
					"data"            => $data
				);
				echo json_encode($output);
				exit();
			}

			$list = $this->hadiah_detail->get_datatables('_get_datatables_query_barang', $id_hadiah_m);
			$data = array();
			$no   = $_POST['start'];

			foreach ($list as $bp){
				$no++;
				$row     = array();
				$row[]   = $no;
				
				$row[]   = $bp->sku;
				$row[]   = $bp->nama_barang;
				$row[]   = '<span class="pull-right">'.number_format($bp->harga_eceran,'0',',','.').'</span>';
				$row[]   = '<span class="pull-right">'.number_format($bp->total_stok,'0',',','.').'</span>';
				
				$row[]   =	'<a class="btn btn-primary btn-xs" onclick="proses_barang('."'".$bp->sku."'".')">
								<i class ="fa fa-check"></i> PILIH
							</a>';
				$data[]  = $row;
			}

			$output = array(	
				"draw"            => $_POST['draw'],
				"recordsTotal"    => $this->hadiah_detail->count_all('vamr4846_vama.barang_pusat', $id_hadiah_m),
				"recordsFiltered" => $this->hadiah_detail->count_filtered('_get_datatables_query_barang', $id_hadiah_m),
				"data"            => $data
			);
			echo json_encode($output);
		}else{
			redirect(base_url().'hadiah');
		}
	}

	public function ajax_edit()
	{
		if($this->input->is_ajax_request()){
			// Cek user acces menu
			$id_pegawai     = $this->session->userdata('id_pegawai');
			$cek_useraccess = $this->useraccess_pusat->cek_access($id_pegawai, '31');
			if($cek_useraccess->act_update == '0' or $cek_useraccess->act_update == '-'){
				echo json_encode(array(
					"status" => 0,
					"pesan"  => "Maaf anda tidak di izinkan untuk mengedit barang hadiah"
				));
				exit();
			}

			$id_hadiah_d    = $this->input->post('id_hadiah_d');
			if($id_hadiah_d == ''){
				redirect(base_url()."hadiah");					
			}else{
				$data = $this->hadiah_detail->get_hadiah_detail($id_hadiah_d);
				if($data){
					echo json_encode(array(
						"status" => 1,
						"data"   => $data
					));
				}else{
					echo json_encode(array(
						"status" => 0,
						"pesan"  => "Data barang hadiah tidak ditemukan."
					));
				}				
			}
		}else{
			redirect(base_url().'hadiah');
		}
	}

	public function ajax_verifikasi_hapus_detail()
	{
		if($this->input->is_ajax_request()){
			// Cek user acces menu
			$id_pegawai     = $this->session->userdata('id_pegawai');
			$cek_useraccess = $this->useraccess_pusat->cek_access($id_pegawai, '31');
			if($cek_useraccess->act_delete == '0' or $cek_useraccess->act_delete == '-'){
				echo json_encode(array(
					"status" => 0,
					"pesan"  => "Maaf anda tidak di izinkan untuk menghapus barang hadiah"
				));
				exit();
			}

			$id_hadiah_d    = $this->input->post('id_hadiah_d');
			if($id_hadiah_d == ''){
				redirect(base_url()."hadiah");					
			}else{			
				$data = $this->hadiah_detail->get_hadiah_detail($id_hadiah_d);
				if($data){
					echo json_encode(array(
						'status' 	=> 1,
						'pesan' 	=> "
										<small>SKU : </small><br/>
										<b class='text-dark'>$data->sku </b><br/><br/>

										<small>Nama Barang : </small><br/>	
										<b class='text-dark'>$data->nama_barang</b></br><br/>

										<small>Jumlah Hadiah : </small></br>
										<h1 class='text-danger'>$data->jumlah_hadiah </h1>

										<textarea name='keterangan_batal' id='keterangan_batal' class='form-control input-md' placeholder='Keterangan batal' style='resize: vertical;'></textarea>
									 ",

						'footer'	=> "
										<button onclick='hapus_hadiah_detail($data->id_hadiah_d)' type='button' class='btn btn-primary waves-effect waves-light' data-dismiss='modal' autofocus>Iya, Hapus</button> 
										<button type='button' class='btn btn-default waves-effect' data-dismiss='modal'>Batal</button>
									"
					));
				}else{
					echo json_encode(array(
						'status' => 0,
						'pesan'  =>	"Barang hadiah tidak ditemukan"
					));
				}
			}
		}else{
			redirect(base_url().'hadiah');
		}
	}

	public function ajax_hapus_detail()
	{
		if($this->input->is_ajax_request()){
			// Cek user acces menu
			$id_pegawai 			= $this->session->userdata('id_pegawai');
			$cek_useraccess 		= $this->useraccess_pusat->cek_access($id_pegawai, '31');
			if($cek_useraccess->act_delete == '0' or $cek_useraccess->act_delete == '-'){
				echo json_encode(array(
					'status' => 0,
					'pesan'  => 'Maaf anda tidak di izinkan
								 untuk menghapus barang hadiah'
				));
				exit();
			}

			$id_hadiah_d       = $this->input->post('id_hadiah_d');
			$nama_hadiah       = $this->clean_tag_input($this->input->post('nama_hadiah'));
			$keterangan_batal  = $this->input->post('keterangan_batal');
			$tanggal           = date('Y-m-d H:i:s');
			$catatan           = $this->clean_tag_input($this->input->post('catatan'));
			$id_customer_pusat = $this->input->post('id_customer_pusat');

			// Ambil data hadiah detail
			$dt = $this->hadiah_detail->get_hadiah_detail($id_hadiah_d);
			if($dt){
				$id_master     = $dt->id_hadiah_m;
				$sku           = $dt->sku;
				$hapus         = $this->hadiah_detail->hapus_hadiah_detail(
					$dt->id_hadiah_d, $dt->id_hadiah_m, $dt->id_customer_pusat, $dt->no_hadiah, $dt->nama_hadiah, 
					$dt->id_barang_pusat, $dt->modal, $dt->harga_satuan, $dt->jumlah_hadiah, $dt->subtotal,
					$dt->id_pegawai_pembuatan, $dt->id_pegawai_pembaharuan, $id_pegawai,
					$dt->tanggal_pembuatan, $dt->tanggal_pembaharuan, $keterangan_batal
				);
			}else{
				echo json_encode(array(
					'status' => 0,
					'pesan'  => 'Data barang hadiah tidak ditemukan'
				));
				exit();
			}
			
			if($hapus){
				$data_total_hadiah = $this->hadiah_detail->get_total($id_master);
				if($data_total_hadiah){
					$status_hadiah       = $data_total_hadiah->status_hadiah;
					$ttl                 = $data_total_hadiah->total;
					$jumlah_barang       = $data_total_hadiah->jumlah_barang;

					$data_batal = $this->hadiah_master->get_jumlah_batal($id_master);
					if($data_batal){
						$jumlah_barang_batal = $data_batal->jumlah_barang;	
					}else{
						$jumlah_barang_batal = '0';
					}

					if($jumlah_barang == 0){
						$hapus_master = $this->hadiah_master->hapus_master($id_master);
					}else{
						// Update total dan grandtotal di hadiah master
						$master = $this->hadiah_master->update_master( 
							$id_master, $nama_hadiah, $id_pegawai, $id_customer_pusat, 
							$ttl, $tanggal, $catatan, $status_hadiah);
					}
				}

				// Tampilkan informasi hapus
				echo json_encode(array(
					'status'              => '1',
					'sku'                 => $sku,
					'id_hm'               => $id_master,
					'jumlah_barang'       => $jumlah_barang,
					'jumlah_barang_batal' => $jumlah_barang_batal,
					'total'               => $ttl,
					'pesan'               => "Data hadiah barang berhasil dihapus"
					)
				);
			}else{
				echo json_encode(array(
					'status' => 0,
					'pesan' => 'Gagal menghapus hadiah barang, harap coba kembali'
				));
			}		
		}else{
			redirect(base_url().'hadiah');
		}
	}

	public function ajax_cari_barang_hadiah()
	{
		if($this->input->is_ajax_request()){
			$id_pegawai     = $this->session->userdata('id_pegawai');
			$cek_useraccess = $this->useraccess_pusat->cek_access($id_pegawai, '31');
			if($cek_useraccess->act_create == '1'){
				$sku         = $this->input->post('sku');
				$data_barang = $this->barang_pusat->ambil_barang_untuk_transaksi($sku);				

				if($data_barang->row()){
					$json['status'] = 1;
					// $json['data']   = $data_barang->row();
					$json['data']   = array(	
											'id_barang_pusat' => $data_barang->row()->id_barang_pusat,
											'sku'             => $data_barang->row()->sku,
											'nama_barang'     => $data_barang->row()->nama_barang,
											'harga_satuan'    => $data_barang->row()->harga_eceran,
											'total_stok'      => $data_barang->row()->total_stok
									  		);
				}else{
					$json['status'] = 0;
					$json['pesan']  = "	SKU : ".$sku."  
										Tidak ditemukan di master barang!
									";
				}
			}else{
				$json['status'] = 2;
				$json['pesan']  = "	Maaf anda tidak di izinkan 
									untuk menambahkan barang di transaksi hadiah.";
			}
			echo json_encode($json);
		}else{
			redirect(base_url().'hadiah');
		}
	}

	public function ajax_kode()
	{
		if($this->input->is_ajax_request()){
			// Cek user acces menu
			$id_pegawai     = $this->session->userdata('id_pegawai');
			$cek_useraccess = $this->useraccess_pusat->cek_access($id_pegawai, '31');
			if($cek_useraccess->act_create == '0' or $cek_useraccess->act_create == '-'){
				$json['status']      = 2;
				$json['judul_pesan'] = 'Oops!';
				$json['isi_pesan']   = 'Maaf anda tidak diberikan izin 
										untuk menambahkan transaksi hadiah.';
			}else{
				$keyword = $this->input->post('keyword');
				$id_hm   = $this->input->post('id_hm');			
				$barang  = $this->barang_pusat->cari_kode_hadiah($keyword, $id_hm);				

				if($barang->num_rows() > 0){
					$json['status']  = 1;
					$json['datanya'] = "<ul id='daftar-autocomplete' class='list-group user-list'>";
					
					foreach($barang->result() as $b){
						if($b->foto!=""){
							$json['datanya'] .= "
				                <li class='user-list-item'>
				                	<div class='avatar avatar-sm-box'>
	                                    <img src='assets/upload/image/barang/thumbs/$b->foto'>
				                	</div>";
				        }else{
							$nama_barang 	  = $b->nama_barang;
							$inisial 		  = substr($nama_barang, 0, 1);
							$json['datanya'] .= "
				                <li class='user-list-item'>
				                	<div class='avatar avatar-sm-box'>
	                                    <span class='avatar-sm-box bg-info'>".$inisial."</span>
				                	</div>";
						}

						$json['datanya'] .= "
			                	<div class='user-desc'>
									<span id='barangnya' class='name'>".$b->nama_barang."</span>
									<span id='skunya' class='name text-muted'>".$b->sku."</span>
									<span id='total_stok' class='name text-muted'>Stok : ".$b->total_stok."</span>
									<span id='harga_eceran' class='name text-muted'>EC : Rp. ".$b->eceran."</span>
									<span id='eceran' style='display:none;'>".$b->harga_eceran."</span>
									<span id='id_barang_pusat' style='display:none;'>".$b->id_barang_pusat."</span>
								</div>
							</li>
						";
					}

					$json['datanya'] .= "</ul>";
				}else{
					$json['status'] = 0;
				}
			}
			echo json_encode($json);
		}else{
			redirect(base_url().'hadiah');
		}
	}

	public function ajax_pencarian_customer()
	{
		if($this->input->is_ajax_request())
		{
			$keyword 	= $this->input->post('keyword');
			$customer 	= $this->customer_pusat->cari_customer($keyword, 'MRC');

			if($customer->num_rows() > 0)
			{
				$json['status'] 	= 1;
				$json['datanya'] 	= "<ul id='daftar_autocompleted_customer' class='user-list'>";
				foreach($customer->result() as $c)
				{
					if($c->foto!="")
					{
					$json['datanya'] .= "
						                <li class='user-list-item'>
						                	<div class='avatar avatar-sm-box'>
				                                <img src='assets/upload/image/customer_pusat/thumbs/$c->foto'>
						                	</div>";
			        }
			        else
					{
					$nama_customer 	  = $c->nama_customer_pusat;
					$inisial 		  = substr($nama_customer, 0, 1);
					$json['datanya'] .= "
						                <li class='user-list-item'>
						                	<div class='avatar avatar-sm-box'>
			                                    <span class='avatar-sm-box bg-info'>".$inisial."</span>
						                	</div>";
					}

					$json['datanya'] .= "
						                	<div class='user-desc'>
												<span id='id_customer' style='display:none;'>".$c->id_customer_pusat."</span>
												<span id='kode_customer' style='display:none;'>".$c->kode_customer_pusat."</span>
												<span id='nama_customer' class='name'>".$c->nama_customer_pusat."</span>
												<span id='email_customer' class='name'>".$c->email."</span>
												<span id='hp_customer' class='name'>".$c->handphone1."</span>
												<span id='tipe_customer' class='desc' style='display:none;'>".$c->tipe_customer_pusat."</span>
											</div>
										</li>
										";
				}
				$json['datanya'] .= "</ul>";
			}
			else
			{
				$json['status'] 		= 0;
				// $json['perintah_sql'] 	= $this->db->last_query();				
			}

			echo json_encode($json);
		}
	}

	public function ajax_pencarian_harga()
	{
		if($this->input->is_ajax_request())
		{
			$sku 			= $this->input->post('sku');
			$barang_pusat 	= $this->barang_pusat->cari_harga_barang_pusat($sku);
			$b 				= $barang_pusat;
			if($b)
			{
				$json['status'] 	= 1;
				$json['datanya'] 	= "<ul id='daftar_autocompleted_harga' class='list-group'>";
				$json['datanya'] .= "
					                <li class='list-group-item'>
					                	EC : Rp.
										<span id='harga' class='text-dark text-right'>".$b->eceran."</span>
										<span id='harga_hidden' style='display:none;'>".$b->harga_eceran."</span>
										<span id='jenis' style='display:none;'>ECERAN</span>
									</li>
					                <li class='list-group-item'>
					                	G1 : Rp. 
										<span id='harga' class='text-dark text-right'>".$b->grosir1."</span>
										<span id='harga_hidden' style='display:none;'>".$b->harga_grosir1."</span>
										<span id='jenis' style='display:none;'>GROSIR 1</span>
									</li>
					                <li class='list-group-item'>
					                	G2 : Rp. 
										<span id='harga' class='text-dark text-right'>".$b->grosir2."</span>
										<span id='harga_hidden' style='display:none;'>".$b->harga_grosir2."</span>
										<span id='jenis' style='display:none;'>GROSIR 2</span>
									</li>
					                <li class='list-group-item'>
					                	<a class='btn btn-rounded btn-default btn-xs'>
					                		<span id='jenis' style='display:none;'>KHUSUS</span>
					                		<i class='fa fa-pencil' style='color:blue;'></i>
					                		Harga Khusus
					                	</a>
									</li>									
									";
				$json['datanya'] .= "</ul>";
			}
			else
			{
				$json['status'] 		= 0;
				// $json['perintah_sql'] 	= $this->db->last_query();				
			}

			echo json_encode($json);
		}
	}

	public function form_customer()
	{
		if($_POST){
			$this->_validate();
			$email 					= $this->input->post('email');
			$nama_customer_pusat	= $this->input->post('nama_customer_pusat');
			$tipe_customer_pusat	= $this->input->post('tipe_customer_pusat');
			$handphone1				= $this->input->post('handphone1');
			$alamat_customer_pusat	= $this->input->post('alamat_customer_pusat');

			$data = array(
							'nama_customer_pusat' 		=> $nama_customer_pusat,
							'no_siup'		 			=> $this->input->post('no_siup'),
							'alamat_customer_pusat' 	=> $this->clean_tag_input($alamat_customer_pusat),
							'asal_customer_pusat' 		=> $this->input->post('asal_customer_pusat'),
							'tipe_bisnis' 				=> $this->input->post('tipe_bisnis'),
							'tipe_customer_pusat' 		=> $tipe_customer_pusat,
							'kontak_pribadi' 			=> $this->input->post('kontak_pribadi'),
							'jabatan' 					=> $this->input->post('jabatan'),
							'telephone1' 				=> $this->input->post('telephone1'),
							'telephone2' 				=> $this->input->post('telephone2'),
							'handphone1' 				=> $handphone1,
							'handphone2' 				=> $this->input->post('handphone2'),
							'fax' 						=> $this->input->post('fax'),
							'email' 					=> $email,
							'operator_pembuatan'		=> $this->session->userdata('username'),
							'tanggal_pembuatan'			=> date('Y-m-d H:i:s')
						);
			$insert = $this->customer_pusat->save($data);

			if($insert)
			{
				$id_customer_pusat 	= $this->customer_pusat->get_customer_by_email($email)->row()->id_customer_pusat;
				echo json_encode(array(
					'status' 					=> TRUE,
					'pesan' 					=> "<div class='alert alert-success'>
													<i class='mdi mdi-check-all'></i> <b>".$nama_customer_pusat." </b> 
													berhasil ditambahkan sebagai pelanggan.</div>",
					'id_customer_pusat' 		=> $id_customer_pusat,
					'nama_customer_pusat' 		=> $nama_customer_pusat,
					'tipe_customer_pusat'		=> $tipe_customer_pusat,
					'alamat_customer_pusat' 	=> preg_replace("/\r\n|\r|\n/",'<br />', $alamat_customer_pusat),
					'handphone1' 				=> $handphone1,
					'email' 					=> $email						
				));
			}
			else
			{
				$this->query_error();
			}
		}else{
			$this->load->view('admin/transaksi/hadiah/form_customer');
		}
	}

	private function _validate()
	{
		$data = array();
		$data['error_string'] = array();
		$data['inputerror'] = array();
		$data['status'] = TRUE;

		if($this->input->post('nama_customer_pusat') == ''){
			$data['inputerror'][] = 'nama_customer_pusat';
			$data['error_string'][] = 'Nama customer pusat wajib diisi';
			$data['status'] = FALSE;
		}

		if($this->input->post('alamat_customer_pusat') == ''){
			$data['inputerror'][] = 'alamat_customer_pusat';
			$data['error_string'][] = 'Alamat customer pusat wajib diisi';
			$data['status'] = FALSE;
		}

		if($this->input->post('email') == ''){
			$data['inputerror'][] = 'email';
			$data['error_string'][] = 'Email customer pusat wajib diisi';
			$data['status'] = FALSE;
		}

		if($this->input->post('handphone1') == ''){
			$data['inputerror'][] = 'handphone1';
			$data['error_string'][] = 'Handphone-1 customer pusat wajib diisi';
			$data['status'] = FALSE;
		}

		if($data['status'] === FALSE){
			echo json_encode($data);
			exit();
		}
	}

	public function transaksi_cetak()
	{
			
	}		
}
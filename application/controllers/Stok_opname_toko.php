<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Stok_opname_toko extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('pegawai_pusat_model','pegawai_pusat');
        $this->load->model('useraccess_pusat_model','useraccess_pusat');

		$this->load->model('stok_opname_toko_model','stok_opname_toko');
		$this->load->model('barang_pusat_model','barang_pusat');
	}

	public function index()
	{
		// cari data pegawai
		$id_pegawai 					= $this->session->userdata('id_pegawai');
		$data_pegawai					= $this->pegawai_pusat->get_by_id($id_pegawai);
		$data['data_pegawai']    		= $data_pegawai;
		$data['data_induk_menu'] 		= $this->useraccess_pusat->get_induk_menu($id_pegawai);
			
		$data['atribut_halaman'] 		= 'Stok Opname Toko';
		$data['list_barang_pusat']		= $this->barang_pusat->listing();
		$data['halaman_list'] 			= $this->load->view('admin/transaksi/stok_opname_toko/list',$data,true);
		$data['halaman_form'] 			= $this->load->view('admin/transaksi/stok_opname_toko/form',$data,true);
		$data['halaman_plugin'] 		= $this->load->view('admin/transaksi/stok_opname_toko/plugin',$data,true);
		$data['isi_halaman'] 			= $this->load->view('admin/transaksi/stok_opname_toko/index',$data,true);
		$this->load->view('admin/layout',$data);

		// $this->load->helper('url');
		// $this->load->view('stok_opname_pusat_view');
	}

	public function ajax_list()
	{
		$list = $this->stok_opname_toko->get_datatables('stok_opname_toko');
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $stok_opname_toko) {
			$no++;
			$row = array();

			if($stok_opname_toko->foto!="")
			{
			$row[] = '<img src="'."".base_url()."".'assets/upload/image/barang/thumbs/'."".$stok_opname_toko->foto."".'" class="img-circle" width="50">';
			} 
			else 
			{
			$row[] = 'Tidak Ada';			
			}
			
			$row[] = $stok_opname_toko->sku;
			$row[] = $stok_opname_toko->nama_barang;
			$row[] = number_format($stok_opname_toko->qty,'0',',','.');
			$row[] = $stok_opname_toko->tanggal_pembuatan;
			$row[] = $stok_opname_toko->tanggal_pembaharuan;

			//add html for action
			$row[] = '
					<a class="btn btn-xs btn-success" href="javascript:void(0)" title="Edit" onclick="view_stok_opname_pusat('."'".$stok_opname_toko->id_stok_opname_toko."'".')"><i class="fa fa-eye"></i>
					</a>
					<a class="btn btn-xs btn-primary" href="javascript:void(0)" title="Edit" onclick="edit_stok_opname_pusat('."'".$stok_opname_toko->id_stok_opname_toko."'".')"><i class="glyphicon glyphicon-pencil"></i>
					</a>
				  	<a class="btn btn-xs btn-danger" href="javascript:void(0)" title="Hapus" onclick="delete_stok_opname_pusat('."'".$stok_opname_toko->id_stok_opname_toko."'".')"><i class="glyphicon glyphicon-trash"></i>
				  	</a>
				  	';
		
			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->stok_opname_toko->count_all('stok_opname_toko'),
						"recordsFiltered" => $this->stok_opname_toko->count_filtered('stok_opname_toko'),
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}

	public function ajax_list_riwayat()
	{
		$list = $this->stok_opname_toko->get_datatables_riwayat('riwayat_stok_opname_toko');
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $stok_opname_toko) {
			$no++;
			$row = array();

			if($stok_opname_toko->foto!="")
			{
			$row[] = '<img src="'."".base_url()."".'assets/upload/image/barang/thumbs/'."".$stok_opname_toko->foto."".'" class="img-circle" width="50">';
			} 
			else 
			{
			$row[] = 'Tidak Ada';			
			}
			
			$row[] = $stok_opname_toko->sku;
			$row[] = $stok_opname_toko->nama_barang;
			$row[] = number_format($stok_opname_toko->qty_sebelumnya,'0',',','.');
			$row[] = number_format($stok_opname_toko->qty,'0',',','.');
			$row[] = $stok_opname_toko->tanggal_pembuatan;
			$row[] = $stok_opname_toko->tanggal_pembaharuan;		

			//add html for action
			// $row[] = '
			// 		<a class="btn btn-xs btn-success" href="javascript:void(0)" title="Edit" onclick="view_stok_opname_pusat('."'".$stok_opname_toko->id_stok_opname_toko."'".')"><i class="fa fa-eye"></i>
			// 		</a>
			// 		<a class="btn btn-xs btn-primary" href="javascript:void(0)" title="Edit" onclick="edit_stok_opname_pusat('."'".$stok_opname_toko->id_stok_opname_toko."'".')"><i class="glyphicon glyphicon-pencil"></i>
			// 		</a>
			// 	  	<a class="btn btn-xs btn-danger" href="javascript:void(0)" title="Hapus" onclick="delete_stok_opname_pusat('."'".$stok_opname_toko->id_stok_opname_toko."'".')"><i class="glyphicon glyphicon-trash"></i>
			// 	  	</a>
			// 	  	';
		
			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->stok_opname_toko->count_all('riwayat_stok_opname_toko'),
						"recordsFiltered" => $this->stok_opname_toko->count_filtered('riwayat_stok_opname_toko'),
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}	

	public function ajax_edit($id_stok_opname_toko)
	{
		$data = $this->stok_opname_toko->get_by_id($id_stok_opname_toko);
		// $data->dob = ($data->dob == '0000-00-00') ? '' : $data->dob; // if 0000-00-00 set tu empty for datepicker compatibility
		echo json_encode($data);
	}

	public function ajax_add()
	{
		// Simpan data riwayat stok opname
		$this->_validate();
		$data = array(
						'kode_barang'			=> $this->input->post('kode_barang'),
						'qty_sebelumnya' 		=> $this->input->post('qty_sebelumnya'),
						'qty' 					=> $this->input->post('qty'),
						'kode_toko'				=> 'TK001',					
						'kode_periode_toko'		=> 'PSTK0011',
						'operator_pembuatan'	=> $this->session->userdata('username'),
						'tanggal_pembuatan'		=> date('Y-m-d H:i:s')
					);
		$insert = $this->stok_opname_toko->save($data,'riwayat_stok_opname_toko');

		// Simpan data stok opname
		$this->_validate();
		$data = array(
						'kode_barang'			=> $this->input->post('kode_barang'),
						'qty' 					=> $this->input->post('qty'),
						'kode_toko'				=> 'TK001',					
						'kode_periode_toko'		=> 'PSTK0011',
						'operator_pembuatan'	=> $this->session->userdata('username'),
						'tanggal_pembuatan'		=> date('Y-m-d H:i:s')
					);
		$insert = $this->stok_opname_toko->save($data,'stok_opname_toko');
		echo json_encode(array("status" => TRUE));
	}

	public function ajax_update()
	{
		// Simpan data riwayat stok opname
		$this->_validate();
		$data = array(
						'kode_barang'			=> $this->input->post('kode_barang'),
						'qty_sebelumnya' 		=> $this->input->post('qty_sebelumnya'),
						'qty' 					=> $this->input->post('qty'),
						'kode_toko'				=> 'TK001',	
						'kode_periode_toko'		=> 'PSTK0011',
						'operator_pembuatan'	=> 'ali',
						'tanggal_pembuatan'		=> date('Y-m-d H:i:s')
					);
		$insert = $this->stok_opname_toko->save($data,'riwayat_stok_opname_toko');

		$this->_validate();
		$data = array(
						'qty' 					=> $this->input->post('qty'),
						'operator_pembaharuan'	=> 'ali'
					);
		$this->stok_opname_toko->update(array('id_stok_opname_toko' => $this->input->post('id_stok_opname_toko')), $data);
		echo json_encode(array("status" => TRUE));
	}

	public function ajax_delete($id_stok_opname_toko)
	{
		$this->stok_opname_toko->delete_by_id($id_stok_opname_toko);
		echo json_encode(array("status" => TRUE));
	}

	private function _validate()
	{
		$data = array();
		$data['error_string'] = array();
		$data['inputerror'] = array();
		$data['status'] = TRUE;

		if($this->input->post('kode_barang') == '')
		{
			$data['inputerror'][] = 'kode_barang';
			$data['error_string'][] = 'Kode barang wajib di diisi';
			$data['status'] = FALSE;
		}

		if($this->input->post('qty') == '')
		{
			$data['inputerror'][] = 'qty';
			$data['error_string'][] = 'Qty wajib diisi';
			$data['status'] = FALSE;
		}

		if($data['status'] === FALSE)
		{
			echo json_encode($data);
			exit();
		}
	}

}

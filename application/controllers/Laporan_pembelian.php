<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Laporan_pembelian extends MY_Controller 
{
	function __construct()
	{
		parent::__construct();
		$this->CI =& get_instance();
		$this->load->model('pegawai_pusat_model','pegawai_pusat');
		$this->load->model('useraccess_pusat_model','useraccess_pusat');
		$this->load->model('supplier_model','supplier');
		$this->load->model('barang_pusat_model','barang_pusat');
		$this->load->model('pembelian_master_model','pembelian_master');
		$this->load->model('pembelian_detail_model','pembelian_detail');
	}

	public function index()
	{
		// Cek user acces menu
		$id_pegawai = $this->session->userdata('id_pegawai');
		if($id_pegawai == ''){
			redirect(base_url().'dashboard');
		}
		
		$cek_useraccess = $this->useraccess_pusat->cek_access($id_pegawai, '36');
		if($cek_useraccess){
			if($cek_useraccess->act_read == '0' or $cek_useraccess->act_read == '-'){
				redirect(base_url().'dashboard');
			}					
		}else{
			redirect(base_url().'dashboard');
		}

		$data_pegawai = $this->pegawai_pusat->get_by_id($id_pegawai);
		if($data_pegawai){		
			$data['access_create']   = $cek_useraccess->act_create;
			$data['data_pegawai']    = $data_pegawai;
			$data['data_induk_menu'] = $this->useraccess_pusat->get_induk_menu($id_pegawai);
			
			$data['atribut_halaman'] = 'Laporan Pembelian';
			$data['halaman_laporan'] = $this->load->view('admin/laporan/pembelian/laporan',$data,true);
			$data['halaman_plugin']  = $this->load->view('admin/laporan/pembelian/plugin',$data,true);
			$data['isi_halaman']     = $this->load->view('admin/laporan/pembelian/index',$data,true);
			$this->load->view('admin/layout',$data);
		}else{
			redirect(base_url().'login');
		}
	}

	public function ajax_list()
	{
		// Cek user acces menu
		$id_pegawai               = $this->session->userdata('id_pegawai');
		$cek_useraccess_transaksi = $this->useraccess_pusat->cek_access($id_pegawai, '25');
		$cek_useraccess_laporan   = $this->useraccess_pusat->cek_access($id_pegawai, '36');
		if($cek_useraccess_laporan->act_read == '0' or $cek_useraccess_laporan->act_read == '-'){
			redirect(base_url().'dashboard');
		}

		$tanggal_filter = $this->input->post('tanggal_filter');
		if($tanggal_filter == ''){
			redirect(base_url().'laporan_pembelian');
			exit();
		}

		$tanggal_awal           = substr($tanggal_filter, 0, 10);
		$tanggal_akhir          = substr($tanggal_filter, 18, 27);
		$data_laporan_pembelian = $this->pembelian_master->get_laporan('_get_laporan_query', $tanggal_awal, $tanggal_akhir);
			
		$data = array();
		$no   = $_POST['start'];

		foreach ($data_laporan_pembelian as $dlp){
			$no++;
			$row 	= array();
			$row[] 	= $no;
			$status = $dlp->status_pembelian;

			//Awal tombol
			if($cek_useraccess_laporan->act_update == 1){
				$tombol_edit 	 = '<a class="btn btn-default btn-rounded btn-xs" onclick="edit_pembelian('."'".$dlp->id_pembelian_m."'".')">
								   		<i class="fa fa-pencil" style="color:blue;"></i>
								    </a>';
			}else{
				if($cek_useraccess_transaksi->act_update == 1 AND $status=="MENUNGGU" 
					OR $cek_useraccess_transaksi->act_create == 1 AND $status=="MENUNGGU"){
					$tombol_edit = '<a class="btn btn-default btn-rounded btn-xs" onclick="edit_pembelian('."'".$dlp->id_pembelian_m."'".')">
										<i class="fa fa-pencil" style="color:blue;"></i>
									</a>';
				}else{
					$tombol_edit = '';
				}
			}

			// if($cek_useraccess_laporan->act_delete == 1){
			// 	$tombol_hapus = '<a class="btn btn-default btn-rounded btn-xs" 
			// 						onclick="verifikasi_hapus_pembelian('."'".$dlp->id_pembelian_m."'".')">
			// 					 	<i class="fa fa-times" style="color:red;"></i>
			// 					 </a>';
			// }else{
			// 	$tombol_hapus = '';
			// 	if($cek_useraccess_transaksi->act_delete == 1 AND $status=="MENUNGGU"){
			// 		$tombol_hapus = '<a class="btn btn-default btn-rounded btn-xs" 
			// 							onclick="verifikasi_hapus_pembelian('."'".$dlp->id_pembelian_m."'".')">
			// 						 	<i class="fa fa-times" style="color:red;"></i>
			// 					 	 </a>';
			// 	}else{
			// 		$tombol_hapus = '';
			// 	}
			// }
			$tombol_hapus = '';

			$row[] = '
				'.$tombol_edit.'
				'.$tombol_hapus.'
			';
			// Akhir tombol

			$row[] 	= $dlp->no_pembelian;
			$row[] 	= $dlp->no_faktur;
			if($status == "SELESAI"){
				$row[] 	= '<span class="btn btn-success btn-xs btn-block btn-rounded text-center">'.$status.'</span>';
			}elseif($status=="MENUNGGU"){
				$row[] 	= '<span class="btn btn-danger btn-xs btn-block btn-rounded text-center">'.$status.'</span>';
			}
			$row[] 	= date('d-m-Y', strtotime($dlp->tanggal));
			$row[] 	= $dlp->nama_supplier;

			$row[] 	= '<span class="pull-right">'.number_format($dlp->jml_barang,'0', ',', '.').'</span>';

			if($this->session->userdata('usergroup_name') == 'Super Admin'){
				$row[] 	= '<span class="pull-right">'.number_format($dlp->total, '0', ',', '.').'</span>';
				$row[] 	= '<span class="pull-right">'.number_format($dlp->biaya_lain, '0', ',', '.').'</span>';
				$row[] 	= '<span class="pull-right">'.number_format($dlp->ppn, '0', ',', '.').'</span>';
				$row[] 	= '<span class="pull-right">'.number_format($dlp->grand_total, '0', ',', '.').'</span>';
			}

			$row[] 	= $dlp->keterangan_lain;
			$row[] 	= $dlp->pegawai_save;
			$row[] 	= $dlp->tanggal_pembuatan;
			$row[] 	= $dlp->pegawai_edit;
			$row[] 	= $dlp->tanggal_pembaharuan;
			$data[]	= $row;
		}

		$output = array(
			"draw"            => $_POST['draw'],
			"recordsTotal"    => $this->pembelian_master->count_all('vamr4846_vama.pembelian_master', $tanggal_awal, $tanggal_akhir),
			"recordsFiltered" => $this->pembelian_master->count_filtered('_get_laporan_query', $tanggal_awal, $tanggal_akhir),
			"data"            => $data
		);		
		echo json_encode($output);
	}

	public function ajax_list_detail()
	{
		// Cek user acces menu
		$id_pegawai 				= $this->session->userdata('id_pegawai');
		$cek_useraccess_transaksi 	= $this->useraccess_pusat->cek_access($id_pegawai, '25');
		$cek_useraccess_laporan 	= $this->useraccess_pusat->cek_access($id_pegawai, '36');
		if($cek_useraccess_laporan->act_read == '0' or $cek_useraccess_laporan->act_read == '-'){
			redirect(base_url().'dashboard');
		}

		$tanggal_filter 			= $this->input->post('tanggal_filter');
		if($tanggal_filter == ''){
			redirect(base_url().'laporan_pembelian');
			exit();
		}

		$tanggal_awal 				= substr($tanggal_filter, 0, 10);
		$tanggal_akhir 				= substr($tanggal_filter, 18, 27);
		$data_laporan_pembelian 	= $this->pembelian_master->get_laporan('_get_laporan_query_detail', $tanggal_awal, $tanggal_akhir);
			
		$data 						= array();
		$no 						= $_POST['start'];
		$no_pembelian 				= '0';

		foreach ($data_laporan_pembelian as $dlp) 
		{
			$no++;
			$row 	= array();
			$row[] 	= $no;
			$status = $dlp->status_pembelian;

			//Awal tombol
			if($cek_useraccess_laporan->act_update == 1){
				$tombol_edit = '<a class="btn btn-default btn-rounded btn-xs" onclick="edit_pembelian('."'".$dlp->id_pembelian_m."'".')">
									<i class="fa fa-pencil" style="color:blue;"></i>
								</a>';
			}else{
				if($cek_useraccess_transaksi->act_update == 1 AND $status=="MENUNGGU" 
					OR $cek_useraccess_transaksi->act_create == 1 AND $status=="MENUNGGU"){
					$tombol_edit = '<a class="btn btn-default btn-rounded btn-xs" onclick="edit_pembelian('."'".$dlp->id_pembelian_m."'".')">
										<i class="fa fa-pencil" style="color:blue;"></i>
									</a>';
				}else{
					$tombol_edit = '';
				}
			}

			// if($cek_useraccess_laporan->act_delete == 1){
			// 	$tombol_hapus = '<a class="btn btn-default btn-rounded btn-xs" 
			// 						onclick="verifikasi_hapus_pembelian('."'".$dlp->id_pembelian_m."'".')">
			// 					 	<i class="fa fa-times" style="color:red;"></i>
			// 					 </a>';
			// }else{
			// 	$tombol_hapus = '';
			// 	if($cek_useraccess_transaksi->act_delete == 1 AND $status=="MENUNGGU"){
			// 		$tombol_hapus = '<a class="btn btn-default btn-rounded btn-xs" 
			// 							onclick="verifikasi_hapus_pembelian('."'".$dlp->id_pembelian_m."'".')">
			// 						 	<i class="fa fa-times" style="color:red;"></i>
			// 					 	 </a>';
			// 	}else{
			// 		$tombol_hapus = '';
			// 	}
			// }
			$tombol_hapus = '';
			
			$row[] =	'<a class="btn btn-default btn-rounded btn-xs" onclick="cetak_barcode('."'".$dlp->id_pembelian_d."'".')">
							<i class="fa fa-barcode"></i>
						 </a>
						'.$tombol_edit.'
						'.$tombol_hapus.'
				  		';
			// Akhir tombol

			if($no_pembelian <> $dlp->no_pembelian){
				$row[] 	= '<span class="btn btn-primary btn-block btn-rounded btn-xs">'.$dlp->no_pembelian.'</span>';
			}else{
				$row[] 	= '<span class="btn btn-default btn-block btn-rounded btn-xs">'.$dlp->no_pembelian.'</span>';
			}

			if($status=="SELESAI"){
				$row[] 	= '<span class="btn btn-success btn-xs btn-block btn-rounded text-center">'.$status.'</span>';
			}elseif($status=="MENUNGGU"){
				$row[] 	= '<span class="btn btn-danger btn-xs btn-block btn-rounded text-center">'.$status.'</span>';
			}
			$row[] 	= date('d-m-Y', strtotime($dlp->tanggal));
			$row[] 	= $dlp->nama_supplier;
			$row[] 	= $dlp->sku;
			$row[] 	= $dlp->nama_barang;
			$row[] 	= '<span class="pull-right">'.number_format($dlp->jumlah_beli,'0', ',', '.').'</span>';

			if($this->session->userdata('usergroup_name') == 'Super Admin'){
				$row[] 	= '<span class="pull-right">'.number_format($dlp->harga, '0', ',', '.').'</span>';
				$row[] 	= '<span class="pull-right">'.$dlp->resistensi_harga.'</span>';
				$row[] 	= '<span class="pull-right">'.number_format($dlp->harga_bersih, '0', ',', '.').'</span>';
				$row[] 	= '<span class="pull-right">'.number_format($dlp->subtotal, '0', ',', '.').'</span>';
			}

			$row[] 	= $dlp->pegawai_save;
			$row[] 	= $dlp->tanggal_pembuatan;
			$row[] 	= $dlp->pegawai_edit;
			$row[] 	= $dlp->tanggal_pembaharuan;
			$data[]	= $row;
			$no_pembelian = $dlp->no_pembelian;
		}

		$output = array(
			"draw"            => $_POST['draw'],
			"recordsTotal"    => $this->pembelian_master->count_all('vamr4846_vama.pembelian_detail', $tanggal_awal, $tanggal_akhir),
			"recordsFiltered" => $this->pembelian_master->count_filtered('_get_laporan_query_detail', $tanggal_awal, $tanggal_akhir),
			"data"            => $data
			);		
		echo json_encode($output);
	}

	public function ajax_list_batal()
	{
		// Cek user acces menu
		$id_pegawai                 = $this->session->userdata('id_pegawai');
		$cek_useraccess 			= $this->useraccess_pusat->cek_access($id_pegawai, '36');
		if($cek_useraccess->act_read == '0' or $cek_useraccess->act_read == '-'){
			redirect(base_url().'dashboard');
		}

		$tanggal_filter 			= $this->input->post('tanggal_filter');
		if($tanggal_filter == ''){
			redirect(base_url().'laporan_pembelian');
			exit();
		}

		$tanggal_awal 				= substr($tanggal_filter, 0, 10);
		$tanggal_akhir 				= substr($tanggal_filter, 18, 27);
		$data_laporan_pembelian 	= $this->pembelian_master->get_laporan('_get_laporan_query_batal', $tanggal_awal, $tanggal_akhir);
			
		$data 						= array();
		$no 						= $_POST['start'];
		$no_pembelian 				= '0';

		foreach ($data_laporan_pembelian as $dlp) 
		{
			$no++;
			$row = array();
			$row[] = $no;

			if($no_pembelian <> $dlp->no_pembelian){
				$row[] 	= '<span class="btn btn-primary btn-block btn-rounded btn-xs">'.$dlp->no_pembelian.'</span>';
			}else{
				$row[] 	= '<span class="btn btn-default btn-block btn-rounded btn-xs">'.$dlp->no_pembelian.'</span>';
			}

			$row[] 	= $dlp->no_faktur;
			$row[] 	= $dlp->nama_supplier;
			$row[] 	= $dlp->sku;
			$row[] 	= $dlp->nama_barang;
			$row[] 	= '<span class="pull-right">'.number_format($dlp->jumlah_beli,'0', ',', '.').'</span>';

			if($this->session->userdata('usergroup_name') == 'Super Admin'){
				$row[] 	= '<span class="pull-right">'.number_format($dlp->harga, '0', ',', '.').'</span>';
				$row[] 	= '<span class="pull-right">'.number_format($dlp->resistensi_harga, '0', ',', '.').'</span>';
				$row[] 	= '<span class="pull-right">'.number_format($dlp->harga_bersih, '0', ',', '.').'</span>';
				$row[] 	= '<span class="pull-right">'.number_format($dlp->subtotal, '0', ',', '.').'</span>';
			}

			$row[] 	= $dlp->keterangan_batal;
			$row[] 	= $dlp->pegawai_pembatalan;
			$row[] 	= $dlp->tanggal_pembatalan;
			$row[] 	= $dlp->pegawai_save;
			$row[] 	= $dlp->tanggal_pembuatan;
			$row[] 	= $dlp->pegawai_edit;
			$row[] 	= $dlp->tanggal_pembaharuan;

			$data[]	= $row;
			$no_pembelian = $dlp->no_pembelian;
		}

		$output = array(
			"draw"            => $_POST['draw'],
			"recordsTotal"    => $this->pembelian_master->count_all('vamr4846_vama.pembelian_detail_batal', $tanggal_awal, $tanggal_akhir),
			"recordsFiltered" => $this->pembelian_master->count_filtered('_get_laporan_query_batal', $tanggal_awal, $tanggal_akhir),
			"data"            => $data
		);		
		echo json_encode($output);
	}

	public function ambil_total()
	{
		// Cek user acces menu
		$id_pegawai     = $this->session->userdata('id_pegawai');
		$cek_useraccess = $this->useraccess_pusat->cek_access($id_pegawai, '36');
		if($cek_useraccess->act_read == '0' or $cek_useraccess->act_read == '-'){
			redirect(base_url().'dashboard');
		}

		if($this->session->userdata('usergroup_name') !== 'Super Admin'){
			redirect(base_url().'laporan_pembelian');
			exit();
		}

		$tanggal_filter 			= $this->input->post('tanggal_filter');
		if($tanggal_filter == ''){
			redirect(base_url().'laporan_pembelian');
			exit();
		}

		$tanggal_awal 				= substr($tanggal_filter, 0, 10);
		$tanggal_akhir 				= substr($tanggal_filter, 18, 27);

		// Ambil jumlah transaksi, total tunai, debit dan total uang masuk
		$data_uang_masuk 			= $this->pembelian_master->get_grand_total($tanggal_awal, $tanggal_akhir);
		if ($data_uang_masuk){
			$jml_transaksi			= number_format($data_uang_masuk->jml_transaksi,'0', ',', '.');
			$grand_total_pembelian 	= '<span>Rp. <p class="pull-right">'.number_format($data_uang_masuk->grand_total_pembelian,'0', ',', '.').'</p></span>';
		}else{
			$jml_transaksi			= '0';
			$grand_total_pembelian 	= '0';
		}
		
		echo json_encode(array(
			'status' 				=> 1,
			"jml_transaksi"			=> $jml_transaksi,
			"grand_total_pembelian"	=> $grand_total_pembelian
		));
	}

	public function ajax_verifikasi_hapus_transaksi()
	{		
		$id_pembelian_m 	= $this->input->post('id_pembelian_m');
		if($id_pembelian_m == ''){
			redirect(base_url().'laporan_pembelian');
			exit();
		}

		// Cek user acces menu
		$id_pegawai 		= $this->session->userdata('id_pegawai');
		$cek_useraccess 	= $this->useraccess_pusat->cek_access($id_pegawai, '36');
		$data_master 		= $this->pembelian_master->get_master2($id_pembelian_m);
		if($data_master){
			if($data_master->status_pembelian == 'SELESAI'){
				if($cek_useraccess->act_delete == '0' or $cek_useraccess->act_delete == '-'){
					echo json_encode(array(
						"status" 		=> 0,
					  	"info_pesan" 	=> "Maaf anda tidak diberikan izin untuk menghapus transaksi pembelian,
											dengan status pembelian : SELESAI !",
						"pesan" 		=> "
												No. Pembelian : ".$data_master->no_pembelian."
												No. Faktur : ".$data_master->no_faktur."
												Status Pembelian : ".$data_master->status_pembelian."
											",
						"tipe_pesan" 	=> "error"
					));
					exit();
				}
			}
		}

		// Awal validasi stok
		$data_pembelian_detail 		= $this->pembelian_detail->get_pembelian_detail_all($id_pembelian_m);
		foreach($data_pembelian_detail AS $dpd){
			$id_barang_pusat 		= $dpd->id_barang_pusat;
			$jumlah_beli 			= $dpd->jumlah_beli;

			// Ambil data barang dari master barang pusat
			$data_barang 			= $this->barang_pusat->get_id($id_barang_pusat);
			if($data_barang){
				$sku 				= $data_barang->sku;
				$nama_barang 		= $data_barang->nama_barang;
				$stok_sekarang 		= $data_barang->total_stok;

				$validasi_stok1 	= $stok_sekarang - $jumlah_beli;
				$validasi_stok2 	= $validasi_stok1 + 0;

				if($validasi_stok2 < 0){
					echo json_encode(array(
						"status" 		=> 0,
					  	"info_pesan" 	=> "Gagal!",
						"pesan" 		=> "
												Maaf anda tidak bisa menghapus transaksi pembelian ini  !
												Karna akan menyebabkan stok barang ini menjadi minus : 
												No. Pembelian : ".$dpd->no_pembelian."
												SKU : ".$sku."
												Nama Barang : ".$nama_barang."
												Total Minus : ".$validasi_stok2."
											",
						"tipe_pesan" 	=> "error"
					));
					exit();
				}
			}else{
				exit();
			}
		}
		// Akhir validasi stok

		if($data_master){
			$jumlah_barang 	= number_format($data_master->jumlah_barang,'0',',','.');
			echo json_encode(array(
				"status" 	=> 1,
				'pesan' 	=>	"<table class='table' id='detail_hapus_pembelian'>
									<thead>
									</thead>
									<tbody>
										<tr>
											<td>
												<small>No. Pembelian : </small>
												<b class='text-dark'>$data_master->no_pembelian </b></br>
												
												<small>No. Faktur : </small>
												<b class='text-dark'>$data_master->no_faktur </b></br></br>

												<small>Nama Supplier : </small></br>	
												<b class='text-dark'>$data_master->nama_supplier </b> </br></br>

												<small class='text-dark'>Jumlah Barang : </small> <br/>	
												<h2 class='text-primary'>$jumlah_barang </h2>

												<textarea name='keterangan_batal' id='keterangan_batal' class='form-control input-md' placeholder='Keterangan batal' style='resize: vertical;'></textarea>
											</td>
										</tr>
									</tbody>
								</table>",

				'footer'	=> 	"<button onclick='hapus_pembelian($data_master->id_pembelian_m)' type='button' class='btn btn-primary 
								 waves-effect waves-light' data-dismiss='modal' autofocus>Iya, Hapus</button> 
								<button type='button' class='btn btn-default waves-effect' data-dismiss='modal'>Batal</button>"
			));
		}else{
			redirect(base_url().'laporan_pembelian');
		}
	}

	public function ajax_hapus_transaksi()
	{
		if($this->input->is_ajax_request()){
			$id_pembelian_m 		= $this->input->post('id_pembelian_m');
			if($id_pembelian_m == ''){
				redirect(base_url().'laporan_pembelian');
				exit();
			}

			// Cek user acces menu
			$id_pegawai     = $this->session->userdata('id_pegawai');
			$cek_useraccess = $this->useraccess_pusat->cek_access($id_pegawai, '36');
			$master         = $this->pembelian_master->get_master2($id_pembelian_m);
			$no_pembelian   = $master->no_pembelian;
			$no_faktur      = $master->no_faktur;
			$id_supplier    = $master->id_supplier;

			if($master){
				if($master->status_pembelian == 'SELESAI'){
					if($cek_useraccess->act_delete == '0' or $cek_useraccess->act_delete == '-'){
						echo json_encode(array(
							"status" 		=> 0,
						  	"info_pesan" 	=> "Maaf anda tidak diberikan izin untuk menghapus transaksi pembelian,
												dengan status pembelian : SELESAI !",
							"pesan" 		=> "
													No. Pembelian : ".$master->no_pembelian."
													No. Faktur : ".$master->no_faktur."
													Status Pembelian : ".$master->status_pembelian."
												",
							"tipe_pesan" 	=> "error",
							"gaya_tombol"	=> "btn-danger btn-md waves-effect waves-light"
						));
						exit();
					}
				}
			}

			// Awal validasi stok
			$data_pembelian_detail 	= $this->pembelian_detail->get_pembelian_detail_all($id_pembelian_m);
			foreach($data_pembelian_detail AS $dpd){
				$id_barang_pusat 		= $dpd->id_barang_pusat;
				$jumlah_beli 			= $dpd->jumlah_beli;

				// Ambil data barang dari master barang pusat
				$data_barang 			= $this->barang_pusat->get_id($id_barang_pusat);
				if($data_barang){
					$sku 				= $data_barang->sku;
					$nama_barang 		= $data_barang->nama_barang;
					$stok_sekarang 		= $data_barang->total_stok;

					$validasi_stok1 	= $stok_sekarang - $jumlah_beli;
					$validasi_stok2 	= $validasi_stok1 + 0;

					if($validasi_stok2 < 0){
						echo json_encode(array(
							"status" 		=> 0,
						  	"info_pesan" 	=> "Gagal!",
							"pesan" 		=> "
													Maaf anda tidak bisa menghapus transaksi pembelian ini !
													Karna akan menyebabkan stok barang ini menjadi minus : 
													SKU : ".$sku."
													Nama Barang : ".$nama_barang."
													Total Minus : ".$validasi_stok2."
												",
							"tipe_pesan" 	=> "error",
							"gaya_tombol"	=> "btn-danger btn-md waves-effect waves-light"
						));
						exit();
					}
				}else{
					exit();
				}
			}
			// Akhir validasi stok
		

			$id_pegawai_pembatalan 	= $this->session->userdata('id_pegawai');
			$keterangan_batal 		= $this->input->post('keterangan_batal');
			$hapus 					= $this->pembelian_master->hapus_transaksi($id_pembelian_m, $id_supplier, 
																		   $no_pembelian, $no_faktur,
																		   $keterangan_batal, $id_pegawai_pembatalan);
			if($hapus){
				echo json_encode(array(
					"status" 		=> 1,
					"info_pesan" 	=> "Berhasil!",
					"pesan"			=> "Transaksi pembelian berhasil dihapus,
									    Dengan no. pembelian ".$no_pembelian."",
					"tipe_pesan" 	=> 'success',
					"gaya_tombol"	=> "btn-success btn-md waves-effect waves-light"
				));
			}else{
				echo json_encode(array(
					"status"		=> 0,
					"info_pesan" 	=> "Gagal!",
					"pesan" 		=> "Terjadi kesalahan, coba lagi !",
					"tipe_pesan" 	=> "error",
					"gaya_tombol"	=> "btn-danger btn-md waves-effect waves-light"
				));
			}
		}else{
			redirect(base_url().'laporan_pembelian');
		}
	}
}
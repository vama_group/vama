<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Perakitan extends MY_Controller 
{
	function __construct()
	{
		parent::__construct();
		$this->CI =& get_instance();
		$this->load->model('pegawai_pusat_model','pegawai_pusat');
        $this->load->model('useraccess_pusat_model','useraccess_pusat');
		$this->load->model('supplier_model','supplier');
		$this->load->model('barang_pusat_model','barang_pusat');
		$this->load->model('pegawai_pusat_model','pegawai_pusat');
		$this->load->model('perakitan_master_model','perakitan_master');
		$this->load->model('perakitan_detail_model','perakitan_detail');
	}

	public function index()
	{
		// Cek user acces menu
		$id_pegawai = $this->session->userdata('id_pegawai');
		if($id_pegawai == ''){
			redirect(base_url().'login');
		}
		
		$cek_useraccess = $this->useraccess_pusat->cek_access($id_pegawai, '29');
		if($cek_useraccess){
			if($cek_useraccess->act_read == '0' or $cek_useraccess->act_read == '-'){
				redirect(base_url().'dashboard');
			}					
		}else{
			redirect(base_url().'dashboard');
		}
		
		$data_pegawai = $this->pegawai_pusat->get_by_id($id_pegawai);
		if($data_pegawai){
			$cek_useraccess_laporan    = $this->useraccess_pusat->cek_access($id_pegawai, '44');
			$data['data_pegawai']      = $data_pegawai;
			$data['data_induk_menu']   = $this->useraccess_pusat->get_induk_menu($id_pegawai);
			$data['access_create']     = $cek_useraccess->act_create;
			$data['access_update']     = $cek_useraccess->act_update;
			$data['access_laporan']    = $cek_useraccess_laporan->act_read;
			
			$data['atribut_halaman']   = 'Perakitan';
			$data['id_perakitan_m']    = '0';
			$data['halaman_transaksi'] = $this->load->view('admin/transaksi/perakitan/transaksi',$data,true);
			$data['halaman_plugin']    = $this->load->view('admin/transaksi/perakitan/plugin',$data,true);
			$data['isi_halaman']       = $this->load->view('admin/transaksi/perakitan/index',$data,true);
			$this->load->view('admin/layout',$data);
		}else{
			redirect(base_url().'login');
		}
	}

	public function no_perakitan_baru($id_pegawai){
		if($id_pegawai == ''){
			redirect(base_url().'perakitan');
			exit();
		}

		$tanggal_sekarang = date('Y-m-d');
		$bulan_sekarang   = date('m');
		$tahun_sekarang   = date('Y');
		$tahun_sekarang_2 = date('y'); 
		
		$inisial          = "PR";
		$akhir            = $this->perakitan_master->no_perakitan_baru($tahun_sekarang, $bulan_sekarang, $id_pegawai);
		
		if($akhir){
			$no_baru = $akhir->no_baru;
		}else{
			$no_baru = 1;
		}
		
		if($no_baru < 10){
			$depan = '000';
		}elseif($no_baru< 100) {
			$depan = '00';
		}elseif($no_baru< 1000) {
			$depan = '0';
		}else{
			$depan = '';
		}
		
		$no_perakitan_baru = $inisial.'-'.$bulan_sekarang.$tahun_sekarang_2.'-'.$id_pegawai.'-'.$depan.$no_baru;
		return $no_perakitan_baru;
	}

	public function transaksi()
	{
		$id_perakitan_m = $this->input->get('id');
		$data_perakitan = $this->perakitan_master->get_master($id_perakitan_m);
		if($data_perakitan->id_perakitan_m == ''){
			redirect(base_url().'perakitan');
			exit();
		}

		// Cek user acces menu
		$id_pegawai             = $this->session->userdata('id_pegawai');
		$cek_useraccess_laporan = $this->useraccess_pusat->cek_access($id_pegawai, '44');
		$data['access_laporan'] = $cek_useraccess_laporan->act_read;
		
		if($cek_useraccess_laporan->act_update == '1' or $data_perakitan->status_perakitan == 'MENUNGGU'){		
			$cek_useraccess            = $this->useraccess_pusat->cek_access($id_pegawai, '29');
			$data['access_create']     = $cek_useraccess->act_create;
			$data['access_update']     = $cek_useraccess->act_update;
			
			// cari data pegawai
			$id_pegawai                = $this->session->userdata('id_pegawai');
			$data_pegawai              = $this->pegawai_pusat->get_by_id($id_pegawai);
			$this->CI->session->set_userdata('usergroup_name', $data_pegawai->usergroup_name);
			$data['data_pegawai']      = $data_pegawai;
			$data['data_induk_menu']   = $this->useraccess_pusat->get_induk_menu($id_pegawai);
			
			$data['atribut_halaman']   = 'Perakitan';
			$data['id_perakitan_m']    = $id_perakitan_m;
			$data['no_perakitan']      = $data_perakitan->no_perakitan;
			$data['halaman_transaksi'] = $this->load->view('admin/transaksi/perakitan/transaksi',$data,true);
			$data['halaman_plugin']    = $this->load->view('admin/transaksi/perakitan/plugin',$data,true);
			$data['isi_halaman']       = $this->load->view('admin/transaksi/perakitan/index',$data,true);
			$this->load->view('admin/layout',$data);
		}else{
			redirect(base_url().'perakitan');
			exit();
		}				
	}

	public function simpan_transaksi(){
		if( ! empty($_POST['id_perakitan_m'])){
			// Cek user acces menu
			$id_pegawai     = $this->session->userdata('id_pegawai');
			$cek_useraccess = $this->useraccess_pusat->cek_access($id_pegawai, '29');

			if($cek_useraccess->act_create == '1' or $cek_useraccess->act_update == '1'){
				$id_perakitan_m = $this->input->post('id_perakitan_m');				
				$data_master    = $this->perakitan_master->get_by_id($id_perakitan_m);

				if($data_master){
					$id_barang_perakitan = $data_master->id_barang_pusat;
					$no_perakitan        = $data_master->no_perakitan;
					$sku                 = $data_master->sku;
					$nama_barang         = $data_master->nama_barang;
					$jumlah_perakitan    = $data_master->jumlah_perakitan;	
					$catatan             = $this->clean_tag_input($this->input->post('catatan')); 
				}else{
					echo json_encode(array(	'status' 	=> 0,
											'pesan'		=> 'Data perakitan tidak ditemukan!'
									));
					exit();
				}
				
				$master = $this->perakitan_master->update_master(
					$id_perakitan_m, $catatan, $id_pegawai, 'SELESAI'
				);

				if($master){
					echo json_encode(array('status'	 	=> 1, 
										   'pesan' 		=> "	Transaksi berhasil disimpan,
										   						No. Perakitan : ".$no_perakitan.",
										   						SKU : ".$sku.",
										   						Nama Barang : ".$nama_barang.",
										   						Jumlah Perakitan : ".$jumlah_perakitan.",
										   						Apakah anda ingin mencetak fakturnya ?
										   					"
									));
				}else{
					echo json_encode(array(	'status' 	=> 0,
											'pesan'		=> 'Terjadi kesalah saat menyimpan transaksi, harap coba kembali.'
									));
				}
			}else{
				echo json_encode(array(	'status' 	=> 0,
										'pesan'		=> 'Maaf anda tidak diizinkan untuk menyimpan transaksi perakitan.'
								));
			}
		}else{
			redirect(base_url().'perakitan');
		}
	}

	public function tahan_transaksi()
	{
		if( ! empty($_POST['id_perakitan_m'])){
			// Cek user acces menu
			$id_pegawai     = $this->session->userdata('id_pegawai');
			$cek_useraccess = $this->useraccess_pusat->cek_access($id_pegawai, '29');

			if($cek_useraccess->act_create == '1' or $cek_useraccess->act_update == '1'){
				$id_perakitan_m = $this->input->post('id_perakitan_m');				
				$data_master    = $this->perakitan_master->get_by_id($id_perakitan_m);

				if($data_master){
					$id_barang_perakitan = $data_master->id_barang_pusat;
					$no_perakitan        = $data_master->no_perakitan;
					$sku                 = $data_master->sku;
					$nama_barang         = $data_master->nama_barang;
					$jumlah_perakitan    = $data_master->jumlah_perakitan;	
					$catatan             = $this->clean_tag_input($this->input->post('catatan')); 
				}else{
					echo json_encode(array(	'status' 	=> 0,
											'pesan'		=> 'Data perakitan tidak ditemukan!'
									));
					exit();
				}
				
				$master = $this->perakitan_master->update_master(
					$id_perakitan_m, $catatan, $id_pegawai, 'MENUNGGU'
				);

				if($master){
					echo json_encode(array('status'	 	=> 1, 
										   'pesan' 		=> "	Transaksi berhasil ditahan,
										   						No. Perakitan : ".$no_perakitan.",
										   						SKU : ".$sku.",
										   						Nama Barang : ".$nama_barang.",
										   						Jumlah Perakitan : ".$jumlah_perakitan.",
										   						Apakah anda ingin mencetak fakturnya ?
										   					"
									));
				}else{
					echo json_encode(array(	'status' 	=> 0,
											'pesan'		=> 'Terjadi kesalah saat menahan transaksi, harap coba kembali.'
									));
				}
			}else{
				echo json_encode(array(	'status' 	=> 0,
										'pesan'		=> 'Maaf anda tidak diizinkan untuk menahan transaksi perakitan.'
								));
			}
		}else{
			redirect(base_url().'perakitan');
		}
	}

	public function ambil_data()
	{
		if($this->input->is_ajax_request()){
			// Cek user acces menu
			$id_pegawai     = $this->session->userdata('id_pegawai');
			$cek_useraccess = $this->useraccess_pusat->cek_access($id_pegawai, '29');
			if($cek_useraccess->act_read == '0' or $cek_useraccess->act_read == '-'){
				redirect(base_url().'perakitan');
				exit();
			}

			$id_perakitan_m = $this->input->post('id_perakitan_m');
			$data           = $this->perakitan_master->get_master($id_perakitan_m);		
			$dtm            = $this->perakitan_detail->get_total($id_perakitan_m);

			echo json_encode(array(
								'id_perakitan_m'      => $data->id_perakitan_m,
								'no_perakitan'        => $data->no_perakitan,
								'id_barang_pusat'     => $data->id_barang_pusat,
								'sku'                 => $data->sku,
								'nama_barang'         => $data->nama_barang,
								'harga_eceran'        => $data->harga_eceran,
								'jumlah_perakitan'    => $data->jumlah_perakitan,
								'keterangan_lain'     => $data->keterangan_lain,
								'jumlah_barang'       => $dtm->jumlah_barang,
								'jumlah_barang_batal' => $dtm->jumlah_barang_batal
							));
		}else{
			redirect(base_url().'perakitan');
		}
	}

	public function update_stok_perakitan()
	{
		if($this->input->is_ajax_request()){
			$id_pegawai     = $this->session->userdata('id_pegawai');
			$cek_useraccess = $this->useraccess_pusat->cek_access($id_pegawai, '29');
			if($cek_useraccess->act_update == '0' or $cek_useraccess->act_update == '-'){
				echo json_encode(array( 
					'status' => 0,
					'pesan'  => "Maaf anda tidak izinkan untuk memperbaharui
								 jumlah perakitan"
			  	));
				exit();
			}

			$jumlah_perakitan = $this->input->post('jumlah_perakitan');
			$id_perakitan_m   = $this->input->post('id_perakitan_m');

			// Validasi jumlah perakitan tidak boleh kosong
			if($jumlah_perakitan == '' or $jumlah_perakitan <='0' ){
				echo json_encode(array( 
					'status' => 0,
					'pesan'  => "Harap masukan jumlah perakitan !"
				));
				exit();
			}

			$data_perakitan_master = $this->perakitan_master->get_by_id($id_perakitan_m);
			if ($data_perakitan_master){
				$id_master             = $data_perakitan_master->id_perakitan_m;
				$id_barang_perakitan   = $data_perakitan_master->id_barang_pusat;
				$jumlah_perakitan_lama = $data_perakitan_master->jumlah_perakitan;
			}else{
				echo json_encode(array( 
					'status' => 0,
					'pesan'  => "Transaksi perakitan tidak ditemukan !"
			  	));
				exit();
			}
			
			// Validasi stok per komponen sebelum diupdate stoknya secara masal 
			$list = $this->perakitan_detail->get_detail($id_master);
			foreach ($list as $pd){
				$barang_pusat  = $this->barang_pusat->get_id($pd->id_barang_pusat);
				$stok_sekarang = $barang_pusat->total_stok + $jumlah_perakitan_lama;
				
				if($jumlah_perakitan > $stok_sekarang){
					echo json_encode(array(
						'status'      => 2,
						'stok_skrng'  => $barang_pusat->total_stok,
						'jumlah_lama' => $jumlah_perakitan_lama,
						'pesan'       => "	<b class='text-danger'>Maaf anda tidak bisa memperbaharui jumlah perakitan, 
											dengan jumlah : </b></br>
											<h1 class='text-danger'>".$jumlah_perakitan."</h1>
											<hr>
											Karna,
											SKU : </br>
											<b class ='text-dark'>".$barang_pusat->sku."</b><br/><br/>
										
											Nama Barang : <br/>
											<b class ='text-dark'>".$barang_pusat->nama_barang."</b><br/><br/>
										
											saat ini hanya tersisa  : </br> 
											<h1 class ='text-danger'>".$stok_sekarang."</h1>"
					));
					exit();
				}
			}

			$this->perakitan_master->update_qty_perakitan(
				$id_master, $id_barang_perakitan, $jumlah_perakitan, $jumlah_perakitan_lama, $id_pegawai
			);
			echo json_encode(array(
									'status' 				=> 1,
									'id_barang_perakitan'	=> $id_barang_perakitan, 
									'jumlah_perakitan' 		=> $jumlah_perakitan,
									'jumlah_perakitan_lama' => $jumlah_perakitan_lama
								   )
							);
		}
	}

	public function cek_stok_komponen()
	{
		if($this->input->is_ajax_request()){
			// Cek user acces menu
			$id_pegawai     = $this->session->userdata('id_pegawai');
			$cek_useraccess = $this->useraccess_pusat->cek_access($id_pegawai, '29');
			if($cek_useraccess->act_create == '0' AND $cek_useraccess->act_update == '0'){
				echo json_encode(array(
					'status'  => 0,
					'pesan'   => "Maaf anda tidak diizinkan 
								  untuk menambahkan atau memperbaharui komponen perakitan",
					'arahkan' => "halaman_utama"
				));
				exit();
			}

			$id_barang_komponen = $this->input->post('id_barang_komponen');
			if($id_barang_komponen == '' or $id_barang_komponen == '0'){
				echo json_encode(array(	
					'status' => 0,
					'pesan'  => 'Harap pilih komponen barang terlebih dahulu.',
					'arahkan' => "halaman_utama"
				));
				exit();
			}

			$jumlah_perakitan   = $this->input->post('jumlah_perakitan');
			$id_perakitan_m     = $this->clean_tag_input($this->input->post('id_perakitan_m'));

			$data_perakitan_master = $this->perakitan_master->get_by_id($id_perakitan_m);
			if($data_perakitan_master){
				$id_master        = $data_perakitan_master->id_perakitan_m;
				$jumlah_perakitan = $data_perakitan_master->jumlah_perakitan;
			}else{
				$id_master        = '';
				
				// Validasi jumlah perakitan tidak boleh kosong
				if($jumlah_perakitan == '' or $jumlah_perakitan <='0'){
					echo json_encode(array(
						'status'  => 0,
						'pesan'   => "Harap masukan jumlah perakitan !",
						'arahkan' => "jumlah_perakitan"
					));
					exit();
				}
			}
			
			if($id_master){
				$data_perakitan_detail = $this->perakitan_detail->get_id($id_master, $id_barang_komponen);
				if($data_perakitan_detail){
					$jumlah_komponen_lama = $data_perakitan_detail->jumlah_komponen;	
				}else{
					$jumlah_komponen_lama = '0';
				}
			}else{
				$jumlah_komponen_lama = '0';
			}

			$get_stok_komponen = $this->barang_pusat->get_stok_id($id_barang_komponen, $jumlah_komponen_lama);
			$stok_sekarang     = $get_stok_komponen->total_stok;

			if($jumlah_perakitan > $stok_sekarang){
				echo json_encode(array(
				'status'      => 0,
				'stok_skrng'  => $stok_sekarang,
				'jumlah_lama' => $jumlah_komponen_lama,
				'pesan'       => "Stok untuk :
								  ".$get_stok_komponen->nama_barang." 
								  Saat ini hanya tersisa  : 
								  ".$stok_sekarang."",
				'arahkan'     => "jumlah_perakitan"
				));
			}else{
				echo json_encode(array('status' => 1));
			}
		}else{
			redirect(base_url().'perakitan');
		}
	}

	public function simpan_detail_komponen()
	{
		if($this->input->is_ajax_request()){
			// Cek user acces menu
			$id_pegawai          = $this->session->userdata('id_pegawai');
			$cek_useraccess      = $this->useraccess_pusat->cek_access($id_pegawai, '29');
			
			// Ambil data perakitan master
			$id_perakitan_m      = $this->input->post('id_perakitan_m');
			$id_barang_perakitan = $this->clean_tag_input($this->input->post('id_barang_perakitan'));
			$jumlah_perakitan    = $this->input->post('jumlah_perakitan');

			$tanggal             = date('Y-m-d H:i:s');
			$catatan             = $this->clean_tag_input($this->input->post('catatan'));

			// Validasi perakitan master sudah ada atau belum
			$data_perakitan_master	= $this->perakitan_master->get_by_id($id_perakitan_m);
			if($data_perakitan_master){
				if($cek_useraccess->act_create == '1' or $cek_useraccess->act_update == '1'){
					$id_master                = $data_perakitan_master->id_perakitan_m;
					$id_barang_perakitan_lama = $data_perakitan_master->id_barang_pusat;
					$sku_perakitan            = $data_perakitan_master->sku;
					$nama_barang_perakitan    = $data_perakitan_master->nama_barang;
					$harga_eceran_perakitan   = $data_perakitan_master->harga_eceran;
					$jumlah_perakitan         = $data_perakitan_master->jumlah_perakitan;
				}else{
					echo json_encode(array(	
						'status'  => 0,
						'pesan'   => 'Maaf anda tidak diizinkan untuk menyimpan data perakitan barang.',
						'arahkan' => "halaman_utama"
					));
					exit();
				}
			}else{
				$id_master         = '';
				$data_barang_pusat = $this->barang_pusat->get_id($id_barang_perakitan);
				if($data_barang_pusat){
					$sku_perakitan          = $data_barang_pusat->sku;
					$nama_barang_perakitan  = $data_barang_pusat->nama_barang;
					$harga_eceran_perakitan = $data_barang_pusat->harga_eceran;
				}else{
					echo json_encode(array(
						'status'  => 0,
						'pesan'   => "Barang perakitan tidak dikenal !
									  harap periksa kembali sku barang perakitan",
						'arahkan' => "halaman_utama"
					));
					exit();
				}
			}

			if($id_master){
				if($cek_useraccess->act_create == '1' or $cek_useraccess->act_update == '1'){
					// Update perakitan master
					$master = $this->perakitan_master->update_master(
						$id_master, $catatan, $id_pegawai, 'MENUNGGU'
					);
				}else{
					echo json_encode(array(	
						'status'  => 0,
						'pesan'   => 'Maaf anda tidak diizinkan untuk memperbaharui transaksi perakitan.',
						'arahkan' => "halaman_utama"
					));
					exit();
				}
			}else{
				if($cek_useraccess->act_create == '1'){	
					// Insert perakitan master
					$no_perakitan = $this->no_perakitan_baru($id_pegawai);
					$master       = $this->perakitan_master->insert_master(
						$no_perakitan, $id_barang_perakitan, $jumlah_perakitan,
						$catatan, $tanggal, $id_pegawai
					);

					$data_perakitan_master = $this->perakitan_master->get_by_no_trans($no_perakitan);
					$id_master             = $data_perakitan_master->id_perakitan_m;
				}else{
					echo json_encode(array(	
						'status'  => 0,
						'pesan'   => 'Maaf anda tidak diizinkan untuk menambahkan transaksi perakitan.',
						'arahkan' => "halaman_utama"
					));
					exit();
				}
			}

			if($id_master){
				// Ambil data perakitan detail
				$id_barang_komponen = $this->input->post('id_barang_komponen');

				// Validasi insert atau update di perakitan detail
				$data_perakitan_detail = $this->perakitan_detail->get_id($id_master, $id_barang_komponen);
				if ($data_perakitan_detail){
					$id_detail            = $data_perakitan_detail->id_perakitan_d;
					$jumlah_komponen_lama = $data_perakitan_detail->jumlah_komponen;
				}else{
					$id_detail            = '';
					$jumlah_komponen_lama = '';
				}

				if($id_detail){
					// update dan kembalikan stok sebelumnya
					$simpan_data = $this->perakitan_detail->update_detail(
						$id_master, $id_barang_komponen, $jumlah_perakitan, $id_pegawai, $jumlah_komponen_lama
					);
				}else{
					// insert
					$simpan_data = $this->perakitan_detail->insert_detail(
						$id_master, $id_barang_komponen, $jumlah_perakitan, $id_pegawai
					);				
				}

				if($simpan_data){			
					$dtm = $this->perakitan_detail->get_total($id_master);
					if($dtm){
						$jumlah_barang       = $dtm->jumlah_barang;
						$jumlah_barang_batal = $dtm->jumlah_barang_batal;
					}else{
						$jumlah_barang       = '0';
						$jumlah_barang_batal = '0';
					}

					echo json_encode(array(
						'status'                 => 1,
						'url'                    => 'perakitan/transaksi/?&id='.$id_master,
						'id_pr'                  => $id_master,
						'id_barang_perakitan'    => $id_barang_perakitan,
						'sku_perakitan'          => $sku_perakitan,
						'nama_barang_perakitan'  => $nama_barang_perakitan,
						'harga_eceran_perakitan' => number_format($harga_eceran_perakitan, '0', ',', '.'),
						'jumlah_perakitan'       => $jumlah_perakitan, 
						'jumlah_barang'			 => $jumlah_barang,
						'jumlah_barang_batal' 	 => $jumlah_barang_batal
					));
				}else{
					echo json_encode(array(
						'status'  => 0,
						'pesan'   => 'Data komponen perakitan gagal disimpan',
						'arahkan' => "halaman_utama"
					));
				}
			}else{
				echo json_encode(array(
					'status'  => 0,
					'pesan'   => 'Transaksi perakitan gagal disimpan',
					'arahkan' => "halaman_utama"
				));
			}
		}else{
			redirect(base_url().'perakitan');
		}
	}

	public function ajax_list()
	{
		if($this->input->is_ajax_request()){
			// Cek user acces menu
			$id_pegawai     = $this->session->userdata('id_pegawai');
			$cek_useraccess = $this->useraccess_pusat->cek_access($id_pegawai, '29');
			if($cek_useraccess->act_read == '0' or $cek_useraccess->act_read == '-'){
				redirect(base_url().'dashboard');
			}

			$id_perakitan_m = $this->clean_tag_input($this->input->post('id_perakitan_m'));
			if($id_perakitan_m == ''){
				exit();
			}

			$data_perakitan_master = $this->perakitan_master->get_by_id($id_perakitan_m);
			if($data_perakitan_master){
				$id_master = $data_perakitan_master->id_perakitan_m;
			}else{
				$id_master = '0';
			}

			$list = $this->perakitan_detail->get_datatables('_get_datatables_query', $id_master, '');
			$data = array();
			$no   = $_POST['start'];

			foreach ($list as $pd){
				$no++;
				$row   = array();
				$row[] = $no;

				//add html for action
				if($cek_useraccess->act_delete == 1){
					$tombol_hapus = '<a class="btn btn-rounded btn-default btn-xs" 
										onclick="verifikasi_hapus_detail('."'".$pd->id_perakitan_d."'".')">
										<i class="fa fa-times" style="color:red;"></i>
									</a>';
				}else{
					$tombol_hapus = '<b class="btn btn-rounded btn-default btn-xs text-muted text-center">Tidak ada akses</b>';
				}

				$row[]  = ''.$tombol_hapus.'';
				$row[]  = $pd->sku;
				$row[]  = $pd->nama_barang;
				$row[]  = '<span class="pull-right">'.number_format($pd->harga_eceran,'0',',','.').'</span>';
				$row[]  = '<span class="pull-right">'.number_format($pd->jumlah_komponen,'0',',','.').'</span>';

				$row[]  = $pd->pegawai_save;
				$row[]  = $pd->tanggal_pembuatan;
				$row[]  = $pd->pegawai_edit;
				$row[]  = $pd->tanggal_pembaharuan;
				$data[] = $row;
			}

			$output = array(	
				"draw"            => $_POST['draw'],
				"recordsTotal"    => $this->perakitan_detail->count_all('vamr4846_vama.perakitan_detail', $id_master, ''),
				"recordsFiltered" => $this->perakitan_detail->count_filtered('_get_datatables_query', $id_master, ''),
				"data"            => $data
			);		
			echo json_encode($output);
		}else{
			redirect(base_url().'perakitan');
		}
	}

	public function ajax_list_batal()
	{
		if($this->input->is_ajax_request()){
			// Cek user acces menu
			$id_pegawai     = $this->session->userdata('id_pegawai');
			$cek_useraccess = $this->useraccess_pusat->cek_access($id_pegawai, '29');
			if($cek_useraccess->act_read == '0' or $cek_useraccess->act_read == '-'){
				redirect(base_url().'dashboard');
			}

			$id_perakitan_m = $this->clean_tag_input($this->input->post('id_perakitan_m'));
			if($id_perakitan_m == ''){
				exit();
			}

			$data_perakitan_master = $this->perakitan_master->get_by_id($id_perakitan_m);
			if($data_perakitan_master){
				$id_master = $data_perakitan_master->id_perakitan_m;
			}else{
				$id_master = '0';
			}

			$list = $this->perakitan_detail->get_datatables('_get_datatables_query_batal', $id_master, '');
			$data = array();
			$no   = $_POST['start'];

			foreach ($list as $pd){
				$no++;
				$row   = array();
				$row[] = $no;

				//add html for action
				if($cek_useraccess->act_create == 1){
					$tombol_pulihkan = '<a class="btn btn-rounded btn-default btn-xs" 
										onclick="pulihkan_data_detail('."'".$pd->id_perakitan_d."'".')">
										<i class="fa fa-undo" style="color:blue;"></i>
										</a>';
				}else{
					$tombol_pulihkan = '<b class="btn btn-rounded btn-default btn-xs text-muted text-center">Tidak ada akses</b>';
				}

				$row[]  = ''.$tombol_pulihkan.'';
				$row[]  = $pd->sku;
				$row[]  = $pd->nama_barang;
				$row[]  = '<span class="pull-right">'.number_format($pd->harga_eceran,'0',',','.').'</span>';
				$row[]  = '<span class="pull-right">'.number_format($pd->jumlah_komponen,'0',',','.').'</span>';
				$row[]  = $pd->keterangan_batal;

				$row[]  = $pd->pegawai_pembatalan;
				$row[]  = $pd->tanggal_pembatalan;
				$row[]  = $pd->pegawai_save;
				$row[]  = $pd->tanggal_pembuatan;
				$row[]  = $pd->pegawai_edit;
				$row[]  = $pd->tanggal_pembaharuan;
				$data[] = $row;
			}

			$output = array(	
				"draw"            => $_POST['draw'],
				"recordsTotal"    => $this->perakitan_detail->count_all('vamr4846_vama.perakitan_detail_batal', $id_master, ''),
				"recordsFiltered" => $this->perakitan_detail->count_filtered('_get_datatables_query_batal', $id_master, ''),
				"data"            => $data
			);		
			echo json_encode($output);
		}else{
			redirect(base_url().'perakitan');
		}
	}

	public function ajax_list_barang()
	{
		if($this->input->is_ajax_request()){
			// Cek user acces menu
			$id_pegawai     = $this->session->userdata('id_pegawai');
			$cek_useraccess = $this->useraccess_pusat->cek_access($id_pegawai, '25');
			if($cek_useraccess->act_read == '0' or $cek_useraccess->act_read == '-'){
				redirect(base_url().'dashboard');
			}

			$id_perakitan_m = $this->clean_tag_input($this->input->post('id_perakitan_m'));
			if($id_perakitan_m == ''){
				redirect(base_url().'perakitan');
			}

			$data_perakitan_master = $this->perakitan_master->get_by_id($id_perakitan_m);
			if($data_perakitan_master){
				$id_barang_pusat = $data_perakitan_master->id_barang_pusat;
			}else{
				$id_barang_pusat = '0';
			}

			$list = $this->perakitan_detail->get_datatables('_get_datatables_query_barang', $id_perakitan_m, $id_barang_pusat);
			$data = array();
			$no   = $_POST['start'];

			foreach ($list as $bp){
				$no++;
				$row   = array();
				$row[] = $no;				
				$row[] = $bp->sku;
				$row[] = $bp->nama_barang;
				$row[] = '<span class="pull-right">'.number_format($bp->total_stok,'0',',','.').'</span>';
				$row[] = '<span class="pull-right">'.number_format($bp->harga_eceran,'0',',','.').'</span>';
				$row[] = '<a class="btn btn-primary btn-xs" onclick="proses_barang('."'".$bp->sku."'".')">
						  <i class ="fa fa-check"></i> PILIH
						  </a>';
				$data[] = $row;
			}

			$output = array(	
				"draw"            => $_POST['draw'],
				"recordsTotal"    => $this->perakitan_detail->count_all('vamr4846_vama.barang_pusat', $id_perakitan_m, $id_barang_pusat),
				"recordsFiltered" => $this->perakitan_detail->count_filtered(
					'_get_datatables_query_barang', $id_perakitan_m, $id_barang_pusat
				),
				"data"            => $data
			);
			echo json_encode($output);
		}else{
			redirect(base_url().'perakitan');
		}
	}

	public function ajax_verifikasi_hapus_detail()
	{
		$id_perakitan_d =  $this->input->post('id_perakitan_d');
		$data           = $this->perakitan_detail->get_perakitan_detail($id_perakitan_d);
		if($data){
			echo json_encode(array(
				'status' 	=> 1,
				'pesan' 	=> "
									<b>DATA PERAKITAN</b><br/>
									<small>SKU : </small>
									<b class='text-dark'>$data->sku_perakitan</b><br/>
									
									<small>Nama Barang : </small>	
									<b class='text-dark'>$data->nama_barang_perakitan </b> </br>

									<small>Jumlah Perakitan : </small>
									<b class='text-dark'>$data->jumlah_perakitan </b>
									<hr>

									<b>DATA KOMPONEN</b><br/>
									<small>SKU : </small>
									<b class='text-dark'>$data->sku_komponen</b><br/>
									
									<small>Nama Barang : </small>	
									<b class='text-dark'>$data->nama_barang_komponen </b> </br>

									<small>Jumlah Komponen : </small>
									<b class='text-dark'>$data->jumlah_komponen</b>
									<hr>


									<textarea name='keterangan_batal' id='keterangan_batal' class='form-control' placeholder='Keterangan batal' style='resize: vertical;'></textarea>
							 	",

				'footer'	=> 	"	<button onclick='hapus_detail($data->id_perakitan_d)' type='button' 
										class='btn btn-primary waves-effect waves-light' data-dismiss='modal' autofocus>
										Iya, Hapus
									</button> 
									<button type='button' class='btn btn-default waves-effect' data-dismiss='modal'>Batal</button>

							   	"
			));
		}else{
			echo json_encode(array(
				'status' 	=> 0,
				'pesan' 	=>	"Barang tidak ditemukan</b>",
			));
		}
	}

	public function ajax_hapus_detail()
	{
		if($this->input->is_ajax_request()){
			$id_perakitan_d        =  $this->input->post('id_perakitan_d');
			$id_pegawai_pembatalan = $this->session->userdata('id_pegawai');
			$keterangan_batal      = $this->input->post('keterangan_batal');
			$catatan               = $this->clean_tag_input($this->input->post('catatan'));
			
			$dt = $this->perakitan_detail->get_perakitan_detail($id_perakitan_d);
			if($dt){
				$id_master    = $dt->id_perakitan_m;
				$no_perakitan = $dt->no_perakitan;
				$hapus        = $this->perakitan_detail->hapus_perakitan_detail(
									$dt->id_perakitan_d, $dt->id_perakitan_m, 
									$dt->id_barang_perakitan, $dt->no_perakitan,
									$dt->id_barang_pusat, $dt->jumlah_komponen,

									$dt->id_pegawai_pembuatan, $dt->id_pegawai_pembaharuan, $id_pegawai_pembatalan,
									$dt->tanggal_pembuatan, $dt->tanggal_pembaharuan, $keterangan_batal
								);

				if($hapus){
					// Hitung apakah masih ada perakaitan detail berdasarkan id perakitan master
					$dtm = $this->perakitan_detail->get_total($id_master);
					if($dtm){
						// Jika sudah tidak ada komponen yang disimpan 
						// berdasarkan id perakitan master maka kembalikan stok perakitan
						if($dtm->jumlah_barang == 0){
							$this->barang_pusat->update_stok($dtm->id_barang_pusat, $dtm->jumlah_perakitan);
							$this->perakitan_master->update_stok_master($id_master, $dtm->jumlah_perakitan);
						}
					}

					// Tampilkan informasi hapus
					echo json_encode(array(
						'status'              => '1',
						'jumlah_barang'       => $dtm->jumlah_barang,
						'jumlah_barang_batal' => $dtm->jumlah_barang_batal,
						'id_pr'               => $id_master,
						'pesan'               => "Komponen perakitan berhasil dihapus"
					));
				}else{
					echo json_encode(array(
						'status' => 0, 
						'pesan'  => 'Gagal menghapus komponen perakitan,
									 Harap coba kembali.'
					));
				}
			}else{

			}		
		}
	}

	public function ajax_cari_barang_perakitan()
	{
		if($this->input->is_ajax_request()){
			$id_pegawai     = $this->session->userdata('id_pegawai');
			$cek_useraccess = $this->useraccess_pusat->cek_access($id_pegawai, '29');
			if($cek_useraccess->act_create == '1'){
				$sku         = $this->input->post('sku');
				$data_barang = $this->barang_pusat->ambil_barang_untuk_transaksi($sku);				

				if($data_barang->row()){
					$json['status'] = 1;
					$json['data']   = $data_barang->row();
				}else{
					$json['status'] = 0;
					$json['pesan']  = "	SKU : ".$sku."  
										Tidak ditemukan di master barang!
									";
				}
			}else{
				$json['status'] = 2;
				$json['pesan']  = "	Maaf anda tidak di izinkan 
									untuk menambahkan barang perakitan.";
			}
			echo json_encode($json);
		}else{
			redirect(base_url().'perakitan');
			exit();
		}
	}

	public function ajax_cari_barang_komponen()
	{
		if($this->input->is_ajax_request()){
			$id_pegawai     = $this->session->userdata('id_pegawai');
			$cek_useraccess = $this->useraccess_pusat->cek_access($id_pegawai, '29');
			if($cek_useraccess->act_create == '1'){
				$sku         = $this->input->post('sku');
				$data_barang = $this->barang_pusat->ambil_barang_untuk_transaksi($sku);				

				if($data_barang->row()){
					$json['status'] = 1;
					$json['data']   = $data_barang->row();
				}else{
					$json['status'] = 0;
					$json['pesan']  = "	SKU : ".$sku."  
										Tidak ditemukan di master barang!
									";
				}
			}else{
				$json['status'] = 2;
				$json['pesan']  = "	Maaf anda tidak di izinkan 
									untuk menambahkan komponen perakitan.";
			}
			echo json_encode($json);
		}else{
			redirect(base_url().'perakitan');
			exit();
		}
	}

	public function ajax_kode_perakitan()
	{
		if($this->input->is_ajax_request()){
			// Cek user acces menu
			$id_pegawai     = $this->session->userdata('id_pegawai');
			$cek_useraccess = $this->useraccess_pusat->cek_access($id_pegawai, '29');
			if($cek_useraccess->act_create == '0' or $cek_useraccess->act_create == '-'){
				$json['status']      = 2;
				$json['judul_pesan'] = 'Oops!';
				$json['isi_pesan']   = 'Maaf anda tidak diberikan izin 
										untuk menambahkan transaksi perakitan barang.';
			}else{
				$keyword = $this->input->post('keyword');
				$id_pr   = $this->input->post('id_pr');
				$barang  = $this->barang_pusat->cari_kode_by_perakitan($keyword, $id_pr);

				if($barang->num_rows() > 0){
					$json['status']  = 1;
					$json['datanya'] = "<ul id='daftar_autocompleted_perakitan' class='list-group user-list'>";
					
					foreach($barang->result() as $b){
						if($b->foto!=""){
							$json['datanya'] .= "
				                <li class='user-list-item'>
				                	<div class='avatar avatar-sm-box'>
	                                    <img src='assets/upload/image/barang/thumbs/$b->foto'>
				                	</div>";
				        }else{
							$nama_barang 	  = $b->nama_barang;
							$inisial 		  = substr($nama_barang, 0, 1);
							$json['datanya'] .= "
				                <li class='user-list-item'>
				                	<div class='avatar avatar-sm-box'>
	                                    <span class='avatar-sm-box bg-info'>".$inisial."</span>
				                	</div>";
						}

						$json['datanya'] .= "
			                	<div class='user-desc'>
									<span id='barangnya_perakitan' class='name'>".$b->nama_barang."</span>
									<span id='skunya_perakitan' class='name text-muted'>".$b->sku."</span>
									<span id='harga_perakitan_tampil' class='name text-muted'>Eceran : ".number_format($b->harga_eceran, '0', ',', '.')."</span>
									<span id='harga_perakitan' class='name text-muted' style='display: none;'>".number_format($b->harga_eceran, '0', ',', '.')."</span>
									<span id='total_stok_perakitan' class='name text-muted'>Stok : ".$b->total_stok."</span>
									
									<span id='id_barang_perakitan' style='display :none;'>".$b->id_barang_pusat."</span>
								</div>
							</li>
						";
					}
					$json['datanya'] .= "</ul>";
				}else{
					$json['status'] = 0;
				}
			}
			echo json_encode($json);
		}else{
			redirect(base_url().'perakitan');
		}
	}

	public function ajax_kode_komponen()
	{
		if($this->input->is_ajax_request()){
			// Cek user acces menu
			$id_pegawai     = $this->session->userdata('id_pegawai');
			$cek_useraccess = $this->useraccess_pusat->cek_access($id_pegawai, '29');
			if($cek_useraccess->act_create == '0' or $cek_useraccess->act_create == '-'){
				$json['status']      = 2;
				$json['judul_pesan'] = 'Oops!';
				$json['isi_pesan']   = 'Maaf anda tidak diberikan izin 
										untuk menambahkan komponen perakitan.';
			}else{
				$keyword             = $this->input->post('keyword');
				$id_pr               = $this->input->post('id_pr');
				$id_barang_perakitan = $this->input->post('id_barang_perakitan');
				$barang              = $this->barang_pusat->cari_kode_by_komponen($keyword, $id_pr, $id_barang_perakitan);

				if($barang->num_rows() > 0){
					$json['status']  = 1;
					$json['datanya'] = "<ul id='daftar_autocompleted_komponen' class='list-group user-list'>";
					
					foreach($barang->result() as $b){
						if($b->foto != ""){
							$json['datanya'] .= "
				                <li class='user-list-item'>
				                	<div class='avatar avatar-sm-box'>
	                                    <img src='assets/upload/image/barang/thumbs/$b->foto'>
				                	</div>";
				        }else{
							$nama_barang 	  = $b->nama_barang;
							$inisial 		  = substr($nama_barang, 0, 1);
							$json['datanya'] .= "
				                <li class='user-list-item'>
				                	<div class='avatar avatar-sm-box'>
	                                    <span class='avatar-sm-box bg-info'>".$inisial."</span>
				                	</div>";
						}

						$json['datanya'] .= "
			                	<div class='user-desc'>
									<span id='barangnya_komponen' class='name'>".$b->nama_barang."</span>
									<span id='skunya_komponen' class='name text-muted'>".$b->sku."</span>
									<span id='harga_komponen_tampil' class='name text-muted'>Eceran : ".number_format($b->harga_eceran, '0', ',', '.')."</span>
									<span id='harga_komponen' class='name text-muted' style='display: none;'>".number_format($b->harga_eceran, '0', ',', '.')."</span>
									<span id='total_stok_komponen' class='name text-muted'>Stok : ".$b->total_stok."</span>
									<span id='id_barang_komponen' style='display :none;'>".$b->id_barang_pusat."</span>
								</div>
							</li>
						";
					}
					$json['datanya']      .= "</ul>";
				}else{
					$json['status'] = 0;
				}
			}
			echo json_encode($json);
		}else{
			redirect(base_url().'perakitan');
		}
	}
}
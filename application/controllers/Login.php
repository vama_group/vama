<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {
	// Load database
	public function __construct()
	{
		parent::__construct();
		$this->load->model('pegawai_pusat_model','pegawai_pusat');
	}

	// Halaman login
	public function index()
	{
		// Redirect ke dashboard kalau sudah login
		if ($this->session->userdata('username') !=""){
			redirect(base_url('dashboard'),'refresh');
		}

		// Validasi
		$username = $this->input->post('email');
		$password = $this->input->post('password');
		$valid    = $this->form_validation;

		$valid->set_rules('email','Username/email','required',
				array(	'required'	=> 'Username/email harus diisi'));

		$valid->set_rules('password','Password','required|min_length[6]',
			array(	
				'required'   => 'Password harus diisi',
				'min_length' => 'Password minimal 6 karakter'
			));

		if($valid->run()) {
			// Proses login jalan
			$this->simple_login->login($username, $password,
								 base_url('dashboard'),
								 base_url('login'));
		}
		// End validasi

		$data = array ('title' => 'Vama - masuk');
		$this->load->view('login_view', $data, FALSE);
	}

	// Logout
	public function logout(){
		$this->simple_login->logout();
	}

	// Lock screen
	public function lock(){
		$this->session->unset_userdata('password');
		$id_pegawai = $this->session->userdata('id_pegawai');

		if(isset($_GET['redirect'])) {
			$link = str_replace('index.php/', '',$_GET['redirect']);
		}else{
			$_GET['redirect'] = base_url('dashboard');
			$link             = $_GET['redirect'];
		}
		$link = str_replace('index.php/','',$_GET['redirect']);

		// Validasi password
		$password = $this->input->post('password');

		$this->form_validation->set_rules('password','Password','required',
				array(	'required'	=> 'Password harus diisi'));

		if($this->form_validation->run()){
			$pegawai = $this->pegawai_pusat->password($id_pegawai, $password);
			if($pegawai !== '') {
				$this->session->set_userdata('password',$password);
				redirect($link);
			}
		}
		// End validasi

		$data = array ('title' => 'Masukkan Password Lock Screen');
		$this->load->view('lock_screen', $data, FALSE);		
	}
}

/* End of file Login.php */
/* Location: ./application/controllers/Login.php */
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard_grafik extends MY_Controller {
	
	public function __construct() {
        parent::__construct();
        $this->CI =& get_instance();
        $this->load->model('pegawai_pusat_model','pegawai_pusat');
        $this->load->model('useraccess_pusat_model','useraccess_pusat');
        $this->load->model('customer_pusat_model', 'customer_pusat');
        $this->load->model('dashboard_grafik_model', 'dashboard_grafik');
        $this->load->model('penjualan_master_model', 'penjualan_master');
        $this->load->helper('tgl_indo');
    }

	public function index()
	{
		// Cek user acces menu
		$id_pegawai = $this->session->userdata('id_pegawai');
		if($id_pegawai == ''){
			redirect(base_url().'login');
			exit();
		}

		$data_pegawai  = $this->pegawai_pusat->get_by_id($id_pegawai);
		$tanggal_awal  = date('Y-m-').'01';
		$tanggal_akhir = date('Y-m-d');
		$bulan         = bulan(date('m')).' - '.date('Y');

		if($data_pegawai){
			$daftar_toko               = $this->customer_pusat->listing_toko();
			$data['data_toko']         = $daftar_toko;
			
			$data_pegawai              = $this->pegawai_pusat->get_by_id($id_pegawai);		
			$data['data_pegawai']      = $data_pegawai;
			
			$data['data_induk_menu']   = $this->useraccess_pusat->get_induk_menu($id_pegawai);		
			$data['atribut_halaman']   = 'Dashboard Pusat';
			$data['tanggal_awal']      = $tanggal_awal;
			$data['tanggal_akhir']     = $tanggal_akhir;
			$data['bulan']             = $bulan;
			$data['halaman_dashboard'] = $this->load->view('admin/dashboard/dashboard_pusat/dashboard_view_grafik', $data, true);
			$data['halaman_plugin']    = $this->load->view('admin/dashboard/dashboard_pusat/plugin_grafik', $data, true);
			$data['isi_halaman']       = $this->load->view('admin/dashboard/dashboard_pusat/index', $data, true);
			$this->load->view('admin/layout',$data);
		}else{
			redirect(base_url().'login');
			exit();
		}
	}

	public function chart_penjualan_toko_perbulan(){
		$tanggal_awal  = $this->input->post('tanggal_awal');
		$tanggal_akhir = $this->input->post('tanggal_akhir');
		if($tanggal_awal == '' AND $tanggal_akhir == ''){
			redirect(base_url().'dashboard');
			exit();
		}
		
		if($this->session->userdata('usergroup_name') == 'Super Admin' OR $this->session->userdata('usergroup_name') == 'Admin'){
			$daftar_toko  = $this->customer_pusat->listing_toko();
	        if(count($daftar_toko) > 0){
				$data = array();
	        	foreach ($daftar_toko as $daftar_toko){
					$data_penjualan_toko = $this->dashboard_grafik->get_chart_penjualan_toko_perbulan($tanggal_awal, $tanggal_akhir, $daftar_toko->kode_customer_pusat);
					if($data_penjualan_toko){
						$data[$daftar_toko->kode_customer_pusat] = $data_penjualan_toko;
					}
				}


				$daftar_toko = $this->customer_pusat->listing_toko();
	        	foreach ($daftar_toko as $daftar_toko){
					$row              = array(); 
					$row['kode_toko'] = $daftar_toko->kode_customer_pusat;
					$row['nama_toko'] = $daftar_toko->nama_customer_pusat;
					$data_toko[]      = $row;
				}
				$data['daftar_toko'] = $data_toko;

				$data_penjualan_online = $this->dashboard_grafik->get_chart_penjualan_online_perbulan($tanggal_awal, $tanggal_akhir, 'mrc004');
				if($data_penjualan_online){
					$data['online'] = $data_penjualan_online;
				}else{
					$data['online'] = '';
				}
			}
			echo json_encode($data, JSON_NUMERIC_CHECK);
		}
	}

	public function chart_penjualan_pusat_perbulan(){
		$tanggal_awal  = $this->input->post('tanggal_awal');
		$tanggal_akhir = $this->input->post('tanggal_akhir');
		if($tanggal_awal == '' AND $tanggal_akhir == ''){
			redirect(base_url().'dashboard');
			exit();
		}
		
		if($this->session->userdata('usergroup_name') == 'Super Admin' OR $this->session->userdata('usergroup_name') == 'Admin'){
			$grosir                   = $this->dashboard_grafik->get_chart_penjualan_pusat_perbulan('!=', 'SELESAI', $tanggal_awal, $tanggal_akhir);
			$mrc                      = $this->dashboard_grafik->get_chart_penjualan_pusat_perbulan('=', 'SELESAI', $tanggal_awal, $tanggal_akhir);
			
			$data                     = array();
			$data['penjualan_mrc']    = $mrc;
			$data['penjualan_grosir'] = $grosir;
			echo json_encode($data, JSON_NUMERIC_CHECK);
		}
	}

	public function chart_penjualan_toko_perhari(){		
		if($this->session->userdata('usergroup_name') == 'Super Admin' OR $this->session->userdata('usergroup_name') == 'Admin'){
			$daftar_toko  = $this->customer_pusat->listing_toko();
	        if(count($daftar_toko) > 0){
				$data = array();
	        	foreach ($daftar_toko as $daftar_toko){
					$data_penjualan_toko = $this->dashboard_grafik->get_chart_penjualan_toko_perhari($daftar_toko->kode_customer_pusat);
					if($data_penjualan_toko){
						$data[$daftar_toko->kode_customer_pusat] = $data_penjualan_toko;
					}
				}

				$daftar_toko = $this->customer_pusat->listing_toko();
	        	foreach ($daftar_toko as $daftar_toko){
					$row              = array(); 
					$row['kode_toko'] = $daftar_toko->kode_customer_pusat;
					$row['nama_toko'] = $daftar_toko->nama_customer_pusat;
					$data_toko[]      = $row;
				}
				$data['daftar_toko'] = $data_toko;

				$data_penjualan_toko = $this->dashboard_grafik->get_chart_penjualan_online_perhari('mrc004');
				if($data_penjualan_toko){
					$data['online'] = $data_penjualan_toko;
				}else{
					$data['online'] = '';
				}
			}
			echo json_encode($data, JSON_NUMERIC_CHECK);
		}
	}

	public function chart_penjualan_pusat_perhari(){
		$tanggal_awal    = $this->input->post('tanggal_awal');
		$tanggal_akhir   = $this->input->post('tanggal_akhir');
		if($tanggal_awal == '' AND $tanggal_akhir == ''){
			redirect(base_url().'dashboard');
			exit();
		}
		
		if($this->session->userdata('usergroup_name') == 'Super Admin' OR $this->session->userdata('usergroup_name') == 'Admin'){
			$penjualan_menunggu_umum  = $this->penjualan_master->get_chart_penjualan_perbulan('!=', 'MENUNGGU', $tanggal_awal, $tanggal_akhir);
			$penjualan_disiapkan_umum = $this->penjualan_master->get_chart_penjualan_perbulan('!=', 'DISIAPKAN', $tanggal_awal, $tanggal_akhir);
			$penjualan_selesai_umum   = $this->penjualan_master->get_chart_penjualan_perbulan('!=', 'SELESAI', $tanggal_awal, $tanggal_akhir);
			
			$penjualan_menunggu_mrc   = $this->penjualan_master->get_chart_penjualan_perbulan('=', 'MENUNGGU', $tanggal_awal, $tanggal_akhir);
			$penjualan_disiapkan_mrc  = $this->penjualan_master->get_chart_penjualan_perbulan('=', 'DISIAPKAN', $tanggal_awal, $tanggal_akhir);
			$penjualan_dikirim_mrc    = $this->penjualan_master->get_chart_penjualan_perbulan('=', 'DIKIRIM', $tanggal_awal, $tanggal_akhir);
			$penjualan_selesai_mrc    = $this->penjualan_master->get_chart_penjualan_perbulan('=', 'SELESAI', $tanggal_awal, $tanggal_akhir);
			
			$data                     = array();
			$data['menunggu_umum']    = $penjualan_menunggu_umum;
			$data['disiapkan_umum']   = $penjualan_disiapkan_umum;
			$data['selesai_umum']     = $penjualan_selesai_umum;
			
			$data['menunggu_mrc']     = $penjualan_menunggu_mrc;
			$data['disiapkan_mrc']    = $penjualan_disiapkan_mrc;
			$data['dikirim_mrc']      = $penjualan_dikirim_mrc;
			$data['selesai_mrc']      = $penjualan_selesai_mrc;
			echo json_encode($data, JSON_NUMERIC_CHECK);
		}
	}
}
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kartu_edc_pusat extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('pegawai_pusat_model','pegawai_pusat');
        $this->load->model('useraccess_pusat_model','useraccess_pusat');
        
		$this->load->model('kartu_edc_pusat_model','kartu_edc_pusat');
	}

	public function index()
	{
		// Cek user acces menu
		$id_pegawai = $this->session->userdata('id_pegawai');
		if($id_pegawai == ''){
			redirect(base_url().'dashboard');
		}
		
		$cek_useraccess = $this->useraccess_pusat->cek_access($id_pegawai, '14');
		if($cek_useraccess){
			if($cek_useraccess->act_read == '0' or $cek_useraccess->act_read == '-'){
				redirect(base_url().'dashboard');
			}					
		}else{
			redirect(base_url().'dashboard');
		}

		// cari data pegawai
		$data_pegawai					= $this->pegawai_pusat->get_by_id($id_pegawai);
		$data['access_create'] 			= $cek_useraccess->act_create;
		$data['access_update'] 			= $cek_useraccess->act_update;
		$data['access_delete'] 			= $cek_useraccess->act_delete;
		$data['data_pegawai']    		= $data_pegawai;
		$data['data_induk_menu'] 		= $this->useraccess_pusat->get_induk_menu($id_pegawai);

		$data['atribut_halaman'] 		= 'Kartu EDC Pusat';
		$data['halaman_list'] 			= $this->load->view('admin/master_data/kartu_edc_pusat/list',$data,true);
		$data['halaman_form'] 			= $this->load->view('admin/master_data/kartu_edc_pusat/form',$data,true);
		$data['halaman_plugin'] 		= $this->load->view('admin/master_data/kartu_edc_pusat/plugin',$data,true);
		$data['isi_halaman'] 			= $this->load->view('admin/master_data/kartu_edc_pusat/index',$data,true);
		$this->load->view('admin/layout',$data);
	}

	public function ajax_list()
	{
		// Cek user acces menu
		$id_pegawai 	= $this->session->userdata('id_pegawai');
		$cek_useraccess = $this->useraccess_pusat->cek_access($id_pegawai, '14');
		if($cek_useraccess->act_read == '0' or $cek_useraccess->act_read == '0'){
			redirect(base_url().'dashboard');
			exit();
		}

		$id_periode_stok_pusat = $this->input->post('id_periode_stok_pusat');
		if($id_periode_stok_pusat == ''){
			redirect(base_url().'kartu_edc_pusat');
			exit();
		}

		$list = $this->kartu_edc_pusat->get_datatables();
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $kartu_edc_pusat) {
			$no++;
			$row 	= array();
			$row[] 	= $no;

			//add html for action
			if($cek_useraccess->act_update == 1){
				$tombol_edit 	 = '<a class="btn btn-rounded btn-xs btn-default" href="javascript:void(0)" title="Edit" 
										onclick="edit_kartu_edc_pusat('."'".$kartu_edc_pusat->id_kartu_edc_pusat."'".')">
										<i class="fa fa-pencil" style="color: blue;"></i>
									</a>';
			}else{$tombol_edit 	 = '';}

			if($cek_useraccess->act_delete == 1){
				$tombol_hapus 	 = '<a class="btn btn-rounded btn-xs btn-default" href="javascript:void(0)" title="Hapus" 
										onclick="verifikasi_delete('."'".$kartu_edc_pusat->id_kartu_edc_pusat."'".')">
										<i class="fa fa-remove" style="color: red;"></i>
				  					</a>';
			}else{$tombol_hapus  = '';}

			if($cek_useraccess->act_update == 1 or $cek_useraccess->act_delete == 1){
				$row[] 	= '
							'.$tombol_edit.'
							'.$tombol_hapus.'
					  	';
			}

			$row[] 	= $kartu_edc_pusat->no_kartu_edc;
			$row[] 	= $kartu_edc_pusat->nama_bank;
			$row[] 	= $kartu_edc_pusat->atas_nama;			

			$row[]	= $kartu_edc_pusat->pegawai_save;
			$row[] 	= $kartu_edc_pusat->tanggal_pembuatan;
			$row[] 	= $kartu_edc_pusat->pegawai_edit;
			$row[] 	= $kartu_edc_pusat->tanggal_pembaharuan;
			$data[] = $row;
		}

		$output = array(	"draw" 				=> $_POST['draw'],
							"recordsTotal" 		=> $this->kartu_edc_pusat->count_all(),
							"recordsFiltered" 	=> $this->kartu_edc_pusat->count_filtered(),
							"data" 				=> $data,
						);
		echo json_encode($output);
	}

	public function ajax_add()
	{
		// Cek user acces menu
		$id_pegawai 	= $this->session->userdata('id_pegawai');
		$cek_useraccess = $this->useraccess_pusat->cek_access($id_pegawai, '14');
		if($cek_useraccess->act_create == '0' or $cek_useraccess->act_create == '-'){
			redirect(base_url().'kartu_edc_pusat');
			exit();
		}

		$this->_validate('0');

		$data = array(
						'no_kartu_edc'				=> $this->input->post('no_kartu_edc'),
						'nama_bank'					=> $this->input->post('nama_bank'),
						'atas_nama'					=> $this->input->post('atas_nama'),
						'id_pegawai_pembuatan'		=> $this->session->userdata('id_pegawai'),
						'tanggal_pembuatan'			=> date('Y-m-d H:i:s')
					);
		$insert = $this->kartu_edc_pusat->save($data);
		echo json_encode(array("status" => TRUE));
	}

	public function ajax_edit()
	{
		// Cek user acces menu
		$id_pegawai 	= $this->session->userdata('id_pegawai');
		$cek_useraccess = $this->useraccess_pusat->cek_access($id_pegawai, '14');
		if($cek_useraccess->act_update == '0' or $cek_useraccess->act_update == '-'){
			redirect(base_url().'kartu_edc_pusat');
			exit();
		}

		$id_kartu_edc_pusat = $this->input->post('id_kartu_edc_pusat');
		if($id_kartu_edc_pusat == ''){
			redirect(base_url().'kartu_edc_pusat');
			exit();
		}else{
			$data = $this->kartu_edc_pusat->get_by_id($id_kartu_edc_pusat);
			echo json_encode($data);
		}
	}

	public function ajax_update()
	{
		// Cek user acces menu
		$id_pegawai 	= $this->session->userdata('id_pegawai');
		$cek_useraccess = $this->useraccess_pusat->cek_access($id_pegawai, '14');
		if($cek_useraccess->act_update == '0' or $cek_useraccess->act_update == '-'){
			redirect(base_url().'kartu_edc_pusat');
			exit();
		}

		$id_kartu_edc_pusat = $this->input->post('id_kartu_edc_pusat');
		if($id_kartu_edc_pusat == ''){
			redirect(base_url().'kartu_edc_pusat');
			exit();
		}

		$this->_validate($id_kartu_edc_pusat);
		$data = array(
						'no_kartu_edc'				=> $this->input->post('no_kartu_edc'),
						'nama_bank'					=> $this->input->post('nama_bank'),
						'atas_nama'					=> $this->input->post('atas_nama'),
						'id_pegawai_pembaharuan'	=> $this->session->userdata('id_pegawai')
					);
		$this->kartu_edc_pusat->update(array('id_kartu_edc_pusat' => $id_kartu_edc_pusat), $data);
		echo json_encode(array("status" => TRUE));
	}

	public function ajax_verifikasi_delete()
	{
		// Cek user acces menu
		$id_pegawai 	= $this->session->userdata('id_pegawai');
		$cek_useraccess = $this->useraccess_pusat->cek_access($id_pegawai, '14');
		if($cek_useraccess->act_delete == '0' or $cek_useraccess->act_delete == '-'){
			redirect(base_url().'kartu_edc_pusat');
			exit();
		}

		$id_kartu_edc_pusat = $this->input->post('id_kartu_edc_pusat');
		if($id_kartu_edc_pusat == ''){
			redirect(base_url().'kartu_edc_pusat');
			exit();
		}
		
		$data = $this->kartu_edc_pusat->get_by_id($id_kartu_edc_pusat);		
		echo json_encode(array(
								'pesan' 	=> "<b class='text-danger'>Yakin ingin menghapus kartu edc pusat ini ? </b></br></br>
												Nama Bank : </br>
												<h2 class='text-dark'>".$data->nama_bank."</h2></br>

												No. Kartu EDC :</br>
												<b class='text-dark'>".$data->no_kartu_edc."</b></br></br>

												Atas Nama :</br>
												<b class='text-dark'>".$data->atas_nama."</b>",

								'footer'	=> 	"<button onclick='delete_kartu_edc_pusat($id_kartu_edc_pusat)' 
													type='button' class='btn btn-primary waves-effect waves-light' 
													data-dismiss='modal' autofocus>Iya, Hapus</button> 
												<button type='button' class='btn btn-default waves-effect' 
													data-dismiss='modal'>Batal</button>"
							  	)
						);
	}

	public function ajax_delete()
	{
		$id_pegawai 	= $this->session->userdata('id_pegawai');
		$cek_useraccess = $this->useraccess_pusat->cek_access($id_pegawai, '14');
		if($cek_useraccess->act_delete == '0' or $cek_useraccess->act_delete == '-'){
			redirect(base_url().'kartu_edc_pusat');
			exit();
		}

		$id_kartu_edc_pusat = $this->input->post('id_kartu_edc_pusat');
		if($id_kartu_edc_pusat == ''){
			redirect(base_url().'kartu_edc_pusat');
			exit();
		}

		$this->kartu_edc_pusat->update_status_hapus($id_kartu_edc_pusat, $id_pegawai);
		echo json_encode(array("status" => TRUE));
	}

	private function _validate($id_kartu_edc_pusat)
	{
		$data 					= array();
		$data['error_string'] 	= array();
		$data['inputerror'] 	= array();
		$data['status'] 		= TRUE;
		$no_kartu_edc 			= $this->input->post('no_kartu_edc');
		$nama_bank 				= $this->input->post('nama_bank');

		if($no_kartu_edc == ''){
			$data['inputerror'][] 	= 'no_kartu_edc';
			$data['error_string'][] = 'No. kartu EDC wajib diisi';
			$data['status'] 		= FALSE;
		}

		if($nama_bank == ''){
			$data['inputerror'][] 	= 'nama_bank';
			$data['error_string'][] = 'Nama bank wajib diisi';
			$data['status'] 		= FALSE;
		}

		if($this->input->post('atas_nama') == ''){
			$data['inputerror'][] 	= 'atas_nama';
			$data['error_string'][] = 'Atas nama wajib diisi';
			$data['status'] 		= FALSE;
		}

		$validasi_no = $this->kartu_edc_pusat->get_by_no($no_kartu_edc);
		if($validasi_no){
			if($id_kartu_edc_pusat <> $validasi_no->id_kartu_edc_pusat){
				$data['inputerror'][] 	= 'no_kartu_edc';
				$data['error_string'][] = 'No. kartu edc sudah digunakan, harap ganti no. kartu edc';
				$data['status'] 		= FALSE;
			}
		}

		$validasi_nama = $this->kartu_edc_pusat->get_by_nama_bank($nama_bank);
		if($validasi_nama){
			if($id_kartu_edc_pusat <> $validasi_nama->id_kartu_edc_pusat){
				$data['inputerror'][] 	= 'nama_bank';
				$data['error_string'][] = 'Nama bank sudah digunakan, harap ganti nama bank';
				$data['status'] 		= FALSE;
			}
		}

		if($data['status'] === FALSE){
			echo json_encode($data);
			exit();
		}
	}
}

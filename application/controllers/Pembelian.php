<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require('./assets/zebra/vendor/autoload.php');
use Zebra\Client;
use Zebra\Zpl\Image;
use Zebra\Zpl\Builder;
use Zebra\Zpl\GdDecoder;

class Pembelian extends MY_Controller 
{
	var $CI = NULL;
	function __construct()
	{
		parent::__construct();
		$this->CI =& get_instance();
		$this->load->model('pegawai_pusat_model','pegawai_pusat');
		$this->load->model('useraccess_pusat_model','useraccess_pusat');
		$this->load->model('supplier_model','supplier');
		$this->load->model('barang_pusat_model','barang_pusat');
		$this->load->model('pembelian_master_model','pembelian_master');
		$this->load->model('pembelian_detail_model','pembelian_detail');
	}

	public function index()
	{
		// Cek user acces menu
		$id_pegawai = $this->session->userdata('id_pegawai');
		if($id_pegawai == ''){
			redirect(base_url().'login');
		}
		
		$cek_useraccess = $this->useraccess_pusat->cek_access($id_pegawai, '25');
		$cek_useraccess_laporan = $this->useraccess_pusat->cek_access($id_pegawai, '36');
		if($cek_useraccess){
			if($cek_useraccess->act_read == '0' or $cek_useraccess->act_read == '-'){
				redirect(base_url().'dashboard');
			}					
		}else{
			redirect(base_url().'dashboard');
		}

		$data_pegawai = $this->pegawai_pusat->get_by_id($id_pegawai);
		if($data_pegawai){
			$data['data_pegawai']        = $data_pegawai;
			$data['data_induk_menu']     = $this->useraccess_pusat->get_induk_menu($id_pegawai);
			$data['access_create']       = $cek_useraccess->act_create;
			$data['access_update']       = $cek_useraccess->act_update;
			$data['access_laporan']      = $cek_useraccess_laporan->act_read;
			
			$data['atribut_halaman']     = 'Pembelian Pusat';
			$data['id_pembelian_m']      = '0';
			$data['halaman_transaksi']   = $this->load->view('admin/transaksi/pembelian/transaksi',$data,true);
			$data['halaman_plugin']      = $this->load->view('admin/transaksi/pembelian/plugin',$data,true);
			$data['isi_halaman']         = $this->load->view('admin/transaksi/pembelian/index',$data,true);
			$this->load->view('admin/layout',$data);
		}else{
			redirect(base_url().'login');
		}
	}

	public function transaksi()
	{	
		$id_pegawai = $this->session->userdata('id_pegawai');
		if($id_pegawai == ''){
			redirect(base_url().'login');
			exit();
		}

		$data_pegawai = $this->pegawai_pusat->get_by_id($id_pegawai);
		if($data_pegawai){
			$id_pembelian_m = $this->input->get('id');
			$data_pembelian = $this->pembelian_master->get_master2($id_pembelian_m);
			if($data_pembelian->id_pembelian_m == ''){
				redirect(base_url().'pembelian');
				exit();
			}

			// Cek user acces menu
			$cek_useraccess_laporan = $this->useraccess_pusat->cek_access($id_pegawai, '36');
			$data['access_laporan'] = $cek_useraccess_laporan->act_read;
			
			if($cek_useraccess_laporan->act_update == '1' or $data_pembelian->status_pembelian == 'MENUNGGU'){		
				$cek_useraccess            = $this->useraccess_pusat->cek_access($id_pegawai, '25');
				$data['access_create']     = $cek_useraccess->act_create;
				$data['access_update']     = $cek_useraccess->act_update;
				
				// cari data pegawai
				$id_pegawai                = $this->session->userdata('id_pegawai');
				$data_pegawai              = $this->pegawai_pusat->get_by_id($id_pegawai);
				$data['data_pegawai']      = $data_pegawai;
				$data['data_induk_menu']   = $this->useraccess_pusat->get_induk_menu($id_pegawai);
				
				$data['atribut_halaman']   = 'Pembelian';
				$data['id_pembelian_m']    = $id_pembelian_m;
				$data['no_pembelian']      = $data_pembelian->no_pembelian;
				$data['halaman_transaksi'] = $this->load->view('admin/transaksi/pembelian/transaksi',$data,true);
				$data['halaman_plugin']    = $this->load->view('admin/transaksi/pembelian/plugin',$data,true);
				$data['isi_halaman']       = $this->load->view('admin/transaksi/pembelian/index',$data,true);
				$this->load->view('admin/layout',$data);
			}else{
				redirect(base_url().'dashboard');
				exit();
			}
		}else{
			redirect(base_url().'login');
		}
	}

	public function simpan_transaksi()
	{		
		if( ! empty($_POST['id_pembelian_m'])){
			// Cek user acces menu
			$url            = $this->input->post('url');
			$id_pegawai     = $this->session->userdata('id_pegawai');
			$cek_useraccess = $this->useraccess_pusat->cek_access($id_pegawai, '25');
			$id_pembelian_m = $this->input->post('id_pembelian_m');

			if($cek_useraccess->act_create == '1' or $cek_useraccess->act_update == '1'){
				$no_pembelian = $this->input->post('no_pembelian');
				$no_faktur    = $this->input->post('no_faktur');
				$id_pegawai   = $this->session->userdata('id_pegawai');
				$id_supplier  = $this->input->post('id_supplier');
				$catatan      = $this->clean_tag_input($this->input->post('catatan'));
				$ttl          = $this->input->post('total');
				$ppn          = $this->input->post('ppn');
				$biaya_lain   = $this->input->post('biaya_lain');

				if($this->session->userdata('usergroup_name') !== "Super Admin"){
				$data_total = $this->pembelian_detail->get_total($id_pembelian_m);
					if($data_total){					
						$biaya_lain = $data_total->biaya_lain;
						// $ppn        = $data_total->ppn;
						$ppn        = '0';
						$ttl        = $data_total->total;
					}
				}

				if($biaya_lain == '' or $biaya_lain == '0'){
					$biaya_lain = '0';
				}

				if($ppn == '' or $ppn == '0'){
					$ppn = '0';
				}

				$grand_total  = $ttl+$ppn+$biaya_lain;
				
				// Update total dan grandtotal di pembelian master
				$master = $this->pembelian_master->update_master(
					$id_pembelian_m, $no_faktur, $id_supplier, 
					$ttl, $biaya_lain, $ppn, $grand_total, 
					$catatan, $id_pegawai, 'SELESAI'
				);

				if($master){
					echo json_encode(array(
						'status'	=> 1, 
						'pesan' 	=> "Transaksi berhasil disimpan,
					   					No. Pembelian : ".$no_pembelian."."
					));
				}else{
					echo json_encode(array(	
						'status' => 0,
						'pesan'  => 'Terjadi kesalah saat menyimpan, harap coba kembali.',
						'url'    => 'pembelian/transaksi/?&id='.$id_pembelian_m
					));
				}
			}else{
				echo json_encode(array(	
					'status' 	=> 2,
					'pesan'		=> 'Maaf anda tidak diizinkan 
									untuk menyimpan transaksi pembelian.',
					'url' 		=> 'pembelian/transaksi/?&id='.$id_pembelian_m
				));
			}
		}else{
			redirect(base_url().'pembelian');
		}
	}

	public function tahan_transaksi()
	{	
		if( ! empty($_POST['id_pembelian_m'])){
			// Cek user acces menu
			$url 					= $this->input->post('url');
			$id_pegawai 			= $this->session->userdata('id_pegawai');
			$cek_useraccess 		= $this->useraccess_pusat->cek_access($id_pegawai, '25');
			$id_pembelian_m 		= $this->input->post('id_pembelian_m');

			if($cek_useraccess->act_create == '1' or $cek_useraccess->act_update == '1'){
				$no_pembelian 		= $this->input->post('no_pembelian');
				$no_faktur 			= $this->input->post('no_faktur');
				$id_pegawai			= $this->session->userdata('id_pegawai');
				$id_supplier		= $this->input->post('id_supplier');
				$catatan			= $this->clean_tag_input($this->input->post('catatan'));
				$ttl 				= $this->input->post('total');
				$ppn				= $this->input->post('ppn');
				$biaya_lain			= $this->input->post('biaya_lain');				
				

				if($this->session->userdata('usergroup_name') !== "Super Admin"){
				$data_total = $this->pembelian_detail->get_total($id_pembelian_m);
					if($data_total){					
						$biaya_lain = $data_total->biaya_lain;
						// $ppn        = $data_total->ppn;
						$ppn        = '0';
						$ttl        = $data_total->total;
					}
				}

				if($biaya_lain == '' or $biaya_lain == '0'){
					$biaya_lain = '0';
				}

				if($ppn == '' or $ppn == '0'){
					$ppn = '0';
				}

				$grand_total = $ttl+$ppn+$biaya_lain;
				
				// Update total dan grandtotal di pembelian master
				$master = $this->pembelian_master->update_master(
					$id_pembelian_m, $no_faktur, $id_supplier, 
					$ttl, $biaya_lain, $ppn, $grand_total, 
					$catatan, $id_pegawai, 'MENUNGGU'
				);

				if($master){
					echo json_encode(array('status'	 	=> 1, 
										   'pesan' 		=> "Transaksi pembelian berhasil ditahan, 
										   					dengan no. pembelian : ".$no_pembelian.".
										   				   "));
				}else{
					echo json_encode(array(	'status' 	=> 0,
											'pesan'		=> 'Terjadi kesalah saat menahan transaksi, harap coba kembali.',
											'url' 		=> 'retur_pembelian/transaksi/?&id='.$id_pembelian_m
										  )
									);
				}
				
			}else{
				echo json_encode(array(	'status' 	=> 0,
										'pesan'		=> 'Maaf anda tidak diizinkan untuk menahan transaksi pembelian.',
										'url' 		=> 'pembelian/transaksi/?&id='.$id_pembelian_m
									  )
								);
				exit();
			}
		}else{
			redirect(base_url().'pembelian');
			exit();
		}
	}

	public function ambil_data()
	{
		// Cek user acces menu
		$id_pegawai 		= $this->session->userdata('id_pegawai');
		$cek_useraccess 	= $this->useraccess_pusat->cek_access($id_pegawai, '25');
		if($cek_useraccess->act_read == '0' or $cek_useraccess->act_read == '-'){
			redirect(base_url().'pembelian');
			exit();
		}
		
		$id_pembelian_m		= $this->input->post('id_pembelian_m');
		if($id_pembelian_m == ''){
			redirect(base_url().'pembelian');
			exit();
		}

		if($this->session->userdata('usergroup_name') == 'Super Admin'){
			$data_induk 	= $this->pembelian_master->get_master1($id_pembelian_m);
		}else{
			$data_induk 	= $this->pembelian_master->get_master2($id_pembelian_m);
		}

		$data_batal 		= $this->pembelian_master->get_jumlah_batal($id_pembelian_m);
		echo json_encode(array(
			"data_induk" => $data_induk,
			"data_batal" => $data_batal
		));
	}

	public function no_pembelian_baru($id_pegawai){
		if($id_pegawai == ''){
			redirect(base_url().'pembelian');
			exit();
		}
		
		$tanggal_sekarang = date('Y-m-d');
		$bulan_sekarang   = date('m');
		$tahun_sekarang   = date('Y');
		$tahun_sekarang_2 = date('y'); 
		
		$inisial = "PB";
		$akhir   = $this->pembelian_master->no_pembelian_baru($tahun_sekarang, $bulan_sekarang, $id_pegawai);
		
	    if($akhir){
			$no_baru = $akhir->no_baru;
	    }else{
			$no_baru = 1;	
	    }

	    if($no_baru < 10){
			$depan = '000';
	    }elseif($no_baru< 100){
			$depan = '00';
	    }elseif($no_baru< 1000){
			$depan = '0';
	    }else{
			$depan = '';
	    }

		$jumlah            = $no_baru;
		$no_pembelian_baru = $inisial.'-'.$bulan_sekarang.$tahun_sekarang_2.'-'.$id_pegawai.'-'.$depan.$jumlah;
	    return $no_pembelian_baru;
	}

	public function simpan_detail()
	{		
		if($this->input->is_ajax_request()){
			// Cek user acces menu
			$id_pegawai      = $this->session->userdata('id_pegawai');
			$cek_useraccess  = $this->useraccess_pusat->cek_access($id_pegawai, '25');
			
			// Ambil data pembelian master
			$id_barang_pusat = $this->input->post('id_barang_pusat');
			$id_pembelian_m  = $this->clean_tag_input($this->input->post('id_pembelian_m'));
			$no_pembelian    = $this->clean_tag_input($this->input->post('no_pembelian'));
			$no_faktur       = $this->clean_tag_input($this->input->post('no_faktur'));
			$id_supplier     = $this->input->post('id_supplier');
			$tanggal         = date('Y-m-d H:i:s');
			$id_pegawai      = $this->session->userdata('id_pegawai');
			$catatan         = $this->clean_tag_input($this->input->post('catatan'));
			$biaya_lain      = $this->input->post('biaya_lain');
			$ppn             = $this->input->post('ppn');

			if($biaya_lain == '' or $biaya_lain == '0'){
				$biaya_lain = '0';
			}

			if($ppn == '' or $ppn == '0'){
				$ppn = '0';
			}

			// Validasi pembelian master sudah ada atau belum
			$data_pembelian_master = $this->pembelian_master->get_by_id($id_pembelian_m);
			if($data_pembelian_master){
				if($cek_useraccess->act_create == '1' or $cek_useraccess->act_update == '1'){
					$id_master   = $data_pembelian_master->id_pembelian_m;
					$id_supplier = $data_pembelian_master->id_supplier;
				}else{
					echo json_encode(array(	
						'status' => 2,
						'pesan'  => 'Maaf anda tidak diizinkan untuk menyimpan data pembelian barang.',
						'url'    => 'pembelian/transaksi/?&id='.$id_pembelian_m
					));
					exit();
				}
			}else{
				if($cek_useraccess->act_create == '1'){
					// Insert pembelian master
					$no_pembelian = $this->no_pembelian_baru($id_pegawai);
					$master       = $this->pembelian_master->insert_master(
										$no_pembelian, $no_faktur, $id_supplier, 
									   	'0', '0', '0', '0', 
									   	$catatan, $tanggal, $id_pegawai
									);
					$data_pembelian_master = $this->pembelian_master->get_by_no_trans($no_pembelian);
					$id_master             = $data_pembelian_master->id_pembelian_m;
				}else{
					echo json_encode(array(	
						'status' => 2,
						'pesan'  => 'Maaf anda tidak diizinkan untuk menyimpan data pembelian barang.',
						'url'    => 'pembelian/transaksi/?&id='.$id_pembelian_m
					));
					exit();
				}
			}

			if($id_master){
				// Ambil data pembelian detail
				$jumlah_beli = $this->input->post('jumlah_beli');

				// Ambil data barang dari master barang pusat
				$data_barang = $this->barang_pusat->get_id($id_barang_pusat);
				if($data_barang){
					$sku              = $data_barang->sku;
					$nama_barang      = $data_barang->nama_barang;
					$harga            = $data_barang->modal;
					$resistensi_harga = $data_barang->resistensi_modal;
					$harga_bersih     = $data_barang->modal_bersih;
					$subtotal         = $harga * $jumlah_beli;
					$stok_sekarang    = $data_barang->total_stok;
				}

				// Ambil data barang dari inputan
				if($this->session->userdata('usergroup_name') == 'Super Admin'){
					$harga            = $this->input->post('harga_beli');
					$resistensi_harga = $this->input->post('resistensi_harga');
					$harga_bersih     = $this->input->post('harga_bersih');
					$subtotal         = $this->input->post('subtotal');
				}

				// Validasi insert atau update di pembelian detail
				$data_pembelian_detail 		= $this->pembelian_detail->get_id($id_master, $id_barang_pusat);
				if ($data_pembelian_detail){
					if($cek_useraccess->act_update == '1'){
						$id_detail        = $data_pembelian_detail->id_pembelian_d;
						$jumlah_beli_lama = $data_pembelian_detail->jumlah_beli;
						$validasi_stok1   = $stok_sekarang - $jumlah_beli_lama;
						$validasi_stok2   = $validasi_stok1 + $jumlah_beli;

						// Validasi stok
						if($validasi_stok2 < 0){
							echo json_encode(array(
								"status" 	=> 3,
							  	"pesan"   	=> "<b class='text-danger'>
							  						Maaf anda tidak bisa memperbaharui pembelian barang dengan  jumlah : ".$jumlah_beli."
							  					</b></br></br>
							  					SKU : </br>
							  					<b class='text-dark'>".$sku."</b></br></br>

							  					Nama barang : </br>
							  					<b class='text-dark'>".$nama_barang."</b></br></br>

							  					<b class='text-dark'>Karna bisa menyebabkan minus sebanyak : </b></br>
							  					<h2 class='text-danger'>".$validasi_stok2."</h2>
												  					"
							));
							exit();
						}

						// update dan kembalikan stok sebelumnya
						$simpan_data = $this->pembelian_detail->update_detail( 
							$id_master, $id_barang_pusat,  
							$harga, $resistensi_harga, $harga_bersih, $jumlah_beli, $subtotal, $id_pegawai, $jumlah_beli_lama
						);
					}else{
						echo json_encode(array(	
							'status' => 2,
							'pesan'  => 'Maaf anda tidak diizinkan untuk mengedit data pembelian barang.',
							'url'    => 'pembelian/transaksi/?&id='.$id_pembelian_m
						));
						exit();
					}
				}else{
					if($cek_useraccess->act_create == '1'){
						// insert
						$simpan_data = $this->pembelian_detail->insert_detail(
							$id_master, $id_barang_pusat, $harga, $resistensi_harga, 
							$harga_bersih, $jumlah_beli, $subtotal, $id_pegawai
						);
					}else{
						echo json_encode(array(	
							'status' => 2,
							'pesan'  => 'Maaf anda tidak diizinkan untuk menambahkan data pembelian barang.',
							'url'    => 'pembelian/transaksi/?&id='.$id_pembelian_m
						));
						exit();
					}
				}

				if($simpan_data){
					$data_total 		= $this->pembelian_detail->get_total($id_master);
					if($data_total){
						if($this->session->userdata('usergroup_name') !== "Super Admin"){
							$biaya_lain = $data_total->biaya_lain;
							$ppn        = $data_total->ppn;
						}

						$ttl 			= $data_total->total;
						// $ppn 			= $ttl*(10/100);
						$ppn 			= 0;
						$grand_total	= $ttl+$ppn+$biaya_lain;
						$jumlah_barang 	= $data_total->jumlah_barang;

						// Update total dan grandtotal di pembelian master
						$master = $this->pembelian_master->update_master(	
							$id_master, $no_faktur, $id_supplier, 
							$ttl, $biaya_lain, $ppn, $grand_total, 
							$catatan, $id_pegawai, 'MENUNGGU'
						);
					}else{
						$ttl 			= '0';
						$grand_total	= '0';
						$jumlah_barang 	= '0';
					}

					$data_batal 		= $this->pembelian_master->get_jumlah_batal($id_master);
					if($data_batal){
						$jumlah_barang_batal = $data_batal->jumlah_barang;
					}else{
						$jumlah_barang_batal = '0';
					}

					// Tambah stok dan update modal, resistensi dan modal bersih
					$this->barang_pusat->update_tambah_stok($id_barang_pusat, $jumlah_beli);
					$this->barang_pusat->update_modal($id_barang_pusat, $harga, $resistensi_harga, $harga_bersih, $id_pegawai);
					
					if($this->session->userdata('usergroup_name') == "Super Admin"){
						echo json_encode(array(
												'status'              => 1,
												'pesan'               => "Data pembelian barang berhasil disimpan,
																		  Apakah anda ingin mencetak barcodenya ?",
												'url'                 => 'pembelian/transaksi/?&id='.$id_master,
												'jumlah_barang'       => $jumlah_barang,
												'jumlah_barang_batal' => $jumlah_barang_batal,
												'total'               => $ttl,
												'ppn'                 => $ppn,
												'grand_total'         => $grand_total
										));
					}else{
						echo json_encode(array(	'status'              => 1,
												'pesan'               => "Data pembelian barang berhasil disimpan,
																		  Apakah anda ingin mencetak barcodenya ?",
												'url'                 => 'pembelian/transaksi/?&id='.$id_master,
												'jumlah_barang'       => $jumlah_barang,
												'jumlah_barang_batal' => $jumlah_barang_batal
										));
					}
				}else{
					echo json_encode(array(
											'status' 	=> 2,
											'url' 		=> 'pembelian/transaksi/?&id='.$id_master
										  )
									);
				}
			}
		}else{
			redirect(base_url().'pembelian');
		}
	}

	public function ajax_list()
	{
		// Cek user acces menu
		$id_pegawai 				= $this->session->userdata('id_pegawai');
		$cek_useraccess 			= $this->useraccess_pusat->cek_access($id_pegawai, '25');
		if($cek_useraccess->act_read == '0' or $cek_useraccess->act_read == '-'){
			redirect(base_url().'dashboard');
		}

		$id_pembelian_m 			= $this->clean_tag_input($this->input->post('id_pembelian_m'));
		if($id_pembelian_m == ''){
			redirect(base_url().'pembelian');
		}

		$data_pembelian_master 		= $this->pembelian_master->get_by_id($id_pembelian_m);
		if($data_pembelian_master){
			$id_master 				= $data_pembelian_master->id_pembelian_m;
		}else{
			$id_master				= '0';
		}

		$list 						= $this->pembelian_detail->get_datatables('_get_datatables_query', $id_master, '');
		$data 						= array();
		$no 						= $_POST['start'];

		foreach ($list as $pd){
			$no++;
			$row = array();
			$row[] = $no;

			//add html for action
			if($cek_useraccess->act_update == 1){
				$tombol_edit = '<a class="btn btn-rounded btn-default btn-xs" 
									onclick="edit_detail('."'".$pd->id_pembelian_d."'".')"><i class="fa fa-pencil" style="color:blue;"></i>
								</a>';
			}else{
				$tombol_edit = '';
			}

			if($cek_useraccess->act_delete == 1){
				$tombol_hapus = '<a class="btn btn-rounded btn-default btn-xs" 
									onclick="verifikasi_hapus_detail('."'".$pd->id_pembelian_d."'".')"><i class="fa fa-times" style="color:red;"></i>
								</a>';
			}else{
				$tombol_hapus = '';
			}

			if($tombol_edit == '' AND $tombol_hapus == '' AND $cek_useraccess->act_create == ''){
				$tombol_cetak = '<b class="btn btn-rounded btn-default btn-xs text-muted text-center">Tidak ada akses</b>';
			}else{
				$tombol_cetak = '<a class="btn btn-rounded btn-default btn-xs" onclick="cetak_barcode('."'".$pd->id_pembelian_d."'".')">
									<i class="fa fa-barcode"></i>
								</a>';
			}

			$row[] =	'
							'.$tombol_cetak.'
							'.$tombol_edit.'
							'.$tombol_hapus.'
				  		';

			$row[] 	= $pd->sku;
			$row[] 	= $pd->nama_barang;
			$row[] 	= '<span class="pull-right">'.number_format($pd->jumlah_beli,'0',',','.').'</span>';

			if($this->session->userdata('usergroup_name') == 'Super Admin'){
				$row[] 	= '<span class="pull-right">'.number_format($pd->harga,'0',',','.').'</span>';
				$row[] 	= '<span class="pull-right">'.$pd->resistensi_harga.'</span>';
				$row[] 	= '<span class="pull-right">'.number_format($pd->harga_bersih,'0',',','.').'</span>';
				$row[] 	= '<span class="pull-right">'.number_format($pd->subtotal,'0',',','.').'</span>';
			}

			$row[] 	= $pd->pegawai_save;
			$row[] 	= $pd->tanggal_pembuatan;	
			$row[] 	= $pd->pegawai_edit;
			$row[] 	= $pd->tanggal_pembaharuan;				
			$data[]	= $row;
		}

		$output = array(
			"draw"            => $_POST['draw'],
			"recordsTotal"    => $this->pembelian_detail->count_all('vamr4846_vama.pembelian_detail', $id_master, ''),
			"recordsFiltered" => $this->pembelian_detail->count_filtered('_get_datatables_query', $id_master, ''),
			"data"            => $data
		);
		echo json_encode($output);
	}

	public function ajax_list_batal()
	{
		// Cek user acces menu
		$id_pegawai 				= $this->session->userdata('id_pegawai');
		$cek_useraccess 			= $this->useraccess_pusat->cek_access($id_pegawai, '25');
		if($cek_useraccess->act_read == '0' or $cek_useraccess->act_read == '-'){
			redirect(base_url().'dashboard');
		}

		$id_pembelian_m 			= $this->clean_tag_input($this->input->post('id_pembelian_m'));
		if($id_pembelian_m == ''){
			redirect(base_url().'pembelian');
		}

		$data_pembelian_master 		= $this->pembelian_master->get_by_id($id_pembelian_m);
		if($data_pembelian_master){
			$id_master 				= $data_pembelian_master->id_pembelian_m;
		}else{
			$id_master				= '0';
		}

		$list 						= $this->pembelian_detail->get_datatables('_get_datatables_query_batal', $id_master, '');
		$data 						= array();
		$no 						= $_POST['start'];

		foreach ($list as $pd){
			$no++;
			$row = array();
			$row[] = $no;
			$row[] 	= $pd->sku;
			$row[] 	= $pd->nama_barang;
			$row[] 	= '<span class="pull-right">'.number_format($pd->jumlah_beli,'0',',','.').'</span>';

			if($this->session->userdata('usergroup_name') == 'Super Admin'){
				$row[] 	= '<span class="pull-right">'.number_format($pd->harga,'0',',','.').'</span>';
				$row[] 	= '<span class="pull-right">'.$pd->resistensi_harga.'</span>';
				$row[] 	= '<span class="pull-right">'.number_format($pd->harga_bersih,'0',',','.').'</span>';
				$row[] 	= '<span class="pull-right">'.number_format($pd->subtotal,'0',',','.').'</span>';
			}

			$row[] 	= $pd->keterangan_batal;
			$row[] 	= $pd->nama_pegawai;
			$row[] 	= $pd->tanggal_pembatalan;			
			$data[]	= $row;
		}

		$output = array(
			"draw"            => $_POST['draw'],
			"recordsTotal"    => $this->pembelian_detail->count_all('vamr4846_vama.pembelian_detail_batal', $id_master, ''),
			"recordsFiltered" => $this->pembelian_detail->count_filtered('_get_datatables_query_batal', $id_master, ''),
			"data"            => $data
		);
		echo json_encode($output);
	}

	public function ajax_list_barang()
	{
		// Cek user acces menu
		$id_pegawai 				= $this->session->userdata('id_pegawai');
		$cek_useraccess 			= $this->useraccess_pusat->cek_access($id_pegawai, '25');
		if($cek_useraccess->act_read == '0' or $cek_useraccess->act_read == '-'){
			redirect(base_url().'dashboard');
		}

		$id_pembelian_m 			= $this->clean_tag_input($this->input->post('id_pembelian_m'));
		if($id_pembelian_m == ''){
			redirect(base_url().'pembelian');
		}

		$id_supplier 				= $this->clean_tag_input($this->input->post('id_supplier'));
		$data_supplier 				= $this->supplier->get_by_id($id_supplier);
		if($data_supplier){
			$kode_supplier 			= $data_supplier->kode_supplier;
		}else{
			$kode_supplier 			= '0';
		}

		$data_pembelian_master 		= $this->pembelian_master->get_by_id($id_pembelian_m);
		if($data_pembelian_master){
			$id_master 				= $data_pembelian_master->id_pembelian_m;
			$id_supplier 			= $data_pembelian_master->id_supplier;
		}else{
			$id_master				= '0';
			$id_supplier 			= '0';
		}

		$list 						= $this->pembelian_detail->get_datatables('_get_datatables_query_barang', $id_master, $kode_supplier);
		$data 						= array();
		$no 						= $_POST['start'];

		foreach ($list as $bp){
			$no++;
			$row 	= array();
			$row[] 	= $no;

			$row[] 	= $bp->sku;
			$row[] 	= $bp->nama_barang;
			$row[] 	= '<span class="pull-right">'.number_format($bp->total_stok,'0',',','.').'</span>';

			if($this->session->userdata('usergroup_name') == 'Super Admin'){
				$row[] 	= '<span class="pull-right">'.number_format($bp->modal,'0',',','.').'</span>';
				$row[] 	= '<span class="pull-right">'.number_format($bp->modal_bersih,'0',',','.').'</span>';
			}

			$row[] 	=	'<a class="btn btn-primary btn-xs" onclick="proses_barang('."'".$bp->sku."'".')">
							<i class="fa fa-check"></i> PILIH
						</a>';
			$data[]	= $row;
		}

		$output = array(	
			"draw"            => $_POST['draw'],
			"recordsTotal"    => $this->pembelian_detail->count_all('vamr4846_vama.barang_pusat', $id_master, $kode_supplier),
			"recordsFiltered" => $this->pembelian_detail->count_filtered('_get_datatables_query_barang', $id_master, $kode_supplier),
			"data"            => $data
		);
		echo json_encode($output);
	}

	public function ajax_edit()
	{
		// Cek user acces menu
		$id_pegawai     = $this->session->userdata('id_pegawai');
		$cek_useraccess = $this->useraccess_pusat->cek_access($id_pegawai, '25');
		if($cek_useraccess->act_update == '0' or $cek_useraccess->act_update == '-'){
			redirect(base_url()."pembelian");
			exit();
		}

		$id_pembelian_d 	= $this->input->post('id_pembelian_d');
		$url 				= $this->input->post('url');
		if($id_pembelian_d == ''){
			if($url == ''){
				redirect(base_url()."pembelian");					
			}else{
				redirect(base_url().$url);
			}
		}else{
			if($this->session->userdata('usergroup_name') == "Super Admin"){
				$data = $this->pembelian_detail->get_pembelian_detail_1($id_pembelian_d);
				echo json_encode(array(
										"status" 	=> 1,
										"data" 		=> $data
									  )
								);
			}else{
				$data = $this->pembelian_detail->get_pembelian_detail_2($id_pembelian_d);
				echo json_encode(array(
										"status" 	=> 1,
										"data" 		=> $data
									  )
								);				
			}
		}
	}

	public function ajax_cetak_barcode()
	{
		$decoder = GdDecoder::fromPath('happy.png');
		$image = new Image($decoder);

		$zpl = new Builder();
		$zpl->fo(50, 50)->gf($image)->fs();

		$client = new Client('10.0.0.50');
		$client->send($zpl);

		// Cek user acces menu
		// $id_pegawai     = $this->session->userdata('id_pegawai');
		// $cek_useraccess = $this->useraccess_pusat->cek_access($id_pegawai, '25');
		// if($cek_useraccess->act_update == '0' or $cek_useraccess->act_update == '-'){
		// 	redirect(base_url()."pembelian");
		// 	exit();
		// }

		// $id_pembelian_d = $this->input->get('id_pembelian_d');
		// if($id_pembelian_d == ''){
		// 	redirect(base_url().'pembelian');
		// 	exit();	
		// }else{
			// $data = $this->pembelian_detail->get_pembelian_detail_2($id_pembelian_d);

			// -------------------------------------------------------------------------------------
			// $mpdf = new \Mpdf\Mpdf([
			// 	'mode'          => 'utf-8',
			// 	'format'        => [100, 18],
			// 	'margin_left'   => 0,
			// 	'margin_right'  => 0,
			// 	'margin_top'    => 0,
			// 	'margin_bottom' => 0,
			// 	'margin_header' => 0,
			// 	'margin_footer' => 0
			// ]);				

			// $html = $this->load->view('admin/transaksi/pembelian/cetak_barcode', $data, true);
			// $mpdf->WriteHTML($html);
			// $mpdf->Output();
			// $mpdf->Output($data->sku.'.pdf');


		// }
	}

	public function ajax_verifikasi_hapus_detail()
	{
		// Cek user acces menu
		$id_pegawai 		= $this->session->userdata('id_pegawai');
		$cek_useraccess 	= $this->useraccess_pusat->cek_access($id_pegawai, '25');
		if($cek_useraccess->act_delete == '0' or $cek_useraccess->act_delete == '-'){
			redirect(base_url()."pembelian");
			exit();
		}

		$id_pembelian_d 	= $this->input->post('id_pembelian_d');
		$url 				= $this->input->post('url');
		if($id_pembelian_d == ''){
			if($url == ''){
				redirect(base_url()."pembelian");					
			}else{
				redirect(base_url().$url);
			}
		}

		$data = $this->pembelian_detail->get_pembelian_detail_1($id_pembelian_d);
		if($data){
			$id_barang_pusat 		= $data->id_barang_pusat;
			$jumlah_beli 			= $data->jumlah_beli;

			// Ambil data barang dari master barang pusat
			$data_barang 			= $this->barang_pusat->get_id($id_barang_pusat);
			if($data_barang){
				$sku 				= $data_barang->sku;
				$nama_barang 		= $data_barang->nama_barang;
				$stok_sekarang 		= $data_barang->total_stok;

				$validasi_stok1 	= $stok_sekarang - $jumlah_beli;
				$validasi_stok2 	= $validasi_stok1 + 0;

				// Validasi stok
				if($validasi_stok2 < 0){
					echo json_encode(array( "status" 	=> 3,
										  	"pesan"   	=> "<b class='text-danger'>
										  						Maaf anda tidak bisa menghapus pembelian barang dengan  jumlah : ".$jumlah_beli."
										  					</b></br></br>
										  					SKU : </br>
										  					<b class='text-dark'>".$sku."</b></br></br>

										  					Nama barang : </br>
										  					<b class='text-dark'>".$nama_barang."</b></br></br>

										  					<b class='text-dark'>Karna bisa menyebabkan minus sebanyak : </b></br>
										  					<h2 class='text-danger'>".$validasi_stok2."</h2>
										  					",
										  	'footer'	=> 	"<button type='button' class='btn btn-danger waves-effect' data-dismiss='modal'>
										  						Ok, saya mengerti
										  					</button>"
										 )
								   );
					exit();
				}else{
					echo json_encode(array(	"status" 	=> 1,
											"pesan" 	=> "<table class='table' id='detail_hapus'>
																<thead></thead>
																<tbody>
																	<tr>
																		<td>
																			<small>SKU : </small></br>
																			<b class='text-dark'>$data->sku </b><br/></br>

																			<small>Nama Barang : </small> <br/>	
																			<b class='text-dark'>$data->nama_barang </b> </br><br/>

																			<small>Jumlah Beli : </small></br>
																			<h2 class='text-dark'>$data->jumlah_beli </h2>

																			<textarea name='keterangan_batal' id='keterangan_batal' class='form-control input-md' placeholder='Keterangan batal' style='resize: vertical;'></textarea>
																		</td>
																	</tr>
																</tbody>
															</table>",
											'footer'	=> 	"<button onclick='hapus_detail($data->id_pembelian_d)' 
																type='button' class='btn btn-primary waves-effect waves-light' 
																data-dismiss='modal' autofocus>
																Iya, Hapus
															</button> 
															<button type='button' class='btn btn-default waves-effect' data-dismiss='modal'>
																Batal
															</button>"
										  )
									);
				}
			}else{
				redirect(base_url().'pembelian');
			}
		}else{
			redirect(base_url().'pembelian');
		}
	}

	public function ajax_hapus_detail()
	{
		if($this->input->is_ajax_request()){
			// Cek user acces menu
			$id_pegawai 			= $this->session->userdata('id_pegawai');
			$cek_useraccess 		= $this->useraccess_pusat->cek_access($id_pegawai, '25');
			if($cek_useraccess->act_delete == '0' or $cek_useraccess->act_delete == '-'){
				redirect(base_url()."pembelian");
				exit();
			}

			$id_pegawai_pembatalan = $this->session->userdata('id_pegawai');
			$keterangan_batal      = $this->input->post('keterangan_batal');
			$id_supplier           = $this->input->post('id_supplier');
			$catatan               = $this->clean_tag_input($this->input->post('catatan'));
			$biaya_lain            = $this->input->post('biaya_lain');
			
			if($biaya_lain == '' or $biaya_lain == '0'){
				$biaya_lain = 0;
			}

			$id_pembelian_d 		= $this->input->post('id_pembelian_d');
			$url 					= $this->input->post('url');

			// Ambil data pembelian detail
			$dt						= $this->pembelian_detail->get_pembelian_detail_1($id_pembelian_d);
			$id_barang_pusat 		= $dt->id_barang_pusat;
			$jumlah_beli 			= $dt->jumlah_beli;

			// Ambil data barang dari master barang pusat
			$data_barang 			= $this->barang_pusat->get_id($id_barang_pusat);
			if($data_barang){
				$sku 				= $data_barang->sku;
				$nama_barang 		= $data_barang->nama_barang;
				$stok_sekarang 		= $data_barang->total_stok;

				$validasi_stok1 	= $stok_sekarang - $jumlah_beli;
				$validasi_stok2 	= $validasi_stok1 + 0;

				// Validasi stok
				if($validasi_stok2 < 0){
					echo json_encode(array(
											"status" 	=> 3,
										  	"pesan"   	=> "<b class='text-danger'>
										  						Maaf anda tidak bisa menghapus pembelian barang dengan  jumlah : ".$jumlah_beli."
										  					</b></br></br>
										  					SKU : </br>
										  					<b class='text-dark'>".$sku."</b></br></br>

										  					Nama barang : </br>
										  					<b class='text-dark'>".$nama_barang."</b></br></br>

										  					<b class='text-dark'>Karna bisa menyebabkan minus sebanyak : </b></br>
										  					<h2 class='text-danger'>".$validasi_stok2."</h2>
										  					",
										  	'footer'	=> 	"<button type='button' class='btn btn-primary waves-effect' data-dismiss='modal'>Ok, saya mengerti</button>"
										 )
								   );
					exit();
				}else{
					if($dt){
						$id_master 			= $dt->id_pembelian_m;
						$no_faktur 			= $dt->no_faktur;
						$no_pembelian 		= $dt->no_pembelian;

						$hapus 				= $this->pembelian_detail->hapus_pembelian_detail(
												$dt->id_pembelian_d, $dt->id_pembelian_m, $dt->id_supplier, $no_pembelian, $no_faktur,
												$dt->id_barang_pusat, $dt->harga, $dt->resistensi_harga,
												$dt->harga_bersih, $dt->jumlah_beli, $dt->subtotal,

												$dt->id_pegawai_pembuatan, $dt->id_pegawai_pembaharuan, $id_pegawai_pembatalan,
												$dt->tanggal_pembuatan, $dt->tanggal_pembaharuan, $keterangan_batal
											  );
					}
					
					if($hapus){
						$data_total 		= $this->pembelian_detail->get_total($id_master);				
						if($data_total){
							if($this->session->userdata('usergroup_name') !== "Super Admin"){
								$biaya_lain = $data_total->biaya_lain;
								$ppn        = $data_total->ppn;
							}
							
							$ttl 			= $data_total->total;
							// $ppn 			= $ttl*(10/100);
							$ppn 			= 0;
							$grand_total	= $ttl+$ppn+$biaya_lain;
							$jumlah_barang 	= $data_total->jumlah_barang;

							if($jumlah_barang == '0'){
								$hapus 		= $this->pembelian_master->hapus_transaksi($id_master, $dt->id_supplier, 
											  $no_pembelian, $no_faktur, $keterangan_batal, $id_pegawai_pembatalan);
								echo json_encode(array('status' 	=> 0,
													   'judul'		=> 'Berhasil!',
													   'pesan' 		=> 'Data pembelian barang berhasil dihapus.',
													   'tipe_pesan'	=> 'success',
													   'url' 		=> 'pembelian'
								));
								exit();
							}

							// Update total dan grandtotal di pembelian master
							$master = $this->pembelian_master->update_master($id_master, $no_faktur, $id_supplier, 
																			 $ttl, $biaya_lain, $ppn, $grand_total, 
																			 $catatan, $id_pegawai_pembatalan, 'MENUNGGU');
						}else{
							$ttl 			= '0';
							$grand_total	= '0';
							$jumlah_barang 	= '0';
						}

						$data_batal 		= $this->pembelian_master->get_jumlah_batal($id_master);
						if($data_batal){
							$jumlah_barang_batal = $data_batal->jumlah_barang;
						}else{
							$jumlah_barang_batal = '0';
						}

						// Tampilkan informasi hapus sesuai dengan user group
						if($this->session->userdata('usergroup_name') == "Super Admin"){
							echo json_encode(array(
													'status' 				=> '1',
													'id_pm'					=> $id_master,
													'jumlah_barang'			=> $jumlah_barang,
													'jumlah_barang_batal'	=> $jumlah_barang_batal,
													'total'					=> $ttl,
													'ppn'					=> $ppn,
													'grand_total'			=> $grand_total,
													'pesan' 				=>	"Data berhasil dihapus</br>"
											));
						}else{
							echo json_encode(array(
													'status' 				=> '1',
													'id_pm'					=> $id_master,
													'jumlah_barang'			=> $jumlah_barang,
													'jumlah_barang_batal'	=> $jumlah_barang_batal,
													'pesan' 				=>	"Data berhasil dihapus</br>"
											));
						}
					}else{
						echo json_encode(array('status' 	=> 0,
											   'judul'		=> 'Gagal!',
											   'pesan' 		=> 'Gagal menghapus data dari ajax.',
											   'tipe_pesan'	=> 'error',
											   'url' 		=> $url
										));
					}
				}
			}
		}else{
			redirect(base_url().'pembelian');
		}
	}

	public function cek_nota($nota)
	{
		$this->load->model('pembelian_master_model');
		$cek = $this->pembelian_master_model->cek_nota_validasi($nota);

		if($cek->num_rows() > 0)
		{
			return FALSE;
		}
		return TRUE;
	}

	public function ajax_kode()
	{
		if($this->input->is_ajax_request()){
			// Cek user acces menu
			$id_pegawai     = $this->session->userdata('id_pegawai');
			$cek_useraccess = $this->useraccess_pusat->cek_access($id_pegawai, '25');
			if($cek_useraccess->act_create == '0' or $cek_useraccess->act_create == '-'){
				$json['status']      = 2;
				$json['judul_pesan'] = 'Oops!';
				$json['isi_pesan']   = 'Maaf anda tidak diberikan izin untuk menambahkan barang pembelian.';
			}else{
				$keyword       = $this->input->post('keyword');
				$id_pm         = $this->input->post('id_pm');
				$kode_supplier = $this->input->post('kode_supplier');
				$barang        = $this->barang_pusat->cari_kode_by_supplier($keyword, $id_pm, $kode_supplier);

				if($barang->num_rows() > 0){
					$json['kode_supplier'] = $kode_supplier;
					$json['status']        = 1;
					$json['datanya']       = "<ul id='daftar-autocomplete' class='list-group user-list'>";
					
					foreach($barang->result() as $b){
						if($b->foto!=""){
							$json['datanya'] .= "
				                <li class='user-list-item'>
				                	<div class='avatar avatar-sm-box'>
	                                    <img src='assets/upload/image/barang/thumbs/$b->foto'>
				                	</div>";
				        }else{
							$nama_barang 	  = $b->nama_barang;
							$inisial 		  = substr($nama_barang, 0, 1);
							$json['datanya'] .= "
				                <li class='user-list-item'>
				                	<div class='avatar avatar-sm-box'>
	                                    <span class='avatar-sm-box bg-info'>".$inisial."</span>
				                	</div>";
						}

						$json['datanya'] .= "	<div class='user-desc'>
													<span id='barangnya' class='name'><b>".$b->nama_barang."</b></span>
													<span id='kode_supplier' class='name'>".$b->nama_supplier."</span>
													<span id='skunya' class='name'>".$b->sku."</span>
													<span id='total_stok' class='name'>Stok : ".$b->total_stok."</span>
													<span id='id_barang' style='display :none;'>".$b->id_barang_pusat."</span>
											";

						if($this->session->userdata('usergroup_name') == 'Super Admin'){
							$json['datanya'] .= "		<span id='harga_modal' class='name'>MODALE : Rp. ".$b->harga_modal."</span>
														<span id='res_mod' class='name'>RESISTENSI : ".$b->resistensi_modal."</span>
														<span id='harga_modal_bersih' class='name'>MODAL BERSIH : Rp. ".$b->harga_modal_bersih."</span>

														<span id='modal' style='display:none;'>".$b->modal."</span>
														<span id='resistensi_modal' style='display:none;'>".$b->resistensi_modal."</span>
														<span id='modal_bersih' style='display:none;'>".$b->modal_bersih."</span>
												";
						}

						$json['datanya'] .= "	
									</div>
								</li>
						";
					}

					$json['datanya'] .= "</ul>";
				}else{
					$json['status'] = 0;
				}
			}
			echo json_encode($json);
		}else{
			redirect(base_url().'pembelian');
		}
	}
}
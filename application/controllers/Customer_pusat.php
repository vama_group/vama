<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Customer_pusat extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('pegawai_pusat_model','pegawai_pusat');
        $this->load->model('useraccess_pusat_model','useraccess_pusat');
		$this->load->model('customer_pusat_model','customer_pusat');
		$this->load->model('referensi_sumber_online_model', 'referensi_sumber_online');
	}

	public function index()
	{
		// Cek user acces menu
		$id_pegawai     = $this->session->userdata('id_pegawai');
		$cek_useraccess = $this->useraccess_pusat->cek_access($id_pegawai, '11');
		if($cek_useraccess){
			if($cek_useraccess->act_read == '0' or $cek_useraccess->act_read == '-'){
				redirect(base_url().'dashboard');
			}					
		}else{
			redirect(base_url().'dashboard');
		}

		// cari data pegawai
		$id_pegawai              = $this->session->userdata('id_pegawai');
		$data_pegawai            = $this->pegawai_pusat->get_by_id($id_pegawai);
		$data['access_create']   = $cek_useraccess->act_create;
		$data['access_update']   = $cek_useraccess->act_update;
		$data['access_delete']   = $cek_useraccess->act_delete;
		$data['data_pegawai']    = $data_pegawai;
		$data['data_induk_menu'] = $this->useraccess_pusat->get_induk_menu($id_pegawai);
		
		$data['atribut_halaman'] = 'Customer Pusat';
		$data['list_sumber']     = $this->referensi_sumber_online->listing();
		$data['halaman_list']    = $this->load->view('admin/master_data/customer_pusat/list',$data,true);
		$data['halaman_form']    = $this->load->view('admin/master_data/customer_pusat/form',$data,true);
		$data['halaman_plugin']  = $this->load->view('admin/master_data/customer_pusat/plugin',$data,true);
		$data['isi_halaman']     = $this->load->view('admin/master_data/customer_pusat/index',$data,true);
		$this->load->view('admin/layout',$data);

		// $this->load->helper('url');
		// $this->load->view('customer_pusat_view');
	}

	public function ajax_list()
	{
		// Cek user acces menu
		$id_pegawai 	= $this->session->userdata('id_pegawai');
		$cek_useraccess = $this->useraccess_pusat->cek_access($id_pegawai, '11');
		if($cek_useraccess->act_read == '0' or $cek_useraccess->act_read == '0'){
			redirect(base_url().'dashboard');
			exit();
		}

		$id_periode_stok_pusat = $this->input->post('id_periode_stok_pusat');
		if($id_periode_stok_pusat == ''){
			redirect(base_url().'customer_pusat');
			exit();
		}

		$list = $this->customer_pusat->get_datatables('_get_datatables_query', '');
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $customer_pusat) {
			$no++;
			$row 	= array();
			$row[] 	= $no;

			//add html for action
			if($cek_useraccess->act_update == 1){
				$tombol_edit 	 = '<a class="btn btn-rounded btn-xs btn-default" href="javascript:void(0)" title="Edit" 
										onclick="edit_customer_pusat('."'".$customer_pusat->id_customer_pusat."'".')">
										<i class="fa fa-pencil" style="color:blue;"></i>
									</a>';
			}else{$tombol_edit 	 = '';}

			if($cek_useraccess->act_delete == 1){
				$tombol_hapus 	 = '<a class="btn btn-rounded btn-xs btn-default" href="javascript:void(0)" title="Hapus" 
										onclick="verifikasi_delete('."'".$customer_pusat->id_customer_pusat."'".')">
										<i class="fa fa-remove" style="color:red;"></i>
					  				</a>';
			}else{$tombol_hapus  = '';}

			if($cek_useraccess->act_update == 1 or $cek_useraccess->act_delete == 1){
				$row[] 	= '
					  		'.$tombol_edit.'
						  	'.$tombol_hapus.'
					  	';
			}

			$row[] 	= $customer_pusat->kode_customer_pusat;
			$row[] 	= $customer_pusat->nama_customer_pusat;
			$row[] 	= $customer_pusat->email;
			$row[] 	= $customer_pusat->telephone1;
			$row[] 	= $customer_pusat->handphone1;
			$row[] 	= $customer_pusat->asal_customer_pusat;
			$row[] 	= $customer_pusat->nama_sumber_online;
			$row[] 	= $customer_pusat->tipe_customer_pusat;

			$row[]	= $customer_pusat->pegawai_save;
			$row[] 	= $customer_pusat->tanggal_pembuatan;
			$row[] 	= $customer_pusat->pegawai_edit;
			$row[] 	= $customer_pusat->tanggal_pembaharuan;
			$data[] = $row;
		}

		$output = array(
			"draw"            => $_POST['draw'],
			"recordsTotal"    => $this->customer_pusat->count_all('vamr4846_vama.customer_pusat', ''),
			"recordsFiltered" => $this->customer_pusat->count_filtered('_get_datatables_query', ''),
			"data"            => $data
		);
		echo json_encode($output);
	}

	public function ajax_list_pendapatan_poin()
	{
		// Cek user acces menu
		$id_pegawai 	= $this->session->userdata('id_pegawai');
		$cek_useraccess = $this->useraccess_pusat->cek_access($id_pegawai, '11');
		if($cek_useraccess->act_read == '0' or $cek_useraccess->act_read == '0'){
			redirect(base_url().'dashboard');
			exit();
		}

		$id_customer_pusat = $this->input->post('id_customer_pusat');
		if($id_customer_pusat == ''){
			redirect(base_url().'customer_pusat');
			exit();
		}

		$list = $this->customer_pusat->get_datatables('_get_datatables_query_pendapatan_poin', $id_customer_pusat);
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $customer_pusat) {
			$no++;
			$row 	= array();
			$row[] 	= $no;
			$row[] 	= $customer_pusat->no_penjualan;
			$row[] 	= 'Rp. '.number_format($customer_pusat->total,'0',',','.');
			$row[] 	= '<span class="pull-right">'.number_format($customer_pusat->jumlah_poin,'0',',','.').'</span>';
			$row[] 	= $customer_pusat->tanggal_pembuatan;
			$row[] 	= $customer_pusat->pegawai_save;
			if($customer_pusat->tanggal_pembuatan == $customer_pusat->tanggal_pembaharuan){
				$row[] 	= '';
			}else{
				$row[] 	= $customer_pusat->tanggal_pembaharuan;
			}
			$row[] 	= $customer_pusat->pegawai_edit;
			$data[] = $row;
		}

		$output = array(
			"draw"              => $_POST['draw'],
			"recordsTotal"      => $this->customer_pusat->count_all('vamr4846_vama.poin_penjualan_online', $id_customer_pusat),
			"recordsFiltered"   => $this->customer_pusat->count_filtered('_get_datatables_query_pendapatan_poin', $id_customer_pusat),
			"data"              => $data,
			"id_customer_pusat" => $id_customer_pusat
		);
		echo json_encode($output);
	}

	public function ajax_add()
	{
		// Cek user acces menu
		$id_pegawai 	= $this->session->userdata('id_pegawai');
		$cek_useraccess = $this->useraccess_pusat->cek_access($id_pegawai, '11');
		if($cek_useraccess->act_create == '0' or $cek_useraccess->act_create == '-'){
			redirect(base_url().'customer_pusat');
			exit();
		}

		$this->_validate('0');
		$tipe_customer_pusat	= $this->input->post('tipe_customer_pusat');
		if($tipe_customer_pusat == 'mrc'){
			$akhir				= $this->customer_pusat->akhir_toko();
			$inisial			= "mrc";
		}else{
			$akhir				= $this->customer_pusat->akhir_umum();
			$inisial			= "CSP";
		}

		// Buat kode customer
		$jumlah_customer	= $akhir->jumlah_customer;
		if($jumlah_customer < 10){
			if($tipe_customer_pusat == 'mrc'){
				$depan 		= '00';
			}else{
				$depan 		= '000';
			}
		}elseif($jumlah_customer < 100){
			if($tipe_customer_pusat == 'mrc'){
				$depan 		= '0';
			}else{
				$depan 		= '00';
			}
		}elseif($jumlah_customer < 1000){
			if($tipe_customer_pusat == 'mrc'){
				$depan 		= '';
			}else{
				$depan 		= '0';
			}
		}elseif($jumlah_customer < 10000){
			if($tipe_customer_pusat == 'mrc'){
				$depan 		= '';
			}else{
				$depan 		= '';
			}
		}

		$kode_customer_pusat 	= $inisial.$depan.$jumlah_customer;
		// // Akhir buat kode baru

		$this->_validate('0');
		$data = array(	
			'kode_customer_pusat'   => $kode_customer_pusat,
			'nama_customer_pusat'   => $this->input->post('nama_customer_pusat'),
			'no_siup'               => $this->input->post('no_siup'),
			'alamat_customer_pusat' => $this->input->post('alamat_customer_pusat'),
			'asal_customer_pusat'   => $this->input->post('asal_customer_pusat'),
			'id_referensi'          => $this->input->post('sumber_online'),
			'tipe_customer_pusat'   => $tipe_customer_pusat,
			'kontak_pribadi'        => $this->input->post('kontak_pribadi'),
			'jabatan'               => $this->input->post('jabatan'),
			'pin_bb1'               => $this->input->post('pin_bb1'),
			'pin_bb2'               => $this->input->post('pin_bb2'),
			'telephone1'            => $this->input->post('telephone1'),
			'telephone2'            => $this->input->post('telephone2'),
			'handphone1'            => $this->input->post('handphone1'),
			'handphone2'            => $this->input->post('handphone2'),
			'fax'                   => $this->input->post('fax'),
			'email'                 => $this->input->post('email'),						
			'id_pegawai_pembuatan'  => $this->session->userdata('id_pegawai'),
			'tanggal_pembuatan'     => date('Y-m-d H:i:s')
		);
		$insert = $this->customer_pusat->save($data);
		if($insert){
			if($tipe_customer_pusat == 'mrc'){
				$this->customer_pusat->create_table_toko($kode_customer_pusat);
			}
		}
		echo json_encode(array("status" => TRUE));
	}

	public function ajax_edit()
	{
		// Cek user acces menu
		$id_pegawai 	= $this->session->userdata('id_pegawai');
		$cek_useraccess = $this->useraccess_pusat->cek_access($id_pegawai, '11');
		if($cek_useraccess->act_update == '0' or $cek_useraccess->act_update == '-'){
			redirect(base_url().'customer_pusat');
			exit();
		}

		$id_customer_pusat = $this->input->post('id_customer_pusat');
		if($id_customer_pusat == ''){
			redirect(base_url().'customer_pusat');
			exit();
		}else{
			$data = $this->customer_pusat->get_by_id($id_customer_pusat);
			echo json_encode($data);
		}
	}

	public function ajax_update()
	{
		$id_pegawai 	= $this->session->userdata('id_pegawai');
		$cek_useraccess = $this->useraccess_pusat->cek_access($id_pegawai, '11');
		if($cek_useraccess->act_update == '0' or $cek_useraccess->act_update == '0'){
			redirect(base_url().'customer_pusat');
			exit();
		}

		$id_customer_pusat = $this->input->post('id_customer_pusat');
		if($id_customer_pusat == ''){
			redirect(base_url().'customer_pusat');
			exit();
		}

		$this->_validate($id_customer_pusat);
		$data = array(
			'nama_customer_pusat'    => $this->input->post('nama_customer_pusat'),
			'no_siup'                => $this->input->post('no_siup'),
			'alamat_customer_pusat'  => $this->input->post('alamat_customer_pusat'),
			'asal_customer_pusat'    => $this->input->post('asal_customer_pusat'),
			'id_referensi'           => $this->input->post('sumber_online'),
			'tipe_customer_pusat'    => $this->input->post('tipe_customer_pusat'),
			'kontak_pribadi'         => $this->input->post('kontak_pribadi'),
			'jabatan'                => $this->input->post('jabatan'),
			'pin_bb1'                => $this->input->post('pin_bb1'),
			'pin_bb2'                => $this->input->post('pin_bb2'),
			'telephone1'             => $this->input->post('telephone1'),
			'telephone2'             => $this->input->post('telephone2'),
			'handphone1'             => $this->input->post('handphone1'),
			'handphone2'             => $this->input->post('handphone2'),
			'fax'                    => $this->input->post('fax'),
			'email'                  => $this->input->post('email'),
			'id_pegawai_pembaharuan' => $this->session->userdata('id_pegawai')
		);
		$this->customer_pusat->update(array('id_customer_pusat' => $this->input->post('id_customer_pusat')), $data);
		echo json_encode(array("status" => TRUE));
	}

	public function ajax_verifikasi_delete()
	{
		// Cek user acces menu
		$id_pegawai 	= $this->session->userdata('id_pegawai');
		$cek_useraccess = $this->useraccess_pusat->cek_access($id_pegawai, '11');
		if($cek_useraccess->act_delete == '0' or $cek_useraccess->act_delete == '-'){
			redirect(base_url().'customer_pusat');
			exit();
		}

		$id_customer_pusat = $this->input->post('id_customer_pusat');
		if($id_customer_pusat == ''){
			redirect(base_url().'customer_pusat');
			exit();
		}
		
		$data = $this->customer_pusat->get_by_id($id_customer_pusat);		
		echo json_encode(array(
								'pesan' 	=> "<b class='text-danger'>Yakin ingin menghapus customer ini ? </b></br></br>
												Nama Customer : </br>
												<h2 class='text-dark'>".$data->nama_customer_pusat."</h2></br>

												Tipe Customer : </br>
												<b class='text-dark'>".$data->tipe_customer_pusat."</b></br></br>

												Email : </br>
												<b class='text-dark'>".$data->email."</b></br></br>

												Handphone : </br>
												<b class='text-dark'>".$data->handphone1."</b></br></br>",

								'footer'	=> 	"<button onclick='delete_customer_pusat($id_customer_pusat)' 
													type='button' class='btn btn-primary waves-effect waves-light' 
													data-dismiss='modal' autofocus>Iya, Hapus</button> 
												<button type='button' class='btn btn-default waves-effect' 
													data-dismiss='modal'>Batal</button>"
							  	)
						);
	}

	public function ajax_delete()
	{
		$id_pegawai 	= $this->session->userdata('id_pegawai');
		$cek_useraccess = $this->useraccess_pusat->cek_access($id_pegawai, '11');
		if($cek_useraccess->act_delete == '0' or $cek_useraccess->act_delete == '-'){
			redirect(base_url().'customer_pusat');
			exit();
		}

		$id_customer_pusat = $this->input->post('id_customer_pusat');
		if($id_customer_pusat == ''){
			redirect(base_url().'customer_pusat');
			exit();
		}

		$this->customer_pusat->update_status_hapus($id_customer_pusat, $id_pegawai);
		echo json_encode(array("status" => TRUE));
	}

	public function ajax_import_customer_pusat()
	{
		// Cek user acces menu
		$id_pegawai 	= $this->session->userdata('id_pegawai');
		$cek_useraccess = $this->useraccess_pusat->cek_access($id_pegawai, '11');
		if($cek_useraccess->act_update == '0' or $cek_useraccess->act_update == '-'){
			redirect(base_url().'customer_pusat');
			exit();
		}

		$data_customer = $this->customer_pusat->ambil_customer_pusat_lama();
		if($data_customer){
			$no = 1;
			ini_set('max_execution_time', 0); 
			ini_set('memory_limit','2048M');
			foreach($data_customer->result() as $c){
				// Validasi barang
				$validasi_customer = $this->customer_pusat->validasi_customer($c->Kode_Customer);
				if($validasi_customer){
				}else{

					// Awal kode customer baru
					$akhir   = $this->customer_pusat->akhir_umum();
					$inisial = "CSP";
					$jumlah_customer = $akhir->jumlah_customer;
					if($jumlah_customer < 10){
						$depan 		= '000';
					}elseif($jumlah_customer < 100){
						$depan 		= '00';
					}elseif($jumlah_customer < 1000){
						$depan 		= '0';
					}elseif($jumlah_customer < 10000){
						$depan 		= '';
					}
					$kode_customer_baru = $inisial.$depan.$jumlah_customer;
					// Akhir kode customer baru

					$sql_insert_customer = "
						INSERT INTO vamr4846_vama.customer_pusat (
							kode_customer_lama, kode_customer_pusat, nama_customer_pusat, no_siup,
							alamat_customer_pusat, asal_customer_pusat, id_referensi,
							tipe_customer_pusat, kontak_pribadi, jabatan, foto,
							telephone1, telephone2, handphone1, handphone2, 
							fax, email, status_hapus,
							id_pegawai_pembuatan, id_pegawai_pembaharuan,
							tanggal_pembuatan, tanggal_pembaharuan
						)  
						VALUES (
							'".$c->Kode_Customer."', '".$kode_customer_baru."', '".$c->Nama_Customer."', '".$c->No_SIUP."',
							'".$c->Alamat_Customer."', '".$c->Asal_Customer."', '0',
							'".$c->Tipe_Customer."', '".$c->Contact_Person."', '".$c->Jabatan."', '',
							'".$c->Telephone1."', '".$c->Telephone2."', '".$c->Handphone1."', '".$c->Handphone2."', 
							'".$c->Fax."', '".$c->Email."', '".$c->Status_Hapus."',
							'1', '0',
							'".$c->Tanggal_Pembuatan."', '".$c->Tanggal_Pembaharuan."'
						)
					";
					$this->db->query($sql_insert_customer);

					$no = $no + 1;
				}
			}
		}
		if($no > 1){
			echo json_encode(array(
				'status' => 1
			));
		}else{
			echo json_encode(array(
				'status' => 0
			));
		}
	}

	private function _validate($id_customer_pusat)
	{
		$data = array();
		$data['error_string'] 	= array();
		$data['inputerror'] 	= array();
		$data['status'] 		= TRUE;
		$nama_customer_pusat 	= $this->input->post('nama_customer_pusat');
		$email 					= $this->input->post('email');
		$handphone1 			= $this->input->post('handphone1');
		$handphone2 			= $this->input->post('handphone2');

		if($this->input->post('alamat_customer_pusat') == ''){
			$data['inputerror'][] 	= 'alamat_customer_pusat';
			$data['error_string'][] = 'Alamat customer pusat wajib diisi';
			$data['status'] 		= FALSE;
		}

		// if($this->input->post('email') == ''){
		// 	$data['inputerror'][]	= 'email';
		// 	$data['error_string'][] = 'Email customer pusat wajib diisi';
		// 	$data['status'] 		= FALSE;
		// }

		// if($this->input->post('handphone1') == ''){
		// 	$data['inputerror'][] 	= 'handphone1';
		// 	$data['error_string'][] = 'Handphone-1 customer pusat wajib diisi';
		// 	$data['status'] 		= FALSE;
		// }

		// $validasi_handphone1 = $this->customer_pusat->get_by_handphone1($handphone1);
		// if($validasi_handphone1){
		// 	$data['inputerror'][] 	= 'handphone1';
		// 	$data['error_string'][] = 'No. handphone customer pusat sudah digunakan, harap ganti no. handphone customer';
		// 	$data['status'] 		= 0;
		// }

		// $validasi_handphone2 = $this->customer_pusat->get_by_handphone1($handphone2);
		// if($validasi_handphone2){
		// 	$data['inputerror'][] 	= 'handphone2';
		// 	$data['error_string'][] = 'No. handphone customer pusat sudah digunakan, harap ganti no. handphone customer';
		// 	$data['status'] 		= 0;
		// }

	    if($nama_customer_pusat == ''){
			$data['inputerror'][] 	= 'nama_customer_pusat';
			$data['error_string'][] = 'Nama customer pusat wajib diisi';
			$data['status'] 		= FALSE;
		}

		$validasi_nama = $this->customer_pusat->get_by_nama($nama_customer_pusat);
		if($validasi_nama){
			if($id_customer_pusat <> $validasi_nama->id_customer_pusat){
				$data['inputerror'][] 	= 'nama_customer_pusat';
				$data['error_string'][] = 'Nama customer pusat sudah digunakan, harap ganti nama customer';
				$data['status'] 		= FALSE;
			}
		}

		// $validasi_email = $this->customer_pusat->get_by_email($email);
		// if($validasi_email){
		// 	if($id_customer_pusat <> $validasi_email->id_customer_pusat){
		// 		$data['inputerror'][] 	= 'email';
		// 		$data['error_string'][] = 'Email customer pusat sudah digunakan, harap ganti email customer';
		// 		$data['status'] 		= FALSE;
		// 	}
		// }

		if($data['status'] === FALSE){
			echo json_encode($data);
			exit();
		}
	}
}

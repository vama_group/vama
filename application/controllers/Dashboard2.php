<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard2 extends MY_Controller {
	
	public function __construct() {
        parent::__construct();
        $this->CI =& get_instance();
        $this->load->model('pegawai_pusat_model','pegawai_pusat');
        $this->load->model('useraccess_pusat_model','useraccess_pusat');
        $this->load->model('customer_pusat_model', 'customer_pusat');
        $this->load->model('dashboard_detail_model', 'dashboard_detail');
        $this->load->model('penjualan_master_model', 'penjualan_master');
        $this->load->model('retur_penjualan_master_model', 'retur_penjualan_master');
        $this->load->helper('tgl_indo');
    }

	public function index()
	{
		// Cek user acces menu
		$id_pegawai = $this->session->userdata('id_pegawai');
		if($id_pegawai == ''){
			redirect(base_url().'login');
			exit();
		}

		$data_pegawai = $this->pegawai_pusat->get_by_id($id_pegawai);
		$tanggal_awal  = date('Y-m-').'01';
		$tanggal_akhir = date('Y-m-d');
		if($data_pegawai){
			$daftar_toko               = $this->customer_pusat->listing_toko();
			$data['data_toko']         = $daftar_toko;

			$data_pegawai              = $this->pegawai_pusat->get_by_id($id_pegawai);		
			$data['data_pegawai']      = $data_pegawai;

			$data['data_induk_menu']   = $this->useraccess_pusat->get_induk_menu($id_pegawai);		
			$data['atribut_halaman']   = 'Dashboard Pusat';
			$data['tanggal_awal']      = $tanggal_awal;
			$data['tanggal_akhir']     = $tanggal_akhir;
			$data['halaman_dashboard'] = $this->load->view('admin/dashboard/dashboard_pusat/dashboard_view2', $data, true);
			$data['halaman_plugin']    = $this->load->view('admin/dashboard/dashboard_pusat/plugin2', $data, true);
			$data['isi_halaman']       = $this->load->view('admin/dashboard/dashboard_pusat/index', $data, true);
			$this->load->view('admin/layout',$data);
		}else{
			redirect(base_url().'login');
			exit();
		}
	}

	// Awal daftar penjualan perhari pusat
	public function ajax_list_penjualan_perhari_pusat()
	{
		// Cek user acces menu
		$id_pegawai               = $this->session->userdata('id_pegawai');
		$cek_useraccess_transaksi = $this->useraccess_pusat->cek_access($id_pegawai, '27');
		$cek_useraccess_laporan   = $this->useraccess_pusat->cek_access($id_pegawai, '39');
		if($cek_useraccess_laporan->act_read == '0' or $cek_useraccess_laporan->act_read == '-'){
			// redirect(base_url().'dashboard2');
		}

		$tanggal_awal  = $this->input->post('tanggal_awal');
		$tanggal_akhir = $this->input->post('tanggal_akhir');
		if($tanggal_awal == '' AND $tanggal_akhir == ''){
			// redirect(base_url().'dashboard2');
			exit();
		}
		
		$status_tampil          = $this->input->post('status_tampil');
		$jenis_perintah         = $this->input->post('jenis_perintah');
		$data_laporan_penjualan = $this->dashboard_detail->get_laporan(
			'_get_laporan_query_penjualan_perhari_pusat',
		   $tanggal_awal, $tanggal_akhir, $jenis_perintah
		);

		$data = array();
		$no   = $_POST['start'];

		foreach ($data_laporan_penjualan as $dlpj){
			$no++;
			$row 	= array();
			$row[] 	= $no;

			$row[] 	= date('d-m-Y', strtotime($dlpj->tanggal));
			// if($dlpj->tipe_customer !== 'MRC'){
			// }

			$row[] 	= '<span class="pull-right text-dark">'.number_format($dlpj->jml_transaksi,'0',',','.').'</span>';
			$row[] 	= '<span class="pull-right text-dark">'.number_format($dlpj->tunai,'0',',','.').'</span>';
			$row[] 	= '<span class="pull-right text-primary">'.number_format($dlpj->debit,'0',',','.').'</span>';
			$row[] 	= '<span class="pull-right text-dark">'.number_format($dlpj->total,'0',',','.').'</span>';
			$row[] 	= '<span class="pull-right text-danger">'.number_format($dlpj->total_retur,'0',',','.').'</span>';
			$row[] 	= '<span class="pull-right text-dark">'.number_format($dlpj->total_bersih,'0',',','.').'</span>';
			
			if($status_tampil == '1'){
				$row[] 	= '<span class="pull-right text-dark">'.number_format($dlpj->total_modal,'0',',','.').'</span>';
				$row[] 	= '<span class="pull-right text-dark">'.number_format($dlpj->total_keuntungan,'0',',','.').'</span>';
			}

			$data[]	= $row;
		}

		$output = array(	
			"draw"            => $_POST['draw'],
			"recordsTotal"    => $this->dashboard_detail->count_all('vamr4846_vama.penjualan_master', $tanggal_awal, $tanggal_akhir),
			"recordsFiltered" => $this->dashboard_detail->count_filtered(
									'_get_laporan_query_penjualan_perhari_pusat',
									$tanggal_awal, $tanggal_akhir, $jenis_perintah
								),
			"data"            => $data
		);
		echo json_encode($output);
	}
	// Akhir daftar penjualan perhari pusat

	// Awal daftar top customer pusat
	public function ajax_list_top_customer_pusat()
	{
		// Cek user acces menu
		$id_pegawai               = $this->session->userdata('id_pegawai');
		$cek_useraccess_transaksi = $this->useraccess_pusat->cek_access($id_pegawai, '27');
		$cek_useraccess_laporan   = $this->useraccess_pusat->cek_access($id_pegawai, '39');
		if($cek_useraccess_laporan->act_read == '0' or $cek_useraccess_laporan->act_read == '-'){
			redirect(base_url().'dashboard');
		}

		$tanggal_awal  = $this->input->post('tanggal_awal');
		$tanggal_akhir = $this->input->post('tanggal_akhir');
		if($tanggal_awal == '' AND $tanggal_akhir == ''){
			redirect(base_url().'dashboard2');
			exit();
		}
		
		$status_tampil          = $this->input->post('status_tampil');
		$jenis_perintah         = $this->input->post('jenis_perintah');
		$data_laporan_penjualan = $this->dashboard_detail->get_laporan(
			'_get_laporan_query_top_customer_pusat',
		   $tanggal_awal, $tanggal_akhir, $jenis_perintah
		);

		$data = array();
		$no   = $_POST['start'];

		foreach ($data_laporan_penjualan as $dlpj){
			$no++;
			$row   = array();
			$row[] = $no;
			
			$row[] = $dlpj->kode_customer;
			$row[] = $dlpj->nama_customer;
			$row[] = $dlpj->tanggal_awal.' sampai '.$dlpj->tanggal_akhir;
			
			$row[] = '<span class="pull-right text-dark">'.number_format($dlpj->jml_transaksi,'0',',','.').'</span>';
			$row[] = '<span class="pull-right text-dark">'.number_format($dlpj->total_jual,'0',',','.').'</span>';
			$row[] = '<span class="pull-right text-danger">'.number_format($dlpj->biaya_lain,'0',',','.').'</span>';
			$row[] = '<span class="pull-right text-danger">'.number_format($dlpj->ppn,'0',',','.').'</span>';
			$row[] = '<span class="pull-right text-primary">'.number_format($dlpj->total_bersih,'0',',','.').'</span>';
			$row[] = '<span class="pull-right text-danger">'.number_format($dlpj->total_retur,'0',',','.').'</span>';
			$row[] = '<span class="pull-right text-success">'.number_format($dlpj->total_fix,'0',',','.').'</span>';
			
			$data[]	= $row;
		}

		$output = array(	
			"draw"            => $_POST['draw'],
			"recordsTotal"    => $this->dashboard_detail->count_all('vamr4846_vama.penjualan_master', $tanggal_awal, $tanggal_akhir),
			"recordsFiltered" => $this->dashboard_detail->count_filtered(
									'_get_laporan_query_top_customer_pusat',
									$tanggal_awal, $tanggal_akhir, $jenis_perintah
								),
			"data"            => $data
		);
		echo json_encode($output);
	}	
	// Akhir daftar top customer pusat

	// Awal daftar top barang pusat
	public function ajax_list_top_barang_pusat()
	{
		// Cek user acces menu
		$id_pegawai               = $this->session->userdata('id_pegawai');
		$cek_useraccess_transaksi = $this->useraccess_pusat->cek_access($id_pegawai, '27');
		$cek_useraccess_laporan   = $this->useraccess_pusat->cek_access($id_pegawai, '39');
		if($cek_useraccess_laporan->act_read == '0' or $cek_useraccess_laporan->act_read == '-'){
			redirect(base_url().'dashboard');
		}

		$tanggal_awal  = $this->input->post('tanggal_awal');
		$tanggal_akhir = $this->input->post('tanggal_akhir');
		if($tanggal_awal == '' AND $tanggal_akhir == ''){
			redirect(base_url().'dashboard2');
			exit();
		}
		
		$status_tampil          = $this->input->post('status_tampil');
		$jenis_perintah         = $this->input->post('jenis_perintah');
		$data_laporan_penjualan = $this->dashboard_detail->get_laporan(
			'_get_laporan_query_top_barang_pusat',
		   $tanggal_awal, $tanggal_akhir, $jenis_perintah
		);

		$data = array();
		$no   = $_POST['start'];

		foreach ($data_laporan_penjualan as $dlpj){
			$no++;
			$row   = array();
			$row[] = $no;
			
			$row[] = $dlpj->sku;
			$row[] = $dlpj->nama_barang;
			$row[] = '<span class="pull-right text-dark">'.number_format($dlpj->harga_satuan,'0',',','.').'</span>';
			$row[] = $dlpj->tanggal_awal.' sampai '.$dlpj->tanggal_akhir;
			
			$row[] = '<span class="pull-right text-dark">'.number_format($dlpj->jml_transaksi,'0',',','.').'</span>';
			$row[] = '<span class="pull-right text-dark">'.number_format($dlpj->jml_qty,'0',',','.').'</span>';
			$row[] = '<span class="pull-right text-dark">'.number_format($dlpj->total_jual,'0',',','.').'</span>';
			$row[] = '<span class="pull-right text-danger">'.number_format($dlpj->total_disc,'0',',','.').'</span>';
			$row[] = '<span class="pull-right text-primary">'.number_format($dlpj->total_bersih,'0',',','.').'</span>';
			
			$data[]	= $row;
		}

		$output = array(	
			"draw"            => $_POST['draw'],
			"recordsTotal"    => $this->dashboard_detail->count_all('vamr4846_vama.penjualan_master', $tanggal_awal, $tanggal_akhir),
			"recordsFiltered" => $this->dashboard_detail->count_filtered(
									'_get_laporan_query_top_barang_pusat',
									$tanggal_awal, $tanggal_akhir, $jenis_perintah
								),
			"data"            => $data
		);
		echo json_encode($output);
	}	
	// Akhir daftar top barang pusat

	// Awal daftar top barang per customer
	public function ajax_list_top_barang_percustomer()
	{
		// Cek user acces menu
		$id_pegawai               = $this->session->userdata('id_pegawai');
		$cek_useraccess_transaksi = $this->useraccess_pusat->cek_access($id_pegawai, '27');
		$cek_useraccess_laporan   = $this->useraccess_pusat->cek_access($id_pegawai, '39');
		if($cek_useraccess_laporan->act_read == '0' or $cek_useraccess_laporan->act_read == '-'){
			redirect(base_url().'dashboard');
		}

		$tanggal_awal  = $this->input->post('tanggal_awal');
		$tanggal_akhir = $this->input->post('tanggal_akhir');
		if($tanggal_awal == '' AND $tanggal_akhir == ''){
			redirect(base_url().'dashboard2');
			exit();
		}
		
		$status_tampil          = $this->input->post('status_tampil');
		$jenis_perintah         = $this->input->post('jenis_perintah');
		$data_laporan_penjualan = $this->dashboard_detail->get_laporan(
			'_get_laporan_query_top_barang_percustomer',
		   $tanggal_awal, $tanggal_akhir, $jenis_perintah
		);

		$data = array();
		$no   = $_POST['start'];

		foreach ($data_laporan_penjualan as $dlpj){
			$no++;
			$row   = array();
			$row[] = $no;
			
			$row[] = $dlpj->nama_customer_pusat;
			$row[] = $dlpj->sku;
			$row[] = $dlpj->nama_barang;
			$row[] = '<span class="pull-right text-dark">'.number_format($dlpj->harga_satuan,'0',',','.').'</span>';
			$row[] = $dlpj->tanggal_awal.' sampai '.$dlpj->tanggal_akhir;
			
			$row[] = '<span class="pull-right text-dark">'.number_format($dlpj->jml_transaksi,'0',',','.').'</span>';
			$row[] = '<span class="pull-right text-dark">'.number_format($dlpj->jml_qty,'0',',','.').'</span>';
			$row[] = '<span class="pull-right text-dark">'.number_format($dlpj->total_jual,'0',',','.').'</span>';
			$row[] = '<span class="pull-right text-danger">'.number_format($dlpj->total_disc,'0',',','.').'</span>';
			$row[] = '<span class="pull-right text-primary">'.number_format($dlpj->total_bersih,'0',',','.').'</span>';
			
			$data[]	= $row;
		}

		$output = array(	
			"draw"            => $_POST['draw'],
			"recordsTotal"    => $this->dashboard_detail->count_all('vamr4846_vama.penjualan_master', $tanggal_awal, $tanggal_akhir),
			"recordsFiltered" => $this->dashboard_detail->count_filtered(
									'_get_laporan_query_top_barang_percustomer',
									$tanggal_awal, $tanggal_akhir, $jenis_perintah
								),
			"data"            => $data
		);
		echo json_encode($output);
	}	
	// Akhir daftar top barang per customer

	// Awal daftar penjualan perhari toko
	public function ajax_list_penjualan_perhari_mrc()
	{
		// Cek user acces menu
		$id_pegawai               = $this->session->userdata('id_pegawai');
		$cek_useraccess_transaksi = $this->useraccess_pusat->cek_access($id_pegawai, '27');
		$cek_useraccess_laporan   = $this->useraccess_pusat->cek_access($id_pegawai, '39');
		if($cek_useraccess_laporan->act_read == '0' or $cek_useraccess_laporan->act_read == '-'){
			// redirect(base_url().'dashboard2');
		}

		$tanggal_awal  = $this->input->post('tanggal_awal');
		$tanggal_akhir = $this->input->post('tanggal_akhir');
		if($tanggal_awal == '' AND $tanggal_akhir == ''){
			// redirect(base_url().'dashboard2');
			exit();
		}
		
		$kode_toko = $this->input->post('kode_toko');
		if($kode_toko == ''){
			// redirect(base_url().'dashboard2');
			exit();
		}

		$status_tampil          = $this->input->post('status_tampil');
		$data_laporan_penjualan = $this->dashboard_detail->get_laporan_mrc(
			'_get_laporan_query_penjualan_perhari_toko',
		   $tanggal_awal, $tanggal_akhir, $kode_toko
		);

		$data = array();
		$no   = $_POST['start'];

		foreach ($data_laporan_penjualan as $dlpj){
			$no++;
			$row   = array();
			$row[] = $no;
			$row[] = date('d-m-Y', strtotime($dlpj->tanggal));
			
			$row[] = '<span class="pull-right text-dark">'.number_format($dlpj->jml_transaksi,'0',',','.').'</span>';
			$row[] = '<span class="pull-right text-dark">'.number_format($dlpj->tunai,'0',',','.').'</span>';
			$row[] = '<span class="pull-right text-primary">'.number_format($dlpj->debit,'0',',','.').'</span>';
			$row[] = '<span class="pull-right text-dark">'.number_format($dlpj->total,'0',',','.').'</span>';
			$row[] = '<span class="pull-right text-danger">'.number_format($dlpj->total_retur,'0',',','.').'</span>';
			$row[] = '<span class="pull-right text-dark">'.number_format($dlpj->total_bersih,'0',',','.').'</span>';
			
			if($status_tampil == '1'){
				$row[] 	= '<span class="pull-right text-dark">'.number_format($dlpj->total_modal,'0',',','.').'</span>';
				$row[] 	= '<span class="pull-right text-dark">'.number_format($dlpj->total_keuntungan,'0',',','.').'</span>';
			}

			$data[]	= $row;
		}

		$output = array(	
			"draw"            => $_POST['draw'],
			"recordsTotal"    => $this->dashboard_detail->count_all_mrc('vamr4846_toko_mrc.penjualan_master_'.$kode_toko.'', $tanggal_awal, $tanggal_akhir, $kode_toko),
			"recordsFiltered" => $this->dashboard_detail->count_filtered_mrc(
									'_get_laporan_query_penjualan_perhari_toko',
									$tanggal_awal, $tanggal_akhir, $kode_toko
								),
			"data"            => $data
		);
		echo json_encode($output);
	}
	// Akhir daftar penjualan perhari pusat

	// Awal daftar top customer toko
	public function ajax_list_top_customer_mrc()
	{
		// Cek user acces menu
		$id_pegawai               = $this->session->userdata('id_pegawai');
		$cek_useraccess_transaksi = $this->useraccess_pusat->cek_access($id_pegawai, '27');
		$cek_useraccess_laporan   = $this->useraccess_pusat->cek_access($id_pegawai, '39');
		if($cek_useraccess_laporan->act_read == '0' or $cek_useraccess_laporan->act_read == '-'){
			redirect(base_url().'dashboard');
		}

		$tanggal_awal  = $this->input->post('tanggal_awal');
		$tanggal_akhir = $this->input->post('tanggal_akhir');
		if($tanggal_awal == '' AND $tanggal_akhir == ''){
			redirect(base_url().'dashboard2');
			exit();
		}
		
		$kode_toko = $this->input->post('kode_toko');
		if($kode_toko == ''){
			// redirect(base_url().'dashboard2');
			exit();
		}

		$status_tampil          = $this->input->post('status_tampil');
		$data_laporan_penjualan = $this->dashboard_detail->get_laporan_mrc(
			'_get_laporan_query_top_customer_toko',
		   $tanggal_awal, $tanggal_akhir, $kode_toko
		);

		$data = array();
		$no   = $_POST['start'];

		foreach ($data_laporan_penjualan as $dlpj){
			$no++;
			$row   = array();
			$row[] = $no;
			
			$row[] = $dlpj->kode_customer;
			$row[] = $dlpj->nama_customer;
			$row[] = $dlpj->tanggal_awal.' sampai '.$dlpj->tanggal_akhir;
			
			$row[] = '<span class="pull-right text-dark">'.number_format($dlpj->jml_transaksi,'0',',','.').'</span>';
			$row[] = '<span class="pull-right text-dark">'.number_format($dlpj->total_jual,'0',',','.').'</span>';
			$row[] = '<span class="pull-right text-danger">'.number_format($dlpj->biaya_lain,'0',',','.').'</span>';
			$row[] = '<span class="pull-right text-danger">'.number_format($dlpj->ppn,'0',',','.').'</span>';
			$row[] = '<span class="pull-right text-primary">'.number_format($dlpj->total_bersih,'0',',','.').'</span>';
			$row[] = '<span class="pull-right text-danger">'.number_format($dlpj->total_retur,'0',',','.').'</span>';
			$row[] = '<span class="pull-right text-success">'.number_format($dlpj->total_fix,'0',',','.').'</span>';
			
			$data[]	= $row;
		}

		$output = array(	
			"draw"            => $_POST['draw'],
			"recordsTotal"    => $this->dashboard_detail->count_all_mrc(
				'vamr4846_toko_mrc.penjualan_master_'.$kode_toko.'', $tanggal_awal, $tanggal_akhir, $kode_toko
			),
			"recordsFiltered" => $this->dashboard_detail->count_filtered_mrc(
									'_get_laporan_query_top_customer_toko',
									$tanggal_awal, $tanggal_akhir, $kode_toko
								),
			"data"            => $data
		);
		echo json_encode($output);
	}	
	// Akhir daftar top customer toko

	// Awal daftar top barang toko
	public function ajax_list_top_barang_mrc()
	{
		// Cek user acces menu
		$id_pegawai               = $this->session->userdata('id_pegawai');
		$cek_useraccess_transaksi = $this->useraccess_pusat->cek_access($id_pegawai, '27');
		$cek_useraccess_laporan   = $this->useraccess_pusat->cek_access($id_pegawai, '39');
		if($cek_useraccess_laporan->act_read == '0' or $cek_useraccess_laporan->act_read == '-'){
			redirect(base_url().'dashboard');
		}

		$tanggal_awal  = $this->input->post('tanggal_awal');
		$tanggal_akhir = $this->input->post('tanggal_akhir');
		if($tanggal_awal == '' AND $tanggal_akhir == ''){
			redirect(base_url().'dashboard2');
			exit();
		}
		
		$kode_toko = $this->input->post('kode_toko');
		if($kode_toko == ''){
			// redirect(base_url().'dashboard2');
			exit();
		}

		$status_tampil          = $this->input->post('status_tampil');
		$data_laporan_penjualan = $this->dashboard_detail->get_laporan_mrc(
			'_get_laporan_query_top_barang_toko',
		   $tanggal_awal, $tanggal_akhir, $kode_toko
		);

		$data = array();
		$no   = $_POST['start'];

		foreach ($data_laporan_penjualan as $dlpj){
			$no++;
			$row   = array();
			$row[] = $no;
			
			$row[] = $dlpj->sku;
			$row[] = $dlpj->nama_barang;
			$row[] = '<span class="pull-right text-dark">'.number_format($dlpj->harga_satuan,'0',',','.').'</span>';
			$row[] = $dlpj->tanggal_awal.' sampai '.$dlpj->tanggal_akhir;
			
			$row[] = '<span class="pull-right text-dark">'.number_format($dlpj->jml_transaksi,'0',',','.').'</span>';
			$row[] = '<span class="pull-right text-dark">'.number_format($dlpj->jml_qty,'0',',','.').'</span>';
			$row[] = '<span class="pull-right text-dark">'.number_format($dlpj->total_jual,'0',',','.').'</span>';
			$row[] = '<span class="pull-right text-danger">'.number_format($dlpj->total_disc,'0',',','.').'</span>';
			$row[] = '<span class="pull-right text-primary">'.number_format($dlpj->total_bersih,'0',',','.').'</span>';
			
			$data[]	= $row;
		}

		$output = array(	
			"draw"            => $_POST['draw'],
			"recordsTotal"    => $this->dashboard_detail->count_all_mrc(
				'vamr4846_toko_mrc.penjualan_master_'.$kode_toko.'', $tanggal_awal, $tanggal_akhir, $kode_toko
			),
			"recordsFiltered" => $this->dashboard_detail->count_filtered_mrc(
				'_get_laporan_query_top_barang_toko',
				$tanggal_awal, $tanggal_akhir, $kode_toko
			),
			"data"            => $data
		);
		echo json_encode($output);
	}	
	// Akhir daftar top barang toko

	public function ambil_total_penjualan()
	{
		// Cek user acces menu
		$id_pegawai     = $this->session->userdata('id_pegawai');
		$cek_useraccess = $this->useraccess_pusat->cek_access($id_pegawai, '39');
		if($cek_useraccess->act_read == '0' or $cek_useraccess->act_read == '-'){
			redirect(base_url().'dashboard');
		}

		$tanggal_awal  = $this->input->post('tanggal_awal');
		$tanggal_akhir = $this->input->post('tanggal_akhir');
		if($tanggal_awal == '' AND $tanggal_akhir == ''){
			redirect(base_url().'laporan_penjualan_pusat');
			exit();
		}

		$status_tampil  = $this->input->post('status_tampil');
		$jenis_perintah = $this->input->post('jenis_perintah');

		// Ambil jumlah transaksi, total tunai, debit dan total uang masuk
		$data_uang_masuk = $this->penjualan_master->get_uang($tanggal_awal, $tanggal_akhir,'SELESAI', $jenis_perintah);
		if ($data_uang_masuk){
			$total_bersih     = $data_uang_masuk->total_bersih;
		}else{
			$total_bersih     = '0';
		}

		// Ambil data modal dan keuntungan
		$data_modal = $this->penjualan_master->get_modal($tanggal_awal, $tanggal_akhir, $jenis_perintah);
		if ($data_modal){
			$total_modal      = $data_modal->total_modal;
			$total_keuntungan = $data_modal->total_keuntungan;
		}else{
			$total_modal      = '0';
			$total_keuntungan = '0';
		}

		// Ambil data asset
		$data_asset = $this->dashboard_detail->get_asset_pusat();
		if ($data_asset){
			$total_asset = $data_asset->total_asset;
		}else{
			$total_asset = '0';
		}

		if($this->session->userdata('usergroup_name') == 'Super Admin'){
			if($status_tampil == '1'){
				echo json_encode(array(	
					"status"           => 1,
					"total_bersih"     => $total_bersih,
					"total_modal"      => $total_modal,
					"total_keuntungan" => $total_keuntungan,
					"total_asset"      => $total_asset
				));
	        }else{
	        	echo json_encode(array(	
					"status"           => 1,
					"total_bersih"     => $total_bersih
				));
	        }
		}else if($this->session->userdata('usergroup_name') == 'Admin'){
			echo json_encode(array(	
				"status"           => 1,
				"total_bersih"     => $total_bersih
			));
		}
	}

	public function ambil_total_penjualan_mrc()
	{
		// Cek user acces menu
		$id_pegawai     = $this->session->userdata('id_pegawai');
		$cek_useraccess = $this->useraccess_pusat->cek_access($id_pegawai, '39');
		if($cek_useraccess->act_read == '0' or $cek_useraccess->act_read == '-'){
			redirect(base_url().'dashboard');
		}
		
		$kode_toko     = $this->input->post('kode_toko');
		$status_tampil = $this->input->post('status_tampil');
		$tanggal_awal  = $this->input->post('tanggal_awal');
		$tanggal_akhir = $this->input->post('tanggal_akhir');
		if($tanggal_awal == '' AND $tanggal_akhir == ''){
			redirect(base_url().'laporan_penjualan_pusat');
			exit();
		}

		// Awal hitung jumlah toko untuk looping hitung penjualan toko
		$total_bersih     = '0';
		$total_modal      = '0';
		$total_keuntungan = '0';
		$total_asset      = '0';

        $daftar_toko  = $this->customer_pusat->listing_toko();
        if(count($daftar_toko) > 0){
        	foreach ($daftar_toko as $daftar_toko){ 
				$kode_per_toko = $daftar_toko->kode_customer_pusat;

				// Ambil total bersih
				$data_uang_masuk = $this->dashboard_detail->get_uang_mrc($tanggal_awal, $tanggal_akhir,'SELESAI', $kode_per_toko);
				if ($data_uang_masuk){
					$total_bersih = $total_bersih + $data_uang_masuk->total_bersih;
				}else{
					$total_bersih = $total_bersih + 0;
				}

				// Ambil data modal dan keuntungan
				$data_modal = $this->dashboard_detail->get_modal_mrc($tanggal_awal, $tanggal_akhir, $kode_per_toko);
				if ($data_modal){
					$total_modal      = $total_modal + $data_modal->total_modal;
					$total_keuntungan = $total_keuntungan + $data_modal->total_keuntungan;
				}else{
					$total_modal      = $total_modal + 0;
					$total_keuntungan = $total_keuntungan + 0;
				}

				// Ambil data asset
				$data_asset = $this->dashboard_detail->get_asset_mrc($kode_per_toko);
				if ($data_asset){
					$total_asset = $total_asset + $data_asset->total_asset;
				}else{
					$total_asset = '0';
				}
			}
        }
        // Akhir hitung jumlah toko untuk looping hitung penjualan toko

        if($kode_toko !==''){
	        // Ambil total bersih per toko
			$data_uang_masuk = $this->dashboard_detail->get_uang_mrc($tanggal_awal, $tanggal_akhir,'SELESAI', $kode_toko);
			if ($data_uang_masuk){
				$total_bersih_toko = $data_uang_masuk->total_bersih;
			}else{
				$total_bersih_toko = 0;
			}

			// Ambil data modal dan keuntungan per toko
			$data_modal = $this->dashboard_detail->get_modal_mrc($tanggal_awal, $tanggal_akhir, $kode_toko);
			if ($data_modal){
				$total_modal_toko      = $data_modal->total_modal;
				$total_keuntungan_toko = $data_modal->total_keuntungan;
			}else{
				$total_modal_toko      = 0;
				$total_keuntungan_toko = 0;
			}

			// Ambil data asset
			$data_asset = $this->dashboard_detail->get_asset_mrc($kode_toko);
			if ($data_asset){
				$total_asset_toko = $data_asset->total_asset;
			}else{
				$total_asset_toko = '0';
			}        	
        }else{
			$total_bersih_toko     = '0';
			$total_modal_toko      = '0';
			$total_keuntungan_toko = '0';
			$total_asset_toko      = '0';
        }

        if($this->session->userdata('usergroup_name') == 'Super Admin'){
			if($status_tampil == '1'){
				echo json_encode(array(	
					"status"                => 1,
					"total_bersih"          => $total_bersih,
					"total_modal"           => $total_modal,
					"total_keuntungan"      => $total_keuntungan,
					"total_asset"           => $total_asset,
					
					"total_bersih_toko"     => $total_bersih_toko,
					"total_modal_toko"      => $total_modal_toko,
					"total_keuntungan_toko" => $total_keuntungan_toko,
					"total_asset_toko"      => $total_asset_toko
				));
	        }else{
	        	echo json_encode(array(	
					"status"            => 1,
					"total_bersih"      => $total_bersih,
					"total_bersih_toko" => $total_bersih_toko
				));
	        }
		}else if($this->session->userdata('usergroup_name') == 'Admin'){
			echo json_encode(array(	
				"status"            => 1,
				"total_bersih"      => $total_bersih,
				"total_bersih_toko" => $total_bersih_toko
			));
		}
	}

	public function ambil_total_penjualan_online()
	{
		// Cek user acces menu
		$id_pegawai     = $this->session->userdata('id_pegawai');
		$cek_useraccess = $this->useraccess_pusat->cek_access($id_pegawai, '39');
		if($cek_useraccess->act_read == '0' or $cek_useraccess->act_read == '-'){
			redirect(base_url().'dashboard');
		}
		
		$status_tampil = $this->input->post('status_tampil');
		$tanggal_awal  = $this->input->post('tanggal_awal');
		$tanggal_akhir = $this->input->post('tanggal_akhir');
		if($tanggal_awal == '' AND $tanggal_akhir == ''){
			exit();
		}

		// Awal hitung jumlah toko untuk looping hitung penjualan toko
		$total_bersih     = '0';
		$total_modal      = '0';
		$total_keuntungan = '0';

        $daftar_toko  = $this->customer_pusat->listing_toko();
        if(count($daftar_toko) > 0){
        	foreach ($daftar_toko as $daftar_toko){ 
				$kode_per_toko = $daftar_toko->kode_customer_pusat;

				// Ambil total bersih
				$data_uang_masuk = $this->dashboard_detail->get_uang_online($tanggal_awal, $tanggal_akhir,'SELESAI', $kode_per_toko);
				if ($data_uang_masuk){
					$total_bersih = $total_bersih + $data_uang_masuk->total_bersih;
				}else{
					$total_bersih = $total_bersih + 0;
				}

				// Ambil data modal dan keuntungan
				$data_modal = $this->dashboard_detail->get_modal_online($tanggal_awal, $tanggal_akhir, $kode_per_toko);
				if ($data_modal){
					$total_modal      = $total_modal + $data_modal->total_modal;
					$total_keuntungan = $total_keuntungan + $data_modal->total_keuntungan;
				}else{
					$total_modal      = $total_modal + 0;
					$total_keuntungan = $total_keuntungan + 0;
				}
			}
        }
        // Akhir hitung jumlah toko untuk looping hitung penjualan toko

        if($this->session->userdata('usergroup_name') == 'Super Admin'){
			if($status_tampil == '1'){
				echo json_encode(array(	
					"status"                => 1,
					"total_bersih"          => $total_bersih,
					"total_modal"           => $total_modal,
					"total_keuntungan"      => $total_keuntungan,					
				));
	        }else{
	        	echo json_encode(array(	
					"status"            => 1,
					"total_bersih"      => $total_bersih,
				));
	        }
		}else if($this->session->userdata('usergroup_name') == 'Admin'){
			echo json_encode(array(	
				"status"            => 1,
				"total_bersih"      => $total_bersih,
			));
		}
	}
}
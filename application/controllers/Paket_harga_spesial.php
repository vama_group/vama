<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Paket_harga_spesial extends MY_Controller 
{
	function __construct()
	{
		parent::__construct();
		$this->CI =& get_instance();
		$this->load->model('pegawai_pusat_model','pegawai_pusat');
    $this->load->model('useraccess_pusat_model','useraccess_pusat');
		$this->load->model('customer_pusat_model','customer_pusat');
		$this->load->model('supplier_model','supplier');
		$this->load->model('barang_toko_model','barang_toko');
		$this->load->model('barang_pusat_model','barang_pusat');
		$this->load->model('kategori_paket_model','kategori_paket');
		$this->load->model('paket_promo_master_model','paket_promo_master');
		$this->load->model('paket_promo_detail_model','paket_promo_detail');
	}

	public function index()
	{
		$id_pegawai = $this->session->userdata('id_pegawai');
		if($id_pegawai == ''){
			redirect(base_url().'login');
		}

		// Cek user acces menu
		$data_pegawai = $this->pegawai_pusat->get_by_id($id_pegawai);
		if($data_pegawai){
			$cek_useraccess = $this->useraccess_pusat->cek_access($id_pegawai, '30');
			if($cek_useraccess){
				if($cek_useraccess->act_read == '0' or $cek_useraccess->act_read == '-'){
					redirect(base_url().'dashboard');
				}					
			}else{
				redirect(base_url().'dashboard');
			}
			
			// Ambil daftar toko
			$daftar_toko               = $this->customer_pusat->listing_toko();
			$data['data_toko']         = $daftar_toko;
			$data['data_pegawai']      = $data_pegawai;
			$data['data_induk_menu']   = $this->useraccess_pusat->get_induk_menu($id_pegawai);
			$data['access_create']     = $cek_useraccess->act_create;
			$data['access_update']     = $cek_useraccess->act_update;
			
			$data['atribut_halaman']   = 'Paket Promo';
			$data['id_paket_promo_m']  = '0';
			$data['halaman_transaksi'] = $this->load->view('admin/master_data/paket_harga_spesial/transaksi',$data,true);
			$data['halaman_plugin']    = $this->load->view('admin/master_data/paket_harga_spesial/plugin',$data,true);
			$data['isi_halaman']       = $this->load->view('admin/master_data/paket_harga_spesial/index',$data,true);
			$this->load->view('admin/layout',$data);
		}else{
			redirect(base_url().'login');
		}	
	}

	public function list_kategori_paket(){
		$nk                  = $this->input->get('nk');
		$data_kategori_paket = $this->kategori_paket->listing($nk);
		if($data_kategori_paket){
			echo json_encode($data_kategori_paket);
		}else{
			echo json_encode('');				
		}
	}

	public function form_kategori_paket()
	{
		$id_pegawai   = $this->session->userdata('id_pegawai');
		$data_pegawai = $this->pegawai_pusat->get_by_id($id_pegawai);
		if($data_pegawai){
			$id_toko   = $data_pegawai->id_toko;
			$kode_toko = $data_pegawai->kode_toko;
		}else{
			echo json_encode(array(	
				"status" => 0,
				"pesan"  =>	"Data pegawai tidak terdaftar"
			));
		}

		$cek_useraccess = $this->useraccess_pusat->cek_access($id_pegawai, '30');
		if($cek_useraccess){
			if($cek_useraccess->act_create == '0' or $cek_useraccess->act_create == '-'){
				echo json_encode(array(	
					"status" => 0,
					"pesan"  =>	"Anda tidak di izinkan untuk menambahkan data kategori paket"
				));
				exit();
			}
		}else{
			echo json_encode(array(	
				"status" => 0,
				"pesan"  =>	"Anda tidak di izinkan untuk menambahkan data kategori paket"
			));
			exit();
		}

		echo json_encode(array("status" => 1));
	}

	public function inputan_kategori_paket(){
		$id_pegawai     = $this->session->userdata('id_pegawai');
		$cek_useraccess = $this->useraccess_pusat->cek_access($id_pegawai, '30');
		if($cek_useraccess->act_create == '0' or $cek_useraccess->act_create == '-'){
			echo json_encode(array(	
				"status" => 0,
				"pesan"  =>	"Anda tidak di izinkan untuk menambahkan data kategori paket"
			));
			redirect(base_url().'paket_harga_spesial');
			exit();
		}

		$this->load->view('admin/master_data/paket_harga_spesial/form_input_kategori');
	}

	public function transaksi()
	{		
		$id_pegawai   = $this->session->userdata('id_pegawai');
		$data_pegawai = $this->pegawai_pusat->get_by_id($id_pegawai);
		if($data_pegawai){
			$id_paket_promo_m  = $this->input->get('id');
			$data_paket_promo  = $this->paket_promo_master->get_master($id_paket_promo_m);
			if($data_paket_promo->id_paket_promo_m == ''){
				redirect(base_url().'paket_harga_spesial');
				exit();
			}
	
			$cek_useraccess        = $this->useraccess_pusat->cek_access($id_pegawai, '30');
			$data['access_create'] = $cek_useraccess->act_create;
			$data['access_update'] = $cek_useraccess->act_update;

			if($data_paket_promo->status_paket == 'DI SIAPKAN'){
				redirect(base_url().'paket_harga_spesial');
				exit();
			}

			if($cek_useraccess->act_update == 0 AND $data_paket_promo->status_paket == 'DI TERIMA'){
				redirect(base_url().'paket_harga_spesial');
				exit();
			}
			

			$this->CI->session->set_userdata('usergroup_name', $data_pegawai->usergroup_name);
			// Ambil daftar toko
			$daftar_toko               = $this->customer_pusat->listing_toko();
			$data['data_toko']         = $daftar_toko;
			
			$data['data_pegawai']      = $data_pegawai;
			$data['data_induk_menu']   = $this->useraccess_pusat->get_induk_menu($id_pegawai);
			
			$data['atribut_halaman']   = 'Paket & Harga Spesial';
			$data['id_paket_promo_m']  = $id_paket_promo_m;
			$data['no_paket_promo']    = $data_paket_promo->kode_paket_promo;
			$data['halaman_transaksi'] = $this->load->view('admin/master_data/paket_harga_spesial/transaksi',$data,true);
			$data['halaman_plugin']    = $this->load->view('admin/master_data/paket_harga_spesial/plugin',$data,true);
			$data['isi_halaman']       = $this->load->view('admin/master_data/paket_harga_spesial/index',$data,true);
			$this->load->view('admin/layout',$data);
			
		}else{
			redirect(base_url().'paket_harga_spesial');
			exit();
		}
	}

	public function simpan_transaksi()
	{
		$id_pegawai   = $this->session->userdata('id_pegawai');
		$data_pegawai = $this->pegawai_pusat->get_by_id($id_pegawai);
		if($data_pegawai){
		}else{
			redirect(base_url().'paket_harga_spesial');
			exit();
		}

		$url               = $this->input->post('url');
		$cek_useraccess    = $this->useraccess_pusat->cek_access($id_pegawai, '30');
		$id_paket_promo_m  = $this->input->post('id_paket_promo_m');
		$kode_paket_promo  = $this->input->post('kode_paket_promo');
		$nama_paket_promo  = $this->input->post('nama_paket_promo');
		$tipe_paket        = $this->input->post('tipe_paket');
		$id_kategori_paket = $this->input->post('id_kategori_paket');
		$id_jenis_paket    = $this->input->post('id_jenis_paket');
		$tanggal_awal      = $this->input->post('tanggal_awal');
		$tanggal_akhir     = $this->input->post('tanggal_akhir');
		$catatan           = $this->clean_tag_input($this->input->post('catatan'));

		// Cek user acces menu
		if($cek_useraccess->act_create == '1' or $cek_useraccess->act_update == '1'){
			// Validasi apakah sudah ada data barang yang sudah disimpan
			$validasi_master = $this->paket_promo_master->get_by_id($id_paket_promo_m);
			if($validasi_master){
				$kode_toko = $validasi_master->kode_toko;
				$id_toko   = $validasi_master->id_toko;

				// Validasi status paket tidak boleh di terima
				if($validasi_master->status_paket == 'DI TERIMA'){
					echo json_encode(array(	
						'status' => 0,
						'pesan'  => 'Maaf paket ini sudah di terima oleh pusat dan sudah tidak bisa di ubah kembali.',
						'url'    => 'paket_harga_spesial/transaksi/?&id='.$id_paket_promo_m
					));
					exit();
				}else{
					$master = $this->paket_promo_master->update_master(
						$id_paket_promo_m, $nama_paket_promo, $tipe_paket, $id_kategori_paket, $id_jenis_paket, $tanggal_awal, $tanggal_akhir, 'DI TERIMA',
						$catatan, $id_pegawai, $kode_toko
					);

					if($master){
						echo json_encode(array(
							'status' => 1, 
							'pesan'  => "Transaksi berhasil ditahan,
										dengan kode paket promo : ".$kode_paket_promo."
										nama paket promo : ".$nama_paket_promo.""
						));					
					}else{
						echo json_encode(array(	'status' 	=> 0,
												'pesan'		=> 'Terjadi kesalah saat menyimpan, harap coba kembali.',
												'url' 		=> 'paket_harga_spesial/transaksi/?&id='.$id_pembelian_m
										));
					}
				}
			}else{
				echo json_encode(array(	
					'status' => 0,
					'pesan'  => 'Belum ada barang yang anda simpan di paket promo ini.',
					'url'    => 'paket_harga_spesial/transaksi/?&id='.$id_paket_promo_m
				));
			}
		}else{
			echo json_encode(array(	'status' 	=> 0,
									'pesan'		=> 'Maaf anda tidak diizinkan untuk menyimpan transaksi paket promo.',
									'url' 		=> 'paket_harga_spesial/transaksi/?&id='.$id_pembelian_m
							));
		}
	}
	
	public function tahan_transaksi()
	{
		$id_pegawai   = $this->session->userdata('id_pegawai');
		$data_pegawai = $this->pegawai_pusat->get_by_id($id_pegawai);
		if($data_pegawai){
		}else{
			redirect(base_url().'paket_harga_spesial');
			exit();
		}

		$url               = $this->input->post('url');
		$cek_useraccess    = $this->useraccess_pusat->cek_access($id_pegawai, '30');
		$id_paket_promo_m  = $this->input->post('id_paket_promo_m');
		$kode_paket_promo  = $this->input->post('kode_paket_promo');
		$nama_paket_promo  = $this->input->post('nama_paket_promo');
		$tipe_paket        = $this->input->post('tipe_paket');
		$id_kategori_paket = $this->input->post('id_kategori_paket');
		$id_jenis_paket    = $this->input->post('id_jenis_paket');
		$tanggal_awal      = $this->input->post('tanggal_awal');
		$tanggal_akhir     = $this->input->post('tanggal_akhir');
		$catatan           = $this->clean_tag_input($this->input->post('catatan'));

		// Cek user acces menu
		if($cek_useraccess->act_create == '1' or $cek_useraccess->act_update == '1'){
			// Validasi apakah sudah ada data barang yang sudah disimpan
			$validasi_master = $this->paket_promo_master->get_by_id($id_paket_promo_m);
			if($validasi_master){
				$kode_toko = $validasi_master->kode_toko;
				$id_toko   = $validasi_master->id_toko;
				
				// Validasi status paket tidak boleh di terima
				if($validasi_master->status_paket == 'DI TERIMA'){
					echo json_encode(array(	
						'status' => 0,
						'pesan'  => 'Maaf paket ini sudah di terima oleh pusat dan sudah tidak bisa di ubah kembali.',
						'url'    => 'paket_harga_spesial/transaksi/?&id='.$id_paket_promo_m
					));
					exit();
				}else{
					$master = $this->paket_promo_master->update_master(
						$id_paket_promo_m, $nama_paket_promo, $tipe_paket, $id_kategori_paket, $id_jenis_paket, $tanggal_awal, $tanggal_akhir, 'MENUNGGU',
						$catatan, $id_pegawai, $kode_toko
					);

					if($master){
						echo json_encode(array('status'	 	=> 1, 
											   'pesan' 		=> "	Transaksi berhasil ditahan,
											   						dengan kode paket promo : ".$kode_paket_promo."
											   						nama paket promo : ".$nama_paket_promo."
											   				   "
										));
					}else{
						echo json_encode(array(	'status' 	=> 0,
												'pesan'		=> 'Terjadi kesalah saat menahan transaksi, harap coba kembali.',
												'url' 		=> 'paket_harga_spesial/transaksi/?&id='.$id_paket_promo_m
										));
					}
				}
			}else{
				echo json_encode(array(	
					'status' => 0,
					'pesan'  => 'Belum ada barang yang anda simpan di paket promo ini.',
					'url'    => 'paket_harga_spesial/transaksi/?&id='.$id_paket_promo_m
				));
			}
		}else{
			echo json_encode(array(	'status' 	=> 0,
									'pesan'		=> 'Maaf anda tidak diizinkan untuk menahan transaksi retur pembelian.',
									'url' 		=> 'paket_harga_spesial/transaksi/?&id='.$id_paket_promo_m
							));
		}
	}

	private function kode_paket_promo_baru($id_pegawai, $kode_toko, $tipe_paket){
		$id_pegawai   = $this->session->userdata('id_pegawai');
		$data_pegawai = $this->pegawai_pusat->get_by_id($id_pegawai);
		if($data_pegawai){
		}else{
			redirect(base_url().'paket_harga_spesial');
			exit();
		}

		// Ambil id toko
		$data_toko = $this->customer_pusat->get_by_kode($kode_toko);
		if($data_toko){
			$id_toko = $data_toko->id_customer_pusat;
		}else{
			redirect(base_url().'paket_harga_spesial');
			exit();
		}

		$tanggal_sekarang = date('Y-m-d');
		$bulan_sekarang   = date('m');
		$tahun_sekarang   = date('Y');
		$tahun_sekarang_2 = date('y'); 

		if($tipe_paket == 'PAKET KHUSUS'){
			$inisial = "PKK";
		}elseif($tipe_paket == 'HARGA PROMO'){
			$inisial = "HRP";
		}elseif($tipe_paket == 'PAKET REGULER'){
			$inisial = "PKR";
		}

		$akhir = $this->paket_promo_master->kode_paket_promo_baru($tahun_sekarang, $bulan_sekarang, $id_pegawai, $kode_toko, $tipe_paket);
		if($akhir){
			$no_baru = $akhir->no_baru;
		}else{
			$no_baru = 1;	
		}

		if($no_baru < 10){
			$depan = '000';
		}elseif($no_baru< 100) {
			$depan      		= '00';
		}elseif($no_baru< 1000) {
			$depan = '0';
		}else{
			$depan = '';
		}

		$jumlah                = $no_baru;
		$kode_paket_promo_baru = $inisial.'-'.$id_toko.'-'.$depan.$jumlah;
		return $kode_paket_promo_baru;
	}

	public function edit_transaksi()
	{
		$id_pegawai   = $this->session->userdata('id_pegawai');
		$data_pegawai = $this->pegawai_pusat->get_by_id($id_pegawai);
		if($data_pegawai){
			$kode_toko = $data_pegawai->kode_toko;
		}else{
			redirect(base_url().'paket_harga_spesial');
			exit();
		}

		$id_paket_promo_m         = $this->input->get('id');
		$data_paket_promo         = $this->paket_promo_master->get_master($id_paket_promo_m, $kode_toko);

		$data['data_pegawai']         = $data_pegawai;
		$data['data_induk_menu']      = $this->useraccess_pusat->get_induk_menu($id_pegawai);
		$data['atribut_halaman']      = 'Retur Pembelian';
		$data['id_paket_promo_m'] = $id_paket_promo_m;
		$data['no_paket_promo']   = $data_paket_promo->no_paket_promo;
		$data['halaman_transaksi']    = $this->load->view('admin/transaksi/paket_harga_spesial/transaksi',$data,true);
		$data['halaman_plugin']       = $this->load->view('admin/transaksi/paket_harga_spesial/plugin',$data,true);
		$data['isi_halaman']          = $this->load->view('admin/transaksi/paket_harga_spesial/index',$data,true);
		$this->load->view('admin/layout',$data);
	}

	public function ambil_data()
	{
		$id_pegawai   = $this->session->userdata('id_pegawai');
		$data_pegawai = $this->pegawai_pusat->get_by_id($id_pegawai);
		if($data_pegawai){
			// Cek user acces menu
			$cek_useraccess = $this->useraccess_pusat->cek_access($id_pegawai, '30');
			if($cek_useraccess->act_read == '0' or $cek_useraccess->act_read == '-'){
				redirect(base_url().'paket_harga_spesial');
				exit();
			}
			
			$id_paket_promo_m = $this->input->post('id_paket_promo_m');
			if($id_paket_promo_m == ''){
				redirect(base_url().'paket_harga_spesial');
				exit();
			}
	
			$data_induk = $this->paket_promo_master->get_master($id_paket_promo_m);
			if($data_induk){
				$data_batal = $this->paket_promo_master->get_jumlah_batal($id_paket_promo_m);
				echo json_encode(array(
					"data_induk" => $data_induk,
					"data_batal" => $data_batal
				));
			}else{
				redirect(base_url().'paket_harga_spesial');
				exit();	
			}
		}else{
			redirect(base_url().'paket_harga_spesial');
			exit();
		}
	}

	public function simpan_detail()
	{
		$id_pegawai   = $this->session->userdata('id_pegawai');
		$data_pegawai = $this->pegawai_pusat->get_by_id($id_pegawai);
		if($data_pegawai){
		}else{
			redirect(base_url().'paket_harga_spesial');
			exit();
		}

		// Cek user acces menu
		$cek_useraccess  = $this->useraccess_pusat->cek_access($id_pegawai, '30');
		$id_barang_pusat = $this->input->post('id_barang_pusat');
		$discount_value  = $this->input->post('discount_value');
		$discount_harga  = $this->input->post('discount_harga');
		if($id_barang_pusat == ''){
			redirect(base_url().'paket_harga_spesial');
			exit();
		}

		// Ambil data paket_promo master
		$kode_toko         = $this->input->post('kode_toko');
		$id_paket_promo_m  = $this->clean_tag_input($this->input->post('id_paket_promo_m'));
		$kode_paket_promo  = $this->clean_tag_input($this->input->post('kode_paket_promo'));
		$nama_paket_promo  = $this->clean_tag_input($this->input->post('nama_paket_promo'));
		$tipe_paket        = $this->clean_tag_input($this->input->post('tipe_paket'));
		$id_kategori_paket = $this->clean_tag_input($this->input->post('id_kategori_paket'));
		$id_jenis_paket    = $this->clean_tag_input($this->input->post('id_jenis_paket'));
		$tanggal_awal      = $this->clean_tag_input($this->input->post('tanggal_awal'));
		$tanggal_akhir     = $this->clean_tag_input($this->input->post('tanggal_akhir'));
		$tanggal           = date('Y-m-d H: i: s');
		$catatan           = $this->clean_tag_input($this->input->post('catatan'));
		$jumlah_barang     = $this->input->post('jumlah_barang');

		// Ambil id toko
		$data_toko = $this->customer_pusat->get_by_kode($kode_toko);
		if($data_toko){
			$id_toko = $data_toko->id_customer_pusat;
		}else{
			redirect(base_url().'paket_harga_spesial');
			exit();
		}

		// Validasi paket_promo master sudah ada atau belum
		$data_master = $this->paket_promo_master->get_by_id($id_paket_promo_m);
		if($data_master){
			if($cek_useraccess->act_create == '1' or $cek_useraccess->act_update == '1'){
				$id_master = $data_master->id_paket_promo_m;
				$kode_toko = $data_master->kode_toko;
				$id_toko   = $data_master->id_toko;
				if($data_master->tipe_paket == 'PAKET REGULER'){
						$tipe_paket = $data_master->tipe_paket;
				}
			}else{
				echo json_encode(array(	
					'status' => 2,
					'pesan'  => 'Maaf anda tidak diizinkan untuk menyimpan data paket promo barang.',
					'url'    => 'paket_harga_spesial/transaksi/?&id='.$id_paket_promo_m
				));
				exit();
			}
		}else{
			if($cek_useraccess->act_create == '1' or $cek_useraccess->act_update == '1'){
				$this->validate_stok($id_barang_pusat, $jumlah_barang, $kode_toko);

				// Insert paket_promo master
				$kode_paket_promo = $this->kode_paket_promo_baru($id_pegawai, $kode_toko, $tipe_paket);
				$master           = $this->paket_promo_master->insert_master(
					$kode_paket_promo, $nama_paket_promo, $tipe_paket, $id_kategori_paket, $id_jenis_paket, $tanggal_awal, $tanggal_akhir,
					$catatan, $tanggal, $id_pegawai, $id_toko, $kode_toko
				);

				$data_master = $this->paket_promo_master->get_by_kode_paket($kode_paket_promo, $kode_toko);
				$id_master               = $data_master->id_paket_promo_m;
			}else{
				echo json_encode(array(	
					'status' => 2,
					'pesan'  => 'Maaf anda tidak diizinkan untuk menyimpan data paket promo barang.',
					'url'    => 'paket_harga_spesial/transaksi/?&id='.$id_paket_promo_m
				));
				exit();
			}
		}

		if($id_master){
			// Ambil data barang dari master barang pusat
			$data_barang = $this->barang_toko->get_by_id_barang_pusat($id_barang_pusat, $kode_toko);
			if($data_barang){
				$sku           = $data_barang->sku;
				$nama_barang   = $data_barang->nama_barang;
				$harga_satuan  = $data_barang->harga_eceran;
				$stok_sekarang = $data_barang->total_stok;
			}

			// Validasi insert atas update di paket_promo detail
			$data_paket_promo_detail = $this->paket_promo_detail->get_id($id_master, $id_barang_pusat);
			if ($data_paket_promo_detail){
				if($cek_useraccess->act_update == '1'){
					$id_detail = $data_paket_promo_detail->id_paket_promo_d;
					$this->validate_stok($id_barang_pusat, $jumlah_barang, $kode_toko);

					// update paket detail
					$simpan_data = $this->paket_promo_detail->update_detail( 
						$id_master, $id_barang_pusat,  
						$jumlah_barang, $harga_satuan, 
						$discount_value, $discount_harga, 
						$id_pegawai, $kode_toko
					);
				}else{
					echo json_encode(array(	
						'status' => 2,
						'pesan'  => 'Maaf anda tidak diizinkan untuk mengedit data paket promo barang.',
						'url'    => 'paket_harga_spesial/transaksi/?&id='.$id_paket_promo_m
					));
					exit();
				}
			}else{
				if($cek_useraccess->act_create == '1'){
					$id_detail 			= '';
					$this->validate_stok($id_barang_pusat, $jumlah_barang, $kode_toko);

					// insert
					$simpan_data = $this->paket_promo_detail->insert_detail( 
						$id_master, $id_barang_pusat,
						$jumlah_barang, $harga_satuan,
						$discount_value, $discount_harga,
						$id_pegawai, $kode_toko
					);
				}else{
					echo json_encode(array(	
						'status' => 2,
						'pesan'  => 'Maaf anda tidak diizinkan untuk menambahkan data paket promo barang.',
						'url'    => 'paket_harga_spesial/transaksi/?&id='.$id_paket_promo_m
					));
					exit();
				}
			}

			if($simpan_data){
				$data_master = $this->paket_promo_master->get_master($id_master, $kode_toko);
				if($data_master){
					$jumlah_barang_paket = $data_master->jumlah_barang;
					$status_paket        = $data_master->status_paket;
				}else{
					$jumlah_barang_paket = '0';
				}

				$data_batal = $this->paket_promo_master->get_jumlah_batal($id_master, $kode_toko);
				if($data_batal){
					$jumlah_barang_paket_batal = $data_batal->jumlah_barang;
				}else{
					$jumlah_barang_paket_batal = '0';
				}

				// Update total dan grandtotal di paket_promo master
				$master = $this->paket_promo_master->update_master(
					$id_master, $nama_paket_promo, $tipe_paket, $id_kategori_paket, $id_jenis_paket, $tanggal_awal, $tanggal_akhir, $status_paket,
					$catatan, $id_pegawai, $kode_toko
				);

				echo json_encode(array(
					'status'                    => 1,
					'pesan'                     => "Data retur pembelian barang berhasil disimpan!",
					'url'                       => 'paket_harga_spesial/transaksi/?&id='.$id_master,
					'id_rpm'                    => $id_master,
					'jumlah_barang_paket'       => $jumlah_barang_paket,
					'jumlah_barang_paket_batal' => $jumlah_barang_paket_batal
				));
			}else{
				echo json_encode(array(
					'status' => 2,
					'url'    => 'paket_harga_spesial/transaksi/?&id='.$id_master
				));
			}
		}
	}

	public function simpan_kategori()
	{
		$id_pegawai   = $this->session->userdata('id_pegawai');
		$data_pegawai = $this->pegawai_pusat->get_by_id($id_pegawai);
		if($data_pegawai){
			$kode_toko = $data_pegawai->kode_toko;
			$id_toko   = $data_pegawai->id_toko;
		}else{
			echo json_encode(array("status" => 1));
			exit();
		}

		$id_kategori_paket   = $this->input->post('id_kategori_paket');
		$nama_kategori_paket = $this->input->post('nama_kategori_paket');

		// Validasi id kategori paket, untuk menentukan tambah atau edit
		$validasi_id = $this->kategori_paket->get_kategori('id_kategori_paket', $id_kategori_paket);
		if($validasi_id){
			// Validasi nama kategori yang akan di edit
			$validasi_nama = $this->kategori_paket->get_kategori('nama_kategori_paket', $nama_kategori_paket);
			if($validasi_nama){
				if($id_kategori_paket = $validasi_nama->id_kategori_paket){
					// Edit
					$master = $this->kategori_paket->update_kategori_paket(
						$id_kategori_paket, $nama_kategori_paket, $id_pegawai
			   		);
				}else{
					echo json_encode(array(		
						'status' => 0,
						'pesan'  => 'Nama kategori paket sudah digunakan.'
					));
					exit();
				} 
			}else{				
				// Edit
				$master = $this->kategori_paket->update_kategori_paket(
					$id_kategori_paket, $nama_kategori_paket, $id_pegawai
		   		);
			}
		}else{
			// Validasi nama kategori yang akan di tambah
			$validasi_nama = $this->kategori_paket->get_kategori('nama_kategori_paket', $nama_kategori_paket);
			if($validasi_nama){
				echo json_encode(array(		
					'status' => 0,
					'pesan'  => 'Nama kategori paket sudah digunakan.'
				));
				exit();
			}else{				
				// Tambah
				$master = $this->kategori_paket->insert_kategori_paket(
					$nama_kategori_paket, $id_pegawai
		  		);
			}
		}

		echo json_encode(array(		
			'status' => 1
		));
	}

	public function ajax_list_kategori()
	{
		$id_pegawai   = $this->session->userdata('id_pegawai');
		$data_pegawai = $this->pegawai_pusat->get_by_id($id_pegawai);
		if($data_pegawai){
			// Cek user acces menu
			$cek_useraccess = $this->useraccess_pusat->cek_access($id_pegawai, '30');
			if($cek_useraccess->act_read == '0' or $cek_useraccess->act_read == '-'){
				redirect(base_url().'dashboard');
			}
			
			$list = $this->paket_promo_detail->get_datatables('_get_datatables_query_kategori', '', '');
			$data = array();
			$no   = $_POST['start'];
	
			foreach ($list as $rpd) {
				$no++;
				$row = array();
				$row[] = $no;
	
				//add html for action
				if($cek_useraccess->act_update == 1){
					$tombol_edit = '<a class="btn btn-rounded btn-default btn-xs" onclick="edit_kategori('."'".$rpd->id_kategori_paket."'".')">
										<i class="fa fa-pencil" style="color:blue;"></i>
									</a>';
				}else{
					$tombol_edit = '';
				}
	
				if($cek_useraccess->act_delete == 1){
					$tombol_hapus = '<a class="btn btn-rounded btn-default btn-xs" onclick="hapus_kategori('."'".$rpd->id_kategori_paket."'".')">
										<i class="fa fa-times" style="color:red;"></i>
									</a>';
				}else{
					$tombol_hapus = '';
				}
	
				if($tombol_edit == '' AND $tombol_hapus == ''){
					$tombol_keterangan = '<b class="btn btn-rounded btn-default btn-xs text-muted text-center">Tidak ada akses</b>';
				}else{
					$tombol_keterangan = '';
				}
	
				$row[] = '
					'.$tombol_edit.'
					'.$tombol_hapus.'
					'.$tombol_keterangan.'
				';
	
				$row[] 	= $rpd->nama_kategori_paket;	
				$data[]	= $row;
			}
	
			$output = array(
				"draw"            => $_POST['draw'],
				"recordsTotal"    => $this->paket_promo_detail->count_all('vamr4846_toko_mrc.kategori_paket', '', ''),
				"recordsFiltered" => $this->paket_promo_detail->count_filtered('_get_datatables_query_kategori', '', ''),
				"data"            => $data
			);		
			echo json_encode($output);
		}else{
			redirect(base_url().'paket_harga_spesial');
			exit();
		}
	}

	public function ajax_list()
	{
		$id_pegawai   = $this->session->userdata('id_pegawai');
		$data_pegawai = $this->pegawai_pusat->get_by_id($id_pegawai);
		if($data_pegawai){
			// Cek user acces menu
			$cek_useraccess = $this->useraccess_pusat->cek_access($id_pegawai, '30');
			if($cek_useraccess->act_read == '0' or $cek_useraccess->act_read == '-'){
				redirect(base_url().'dashboard');
				exit();
			}
	
			$id_paket_promo_m = $this->clean_tag_input($this->input->post('id_paket_promo_m'));
			if($id_paket_promo_m == ''){
				redirect(base_url().'paket_harga_spesial');
				exit();
			}
			
			// Validasi paket master
			$data_master = $this->paket_promo_master->get_master($id_paket_promo_m);
			if($data_master){
				$kode_toko = $data_master->kode_toko;
			}else{
				redirect(base_url().'paket_harga_spesial');
				exit();
			}

			$list = $this->paket_promo_detail->get_datatables('_get_datatables_query', $id_paket_promo_m, $kode_toko);
			$data = array();
			$no   = $_POST['start'];
	
			foreach ($list as $rpd) {
				$no++;
				$row = array();
				$row[] = $no;
	
				//add html for action
				if($cek_useraccess->act_update == 1){
					$tombol_edit = '<a class="btn btn-rounded btn-default btn-xs" onclick="edit_detail('."'".$rpd->id_paket_promo_d."'".', '."'".$rpd->id_paket_promo_m."'".')">
										<i class="fa fa-pencil" style="color:blue;"></i>
									</a>';
				}else{
					$tombol_edit = '';
				}
	
				if($cek_useraccess->act_delete == 1){
					$tombol_hapus = '<a class="btn btn-rounded btn-default btn-xs" onclick="verifikasi_hapus_detail('."'".$rpd->id_paket_promo_d."'".', '."'".$rpd->id_paket_promo_m."'".')">
										<i class="fa fa-times" style="color:red;"></i>
									</a>';
				}else{
					$tombol_hapus = '';
				}
	
				if($tombol_edit == '' AND $tombol_hapus == ''){
					$tombol_keterangan = '<b class="btn btn-rounded btn-default btn-xs text-muted text-center">Tidak ada akses</b>';
				}else{
					$tombol_keterangan = '';
				}
	
				$row[] =	'
								'.$tombol_edit.'
								'.$tombol_hapus.'
								'.$tombol_keterangan.'
								';
	
				$row[] 	= $rpd->sku;
				$row[] 	= $rpd->nama_barang;
				$row[] 	= '<span class="pull-right">'.number_format($rpd->harga_eceran,'0',',','.').'</span>';
				$row[] 	= '<span class="pull-right">'.number_format($rpd->discount_harga,'0',',','.').'</span>';
				$row[] 	= '<span class="pull-right">'.number_format($rpd->qty,'0',',','.').'</span>';
				$row[] 	= '<span class="pull-right">'.number_format($rpd->subtotal,'0',',','.').'</span>';
				$row[] 	= $rpd->pegawai_save;
				$row[] 	= $rpd->tanggal_pembuatan;	
				$row[] 	= $rpd->pegawai_edit;
				$row[] 	= $rpd->tanggal_pembaharuan;		
				$data[]	= $row;
			}
	
			$output = array(
				"draw"            => $_POST['draw'],
				"recordsTotal"    => $this->paket_promo_detail->count_all('vamr4846_toko_mrc.paket_detail', $id_paket_promo_m, 'TIDAK'),
				"recordsFiltered" => $this->paket_promo_detail->count_filtered('_get_datatables_query', $id_paket_promo_m, $kode_toko),
				"data"            => $data
			);		
			echo json_encode($output);
		}else{
			redirect(base_url().'paket_harga_spesial');
			exit();
		}
	}

	public function ajax_list_batal()
	{
		$id_pegawai   = $this->session->userdata('id_pegawai');
		$data_pegawai = $this->pegawai_pusat->get_by_id($id_pegawai);
		if($data_pegawai){
			// Cek user acces menu
			$cek_useraccess = $this->useraccess_pusat->cek_access($id_pegawai, '30');
			if($cek_useraccess->act_read == '0' or $cek_useraccess->act_read == '-'){
				redirect(base_url().'dashboard');
			}
	
			$id_paket_promo_m = $this->clean_tag_input($this->input->post('id_paket_promo_m'));
			if($id_paket_promo_m == ''){
				redirect(base_url().'paket_harga_spesial');
			}
	
			// Validasi paket master
			$data_master = $this->paket_promo_master->get_master($id_paket_promo_m);
			if($data_master){
				$kode_toko = $data_master->kode_toko;
			}else{
				redirect(base_url().'paket_harga_spesial');
				exit();
			}
	
			$list = $this->paket_promo_detail->get_datatables('_get_datatables_query_batal', $id_paket_promo_m, $kode_toko);
			$data = array();
			$no   = $_POST['start'];
	
			foreach ($list as $rpd) {
				$no++;
				$row = array();
				$row[] = $no;
	
				$row[] 	= $rpd->sku;
				$row[] 	= $rpd->nama_barang;
				$row[] 	= '<span class="pull-right">'.number_format($rpd->harga_eceran,'0',',','.').'</span>';
				$row[] 	= '<span class="pull-right">'.number_format($rpd->qty,'0',',','.').'</span>';
				
				$row[] 	= $rpd->keterangan_hapus;
				$row[] 	= $rpd->pegawai_hapus;
				$row[] 	= $rpd->tanggal_hapus;	
				$row[] 	= $rpd->pegawai_save;
				$row[] 	= $rpd->tanggal_pembuatan;	
				$row[] 	= $rpd->pegawai_edit;
				$row[] 	= $rpd->tanggal_pembaharuan;		
				$data[]	= $row;
			}
	
	
			$output = array(
				"draw"            => $_POST['draw'],
				"recordsTotal"    => $this->paket_promo_detail->count_all('vamr4846_toko_mrc.paket_detail', $id_paket_promo_m, 'IYA'),
				"recordsFiltered" => $this->paket_promo_detail->count_filtered('_get_datatables_query_batal', $id_paket_promo_m, $kode_toko),
				"data"            => $data
			);		
			echo json_encode($output);
		}else{
			redirect(base_url().'paket_harga_spesial');
			exit();
		}
	}

	public function ajax_list_barang()
	{
		$id_pegawai   = $this->session->userdata('id_pegawai');
		$data_pegawai = $this->pegawai_pusat->get_by_id($id_pegawai);
		if($data_pegawai){
		}else{
			redirect(base_url().'paket_harga_spesial');
			exit();
		}

		// Cek user acces menu
		$cek_useraccess = $this->useraccess_pusat->cek_access($id_pegawai, '30');
		if($cek_useraccess->act_read == '0' or $cek_useraccess->act_read == '-'){
			redirect(base_url().'dashboard');
			exit();
		}

		$id_paket_promo_m = $this->clean_tag_input($this->input->post('id_paket_promo_m'));
		if($id_paket_promo_m == ''){
			redirect(base_url().'paket_harga_spesial');
			exit();
		}

		$data_master = $this->paket_promo_master->get_by_id($id_paket_promo_m);
		if($data_master){
			$id_master = $data_master->id_paket_promo_m;
			$kode_toko = $data_master->kode_toko;
		}else{
			$id_master = '0';
			$kode_toko = $this->input->post('kode_toko');
		}

		$list = $this->paket_promo_detail->get_datatables('_get_datatables_query_barang', $id_master, $kode_toko);
		$data = array();
		$no   = $_POST['start'];

		foreach ($list as $bt){
			$no++;
			$row   = array();
			$row[] = $no;
			$row[] = $bt->sku;
			$row[] = $bt->nama_barang;
			$row[] 	= '<span class="pull-right">'.number_format($bt->harga_satuan,'0',',','.').'</span>';
			$row[] 	= '<span class="pull-right">'.number_format($bt->total_stok,'0',',','.').'</span>';
			$row[] 	= '<span class="pull-right">'.number_format($bt->total_stok_rusak,'0',',','.').'</span>';

			$row[] 	= '<a class="btn btn-primary btn-xs" onclick="proses_barang('."'".$bt->sku."'".', '."'".$bt->kode_toko."'".')">
					   	<i class="fa fa-check"></i> PILIH
					   </a>';
			$data[]	= $row;
		}

		$output = array(
			"draw"            => $_POST['draw'],
			"recordsTotal"    => $this->paket_promo_detail->count_all('vamr4846_toko_mrc.barang_toko_'.$kode_toko.'', $id_master, $kode_toko),
			"recordsFiltered" => $this->paket_promo_detail->count_filtered('_get_datatables_query_barang', $id_master, $kode_toko),
			"data"            => $data
		);
		echo json_encode($output);
	}

	public function ajax_cari_barang()
	{
		if($this->input->is_ajax_request()){
			$id_pegawai   = $this->session->userdata('id_pegawai');
			$data_pegawai = $this->pegawai_pusat->get_by_id($id_pegawai);
			if($data_pegawai){
			}else{
				redirect(base_url().'paket_harga_spesial');
				exit();
			}

			$cek_useraccess = $this->useraccess_pusat->cek_access($id_pegawai, '30');
			if($cek_useraccess->act_create == '1'){
				$sku         = $this->input->post('sku');
				$kode_toko   = $this->input->post('kode_toko');
				$data_barang = $this->barang_toko->ambil_barang_untuk_penjualan($sku, $kode_toko);				

				if($data_barang->row()){
					$json['status'] = 1;
					$json['data']   = $data_barang->row();
				}else{
					$json['status'] = 0;
					$json['pesan']  = "	SKU : </br>
										<b class='text-dark'>".$sku."</b></br></br>  
										<b class='text-danger'>Tidak terdaftar!</b></br></br>
									";
				}
			}else{
				$json['status'] = 2;
				$json['pesan']  = "	Maaf anda tidak di izinkan 
									untuk menambahkan barang retur pembelian.";
			}
			echo json_encode($json);
		}else{
			redirect(base_url().'paket_harga_spesial');
			exit();
		}
	}

	public function ajax_edit()
	{
		$id_pegawai   = $this->session->userdata('id_pegawai');
		$data_pegawai = $this->pegawai_pusat->get_by_id($id_pegawai);
		if($data_pegawai){

		}else{
			redirect(base_url().'paket_harga_spesial');
			exit();
		}

		// Cek user acces menu
		$cek_useraccess 	= $this->useraccess_pusat->cek_access($id_pegawai, '30');
		if($cek_useraccess->act_update == '0' or $cek_useraccess->act_update == '-'){
			redirect(base_url()."paket_harga_spesial");
			exit();
		}

		$id_paket_promo_d = $this->input->post('id_paket_promo_d');
		$id_paket_promo_m = $this->input->post('id_paket_promo_m');
		$url              = $this->input->post('url');
		if($id_paket_promo_d == ''){
			if($url == ''){
				redirect(base_url()."paket_harga_spesial");					
			}else{
				redirect(base_url().$url);
			}
		}else{
			// Ambil kode toko
			$data_master = $this->paket_promo_master->get_by_id($id_paket_promo_m);
			if($data_master){
				$data = $this->paket_promo_detail->get_paket_promo_detail($id_paket_promo_d, $data_master->kode_toko);
				echo json_encode(array(
					"status" => 1,
					"data"   => $data
				));				
			}else{
				redirect(base_url()."paket_harga_spesial");	
			}
		}
	}

	public function ajax_edit_kategori($id_kategori_paket)
	{
		$id_pegawai   = $this->session->userdata('id_pegawai');
		$data_pegawai = $this->pegawai_pusat->get_by_id($id_pegawai);
		if($data_pegawai){
			$kode_toko = $data_pegawai->kode_toko;
			$id_toko   = $data_pegawai->id_toko;
		}else{
			echo json_encode(array("status" => 1));
			exit();
		}

		$data = $this->kategori_paket->get_kategori('id_kategori_paket', $id_kategori_paket);
		echo json_encode($data);
	}

	public function ajax_verifikasi_hapus_detail()
	{
		$id_pegawai   = $this->session->userdata('id_pegawai');
		$data_pegawai = $this->pegawai_pusat->get_by_id($id_pegawai);
		if($data_pegawai){
		}else{
			redirect(base_url().'paket_harga_spesial');
			exit();
		}

		// Cek user acces menu
		$cek_useraccess = $this->useraccess_pusat->cek_access($id_pegawai, '30');
		if($cek_useraccess->act_delete == '0' or $cek_useraccess->act_delete == '-'){
			redirect(base_url()."paket_harga_spesial");
			exit();
		}

		$id_paket_promo_m = $this->input->post('id_paket_promo_m');
		$id_paket_promo_d = $this->input->post('id_paket_promo_d');
		$url              = $this->input->post('url');
		if($id_paket_promo_d == ''){
			if($url == ''){
				redirect(base_url()."paket_harga_spesial");					
			}else{
				redirect(base_url().$url);
			}
		}

		// Validasi paket master
		$data_master = $this->paket_promo_master->get_by_id($id_paket_promo_m);
		if($data_master){
			$data = $this->paket_promo_detail->get_paket_promo_detail($id_paket_promo_d, $data_master->kode_toko);
			if($data){
				echo json_encode(array(	"status"	=> 1,
										"pesan" 	=>	"<table class='table' id='detail_hapus'>
															<thead></thead>
															<tbody>
																<tr>
																	<td>
																		<small>SKU : </small>
																		<b class='text-dark'>$data->sku </b><br/><br/>
																																			
																		<small>Nama Barang : </small> <br/>	
																		<b class='text-dark'>$data->nama_barang</b></br><br/>
																		
																		<small>Harga Eceran : </small>
																		<b class='text-dark'>Rp. $data->harga</b><br/><br/>
																		
																		<small>Qty  : </small><br/>	
																		<h2 class='text-dark'>$data->qty</h2>

																		<textarea name='keterangan_batal' id='keterangan_batal' class='form-control input-md' placeholder='Keterangan batal' style='resize: vertical;'></textarea>
																	</td>
																</tr>
															</tbody>
														</table>",
										"footer"	=> 	"<button onclick='hapus_detail($data->id_paket_promo_d, $data->id_paket_promo_m)' type='button' class='btn btn-primary waves-effect waves-light' 
															data-dismiss='modal' autofocus>
															Iya, Hapus
														</button> 
														<button type='button' class='btn btn-default waves-effect' data-dismiss='modal'>Batal</button>"
									)
								);
			}else{
				echo json_encode(array(	
					"status" => 0,
					"pesan"  => "Data barang tidak di temukan."
				));
			}
		}else{
			redirect(base_url()."paket_harga_spesial");	
		}
	}

	public function ajax_hapus_detail()
	{
		if($this->input->is_ajax_request()){						
			$id_pegawai   = $this->session->userdata('id_pegawai');
			$data_pegawai = $this->pegawai_pusat->get_by_id($id_pegawai);
			if($data_pegawai){
			}else{
				redirect(base_url().'paket_harga_spesial');
				exit();
			}

			// Cek user acces menu
			$cek_useraccess = $this->useraccess_pusat->cek_access($id_pegawai, '30');
			if($cek_useraccess->act_delete == '0' or $cek_useraccess->act_delete == '-'){
				redirect(base_url()."paket_harga_spesial");
				exit();
			}

			$id_pegawai_pembatalan = $this->session->userdata('id_pegawai');
			$nama_paket_promo      = $this->input->post('nama_paket_promo');
			$tipe_paket            = $this->input->post('tipe_paket');
			$id_kategori_paket     = $this->input->post('id_kategori_paket');
			$id_jenis_paket        = $this->input->post('id_jenis_paket');
			$keterangan_batal      = $this->input->post('keterangan_batal');
			$tanggal_awal          = $this->input->post('tanggal_awal');
			$tanggal_akhir         = $this->input->post('tanggal_akhir');
			$catatan               = $this->clean_tag_input($this->input->post('catatan'));
			$id_paket_promo_m      = $this->input->post('id_paket_promo_m');
			$id_paket_promo_d      = $this->input->post('id_paket_promo_d');
			$url                   = $this->input->post('url');

			if($url == ''){
				$url = 'paket_harga_spesial';					
			}

			if($id_paket_promo_d == ''){
				redirect(base_url()."paket_harga_spesial");
				exit();
			}

			// Ambil data paket_promo detail
			$data_master = $this->paket_promo_master->get_by_id($id_paket_promo_m);
			if($data_master){
				$kode_toko = $data_master->kode_toko;
				$dt        = $this->paket_promo_detail->get_paket_promo_detail($id_paket_promo_d, $kode_toko);
				if($dt){
					// Validasi status paket, jika sudah di terima, maka sudah tidak bisa di hapus
					if($dt->status_paket == 'DI TERIMA'){
						echo json_encode(array(
							'status'      => 0,
							'judul'       => 'Gagal!',
							'pesan'       => 'Maaf paket sudah di terima / di approve pusat dan sudah tidak bisa di hapus.',
							'tipe_pesan'  => 'error',
							'gaya_tombol' => 'btn-danger btn-md waves-effect waves-light',
							'url'         => $url
						));
						exit();
					}else{
						$id_master = $dt->id_paket_promo_m;
						$hapus     = $this->paket_promo_detail->hapus_paket_promo_detail(
							$dt->id_paket_promo_d, $dt->id_paket_promo_m,
							$id_pegawai_pembatalan, $keterangan_batal
						);
					}
				}
				
				if($hapus){
					$data_induk = $this->paket_promo_master->get_master($id_master, $kode_toko);
					$data_batal = $this->paket_promo_master->get_jumlah_batal($id_master, $kode_toko);		
					if ($data_induk){
						$jumlah_barang_paket       = $data_induk->jumlah_barang;
						$jumlah_barang_paket_batal = $data_induk->jumlah_barang;
						$status_paket              = $data_induk->status_paket;

						if($jumlah_barang_paket == '0'){
							$hapus = $this->paket_promo_master->hapus_transaksi($id_master, $id_pegawai_pembatalan, $keterangan_batal);
							echo json_encode(array(
								'status'     => 0,
								'judul'      => 'Berhasil!',
								'pesan'      => 'Data paket promo barang berhasil dihapus.',
								'tipe_pesan' => 'success',
								'gaya_tombol' => 'btn-success btn-md waves-effect waves-light',
								'url'        => 'paket_harga_spesial'
							));
							exit();
						}

						// Update total dan grandtotal di paket_promo master
						$master = $this->paket_promo_master->update_master(
							$id_master, $nama_paket_promo, $tipe_paket, $id_kategori_paket, $id_jenis_paket, $tanggal_awal, $tanggal_akhir, $status_paket,
							$catatan, $id_pegawai, $kode_toko
						);
					}else{
						$jumlah_barang_retur = '0';
					}

					$data_batal = $this->paket_promo_master->get_jumlah_batal($id_master, $kode_toko);
					if($data_batal){
						$jumlah_barang_paket_batal = $data_batal->jumlah_barang;
					}else{
						$jumlah_barang_paket_batal = '0';
					}

					// Tampilkan informasi hapus sesuai dengan user group
					echo json_encode(array(
						'status'                    => '1',
						'id_pm'                     => $id_master,
						'jumlah_barang_paket'       => $jumlah_barang_paket,
						'jumlah_barang_paket_batal' => $jumlah_barang_paket_batal,
						'pesan'                     => "Data paket promo barang berhasil dihapus."
					));
				}else{
					echo json_encode(array(
						'status'      => 0,
						'judul'       => 'Gagal!',
						'pesan'       => 'Gagal menghapus data paket promo barang dari ajax.',
						'tipe_pesan'  => 'error',
						'gaya_tombol' => 'btn-danger btn-md waves-effect waves-light',
						'url'         => $url
					));
				}
			}else{
				redirect(base_url().'paket_harga_spesial');
			}		
		}else{
			redirect(base_url().'paket_harga_spesial');
		}
	}

	public function ajax_kode()
	{
		if($this->input->is_ajax_request()){
			$id_pegawai   = $this->session->userdata('id_pegawai');
			$data_pegawai =  $this->pegawai_pusat->get_by_id($id_pegawai);
			if($data_pegawai){
			}else{
				$json['status'] = 0;
				exit();
			}

			$keyword   = $this->input->post('keyword');
			$id_master = $this->input->post('id_master');
			$kode_toko = $this->input->post('kode_toko');

			// Validasi paket master
			$data_master = $this->paket_promo_master->get_by_id($id_master);
			if($data_master){
				$kode_toko = $data_master->kode_toko;
			}
			$barang = $this->barang_toko->cari_kode_paket_promo($keyword, $id_master, $kode_toko);

			if($barang->num_rows() > 0){
				$json['status'] 		= 1;
				$json['datanya'] 		= "<ul id='daftar-autocomplete' class='list-group user-list'>";
				
				foreach($barang->result() as $b){
					if($b->foto!=""){
						$json['datanya'] .= "
			                <li class='user-list-item'>
			                	<div class='avatar avatar-sm-box'>
                                    <img src='assets/upload/image/barang/thumbs/$b->foto'>
			                	</div>";
			        }else{
						$nama_barang 	  = $b->nama_barang;
						$inisial 		  = substr($nama_barang, 0, 1);
						$json['datanya'] .= "
			                <li class='user-list-item'>
			                	<div class='avatar avatar-sm-box'>
                                    <span class='avatar-sm-box bg-info'>".$inisial."</span>
			                	</div>";
					}

					$json['datanya'] .= "
		                	<div class='user-desc'>
								<span id='barangnya' class='name'><b>".$b->nama_barang."</b></span>
								<span id='skunya' class='name'>".$b->sku."</span>
								<span id='eceran' class='name text-dark'>HARGA : Rp. ".$b->harga."</span>
								<span id='harga_eceran' style='display:none;'>".$b->harga_eceran."</span>
								<span id='total_stok' class='name text-primary'>STOK JUAL : ".$b->total_stok."</span>
				
								<span id='id_barang' style='display :none;'>".$b->id_barang_pusat."</span>
					";

					$json['datanya'] .= "	
								</div>
							</li>
					";
				}

				$json['datanya'] .= "</ul>";
			}else{
				$json['status'] = 0;
			}

			echo json_encode($json);
		}
	}

	public function cek_kode_barang($sku)
	{
		$cek_kode = $this->barang_pusat->cek_kode($sku);

		if($cek_kode->num_rows() > 0)
		{
			return TRUE;
		}
		return FALSE;
	}

	private function _validate()
	{
		$data = array();
		$data['error_string'] = array();
		$data['inputerror'] = array();
		$data['status'] = TRUE;

		if($this->input->post('nama_customer_pusat') == ''){
			$data['inputerror'][] = 'nama_customer_pusat';
			$data['error_string'][] = 'Nama customer pusat wajib diisi';
			$data['status'] = FALSE;
		}

		if($this->input->post('alamat_customer_pusat') == ''){
			$data['inputerror'][] = 'alamat_customer_pusat';
			$data['error_string'][] = 'Alamat customer pusat wajib diisi';
			$data['status'] = FALSE;
		}

		if($this->input->post('email') == ''){
			$data['inputerror'][] = 'email';
			$data['error_string'][] = 'Email customer pusat wajib diisi';
			$data['status'] = FALSE;
		}

		if($this->input->post('handphone1') == ''){
			$data['inputerror'][] = 'handphone1';
			$data['error_string'][] = 'Handphone-1 customer pusat wajib diisi';
			$data['status'] = FALSE;
		}

		if($data['status'] === FALSE){
			echo json_encode($data);
			exit();
		}
	}

	private function validate_stok($id_barang_pusat, $jumlah_barang, $kode_toko)
	{
		$data           = array();
		$data['status'] = TRUE;		
		$data_cek_stok  = $this->barang_toko->cek_stok($id_barang_pusat, $kode_toko);

		if($jumlah_barang > $data_cek_stok->total_stok){
			echo json_encode(array(
				'status' => 3,
				'pesan'	 => "STOK UNTUK : </br>
										SKU </br>
										<b class='text-dark'>".$data_cek_stok->sku."</b></br></br>

										Nama Barang </br>
										<b class='text-dark'>".$data_cek_stok->nama_barang."</b></br></br> 
										
										Saat ini hanya tersisa  : </br>
										<h2 class='text-danger'>".$data_cek_stok->total_stok."</h2>",
				'jumlah input' => $jumlah_barang
			));
			exit();
		}
	}
}
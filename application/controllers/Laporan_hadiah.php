<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Laporan_hadiah extends MY_Controller 
{
	function __construct()
	{
		parent::__construct();
		$this->CI =& get_instance();
		$this->load->model('pegawai_pusat_model','pegawai_pusat');
		$this->load->model('useraccess_pusat_model','useraccess_pusat');
		$this->load->model('customer_pusat_model','supplier');
		$this->load->model('barang_pusat_model','barang_pusat');
		$this->load->model('hadiah_master_model','hadiah_master');
		$this->load->model('hadiah_detail_model','hadiah_detail');
	}

	public function index()
	{
		// Cek user acces menu
		$id_pegawai = $this->session->userdata('id_pegawai');
		if($id_pegawai == ''){
			redirect(base_url().'dashboard');
		}
		
		$cek_useraccess = $this->useraccess_pusat->cek_access($id_pegawai, '46');
		if($cek_useraccess){
			if($cek_useraccess->act_read == '0' or $cek_useraccess->act_read == '-'){
				redirect(base_url().'dashboard');
			}					
		}else{
			redirect(base_url().'dashboard');
		}

		$data_pegawai = $this->pegawai_pusat->get_by_id($id_pegawai);
		if($data_pegawai){
			$data['access_create']   = $cek_useraccess->act_create;
			$data['data_pegawai']    = $data_pegawai;
			$data['data_induk_menu'] = $this->useraccess_pusat->get_induk_menu($id_pegawai);

			$data['atribut_halaman'] = 'Laporan Hadiah';
			$data['halaman_laporan'] = $this->load->view('admin/laporan/hadiah/laporan',$data,true);
			$data['halaman_plugin']  = $this->load->view('admin/laporan/hadiah/plugin',$data,true);
			$data['isi_halaman']     = $this->load->view('admin/laporan/hadiah/index',$data,true);
			$this->load->view('admin/layout',$data);
		}else{
			redirect(base_url().'login');
		}
	}

	public function ajax_list()
	{
		// Cek user acces menu
		$id_pegawai               = $this->session->userdata('id_pegawai');
		$cek_useraccess_transaksi = $this->useraccess_pusat->cek_access($id_pegawai, '31');
		$cek_useraccess_laporan   = $this->useraccess_pusat->cek_access($id_pegawai, '46');
		if($cek_useraccess_laporan->act_read == '0' or $cek_useraccess_laporan->act_read == '-'){
			redirect(base_url().'dashboard');
		}

		$tanggal_filter = $this->input->post('tanggal_filter');
		if($tanggal_filter == ''){
			redirect(base_url().'laporan_hadiah');
			exit();
		}

		$tanggal_awal        = substr($tanggal_filter, 0, 10);
		$tanggal_akhir       = substr($tanggal_filter, 18, 27);
		$data_laporan_hadiah = $this->hadiah_master->get_laporan('_get_laporan_query', $tanggal_awal, $tanggal_akhir);
		
		$data                = array();
		$no                  = $_POST['start'];

		foreach ($data_laporan_hadiah as $dlp){
			$no++;
			$row    = array();
			$row[]  = $no;
			$status = $dlp->status_hadiah;

			if($cek_useraccess_laporan->act_update == 1){
				$tombol_edit 	 = '<a class="btn btn-rounded btn-default btn-xs" 
										onclick="edit_transaksi('."'".$dlp->id_hadiah_m."'".')">
										<i class="fa fa-pencil" style="color:blue;"></i>
									</a>';
			}else{
				if($cek_useraccess_transaksi->act_update == 1 AND $status=="MENUNGGU" 
					OR $cek_useraccess_transaksi->act_create == 1 AND $status=="MENUNGGU"){
					$tombol_edit = '<a class="btn btn-rounded btn-default btn-xs" 
										onclick="edit_transaksi('."'".$dlp->id_hadiah_m."'".')">
										<i class="fa fa-pencil" style="color:blue;"></i>
									</a>';
				}else{
					$tombol_edit = '';
				}
			}

			if($cek_useraccess_laporan->act_delete == 1){
				$tombol_hapus = '<a class="btn btn-rounded btn-default btn-xs" 
									onclick="verifikasi_hapus_transaksi('."'".$dlp->id_hadiah_m."'".')">
									<i class="fa fa-times" style="color:red;"></i>
								</a>';
			}else{
				$tombol_hapus = '';
				if($cek_useraccess_transaksi->act_delete == 1 AND $status=="MENUNGGU"){
					$tombol_hapus = '<a class="btn btn-rounded btn-default btn-xs" 
										onclick="verifikasi_hapus_transaksi('."'".$dlp->id_hadiah_m."'".')">
										<i class="fa fa-times" style="color:red;"></i>
									</a>';
				}else{
					$tombol_hapus = '';
				}
			}

			$row[] = '<a class="btn btn-rounded btn-default btn-xs" onclick="cetak_faktur('."'".$dlp->id_hadiah_m."'".')"><i class="fa fa-print"></i></a>
					  '.$tombol_edit.'
					  '.$tombol_hapus.'
				  	  ';
			// Akhir tombol

			$row[] 	= '<span class="text-dark">'.$dlp->no_hadiah.'</span><br/>';
			$row[] 	= '<span class="text-dark">'.$dlp->nama_customer.'</label>';
			$row[] 	= '<span class="text-dark">'.$dlp->nama_hadiah.'</span><br/>';
			$status = $dlp->status_hadiah;
			if($status=="SELESAI"){
				$row[] 	= '<span class="btn btn-success btn-xs btn-block btn-rounded text-center">'.$status.'</span>';
			}elseif($status=="MENUNGGU"){
				$row[] 	= '<span class="btn btn-danger btn-xs btn-block btn-rounded text-center">'.$status.'</span>';
			}
			$row[] 	= date('d-m-Y', strtotime($dlp->tanggal));

			$row[] 	= '<span><p class="pull-right">'.number_format($dlp->jumlah_barang,'0', ',', '.').'</p></span>';
			$row[] 	= '<span><p class="pull-right">'.number_format($dlp->total_qty,'0', ',', '.').'</p></span>';
			$row[] 	= '<span><p class="pull-right">'.number_format($dlp->total,'0', ',', '.').'</p></span>';

			$row[]  = $dlp->keterangan_lain;
			$row[]  = $dlp->pegawai_save;
			$row[]  = $dlp->tanggal_pembuatan;
			$row[]  = $dlp->pegawai_edit;
			$row[]  = $dlp->tanggal_pembaharuan;
			$data[] = $row;
		}

		$output = array(
			"draw"            => $_POST['draw'],
			"recordsTotal"    => $this->hadiah_master->count_all('vamr4846_vama.hadiah_master', $tanggal_awal, $tanggal_akhir),
			"recordsFiltered" => $this->hadiah_master->count_filtered('_get_laporan_query', $tanggal_awal, $tanggal_akhir),
			"data"            => $data
		);

		echo json_encode($output);
	}

	public function ajax_list_detail()
	{
		// Cek user acces menu
		$id_pegawai               = $this->session->userdata('id_pegawai');
		$cek_useraccess_transaksi = $this->useraccess_pusat->cek_access($id_pegawai, '31');
		$cek_useraccess_laporan   = $this->useraccess_pusat->cek_access($id_pegawai, '46');
		if($cek_useraccess_laporan->act_read == '0' or $cek_useraccess_laporan->act_read == '-'){
			redirect(base_url().'dashboard');
		}

		$tanggal_filter = $this->input->post('tanggal_filter');
		if($tanggal_filter == ''){
			redirect(base_url().'laporan_hadiah');
			exit();
		}

		$tanggal_awal        = substr($tanggal_filter, 0, 10);
		$tanggal_akhir       = substr($tanggal_filter, 18, 27);
		$data_laporan_hadiah = $this->hadiah_master->get_laporan('_get_laporan_query_detail', $tanggal_awal, $tanggal_akhir);
		
		$data                = array();
		$no                  = $_POST['start'];
		$no_hadiah           = '0';

		foreach ($data_laporan_hadiah as $dlh){
			$no++;
			$row    = array();
			$row[]  = $no;
			$status = $dlh->status_hadiah;

			// Awal otoritas tombol
			if($cek_useraccess_laporan->act_update == 1){
				$tombol_edit 	 = '<a class="btn btn-rounded btn-default btn-xs" 
										onclick="edit_transaksi('."'".$dlh->id_hadiah_m."'".')">
										<i class="fa fa-pencil" style="color:blue;"></i>
									</a>';
			}else{
				if($cek_useraccess_transaksi->act_update == 1 AND $status=="MENUNGGU" 
					OR $cek_useraccess_transaksi->act_create == 1 AND $status=="MENUNGGU"){
					$tombol_edit = '<a class="btn btn-rounded btn-default btn-xs" 
										onclick="edit_transaksi('."'".$dlh->id_hadiah_m."'".')">
										<i class="fa fa-pencil" style="color:blue;"></i>
									</a>';
				}else{
					$tombol_edit = '';
				}
			}

			if($cek_useraccess_laporan->act_delete == 1){
				$tombol_hapus = '<a class="btn btn-rounded btn-default btn-xs" 
									onclick="verifikasi_hapus_transaksi('."'".$dlh->id_hadiah_m."'".')">
									<i class="fa fa-times" style="color:red;"></i>
								</a>';
			}else{
				$tombol_hapus = '';
				if($cek_useraccess_transaksi->act_delete == 1 AND $status=="MENUNGGU"){
					$tombol_hapus = '<a class="btn btn-rounded btn-default btn-xs" 
										onclick="verifikasi_hapus_transaksi('."'".$dlh->id_hadiah_m."'".')">
										<i class="fa fa-times" style="color:red;"></i>
									</a>';
				}else{
					$tombol_hapus = '';
				}
			}

			$row[] = '<a class="btn btn-rounded btn-default btn-xs" onclick="cetak_faktur('."'".$dlh->id_hadiah_m."'".')"><i class="fa fa-print"></i></a>
					  '.$tombol_edit.'
					  '.$tombol_hapus.'
				  	  ';
			// Akhir otoritas tombol

			if($no_hadiah <> $dlh->no_hadiah){
				$row[] = '<span class="btn btn-pink btn-block btn-rounded btn-xs">'.$dlh->no_hadiah.'</span>';
			}else{
				$row[] = '<span class="btn btn-default btn-block btn-rounded btn-xs">'.$dlh->no_hadiah.'</span>';
			}

			$row[] 	= '<span class="text-dark">'.$dlh->nama_hadiah.'</span><br/>';
			if($status=="SELESAI"){
				$row[] = '<span class="btn btn-success btn-xs btn-block btn-rounded text-center">'.$status.'</span>';
			}elseif($status=="MENUNGGU"){
				$row[] = '<span class="btn btn-danger btn-xs btn-block btn-rounded text-center">'.$status.'</span>';
			}
			$row[]     = date('d-m-Y', strtotime($dlh->tanggal));
			
			$row[]     = $dlh->sku;
			$row[]     = $dlh->nama_barang;
			$row[]     = '<span><p class="pull-right">'.number_format($dlh->harga_satuan,'0', ',', '.').'</p></span>';			
			$row[]     = '<span><p class="pull-right">'.number_format($dlh->jumlah_hadiah,'0', ',', '.').'</p></span>';
			$row[]     = '<span><p class="pull-right">'.number_format($dlh->subtotal,'0', ',', '.').'</p></span>';
			
			$row[]     = '<span class="text-dark">'.$dlh->nama_customer.'</label>';
			$row[]     = $dlh->keterangan_lain;
			$row[]     = $dlh->pegawai_save;
			$row[]     = $dlh->tanggal_pembuatan;
			$row[]     = $dlh->pegawai_edit;
			$row[]     = $dlh->tanggal_pembaharuan;
			$data[]    = $row;
			$no_hadiah = $dlh->no_hadiah;
		}

		$output = array(
			"draw"            => $_POST['draw'],
			"recordsTotal"    => $this->hadiah_master->count_all('vamr4846_vama.hadiah_detail', $tanggal_awal, $tanggal_akhir),
			"recordsFiltered" => $this->hadiah_master->count_filtered('_get_laporan_query_detail', $tanggal_awal, $tanggal_akhir),
			"data"            => $data
		);

		echo json_encode($output);
	}

	public function ajax_list_batal()
	{
		// Cek user acces menu
		$id_pegawai               = $this->session->userdata('id_pegawai');
		$cek_useraccess_transaksi = $this->useraccess_pusat->cek_access($id_pegawai, '31');
		$cek_useraccess_laporan   = $this->useraccess_pusat->cek_access($id_pegawai, '46');
		if($cek_useraccess_laporan->act_read == '0' or $cek_useraccess_laporan->act_read == '-'){
			redirect(base_url().'dashboard');
		}

		$tanggal_filter = $this->input->post('tanggal_filter');
		if($tanggal_filter == ''){
			redirect(base_url().'laporan_hadiah');
			exit();
		}

		$tanggal_awal        = substr($tanggal_filter, 0, 10);
		$tanggal_akhir       = substr($tanggal_filter, 18, 27);
		$data_laporan_hadiah = $this->hadiah_master->get_laporan('_get_laporan_query_batal', $tanggal_awal, $tanggal_akhir);
		
		$data                = array();
		$no                  = $_POST['start'];
		$no_hadiah           = '0';

		foreach ($data_laporan_hadiah as $dlh){
			$no++;
			$row    = array();
			$row[]  = $no;

			if($no_hadiah <> $dlh->no_hadiah){
				$row[] = '<span class="btn btn-pink btn-block btn-rounded btn-xs">'.$dlh->no_hadiah.'</span>';
			}else{
				$row[] = '<span class="btn btn-default btn-block btn-rounded btn-xs">'.$dlh->no_hadiah.'</span>';
			}

			$row[]     = '<span class="text-dark">'.$dlh->nama_hadiah.'</span><br/>';
			$row[]     = $dlh->sku;
			$row[]     = $dlh->nama_barang;
			$row[]     = '<span><p class="pull-right">'.number_format($dlh->harga_satuan,'0', ',', '.').'</p></span>';			
			$row[]     = '<span><p class="pull-right">'.number_format($dlh->jumlah_hadiah,'0', ',', '.').'</p></span>';
			$row[]     = '<span><p class="pull-right">'.number_format($dlh->subtotal,'0', ',', '.').'</p></span>';
			
			$row[]     = '<span class="text-dark">'.$dlh->nama_customer.'</label>';
			$row[]     = $dlh->keterangan_batal;
			$row[]     = $dlh->pegawai_batal;
			$row[]     = $dlh->tanggal_pembatalan;
			$row[]     = $dlh->pegawai_save;
			$row[]     = $dlh->tanggal_pembuatan;
			$row[]     = $dlh->pegawai_edit;
			$row[]     = $dlh->tanggal_pembaharuan;
			$data[]    = $row;
			$no_hadiah = $dlh->no_hadiah;
		}

		$output = array(
			"draw"            => $_POST['draw'],
			"recordsTotal"    => $this->hadiah_master->count_all('vamr4846_vama.hadiah_detail_batal', $tanggal_awal, $tanggal_akhir),
			"recordsFiltered" => $this->hadiah_master->count_filtered('_get_laporan_query_batal', $tanggal_awal, $tanggal_akhir),
			"data"            => $data
		);

		echo json_encode($output);
	}

	public function ambil_total()
	{
		// Cek user acces menu
		$id_pegawai               = $this->session->userdata('id_pegawai');
		$cek_useraccess_transaksi = $this->useraccess_pusat->cek_access($id_pegawai, '31');
		$cek_useraccess_laporan   = $this->useraccess_pusat->cek_access($id_pegawai, '46');
		if($cek_useraccess_laporan->act_read == '0' or $cek_useraccess_laporan->act_read == '-'){
			redirect(base_url().'dashboard');
		}

		$tanggal_filter = $this->input->post('tanggal_filter');
		if($tanggal_filter == ''){
			redirect(base_url().'laporan_hadiah');
			exit();
		}

		$tanggal_awal   = substr($tanggal_filter, 0, 10);
		$tanggal_akhir  = substr($tanggal_filter, 18, 27);

		// Ambil jumlah transaksi, total tunai, debit dan total uang masuk
		$data_uang_masuk = $this->hadiah_master->get_grand_total($tanggal_awal, $tanggal_akhir);
		if($data_uang_masuk){
			$jml_transaksi      = number_format($data_uang_masuk->jml_transaksi,'0', ',', '.');
			$grand_total_hadiah = '<span>Rp. <p class="pull-right">'.number_format($data_uang_masuk->grand_total_hadiah,'0', ',', '.').'</p></span>';
		}else{
			$jml_transaksi      = '0';
			$grand_total_hadiah = '0';
		}
		
		echo json_encode(array(
			'status'             => 1,
			"jml_transaksi"      => $jml_transaksi,
			"grand_total_hadiah" => $grand_total_hadiah
		));
	}

	public function ajax_verifikasi_hapus_transaksi()
	{
		$id_hadiah_m 	= $this->input->post('id_hadiah_m');
		if($id_hadiah_m == ''){
			redirect(base_url().'laporan_hadiah');
			exit();
		}

		// Cek user acces menu
		$id_pegawai             = $this->session->userdata('id_pegawai');
		$cek_useraccess         = $this->useraccess_pusat->cek_access($id_pegawai, '31');
		$cek_useraccess_laporan = $this->useraccess_pusat->cek_access($id_pegawai, '46');
		$data_master            = $this->hadiah_master->get_master($id_hadiah_m);
		if($data_master){
			if($data_master->status_hadiah == 'SELESAI'){
				if($cek_useraccess_laporan->act_delete == '0' or $cek_useraccess_laporan->act_delete == '-'){
					echo json_encode(array(
											"status" 		=> 0,
										  	"info_pesan" 	=> "Maaf anda tidak diberikan izin untuk menghapus transaksi hadiah,
																dengan status hadiah : SELESAI !",
											"pesan" 		=> "
																	No. Hadiah : ".$data_master->no_hadiah."
																	Nama Hadiah : ".$data_master->nama_hadiah."
																	Nama Customer : ".$data_master->nama_customer_pusat."
																	Jumlah Barang : ".$data_master->jumlah_barang."
																	Status Hadiah : ".$data_master->status_hadiah."
																",
											"tipe_pesan" 	=> "error"
									));
					exit();
				}
			}else{
				if($cek_useraccess->act_delete == '0' or $cek_useraccess->act_delete == '-'){
					echo json_encode(array(
											"status" 		=> 0,
										  	"info_pesan" 	=> "Maaf anda tidak diberikan izin untuk menghapus transaksi hadiah!",
											"pesan" 		=> "
																	No. Hadiah : ".$data_master->no_hadiah."
																	Nama Hadiah : ".$data_master->nama_hadiah."
																	Nama Customer : ".$data_master->nama_customer_pusat."
																	Jumlah Barang : ".$data_master->jumlah_barang."
																",
											"tipe_pesan" 	=> "error"
									));
					exit();
				}
			}
		}

		if($data_master){
			echo json_encode(array(
				'status' 	=> 1,
				'pesan' 	=> "
									<small>No. Hadiah : </small></br>
									<b class='text-danger'>$data_master->no_hadiah </b><br/></br>
									
									<small>Nama Hadiah : </small></br>
									<b class='text-danger'>$data_master->nama_hadiah </b><br/><br/>

									<small>Nama Customer : </small> <br/>	
									<b class='text-dark'>$data_master->nama_customer_pusat </b> </br><br/>

									<small>Jumlah Barang : </small> <br/>	
									<b class='text-danger'>$data_master->jumlah_barang </b> </br><br/>

									<small>Status Hadiah : </small> <br/>	
									<b class='text-dark'>$data_master->status_hadiah </b> </br><br/>

									<textarea name='keterangan_batal' id='keterangan_batal' class='form-control input-md' placeholder='Keterangan batal' style='resize: vertical;'></textarea>
								",

				'footer'	=> 	"<button onclick='hapus_transaksi($data_master->id_hadiah_m)' type='button' class='btn btn-primary 
								 waves-effect waves-light' data-dismiss='modal' autofocus>Iya, Hapus</button> 
								<button type='button' class='btn btn-default waves-effect' data-dismiss='modal'>Batal</button>"
			));
		}else{
			echo json_encode(array(
				'status' => 0,
				'pesan'  =>	"Barang tidak ditemukan",
				'footer' => "<button type='button' class='btn btn-default waves-effect' data-dismiss='modal'>Batal</button>"
			));
		}
	}

	public function ajax_hapus_transaksi()
	{
		if($this->input->is_ajax_request()){
			$id_hadiah_m 	= $this->input->post('id_hadiah_m');
			if($id_hadiah_m == ''){
				redirect(base_url().'laporan_hadiah');
				exit();
			}

			// Cek user acces menu
			$id_pegawai             = $this->session->userdata('id_pegawai');
			$cek_useraccess         = $this->useraccess_pusat->cek_access($id_pegawai, '31');
			$cek_useraccess_laporan = $this->useraccess_pusat->cek_access($id_pegawai, '46');
			$data_master            = $this->hadiah_master->get_master($id_hadiah_m);
			if($data_master){
				if($data_master->status_hadiah == 'SELESAI'){
					if($cek_useraccess_laporan->act_delete == '0' or $cek_useraccess_laporan->act_delete == '-'){
						echo json_encode(array(
												"status" 		=> 0,
											  	"info_pesan" 	=> "Maaf anda tidak diberikan izin untuk menghapus transaksi hadiah,
																	dengan status hadiah : SELESAI !",
												"pesan" 		=> "
																		No. Hadiah : ".$data_master->no_hadiah."
																		Nama Hadiah : ".$data_master->nama_hadiah."
																		Nama Customer : ".$data_master->nama_customer_pusat."
																		Jumlah Barang : ".$data_master->jumlah_barang."
																		Status Hadiah : ".$data_master->status_hadiah."
																	",
												"tipe_pesan" 	=> "error",
												"gaya_tombol" 	=> "btn-danger btn-md waves-effect waves-light"
										));
						exit();
					}
				}else{
					if($cek_useraccess->act_delete == '0' or $cek_useraccess->act_delete == '-'){
						echo json_encode(array(
												"status" 		=> 0,
											  	"info_pesan" 	=> "Maaf anda tidak diberikan izin untuk menghapus transaksi hadiah!",
												"pesan" 		=> "
																		No. Hadiah : ".$data_master->no_hadiah."
																		Nama Hadiah : ".$data_master->nama_hadiah."
																		Nama Customer : ".$data_master->nama_customer_pusat."
																		Jumlah Barang : ".$data_master->jumlah_barang."
																	",
												"tipe_pesan" 	=> "error",
												"gaya_tombol"   => "btn-danger btn-md waves-effect waves-light"
										));
						exit();
					}
				}

				$id_customer_pusat   = $data_master->id_customer_pusat;
				$nama_customer_pusat = $data_master->nama_customer_pusat;
				$no_hadiah           = $data_master->no_hadiah;
				$nama_hadiah         = $data_master->nama_hadiah;
				$keterangan_batal    = $this->input->post('keterangan_batal');
				
				$hapus               = $this->hadiah_master->hapus_transaksi(
					$id_hadiah_m, $id_customer_pusat, $no_hadiah, $nama_hadiah, $keterangan_batal, $id_pegawai
				);

				if($hapus){
					echo json_encode(array(
						"status"      => 1,
						"no_hadiah"   => $no_hadiah,
						"info_pesan"  => "Berhasil!",
						"pesan"       => "Transaksi hadiah berhasil dihapus, 
										  dengan
										  No. Hadiah : ".$no_hadiah."
										  Nama Hadiah : ".$nama_hadiah."
										  Nama Customer : ".$nama_customer_pusat."
										  ",
						"tipe_pesan"  => "success",
						"gaya_tombol" => "btn-success btn-md waves-effect waves-light"
					));
				}else{
					echo json_encode(array(
						"status"      => 0,
						"info_pesan"  => "Oops!",
						"pesan"       => "Terjadi kesalahan saat menghapus transaksi hadiah, coba lagi!",
						"tipe_pesan"  => "error",
						"gaya_tombol" => "btn-danger btn-md waves-effect waves-light"
					));
				}
			}else{
				echo json_encode(array(
					"status"      => 0,
					"info_pesan"  => "Oops!",
					"pesan"       => "Data master hadiah tidak ditemukan!",
					"tipe_pesan"  => "error",
					"gaya_tombol" => "btn-danger btn-md waves-effect waves-light"
				));
			}
		}else{
			redirect(base_url().'laporan_hadiah');
		}
	}	
}
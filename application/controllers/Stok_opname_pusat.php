<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Stok_opname_pusat extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('pegawai_pusat_model','pegawai_pusat');
        $this->load->model('useraccess_pusat_model','useraccess_pusat');
		$this->load->model('periode_stok_pusat_model','periode_stok_pusat');
		$this->load->model('stok_opname_pusat_model','stok_opname_pusat');
		$this->load->model('barang_pusat_model','barang_pusat');
	}

	public function index()
	{
		// Cek user acces menu
		$id_pegawai = $this->session->userdata('id_pegawai');
		if($id_pegawai == ''){
			redirect(base_url().'login');
		}
		
		$cek_useraccess = $this->useraccess_pusat->cek_access($id_pegawai, '23');
		if($cek_useraccess){
			if($cek_useraccess->act_read == '0' or $cek_useraccess->act_read == '-'){
				redirect(base_url().'dashboard');
			}					
		}else{
			redirect(base_url().'dashboard');
		}

		$data_pegawai = $this->pegawai_pusat->get_by_id($id_pegawai);
		if($data_pegawai){
			$cek_useraccess_laporan  = $this->useraccess_pusat->cek_access($id_pegawai, '34');
			$data['access_create']   = $cek_useraccess->act_create;
			$data['access_update']   = $cek_useraccess->act_update;		
			$data['access_laporan']  = $cek_useraccess_laporan->act_read;
			$data['data_pegawai']    = $data_pegawai;
			$data['data_induk_menu'] = $this->useraccess_pusat->get_induk_menu($id_pegawai);
			$data['atribut_halaman'] = 'Stok Opname Pusat';

			// Ambil kode dan id periode stok terbaru
			$data_periode = $this->periode_stok_pusat->akhir();
			if($data_periode){
				$id_periode_stok_pusat   = $data_periode->id_periode_stok_pusat;
				$kode_periode_stok_pusat = $data_periode->kode_periode_stok_pusat;
				$tanggal_periode_awal    = $data_periode->tanggal_periode_awal;
				$tanggal_periode_akhir   = $data_periode->tanggal_periode_akhir;
			}else{
				$id_periode_stok_pusat   = '-';
				$kode_periode_stok_pusat = '-';
				$tanggal_periode_awal    = '-';
				$tanggal_periode_akhir   = '-';
			}

			$data['id_periode_stok_pusat']   = $id_periode_stok_pusat;
			$data['kode_periode_stok_pusat'] = $kode_periode_stok_pusat;
			$data['tanggal_periode_awal']    = $tanggal_periode_awal;
			$data['tanggal_periode_akhir']   = $tanggal_periode_akhir;
			$data['halaman_transaksi']       = $this->load->view('admin/transaksi/stok_opname_pusat/transaksi',$data,true);
			$data['halaman_plugin']          = $this->load->view('admin/transaksi/stok_opname_pusat/plugin',$data,true);
			$data['isi_halaman']             = $this->load->view('admin/transaksi/stok_opname_pusat/index',$data,true);
			$this->load->view('admin/layout',$data);
		}else{
			redirect(base_url().'login');
		}
	}

	public function ajax_list()
	{
		// Cek user acces menu
		$id_pegawai 	= $this->session->userdata('id_pegawai');
		$cek_useraccess = $this->useraccess_pusat->cek_access($id_pegawai, '23');
		if($cek_useraccess->act_read == '0' or $cek_useraccess->act_read == '0'){
			redirect(base_url().'dashboard');
			exit();
		}

		$id_periode_stok_pusat = $this->input->post('id_periode_stok_pusat');
		if($id_periode_stok_pusat == ''){
			redirect(base_url().'stok_opname_pusat');
			exit();
		}

		$data_periode 				= $this->periode_stok_pusat->akhir();
		if($data_periode){
			$id_periode_stok_pusat 	= $data_periode->id_periode_stok_pusat;
		}

		$list 	= $this->stok_opname_pusat->get_datatables($id_periode_stok_pusat);
		$data 	= array();
		$no 	= $_POST['start'];
		foreach ($list as $stok_opname_pusat){
			$no++;
			$row = array();

			$row[] = $no;

			//Tombol
			if($cek_useraccess->act_update == 1){
				$tombol_edit 	 = '<a class="btn btn-rounded btn-default btn-xs" href="javascript:void(0)" title="Edit" 
										onclick="edit_stok_opname_pusat('."'".$stok_opname_pusat->id_stok_opname_pusat."'".')">
										<i class="fa fa-pencil" style="color:blue;"></i>
									</a>';
			}else{$tombol_edit 	 = '';}

			if($cek_useraccess->act_delete == 1){
				$tombol_hapus 	 = '<a class="btn btn-rounded btn-default btn-xs" href="javascript:void(0)" title="Hapus" 
										onclick="verifikasi_hapus('."'".$stok_opname_pusat->id_stok_opname_pusat."'".')">
				  						<i class="fa fa-times" style="color:red;"></i>
				  					</a>';
			}else{$tombol_hapus  = '';}

			if($tombol_edit == '' AND $tombol_hapus == ''){
				$tombol_keterangan = '<b class="btn btn-rounded btn-default btn-xs text-muted text-center">Tidak ada akses</b>';
			}else{
				$tombol_keterangan = '';
			}

			$row[] 	= '	'.$tombol_edit.'
						'.$tombol_hapus.'
						'.$tombol_keterangan.'
				  	  ';
			
			if($stok_opname_pusat->jumlah_so == $stok_opname_pusat->jumlah_so_sebelumnya AND 
			   $stok_opname_pusat->tanggal_pembuatan == $stok_opname_pusat->tanggal_pembaharuan){
				$warna_text = 'text-danger';	
			}else{
				$warna_text = 'text-success';
			}

			$row[]	= '<span class="'.$warna_text.'">'.$stok_opname_pusat->sku.'</span>';
			$row[]	= '<span class="'.$warna_text.'">'.$stok_opname_pusat->nama_barang.'</span>';
			$row[] 	= '<span class="'.$warna_text.' pull-right">'.number_format($stok_opname_pusat->harga_eceran,'0',',','.').'</span>';
			$row[] 	= '<span class="'.$warna_text.' pull-right">'.number_format($stok_opname_pusat->jumlah_so_sebelumnya,'0',',','.').'</span>';
			$row[] 	= '<span class="'.$warna_text.' pull-right">'.number_format($stok_opname_pusat->jumlah_so,'0',',','.').'</span>';
			$row[] 	= '<span class="'.$warna_text.'">'.$stok_opname_pusat->masuk_stok.'</span>';
			$row[] 	= '<span class="'.$warna_text.'">'.$stok_opname_pusat->catatan.'</span>';
			$row[] 	= '<span class="'.$warna_text.'">'.$stok_opname_pusat->pegawai_save.'</span>';
			$row[] 	= '<span class="'.$warna_text.'">'.$stok_opname_pusat->tanggal_pembuatan.'</span>';
			$row[] 	= '<span class="'.$warna_text.'">'.$stok_opname_pusat->pegawai_edit.'</span>';
			$row[] 	= '<span class="'.$warna_text.'">'.$stok_opname_pusat->tanggal_pembaharuan.'</span>';
			$data[] = $row;
		}

		$output = array(
			"draw"            => $_POST['draw'],
			"recordsTotal"    => $this->stok_opname_pusat->count_all('stok_opname_pusat'),
			"recordsFiltered" => $this->stok_opname_pusat->count_filtered('_get_datatables_query', $id_periode_stok_pusat),
			"data"            => $data
		);
		echo json_encode($output);
	}

	public function ajax_list_riwayat()
	{
		// Cek user acces menu
		$id_pegawai 	= $this->session->userdata('id_pegawai');
		$cek_useraccess = $this->useraccess_pusat->cek_access($id_pegawai, '23');
		if($cek_useraccess->act_read == '0' or $cek_useraccess->act_read == '0'){
			redirect(base_url().'dashboard');
			exit();
		}

		$id_periode_stok_pusat = $this->input->post('id_periode_stok_pusat');
		if($id_periode_stok_pusat == ''){
			redirect(base_url().'stok_opname_pusat');
			exit();
		}

		$data_periode 				= $this->periode_stok_pusat->akhir();
		if($data_periode){
			$id_periode_stok_pusat 	= $data_periode->id_periode_stok_pusat;
		}

		$list = $this->stok_opname_pusat->get_datatables_riwayat($id_periode_stok_pusat);
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $stok_opname_pusat) {
			$no++;
			$row = array();

			$row[] = $no;
			$row[] = $stok_opname_pusat->sku;
			$row[] = $stok_opname_pusat->nama_barang;
			$row[] = number_format($stok_opname_pusat->jumlah_so_sebelumnya,'0',',','.');
			$row[] = number_format($stok_opname_pusat->jumlah_so,'0',',','.');
			$row[] = $stok_opname_pusat->masuk_stok;
			$row[] = $stok_opname_pusat->catatan;
			$row[] = $stok_opname_pusat->nama_pegawai;
			$row[] = $stok_opname_pusat->tanggal_pembuatan;
		
			$data[] = $row;
		}

		$output = array(
							"draw" 				=> $_POST['draw'],
							"recordsTotal" 		=> $this->stok_opname_pusat->count_all('riwayat_stok_opname_pusat'),
							"recordsFiltered" 	=> $this->stok_opname_pusat->count_filtered('_get_datatables_query_riwayat_so', $id_periode_stok_pusat),
							"data" 				=> $data,
						);

		//output to json format
		echo json_encode($output);
	}	

	public function ajax_list_batal()
	{
		// Cek user acces menu
		$id_pegawai 	= $this->session->userdata('id_pegawai');
		$cek_useraccess = $this->useraccess_pusat->cek_access($id_pegawai, '23');
		if($cek_useraccess->act_read == '0' or $cek_useraccess->act_read == '0'){
			redirect(base_url().'dashboard');
			exit();
		}

		$id_periode_stok_pusat = $this->input->post('id_periode_stok_pusat');
		if($id_periode_stok_pusat == ''){
			redirect(base_url().'stok_opname_pusat');
			exit();
		}

		$data_periode 				= $this->periode_stok_pusat->akhir();
		if($data_periode){
			$id_periode_stok_pusat 	= $data_periode->id_periode_stok_pusat;
		}

		$list = $this->stok_opname_pusat->get_datatables_batal($id_periode_stok_pusat);
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $stok_opname_pusat) {
			$no++;
			$row    = array();
			$row[]  = $no;
			$row[]  = $stok_opname_pusat->sku;
			$row[]  = $stok_opname_pusat->nama_barang;
			$row[]  = number_format($stok_opname_pusat->jumlah_so,'0',',','.');
			$row[]  = $stok_opname_pusat->masuk_stok;
			$row[]  = $stok_opname_pusat->keterangan_batal;
			$row[]  = $stok_opname_pusat->pegawai_batal;
			$row[]  = $stok_opname_pusat->tanggal_pembatalan;
			$row[]  = $stok_opname_pusat->pegawai_save;
			$row[]  = $stok_opname_pusat->tanggal_pembuatan;
			$row[]  = $stok_opname_pusat->pegawai_edit;
			$row[]  = $stok_opname_pusat->tanggal_pembaharuan;			
			$data[] = $row;
		}

		$output = array(
			"draw"            => $_POST['draw'],
			"recordsTotal"    => $this->stok_opname_pusat->count_all('stok_opname_pusat_batal'),
			"recordsFiltered" => $this->stok_opname_pusat->count_filtered('_get_datatables_query_batal_so', $id_periode_stok_pusat),
			"data"            => $data,
		);
		echo json_encode($output);
	}	

	public function ajax_kode()
	{
		if($this->input->is_ajax_request()){
			$data_periode = $this->periode_stok_pusat->akhir();
			if($data_periode){
				$id_periode_stok_pusat = $data_periode->id_periode_stok_pusat;
			}else{
				$json['status'] = 0;
			}

			$keyword = $this->input->post('keyword');
			$barang  = $this->barang_pusat->cari_kode_stok_opname($keyword, $id_periode_stok_pusat);				

			if($barang->num_rows() > 0){
				$json['status'] 	= 1;
				$json['datanya'] 	= "<ul id='daftar-autocomplete' class='list-group user-list'>";
				
				foreach($barang->result() as $b){
					if($b->foto!=""){
						$json['datanya'] .= "
			                <li class='user-list-item'>
			                	<div class='avatar avatar-sm-box'>
                                    <img src='assets/upload/image/barang/thumbs/$b->foto'>
			                	</div>";
			        }else{
						$nama_barang 	  = $b->nama_barang;
						$inisial 		  = substr($nama_barang, 0, 1);
						$json['datanya'] .= "
			                <li class='user-list-item'>
			                	<div class='avatar avatar-sm-box'>
                                    <span class='avatar-sm-box bg-info'>".$inisial."</span>
			                	</div>";
					}

					$json['datanya'] .= "
		                	<div class='user-desc'>
		                		<span id='id_barang' style='display: none;'>".$b->id_barang_pusat."</span>
								<span id='barangnya' class='name'><b>".$b->nama_barang."</b></span>
								<span id='skunya' class='name'>".$b->sku."</span>
								<span id='harga_eceran' class='name'>EC : Rp. ".$b->eceran."</span>
								<span id='eceran' style='display:none;'>".$b->harga_eceran."</span>
								<span id='jumlah_so_sebelumnya' style='display:none;'>".$b->jumlah_so."</span>
								<span id='status' style='display:none;'>".$b->status."</span>
							</div>
						</li>
					";
				}

				$json['datanya'] .= "</ul>";
			}
			else
			{
				$json['status'] = 0;
			}

			echo json_encode($json);
		}
	}

	public function ajax_cari_barang()
	{
		if($this->input->is_ajax_request()){
			$data_periode = $this->periode_stok_pusat->akhir();
			if($data_periode){
				$id_periode_stok_pusat = $data_periode->id_periode_stok_pusat;
			}else{
				$json['status'] = 0;
				$json['pesan']  = "Belum ada periode stok";
				exit();
			}

			$sku         = $this->input->post('sku');
			$data_barang = $this->stok_opname_pusat->get_by_sku($sku, $id_periode_stok_pusat);				

			if($data_barang){
				$json['status'] = 1;
				$json['data']   = $data_barang;
			}else{
				$json['status'] = 0;
				$json['pesan']  = "SKU : $sku, Tidak terdaftar";
				// $json['query']  =  $this->db->last_query();
			}

			echo json_encode($json);
		}else{
			redirect(base_url().'barang_pusat');
			exit();
		}
	}

	public function ajax_edit()
	{
		// Cek user acces menu
		$id_pegawai 	= $this->session->userdata('id_pegawai');
		$cek_useraccess = $this->useraccess_pusat->cek_access($id_pegawai, '23');
		if($cek_useraccess->act_update == '0' or $cek_useraccess->act_update == '-'){
			redirect(base_url().'stok_opname_pusat');
			exit();
		}

		$id_stok_opname_pusat = $this->input->post('id_stok_opname_pusat');
		if($id_stok_opname_pusat == ''){
			redirect(base_url().'stok_opname_pusat');
			exit();
		}else{
			$data = $this->stok_opname_pusat->get_by_id($id_stok_opname_pusat);
			echo json_encode($data);
		}
	}

	public function ajax_simpan()
	{
		$id_pegawai 	= $this->session->userdata('id_pegawai');
		$cek_useraccess = $this->useraccess_pusat->cek_access($id_pegawai, '23');

		$id_periode_stok_pusat = $this->input->post('id_periode_stok_pusat');
		if($id_periode_stok_pusat == ''){
			redirect(base_url().'stok_opname_pusat');
			exit();
		}

		$data_periode = $this->periode_stok_pusat->akhir();
		if($data_periode){
			$id_periode_stok_pusat = $data_periode->id_periode_stok_pusat;
		}

		$id_barang_pusat = $this->input->post('id_barang_pusat');
		$jumlah_so       = $this->input->post('jumlah_so');
		$masuk_stok      = $this->input->post('masuk_stok');
		$id_pegawai      = $this->session->userdata('id_pegawai');
		$catatan         = $this->input->post('catatan');

		// Validasi apakah data sudah ada atau belum
		$data_so = $this->stok_opname_pusat->get_by_id_barang($id_barang_pusat, $id_periode_stok_pusat, $masuk_stok);
		if($data_so){
			// Cek user access menu
			if($cek_useraccess->act_update == '0' or $cek_useraccess->act_update == '-'){
				echo json_encode(array(
					"status" 	=> 0,
				  	"pesan"   	=> "<b class='text-danger'>
				  						Maaf anda tidak di izinkan untuk mengedit stok opname
				  					</b>"
				));
				exit();
			}

			// Perbaharui stok opname dan Update stok 
			$id_stok_opname_pusat = $data_so->id_stok_opname_pusat;
			$jumlah_so_sebelumnya = $data_so->jumlah_so;
			$masuk_stok_lama      = $data_so->masuk_stok;

			if($masuk_stok == 'JUAL'){
				$stok_sekarang = $data_so->total_stok;
			}else if($masuk_stok == 'RUSAK'){
				$stok_sekarang = $data_so->total_stok_rusak;
			} 
			
			$validasi_stok1 = $stok_sekarang-$jumlah_so_sebelumnya;
			$validasi_stok2 = $validasi_stok1+$jumlah_so;

			// Validasi stok
			if($validasi_stok2 < 0){
				echo json_encode(array(
					"status" 	=> 0,
				  	"pesan"   	=> "<b class='text-danger'>
				  						Maaf anda tidak bisa memperbaharui stok opname dengan  jumlah : ".$jumlah_so."
				  					</b></br></br>
				  					SKU : </br>
				  					<b class='text-dark'>".$data_so->sku."</b></br></br>

				  					Nama barang : </br>
				  					<b class='text-dark'>".$data_so->nama_barang."</b></br></br>

				  					Stok : </br>
				  					<b class='text-dark'>".$data_so->masuk_stok."</b></br></br>

				  					<b class='text-dark'>Karna bisa menyebabkan minus sebanyak : </b></br>
				  					<h2 class='text-danger'>".$validasi_stok2."</h2>
				  					"
				));
				exit();
			}


			$this->_validate();
			$data = array(
				'id_barang_pusat'       => $id_barang_pusat,
				'jumlah_so_sebelumnya'  => $jumlah_so_sebelumnya,
				'jumlah_so'             => $jumlah_so,
				'masuk_stok'            => $masuk_stok_lama,
				'id_periode_stok_pusat' => $id_periode_stok_pusat,
				'catatan'               => $catatan,
				'id_pegawai'            => $id_pegawai,
				'tanggal_pembuatan'     => date('Y-m-d H:i:s')
			);
			$insert = $this->stok_opname_pusat->save($data,'vamr4846_vama.riwayat_stok_opname_pusat');

			$this->stok_opname_pusat->update_stok($id_barang_pusat, $jumlah_so, $jumlah_so_sebelumnya, $masuk_stok);
			$data = array(
				'jumlah_so_sebelumnya'   => $jumlah_so_sebelumnya,
				'jumlah_so'              => $jumlah_so,
				'catatan'                => $catatan,
				'id_pegawai_pembaharuan' => $id_pegawai
			);

			$this->stok_opname_pusat->update(array(
				'id_stok_opname_pusat' => $id_stok_opname_pusat
			), $data);
			
			echo json_encode(array(
				"status"     => 1,
				"masuk_stok" => $masuk_stok, 
				"fungsi"     => 'update'
			));

		}else{
			// Cek user acces menu
			if($cek_useraccess->act_create == '0' or $cek_useraccess->act_create == '-'){
				echo json_encode(array(
					"status" => 0,
					"pesan"  => "<b class='text-danger'>Maaf anda tidak di izinkan untuk menyimpan stok opname </b>"
				));
				exit();
			}

			// Simpan data stok opname dan Update stok
			$this->_validate();
			$data = array(
				'id_barang_pusat'       => $id_barang_pusat,
				'jumlah_so_sebelumnya'  => '0',
				'jumlah_so'             => $jumlah_so,
				'masuk_stok'            => $masuk_stok,
				'id_periode_stok_pusat' => $id_periode_stok_pusat,
				'catatan'               => $catatan,
				'id_pegawai'            => $id_pegawai,
				'tanggal_pembuatan'     => date('Y-m-d H:i:s')
			);
			$insert = $this->stok_opname_pusat->save($data,'vamr4846_vama.riwayat_stok_opname_pusat');

			$this->stok_opname_pusat->update_stok($id_barang_pusat, $jumlah_so, '0', $masuk_stok);
			$data = array(
				'id_barang_pusat'       => $id_barang_pusat,
				'jumlah_so'             => $jumlah_so,
				'masuk_stok'            => $masuk_stok,
				'id_periode_stok_pusat' => $id_periode_stok_pusat,
				'catatan'               => $catatan,
				'id_pegawai_pembuatan'  => $id_pegawai,
				'tanggal_pembuatan'     => date('Y-m-d H:i:s')
			);
			$insert = $this->stok_opname_pusat->save($data, 'vamr4846_vama.stok_opname_pusat');
			echo json_encode(array(
				"status"     => 1,
				"masuk_stok" => $masuk_stok, 
				"fungsi"     => 'save'
			));
		}
	}

	public function ajax_verifikasi_hapus()
	{
		// Cek user acces menu
		$id_pegawai 	= $this->session->userdata('id_pegawai');
		$cek_useraccess = $this->useraccess_pusat->cek_access($id_pegawai, '23');
		if($cek_useraccess->act_delete == '0' or $cek_useraccess->act_delete == '-'){
			redirect(base_url().'stok_opname_pusat');
			exit();
		}

		$id_stok_opname_pusat = $this->input->post('id_stok_opname_pusat');
		if($id_stok_opname_pusat == ''){
			redirect(base_url().'stok_opname_pusat');
			exit();
		}

		$data = $this->stok_opname_pusat->get_by_id($id_stok_opname_pusat);
		if($data){
			$jumlah_so_sebelumnya = $data->jumlah_so;
			$masuk_stok           = $data->masuk_stok;

			if($masuk_stok == "JUAL"){
				$stok_sekarang = $data->total_stok;
			}else if($masuk_stok == "RUSAK"){
				$stok_sekarang = $data->total_stok_rusak;
			}

			$validasi_stok1 = $stok_sekarang - $jumlah_so_sebelumnya;
			$validasi_stok2 = $validasi_stok1 + 0;

			// Validasi stok
			if($validasi_stok2 < 0){
				echo json_encode(array(
				  	'pesan'   	=> "<b class='text-danger'>
				  						Maaf anda tidak bisa menghapus stok opname ini !
				  					</b></br></br>
				  					SKU : </br>
				  					<b class='text-dark'>".$data->sku."</b></br></br>

				  					Nama barang : </br>
				  					<b class='text-dark'>".$data->nama_barang."</b></br></br>

				  					Stok : </br>
				  					<b class='text-dark'>".$data->masuk_stok."</b></br></br>

				  					<b class='text-dark'>Karna bisa menyebabkan minus sebanyak : </b></br>
				  					<h2 class='text-danger'>".$validasi_stok2."</h2>
				  					",

					'footer'	=> 	"<button type='button' class='btn btn-primary waves-effect' data-dismiss='modal'>Ok, saya mengerti</button>"
			));
			}else{
				echo json_encode(array(
					'pesan' 	=>	"
										<table class='table' id='detail_hapus'>
											<thead></thead>
											<tbody>
												<tr>
													<td>
														<small>SKU : </small>
														<b class='text-dark'>$data->sku </b><br/>
														
														<small>Jumlah SO : </small>
														<b class='text-dark'>$data->jumlah_so </b><br/><br/>

														<small>Nama Barang : </small> <br/>	
														<b class='text-dark'>$data->nama_barang </b> </br><br/>

														<textarea name='keterangan_batal' id='keterangan_batal' class='form-control input-md' placeholder='Keterangan batal' style='resize: vertical;'></textarea>
													</td>
												</tr>
											</tbody>
										</table>
								 	",

					'footer'	=> 	"
										<button onclick='hapus_so($data->id_stok_opname_pusat)' type='button' class='btn btn-primary waves-effect waves-light' data-dismiss='modal' autofocus>Iya, Hapus</button> 
										<button type='button' class='btn btn-default waves-effect' data-dismiss='modal'>Batal</button>

								   	"
			));
			}
		}else{
			echo json_encode(array(
				'pesan' 	=>	"
									<table class='table' id='detail_hapus'>
										<thead></thead>
										<tbody>
											<tr>
												<td>
													<b>Barang tidak ditemukan</b>
												</td>
											</tr>
										</tbody>
									</table>
							 	",

				'footer'	=> 	"<button type='button' class='btn btn-default waves-effect' data-dismiss='modal'>Batal</button>"
		));
		}
	}

	public function ajax_hapus()
	{
		// Cek user acces menu
		$id_pegawai 	= $this->session->userdata('id_pegawai');
		$cek_useraccess = $this->useraccess_pusat->cek_access($id_pegawai, '23');
		if($cek_useraccess->act_delete == '0' or $cek_useraccess->act_delete == '-'){
			redirect(base_url().'stok_opname_pusat');
			exit();
		}

		$id_stok_opname_pusat = $this->input->post('id_stok_opname_pusat');
		if($id_stok_opname_pusat == ''){
			redirect(base_url().'stok_opname_pusat');
			exit();
		}

		$data = $this->stok_opname_pusat->get_by_id($id_stok_opname_pusat);
		if($data){
			$id_barang_pusat = $data->id_barang_pusat;
			$jumlah_so       = $data->jumlah_so;
			$masuk_stok      = $data->masuk_stok;
			
			if($masuk_stok == "JUAL"){
				$stok_sekarang = $data->total_stok;
			}else if($masuk_stok == "RUSAK"){
				$stok_sekarang = $data->total_stok_rusak;
			}

			$validasi_stok1 = $stok_sekarang - $jumlah_so;
			$validasi_stok2 = $validasi_stok1 + 0;

			// Validasi stok
			if($validasi_stok2 < 0){
				echo json_encode(array(
				  	'pesan'   	=> "<b class='text-danger'>
				  						Maaf anda tidak bisa menghapus stok opname ini !
				  					</b></br></br>
				  					SKU : </br>
				  					<b class='text-dark'>".$data->sku."</b></br></br>

				  					Nama barang : </br>
				  					<b class='text-dark'>".$data->nama_barang."</b></br></br>

				  					Stok : </br>
				  					<b class='text-dark'>".$data->masuk_stok."</b></br></br>

				  					<b class='text-dark'>Karna bisa menyebabkan minus sebanyak : </b></br>
				  					<h2 class='text-danger'>".$validasi_stok2."</h2>
				  					",

					'footer'	=> 	"<button type='button' class='btn btn-primary waves-effect' data-dismiss='modal'>
										Ok, saya mengerti
									</button>"
				));
			}else{
				// Simpan so ke table so batal
				$data = array(
					'id_stok_opname_pusat'   => $data->id_stok_opname_pusat,
					'id_barang_pusat'        => $id_barang_pusat,
					'jumlah_so_sebelumnya'   => $data->jumlah_so_sebelumnya,
					'jumlah_so'              => $jumlah_so,
					'masuk_stok'             => $masuk_stok,
					'id_periode_stok_pusat'  => $data->id_periode_stok_pusat,
					'catatan'                => $data->catatan,
					'id_pegawai_pembuatan'   => $data->id_pegawai_pembuatan,
					'id_pegawai_pembaharuan' => $data->id_pegawai_pembaharuan,
					'id_pegawai_pembatalan'  => $this->session->userdata('id_pegawai'),
					'tanggal_pembuatan'      => $data->tanggal_pembuatan,
					'tanggal_pembaharuan'    => $data->tanggal_pembaharuan,
					'tanggal_pembatalan'     => date('Y-m-d H:i:s'),
					'keterangan_batal'       => $this->input->post('keterangan_batal')
				);

				$insert = $this->stok_opname_pusat->save($data,'vamr4846_vama.stok_opname_pusat_batal');
				$this->stok_opname_pusat->delete_by_id($id_stok_opname_pusat, $id_barang_pusat, $jumlah_so, $masuk_stok);
				echo json_encode(array("status" => 1));
			}
		}
	}

	private function _validate()
	{
		$data = array();
		$data['error_string'] = array();
		$data['inputerror'] = array();
		$data['status'] = TRUE;

		if($this->input->post('id_barang_pusat') == ''){
			$data['inputerror'][] 	= 'pencarian_kode';
			$data['error_string'][] = 'Kode barang wajib di diisi';
			$data['status'] 		= FALSE;
		}

		if($this->input->post('jumlah_so') == ''){
			$data['inputerror'][] 	= 'jumlah_so';
			$data['error_string'][] = 'Qty wajib diisi';
			$data['status'] 		= FALSE;
		}

		if($data['status'] === FALSE){
			echo json_encode($data);
			exit();
		}
	}

}

<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Retur_barang_dari_toko extends MY_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->CI =& get_instance();
		$this->load->model('pegawai_pusat_model','pegawai_pusat');
        $this->load->model('useraccess_pusat_model','useraccess_pusat');
		$this->load->model('customer_pusat_model','customer_pusat');
		$this->load->model('barang_pusat_model','barang_pusat');
		$this->load->model('retur_barang_dari_toko_master_model','retur_barang_dari_toko_master');
		$this->load->model('retur_barang_dari_toko_detail_model','retur_barang_dari_toko_detail');
	}

	public function index()
	{
		// Cek user acces menu
		$id_pegawai = $this->session->userdata('id_pegawai');
		if($id_pegawai == ''){
			redirect(base_url().'login');
		}
		
		$cek_useraccess = $this->useraccess_pusat->cek_access($id_pegawai, '51');
		if($cek_useraccess){
			if($cek_useraccess->act_read == '0' or $cek_useraccess->act_read == '-'){
				redirect(base_url().'dashboard');
			}					
		}else{
			redirect(base_url().'dashboard');
		}

		$data_pegawai = $this->pegawai_pusat->get_by_id($id_pegawai);
		if($data_pegawai){
			$cek_useraccess_laporan              = $this->useraccess_pusat->cek_access($id_pegawai, '52');
			// Ambil daftar toko
			$daftar_toko                         = $this->customer_pusat->listing_toko();
			$data['data_toko']                   = $daftar_toko;
			
			$data['id_retur_pembelian_m']        = '';
			$data['no_retur_pembelian']          = '';
			$data['data_pegawai']                = $data_pegawai;
			$data['data_induk_menu']             = $this->useraccess_pusat->get_induk_menu($id_pegawai);
			$data['access_create']               = $cek_useraccess->act_create;
			$data['access_update']               = $cek_useraccess->act_update;
			$data['access_laporan']              = $cek_useraccess_laporan->act_read;
			$data['atribut_halaman']             = 'Retur Barang Dari Toko';
			$data['id_retur_barang_dari_toko_m'] = '0';
			$data['halaman_transaksi']           = $this->load->view('admin/transaksi/retur_barang_dari_toko/transaksi',$data,true);
			$data['halaman_plugin']              = $this->load->view('admin/transaksi/retur_barang_dari_toko/plugin',$data,true);
			$data['isi_halaman']                 = $this->load->view('admin/transaksi/retur_barang_dari_toko/index',$data,true);
			$this->load->view('admin/layout',$data);
		}else{
			redirect(base_url().'login');
		}
	}

	public function ajax_cari_no_penjualan()
	{
		if($this->input->is_ajax_request()){
			// Cek user acces menu
			$id_pegawai     = $this->session->userdata('id_pegawai');
			$cek_useraccess = $this->useraccess_pusat->cek_access($id_pegawai, '51');
			if($cek_useraccess->act_create == '1'){
				$no_penjualan   = $this->input->post('no_penjualan');
				$data_penjualan = $this->penjualan_master->get_by_no_penjualan($no_penjualan);
				if($data_penjualan){
					// Validasi id_penjualan_m tidak kosong
					if($data_penjualan->id_penjualan_m !== null){
						// Validasi jika sudah ada retur
						if($data_penjualan->id_retur_barang_dari_toko_m <> ''){
							$json['status'] = 0;
							$json['pesan']  = "Maaf no. penjualan ini sudah di retur,
											   No. Penjualan : $no_penjualan,
											   No. Retur Penjualan : $data_penjualan->no_retur_barang_dari_toko
											   Total Retur : Rp. ".number_format($data_penjualan->total_retur,'0',',','.')."
										   	   ";
						}else{
							if($data_penjualan->status_penjualan !== 'SELESAI'){
								$json['status'] = 0;
								$json['pesan']  = "Maaf no. penjualan ini masih dalam status : ".$data_penjualan->status_penjualan.",
												   No. Penjualan : $no_penjualan,
												   Total Penjualan : Rp. ".number_format($data_penjualan->grand_total,'0',',','.')."
											   	   ";
							}else{
								$json['status'] = 1;
								$json['data']   = $data_penjualan;
							}
						}
					}else{
						$json['status'] = 0;
						$json['pesan']  = "No. Penjualan : $no_penjualan
										   Tidak terdaftar.";	
					}
				}else{
					$json['status'] = 0;
					$json['pesan']  = "No. Penjualan : $no_penjualan
									   Tidak terdaftar.";
				}
			}else{
				$json['status'] = 0;
				$json['pesan']  = "Maaf anda tidak diizinkan untuk menambahkan data retur penjualan.";
			}
			echo json_encode($json);
		}else{
			redirect(base_url().'retur_barang_dari_toko');
		}
	}

	public function transaksi()
	{
		$id_retur_barang_dari_toko_m = $this->input->get('id');
		$kode_toko                   = $this->input->get('kd');
		$data_retur_barang_dari_toko = $this->retur_barang_dari_toko_master->get_master1($id_retur_barang_dari_toko_m, $kode_toko);
		if($data_retur_barang_dari_toko->id_retur_pembelian_m == ''){
			redirect(base_url().'retur_barang_dari_toko');
			exit();
		}

		// Cek user acces menu
		$id_pegawai             = $this->session->userdata('id_pegawai');
		$cek_useraccess_laporan = $this->useraccess_pusat->cek_access($id_pegawai, '52');
		$cek_useraccess         = $this->useraccess_pusat->cek_access($id_pegawai, '51');

		if($cek_useraccess->act_create == '1' or $data_retur_barang_dari_toko->status_retur == 'DIKIRIM'){
			$data['access_laporan'] = $cek_useraccess_laporan->act_read;
			$data['access_create']  = $cek_useraccess->act_create;
			$data['access_update']  = $cek_useraccess->act_update;
			
			// cari data pegawai
			$id_pegawai   = $this->session->userdata('id_pegawai');
			$data_pegawai = $this->pegawai_pusat->get_by_id($id_pegawai);
			$data_retur   = $this->retur_barang_dari_toko_master->get_by_id_retur($id_retur_barang_dari_toko_m, $kode_toko);
			if($data_retur){
				// Ambil daftar toko
				$daftar_toko                  = $this->customer_pusat->listing_toko();
				$data['data_toko']            = $daftar_toko;
				$data['data_pegawai']         = $data_pegawai;
				$data['data_induk_menu']      = $this->useraccess_pusat->get_induk_menu($id_pegawai);
				$data['atribut_halaman']      = 'Retur Barang Dari Toko';
				$data['kode_toko']            = $kode_toko;
				$data['id_retur_pembelian_m'] = $data_retur->id_retur_pembelian_m;
				$data['no_retur_pembelian']   = $data_retur->no_retur_pembelian;
				$data['halaman_transaksi']    = $this->load->view('admin/transaksi/retur_barang_dari_toko/transaksi',$data,true);
				$data['halaman_plugin']       = $this->load->view('admin/transaksi/retur_barang_dari_toko/plugin',$data,true);
				$data['isi_halaman']          = $this->load->view('admin/transaksi/retur_barang_dari_toko/index',$data,true);
				$data['url']                  = 'retur_barang_dari_toko/transaksi/?&id='.$id_retur_barang_dari_toko_m.'&kd='.$kode_toko;
				$this->load->view('admin/layout',$data);
			}else{
				// $data['status']               = 0;
				// $data['kode_toko']            = $kode_toko;
				// $data['id_retur_pembelian_m'] = $id_retur_barang_dari_toko_m;
				// echo json_encode($data);
				redirect(base_url().'retur_barang_dari_toko');
				exit();
			}
		}else{
			redirect(base_url().'retur_barang_dari_toko');
			exit();
		}
	}

	public function simpan_transaksi(){
		if( ! empty($_POST['id_retur_pembelian_m'])){
			// Cek user acces menu
			$id_pegawai     = $this->session->userdata('id_pegawai');
			$cek_useraccess = $this->useraccess_pusat->cek_access($id_pegawai, '51');

			if($cek_useraccess->act_create == '1' or $cek_useraccess->act_update == '1'){
				$id_retur_pembelian_m = $this->input->post('id_retur_pembelian_m');
				$no_retur_pembelian   = $this->input->post('no_retur_pembelian');
				$kode_toko            = $this->input->post('kode_toko');
				$tanggal              = date('Y-m-d H:i:s');
				$id_pegawai           = $this->session->userdata('id_pegawai');

				// Validasi retur penjualan
				$data_retur = $this->retur_barang_dari_toko_master->get_by_id($id_retur_pembelian_m, $kode_toko);
				if($data_retur){
					if($data_retur->status_retur == 'SELESAI'){
						$data['status'] = 1;
						$data['pesan']  = "Transaksi berhasil disimpan,
										   No. Retur Barang Toko : ".$no_retur_pembelian.".";
						echo json_encode($data);
					}else{
						if($data_retur->jml_barang_retur == $data_retur->jml_barang_masuk){
							$master = $this->retur_barang_dari_toko_master->update_master(
								$id_retur_pembelian_m, $kode_toko, $id_pegawai, 'SELESAI'
							);

							if($master){
								$data['status'] = 1;
								$data['pesan']  = "Transaksi berhasil disimpan,
												   No. Retur Barang Toko : ".$no_retur_pembelian.".";
								echo json_encode($data);
							}else{
								$data['status'] = 0;
								$data['pesan']  = "Terjadi kesalahan saat menyimpan transaksi, harap coba kembali.";
								$data['url']    = 'retur_barang_dari_toko/transaksi/?&id='.$id_retur_pembelian_m.'&kd='.$kode_toko;
								echo json_encode($data);
							}
						}else{
							$data['status'] = 0;
							$data['pesan']  = "Masih ada barang retur yang belum di setuji/approve.";
							$data['url']    = 'retur_barang_dari_toko/transaksi/?&id='.$id_retur_pembelian_m.'&kd='.$kode_toko;
							echo json_encode($data);
						}
					}
				}else{
					$data['status'] = 0;
					$data['pesan']  = "No. retur barang dari toko tidak terdaftar, harap hubungi administrator.";
					$data['url']    = 'retur_barang_dari_toko/transaksi/?&id='.$id_retur_pembelian_m.'&kd='.$kode_toko;
					echo json_encode($data);
				}
			}else{
				$data['status'] = 0;
				$data['pesan']  = "Maaf anda tidak diizinkan untuk menyimpan transaksi retur barang dari toko.";
				$data['url']    = 'retur_barang_dari_toko/transaksi/?&id='.$id_retur_pembelian_m.'&kd='.$kode_toko;
				echo json_encode($data);
			}
		}else{
			redirect(base_url().'retur_barang_dari_toko');
		}
	}

	public function tahan_transaksi(){
		if( ! empty($_POST['id_retur_pembelian_m'])){
			// Cek user acces menu
			$id_pegawai     = $this->session->userdata('id_pegawai');
			$cek_useraccess = $this->useraccess_pusat->cek_access($id_pegawai, '51');

			if($cek_useraccess->act_create == '1' or $cek_useraccess->act_update == '1'){
				$id_retur_pembelian_m = $this->input->post('id_retur_pembelian_m');
				$no_retur_pembelian   = $this->input->post('no_retur_pembelian');
				$kode_toko            = $this->input->post('kode_toko');
				$tanggal              = date('Y-m-d H:i:s');
				$id_pegawai           = $this->session->userdata('id_pegawai');

				// Validasi status retur penjualan
				$data_retur = $this->retur_barang_dari_toko_master->get_by_id($id_retur_pembelian_m, $kode_toko);
				if($data_retur){
					if($data_retur->status_retur == 'DIKIRIM'){
						$master = $this->retur_barang_dari_toko_master->update_master(
							$id_retur_pembelian_m, $kode_toko, $id_pegawai, 'DIKIRIM'
						);
						if($master){
							$data['status'] = 1;
							$data['pesan']  = "Transaksi berhasil ditahan,
											   No. Retur Barang Dari Toko : ".$no_retur_pembelian."";
							echo json_encode($data);
						}else{
							$data['status'] = 0;
							$data['pesan']  = "Terjadi kesalahan saat menahan transaksi, harap coba kembali.";
							$data['url']    = 'retur_barang_dari_toko/transaksi/?&id='.$id_retur_pembelian_m.'&kd='.$kode_toko;
							echo json_encode($data);
						}
					}else{
						$data['status'] = 0;
						$data['pesan']  = "	Maaf transaksi dengan,
											No. Retur Barang Dari Toko : ".$data_retur->no_retur_pembelian.",
											tidak bisa ditahan,
											Karna status transaksi : ".$data_retur->status_retur."";
						$data['url']    = 'retur_barang_dari_toko/transaksi/?&id='.$id_retur_pembelian_m.'&kd='.$kode_toko;
						echo json_encode($data);
					}
				}else{
					$data['status'] = 0;
					$data['pesan']  = "No. retur barang dari toko tidak terdaftar, harap hubungi administrator.";
					$data['url']    = "retur_barang_dari_toko/transaksi/?&id=".$id_retur_pembelian_m;
					echo json_encode($data);
				}
			}else{
				$data['status'] = 0;
				$data['pesan']  = "Maaf anda tidak diizinkan untuk menyimpan transaksi retur barang dari toko.";
				$data['url']    = 'retur_barang_dari_toko/transaksi/?&id='.$id_retur_pembelian_m.'&kd='.$kode_toko;
				echo json_encode($data);
			}
		}else{
			redirect(base_url().'retur_barang_dari_toko');
		}
	}

	public function ambil_data()
	{
		$id_retur_barang_dari_toko_m = $this->input->post('id_retur_barang_dari_toko_m');
		$kode_toko                   = $this->input->post('kode_toko');
		if($id_retur_barang_dari_toko_m == '' or $kode_toko == ''){
			redirect(base_url().'retur_barang_dari_toko');
			exit();
		}

		// Cek user acces menu
		$id_pegawai     = $this->session->userdata('id_pegawai');
		$cek_useraccess = $this->useraccess_pusat->cek_access($id_pegawai, '51');
		if($cek_useraccess->act_read == '0' or $cek_useraccess->act_read == '-'){
			redirect(base_url().'retur_barang_dari_toko');
			exit();
		}

		$data_retur  = $this->retur_barang_dari_toko_master->get_jumlah_retur($id_retur_barang_dari_toko_m, $kode_toko);
		$data_induk  = $this->retur_barang_dari_toko_master->get_by_id_retur($id_retur_barang_dari_toko_m, $kode_toko);
		$data_batal  = $this->retur_barang_dari_toko_master->get_by_id_retur_batal($id_retur_barang_dari_toko_m, $kode_toko);

		echo json_encode(array(
			'jumlah_retur' => $data_retur->jml_barang,
			'data_induk'   => $data_induk,
			'data_batal'   => $data_batal, 
			'url'          => 'retur_barang_dari_toko/transaksi/?id='.$id_retur_barang_dari_toko_m.'&kd='.$kode_toko
		));
	}

	public function simpan_detail()
	{
		// Cek user acces menu
		$id_pegawai           = $this->session->userdata('id_pegawai');
		$cek_useraccess       = $this->useraccess_pusat->cek_access($id_pegawai, '51');
		
		$kode_toko            = $this->clean_tag_input($this->input->post('kode_toko'));
		$id_retur_pembelian_d = $this->clean_tag_input($this->input->post('id_retur_pembelian_d'));
		$id_retur_pembelian_m = $this->clean_tag_input($this->input->post('id_retur_pembelian_m'));
		$tanggal              = date('Y-m-d H:i:s');

		if($kode_toko == '' OR $id_retur_pembelian_d == '' OR $id_retur_pembelian_m == ''){
			redirect(base_url().'retur_barang_dari_toko');
			exit();
		}

		// Validasi user access
		if($cek_useraccess->act_create == '0' AND $cek_useraccess->act_update == '0'){
			echo json_encode(array(
				'status' => 0,
				'pesan'  => 'Maaf anda tidak diizinkan untuk menyimpan data retur barang dari toko.',
				'url'    => 'retur_barang_dari_toko/transaksi/?&id='.$id_retur_pembelian_m.'&kd='.$kode_toko
			));
			exit();
		}
		
		// Validasi retur pembelian detail
		$data_retur = $this->retur_barang_dari_toko_detail->get_retur_penjualan_detail(
			$id_retur_pembelian_d, $id_retur_pembelian_m, $kode_toko
		);

		if($data_retur){
			$this->retur_barang_dari_toko_detail->simpan_retur_masuk(
				$id_retur_pembelian_d, $id_retur_pembelian_m, $data_retur->no_retur_pembelian,
				$data_retur->id_barang_pusat, $data_retur->harga_satuan, $data_retur->jumlah_retur,
				$data_retur->subtotal, $data_retur->dari_stok, $data_retur->kondisi_barang, $data_retur->keterangan,
				$id_pegawai, $tanggal, $kode_toko
			);

			$data_retur  = $this->retur_barang_dari_toko_master->get_jumlah_retur($id_retur_pembelian_m, $kode_toko);
			$data_induk  = $this->retur_barang_dari_toko_master->get_by_id_retur($id_retur_pembelian_m, $kode_toko);
			$data_batal  = $this->retur_barang_dari_toko_master->get_by_id_retur_batal($id_retur_pembelian_m, $kode_toko);

			echo json_encode(array(
				'status' => 1,
				'jumlah_retur' => $data_retur->jml_barang,
				'jumlah_masuk' => $data_induk->jml_barang,
				'jumlah_batal' => $data_batal->jml_barang,
				'pesan'        => 'Data retur barang dari toko berhasil di simpan.',
				'url'          => 'retur_barang_dari_toko/transaksi/?id='.$id_retur_pembelian_m.'&kd='.$kode_toko
			));
		}else{
			echo json_encode(array(
				'status' => 0,
				'pesan'  => 'Data retur barang dari toko tidak terdaftar,
							 Harap hubungi administrator.',
				'url'    => 'retur_barang_dari_toko/transaksi/?id='.$id_retur_pembelian_m.'&kd='.$kode_toko
			));
		}
	}

	public function ajax_list_retur()
	{
		$kode_toko            = $this->clean_tag_input($this->input->post('kode_toko'));
		$id_retur_pembelian_m = $this->clean_tag_input($this->input->post('id_retur_pembelian_m'));

		if($kode_toko == '' OR $id_retur_pembelian_m == ''){
			redirect(base_url().'retur_barang_dari_toko');
			exit();
		}

		// Cek user acces menu
		$id_pegawai             = $this->session->userdata('id_pegawai');
		$cek_useraccess         = $this->useraccess_pusat->cek_access($id_pegawai, '51');
		$cek_useraccess_laporan = $this->useraccess_pusat->cek_access($id_pegawai, '52');
		if($cek_useraccess->act_read == '0' AND $cek_useraccess->act_read == '0'){
			redirect(base_url().'dashboard');
			exit();
		}

		$list = $this->retur_barang_dari_toko_detail->get_datatables('_get_datatables_query', $kode_toko, $id_retur_pembelian_m);
		$data = array();
		$no   = $_POST['start'];
		foreach ($list as $pd){
			$no++;
			$row   = array();
			$row[] = $no;

			if($pd->jumlah_masuk !=='0'){
				$warna_tulisan = "text-success";
				if($cek_useraccess->act_delete == 1){
					if($pd->status_retur == "SELESAI"){
						if($cek_useraccess_laporan->act_delete == 1){
							$tombol_hapus = '<a class="btn btn-rounded btn-default btn-xs"
									  			onclick="verifikasi_hapus_retur_detail(
									  				'."'".$pd->id_retur_pembelian_m."'".',
									  				'."'".$pd->id_retur_pembelian_d."'".',
									  				'."'".$kode_toko."'".'
									  			)">
									  			<i class="fa fa-times" style="color:red;"></i>
									  		</a>';
						}else{
							$tombol_hapus = '';
						}
					}else{
						$tombol_hapus = '<a class="btn btn-rounded btn-default btn-xs"
								  			onclick="verifikasi_hapus_retur_detail(
								  				'."'".$pd->id_retur_pembelian_m."'".',
												'."'".$pd->id_retur_pembelian_d."'".',
												'."'".$kode_toko."'".'
								  			)">
								  			<i class="fa fa-times" style="color:red;"></i>
								  		</a>';
					}
				}else{
					$tombol_hapus = '';
				}
				$tombol_edit = '';
			}else{
				$warna_tulisan = "text-danger";
				if($cek_useraccess->act_create == 1){
					$tombol_edit  =	'	<a class="btn btn-rounded btn-default btn-xs"
											onclick="simpan_retur_detail(
												'."'".$pd->id_retur_pembelian_m."'".',
												'."'".$pd->id_retur_pembelian_d."'".',
												'."'".$kode_toko."'".'
											)">
											<i class="fa fa-check" style="color: green;"></i>
										</a>
									';
				}else{
					$tombol_edit = '';
				}
				$tombol_hapus = '';
			}

			$row[] =	'	'.$tombol_edit.'
							'.$tombol_hapus.'
						';
			
			$row[]  = '<span class="pull-left '.$warna_tulisan.'">'.$pd->sku.'</span>';
			$row[]  = '<span class="pull-left '.$warna_tulisan.'">'.$pd->nama_barang.'</span>';
			$row[]  = '<span class="pull-right '.$warna_tulisan.'">'.number_format($pd->harga_eceran,'0',',','.').'</span>';
			$row[]  = '<span class="pull-left '.$warna_tulisan.'">'.$pd->dari_stok.'</span>';
			$row[]  = '<span class="pull-left '.$warna_tulisan.'">'.$pd->kondisi_barang.'</span>';
			$row[]  = '<span class="pull-right '.$warna_tulisan.'">'.number_format($pd->jumlah_retur,'0',',','.').'</span>';
			$row[]  = '<span class="pull-right '.$warna_tulisan.'">'.number_format($pd->jumlah_masuk,'0',',','.').'</span>';
			$row[]  = '<span class="'.$warna_tulisan.'">'.$pd->keterangan.'</span>';

			$row[]  = '<span class="'.$warna_tulisan.'">'.$pd->pegawai_save.'</span>';
			$row[]  = '<span class="'.$warna_tulisan.'">'.$pd->tanggal_pembuatan.'</span>';
			$row[]  = '<span class="'.$warna_tulisan.'">'.$pd->pegawai_edit.'</span>';
			$row[]  = '<span class="'.$warna_tulisan.'">'.$pd->tanggal_pembaharuan.'</span>';
			$row[]  = '<span class="'.$warna_tulisan.'">'.$pd->pegawai_masuk.'</span>';
			$row[]  = '<span class="'.$warna_tulisan.'">'.$pd->tanggal_masuk.'</span>';

			$data[] = $row;
		}

		$output['draw']            = $_POST['draw'];
		$output['recordsTotal']    = $this->retur_barang_dari_toko_detail->count_all('vamr4846_toko_mrc.retur_pembelian_detail_'.$kode_toko.'', $kode_toko, $id_retur_pembelian_m);
		$output['recordsFiltered'] = $this->retur_barang_dari_toko_detail->count_filtered('_get_datatables_query', $kode_toko, $id_retur_pembelian_m);
		$output['data']            = $data;
		echo json_encode($output);
	}

	public function ajax_list_retur_batal()
	{
		$kode_toko            = $this->clean_tag_input($this->input->post('kode_toko'));
		$id_retur_pembelian_m = $this->clean_tag_input($this->input->post('id_retur_pembelian_m'));

		if($kode_toko == '' OR $id_retur_pembelian_m == ''){
			redirect(base_url().'retur_barang_dari_toko');
			exit();
		}

		// Cek user acces menu
		$id_pegawai             = $this->session->userdata('id_pegawai');
		$cek_useraccess         = $this->useraccess_pusat->cek_access($id_pegawai, '51');
		$cek_useraccess_laporan = $this->useraccess_pusat->cek_access($id_pegawai, '52');
		if($cek_useraccess->act_read == '0' AND $cek_useraccess->act_read == '0'){
			redirect(base_url().'dashboard');
			exit();
		}

		$list = $this->retur_barang_dari_toko_detail->get_datatables('_get_datatables_query_batal', $kode_toko, $id_retur_pembelian_m);
		$data = array();
		$no   = $_POST['start'];
		foreach ($list as $pd){
			$no++;
			$row    = array();
			$row[]  = $no;
			
			$row[]  = $pd->sku;
			$row[]  = $pd->nama_barang;			
			$row[]  = '<span class="pull-right">'.number_format($pd->harga_satuan,'0',',','.').'</span>';
			$row[]  = $pd->keterangan_batal;
			$row[]  = $pd->dari_stok;
			$row[]  = $pd->kondisi_barang;
			$row[]  = '<span class="pull-right">'.number_format($pd->jumlah_retur,'0',',','.').'</span>';
			$row[]  = '<span class="pull-right">'.number_format($pd->jumlah_masuk,'0',',','.').'</span>';
			$row[]  = $pd->keterangan;
			$row[]  = $pd->pegawai_pembatalan;
			$row[]  = $pd->tanggal_pembatalan;
			$data[] = $row;
		}

		$output['draw']                        = $_POST['draw'];
		$output['recordsTotal']                = $this->retur_barang_dari_toko_detail->count_all('vamr4846_toko_mrc.retur_pembelian_detail_'.$kode_toko.'', $kode_toko, $id_retur_pembelian_m);
		$output['recordsFiltered']             = $this->retur_barang_dari_toko_detail->count_filtered('_get_datatables_query_batal', $kode_toko, $id_retur_pembelian_m);
		$output['data']                        = $data;
		echo json_encode($output);
	}

	public function ajax_edit_retur()
	{
		$id_penjualan_d = $this->input->post('id_penjualan_d');
		if($id_penjualan_d == ''){
			redirect(base_url()."retur_barang_dari_toko");
			exit();
		}

		// Cek user acces menu
		$id_pegawai                    = $this->session->userdata('id_pegawai');
		$cek_useraccess                = $this->useraccess_pusat->cek_access($id_pegawai, '51');
		if($cek_useraccess->act_create == '0' or $cek_useraccess->act_create == '-'){
			$json['status']            = '0';
			$json['pesan']             = 'Maaf anda tidak diizinkan untuk menambahkan data retur penjualan.';
			echo json_encode($json);
			exit();
		}

		$json['status'] = '1';
		$json['data']   = $this->retur_barang_dari_toko_detail->get_retur_barang_dari_toko_detail($id_penjualan_d);
		echo json_encode($json);
	}

	public function ajax_verifikasi_hapus_detail()
	{
		$id_retur_pembelian_m = $this->input->post('id_retur_pembelian_m');
		$id_retur_pembelian_d = $this->input->post('id_retur_pembelian_d');
		$kode_toko            = $this->input->post('kode_toko');
		if($id_retur_pembelian_m == '' or $kode_toko == ''){
			// redirect(base_url()."retur_barang_dari_toko");
			exit();
		}

		// Cek user acces menu
		$data = $this->retur_barang_dari_toko_detail->get_retur_penjualan_detail(
			$id_retur_pembelian_d, $id_retur_pembelian_m, $kode_toko
		);

		$id_pegawai             = $this->session->userdata('id_pegawai');
		$cek_useraccess         = $this->useraccess_pusat->cek_access($id_pegawai, '51');
		$cek_useraccess_laporan = $this->useraccess_pusat->cek_access($id_pegawai, '52');
		if(($cek_useraccess->act_delete == '0' or $cek_useraccess->act_delete == '-') AND 
			($data->status_retur == 'MENUNGGU' or $data->status_retur == 'DIKIRIM')){
			$json['status'] = '0';
			$json['pesan']  = 'Maaf anda tidak diizinkan untuk menghapus data retur penjualan.';
			echo json_encode($json);
			exit();
		}else if(($cek_useraccess_laporan->act_delete == '0' or $cek_useraccess_laporan->act_delete == '-') AND $data->status_retur == 'SELESAI'){
			$json['status'] = '0';
			$json['pesan']  = 'Maaf anda tidak diizinkan untuk menghapus data retur penjualan yang sudah selesai.';
			echo json_encode($json);
			exit();
		}

		if($data){
			// validasi stok berdasarkan isian masuk stok
			$validasi_stok = '0';
			if($data->kondisi_barang == "RUSAK"){
				$validasi_stok = $data->total_stok_rusak - $data->jumlah_retur; 
			}else if($data->kondisi_barang == "BAIK"){
				$validasi_stok = $data->total_stok - $data->jumlah_retur;
			}
		
			if($validasi_stok < 0){
				echo json_encode(array(
					'status'	=> 1,
					'pesan' 	=> "	<b class='text-danger'>
											Maaf anda tidak bisa mengapus data retur barang dari toko dengan jumlah : $data->jumlah_retur
										</b></br></br>

										<small>SKU : </small></br>
										<b class='text-dark'>$data->sku</b><br/></br>

										<small>Nama Barang : </small> <br/>
										<b class='text-dark'>$data->nama_barang </b> </br><br/>

										<small>Dari Stok : </small></br>
										<b class='text-dark'>$data->dari_stok </b><br/><br/>

										<small>Kondisi Barang : </small></br>
										<b class='text-dark'>$data->kondisi_barang </b><br/><br/>

										<small>Keterangan Retur : </small> <br/>
										<b class='text-danger'>$data->keterangan </b><br/><br/>

										<b class='text-dark'>Karna bisa menyebabkan minus sebanyak : </b></br>
										<h1 class='text-danger'>$validasi_stok</h1>
								 	",

					'footer'	=> 	"<button type='button' class='btn btn-danger waves-effect' data-dismiss='modal'>Ok, saya mengerti</button>"
				));
			}else{			
				echo json_encode(array(
					'status'	=> 1,
					'pesan' 	=> "	<small>SKU : </small></br>
										<b class='text-dark'>$data->sku</b><br/></br>

										<small>Nama Barang : </small> <br/>
										<b class='text-dark'>$data->nama_barang </b> </br><br/>

										<small>Dari Stok : </small></br>
										<b class='text-dark'>$data->dari_stok </b><br/><br/>

										<small>Kondisi Barang : </small></br>
										<b class='text-dark'>$data->kondisi_barang </b><br/><br/>

										<small>Keterangan Retur : </small> <br/>
										<b class='text-danger'>$data->keterangan </b><br/><br/>

										<small>Jumlah Retur : </small></br>
										<h1 class='text-danger'>$data->jumlah_retur </h1>
								 	",

					'footer'	=> 	"<button onclick='hapus_retur_barang_dari_toko_detail(
											".$data->id_retur_pembelian_m.", 
											".$data->id_retur_pembelian_d."
										)' 	
										type='button' class='btn btn-danger waves-effect waves-light' data-dismiss='modal' autofocus>
										Iya, Hapus
									</button>
									<button type='button' class='btn btn-default waves-effect' data-dismiss='modal'>Batal</button>"
				));
			}
		}else{
			echo json_encode(array(
				'status' 	=> 0,
				'pesan' 	=>	"Barang retur penjualan tidak ditemukan!",
			));
		}
	}

	public function ajax_hapus_detail()
	{
		if($this->input->is_ajax_request()){
			// Cek user acces menu
			$id_retur_pembelian_m = $this->input->post('id_retur_pembelian_m');
			$id_retur_pembelian_d = $this->input->post('id_retur_pembelian_d');
			$kode_toko            = $this->input->post('kode_toko');
			if($id_retur_pembelian_m == '' or $kode_toko == ''){
				// redirect(base_url()."retur_barang_dari_toko");
				$json['id_retur_pembelian_m'] = $id_retur_pembelian_m;
				$json['id_retur_pembelian_d'] = $id_retur_pembelian_d;
				$json['kode_toko']            = $kode_toko;
				echo json_encode($json);
				exit();
			}

			// Cek user acces menu
			$id_pegawai             = $this->session->userdata('id_pegawai');
			$cek_useraccess         = $this->useraccess_pusat->cek_access($id_pegawai, '51');
			$cek_useraccess_laporan = $this->useraccess_pusat->cek_access($id_pegawai, '52');

			if(($cek_useraccess->act_delete == '0' or $cek_useraccess->act_delete == '-') AND 
				($data->status_retur == 'MENUNGGU' or $data->status_retur == 'DIKIRIM')){
				$json['status']      = '0';
				$json['judul']       = 'Oops!';
				$json['pesan']       = 'Maaf anda tidak diizinkan untuk menghapus data retur barang dari toko.';
				$json['tipe_pesan']  = 'error';
				$json['gaya_tombol'] = 'btn-danger btn-md waves-effect waves-light';
				echo json_encode($json);
				exit();
			}else if(($cek_useraccess_laporan->act_delete == '0' or $cek_useraccess_laporan->act_delete == '-') AND $data->status_retur == 'SELESAI'){
				$json['status']      = '0';
				$json['judul']       = 'Oops!';
				$json['pesan']       = 'Maaf anda tidak diizinkan untuk menghapus data retur barang dari toko yang sudah selesai.';
				$json['tipe_pesan']  = 'error';
				$json['gaya_tombol'] = 'btn-danger btn-md waves-effect waves-light';
				echo json_encode($json);
				exit();
			}

			$hapus                 = '';
			$id_pegawai_pembatalan = $this->session->userdata('id_pegawai');
			$keterangan_batal      = $this->input->post('keterangan_batal');
			$tanggal               = date('Y-m-d H:i:s');
			$id_customer_pusat     = $this->input->post('id_customer_pusat');

			// Ambil data penjualan detail
			$dt = $this->retur_barang_dari_toko_detail->get_retur_penjualan_detail(
				$id_retur_pembelian_d, $id_retur_pembelian_m, $kode_toko
			);

			if($dt){
				// validasi stok berdasarkan isian masuk stok
				$validasi_stok = '0';
				if($dt->kondisi_barang == "RUSAK"){
					$validasi_stok = $dt->total_stok_rusak - $dt->jumlah_retur; 
				}else if($dt->kondisi_barang == "BAIK"){
					$validasi_stok = $dt->total_stok - $dt->jumlah_retur;
				}
			
				if($validasi_stok < 0){
					echo json_encode(array(
						'status'	=> 1,
						'pesan' 	=> "	<b class='text-danger'>
												Maaf anda tidak bisa mengapus data retur penjualan dengan jumlah : $data->jumlah_retur
											</b></br></br>

											<small>SKU : </small></br>
											<b class='text-dark'>$data->sku</b><br/></br>

											<small>Nama Barang : </small> <br/>
											<b class='text-dark'>$data->nama_barang </b> </br><br/>

											<small>Dari Stok : </small></br>
											<b class='text-dark'>$data->dari_stok </b><br/><br/>

											<small>Kondisi Barang : </small></br>
											<b class='text-dark'>$data->kondisi_barang </b><br/><br/>

											<small>Keterangan Retur : </small> <br/>
											<b class='text-danger'>$data->keterangan </b><br/><br/>

											<b class='text-dark'>Karna bisa menyebabkan minus sebanyak : </b></br>
											<h1 class='text-danger'>$validasi_stok</h1>
									 	",

						'footer'	=> 	"<button type='button' class='btn btn-danger waves-effect' data-dismiss='modal'>Ok, saya mengerti</button>"
					));
				}else{
					$hapus = $this->retur_barang_dari_toko_detail->hapus_retur_barang_detail(
						$dt->id_retur, $dt->id_retur_pembelian_d, $dt->id_retur_pembelian_m, $dt->no_retur_pembelian, 
						$dt->id_barang_pusat, $dt->harga_satuan, $dt->jumlah_retur, $dt->jumlah_masuk, 
						$dt->subtotal, $dt->dari_stok, $dt->kondisi_barang, $dt->keterangan, 
						$dt->id_pegawai_pembuatan, $dt->id_pegawai_pembaharuan, $id_pegawai_pembatalan,
						$dt->tanggal_pembuatan, $dt->tanggal_pembaharuan, $keterangan_batal, $kode_toko
					);
				}
			}

			if($hapus){
				// Ambil jumlah barang yang sudah di approve dan jumlah barang batal
				$data_retur = $this->retur_barang_dari_toko_master->get_jumlah_retur($id_retur_pembelian_m, $kode_toko);
				$data_induk = $this->retur_barang_dari_toko_master->get_by_id_retur($id_retur_pembelian_m, $kode_toko);
				$data_batal = $this->retur_barang_dari_toko_master->get_by_id_retur_batal($id_retur_pembelian_m, $kode_toko);

				// Tampilkan informasi hapus
				$json['status']       = '1';
				$json['id_rpm']       = $id_retur_pembelian_m;
				$json['id_rpd']       = $id_retur_pembelian_d;
				$json['jumlah_retur'] = $data_retur->jml_barang;
				$json['jumlah_masuk'] = $data_induk->jml_barang;
				$json['jumlah_batal'] = $data_batal->jml_barang;
				$json['pesan']        = "Data barang retur barang berhasil dihapus";
				$json['url']          = 'retur_barang_dari_toko/transaksi/?id='.$id_retur_pembelian_m.'&kd='.$kode_toko;
				echo json_encode($json);
			}else{
				$json['status']      = '0';
				$json['judul']       = 'Gagal!';
				$json['pesan']       = 'Barang retur barang dari toko, gagal disimpan.';
				$json['tipe_pesan']  = 'error';
				$json['gaya_tombol'] = 'btn-danger btn-md waves-effect waves-light';
				echo json_encode($json);
			}
		}else{
			redirect(base_url()."retur_barang_dari_toko");
			exit();
		}
	}

	public function ajax_pencarian_transaksi()
	{
		if($this->input->is_ajax_request()){
			// Cek user acces menu
			$id_pegawai     = $this->session->userdata('id_pegawai');
			$cek_useraccess = $this->useraccess_pusat->cek_access($id_pegawai, '51');
			if($cek_useraccess->act_create == '0' or $cek_useraccess->act_create == '-'){
				$json['status'] = '2';
			}else{
				$jumlah_jual = '0';
				$kode_toko   = $this->input->post('kode_toko');
				$keyword     = $this->input->post('keyword');
				if($kode_toko == ''){
					$json['status'] = 0;
					$json['pesan']  = 'kode toko tidak terdaftar';
					exit();
				}

				$daftar_transaksi = $this->retur_barang_dari_toko_master->cari_daftar_transaksi($keyword, $kode_toko);

				if($daftar_transaksi->num_rows() > 0){
					$json['query']     = $this->db->last_query();
					$json['kode_toko'] = $kode_toko;
					$json['status']    = 1;
					$json['datanya']   = "<ul id='daftar_autocompleted_transaksi' class='list-group user-list'>";

					foreach($daftar_transaksi->result() as $dt){
						$json['datanya'] .= "
			                <li class='user-list-item'>
			                	<div class='avatar avatar-sm-box'>
                                    <img src='assets/upload/image/avatar/avatar.jpg'>
			                	</div>";

						$json['datanya'] .= "
			                	<div class='user-desc'>
									<span id='data_no_retur' class='name'>".$dt->no_retur_pembelian."</span>
									<span id='jml_barang_retur' class='name'>JML Barang : ".$dt->jml_barang."</span>
									<span id='data_tanggal_retur' class='name'>".$dt->tanggal_retur."</span>
									
									<span id='data_id_retur_pembelian_m' style='display:none;'>".$dt->id_retur_pembelian_m."</span>
									<span id='data_keterangan_lain' style='display:none;'>".$dt->keterangan_lain."</span>
									<span id='data_jml_barang_retur' style='display:none;'>".$dt->jml_barang."</span>
								</div>
							</li>
						";
					}

					$json['datanya'] .= "</ul>";
				}else{
					$json['status']    = 0;
					$json['pesan']     = 'Data tidak ada';
					$json['kode_toko'] = $kode_toko;
					$json['keyword']   = $keyword;
				}
			}
			echo json_encode($json);
		}else{
			redirect(base_url().'retur_barang_dari_toko');
		}
	}
}
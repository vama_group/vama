<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Daftar_paket_harga_spesial extends MY_Controller 
{
	function __construct()
	{
		parent::__construct();
		$this->CI =& get_instance();
		$this->load->model('pegawai_pusat_model','pegawai_pusat');
		$this->load->model('customer_pusat_model','customer_pusat');
		$this->load->model('useraccess_pusat_model','useraccess_pusat');
		$this->load->model('barang_pusat_model','barang_pusat');
		$this->load->model('paket_promo_master_model','paket_promo_master');
		$this->load->model('paket_promo_detail_model','paket_promo_detail');
	}

	public function index()
	{
		// Cek user acces menu
		$id_pegawai     = $this->session->userdata('id_pegawai');
		$data_pegawai   = $this->pegawai_pusat->get_by_id($id_pegawai);
		if($data_pegawai){
			$cek_useraccess = $this->useraccess_pusat->cek_access($id_pegawai, '30');
			$this->CI->session->set_userdata('usergroup_name', $data_pegawai->usergroup_name);
			if($cek_useraccess->act_read == '0' or $cek_useraccess->act_read == '-'){
				redirect(base_url().'dashboard');
			}
			
			// Ambil daftar toko
			$daftar_toko             = $this->customer_pusat->listing_toko();
			$data['data_toko']       = $daftar_toko;

			$data['access_create']   = $cek_useraccess->act_create;
			$data['data_pegawai']    = $data_pegawai;
			$data['data_induk_menu'] = $this->useraccess_pusat->get_induk_menu($id_pegawai);
			
			$data['atribut_halaman'] = 'Daftar Paket Promo';
			$data['halaman_laporan'] = $this->load->view('admin/master_data/daftar_paket_harga_spesial/laporan',$data,true);
			$data['halaman_plugin']  = $this->load->view('admin/master_data/daftar_paket_harga_spesial/plugin',$data,true);
			$data['isi_halaman']     = $this->load->view('admin/master_data/daftar_paket_harga_spesial/index',$data,true);
			$this->load->view('admin/layout',$data);
		}else{
			redirect(base_url().'dashboard');
		}
	}

	public function ajax_list()
	{
		// Cek user acces menu
		$id_pegawai     = $this->session->userdata('id_pegawai');
		$data_pegawai   = $this->pegawai_pusat->get_by_id($id_pegawai);
		$cek_useraccess = $this->useraccess_pusat->cek_access($id_pegawai, '30');
		if($cek_useraccess->act_read == '0' or $cek_useraccess->act_read == '-'){
			redirect(base_url().'dashboard');
			exit();
		}

		$kode_toko              = $this->input->post('kode_toko');
		$status_paket           = $this->input->post('status_paket');
		$data_laporan_pembelian = $this->paket_promo_master->get_laporan('_get_laporan_query', $status_paket, $kode_toko);
		$data                   = array();
		$no                     = $_POST['start'];

		foreach ($data_laporan_pembelian as $dlp){
			$no++;
			$row 	= array();
			$row[] 	= $no;
			$status = $dlp->status_paket;

			//Awal tombol
			if($status !== 'DI TERIMA'){
				if($cek_useraccess->act_update == 1 AND $status=="MENUNGGU" 
					OR $cek_useraccess->act_create == 1 AND $status=="MENUNGGU"){
					$tombol_edit = '<a class="btn btn-default btn-rounded btn-xs" onclick="edit_paket_promo('."'".$dlp->id_paket_promo_m."'".')">
										<i class="fa fa-pencil" style="color:blue;"></i>
									</a>';
				}else{
					$tombol_edit = '<b class="btn btn-rounded btn-xs btn-default text-muted text-center">Tidak ada akses</b>';
				}
			}else{
				if($cek_useraccess->act_update == 1 AND $status=="DI TERIMA"){
					$tombol_edit = '<a class="btn btn-default btn-rounded btn-xs" onclick="edit_paket_promo('."'".$dlp->id_paket_promo_m."'".')">
										<i class="fa fa-pencil" style="color:blue;"></i>
									</a>';
				}else{
					$tombol_edit = '<b class="btn btn-rounded btn-xs btn-default text-muted text-center">Tidak ada akses</b>';
				}
			}

			$row[] = '
				'.$tombol_edit.'
			';
			// Akhir tombol

			if($status == "DI TERIMA"){
				$row[] 	= '<span class="btn btn-success btn-xs btn-block btn-rounded text-center">'.$status.'</span>';
			}elseif($status=="MENUNGGU"){
				$row[] 	= '<span class="btn btn-primary btn-xs btn-block btn-rounded text-center">'.$status.'</span>';
			}elseif($status=="DI SIAPKAN"){
				$row[] 	= '<span class="btn btn-danger btn-xs btn-block btn-rounded text-center">'.$status.'</span>';
			}

			$row[] 	= '<span class="text-dark pull-right">'.$dlp->kode_paket_promo.'</span>';
			$row[] 	= '<span class="text-dark pull-right">'.$dlp->nama_paket_promo.'</span>';

			$row[] 	= date('d-m-Y', strtotime($dlp->tanggal_awal_paket)).' - '.date('d-m-Y', strtotime($dlp->tanggal_awal_paket));
			$row[] 	= '<span class="pull-right">'.number_format($dlp->harga_dasar,'0', ',', '.').'</span>';
			$row[] 	= '<span class="pull-right">'.number_format($dlp->harga_paket,'0', ',', '.').'</span>';
			$row[] 	= '<span class="pull-right">'.number_format($dlp->jml_item,'0', ',', '.').'</span>';

			$row[] 	= $dlp->tipe_paket;
			$row[] 	= $dlp->nama_kategori_paket;
			$row[] 	= $dlp->jenis_paket;
			$row[] 	= $dlp->pegawai_save;
			$row[] 	= $dlp->tanggal_pembuatan;
			$row[] 	= $dlp->pegawai_edit;
			$row[] 	= $dlp->tanggal_pembaharuan;

			$data[]	= $row;
		}

		$output = array(
			"draw"            => $_POST['draw'],
			"recordsTotal"    => $this->paket_promo_master->count_all('vamr4846_toko_mrc.paket_master', 'TIDAK'),
			"recordsFiltered" => $this->paket_promo_master->count_filtered('_get_laporan_query', $status_paket, $kode_toko),
			"data"            => $data
		);		
		echo json_encode($output);
	}

	public function ajax_list_detail()
	{
		// Cek user acces menu
		$id_pegawai             = $this->session->userdata('id_pegawai');
		$data_pegawai           = $this->pegawai_pusat->get_by_id($id_pegawai);
		$cek_useraccess = $this->useraccess_pusat->cek_access($id_pegawai, '30');
		if($cek_useraccess->act_read == '0' or $cek_useraccess->act_read == '-'){
			redirect(base_url().'dashboard');
			exit();
		}

		$kode_toko              = $this->input->post('kode_toko');
		$status_paket           = $this->input->post('status_paket');

		$data_laporan_pembelian = $this->paket_promo_master->get_laporan('_get_laporan_query_detail', $status_paket, $kode_toko);
		$data                   = array();
		$no                     = $_POST['start'];

		foreach ($data_laporan_pembelian as $dlp){
			$no++;
			$row 	= array();
			$row[] 	= $no;
			$status = $dlp->status_paket;

			//Awal tombol
			if($status !== 'DI TERIMA'){
				if($cek_useraccess->act_update == 1){
					$tombol_edit 	 = '<a class="btn btn-default btn-rounded btn-xs" onclick="edit_paket_promo('."'".$dlp->id_paket_promo_m."'".')">
											   <i class="fa fa-pencil" style="color:blue;"></i>
										</a>';
				}else{
					if($cek_useraccess->act_update == 1 AND $status=="DI TERIMA" 
						OR $cek_useraccess->act_create == 1 AND $status=="DI TERIMA"){
						$tombol_edit = '<a class="btn btn-default btn-rounded btn-xs" onclick="edit_paket_promo('."'".$dlp->id_paket_promo_m."'".')">
											<i class="fa fa-pencil" style="color:blue;"></i>
										</a>';
					}else{
						$tombol_edit = '<b class="btn btn-rounded btn-xs btn-default text-muted text-center">Tidak ada akses</b>';
					}
				}
			}else{
				$tombol_edit = '<b class="btn btn-rounded btn-xs btn-default text-muted text-center">Tidak ada akses</b>';
			}

			$row[] = '
				'.$tombol_edit.'
			';
			// Akhir tombol

			if($status == "DI TERIMA"){
				$row[] 	= '<span class="btn btn-success btn-xs btn-block btn-rounded text-center">'.$status.'</span>';
			}elseif($status=="MENUNGGU"){
				$row[] 	= '<span class="btn btn-primary btn-xs btn-block btn-rounded text-center">'.$status.'</span>';
			}elseif($status=="DI SIAPKAN"){
				$row[] 	= '<span class="btn btn-danger btn-xs btn-block btn-rounded text-center">'.$status.'</span>';
			}

			$row[] 	= '<span class="text-dark pull-left">'.$dlp->nama_paket_promo.'</span>';
			$row[] 	= date('d-m-Y', strtotime($dlp->tanggal_awal_paket)).' - '.date('d-m-Y', strtotime($dlp->tanggal_awal_paket));
			$row[] 	= '<span class="text-dark pull-left">'.$dlp->sku.'</span>';
			$row[] 	= '<span class="text-dark pull-left">'.$dlp->nama_barang.'</span>';
			$row[] 	= '<span class="pull-right">'.number_format($dlp->harga_eceran,'0', ',', '.').'</span>';
			$row[] 	= '<span class="pull-right">'.number_format($dlp->qty,'0', ',', '.').'</span>';

			$row[] 	= $dlp->pegawai_save;
			$row[] 	= $dlp->tanggal_pembuatan;
			$row[] 	= $dlp->pegawai_edit;
			$row[] 	= $dlp->tanggal_pembaharuan;

			$data[]	= $row;
		}

		$output = array(
			"draw"            => $_POST['draw'],
			"recordsTotal"    => $this->paket_promo_master->count_all('vamr4846_toko_mrc.paket_detail', 'TIDAK'),
			"recordsFiltered" => $this->paket_promo_master->count_filtered('_get_laporan_query_detail', $status_paket, $kode_toko),
			"data"            => $data
		);		
		echo json_encode($output);
	}

	public function ajax_list_batal()
	{
		// Cek user acces menu
		$id_pegawai             = $this->session->userdata('id_pegawai');
		$data_pegawai           = $this->pegawai_pusat->get_by_id($id_pegawai);
		$cek_useraccess = $this->useraccess_pusat->cek_access($id_pegawai, '30');
		if($cek_useraccess->act_read == '0' or $cek_useraccess->act_read == '-'){
			redirect(base_url().'dashboard');
			exit();
		}

		$kode_toko              = $this->input->post('kode_toko');
		$status_paket           = $this->input->post('status_paket');

		$data_laporan_pembelian = $this->paket_promo_master->get_laporan('_get_laporan_query_batal', '', $kode_toko);
		$data                   = array();
		$no                     = $_POST['start'];

		foreach ($data_laporan_pembelian as $dlp){
			$no++;
			$row 	= array();
			$row[] 	= $no;

			$row[] 	= '<span class="text-dark pull-left">'.$dlp->nama_paket_promo.'</span>';
			$row[] 	= date('d-m-Y', strtotime($dlp->tanggal_awal_paket)).' - '.date('d-m-Y', strtotime($dlp->tanggal_awal_paket));
			$row[] 	= '<span class="text-dark pull-left">'.$dlp->sku.'</span>';
			$row[] 	= '<span class="text-dark pull-left">'.$dlp->nama_barang.'</span>';
			$row[] 	= '<span class="pull-right">'.number_format($dlp->harga_eceran,'0', ',', '.').'</span>';
			$row[] 	= '<span class="pull-right">'.number_format($dlp->qty,'0', ',', '.').'</span>';
			
			$row[] 	= $dlp->keterangan_hapus;
			$row[] 	= $dlp->pegawai_hapus;
			$row[] 	= $dlp->tanggal_hapus;
			$row[] 	= $dlp->pegawai_save;
			$row[] 	= $dlp->tanggal_pembuatan;
			$row[] 	= $dlp->pegawai_edit;
			$row[] 	= $dlp->tanggal_pembaharuan;

			$data[]	= $row;
		}

		$output = array(
			"draw"            => $_POST['draw'],
			"recordsTotal"    => $this->paket_promo_master->count_all('vamr4846_toko_mrc.paket_detail', 'IYA'),
			"recordsFiltered" => $this->paket_promo_master->count_filtered('_get_laporan_query_batal', '', $kode_toko),
			"data"            => $data
		);		
		echo json_encode($output);
	}

	public function ambil_total()
	{
		// Cek user acces menu
		$id_pegawai     = $this->session->userdata('id_pegawai');
		$cek_useraccess = $this->useraccess_pusat->cek_access($id_pegawai, '30');
		if($cek_useraccess->act_read == '0' or $cek_useraccess->act_read == '-'){
			redirect(base_url().'dashboard');
		}

		if($this->session->userdata('usergroup_name') !== 'Super Admin'){
			redirect(base_url().'laporan_pembelian');
			exit();
		}

		$tanggal_filter = $this->input->post('tanggal_filter');
		if($tanggal_filter == ''){
			redirect(base_url().'laporan_pembelian');
			exit();
		}

		$tanggal_awal  = substr($tanggal_filter, 0, 10);
		$tanggal_akhir = substr($tanggal_filter, 18, 27);

		// Ambil jumlah transaksi, total tunai, debit dan total uang masuk
		$data_uang_masuk = $this->paket_promo_master->get_grand_total($tanggal_awal, $tanggal_akhir);
		if ($data_uang_masuk){
			$jml_transaksi         = number_format($data_uang_masuk->jml_transaksi,'0', ',', '.');
			$grand_total_pembelian = '<span>Rp. <p class="pull-right">'.number_format($data_uang_masuk->grand_total_pembelian,'0', ',', '.').'</p></span>';
		}else{
			$jml_transaksi         = '0';
			$grand_total_pembelian = '0';
		}
		
		echo json_encode(array(
			'status' 				=> 1,
			"jml_transaksi"			=> $jml_transaksi,
			"grand_total_pembelian"	=> $grand_total_pembelian
		));
	}
}
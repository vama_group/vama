<?php
defined('BASEPATH') OR exit('No direct script access allowed');

// Load library phpspreadsheet
// require('./assets/excel/vendor/autoload.php');
// use PhpOffice\PhpSpreadsheet\Helper\Sample;
// use PhpOffice\PhpSpreadsheet\IOFactory;
// use PhpOffice\PhpSpreadsheet\Spreadsheet;

class Barang_toko extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->CI =& get_instance();
		$this->load->model('pegawai_pusat_model', 'pegawai_pusat');
        $this->load->model('useraccess_pusat_model', 'useraccess_pusat');

        $this->load->model('periode_stok_toko_model', 'periode_stok_toko');
	    $this->load->model('barang_toko_model', 'barang_toko');
	    $this->load->model('customer_pusat_model', 'customer_pusat');
	    $this->load->model('stok_opname_toko_model', 'stok_opname_toko');
	}

	public function index()
	{		
		// Cek user acces menu
		$id_pegawai = $this->session->userdata('id_pegawai');
		if($id_pegawai == ''){
			redirect(base_url().'login');
			exit();
		}

		$data_pegawai   = $this->pegawai_pusat->get_by_id($id_pegawai);
		if($data_pegawai){
			$cek_useraccess = $this->useraccess_pusat->cek_access($id_pegawai, '20');
			if($cek_useraccess){
				if($cek_useraccess->act_read == '0' or $cek_useraccess->act_read == '-'){
					redirect(base_url().'dashboard');
					exit();
				}					
			}else{
				redirect(base_url().'dashboard');
				exit();
			}

			$data['access_create']   = $cek_useraccess->act_create;
			$data['data_pegawai']    = $data_pegawai;
			$data['data_induk_menu'] = $this->useraccess_pusat->get_induk_menu($id_pegawai);
			
			// Ambil daftar toko
			$daftar_toko             = $this->customer_pusat->listing_toko();
			$data['data_toko']       = $daftar_toko;
			
			$data['atribut_halaman'] = 'Barang Toko';
			$data['halaman_list']    = $this->load->view('admin/master_data/barang_toko/list',$data,true);
			$data['halaman_form']    = $this->load->view('admin/master_data/barang_toko/form',$data,true);
			$data['halaman_plugin']  = $this->load->view('admin/master_data/barang_toko/plugin',$data,true);
			$data['isi_halaman']     = $this->load->view('admin/master_data/barang_toko/index',$data,true);
			$this->load->view('admin/layout',$data);
		}else{
			redirect(base_url().'login');
			exit();
		}
	}

	public function ambil_periode_stok(){
		// Cek user acces menu
		$id_pegawai 	= $this->session->userdata('id_pegawai');
		$cek_useraccess = $this->useraccess_pusat->cek_access($id_pegawai, '19');
		if($cek_useraccess->act_read == '0' or $cek_useraccess->act_read == '0'){
			redirect(base_url().'dashboard');
			exit();
		}

		$kode_toko = $this->input->post('kode_toko');
		$data_toko = $this->customer_pusat->get_by_kode($kode_toko);
		if($data_toko){
			$id_toko             = $data_toko->id_customer_pusat;
			$data_periode        = $this->periode_stok_toko->get_by_id_toko($id_toko)->result();
			$data_jumlah_periode = $this->periode_stok_toko->get_by_id_toko($id_toko)->num_rows();
			echo json_encode(array(
				'status' => '1',
				'data'   => $data_periode,
				'jumlah' => $data_jumlah_periode
			));
		}else{
			echo json_encode(array(
				'status' => '0'
			));
		}

	}

	public function ajax_list()
	{
		// Cek user acces menu
		$id_pegawai     = $this->session->userdata('id_pegawai');
		$cek_useraccess = $this->useraccess_pusat->cek_access($id_pegawai, '20');
		if($cek_useraccess->act_read == '0' or $cek_useraccess->act_read == '-'){
			redirect(base_url().'dashboard');
		}

		$kode_toko = $this->input->post('kode_toko');
		$dt        = $this->customer_pusat->cari_kode_customer_pusat($kode_toko);
		if($dt){
			$id_customer_pusat = $dt->id_customer_pusat;
		}else{
			$id_customer_pusat = '';
		}

		$id_periode_stok = $this->input->post('id_periode_stok');	
		if($id_periode_stok == '' || $kode_toko == ''){
			exit();
		}

		$data_periode = $this->periode_stok_toko->get_by_id($id_periode_stok);
		if($data_periode){
			$tanggal_awal  = $data_periode->tanggal_periode_awal;
			$tanggal_akhir = $data_periode->tanggal_periode_akhir;
		}else{
			exit();
		}

		$list 	= $this->barang_toko->get_datatables(
			'', $kode_toko, $id_customer_pusat, '_get_datatables_query', $tanggal_awal, $tanggal_akhir
		);
		$data 	= array();
		$no 	= $_POST['start'];
		foreach ($list as $barang_toko) {
			$no++;
			$row 	= array();
			$row[] 	= $no;

			//add html for action
			if($cek_useraccess->act_update == 1){
				$tombol_edit 	= '<a class="btn btn-rounded btn-xs btn-default" href="javascript:void(0)" title="Edit Barang Toko" 
								   		onclick="edit_barang_toko('."'".$barang_toko->id_barang_toko."'".', '."'".$barang_toko->kode_toko."'".')">
										<i class="fa fa-pencil" style="color: blue;"></i>
								  </a>';
			}else{$tombol_edit 	= '';}

			if($cek_useraccess->act_delete == 1){
				$tombol_hapus 	= '<a class="btn btn-rounded btn-xs btn-default" href="javascript:void(0)" title="Hapus Barang Toko" 
										onclick="verifikasi_delete('."'".$barang_toko->id_barang_toko."'".')">
										<i class="fa fa-remove" style="color: red;"></i>
					  			  </a>';
			}else{$tombol_hapus = '';}

			//add html for action
			$row[] = '	<a class="btn btn-rounded btn-default btn-xs" href="javascript:void(0)" title="Cetak Barcode" 
							onclick="cetak_barcode('."'".$barang_toko->id_barang_toko."'".')">
							<i class="fa fa-barcode"></i>
						</a>
						<a class="btn btn-rounded btn-default btn-xs" href="javascript:void(0)" title="Detail Barang Toko" 
							onclick="view_barang_toko('."'".$barang_toko->id_barang_toko."'".')">
							<i class="fa fa-folder-open-o" style="color: #188ae2;"></i>
						</a>
						
					  	'.$tombol_edit.'
					  	'.$tombol_hapus.'
				  	';

			$row[] 	= $barang_toko->nama_customer_pusat;
			
			if($barang_toko->status == 'AKTIF'){
				$warna_text = 'text-dark';
			}else{
				$warna_text = 'text-danger';
			}

			$row[] 	= $barang_toko->sku;
			$row[] 	= '<b class="'.$warna_text.'"> '.$barang_toko->nama_barang.'</b>';

			$stok       = $barang_toko->total_stok;
			$stok_rusak = $barang_toko->total_stok_rusak;

			// Awal stok jual
			if($stok<="0"){
				$row[] = '
							<div class="btn btn-default btn-rounded btn-block btn-xs">
							<i class="fa fa-ban text-danger"></i>
							<span class="text-danger text-center">'."".$stok."".'</span>
							</div>
						 ';
			}elseif($stok<="4"){
				$row[] = '
							<div class="btn btn-default btn-rounded btn-block btn-xs">
							<i class="fa fa-warning text-warning"></i>
							<span class="text-warning text-center">'."".$stok."".'</span>
							</div>
						 ';
			}elseif($stok<="9"){
				$row[] = '
							<div class="btn btn-default btn-rounded btn-block btn-xs">
							<i class="fa fa-check text-primary"></i>
							<span class="text-primary text-center">'."".$stok."".'</span>
							</div>
						 ';
			}elseif($stok>="10"){
				$row[] = '
							<div class="btn btn-default btn-rounded btn-block btn-xs">
							<i class="fa fa-check text-success"></i>
							<span class="text-success text-center">'."".$stok."".'</span>
							</div>
						 ';
			}
			// Akhir stok jual

			// Awal stok rusak
			if($stok_rusak<="0"){
				$row[] = '
							<div class="btn btn-default btn-rounded btn-block btn-xs">
							<i class="fa fa-ban text-danger"></i>
							<span class="text-danger text-center">'."".$stok_rusak."".'</span>
							</div>
						 ';
			}elseif($stok_rusak<="4"){
				$row[] = '
							<div class="btn btn-default btn-rounded btn-block btn-xs">
							<i class="fa fa-warning text-warning"></i>
							<span class="text-warning text-center">'."".$stok_rusak."".'</span>
							</div>
						 ';
			}elseif($stok_rusak<="9"){
				$row[] = '
							<div class="btn btn-default btn-rounded btn-block btn-xs">
							<i class="fa fa-check text-primary"></i>
							<span class="text-primary text-center">'."".$stok_rusak."".'</span>
							</div>
						 ';
			}elseif($stok_rusak>="10"){
				$row[] = '
							<div class="btn btn-default btn-rounded btn-block btn-xs">
							<i class="fa fa-check text-success"></i>
							<span class="text-success text-center">'."".$stok_rusak."".'</span>
							</div>
						 ';
			} 
			// Akhir stok rusak 

			$row[] 	= number_format($barang_toko->harga_eceran,'0',',','.');
			$row[] 	= '<span class="'.$warna_text.'">'.$barang_toko->status.'</span>';
			$row[] 	= $barang_toko->status2;
			
			$row[]	= $barang_toko->pegawai_save;
			$row[] 	= $barang_toko->tanggal_pembuatan;
			$row[] 	= $barang_toko->pegawai_edit;
			$row[] 	= $barang_toko->tanggal_pembaharuan;
			$data[] = $row;
		}

		$output = array(	
			"draw"            => $_POST['draw'],
			"recordsTotal"    => $this->barang_toko->count_all('', $kode_toko),
			"recordsFiltered" => $this->barang_toko->count_filtered('', $kode_toko, $id_customer_pusat, '_get_datatables_query', $tanggal_awal, $tanggal_akhir),
			"data"            => $data,
			"query" => $this->db->last_query()
		);
		echo json_encode($output);
	}

	public function ajax_list_daftar_transaksi()
	{
		// Cek user acces menu
		$id_pegawai 	= $this->session->userdata('id_pegawai');
		$cek_useraccess = $this->useraccess_pusat->cek_access($id_pegawai, '20');
		if($cek_useraccess->act_read == '0' or $cek_useraccess->act_read == '0'){
			redirect(base_url().'dashboard');
			exit();
		}

		$id_periode_stok = $this->input->post('id_periode_stok');
		if($id_periode_stok == ''){
			exit();
		}

		$kode_toko = $this->input->post('kode_toko');
		$dt        = $this->customer_pusat->cari_kode_customer_pusat($kode_toko);
		if($dt){
			$id_customer_pusat = $dt->id_customer_pusat;
		}else{
			$id_customer_pusat = '';
		}

		$data_periode = $this->periode_stok_toko->get_by_id($id_periode_stok);
		if($data_periode){
			$tanggal_awal  = $data_periode->tanggal_periode_awal;
			$tanggal_akhir = $data_periode->tanggal_periode_akhir;
		}else{
			// redirect(base_url().'barang_toko');
			exit();
		}

		$list = $this->barang_toko->get_datatables(
			'', $kode_toko, $id_customer_pusat, '_get_datatables_query_transaksi', $tanggal_awal, $tanggal_akhir
		);
		
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $barang_toko) {
			$no++;
			$row = array();
			
			$row[] = $no;
			$row[] = $barang_toko->nama_customer_pusat;
			$row[] = $barang_toko->sku;
			$row[] = '<span class="text-dark"><b>'.$barang_toko->nama_barang.'</b></span>';
			$row[] = 'Rp. <span class="text-right">'."".number_format($barang_toko->harga_eceran,'0',',','.')."".'</span>';

			$row[] = '<span class="text-primary">'.$barang_toko->so.'</span>';
			$row[] = '<span class="text-primary">'.$barang_toko->bm.'</span>';
			$row[] = '<span class="text-danger">'.$barang_toko->pj.'</span>';
			$row[] = '<span class="text-danger">'.$barang_toko->rpt.'</span>';
			$row[] = '<span class="text-danger">'.$barang_toko->rbkpt.'</span>';
			$row[] = '<span class="text-danger">'.$barang_toko->pp.'</span>';

			// Awal stok jual
			$stok  = $barang_toko->total_stok_fix;
			if($stok<="0"){
				$row[] = '
							<div class="btn btn-default btn-rounded btn-block btn-xs">
							<i class="fa fa-ban text-danger"></i>
							<span class="text-danger text-center">'."".$stok."".'</span>
							</div>
						 ';
			}elseif($stok<="4"){
				$row[] = '
							<div class="btn btn-default btn-rounded btn-block btn-xs">
							<i class="fa fa-warning text-warning"></i>
							<span class="text-warning text-center">'."".$stok."".'</span>
							</div>
						 ';
			}elseif($stok<="9"){
				$row[] = '
							<div class="btn btn-default btn-rounded btn-block btn-xs">
							<i class="fa fa-check text-primary"></i>
							<span class="text-primary text-center">'."".$stok."".'</span>
							</div>
						 ';
			}elseif($stok>="10"){
				$row[] = '
							<div class="btn btn-default btn-rounded btn-block btn-xs">
							<i class="fa fa-check text-success"></i>
							<span class="text-success text-center">'."".$stok."".'</span>
							</div>
						 ';
			}
			// Akhir stok jual

			$data[] = $row;
		}

		$output = array(	
			"draw" 				=> $_POST['draw'],
			"recordsTotal" 		=> $this->barang_toko->count_all('', $kode_toko),
			"recordsFiltered" 	=> $this->barang_toko->count_filtered(
										'', $kode_toko, $id_customer_pusat, '_get_datatables_query_transaksi', $tanggal_awal, $tanggal_akhir
									),
			"data" 				=> $data
		);
		echo json_encode($output);
	}

	public function ajax_list_daftar_transaksi_rusak()
	{
		// Cek user acces menu
		$id_pegawai 	= $this->session->userdata('id_pegawai');
		$cek_useraccess = $this->useraccess_pusat->cek_access($id_pegawai, '19');
		if($cek_useraccess->act_read == '0' or $cek_useraccess->act_read == '0'){
			redirect(base_url().'dashboard');
			exit();
		}

		$id_periode_stok = $this->input->post('id_periode_stok');
		if($id_periode_stok == ''){
			exit();
		}

		$kode_toko = $this->input->post('kode_toko');
		$dt        = $this->customer_pusat->cari_kode_customer_pusat($kode_toko);
		if($dt){
			$id_customer_pusat = $dt->id_customer_pusat;
		}else{
			$id_customer_pusat = '';
		}

		$data_periode = $this->periode_stok_toko->get_by_id($id_periode_stok);
		if($data_periode){
			$tanggal_awal  = $data_periode->tanggal_periode_awal;
			$tanggal_akhir = $data_periode->tanggal_periode_akhir;
		}else{
			// redirect(base_url().'barang_toko');
			exit();
		}

		$list = $this->barang_toko->get_datatables(
			'', $kode_toko, $id_customer_pusat, '_get_datatables_query_transaksi_rusak', $tanggal_awal, $tanggal_akhir
		);
		
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $barang_toko) {
			$no++;
			$row 	= array();
			$row[] 	= $no;
			$row[] 	= $barang_toko->nama_customer_pusat;
			$row[] 	= $barang_toko->sku;
			$row[] 	= '<span class="text-dark"><b>'.$barang_toko->nama_barang.'</b></span>';
			$row[] 	= 'Rp. <span class="text-right">'."".number_format($barang_toko->harga_eceran,'0',',','.')."".'</span>';
			$row[] 	= '<span class="text-primary">'.$barang_toko->rpr.'</span>';
			$row[] 	= '<span class="text-primary">'.$barang_toko->rbkpr.'</span>';

			// Awal stok rusak
			$stok  = $barang_toko->total_stok_rusak;
			if($stok<="0"){
				$row[] = '
							<div class="btn btn-default btn-rounded btn-block btn-xs">
							<i class="fa fa-ban text-danger"></i>
							<span class="text-danger text-center">'."".$stok."".'</span>
							</div>
						 ';
			}elseif($stok<="4"){
				$row[] = '
							<div class="btn btn-default btn-rounded btn-block btn-xs">
							<i class="fa fa-warning text-warning"></i>
							<span class="text-warning text-center">'."".$stok."".'</span>
							</div>
						 ';
			}elseif($stok<="9"){
				$row[] = '
							<div class="btn btn-default btn-rounded btn-block btn-xs">
							<i class="fa fa-check text-primary"></i>
							<span class="text-primary text-center">'."".$stok."".'</span>
							</div>
						 ';
			}elseif($stok>="10"){
				$row[] = '
							<div class="btn btn-default btn-rounded btn-block btn-xs">
							<i class="fa fa-check text-success"></i>
							<span class="text-success text-center">'."".$stok."".'</span>
							</div>
						 ';
			}
			// Akhir stok jual

			$data[] = $row;
		}

		$output = array(	
			"draw" 				=> $_POST['draw'],
			"recordsTotal" 		=> $this->barang_toko->count_all('', $kode_toko),
			"recordsFiltered" 	=> $this->barang_toko->count_filtered(
									'', $kode_toko, $id_customer_pusat, '_get_datatables_query_transaksi_rusak', $tanggal_awal, $tanggal_akhir),
			"data" 				=> $data
		);
		echo json_encode($output);
	}

	public function ajax_list_pembaharuan_barang()
	{
		// Cek user acces menu
		$id_pegawai 	= $this->session->userdata('id_pegawai');
		$cek_useraccess = $this->useraccess_pusat->cek_access($id_pegawai, '20');
		if($cek_useraccess->act_read == '0' or $cek_useraccess->act_read == '0'){
			redirect(base_url().'dashboard');
			exit();
		}

		$id_periode_stok = $this->input->post('id_periode_stok');
		if($id_periode_stok == ''){
			redirect(base_url().'barang_toko');
			exit();
		}

		$tanggal_awal 	= date('Y-m-01');
		$tanggal_akhir 	= date('Y-m-d');;
		$kode_toko 		= $this->input->post('kode_toko');
		$list 			= $this->barang_toko->get_datatables('', $kode_toko, '', '_get_datatables_query_pembaharuan_barang', $tanggal_awal, $tanggal_akhir);

		$data 	= array();
		$no 	= $_POST['start'];
		foreach ($list as $barang_toko) {
			$no++;
			$row = array();
			$row[] = $no;

			//add html for action
			if($cek_useraccess->act_update == 1){
				$tombol_edit 	 = '<a class="btn btn-rounded btn-default btn-xs" href="javascript:void(0)" title="Edit Barang Toko"
										onclick="edit_barang_toko('."'".$barang_toko->id_barang_toko."'".')">
										<i class="fa fa-pencil" style="color: blue;"></i>
								   </a>';
			}else{$tombol_edit 	 = '';}

			$row[] = '	<a class="btn btn-rounded btn-default btn-xs" href="javascript:void(0)" title="Cetak Barcode" 
							onclick="cetak_barcode('."'".$barang_toko->id_barang_toko."'".')">
							<i class="fa fa-barcode"></i>
						</a>
						<a class="btn btn-rounded btn-default btn-xs" href="javascript:void(0)" title="Detail Barang Toko" 
							onclick="view_barang_toko('."'".$barang_toko->id_barang_toko."'".')">
							<i class="fa fa-folder-open-o" style="color: #188ae2;"></i>
						</a>

					  	'.$tombol_edit.'
				  	';

			$row[] = $barang_toko->nama_customer_pusat;
			$row[] = $barang_toko->sku;
			$row[] = '<span class="text-dark"><b>'.$barang_toko->nama_barang.'</b></span>';

			// Awal stok jual
			$stok  = $barang_toko->total_stok;
			if($stok<="0"){
				$row[] = '
							<div class="btn btn-default btn-rounded btn-block btn-xs">
							<i class="fa fa-ban text-danger"></i>
							<span class="text-danger text-center">'."".$stok."".'</span>
							</div>
						 ';
			}elseif($stok<="4"){
				$row[] = '
							<div class="btn btn-default btn-rounded btn-block btn-xs">
							<i class="fa fa-warning text-warning"></i>
							<span class="text-warning text-center">'."".$stok."".'</span>
							</div>
						 ';
			}elseif($stok<="9"){
				$row[] = '
							<div class="btn btn-default btn-rounded btn-block btn-xs">
							<i class="fa fa-check text-primary"></i>
							<span class="text-primary text-center">'."".$stok."".'</span>
							</div>
						 ';
			}elseif($stok>="10"){
				$row[] = '
							<div class="btn btn-default btn-rounded btn-block btn-xs">
							<i class="fa fa-check text-success"></i>
							<span class="text-success text-center">'."".$stok."".'</span>
							</div>
						 ';
			}
			// Akhir stok jual

			$row[] 	= '<span class="text-right">'."".number_format($barang_toko->harga_eceran,'0',',','.')."".'</span>';
			$row[]	= $barang_toko->pegawai_save;
			$row[] 	= $barang_toko->tanggal_pembuatan;
			$row[] 	= $barang_toko->pegawai_edit;
			$row[] 	= $barang_toko->tanggal_pembaharuan;
			$data[] = $row;
		}

		$output = array(	"draw" 				=> $_POST['draw'],
							"recordsTotal" 		=> $this->barang_toko->count_all('', $kode_toko),
							"recordsFiltered" 	=> $this->barang_toko->count_filtered('', $kode_toko, '', 
											   										  '_get_datatables_query_pembaharuan_barang', 
											   										  $tanggal_awal, $tanggal_akhir),
							"data" 				=> $data
						);
		echo json_encode($output);
	}

	public function ajax_list_periksa_stok()
	{
		// Cek user acces menu
		$id_pegawai     = $this->session->userdata('id_pegawai');
		$cek_useraccess = $this->useraccess_pusat->cek_access($id_pegawai, '20');
		if($cek_useraccess->act_read == '0' or $cek_useraccess->act_read == '-'){
			redirect(base_url().'dashboard');
		}

		$kode_toko = $this->input->post('kode_toko');
		$dt        = $this->customer_pusat->cari_kode_customer_pusat($kode_toko);
		if($dt){
			$id_customer_pusat = $dt->id_customer_pusat;
		}else{
			$id_customer_pusat = '';
			exit();
		}

		$id_periode_stok = $this->input->post('id_periode_stok');	
		if($id_periode_stok == '' || $kode_toko == ''){
			exit();
		}

		$data_periode = $this->periode_stok_toko->get_by_id($id_periode_stok);
		if($data_periode){
			$tanggal_awal  = $data_periode->tanggal_periode_awal;
			$tanggal_akhir = $data_periode->tanggal_periode_akhir;
		}else{
			exit();
		}

		$list 	= $this->barang_toko->get_datatables(
			'', $kode_toko, $id_customer_pusat, '_get_datatables_query_periksa', $tanggal_awal, $tanggal_akhir
		);

		// $list = $this->barang_toko->periksa_stok_toko(
		// 	$kode_toko, $id_customer_pusat, $tanggal_awal, $tanggal_akhir
		// );

		$data = array();
		$no   = $_POST['start'];
		foreach ($list as $barang_toko) {
			$no++;
			$row   = array();
			$row[] = $no;

			$koreksi_stok = $barang_toko->total_stok - $barang_toko->total_stok_fix;
			if($koreksi_stok == '' or $koreksi_stok == '0'){
				$koreksi_stok = '0';
			}

			// if($barang_toko->koreksi <> '0'){
				//add html for action
				if($cek_useraccess->act_update == 1){
					$tombol_edit 	= '<a class="btn btn-rounded btn-xs btn-default" href="javascript:void(0)" title="Edit Barang Toko" 
									   		onclick="edit_barang_toko('."'".$barang_toko->id_barang_toko."'".', '."'".$barang_toko->kode_toko."'".')">
											<i class="fa fa-pencil" style="color: blue;"></i>
									  </a>';
				}else{$tombol_edit 	= '<b class="btn btn-rounded btn-xs btn-default text-muted text-center">Tidak ada akses</b>';}

				//add html for action
				$row[] = '	
						  	'.$tombol_edit.'
					  	';

				$row[] 	= $barang_toko->nama_customer_pusat;			
				$row[] 	= $barang_toko->sku;
				$row[] 	= '<b class="text-dark"> '.$barang_toko->nama_barang.'</b>';
				$row[] 	= number_format($barang_toko->harga_eceran,'0',',','.');
				$row[] 	= number_format($barang_toko->total_stok_fix,'0',',','.');
				$row[] 	= number_format($barang_toko->total_stok,'0',',','.');

				// Awal stok jual
				if($koreksi_stok<="0"){
					$row[] = '
								<div class="btn btn-default btn-rounded btn-block btn-xs">
								<i class="fa fa-ban text-danger"></i>
								<span class="text-danger text-center">'."".$koreksi_stok."".'</span>
								</div>
							 ';
				}elseif($koreksi_stok<="4"){
					$row[] = '
								<div class="btn btn-default btn-rounded btn-block btn-xs">
								<i class="fa fa-warning text-warning"></i>
								<span class="text-warning text-center">'."".$koreksi_stok."".'</span>
								</div>
							 ';
				}elseif($koreksi_stok<="9"){
					$row[] = '
								<div class="btn btn-default btn-rounded btn-block btn-xs">
								<i class="fa fa-check text-primary"></i>
								<span class="text-primary text-center">'."".$koreksi_stok."".'</span>
								</div>
							 ';
				}elseif($koreksi_stok>="10"){
					$row[] = '
								<div class="btn btn-default btn-rounded btn-block btn-xs">
								<i class="fa fa-check text-success"></i>
								<span class="text-success text-center">'."".$koreksi_stok."".'</span>
								</div>
							 ';
				}
				// Akhir stok jual


				$row[] 	= $barang_toko->status;				
				$row[]	= $barang_toko->pegawai_save;
				$row[] 	= $barang_toko->tanggal_pembuatan;
				$row[] 	= $barang_toko->pegawai_edit;
				$row[] 	= $barang_toko->tanggal_pembaharuan;
				$data[] = $row;
			// }
		}

		$output = array(	
			"draw"            => $_POST['draw'],
			"recordsTotal"    => $this->barang_toko->count_all('', $kode_toko),
			"recordsFiltered" => $this->barang_toko->count_filtered('', $kode_toko, $id_customer_pusat, '_get_datatables_query_periksa', $tanggal_awal, $tanggal_akhir),
			"data"            => $data
		);
		echo json_encode($output);
	}

	public function ajax_list_periksa_harga()
	{
		// Cek user acces menu
		$id_pegawai     = $this->session->userdata('id_pegawai');
		$cek_useraccess = $this->useraccess_pusat->cek_access($id_pegawai, '20');
		if($cek_useraccess->act_read == '0' or $cek_useraccess->act_read == '-'){
			redirect(base_url().'dashboard');
		}

		$kode_toko = $this->input->post('kode_toko');
		$dt        = $this->customer_pusat->cari_kode_customer_pusat($kode_toko);
		if($dt){
			$id_customer_pusat = $dt->id_customer_pusat;
		}else{
			$id_customer_pusat = '';
			exit();
		}

		$id_periode_stok = $this->input->post('id_periode_stok');	
		if($id_periode_stok == '' || $kode_toko == ''){
			exit();
		}

		$data_periode = $this->periode_stok_toko->get_by_id($id_periode_stok);
		if($data_periode){
			$tanggal_awal  = $data_periode->tanggal_periode_awal;
			$tanggal_akhir = $data_periode->tanggal_periode_akhir;
		}else{
			exit();
		}

		$list 	= $this->barang_toko->get_datatables(
			'', $kode_toko, $id_customer_pusat, '_get_datatables_query_periksa_harga', $tanggal_awal, $tanggal_akhir
		);

		// $list = $this->barang_toko->periksa_stok_toko(
		// 	$kode_toko, $id_customer_pusat, $tanggal_awal, $tanggal_akhir
		// );

		$data = array();
		$no   = $_POST['start'];
		foreach ($list as $barang_toko) {
			$no++;
			$row   = array();
			$row[] = $no;

			$koreksi_stok = $barang_toko->total_stok - $barang_toko->total_stok_fix;
			if($koreksi_stok == '' or $koreksi_stok == '0'){
				$koreksi_stok = '0';
			}

			// if($barang_toko->koreksi <> '0'){
				//add html for action
				if($cek_useraccess->act_update == 1){
					$tombol_edit 	= '<a class="btn btn-rounded btn-xs btn-default" href="javascript:void(0)" title="Edit Barang Toko" 
									   		onclick="edit_barang_toko('."'".$barang_toko->id_barang_toko."'".', '."'".$barang_toko->kode_toko."'".')">
											<i class="fa fa-pencil" style="color: blue;"></i>
									  </a>';
				}else{$tombol_edit 	= '<b class="btn btn-rounded btn-xs btn-default text-muted text-center">Tidak ada akses</b>';}

				//add html for action
				$row[] = '	
						  	'.$tombol_edit.'
					  	';

				$row[] 	= $barang_toko->nama_customer_pusat;			
				$row[] 	= $barang_toko->sku;
				$row[] 	= '<b class="text-dark"> '.$barang_toko->nama_barang.'</b>';
				$row[] 	= number_format($barang_toko->harga_eceran_pusat,'0',',','.');
				$row[] 	= number_format($barang_toko->harga_eceran,'0',',','.');
				$row[] 	= number_format($barang_toko->harga_eceran_koreksi,'0',',','.');
				$row[] 	= number_format($barang_toko->total_stok_fix,'0',',','.');
				$row[] 	= $barang_toko->status;

				$row[]	= $barang_toko->pegawai_save;
				$row[] 	= $barang_toko->tanggal_pembuatan;
				$row[] 	= $barang_toko->pegawai_edit;
				$row[] 	= $barang_toko->tanggal_pembaharuan;
				$data[] = $row;
			// }
		}

		$output = array(	
			"draw"            => $_POST['draw'],
			"recordsTotal"    => $this->barang_toko->count_all('', $kode_toko),
			"recordsFiltered" => $this->barang_toko->count_filtered('', $kode_toko, $id_customer_pusat, '_get_datatables_query_periksa_harga', $tanggal_awal, $tanggal_akhir),
			"data"            => $data
		);
		echo json_encode($output);
	}

	public function ajax_list_barang_toko()
	{
		// Cek user acces menu
		$id_pegawai     = $this->session->userdata('id_pegawai');
		$cek_useraccess = $this->useraccess_pusat->cek_access($id_pegawai, '20');
		if($cek_useraccess->act_read == '0' or $cek_useraccess->act_read == '-'){
			redirect(base_url().'dashboard');
		}

		$kode_barang = $this->input->post('kode_barang');
		if($kode_barang == ''){
			redirect(base_url().'barang_toko');
			exit();
		}

		$list = $this->barang_toko->get_datatables($kode_barang, '', '', '_get_datatables_query', '', '');
		$data = array();
		$no   = $_POST['start'];

		foreach ($list as $bt) 
		{
			$no++;
			$row = array();
			$row[] = $no;

			//add html for action
			if($cek_useraccess->act_update == 1){
				$tombol_edit 	= '<a class="btn btn-rounded btn-primary btn-xs" 
									onclick="edit_barang_toko('."'".$bt->id_barang_toko."'".')"><i class="fa fa-pencil"></i></a>';
			}else{$tombol_edit 	= '';}

			//add html for action
			$row[] =	''.$tombol_edit.'';

			$row[] 	= $bt->nama_customer_pusat;
			$row[] 	= 'Rp. <span class="pull-right">'.number_format($bt->harga_eceran,'0',',','.').'</span>';
			$row[] 	= $bt->status;
			$row[] 	= $bt->tanggal_pembaharuan;

			$data[]	= $row;
		}

		$output = array
						(
							'kode_barang'		=> $kode_barang,
							"draw"				=> $_POST['draw'],
							"recordsTotal"	 	=> $this->barang_toko->count_all($kode_barang, '', '', '_get_datatables_query', '', ''),
							"recordsFiltered" 	=> $this->barang_toko->count_filtered($kode_barang, '', '', '_get_datatables_query', '', ''),
							"data"				=> $data,
							"query"				=> $this->db->last_query()
						);
		
		//output to json format
		echo json_encode($output);
	}	

	public function ajax_import_barang_toko(){
		// Cek user acces menu
		$id_pegawai 	= $this->session->userdata('id_pegawai');
		$cek_useraccess = $this->useraccess_pusat->cek_access($id_pegawai, '20');
		if($cek_useraccess->act_update == '0' or $cek_useraccess->act_update == '-'){
			redirect(base_url().'barang_toko');
			exit();
		}

		if($id_pegawai == '1'){
			$data_toko = $this->barang_toko->daftar_toko();
			if($data_toko){
				foreach($data_toko->result() as $t){
					$id_toko        = $t->ID_Toko;
					$kode_toko      = $t->Kode_Toko;
					$kode_toko_baru = $t->Kode_Toko_Baru;

					$data_periode_toko = $this->periode_stok_toko->akhir($id_toko);
					if($data_periode_toko){
						$tanggal_awal = $data_periode_toko->tanggal_periode_awal;
						$tanggal_akhi = $data_periode_toko->tanggal_periode_akhir;
					}else{
						$tanggal_awal  = '0000-00-00';
						$tanggal_akhir = '0000-00-00';
					}

					$data_barang = $this->barang_toko->ambil_barang_toko_lama_v2($kode_toko);
					if($data_barang){
						$no = 1;
						ini_set('max_execution_time', 0); 
						ini_set('memory_limit','2048M');
						foreach($data_barang->result() as $b){
							// Validasi barang
							$validasi_barang = $this->barang_toko->validasi_barang($b->SKU, $b->Kode_Barang, $kode_toko_baru);
							if($validasi_barang){
							}else{
								$sql_insert_barang = "
									INSERT INTO vamr4846_toko_mrc.barang_toko_".$kode_toko_baru." (
										id_barang_pusat, kode_barang, sku, kode_toko,
										total_stok, total_stok_rusak, harga_eceran,
										status, status2, status_hapus, id_pegawai_pembuatan,
										tanggal_pembuatan, tanggal_pembaharuan
									)
									VALUES (
										'".$b->id_barang_pusat."', '".$b->Kode_Barang."', '".$b->SKU."', '".$kode_toko_baru."',
										'0', '0', '".$b->Eceran_Toko."',
										'".$b->Status1."', '".$b->Status2."', '".$b->Status_Hapus."', '1', 
										'".$b->Tanggal_Pembuatan."', '".$b->Tanggal_Pembaharuan."'
									)
								";
								$this->db->query($sql_insert_barang);
								$no = $no + 1;
							}
						}
					}
				}
			}

			if($no > 1){
				echo json_encode(array(
					'status' => 1
				));
			}else{
				echo json_encode(array(
					'status' => 0
				));
			}
		}else{
			echo json_encode(array(
				'status' => 0
			));
		}
	} 

	public function ajax_import_stok_toko(){
		// Cek user acces menu
		$id_pegawai 	= $this->session->userdata('id_pegawai');
		$cek_useraccess = $this->useraccess_pusat->cek_access($id_pegawai, '20');
		if($cek_useraccess->act_update == '0' or $cek_useraccess->act_update == '-'){
			redirect(base_url().'barang_toko');
			exit();
		}

		if($id_pegawai == '1'){
			$data_toko = $this->barang_toko->ambil_data_toko('TK005');
			if($data_toko){
				// foreach($data_toko->result() as $t){
					$id_toko        = $data_toko->ID_Toko;
					$kode_toko      = $data_toko->Kode_Toko;
					$kode_toko_baru = $data_toko->Kode_Toko_Baru;

					$data_periode_toko = $this->periode_stok_toko->akhir('5');
					if($data_periode_toko){
						$id_periode_stok_toko = $data_periode_toko->id_periode_stok_toko;
						$tanggal_awal         = $data_periode_toko->tanggal_periode_awal;
						$tanggal_akhir        = $data_periode_toko->tanggal_periode_akhir;
					}else{
						$tanggal_awal  = '0000-00-00';
						$tanggal_akhir = '0000-00-00';
					}

					$data_barang = $this->barang_toko->ambil_barang_toko_lama_v1('TK005', $tanggal_awal, $tanggal_akhir);
					if($data_barang){
						$no = 1;
						ini_set('max_execution_time', 0); 
						ini_set('memory_limit','2048M');
						foreach($data_barang->result() as $b){
							// Validasi barang
							$validasi_barang = $this->barang_toko->validasi_stok_opname($b->id_barang_pusat, $kode_toko_baru);
							if($validasi_barang){
							}else{
								if($b->Stok > 0){
									$catatan = "SO: ".$b->total_so.", BK: ".$b->total_barang_keluar.", 
												RPT: ".$b->total_retur_jual.", PJ: ".$b->total_jual.", 
												RBKPT : ".$b->total_retur_barang.", PP : ".$b->total_penukaran_poin."";
									// Insert stok opname toko
									$tanggal_sekarang  = date('Y-m-d H:i:s');
									$sql_insert_barang = "
										INSERT INTO vamr4846_toko_mrc.stok_opname_toko_".$kode_toko_baru." (
											id_barang_pusat, jumlah_so_sebelumnya, jumlah_so,
											id_periode_stok_toko, masuk_stok, catatan, 
											id_pegawai_pembuatan, tanggal_pembuatan
										)
										VALUES (
											'".$b->id_barang_pusat."', '0', '".$b->Stok."',
											'".$id_periode_stok_toko."', 'JUAL', '".$catatan."',
											'20', '".$tanggal_sekarang."'
										)
									";
									$this->db->query($sql_insert_barang);

									// Insert riwayat stok opname toko
									$sql_insert_barang = "
										INSERT INTO vamr4846_toko_mrc.riwayat_stok_opname_toko_".$kode_toko_baru." (
											id_barang_pusat, jumlah_so_sebelumnya, jumlah_so,
											id_periode_stok_toko, masuk_stok, catatan, 
											id_pegawai, tanggal_pembuatan
										)
										VALUES (
											'".$b->id_barang_pusat."', '0', '".$b->Stok."',
											'".$id_periode_stok_toko."', 'JUAL', '".$catatan."',
											'20', '".$tanggal_sekarang."'
										)
									";
									$this->db->query($sql_insert_barang);

									// Update stok master barang toko
									$sql_update_stok = "
										UPDATE vamr4846_toko_mrc.barang_toko_".$kode_toko_baru."
										SET total_stok='".$b->Stok."', status='AKTIF'
										WHERE id_barang_pusat = '".$b->id_barang_pusat."' 
									";
									$this->db->query($sql_update_stok);
									$no = $no + 1;
								}
							}
						}
					}
				// }
			}

			if($no > 1){
				echo json_encode(array(
					'status' => 1
				));
			}else{
				echo json_encode(array(
					'status' => 0
				));
			}
		}else{
			echo json_encode(array(
				'status' => 0
			));
		}
	}

	public function ajax_fix_stok_toko(){
		// Cek user acces menu
		$id_pegawai     = $this->session->userdata('id_pegawai');
		$cek_useraccess = $this->useraccess_pusat->cek_access($id_pegawai, '20');
		if($cek_useraccess->act_read == '0' or $cek_useraccess->act_read == '-'){
			redirect(base_url().'dashboard');
		}

		$kode_toko = $this->input->post('kode_toko');
		$dt        = $this->customer_pusat->cari_kode_customer_pusat($kode_toko);
		if($dt){
			$id_customer_pusat = $dt->id_customer_pusat;
		}else{
			$id_customer_pusat = '';
			exit();
		}

		$id_periode_stok = $this->input->post('id_periode_stok');	
		if($id_periode_stok == '' || $kode_toko == ''){
			exit();
		}

		$data_periode = $this->periode_stok_toko->get_by_id($id_periode_stok);
		if($data_periode){
			$tanggal_awal  = $data_periode->tanggal_periode_awal;
			$tanggal_akhir = $data_periode->tanggal_periode_akhir;
		}else{
			exit();
		}

		$list = $this->barang_toko->periksa_stok_toko(
			$kode_toko, $id_customer_pusat, $tanggal_awal, $tanggal_akhir
		);
		
		if($list){
			ini_set('max_execution_time', 0); 
			ini_set('memory_limit','2048M');
			foreach ($list as $barang_toko){
				$koreksi_stok = $barang_toko->total_stok - $barang_toko->total_stok_fix;
				if($koreksi_stok == '' or $koreksi_stok == '0'){
					$koreksi_stok = '0';
				}else{
					$this->barang_toko->update_stok_toko(
						$barang_toko->id_barang_pusat, $kode_toko, $barang_toko->total_stok_fix, $id_pegawai
					);
				}
			}

			echo json_encode(array(
				'status' => 1
			));
		}else{
			echo json_encode(array(
				'status' => 0
			));
		}
	}

	public function ajax_fix_harga_eceran_toko(){
		// Cek user acces menu
		$id_pegawai     = $this->session->userdata('id_pegawai');
		$cek_useraccess = $this->useraccess_pusat->cek_access($id_pegawai, '20');
		if($cek_useraccess->act_read == '0' or $cek_useraccess->act_read == '-'){
			redirect(base_url().'dashboard');
		}

		$kode_toko = $this->input->post('kode_toko');
		$dt        = $this->customer_pusat->cari_kode_customer_pusat($kode_toko);
		if($dt){
			$id_customer_pusat = $dt->id_customer_pusat;
		}else{
			$id_customer_pusat = '';
			exit();
		}

		$id_periode_stok = $this->input->post('id_periode_stok');	
		if($id_periode_stok == '' || $kode_toko == ''){
			exit();
		}

		$data_periode = $this->periode_stok_toko->get_by_id($id_periode_stok);
		if($data_periode){
			$tanggal_awal  = $data_periode->tanggal_periode_awal;
			$tanggal_akhir = $data_periode->tanggal_periode_akhir;
		}else{
			exit();
		}

		$list = $this->barang_toko->periksa_harga_ecean_toko(
			$kode_toko, $id_customer_pusat, $tanggal_awal, $tanggal_akhir
		);
		
		if($list){
			ini_set('max_execution_time', 0); 
			ini_set('memory_limit','2048M');
			foreach ($list as $barang_toko){
				$koreksi_harga = $barang_toko->harga_eceran_pusat - $barang_toko->harga_eceran;
				if($koreksi_harga == '' or $koreksi_harga == '0'){
					$koreksi_harga = '0';
				}else{
					$this->barang_toko->update_harga_eceran_toko(
						$barang_toko->id_barang_pusat, $kode_toko, $barang_toko->harga_eceran_pusat, $id_pegawai
					);
				}
			}

			echo json_encode(array(
				'status' => 1
			));
		}else{
			echo json_encode(array(
				'status' => 0
			));
		}
	}

	public function ajax_edit()
	{
		// Cek user acces menu
		$id_pegawai 	= $this->session->userdata('id_pegawai');
		$cek_useraccess = $this->useraccess_pusat->cek_access($id_pegawai, '20');
		if($cek_useraccess->act_update == '0' or $cek_useraccess->act_update == '-'){
			redirect(base_url().'barang_toko');
			exit();
		}

		$id_barang_toko = $this->input->post('id_barang_toko');
		if($id_barang_toko == ''){
			redirect(base_url().'barang_toko');
			exit();
		}

		$kode_toko = $this->input->post('kode_toko');
		if($kode_toko == ''){
			redirect(base_url().'barang_toko');
			exit();
		}

		$data = $this->barang_toko->get_by_id($id_barang_toko, $kode_toko);
		echo json_encode(array(
								'data' 		=> $data,
								'foto'		=> "<div class='thumb'>
													<img id='foto' name='foto' src='assets/upload/image/barang/thumbs/$data->foto' 
													class='thumb-img'>
												</div>"
						));
	}

	public function ajax_edit_barang_toko()
	{
		// Cek user acces menu
		$id_pegawai 					= $this->session->userdata('id_pegawai');
		$cek_useraccess 				= $this->useraccess_pusat->cek_access($id_pegawai, '20');
		if($cek_useraccess->act_update == '0' or $cek_useraccess->act_update == '-'){
			redirect(base_url().'dashboard');
			exit();
		}

		$id_barang_toko = $this->input->post('id_barang_toko');
		if($id_barang_toko == ''){
			redirect(base_url().'barang_toko');
			exit();
		}

		$data = $this->barang_toko->get_by_id($id_barang_toko);
		echo json_encode($data);
	}

	public function ajax_update()
	{
		// Cek user acces menu
		$id_pegawai     = $this->session->userdata('id_pegawai');
		$cek_useraccess = $this->useraccess_pusat->cek_access($id_pegawai, '20');
		if($cek_useraccess->act_update == '0' or $cek_useraccess->act_update == '-'){
			redirect(base_url().'dashboard');
			exit();
		}

		if($this->input->post('id_barang_toko') == ''){
			echo json_encode(array(
				"status" => 0,
				"pesan"  => 'Data barang tidak terdaftar'
			));
			// redirect(base_url().'barang_toko');
			exit();
		}

		if($this->input->post('kode_toko_edit') == ''){
			echo json_encode(array(
				"status" => 0,
				"pesan"  => 'Data toko tidak terdaftar'
			));
			// redirect(base_url().'barang_toko');
			exit();
		}else{
			$kode_toko = $this->input->post('kode_toko_edit');
		}
		
		ini_set('max_execution_time', 0); 
		ini_set('memory_limit', '2048M');
		$this->_validate();
		$data = array(
			'harga_eceran'           => str_replace('.','',$this->input->post('harga_eceran_hidden')),
			'status'                 => $this->input->post('status'),
			'id_pegawai_pembaharuan' => $this->session->userdata('id_pegawai')
		);
		$this->barang_toko->update(
			array('id_barang_toko' => $this->input->post('id_barang_toko')), 
			$data, 
			'vamr4846_toko_mrc.barang_toko_'.$kode_toko.''
		);
		echo json_encode(array(
			"status" => 1
		));
	}

	public function ajax_update_barang_toko()
	{
		ini_set('max_execution_time', 0); 
		ini_set('memory_limit', '2048M');
		$this->_validate_barang_toko();
		$data = array(
			'harga_eceran'           => $this->input->post('harga_eceran'),
			'status'                 => $this->input->post('status'),
			'id_pegawai_pembaharuan' => $this->session->userdata('id_pegawai')
		);
		$this->barang_toko->update(array('id_barang_toko' => $this->input->post('id_barang_toko')), $data);
		echo json_encode(array(
			"status"            => TRUE,
			"harga_eceran_toko" => $this->input->post('harga_eceran'),
			"status_data_toko"  => $this->input->post('status')
		));
	}

	public function ajax_verifikasi_delete()
	{
		// Cek user acces menu
		$id_pegawai 	= $this->session->userdata('id_pegawai');
		$cek_useraccess = $this->useraccess_pusat->cek_access($id_pegawai, '20');
		if($cek_useraccess->act_delete == '0' or $cek_useraccess->act_delete == '-'){
			redirect(base_url().'barang_toko');
			exit();
		}

		$id_barang_toko = $this->input->post('id_barang_toko');
		if($id_barang_toko == ''){
			redirect(base_url().'barang_toko');
			exit();
		}
		
		$data = $this->barang_toko->get_by_id($id_barang_toko);
		if($data->total_stok > 0){
			// Tampilkan informasi stok
			echo json_encode(array(
									'status' 	=> 0,
									'pesan' 	=> "<b class='text-danger'>Maaf barang ini tidak bisa dihapus !</b></br></br>
													Nama Toko : </br>
													<b class='text-dark'>".$data->nama_customer_pusat."</b></br></br>

													SKU : </br>
													<b class='text-dark'>".$data->sku."</b></br></br>

													Nama Barang : </br>
													<b class='text-dark'>".$data->nama_barang."</b></br></br>
													
													Karna masih memiliki stok sebanyak : </br>
													<h2 class='text-dark'>".$data->total_stok."</h2>",
									'footer'	=> 	"<button type='button' class='btn btn-primary waves-effect' 
													data-dismiss='modal'>Ok, Saya Mengerti</button>"
									)
							);
			exit();
		}

		if($data->total_stok_rusak > 0){
			// Tampilkan informasi stok
			echo json_encode(array(
									'status' 	=> 0,
									'pesan' 	=> "<b class='text-danger'>Maaf barang ini tidak bisa dihapus !</b></br></br>
													Nama Toko : </br>
													<b class='text-dark'>".$data->nama_customer_pusat."</b></br></br>

													SKU : </br>
													<b class='text-dark'>".$data->sku."</b></br></br>

													Nama Barang : </br>
													<b class='text-dark'>".$data->nama_barang."</b></br></br>
													
													Karna masih memiliki stok rusak sebanyak : </br>
													<h2 class='text-dark'>".$data->total_stok_rusak."</h2>",
									'footer'	=> 	"<button type='button' class='btn btn-primary waves-effect' 
													data-dismiss='modal'>Ok, Saya Mengerti</button>"
									)
							);
			exit();
		}

		echo json_encode(array(
								'pesan' 	=> "<b class='text-danger'>Yakin ingin menghapus barang toko ini ? </b></br></br>
												Nama Toko : </br>
												<b class='text-dark'>".$data->nama_customer_pusat."</b></br></br>

												SKU : </br>
												<b class='text-dark'>".$data->sku."</b></br></br>
												
												Nama Barang : </br>
												<b class='text-dark'>".$data->nama_barang."</b>",
								'footer'	=> 	"<button onclick='delete_barang_toko($id_barang_toko)' 
													type='button' class='btn btn-primary waves-effect waves-light' 
													data-dismiss='modal' autofocus>Iya, Hapus</button> 
												<button type='button' class='btn btn-default waves-effect' 
													data-dismiss='modal'>Batal</button>"
							  	)
						);
	}

	public function ajax_delete()
	{
		$id_pegawai 	= $this->session->userdata('id_pegawai');
		$cek_useraccess = $this->useraccess_pusat->cek_access($id_pegawai, '20');
		if($cek_useraccess->act_delete == '0' or $cek_useraccess->act_delete == '-'){
			redirect(base_url().'barang_toko');
			exit();
		}

		$id_barang_toko = $this->input->post('id_barang_toko');
		if($id_barang_toko == ''){
			redirect(base_url().'barang_toko');
			exit();
		}

		$this->barang_toko->update_status_hapus($id_barang_toko, $id_pegawai);
		echo json_encode(array("status" => TRUE));
	}

	private function _validate()
	{
		$data                 = array();
		$data['error_string'] = array();
		$data['inputerror']   = array();
		$data['status']       = TRUE;

		if($this->input->post('harga_eceran_hidden') == '0'){
			$data['inputerror'][] = 'harga_eceran';
			$data['error_string'][] = 'Harga eceran wajib diisi';
			$data['status'] = FALSE;
		}

		if($this->input->post('status') == ''){
			$data['inputerror'][] = 'status_data_toko';
			$data['error_string'][] = 'Status barang wajib dipilih';
			$data['status'] = FALSE;
		}

		if($data['status'] === FALSE){
			echo json_encode($data);
			exit();
		}
	}

	private function _validate_barang_toko()
	{
		$data = array();
		$data['error_string'] = array();
		$data['inputerror'] = array();
		$data['status'] = TRUE;

		if($this->input->post('harga_eceran') == '0'){
			$data['inputerror'][] = 'harga_eceran_toko';
			$data['error_string'][] = 'Harga eceran wajib diisi';
			$data['status'] = FALSE;
		}

		if($this->input->post('status') == ''){
			$data['inputerror'][] = 'status_data_toko';
			$data['error_string'][] = 'Status barang wajib dipilih';
			$data['status'] = FALSE;
		}

		if($data['status'] === FALSE){
			echo json_encode($data);
			exit();
		}
	}

	public function ajax_cetak_ke_excel()
	{
		// Cek user acces menu
		$id_pegawai     = $this->session->userdata('id_pegawai');
		$cek_useraccess = $this->useraccess_pusat->cek_access($id_pegawai, '20');
		if($cek_useraccess->act_read == '0' or $cek_useraccess->act_read == '-'){
			redirect(base_url().'dashboard');
		}

		$kode_toko = $this->input->post('kode_toko');
		$dt        = $this->customer_pusat->cari_kode_customer_pusat($kode_toko);
		if($dt){
			$id_toko   = $dt->id_customer_pusat;
			$nama_toko = $dt->nama_customer_pusat; 
		}else{
			echo json_encode(array(
				"status" => 0,
				"pesan"  => "Data toko tidak terdaftar!"
			));
			exit();
		}

		$id_periode_stok = $this->input->post('id_periode_stok');	
		if($id_periode_stok == '' || $kode_toko == ''){
			echo json_encode(array(
				"status" => 0,
				"pesan"  => "Periode stok tidak terdaftar!"
			));
			exit();
		}

		$data_periode = $this->periode_stok_toko->get_by_id($id_periode_stok);
		if($data_periode){
			$tanggal_awal  = $data_periode->tanggal_periode_awal;
			$tanggal_akhir = $data_periode->tanggal_periode_akhir;
		}else{
			echo json_encode(array(
				"status" => 0,
				"pesan"  => "Periode stok tidak terdaftar!"
			));
			exit();
		}
		// -------------------------------------------------------------------------------------------------------------

		$judul       = 'Master Barang Toko - '.$nama_toko.'';
		$data_barang = $this->barang_toko->ambil_master_barang($kode_toko, $id_toko, $tanggal_awal, $tanggal_akhir);
		if($data_barang){
			// $spreadsheet = new Spreadsheet();

			// Set document properties
			// $spreadsheet->getProperties()->setCreator($judul)
			// ->setLastModifiedBy('Ali')
			// ->setTitle('Office 2007 XLSX Test Document')
			// ->setSubject('Office 2007 XLSX Test Document')
			// ->setDescription('Test document for Office 2007 XLSX, generated using PHP classes.')
			// ->setKeywords('office 2007 openxml php')
			// ->setCategory('Test result file');

			// Add some data
			// $spreadsheet->setActiveSheetIndex(0)
			// 	->setCellValue('A1', 'SKU')
			// 	->setCellValue('B1', 'NAMA BARANG')
			// 	->setCellValue('C1', 'HARGA')
			// 	->setCellValue('D1', 'STOK REAL')
			// 	->setCellValue('D1', 'STOK TABLE')
			// 	->setCellValue('E1', 'TGL SAVE')
			// 	->setCellValue('F1', 'PEGAWAI SAVE')
			// 	->setCellValue('G1', 'TGL EDIT')
			// 	->setCellValue('H1', 'PEGAWAI EDIT')
			// ;

			// Miscellaneous glyphs, UTF-8
			// $i=2; foreach($data_barang as $data_barang) {
			// $spreadsheet->setActiveSheetIndex(0)
			// 	->setCellValue('A'.$i, $data_barang->sku)
			// 	->setCellValue('B'.$i, $data_barang->nama_barang)
			// 	->setCellValue('C'.$i, $data_barang->harga_eceran)
			// 	->setCellValue('D'.$i, $data_barang->total_stok_fix)
			// 	->setCellValue('D'.$i, $data_barang->total_stok)
			// 	->setCellValue('E'.$i, $data_barang->pegawai_save)
			// 	->setCellValue('F'.$i, $data_barang->tanggal_pembuatan)
			// 	->setCellValue('G'.$i, $data_barang->tanggal_pembaharuan)
			// 	->setCellValue('H'.$i, $data_barang->pegawai_edit);
			// 	$i++;
			// }

			// Rename worksheet
			// $spreadsheet->getActiveSheet()->setTitle('Master '.date('d-m-Y H'));

			// Set active sheet index to the first sheet, so Excel opens this as the first sheet
			// $spreadsheet->setActiveSheetIndex(0);

			// Redirect output to a client’s web browser (Xlsx)
			// header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
			// header('Content-Disposition: attachment;filename="Report Excel.xlsx"');
			// header('Cache-Control: max-age=0');
			// If you're serving to IE 9, then the following may be needed
			// header('Cache-Control: max-age=1');

			// If you're serving to IE over SSL, //then the following may be needed
			// header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
			// header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
			// header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
			// header('Pragma: public'); // HTTP/1.0

			// $writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
			// $writer->save('php://output');
			
			echo json_encode(array(
				"id_toko"       => $id_toko,
				"nama_toko"     => $nama_toko,
				"tanggal_awal"  => $tanggal_awal,
				"tanggal_akhir" => $tanggal_akhir,
				"judul"         => $judul,
				"data_barang"   => $data_barang,
				"status"        => 1,
				"pesan"         => "Data barang pusat berhasil di export ke excel."
			));
			// exit;
		}else{
			echo json_encode(array(
				"status" => 0,
				"pesan"  => "Tidak ada data barang pusat yang untuk di export ke excel."
			));
		}
	}
}
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Periode_stok_pusat extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->CI =& get_instance();
		$this->load->model('pegawai_pusat_model','pegawai_pusat');
        $this->load->model('useraccess_pusat_model','useraccess_pusat');

		$this->load->model('pegawai_pusat_model','pegawai_pusat');
		$this->load->model('periode_stok_pusat_model','periode_stok_pusat');
	}

	public function index()
	{
		// Cek user acces menu
		$id_pegawai = $this->session->userdata('id_pegawai');
		if($id_pegawai == ''){
			redirect(base_url().'login');
		}
		
		$cek_useraccess = $this->useraccess_pusat->cek_access($id_pegawai, '7');
		if($cek_useraccess){
			if($cek_useraccess->act_read == '0' or $cek_useraccess->act_read == '-'){
				redirect(base_url().'dashboard');
			}					
		}else{
			redirect(base_url().'dashboard');
		}

		$data_pegawai = $this->pegawai_pusat->get_by_id($id_pegawai);
		if($data_pegawai){
			// Buat kode baru
			$akhir   = $this->periode_stok_pusat->akhir();
			$inisial = "PS";

			if($akhir->id_periode_stok_pusat > 0) {
				$id_periode_stok_pusat = $akhir->id_periode_stok_pusat;
			}else{
				$id_periode_stok_pusat = 0;
			}

			$jumlah       = $id_periode_stok_pusat + 1;
			$kode_periode = $inisial.$jumlah;
			// Akhir buat kode baru		
			
			$data['data_pegawai']            = $data_pegawai;
			$data['data_induk_menu']         = $this->useraccess_pusat->get_induk_menu($id_pegawai);
			$data['access_create']           = $cek_useraccess->act_create;
			$data['atribut_halaman']         = 'Periode Stok Pusat';
			$data['kode_periode_stok_pusat'] = $kode_periode;
			$data['halaman_list']            = $this->load->view('admin/master_data/periode_stok_pusat/list',$data,true);
			$data['halaman_form']            = $this->load->view('admin/master_data/periode_stok_pusat/form',$data,true);
			$data['halaman_plugin']          = $this->load->view('admin/master_data/periode_stok_pusat/plugin',$data,true);
			$data['isi_halaman']             = $this->load->view('admin/master_data/periode_stok_pusat/index',$data,true);
			$this->load->view('admin/layout',$data);
		}else{
			redirect(base_url().'login');
		}
	}

	public function ajax_list()
	{
		// Cek user acces menu
		$id_pegawai     = $this->session->userdata('id_pegawai');
		$data_pegawai   = $this->pegawai_pusat->get_by_id($id_pegawai);
		$cek_useraccess = $this->useraccess_pusat->cek_access($id_pegawai, '7');
		$this->CI->session->set_userdata('usergroup_name', $data_pegawai->usergroup_name);
		if($cek_useraccess->act_read == '0' or $cek_useraccess->act_read == '-'){
			redirect(base_url().'dashboard');
		}

		$list = $this->periode_stok_pusat->get_datatables();
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $periode_stok_pusat) {
			$no++;
			$row = array();
			$row[] = $periode_stok_pusat->kode_periode_stok_pusat;
			$row[] = $periode_stok_pusat->tanggal_periode_awal;
			$row[] = $periode_stok_pusat->tanggal_periode_akhir;			
			$row[] = $periode_stok_pusat->pegawai_save;
			$row[] = $periode_stok_pusat->tanggal_pembuatan;

			//add html for action
			if($cek_useraccess->act_update == '0' or $cek_useraccess->act_update == '-'){
				$tombol_edit = '<b class="btn btn-rounded btn-xs btn-default text-muted text-center">Tidak ada akses</b>';
			}else{
				$tombol_edit = '<a class="btn btn-rounded btn-xs btn-primary" href="javascript:void(0)" title="Edit" 
									onclick="edit_periode_stok_pusat('."'".$periode_stok_pusat->id_periode_stok_pusat."'".')">
									<i class="fa fa-pencil"></i>
								</a>';
			}

			$row[]  = $tombol_edit;
			$data[] = $row;
		}

		$output = array(
					"draw" => $_POST['draw'],
					"recordsTotal" => $this->periode_stok_pusat->count_all(),
					"recordsFiltered" => $this->periode_stok_pusat->count_filtered(),
					"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}

	public function ajax_edit($id_periode_stok_pusat)
	{
		// Cek user acces menu
		$id_pegawai 	= $this->session->userdata('id_pegawai');
		$cek_useraccess = $this->useraccess_pusat->cek_access($id_pegawai, '7');
		if($cek_useraccess->act_update == '0' or $cek_useraccess->act_update == '-'){
			redirect(base_url().'periode_stok_pusat');
			exit();
		}

		$data = $this->periode_stok_pusat->get_by_id($id_periode_stok_pusat);
		echo json_encode($data);
	}

	public function ajax_add()
	{
		// Cek user acces menu
		$tanggal_sekarang = date('Y-m-d H:i:s');
		$id_pegawai       = $this->session->userdata('id_pegawai');
		$cek_useraccess   = $this->useraccess_pusat->cek_access($id_pegawai, '7');
		if($cek_useraccess->act_create == '0' or $cek_useraccess->act_create == '-'){
			redirect(base_url().'periode_stok_pusat');
			exit();
		}

		// Buat kode baru
		$akhir   = $this->periode_stok_pusat->akhir();
		$inisial = "PS";

		if($akhir->id_periode_stok_pusat > 0) {
			$id_periode_stok_pusat = $akhir->id_periode_stok_pusat;
			$tanggal_awal          = $akhir->tanggal_periode_awal;
			$tanggal_akhir         = $akhir->tanggal_periode_akhir;
		}else{
			$id_periode_stok_pusat = 0;
			$tanggal_awal          = '-';
			$tanggal_akhir         = '-';
		}

		$jumlah       = $id_periode_stok_pusat+1;
		$kode_periode = $inisial.$jumlah;
		// Akhir buat kode baru

		$this->_validate();
		// Insert periode stok pusat baru
		$data = array(
			'kode_periode_stok_pusat' => $kode_periode,
			'tanggal_periode_awal'    => $this->input->post('tanggal_periode_awal'),
			'tanggal_periode_akhir'   => $this->input->post('tanggal_periode_akhir'),
			'id_pegawai_pembuatan'    => $id_pegawai,
			'tanggal_pembuatan'       => date('Y-m-d H:i:s')
		);
		$insert = $this->periode_stok_pusat->save($data);
		// -----------------------------------------------------------------------------------------------------------------------

		// Update periode stok lama
		$tanggal_periode_akhir_lama     = $this->input->post('tanggal_periode_awal');
		$tanggal_periode_akhir_lama_fix = date('Y-m-d', strtotime($tanggal_periode_akhir_lama. ' - 1 days'));;

		$data = array(
			'tanggal_periode_akhir'  => $tanggal_periode_akhir_lama_fix,
			'id_pegawai_pembaharuan' => $this->session->userdata('username')
		);
		$this->periode_stok_pusat->update(
			array(
				'id_periode_stok_pusat' => $id_periode_stok_pusat
			), 
			$data
		);
		// -----------------------------------------------------------------------------------------------------------------------

		// Hitung stok sekarang kemudian insert stok opname pusat, riwayat stok opname pusat dan update stok dimaster barang pusat
		$daftar_stok_sekarang = $this->periode_stok_pusat->data_stok_sekarang(
			$jumlah, $tanggal_awal, $tanggal_akhir, $id_pegawai, $tanggal_sekarang
		);

		echo json_encode(array(
			"status" => TRUE
		));
	}

	public function ajax_update()
	{
		$this->_validate();
		$data = array(
			'tanggal_awal_periode'   => $this->input->post('tanggal_awal_periode'),
			'tanggal_akhir_periode'  => $this->input->post('tanggal_akhir_periode'),
			'id_pegawai_pembaharuan' => $this->session->userdata('username')
		);

		$this->periode_stok_pusat->update(
			array(
					'id_periode_stok_pusat' => $this->input->post('id_periode_stok_pusat')
				 ), 
			$data
		);
		echo json_encode(array("status" => TRUE));
	}

	private function _validate()
	{
		$data                 = array();
		$data['error_string'] = array();
		$data['inputerror']   = array();
		$data['status']       = TRUE;

		if($this->input->post('tanggal_periode_awal') == ''){
			$data['inputerror'][] = 'tanggal_periode_awal';
			$data['error_string'][] = 'Tanggal awal periode wajib diisi';
			$data['status'] = FALSE;
		}

		if($this->input->post('tanggal_periode_akhir') == ''){
			$data['inputerror'][] = 'tanggal_periode_akhir';
			$data['error_string'][] = 'Tanggal akhir periode wajib diisi';
			$data['status'] = FALSE;
		}

		if($data['status'] === FALSE){
			echo json_encode($data);
			exit();
		}
	}

}

<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kategori_barang extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('pegawai_pusat_model','pegawai_pusat');
        $this->load->model('useraccess_pusat_model','useraccess_pusat');
		$this->load->model('jenis_barang_model','jenis_barang');
		$this->load->model('kategori_barang_model','kategori_barang');
	}

	public function index()
	{
		// Cek user acces menu
		$id_pegawai = $this->session->userdata('id_pegawai');
		if($id_pegawai == ''){
			redirect(base_url().'dashboard');
		}
		
		$cek_useraccess = $this->useraccess_pusat->cek_access($id_pegawai, '18');
		if($cek_useraccess){
			if($cek_useraccess->act_read == '0' or $cek_useraccess->act_read == '-'){
				redirect(base_url().'dashboard');
			}					
		}else{
			redirect(base_url().'dashboard');
		}

		// cari data pegawai
		$id_pegawai 				= $this->session->userdata('id_pegawai');
		$data_pegawai				= $this->pegawai_pusat->get_by_id($id_pegawai);
		$data['access_create'] 		= $cek_useraccess->act_create;
		$data['access_update'] 		= $cek_useraccess->act_update;
		$data['access_delete'] 		= $cek_useraccess->act_delete;
		$data['data_pegawai']    	= $data_pegawai;
		$data['data_induk_menu'] 	= $this->useraccess_pusat->get_induk_menu($id_pegawai);

		$data['atribut_halaman'] 	= 'Kategori Barang';
		$data['list_jenis_barang']	= $this->jenis_barang->listing();
		$data['halaman_list'] 		= $this->load->view('admin/master_data/kategori_barang/list',$data,true);
		$data['halaman_form'] 		= $this->load->view('admin/master_data/kategori_barang/form',$data,true);
		$data['halaman_plugin'] 	= $this->load->view('admin/master_data/kategori_barang/plugin',$data,true);
		$data['isi_halaman'] 		= $this->load->view('admin/master_data/kategori_barang/index',$data,true);
		$this->load->view('admin/layout',$data);
	}

	public function ajax_list()
	{
		// Cek user acces menu
		$id_pegawai 	= $this->session->userdata('id_pegawai');
		$cek_useraccess = $this->useraccess_pusat->cek_access($id_pegawai, '18');
		if($cek_useraccess->act_read == '0' or $cek_useraccess->act_read == '-'){
			redirect(base_url().'kategori_barang');
			exit();
		}

		$id_periode_stok_pusat = $this->input->post('id_periode_stok_pusat');
		if($id_periode_stok_pusat == ''){
			redirect(base_url().'kategori_barang');
			exit();
		}

		$list 	= $this->kategori_barang->get_datatables();
		$data 	= array();
		$no 	= $_POST['start'];
		foreach ($list as $kategori_barang) {
			$no++;
			$row 	= array();
			$row[] 	= $no;

			//add html for action
			if($cek_useraccess->act_update == 1){
				$tombol_edit 	 = '<a class="btn btn-rounded btn-xs btn-default" href="javascript:void(0)" 
										title="Edit" onclick="edit_kategori_barang('."'".$kategori_barang->id_kategori_barang."'".')">
										<i class="fa fa-pencil" style="color: blue;"></i>
									</a>';
			}else{$tombol_edit 	 = '';}

			if($cek_useraccess->act_delete == 1){
				$tombol_hapus 	 = '<a class="btn btn-rounded btn-xs btn-default" href="javascript:void(0)" 
										title="Hapus" onclick="verifikasi_delete('."'".$kategori_barang->id_kategori_barang."'".')">
										<i class="fa fa-remove" style="color: red;"></i>
				  					</a>';
			}else{$tombol_hapus  = '';}

			if($cek_useraccess->act_update == 1 or $cek_useraccess->act_delete == 1){
				$row[] = '
						  	'.$tombol_edit.'
						  	'.$tombol_hapus.'
					  	';
			}

			$row[] 	= $kategori_barang->kode_jenis.' - '.$kategori_barang->nama_jenis;
			$row[] 	= $kategori_barang->kode_kategori;
			$row[] 	= $kategori_barang->nama_kategori;			
			
			$row[]	= $kategori_barang->pegawai_save;
			$row[] 	= $kategori_barang->tanggal_pembuatan;
			$row[] 	= $kategori_barang->pegawai_edit;
			$row[] 	= $kategori_barang->tanggal_pembaharuan;
			$data[] = $row;
		}

		$output = array(	"draw" 				=> $_POST['draw'],
							"recordsTotal" 		=> $this->kategori_barang->count_all(),
							"recordsFiltered"	=> $this->kategori_barang->count_filtered(),
							"data" 				=> $data,
						);
		echo json_encode($output);
	}

	public function ajax_edit()
	{
		// Cek user acces menu
		$id_pegawai 	= $this->session->userdata('id_pegawai');
		$cek_useraccess = $this->useraccess_pusat->cek_access($id_pegawai, '18');
		if($cek_useraccess->act_update == '0' or $cek_useraccess->act_update == '-'){
			redirect(base_url().'kategori_barang');
			exit();
		}

		$id_kategori_barang = $this->input->post('id_kategori_barang');
		if($id_kategori_barang == ''){
			redirect(base_url().'kategori_barang');
			exit();
		}

		$data = $this->kategori_barang->get_by_id($id_kategori_barang);
		echo json_encode($data);
	}

	public function ajax_add()
	{
		$id_pegawai 	= $this->session->userdata('id_pegawai');
		$cek_useraccess = $this->useraccess_pusat->cek_access($id_pegawai, '18');
		if($cek_useraccess->act_create == '0' or $cek_useraccess->act_create == '-'){
			redirect(base_url().'kategori_barang');
			exit();
		}

		// Buat kode baru
		$kode_jenis	= $this->input->post('kode_jenis');
		$akhir 		= $this->kategori_barang->akhir($kode_jenis);
		if($akhir){
			$urutan	= $akhir->jumlah_jenis;
		}else{
			$urutan	= '0';
		}

		if($urutan < 9){
			$depan = '0';
		}else{
			$depan = '';
		}

		$jumlah        = $urutan+1;
		$kode_kategori = $depan.$jumlah;
		// Akhir buat kode baru

		$this->_validate('0');

		
		if($kode_jenis == ''){
			redirect(base_url().'kategori_barang');
			exit();
		}

		$data = array(
			'kode_jenis'           => $kode_jenis,
			'kode_kategori'        => $kode_kategori,
			'nama_kategori'        => $this->input->post('nama_kategori_barang'),
			'id_pegawai_pembuatan' => $this->session->userdata('id_pegawai'),
			'tanggal_pembuatan'    => date('Y-m-d H:i:s')
		);
		$insert = $this->kategori_barang->save($data);
		echo json_encode(array("status" => TRUE));
	}

	public function ajax_update()
	{
		// Cek user acces menu
		$id_pegawai 	= $this->session->userdata('id_pegawai');
		$cek_useraccess = $this->useraccess_pusat->cek_access($id_pegawai, '18');
		if($cek_useraccess->act_update == '0' or $cek_useraccess->act_update == '-'){
			redirect(base_url().'kategori_barang');
			exit();
		}

		$kode_jenis	= $this->input->post('kode_jenis');
		if($kode_jenis == ''){
			redirect(base_url().'kategori_barang');
			exit();
		}

		$id_kategori_barang = $this->input->post('id_kategori_barang');
		$this->_validate($id_kategori_barang);
		
		$data = array(
						'nama_kategori' 			=> $this->input->post('nama_kategori_barang'),
						'id_pegawai_pembaharuan'	=> $this->session->userdata('id_pegawai')
					 );
		$this->kategori_barang->update(array('id_kategori_barang' => $id_kategori_barang), $data);
		echo json_encode(array("status" => TRUE));
	}

	public function ajax_verifikasi_delete()
	{
		// Cek user acces menu
		$id_pegawai 	= $this->session->userdata('id_pegawai');
		$cek_useraccess = $this->useraccess_pusat->cek_access($id_pegawai, '18');
		if($cek_useraccess->act_delete == '0' or $cek_useraccess->act_delete == '-'){
			redirect(base_url().'kategori_barang');
			exit();
		}

		$id_kategori_barang = $this->input->post('id_kategori_barang');
		if($id_kategori_barang == ''){
			redirect(base_url().'kategori_barang');
			exit();
		}
		
		$data = $this->kategori_barang->get_by_id($id_kategori_barang);		
		echo json_encode(array(
			'pesan' 	=> "<b class='text-danger'>Yakin ingin menghapus kategori barang ini ? </b></br></br>
							Jenis Barang : </br>
							<b class='text-dark'>".$data->nama_jenis."</b></br></br>
							
							Kategori Barang : </br>
							<h2 class='text-dark'>".$data->nama_kategori."</h2>",
			'footer'	=> 	"<button onclick='delete_kategori_barang($id_kategori_barang)' 
								type='button' class='btn btn-primary waves-effect waves-light' 
								data-dismiss='modal' autofocus>Iya, Hapus</button> 
							<button type='button' class='btn btn-default waves-effect' 
								data-dismiss='modal'>Batal</button>"
	  	));
	}

	public function ajax_delete()
	{
		$id_pegawai 	= $this->session->userdata('id_pegawai');
		$cek_useraccess = $this->useraccess_pusat->cek_access($id_pegawai, '18');
		if($cek_useraccess->act_delete == '0' or $cek_useraccess->act_delete == '-'){
			redirect(base_url().'kategori_barang');
			exit();
		}

		$id_kategori_barang = $this->input->post('id_kategori_barang');
		if($id_kategori_barang == ''){
			redirect(base_url().'kategori_barang');
			exit();
		}

		$id_pegawai = $this->session->userdata('id_pegawai');
		$this->kategori_barang->update_status_hapus($id_kategori_barang, $id_pegawai);
		echo json_encode(array("status" => TRUE));
	}

	private function _validate($id_kategori_barang)
	{
		$data = array();
		$data['error_string'] = array();
		$data['inputerror'] = array();
		$data['status'] = TRUE;
		$nama_kategori_barang = $this->input->post('nama_kategori_barang');

		if($this->input->post('kode_jenis') == ''){
			$data['inputerror'][] = 'nama_jenis';
			$data['error_string'][] = 'Jenis barang wajib di pilih';
			$data['status'] = FALSE;
		}

		if($nama_kategori_barang == ''){
			$data['inputerror'][] = 'nama_kategori_barang';
			$data['error_string'][] = 'Nama kategori barang wajib diisi';
			$data['status'] = FALSE;
		}

		$validasi_nama = $this->kategori_barang->get_by_nama($nama_kategori_barang);
		if($validasi_nama){
			if($id_kategori_barang <> $validasi_nama->id_kategori_barang){
				$data['inputerror'][] = 'nama_kategori_barang';
				$data['error_string'][] = 'Nama kategori barang sudah digunakan, harap ganti nama kategori barang';
				$data['status'] = FALSE;
			}
		}

		if($data['status'] === FALSE){
			echo json_encode($data);
			exit();
		}
	}

	public function ajax_pencarian_kategori_barang()
	{
		if($this->input->is_ajax_request()){
			$keyword 			= $this->input->post('keyword');
			$kodejenis 			= $this->input->post('kodejenis');
			$kategori_barang 	= $this->kategori_barang->ambil_kategori_barang($keyword,$kodejenis);

			if($kategori_barang->num_rows() > 0){
				$json['status'] 	= 1;
				$json['kodejenis'] 	= $kodejenis;
				$json['datanya'] 	= "<ul id='daftar_autocompleted_kategori_barang' class='list-group'>";
				foreach($kategori_barang->result() as $k){
					$json['datanya'] .= "
						                <li class='list-group-item'>
						                	".$k->kode_kategori." - 
											<span id='data_kode_kategori' style='display:none;'>".$k->kode_kategori."</span>
											<span id='data_nama_kategori'>".$k->nama_kategori."</span>
										</li>
										";
				}
				$json['datanya'] .= "</ul>";
			}else{
				$json['status'] 		= 0;
				$json['kodejenis'] 		= $kodejenis;
				// $json['perintah_sql'] 	= $this->db->last_query();				
			}

			echo json_encode($json);
		}
	}

	public function list_kategori_barang(){
		$id_pegawai = $this->session->userdata('id_pegawai');
		if($id_pegawai){		
			$nk            = $this->input->get('nk');
			$kode_jenis    = $this->input->get('kode_jenis');
			$data_kategori = $this->kategori_barang->cari_kategori_barang($nk, $kode_jenis);
			if($data_kategori){
				echo json_encode($data_kategori);
			}else{
				echo json_encode('');
			}
		}else{
			redirect(base_url().'dashboard');
			exit();
		}
	}
}

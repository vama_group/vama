<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Retur_penjualan extends MY_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->CI =& get_instance();
		$this->load->model('pegawai_pusat_model','pegawai_pusat');
        $this->load->model('useraccess_pusat_model','useraccess_pusat');
		$this->load->model('customer_pusat_model','customer_pusat');
		$this->load->model('barang_pusat_model','barang_pusat');
		$this->load->model('penjualan_master_model','penjualan_master');
		$this->load->model('penjualan_detail_model','penjualan_detail');
		$this->load->model('retur_penjualan_master_model','retur_penjualan_master');
		$this->load->model('retur_penjualan_detail_model','retur_penjualan_detail');
	}

	public function index()
	{
		// Cek user acces menu
		$id_pegawai = $this->session->userdata('id_pegawai');
		if($id_pegawai == ''){
			redirect(base_url().'login');
		}
		
		$cek_useraccess = $this->useraccess_pusat->cek_access($id_pegawai, '28');
		if($cek_useraccess){
			if($cek_useraccess->act_read == '0' or $cek_useraccess->act_read == '-'){
				redirect(base_url().'dashboard');
			}					
		}else{
			redirect(base_url().'dashboard');
		}

		$data_pegawai = $this->pegawai_pusat->get_by_id($id_pegawai);
		if($data_pegawai){
			$cek_useraccess_laporan = $this->useraccess_pusat->cek_access($id_pegawai, '42');		
			$data['data_pegawai']         = $data_pegawai;
			$data['data_induk_menu']      = $this->useraccess_pusat->get_induk_menu($id_pegawai);
			$data['access_create']        = $cek_useraccess->act_create;
			$data['access_update']        = $cek_useraccess->act_update;
			$data['access_laporan']       = $cek_useraccess_laporan->act_read;
			$data['atribut_halaman']      = 'Retur Penjualan Pusat';
			$data['id_retur_penjualan_m'] = '0';
			$data['halaman_transaksi']    = $this->load->view('admin/transaksi/retur_penjualan/transaksi',$data,true);
			$data['halaman_plugin']       = $this->load->view('admin/transaksi/retur_penjualan/plugin',$data,true);
			$data['isi_halaman']          = $this->load->view('admin/transaksi/retur_penjualan/index',$data,true);
			$this->load->view('admin/layout',$data);
		}else{
			redirect(base_url().'login');
		}
	}

	public function ajax_cari_no_penjualan()
	{
		if($this->input->is_ajax_request()){
			// Cek user acces menu
			$id_pegawai     = $this->session->userdata('id_pegawai');
			$cek_useraccess = $this->useraccess_pusat->cek_access($id_pegawai, '28');
			if($cek_useraccess->act_create == '1'){
				$no_penjualan   = $this->input->post('no_penjualan');
				$data_penjualan = $this->penjualan_master->get_by_no_penjualan($no_penjualan);
				if($data_penjualan){
					// Validasi id_penjualan_m tidak kosong
					if($data_penjualan->id_penjualan_m !== null){
						// Validasi jika sudah ada retur
						if($data_penjualan->id_retur_penjualan_m <> ''){
							$json['status'] = 0;
							$json['pesan']  = "Maaf no. penjualan ini sudah di retur,
											   No. Penjualan : $no_penjualan,
											   No. Retur Penjualan : $data_penjualan->no_retur_penjualan
											   Total Retur : Rp. ".number_format($data_penjualan->total_retur,'0',',','.')."
										   	   ";
						}else{
							if($data_penjualan->status_penjualan !== 'SELESAI'){
								$json['status'] = 0;
								$json['pesan']  = "Maaf no. penjualan ini masih dalam status : ".$data_penjualan->status_penjualan.",
												   No. Penjualan : $no_penjualan,
												   Total Penjualan : Rp. ".number_format($data_penjualan->grand_total,'0',',','.')."
											   	   ";
							}else{
								$json['status'] = 1;
								$json['data']   = $data_penjualan;
							}
						}
					}else{
						$json['status'] = 0;
						$json['pesan']  = "No. Penjualan : $no_penjualan
										   Tidak terdaftar.";	
					}
				}else{
					$json['status'] = 0;
					$json['pesan']  = "No. Penjualan : $no_penjualan
									   Tidak terdaftar.";
				}
			}else{
				$json['status'] = 0;
				$json['pesan']  = "Maaf anda tidak diizinkan untuk menambahkan data retur penjualan.";
			}
			echo json_encode($json);
		}else{
			redirect(base_url().'retur_penjualan');
		}
	}

	public function transaksi()
	{
		$id_retur_penjualan_m = $this->input->get('id');
		$data_retur_penjualan = $this->retur_penjualan_master->get_master($id_retur_penjualan_m);
		if($data_retur_penjualan->id_retur_penjualan_m == ''){
			redirect(base_url().'retur_penjualan');
			exit();
		}

		// Cek user acces menu
		$id_pegawai             = $this->session->userdata('id_pegawai');
		$cek_useraccess_laporan = $this->useraccess_pusat->cek_access($id_pegawai, '42');
		if($cek_useraccess_laporan->act_update == '1' or $data_retur_penjualan->status_retur == 'MENUNGGU'){
			$cek_useraccess         = $this->useraccess_pusat->cek_access($id_pegawai, '28');
			$data['access_laporan'] = $cek_useraccess_laporan->act_read;
			$data['access_create']  = $cek_useraccess->act_create;
			$data['access_update']  = $cek_useraccess->act_update;

			// cari data pegawai
			$id_pegawai                   = $this->session->userdata('id_pegawai');
			$data_pegawai                 = $this->pegawai_pusat->get_by_id($id_pegawai);
			$this->CI->session->set_userdata('usergroup_name', $data_pegawai->usergroup_name);
			$data['data_pegawai']         = $data_pegawai;
			$data['data_induk_menu']      = $this->useraccess_pusat->get_induk_menu($id_pegawai);
			$data['atribut_halaman']      = 'Retur Penjualan Pusat';
			$data['id_retur_penjualan_m'] = $id_retur_penjualan_m;
			$data['no_retur_penjualan']   = $data_retur_penjualan->no_retur_penjualan;
			$data['halaman_transaksi']    = $this->load->view('admin/transaksi/retur_penjualan/transaksi',$data,true);
			$data['halaman_plugin']       = $this->load->view('admin/transaksi/retur_penjualan/plugin',$data,true);
			$data['isi_halaman']          = $this->load->view('admin/transaksi/retur_penjualan/index',$data,true);
			$this->load->view('admin/layout',$data);
		}else{
			redirect(base_url().'retur_penjualan');
			exit();
		}
	}

	public function simpan_transaksi(){
		if( ! empty($_POST['id_retur_penjualan_m'])){
			// Cek user acces menu
			$url            = $this->input->post('url');
			$id_pegawai     = $this->session->userdata('id_pegawai');
			$cek_useraccess = $this->useraccess_pusat->cek_access($id_pegawai, '28');
			$id_penjualan_m = $this->input->post('id_penjualan_m');

			if($cek_useraccess->act_create == '1' or $cek_useraccess->act_update == '1'){
				$id_retur_penjualan_m = $this->input->post('id_retur_penjualan_m');
				$no_retur_penjualan   = $this->input->post('no_retur_penjualan');
				$no_penjualan         = $this->input->post('no_penjualan');
				$id_penjualan_m       = $this->input->post('id_penjualan_m');
				$tanggal              = date('Y-m-d H:i:s');
				$id_pegawai           = $this->session->userdata('id_pegawai');
				$id_customer_pusat    = $this->input->post('id_customer_pusat');
				$catatan              = $this->clean_tag_input($this->input->post('catatan'));

				// Validasi retur penjualan
				$data_retur = $this->retur_penjualan_master->get_by_id_retur($id_retur_penjualan_m);
				if($data_retur){
					$master = $this->retur_penjualan_master->update_master_tanpa_total(
						$id_retur_penjualan_m, $id_penjualan_m, $no_penjualan, $id_customer_pusat,
						$tanggal, $catatan, $id_pegawai, 'SELESAI'
					);
					if($master){
						$data['status'] = 1;
						$data['pesan']  = "Transaksi berhasil disimpan,
										   No. Retur Penjualan : ".$no_retur_penjualan.",
										   Apakah anda ingin mencetak fakturnya ?";
						echo json_encode($data);
					}else{
						$data['status'] = 0;
						$data['pesan']  = "Terjadi kesalahan saat menyimpan transaksi, harap coba kembali.";
						$data['url']    = "retur_penjualan/transaksi/?&id=".$id_penjualan_m;
						echo json_encode($data);
					}
				}else{
					$data['status'] = 0;
					$data['pesan']  = "No. retur penjualan tidak terdaftar, harap hubungi administrator.";
					$data['url']    = "retur_penjualan/transaksi/?&id=".$id_retur_penjualan_m;
					echo json_encode($data);
				}
			}else{
				$data['status'] = 0;
				$data['pesan']  = "Maaf anda tidak diizinkan untuk menyimpan transaksi retur penjualan.";
				$data['url']    = "retur_penjualan/transaksi/?&id=".$id_retur_penjualan_m;
				echo json_encode($data);
			}
		}else{
			redirect(base_url().'retur_penjualan');
		}
	}

	public function tahan_transaksi(){
		if( ! empty($_POST['id_retur_penjualan_m'])){
			// Cek user acces menu
			$url            = $this->input->post('url');
			$id_pegawai     = $this->session->userdata('id_pegawai');
			$cek_useraccess = $this->useraccess_pusat->cek_access($id_pegawai, '28');
			$id_penjualan_m = $this->input->post('id_penjualan_m');

			if($cek_useraccess->act_create == '1' or $cek_useraccess->act_update == '1'){
				$id_retur_penjualan_m = $this->input->post('id_retur_penjualan_m');
				$no_retur_penjualan   = $this->input->post('no_retur_penjualan');
				$no_penjualan         = $this->input->post('no_penjualan');
				$id_penjualan_m       = $this->input->post('id_penjualan_m');
				$tanggal              = date('Y-m-d H:i:s');
				$id_pegawai           = $this->session->userdata('id_pegawai');
				$id_customer_pusat    = $this->input->post('id_customer_pusat');
				$catatan              = $this->clean_tag_input($this->input->post('catatan'));

				// Validasi status retur penjualan
				$data_retur = $this->retur_penjualan_master->get_by_id_retur($id_retur_penjualan_m);
				if($data_retur){
					if($data_retur->status_retur == 'MENUNGGU'){
						$master = $this->retur_penjualan_master->update_master_tanpa_total(
							$id_retur_penjualan_m, $id_penjualan_m, $no_penjualan, $id_customer_pusat,
							$tanggal, $catatan, $id_pegawai, 'MENUNGGU'
						);
						if($master){
							$data['status'] = 1;
							$data['pesan']  = "Transaksi berhasil ditahan,
											   No. Retur Penjualan : ".$no_retur_penjualan."";
							echo json_encode($data);
						}else{
							$data['status'] = 0;
							$data['pesan']  = "Terjadi kesalahan saat menahan transaksi, harap coba kembali.";
							$data['url']    = "retur_penjualan/transaksi/?&id=".$id_retur_penjualan_m;
							echo json_encode($data);
						}
					}else{
						$data['status'] = 0;
						$data['pesan']  = "	Maaf transaksi dengan,
											No. Retur Penjualan : ".$data_retur->no_retur_penjualan.",
											sudah tidak bisa ditahan,
											Karna sudah ".$data_retur->status_retur."";
						$data['url']    = "retur_penjualan/transaksi/?&id=".$data_retur->id_retur_penjualan_m;
						echo json_encode($data);
					}
				}else{
					$data['status'] = 0;
					$data['pesan']  = "No. retur penjualan tidak terdaftar, harap hubungi administrator.";
					$data['url']    = "retur_penjualan/transaksi/?&id=".$id_retur_penjualan_m;
					echo json_encode($data);
				}
			}else{
				$data['status'] = 0;
				$data['pesan']  = "Maaf anda tidak diizinkan untuk menyimpan transaksi retur penjualan.";
				$data['url']    = "retur_penjualan/transaksi/?&id=".$id_retur_penjualan_m;
				echo json_encode($data);
			}
		}else{
			redirect(base_url().'retur_penjualan');
		}
	}

	public function ambil_data()
	{
		$id_retur_penjualan_m    = $this->input->post('id_retur_penjualan_m');
		if($id_retur_penjualan_m == ''){
			redirect(base_url().'retur_penjualan');
			exit();
		}

		// Cek user acces menu
		$id_pegawai                  = $this->session->userdata('id_pegawai');
		$cek_useraccess              = $this->useraccess_pusat->cek_access($id_pegawai, '28');
		if($cek_useraccess->act_read == '0' or $cek_useraccess->act_read == '-'){
			redirect(base_url().'retur_penjualan');
			exit();
		}

		$data_induk  = $this->retur_penjualan_master->get_master($id_retur_penjualan_m);
		$data_batal  = $this->retur_penjualan_master->get_jumlah_batal($id_retur_penjualan_m);
		$jumlah_jual = $this->penjualan_master->get_jumlah_jual($data_induk->id_penjualan_m);

		echo json_encode(array(
			'data_induk' => $data_induk,
			'data_batal' => $data_batal,
			'data_jual'  => $jumlah_jual
		));
	}

	private function no_retur_penjualan_baru(){
		$id_pegawai    = $this->session->userdata('id_pegawai');
		if($id_pegawai == ''){
			redirect(base_url().'retur_penjualan');
			exit();
		}

		$id_pegawai       = $this->session->userdata('id_pegawai');
		$tanggal_sekarang = date('Y-m-d');
		$bulan_sekarang   = date('m');
		$tahun_sekarang   = date('Y');
		$tahun_sekarang_2 = date('y');
		
		$akhir            = $this->retur_penjualan_master->no_retur_penjualan_baru($tahun_sekarang, $bulan_sekarang, $id_pegawai);
		$inisial          = "RPJ";

		if($akhir) {
			$no_baru = $akhir->no_baru;
		}else{
			$no_baru = 0;
		}

		if($no_baru < 10){
		$depan = '000';
		}elseif($no_baru < 100) {
		$depan = '00';
		}elseif($no_baru < 1000) {
		$depan = '0';
		}else{
		$depan = '';
		}

		$jumlah                  = $no_baru;
		$no_retur_penjualan_baru = $inisial.'-'.$bulan_sekarang.$tahun_sekarang_2.'-'.$id_pegawai.'-'.$depan.$jumlah;
		return $no_retur_penjualan_baru;
	}

	public function simpan_detail()
	{
		// Cek user acces menu
		$id_pegawai      = $this->session->userdata('id_pegawai');
		$cek_useraccess  = $this->useraccess_pusat->cek_access($id_pegawai, '28');
		$id_barang_pusat = $this->input->post('id_barang_pusat');
		if($id_barang_pusat == ''){
			redirect(base_url().'retur_penjualan');
			exit();
		}

		// Ambil data penjualan master
		$status_simpan        = 0;
		$no_retur_penjualan   = $this->clean_tag_input($this->input->post('no_retur_penjualan'));
		$id_retur_penjualan_m = $this->input->post('id_retur_penjualan_m');
		$id_penjualan_m       = $this->input->post('id_penjualan_m');
		$no_penjualan         = $this->input->post('no_penjualan');
		$id_customer_pusat    = $this->input->post('id_customer_pusat');
		$id_penjualan_d       = $this->input->post('id_penjualan_d');
		$id_barang_pusat      = $this->input->post('id_barang_pusat');
		$sku                  = $this->input->post('sku');
		$harga_satuan         = $this->input->post('harga_satuan');
		$potongan_harga_value = $this->input->post('potongan_harga_value');
		$potongan_harga       = $this->input->post('potongan_harga');
		$jumlah_beli          = $this->input->post('jumlah_beli');
		$jumlah_retur         = $this->input->post('jumlah_retur');
		$subtotal_potongan    = $this->input->post('subtotal_potongan');
		$subtotal_saldo       = $this->input->post('subtotal_saldo');
		$subtotal_retur       = $this->input->post('subtotal_retur');
		$masuk_stok           = $this->input->post('masuk_stok');
		$keterangan_retur     = $this->clean_tag_input($this->input->post('keterangan_retur'));
		$catatan              = $this->clean_tag_input($this->input->post('catatan'));
		$tanggal              = date('Y-m-d H:i:s');
		$id_pegawai           = $this->session->userdata('id_pegawai');

		if($jumlah_retur > $jumlah_beli){
			echo json_encode(array(
				'status' => 0,
				'pesan'  => 'Jumlah retur melebihi jumlah jual,
							 Harap kurangin jumlah retur.',
				'url'    => 'retur_penjualan/transaksi/?&id='.$id_retur_penjualan_m
			));
			exit();
		}

		// Validasi user access
		if($cek_useraccess->act_create == '0' AND $cek_useraccess->act_update == '0'){
			echo json_encode(array(
				'status' => 0,
				'pesan'  => 'Maaf anda tidak diizinkan untuk menyimpan data retur penjualan barang.',
				'url'    => 'retur_penjualan/transaksi/?&id='.$id_retur_penjualan_m
			));
			exit();
		}
		
		// Validasi customer
		$data_customer = $this->customer_pusat->get_by_id($id_customer_pusat);
		if($data_customer){
			$tipe_customer_pusat = $data_customer->tipe_customer_pusat;
		}else{
			echo json_encode(array(
									'status' => 0,
									'pesan'  => 'Data customer tidak terdaftar,
												 Harap hubungi administrator.',
									'url'    => 'retur_penjualan/transaksi/?&id='.$id_retur_penjualan_m
							));
			exit();
		}

		// Validasi retur penjualan master sudah ada atau belum
		$data_retur_penjualan_master = $this->retur_penjualan_master->get_by_id_penjualan($id_penjualan_m);
		if($data_retur_penjualan_master){
			$id_retur_penjualan_m = $data_retur_penjualan_master->id_retur_penjualan_m;
			$tipe_customer_pusat  = $data_retur_penjualan_master->tipe_customer_pusat;
			$status_simpan        = 1;
		}else{
			if($cek_useraccess->act_create == 1){
				$id_retur_penjualan_m = '0';
				$status_simpan        = 1;
			}else{
				echo json_encode(array(
					'status' => 0,
					'pesan'  => 'Maaf anda tidak diizinkan untuk menambahkan data retur penjualan barang.',
					'url'    => 'retur_penjualan/transaksi/?&id='.$id_retur_penjualan_m
				));
				exit();
			}
		}

		if($status_simpan == 1){
			// Validasi insert atau update di retur  penjualan detail dan ambil jumlah beli sebelumnya
			$data_penjualan_retur_detail = $this->retur_penjualan_detail->get_id($id_retur_penjualan_m, $id_barang_pusat);
			if($data_penjualan_retur_detail){
				$id_detail         = $data_penjualan_retur_detail->id_retur_penjualan_d;
				$jumlah_retur_lama = $data_penjualan_retur_detail->jumlah_retur;
				$masuk_stok_lama   = $data_penjualan_retur_detail->masuk_stok;
			}else{
				$id_detail         = '';
			}

			if($id_detail){
				if($cek_useraccess->act_update == 1){
					// update dan kembalikan stok sebelumnya
					$simpan_data = $this->retur_penjualan_detail->update_detail(
					  $id_retur_penjualan_m, $id_detail, $id_barang_pusat, $harga_satuan, $potongan_harga_value,
					  $potongan_harga, $jumlah_beli, $jumlah_retur, $subtotal_potongan, $subtotal_saldo,
					  $subtotal_retur, $masuk_stok, $keterangan_retur, $id_pegawai,
					  $jumlah_retur_lama, $masuk_stok_lama
				  	);
				 }else{
				 	echo json_encode(array(
						'status' => 0,
						'pesan'  => 'Maaf anda tidak diizinkan untuk mengedit data retur penjualan barang.',
						'url'    => 'retur_penjualan/transaksi/?&id='.$id_retur_penjualan_m
					));
					exit();
				 }
			}else{
				if($cek_useraccess->act_create == 1){
					if($id_retur_penjualan_m == '0' or $id_retur_penjualan_m == ''){
						// Insert retur penjualan master
						$no_retur_penjualan = $this->no_retur_penjualan_baru();
						$master             = $this->retur_penjualan_master->insert_master
						(
							$no_retur_penjualan, $id_penjualan_m, $no_penjualan, 
							$id_customer_pusat, $tipe_customer_pusat,
							'0', '0', '0', $tanggal, $catatan, $id_pegawai
						);
						
						$data_retur_penjualan_master = $this->retur_penjualan_master->get_id($no_retur_penjualan);
						$id_retur_penjualan_m        = $data_retur_penjualan_master->id_retur_penjualan_m;
					}

					// insert retur penjualan detail
					$simpan_data = $this->retur_penjualan_detail->insert_detail
					(
						$id_retur_penjualan_m, $id_penjualan_d, $id_barang_pusat, $harga_satuan, $potongan_harga_value,
						$potongan_harga, $jumlah_beli, $jumlah_retur, $subtotal_potongan,
						$subtotal_saldo, $subtotal_retur, $masuk_stok, $keterangan_retur,$id_pegawai
					);
					$masuk_stok_lama = '-';
				}else{
					echo json_encode(array(
						'status' => 0,
						'pesan'  => 'Maaf anda tidak diizinkan untuk menambahkan data retur penjualan barang.',
						'url'    => 'retur_penjualan/transaksi/?&id='.$id_retur_penjualan_m
					));
					exit();	
				}
			}

			if($simpan_data){
				if($masuk_stok == 'JUAL'){
					$this->barang_pusat->update_tambah_stok($id_barang_pusat, $jumlah_retur);
				}elseif($masuk_stok == 'RUSAK'){
					$this->barang_pusat->update_tambah_stok_rusak($id_barang_pusat, $jumlah_retur);
				}

				$data_total_retur_penjualan = $this->retur_penjualan_detail->get_total($id_retur_penjualan_m);
				if($data_total_retur_penjualan){
					$ttl_potongan = $data_total_retur_penjualan->total_potongan;
					$ttl_saldo    = $data_total_retur_penjualan->total_saldo;
					$ttl_retur    = $data_total_retur_penjualan->total_retur;
					$jml_barang   = $data_total_retur_penjualan->jumlah_barang;
					
					// Update total dan grandtotal di penjualan master
					$master       = $this->retur_penjualan_master->update_master(
						$id_retur_penjualan_m, $id_penjualan_m, $no_penjualan, 
						$id_customer_pusat, $tipe_customer_pusat,
						$ttl_potongan, $ttl_saldo, $ttl_retur,
						$tanggal, $catatan, $id_pegawai
					);
				}else{
					$ttl_potongan = '0';
					$ttl_saldo    = '0';
					$ttl_retur    = '0';
					$jml_barang   = '0';
				}

				$data_batal  = $this->retur_penjualan_master->get_jumlah_batal($id_retur_penjualan_m);
				if($data_batal){
					$jumlah_batal = $data_batal->jumlah_barang;
				}else{
					$jumlah_batal = '0';
				}

				echo json_encode(array(
										'status'              => 1,
										'pesan'               => "Data penjualan barang berhasil disimpan",
										'url'                 => 'retur_penjualan/transaksi/?&id='.$id_retur_penjualan_m,
										'id_rpm'              => $id_retur_penjualan_m,
										'id_rpd'              => $id_detail,
										'masuk_stok'          => $masuk_stok,
										'masuk_stok_lama'     => $masuk_stok_lama,
										'jumlah_barang'       => $jml_barang,
										'jumlah_barang_batal' => $jumlah_batal,
										'total_potongan'      => $ttl_potongan,
										'total_saldo'         => $ttl_saldo,
										'total_retur'         => $ttl_retur
								));
			}else{
				echo json_encode(array(
										'status' => 0,
										'pesan'  => 'Data barang retur penjualan gagal disimpan'
								));
			}
		}
	}

	public function ajax_list_retur()
	{
		$id_penjualan_m    = $this->clean_tag_input($this->input->post('id_penjualan_m'));
		if($id_penjualan_m == ''){
			redirect(base_url().'retur_penjualan');
			exit();
		}

		// Cek user acces menu
		$id_pegawai                    = $this->session->userdata('id_pegawai');
		$cek_useraccess                = $this->useraccess_pusat->cek_access($id_pegawai, '28');
		$cek_useraccess_laporan        = $this->useraccess_pusat->cek_access($id_pegawai, '42');
		if($cek_useraccess->act_read == '0' AND $cek_useraccess->act_read == '0'){
			redirect(base_url().'dashboard');
			exit();
		}

		$list = $this->retur_penjualan_detail->get_datatables('_get_datatables_query', $id_penjualan_m);
		$data = array();
		$no   = $_POST['start'];
		foreach ($list as $pd){
			$no++;
			$row   = array();
			$row[] = $no;

			if($pd->subtotal_retur !='0'){
				if($cek_useraccess->act_update == 1){
					if($pd->status_retur == "SELESAI"){
						if($cek_useraccess_laporan->act_update == 1){
							$tombol_edit = '<a class="btn btn-rounded btn-default btn-xs"
												onclick="edit_penjualan_detail('."'".$pd->id_penjualan_d."'".')">
												<i class="fa fa-pencil" style="color:blue;"></i>
											</a>';
						}else{
							$tombol_edit = '';
						}
					}else{
						$tombol_edit = '<a class="btn btn-rounded btn-default btn-xs"
											onclick="edit_penjualan_detail('."'".$pd->id_penjualan_d."'".')">
											<i class="fa fa-pencil" style="color:blue;"></i>
										</a>';
					}
				}else{
					$tombol_edit = '';
				}

				if($cek_useraccess->act_delete == 1){
					if($pd->status_retur == "SELESAI"){
						if($cek_useraccess_laporan->act_delete == 1){
							$tombol_hapus = '<a class="btn btn-rounded btn-default btn-xs"
									  			onclick="verifikasi_hapus_retur_penjualan_detail('."'".$pd->id_retur_penjualan_d."'".')">
									  			<i class="fa fa-times" style="color:red;"></i>
									  		</a>';
						}else{
							$tombol_hapus = '';
						}
					}else{
						$tombol_hapus = '<a class="btn btn-rounded btn-default btn-xs"
								  			onclick="verifikasi_hapus_retur_penjualan_detail('."'".$pd->id_retur_penjualan_d."'".')">
								  			<i class="fa fa-times" style="color:red;"></i>
								  		</a>';
					}
				}else{
					$tombol_hapus = '';
				}
			}else{
				if($cek_useraccess->act_create == 1){
					$tombol_edit  =	'	<a class="btn btn-rounded btn-default btn-xs"
											onclick="edit_penjualan_detail('."'".$pd->id_penjualan_d."'".')">
											<i class="fa fa-shopping-basket" style="color: green;"></i>
										</a>
									';
				}else{
					$tombol_edit = '';
				}
				$tombol_hapus = '';
			}

			$row[] =	'	'.$tombol_edit.'
							'.$tombol_hapus.'
						';
			
			$row[] = $pd->sku;
			$row[] = $pd->nama_barang;
			
			$row[]  = '<span class="pull-right">'.number_format($pd->harga_satuan,'0',',','.').'</span>';
			$row[]  = '<span class="pull-right">'.number_format($pd->jumlah_beli,'0',',','.').'</span>';
			$row[]  = '<span class="pull-right">'.number_format($pd->jumlah_retur,'0',',','.').'</span>';
			$row[]  = '<span class="pull-right">'.number_format($pd->subtotal_retur,'0',',','.').'</span>';
			$row[]  = '<span class="pull-right">'.number_format($pd->subtotal_potongan,'0',',','.').'</span>';
			$row[]  = '<span class="pull-right">'.number_format($pd->subtotal_saldo,'0',',','.').'</span>';
			$row[]  = '<span class="pull-right">'.$pd->masuk_stok.'</span>';
			$row[]  = '<span class="pull-right">'.$pd->keterangan.'</span>';

			$row[]  = $pd->pegawai_save;
			$row[]  = $pd->tanggal_pembuatan;
			$row[]  = $pd->pegawai_edit;
			$row[]  = $pd->tanggal_pembaharuan;
			$data[] = $row;
		}

		$output['id_penjualan_m']  = $id_penjualan_m;
		$output['draw']            = $_POST['draw'];
		$output['recordsTotal']    = $this->penjualan_detail->count_all('vamr4846_vama.penjualan_detail', $id_penjualan_m, '');
		$output['recordsFiltered'] = $this->penjualan_detail->count_filtered('_get_datatables_query', $id_penjualan_m, '');
		$output['data']            = $data;
		echo json_encode($output);
	}

	public function ajax_list_retur_batal()
	{
		$id_retur_penjualan_m    = $this->clean_tag_input($this->input->post('id_retur_penjualan_m'));
		if($id_retur_penjualan_m == ''){
			redirect(base_url().'retur_penjualan');
			exit();
		}

		// Cek user acces menu
		$id_pegawai                    = $this->session->userdata('id_pegawai');
		$cek_useraccess                = $this->useraccess_pusat->cek_access($id_pegawai, '28');
		$cek_useraccess_laporan        = $this->useraccess_pusat->cek_access($id_pegawai, '42');
		if($cek_useraccess->act_read == '0' AND $cek_useraccess->act_read == '0'){
			redirect(base_url().'dashboard');
			exit();
		}

		$list = $this->retur_penjualan_detail->get_datatables('_get_datatables_query_batal', $id_retur_penjualan_m);
		$data = array();
		$no   = $_POST['start'];
		foreach ($list as $pd){
			$no++;
			$row    = array();
			$row[]  = $no;
			
			$row[]  = $pd->sku;
			$row[]  = $pd->nama_barang;			
			$row[]  = $pd->keterangan_batal;
			$row[]  = '<span class="pull-right">'.number_format($pd->harga_satuan,'0',',','.').'</span>';
			$row[]  = '<span class="pull-right">'.number_format($pd->jumlah_beli,'0',',','.').'</span>';
			$row[]  = '<span class="pull-right">'.number_format($pd->jumlah_retur,'0',',','.').'</span>';
			$row[]  = '<span class="pull-right">'.number_format($pd->subtotal_retur,'0',',','.').'</span>';
			$row[]  = '<span class="pull-right">'.number_format($pd->subtotal_potongan,'0',',','.').'</span>';
			$row[]  = '<span class="pull-right">'.number_format($pd->subtotal_saldo,'0',',','.').'</span>';
			
			$row[]  = $pd->masuk_stok;
			$row[]  = $pd->keterangan;
			$row[]  = $pd->pegawai_pembatalan;
			$row[]  = $pd->tanggal_pembatalan;
			$data[] = $row;
		}

		$output['id_retur_penjualan_m'] = $id_retur_penjualan_m;
		$output['draw']                 = $_POST['draw'];
		$output['recordsTotal']         = $this->penjualan_detail->count_all('vamr4846_vama.retur_penjualan_detail_batal', $id_retur_penjualan_m, '');
		$output['recordsFiltered']      = $this->penjualan_detail->count_filtered('_get_datatables_query_batal', $id_retur_penjualan_m, '');
		$output['data']                 = $data;
		echo json_encode($output);
	}

	public function ajax_edit_retur()
	{
		$id_penjualan_d = $this->input->post('id_penjualan_d');
		if($id_penjualan_d == ''){
			redirect(base_url()."retur_penjualan");
			exit();
		}

		// Cek user acces menu
		$id_pegawai                    = $this->session->userdata('id_pegawai');
		$cek_useraccess                = $this->useraccess_pusat->cek_access($id_pegawai, '28');
		if($cek_useraccess->act_create == '0' or $cek_useraccess->act_create == '-'){
			$json['status']            = '0';
			$json['pesan']             = 'Maaf anda tidak diizinkan untuk menambahkan data retur penjualan.';
			echo json_encode($json);
			exit();
		}

		$json['status'] = '1';
		$json['data']   = $this->retur_penjualan_detail->get_retur_penjualan_detail($id_penjualan_d);
		echo json_encode($json);
	}

	public function ajax_verifikasi_hapus_detail()
	{
		$id_retur_penjualan_d    = $this->input->post('id_retur_penjualan_d');
		if($id_retur_penjualan_d == ''){
			redirect(base_url()."retur_penjualan");
			exit();
		}

		// Cek user acces menu
		$data                   = $this->retur_penjualan_detail->get_detail($id_retur_penjualan_d);
		$id_pegawai             = $this->session->userdata('id_pegawai');
		$cek_useraccess         = $this->useraccess_pusat->cek_access($id_pegawai, '28');
		$cek_useraccess_laporan = $this->useraccess_pusat->cek_access($id_pegawai, '42');
		if(($cek_useraccess->act_delete == '0' or $cek_useraccess->act_delete == '-') AND $data->status_retur == 'MENUNGGU'){
			$json['status'] = '0';
			$json['pesan']  = 'Maaf anda tidak diizinkan untuk menghapus data retur penjualan.';
			echo json_encode($json);
			exit();
		}else if(($cek_useraccess_laporan->act_delete == '0' or $cek_useraccess_laporan->act_delete == '-') AND $data->status_retur == 'SELESAI'){
			$json['status'] = '0';
			$json['pesan']  = 'Maaf anda tidak diizinkan untuk menghapus data retur penjualan yang sudah selesai.';
			echo json_encode($json);
			exit();
		}

		if($data){
			// validasi stok berdasarkan isian masuk stok
			$validasi_stok = '0';
			if($data->masuk_stok == "RUSAK"){
				$validasi_stok = $data->total_stok_rusak - $data->jumlah_retur; 
			}else if($data->masuk_stok == "JUAL"){
				$validasi_stok = $data->total_stok - $data->jumlah_retur;
			}
		
			if($validasi_stok < 0){
				echo json_encode(array(
					'status'	=> 1,
					'pesan' 	=> "	<b class='text-danger'>
											Maaf anda tidak bisa mengapus data retur penjualan dengan jumlah : $data->jumlah_retur
										</b></br></br>

										<small>SKU : </small></br>
										<b class='text-dark'>$data->sku</b><br/></br>

										<small>Nama Barang : </small> <br/>
										<b class='text-dark'>$data->nama_barang </b> </br><br/>

										<small>Masuk Stok : </small></br>
										<b class='text-dark'>$data->masuk_stok </b><br/><br/>

										<small>Keterangan Retur : </small> <br/>
										<b class='text-danger'>$data->keterangan </b><br/><br/>

										<b class='text-dark'>Karna bisa menyebabkan minus sebanyak : </b></br>
										<h1 class='text-danger'>$validasi_stok</h1>
								 	",

					'footer'	=> 	"<button type='button' class='btn btn-danger waves-effect' data-dismiss='modal'>Ok, saya mengerti</button>"
				));
			}else{			
				echo json_encode(array(
					'status'	=> 1,
					'pesan' 	=> "	<small>SKU : </small></br>
										<b class='text-dark'>$data->sku</b><br/></br>

										<small>Nama Barang : </small> <br/>
										<b class='text-dark'>$data->nama_barang </b> </br><br/>

										<small>Masuk Stok : </small></br>
										<b class='text-dark'>$data->masuk_stok </b><br/><br/>

										<small>Keterangan Retur : </small> <br/>
										<b class='text-danger'>$data->keterangan </b><br/><br/>

										<small>Jumlah Retur : </small></br>
										<h1 class='text-danger'>$data->jumlah_retur </h1>

										<b class='text-danger'>Keteranga Batal : </b>
										<textarea name='keterangan_batal' id='keterangan_batal' class='form-control input-md' placeholder='Masukan eterangan batal disini' style='resize: vertical;'></textarea>
								 	",

					'footer'	=> 	"<button onclick='hapus_retur_penjualan_detail($data->id_retur_penjualan_d)' 	
										type='button' class='btn btn-danger waves-effect waves-light' data-dismiss='modal' autofocus>
										Iya, Hapus
									</button>
									<button type='button' class='btn btn-default waves-effect' data-dismiss='modal'>Batal</button>"
				));
			}
		}else{
			echo json_encode(array(
				'status' 	=> 0,
				'pesan' 	=>	"Barang retur penjualan tidak ditemukan!",
			));
		}
	}

	public function ajax_hapus_detail()
	{
		if($this->input->is_ajax_request()){
			// Cek user acces menu
			$id_retur_penjualan_d 	= $this->input->post('id_retur_penjualan_d');
			$data                   = $this->retur_penjualan_detail->get_detail($id_retur_penjualan_d);
			$id_pegawai             = $this->session->userdata('id_pegawai');
			$cek_useraccess         = $this->useraccess_pusat->cek_access($id_pegawai, '28');
			$cek_useraccess_laporan = $this->useraccess_pusat->cek_access($id_pegawai, '42');

			if(($cek_useraccess->act_delete == '0' or $cek_useraccess->act_delete == '-') AND $data->status_retur == 'MENUNGGU'){
				$json['status']      = '0';
				$json['judul']       = 'Oops!';
				$json['pesan']       = 'Maaf anda tidak diizinkan untuk menghapus data retur penjualan.';
				$json['tipe_pesan']  = 'error';
				$json['gaya_tombol'] = 'btn-danger btn-md waves-effect waves-light';
				echo json_encode($json);
				exit();
			}else if(($cek_useraccess_laporan->act_delete == '0' or $cek_useraccess_laporan->act_delete == '-') AND $data->status_retur == 'SELESAI'){
				$json['status']      = '0';
				$json['judul']       = 'Oops!';
				$json['pesan']       = 'Maaf anda tidak diizinkan untuk menghapus data retur penjualan yang sudah selesai.';
				$json['tipe_pesan']  = 'error';
				$json['gaya_tombol'] = 'btn-danger btn-md waves-effect waves-light';
				echo json_encode($json);
				exit();
			}

			$hapus                 = '';
			$id_pegawai_pembatalan = $this->session->userdata('id_pegawai');
			$keterangan_batal      = $this->input->post('keterangan_batal');
			$tanggal               = date('Y-m-d H:i:s');
			$id_customer_pusat     = $this->input->post('id_customer_pusat');
			$catatan               = $this->clean_tag_input($this->input->post('catatan'));

			// Ambil data penjualan detail
			$dt = $this->retur_penjualan_detail->get_detail($id_retur_penjualan_d);
			if($dt){
				// validasi stok berdasarkan isian masuk stok
				$id_master     = $dt->id_retur_penjualan_m;
				$validasi_stok = '0';
				if($dt->masuk_stok == "RUSAK"){
					$validasi_stok = $dt->total_stok_rusak - $dt->jumlah_retur; 
				}else if($dt->masuk_stok == "JUAL"){
					$validasi_stok = $dt->total_stok - $dt->jumlah_retur;
				}
			
				if($validasi_stok < 0){
					echo json_encode(array(
						'status'	=> 1,
						'pesan' 	=> "	<b class='text-danger'>
												Maaf anda tidak bisa mengapus data retur penjualan dengan jumlah : $data->jumlah_retur
											</b></br></br>

											<small>SKU : </small></br>
											<b class='text-dark'>$data->sku</b><br/></br>

											<small>Nama Barang : </small> <br/>
											<b class='text-dark'>$data->nama_barang </b> </br><br/>

											<small>Masuk Stok : </small></br>
											<b class='text-dark'>$data->masuk_stok </b><br/><br/>

											<small>Keterangan Retur : </small> <br/>
											<b class='text-danger'>$data->keterangan </b><br/><br/>

											<b class='text-dark'>Karna bisa menyebabkan minus sebanyak : </b></br>
											<h1 class='text-danger'>$validasi_stok</h1>
									 	",

						'footer'	=> 	"<button type='button' class='btn btn-danger waves-effect' data-dismiss='modal'>Ok, saya mengerti</button>"
					));
				}else{	
					$hapus = $this->retur_penjualan_detail->hapus_retur_penjualan_detail
					(
						$dt->id_retur_penjualan_d, $dt->id_retur_penjualan_m, 
						$dt->id_penjualan_m, $dt->id_penjualan_d, $dt->no_retur_penjualan, $dt->no_penjualan, 
						$dt->id_customer_pusat, $dt->tipe_customer_pusat,

						$dt->id_barang_pusat, $dt->harga_satuan, $dt->potongan_harga_value, $dt->potongan_harga,
						$dt->jumlah_beli, $dt->jumlah_retur, $dt->subtotal_potongan, $dt->subtotal_saldo, $dt->subtotal_retur,
						
						$dt->masuk_stok, $dt->keterangan, $dt->id_pegawai_pembuatan,
						$dt->id_pegawai_pembaharuan, $id_pegawai_pembatalan,
						$dt->tanggal_pembuatan, $dt->tanggal_pembaharuan, $keterangan_batal
					);
				}
			}

			if($hapus){
				// Ambil id penjualan m, no penjualan dan id customer
				$drpm = $this->retur_penjualan_master->get_master($id_master);
				if($drpm){
					$id_penjualan_m      = $drpm->id_penjualan_m;
					$no_penjualan        = $drpm->no_penjualan;
					$id_customer_pusat   = $drpm->id_customer_pusat;
					$tipe_customer_pusat = $drpm->tipe_customer_pusat;
					$tanggal             = date('Y-m-d H:i:s');
				}

				$data_total_retur_penjualan = $this->retur_penjualan_detail->get_total($id_master);
				if ($data_total_retur_penjualan){
					$ttl_potongan = $data_total_retur_penjualan->total_potongan;
					$ttl_saldo    = $data_total_retur_penjualan->total_saldo;
					$ttl_retur    = $data_total_retur_penjualan->total_retur;
					$jml_barang   = $data_total_retur_penjualan->jumlah_barang;

					// Update total dan grandtotal di penjualan master
					$master = $this->retur_penjualan_master->update_master
					(
						$id_master, $id_penjualan_m, $no_penjualan, 
						$id_customer_pusat, $tipe_customer_pusat,
						$ttl_potongan, $ttl_saldo, $ttl_retur,
						$tanggal, $catatan, $id_pegawai_pembatalan
					);
				}else{
					$ttl_potongan = '0';
					$ttl_saldo    = '0';
					$ttl_retur    = '0';
					$jml_barang   = '0';
				}

				$data_batal  = $this->retur_penjualan_master->get_jumlah_batal($id_master);
				if($data_batal){
					$jumlah_batal = $data_batal->jumlah_barang;
				}else{
					$jumlah_batal = '0';
				}

				// Check jml barang retur, jika sudah tidak ada barang retur di retur penjualan detail maka hapus data retur penjualan master
				$data_jml_retur = $this->retur_penjualan_detail->get_jml_barang($id_master);
				if($data_jml_retur->jumlah_barang == '0'){
					$this->db->where('id_retur_penjualan_m', $id_master)->delete('vamr4846_vama.retur_penjualan_master');
					$json['status']      = '0';
					$json['judul']       = 'Berhasil!';
					$json['pesan']       = 'Data retur penjualan, barang berhasil dihapus.';
					$json['tipe_pesan']  = 'success';
					$json['gaya_tombol'] = 'btn-success btn-md waves-effect waves-light';
					echo json_encode($json);
					exit();
				}

				// Tampilkan informasi hapus
				$json['status']              = '1';
				$json['id_rpm']              = $id_master;
				$json['id_rpd']              = $id_retur_penjualan_d;
				$json['jumlah_barang']       = $jml_barang;
				$json['jumlah_barang_batal'] = $jumlah_batal;
				$json['total_potongan']      = $ttl_potongan;
				$json['total_saldo']         = $ttl_saldo;
				$json['total_retur']         = $ttl_retur;
				$json['pesan']               = "Data barang retur penjualan berhasil dihapus";
				echo json_encode($json);
			}else{
				$json['status']      = '0';
				$json['judul']       = 'Gagal!';
				$json['pesan']       = 'Barang retur penjualan, gagal disimpan.';
				$json['tipe_pesan']  = 'error';
				$json['gaya_tombol'] = 'btn-danger btn-md waves-effect waves-light';
				echo json_encode($json);
			}
		}else{
			redirect(base_url()."retur_penjualan");
			exit();
		}
	}

	public function ajax_pencarian_transaksi()
	{
		if($this->input->is_ajax_request()){
			// Cek user acces menu
			$id_pegawai     = $this->session->userdata('id_pegawai');
			$cek_useraccess = $this->useraccess_pusat->cek_access($id_pegawai, '28');
			if($cek_useraccess->act_create == '0' or $cek_useraccess->act_create == '-'){
				$json['status'] = '2';
			}else{
				$jumlah_jual      = '0';
				$keyword          = $this->input->post('keyword');
				$daftar_transaksi = $this->retur_penjualan_master->cari_daftar_transaksi($keyword);

				if($daftar_transaksi->num_rows() > 0){
					$json['status']  = 1;
					$json['datanya'] = "<ul id='daftar_autocompleted_transaksi' class='list-group user-list'>";

					foreach($daftar_transaksi->result() as $dt){
						$dt_jual = $this->penjualan_master->get_jumlah_jual($dt->id_penjualan_m);
						if($dt->foto != ""){
							$json['datanya'] .= "
				                <li class='user-list-item'>
				                	<div class='avatar avatar-sm-box'>
	                                    <img src='assets/upload/image/customer_pusat/thumbs/$dt->foto'>
				                	</div>";
				        }else{
							$nama_customer 	= $dt->nama_customer;
							$inisial 		= substr($nama_customer, 0, 1);
							if($inisial!=""){
								$json['datanya'] .= "
					                <li class='user-list-item'>
					                	<div class='avatar avatar-sm-box'>
		                                    <span class='avatar-sm-box bg-primary'>".$inisial."</span>
					                	</div>";
							}else{
								$json['datanya'] .= "
					                <li class='user-list-item'>
					                	<div class='avatar avatar-sm-box'>
		                                    <span class='avatar-sm-box bg-info'>-</span>
					                	</div>";
							}
						}

						$json['datanya'] .= "
			                	<div class='user-desc'>
									<span id='data_nama_customer' class='name'><b>".$dt->nama_customer."</b></span>
									<span id='data_no_penjualan' class='name'>".$dt->no_penjualan."</span>
									<span id='data_grandtotal_penjualan' class='name'>Total : Rp. ".$dt->grandtotal."</span>
									<span id='data_tanggal_penjualan' class='name'>".$dt->tanggal."</span>

									<span id='data_id_penjualan_m' style='display:none;'>".$dt->id_penjualan_m."</span>
									<span id='data_id_customer_pusat' style='display:none;'>".$dt->id_customer_pusat."</span>
									<span id='data_grand_total_penjualan' style='display:none;'>".$dt->grand_total."</span>
									<span id='data_jumlah_jual' style='display:none;'>".$dt_jual->jumlah_jual."</span>
								</div>
							</li>
						";
					}

					$json['datanya'] .= "</ul>";
				}else{
					$json['status'] 		= 0;
				}
			}
			echo json_encode($json);
		}else{
			redirect(base_url().'retur_penjualan');
		}
	}
}
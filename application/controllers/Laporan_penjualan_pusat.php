<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Laporan_penjualan_pusat extends MY_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->CI =& get_instance();
		$this->load->model('pegawai_pusat_model','pegawai_pusat');
		$this->load->model('useraccess_pusat_model','useraccess_pusat');
		$this->load->model('customer_pusat_model','customer_pusat');
		$this->load->model('barang_pusat_model','barang_pusat');
		$this->load->model('barang_toko_model', 'barang_toko');
		$this->load->model('penjualan_master_model','penjualan_master');
		$this->load->model('penjualan_detail_model','penjualan_detail');
		$this->load->model('retur_penjualan_master_model','retur_penjualan_master');
	}

	public function index()
	{
		// Cek user acces menu
		$id_pegawai = $this->session->userdata('id_pegawai');
		if($id_pegawai == ''){
			redirect(base_url().'dashboard');
		}
		
		$cek_useraccess_laporan = $this->useraccess_pusat->cek_access($id_pegawai, '39');
		if($cek_useraccess_laporan){
			if($cek_useraccess_laporan->act_read == '0' or $cek_useraccess_laporan->act_read == '-'){
				redirect(base_url().'dashboard');
			}					
		}else{
			redirect(base_url().'dashboard');
		}

		$data_pegawai = $this->pegawai_pusat->get_by_id($id_pegawai);
		if($data_pegawai){
			$data['access_create']   = $cek_useraccess_laporan->act_create;
			$data['data_pegawai']    = $data_pegawai;
			$data['data_induk_menu'] = $this->useraccess_pusat->get_induk_menu($id_pegawai);
			$data['atribut_halaman'] = 'Laporan Penjualan Pusat';
			
			$data['halaman_laporan'] = $this->load->view('admin/laporan/penjualan/laporan',$data,true);
			$data['halaman_plugin']  = $this->load->view('admin/laporan/penjualan/plugin',$data,true);
			$data['isi_halaman']     = $this->load->view('admin/laporan/penjualan/index',$data,true);
			$this->load->view('admin/layout',$data);
		}else{
			redirect(base_url().'login');
		}
	}

	public function ajax_list()
	{
		// Cek user acces menu
		$id_pegawai               = $this->session->userdata('id_pegawai');
		$cek_useraccess_transaksi = $this->useraccess_pusat->cek_access($id_pegawai, '27');
		$cek_useraccess_laporan   = $this->useraccess_pusat->cek_access($id_pegawai, '39');
		if($cek_useraccess_laporan->act_read == '0' or $cek_useraccess_laporan->act_read == '-'){
			redirect(base_url().'dashboard');
		}

		$tanggal_filter = $this->input->post('tanggal_filter');
		if($tanggal_filter == ''){
			redirect(base_url().'laporan_penjualan_pusat');
			exit();
		}
		
		$status_penjualan       = $this->input->post('status_penjualan');
		$jenis_perintah         = $this->input->post('jenis_perintah');
		$tanggal_filter         = $this->input->post('tanggal_filter');
		$tanggal_awal           = substr($tanggal_filter, 0, 10);
		$tanggal_akhir          = substr($tanggal_filter, 18, 27);
		$data_laporan_penjualan = $this->penjualan_master->get_laporan(
			'_get_laporan_query',
		   $tanggal_awal, $tanggal_akhir,
		   $jenis_perintah, $status_penjualan
		);

		$data = array();
		$no   = $_POST['start'];

		foreach ($data_laporan_penjualan as $dlpj){
			$no++;
			$row 	= array();
			$row[] 	= $no;
			$status = $dlpj->status_penjualan;
			if($status == "MENUNGGU" or $status == "DISIAPKAN"){
				$title_cetak = "Cetak Faktur Penyiapan Barang";
				$pesan_cetak = "Yakin ingin mencetak faktur penyipan barang ?";
				$url_cetak   = "faktur_penyiapan_barang";
			}else{
				$title_cetak = "Cetak Faktur Penjualan";
				$pesan_cetak = "Yakin ingin mencetak faktur penjualan ?";
				$url_cetak   = "faktur_penjualan";
			}

			//Tombol edit
			if($cek_useraccess_laporan->act_update == 1){
				$tombol_edit = '<a class="btn btn-default btn-rounded btn-xs"
										onclick="edit_penjualan('."'".$dlpj->id_penjualan_m."'".')" title="Edit Penjualan">
										<i class="fa fa-pencil" style="color:blue;"></i>
									</a>';
			}else{
				if(	($cek_useraccess_transaksi->act_update == 1 OR  $cek_useraccess_transaksi->act_create == 1) AND
				   	($status=="MENUNGGU" OR $status=="DISIAPKAN")){
					$tombol_edit = '<a class="btn btn-default btn-rounded btn-xs"
										onclick="edit_penjualan('."'".$dlpj->id_penjualan_m."'".')" title="Edit Penjualan">
										<i class="fa fa-pencil" style="color:blue;"></i>
									</a>';
				}else{
					$tombol_edit = '';
				}
			}

			// Tombol hapus
			// if($cek_useraccess_laporan->act_delete == 1){
			// 	$tombol_hapus = '<a class="btn btn-default btn-rounded btn-xs"
			// 						onclick="verifikasi_hapus_penjualan('."'".$dlpj->id_penjualan_m."'".')" title="Hapus Penjualan">
			// 						<i class="fa fa-times" style="color:red;"></i>
			// 		  			</a>';
			// }else{
			// 	if(	$cek_useraccess_transaksi->act_delete == 1 AND ($status=="MENUNGGU" or $status=="DISIAPKAN")){
			// 		$tombol_hapus = '<a class="btn btn-default btn-rounded btn-xs"
			// 							onclick="verifikasi_hapus_penjualan('."'".$dlpj->id_penjualan_m."'".')" title="Hapus Penjualan">
			// 							<i class="fa fa-times" style="color:red;"></i>
			// 		  				</a>';
			// 	}else{
			// 		$tombol_hapus = '';
			// 	}
			// }
			$tombol_hapus = '';

			if(	$cek_useraccess_laporan->act_update == 1 OR  $cek_useraccess_laporan->act_create == 1 OR
				$cek_useraccess_transaksi->act_update == 1 OR  $cek_useraccess_transaksi->act_create == 1 ){
				if($status=="DISIAPKAN"){
					$tombol_selesai = '<a class="btn btn-default btn-rounded btn-xs"
											onclick="selesai_transaksi('."'".$dlpj->id_penjualan_m."'".',
																	   '."'".$dlpj->no_penjualan."'".',
																	   '."'".$dlpj->nama_customer."'".')"
											title="Konfirmasi Selesai">
											<i class="fa fa-check" style="color:green;"></i>
									</a>';
				}else{$tombol_selesai = '';}
			}else{$tombol_selesai = '';}

			$row[] 	=	'	<a class="btn btn-default btn-rounded btn-xs"
								onclick="cetak_faktur('."'".$dlpj->id_penjualan_m."'".',
													  '."'".$pesan_cetak."'".',
													  '."'".$url_cetak."'".')"
								title="'.$title_cetak.'">
								<i class="fa fa-print"></i>
							</a>
							'.$tombol_edit.'
							'.$tombol_hapus.'
							'.$tombol_selesai.'
				  		';

			$row[] 	= '<span class="text-dark">'.$dlpj->no_penjualan.'</span>';
			if($dlpj->tipe_customer !== 'MRC'){
				$row[] 	= '<span class="text-dark">'.$dlpj->no_retur_penjualan.'</span>';
			}
			if($status=="MENUNGGU"){
				$row[] 	= '<span class="btn btn-danger btn-xs btn-block btn-rounded text-center"> '.$status.'</span>';
			}elseif($status=="DISIAPKAN"){
				$row[] 	= '<span class="btn btn-warning btn-xs btn-block btn-rounded text-center"> '.$status.'</span>';
			}elseif($status=="DIKIRIM"){
				$row[] 	= '<span class="btn btn-primary btn-xs btn-block btn-rounded text-center"> '.$status.'</span>';
			}else if($status=="SELESAI"){
				$row[] 	= '<span class="btn btn-success btn-xs btn-block btn-rounded text-center"> '.$status.'</span>';
			}else{
				$row[] 	= '<span class="btn btn-danger btn-xs btn-block btn-rounded text-center"><i class="fa fa-warning"></i></span>';
			}
			$row[] 	= date('d-m-Y', strtotime($dlpj->tanggal));
			$row[] 	= '<span class="text-dark">'.$dlpj->nama_customer.'</label>';

			$row[] 	= '<span class="pull-right text-dark">'.number_format($dlpj->jml_pb,'0',',','.').'</span>';
			if($dlpj->tipe_customer == 'MRC'){
				$row[] 	= '<span class="pull-right text-dark">'.number_format($dlpj->jml_bms,'0',',','.').'</span>';
				$row[] 	= '<span class="pull-right text-dark">'.number_format($dlpj->jml_bmu,'0',',','.').'</span>';
			}
			$row[] 	= '<span class="pull-right text-dark">'.number_format($dlpj->tunai,'0',',','.').'</span>';
			$row[] 	= '<span class="pull-right text-primary">'.number_format($dlpj->debit,'0',',','.').'</span>';
			$row[] 	= '<span class="pull-right text-dark">'.number_format($dlpj->total,'0',',','.').'</span>';
			if($dlpj->tipe_customer !== 'MRC'){
				$row[] 	= '<span class="pull-right text-danger">'.number_format($dlpj->total_retur,'0',',','.').'</span>';
				$row[] 	= '<span class="pull-right text-dark">'.number_format($dlpj->total_bersih,'0',',','.').'</span>';
			}

			$row[] 	= $dlpj->keterangan_lain;
			$row[] 	= $dlpj->pegawai_save;
			$row[] 	= $dlpj->tanggal_pembuatan;
			

			if($dlpj->tipe_customer !== 'MRC'){
				$row[] 	= $dlpj->pegawai_edit;
				$row[] 	= $dlpj->tanggal_pembaharuan;
				
			}else{
				$row[] 	= $dlpj->pegawai_edit;
				$row[] 	= $dlpj->tanggal_pembaharuan;
				$row[] 	= $dlpj->pegawai_masuk;
				$row[] 	= $dlpj->tanggal_masuk;
			}
			$data[]	= $row;
		}

		$output = array(	
			"draw"            => $_POST['draw'],
			"recordsTotal"    => $this->penjualan_master->count_all('vamr4846_vama.penjualan_master', $tanggal_awal, $tanggal_akhir),
			"recordsFiltered" => $this->penjualan_master->count_filtered(
									'_get_laporan_query',
									$tanggal_awal, $tanggal_akhir,
									$jenis_perintah, $status_penjualan
								),
			"data"            => $data
		);
		echo json_encode($output);
	}

	public function ajax_list_detail()
	{
		// Cek user acces menu
		$id_pegawai               = $this->session->userdata('id_pegawai');
		$cek_useraccess_transaksi = $this->useraccess_pusat->cek_access($id_pegawai, '27');
		$cek_useraccess_laporan   = $this->useraccess_pusat->cek_access($id_pegawai, '39');
		if($cek_useraccess_laporan->act_read == '0' or $cek_useraccess_laporan->act_read == '-'){
			redirect(base_url().'dashboard');
		}

		$tanggal_filter = $this->input->post('tanggal_filter');
		if($tanggal_filter == ''){
			redirect(base_url().'laporan_penjualan_pusat');
			exit();
		}

		$status_penjualan       = $this->input->post('status_penjualan');
		$jenis_perintah         = $this->input->post('jenis_perintah');
		$tanggal_filter         = $this->input->post('tanggal_filter');
		$tanggal_awal           = substr($tanggal_filter, 0, 10);
		$tanggal_akhir          = substr($tanggal_filter, 18, 27);
		$data_laporan_penjualan = $this->penjualan_master->get_laporan(
			'_get_laporan_query_detail',
		   $tanggal_awal, $tanggal_akhir,
		   $jenis_perintah, $status_penjualan
		);
		
		$data         = array();
		$no           = $_POST['start'];
		$no_penjualan = '0';

		foreach ($data_laporan_penjualan as $dlpj){
			$no++;
			$row 	= array();
			$row[] 	= $no;
			$status = $dlpj->status_penjualan;

			//Tombol edit
			if($status == 'SELESAI' or $status == 'DIKIRIM'){
				if($cek_useraccess_laporan->act_update == 1){
					$tombol_edit 	 = '<a class="btn btn-default btn-rounded btn-xs"
											onclick="edit_penjualan('."'".$dlpj->id_penjualan_m."'".')" title="Edit Penjualan">
											<i class="fa fa-pencil" style="color:blue;"></i>
										</a>';
				}else{
					$tombol_edit = '';
				}
			}else{
				if(($cek_useraccess_transaksi->act_update == 1 OR  $cek_useraccess_transaksi->act_create == 1) AND
				   ($status=="MENUNGGU" OR $status=="DISIAPKAN")){
					$tombol_edit = '<a class="btn btn-default btn-rounded btn-xs"
										onclick="edit_penjualan('."'".$dlpj->id_penjualan_m."'".')" title="Edit Penjualan">
										<i class="fa fa-pencil" style="color:blue;"></i>
									</a>';
				}else{
					$tombol_edit = '';
				}
			}

			// Tombol hapus
			// if($status == 'SELESAI' or $status == 'DIKIRIM'){
			// 	if($cek_useraccess_laporan->act_delete == 1){
			// 		$tombol_hapus = '<a class="btn btn-default btn-rounded btn-xs"
			// 							onclick="verifikasi_hapus_penjualan('."'".$dlpj->id_penjualan_m."'".')" title="Hapus Penjualan">
			// 							<i class="fa fa-times" style="color:red;"></i>
			// 			  			</a>';
			// 	}else{
			// 		$tombol_hapus = '';
			// 	}
			// }else{
			// 	if(	$cek_useraccess_transaksi->act_delete == 1){
			// 		$tombol_hapus = '<a class="btn btn-default btn-rounded btn-xs"
			// 							onclick="verifikasi_hapus_penjualan('."'".$dlpj->id_penjualan_m."'".')" title="Hapus Penjualan">
			// 							<i class="fa fa-times" style="color:red;"></i>
			// 		  				</a>';
			// 	}else{
			// 		$tombol_hapus = '';
			// 	}
			// }
			$tombol_hapus = '';

			if(	$cek_useraccess_laporan->act_update == 1 OR  $cek_useraccess_laporan->act_create == 1 OR
				$cek_useraccess_transaksi->act_update == 1 OR  $cek_useraccess_transaksi->act_create == 1 ){
				if($status=="DISIAPKAN"){
					$tombol_selesai = '<a class="btn btn-default btn-rounded btn-xs"
											onclick="selesai_transaksi('."'".$dlpj->id_penjualan_m."'".',
																	   '."'".$dlpj->no_penjualan."'".',
																	   '."'".$dlpj->nama_customer."'".')"
											title="Konfirmasi Selesai">
											<i class="fa fa-check" style="color:green;"></i>
									</a>';
				}else{$tombol_selesai = '';}
			}else{$tombol_selesai = '';}

			$row[] 	=	'	'.$tombol_edit.'
							'.$tombol_hapus.'
							'.$tombol_selesai.'
				  		';

			if($no_penjualan <> $dlpj->no_penjualan){
				$row[] 	= '<span class="btn btn-success btn-block btn-rounded btn-xs">'.$dlpj->no_penjualan.'</span>';
			}else{
				$row[] 	= '<span class="btn btn-default btn-block btn-rounded btn-xs">'.$dlpj->no_penjualan.'</span>';
			}

			if($dlpj->jenis_harga !== 'MRC'){
				if($dlpj->jumlah_retur > 0){
					$row[] 	= '<span class="btn btn-default btn-xs btn-block btn-rounded text-dark">'.$dlpj->no_retur_penjualan.'</span>';
				}else{
					$row[] 	= '';
				}
			}
			
			if($status == "MENUNGGU"){
				$row[] 	= '<span class="btn btn-danger btn-xs btn-block btn-rounded text-center"> '.$status.'</span>';
			}elseif($status=="DISIAPKAN"){
				$row[] 	= '<span class="btn btn-warning btn-xs btn-block btn-rounded text-center"> '.$status.'</span>';
			}elseif($status=="DIKIRIM"){
				$row[] 	= '<span class="btn btn-primary btn-xs btn-block btn-rounded text-center"> '.$status.'</span>';
			}else if($status=="SELESAI"){
				$row[] 	= '<span class="btn btn-success btn-xs btn-block btn-rounded text-center"> '.$status.'</span>';
			}else{
				$row[] 	= '<span class="btn btn-danger btn-xs btn-block btn-rounded text-center"><i class="fa fa-warning"></i></span>';
			}

			$row[] 	= date('d-m-Y', strtotime($dlpj->tanggal));
			$row[] 	= '<span class="text-dark">'.$dlpj->nama_customer.'</label>';
			$row[] 	= '<span class="text-dark">'.$dlpj->sku.' - '.$dlpj->nama_barang.'</label>';
			if($dlpj->jenis_harga !== 'MRC'){
				$row[] 	= '<span class="text-dark">'.$dlpj->jenis_harga.'</label>';
			}
			$row[] 	= '<span class="pull-right">'.number_format($dlpj->harga_satuan,'0',',','.').'</span>';
			$row[] 	= '<span class="pull-right">'.number_format($dlpj->discount_harga,'0',',','.').'</span>';
			$row[] 	= '<span class="pull-right">'.number_format($dlpj->harga_bersih,'0',',','.').'</span>';

			$row[] 	= '<span class="pull-right">'.number_format($dlpj->jumlah_beli,'0',',','.').'</span>';
			if($dlpj->jenis_harga == 'MRC'){
				$row[] 	= '<span class="pull-right">'.number_format($dlpj->jumlah_masuk,'0',',','.').'</span>';
			}

			if($dlpj->jenis_harga !== 'MRC'){
				$row[] 	= '<span class="pull-right">'.number_format($dlpj->jumlah_retur,'0',',','.').'</span>';
				$row[] 	= '<span class="pull-right">'.number_format($dlpj->jumlah_bersih,'0',',','.').'</span>';
			}

			$row[] 	= '<span class="pull-right">'.number_format($dlpj->subtotal,'0',',','.').'</span>';
			if($dlpj->jenis_harga !== 'MRC'){
				$row[] 	= '<span class="pull-right">'.number_format($dlpj->subtotal_retur,'0',',','.').'</span>';
				$row[] 	= '<span class="pull-right">'.number_format($dlpj->subtotal_bersih,'0',',','.').'</span>';
			}

			if($dlpj->jenis_harga !== 'MRC'){
				$row[] 	= $dlpj->pegawai_save;
				$row[] 	= $dlpj->tanggal_pembuatan;
				$row[] 	= $dlpj->pegawai_edit;
				$row[] 	= $dlpj->tanggal_pembaharuan;
			}else{
				$row[] 	= $dlpj->pegawai_save;
				$row[] 	= $dlpj->tanggal_pembuatan;
				$row[] 	= $dlpj->pegawai_edit;
				$row[] 	= $dlpj->tanggal_pembaharuan;
				$row[] 	= $dlpj->pegawai_masuk;
				$row[] 	= $dlpj->tanggal_masuk;
			}
			$data[]	= $row;
			$no_penjualan = $dlpj->no_penjualan;
		}

		$output = array(	
			"draw"            => $_POST['draw'],
			"recordsTotal"    => $this->penjualan_master->count_all('vamr4846_vama.penjualan_detail AS pd', $tanggal_awal, $tanggal_akhir),
			"recordsFiltered" => $this->penjualan_master->count_filtered(
									'_get_laporan_query_detail',
									$tanggal_awal, $tanggal_akhir,
									$jenis_perintah, $status_penjualan
								),
			"data"            => $data
		);
		echo json_encode($output);
	}

	public function ajax_list_batal()
	{
		// Cek user acces menu
		$id_pegawai             = $this->session->userdata('id_pegawai');
		$cek_useraccess_laporan = $this->useraccess_pusat->cek_access($id_pegawai, '39');
		if($cek_useraccess_laporan->act_read == '0' or $cek_useraccess_laporan->act_read == '-'){
			redirect(base_url().'dashboard');
		}

		$tanggal_filter = $this->input->post('tanggal_filter');
		if($tanggal_filter == ''){
			redirect(base_url().'laporan_penjualan_pusat');
			exit();
		}

		$status_penjualan       = $this->input->post('status_penjualan');
		$jenis_perintah         = $this->input->post('jenis_perintah');
		$tanggal_filter         = $this->input->post('tanggal_filter');
		$tanggal_awal           = substr($tanggal_filter, 0, 10);
		$tanggal_akhir          = substr($tanggal_filter, 18, 27);
		$data_laporan_penjualan = $this->penjualan_master->get_laporan(
			'_get_laporan_query_batal',
			$tanggal_awal, $tanggal_akhir,
			$jenis_perintah, $status_penjualan, ''
		);

		$data         = array();
		$no           = $_POST['start'];
		$no_penjualan = '0';

		foreach ($data_laporan_penjualan as $dlpj){
			$no++;
			$row 	= array();
			$row[] 	= $no;

			if($no_penjualan <> $dlpj->no_penjualan){
				$row[] 	= '<span class="btn btn-success btn-block btn-rounded btn-xs">'.$dlpj->no_penjualan.'</span>';
			}else{
				$row[] 	= '<span class="btn btn-default btn-block btn-rounded btn-xs">'.$dlpj->no_penjualan.'</span>';
			}

			$row[] 	= '<span class="text-dark">'.$dlpj->nama_customer.'</label>';
			$row[] 	= '<span class="text-dark">'.$dlpj->sku.' - '.$dlpj->nama_barang.'</label>';
			$row[] 	= '<span class="text-dark">'.$dlpj->jenis_harga.'</label>';
			$row[] 	= '<span class="pull-right">'.number_format($dlpj->harga_satuan,'0',',','.').'</span>';
			$row[] 	= '<span class="pull-right">'.number_format($dlpj->discount_harga,'0',',','.').'</span>';
			$row[] 	= '<span class="pull-right">'.number_format($dlpj->harga_bersih,'0',',','.').'</span>';
			$row[] 	= '<span class="pull-right">'.number_format($dlpj->jumlah_beli,'0',',','.').'</span>';
			$row[] 	= '<span class="pull-right">'.number_format($dlpj->subtotal,'0',',','.').'</span>';
			$row[] 	= '<span class="text-dark">'.$dlpj->keterangan_batal.'</label>';

			$row[] 	= $dlpj->pegawai_batal;
			$row[] 	= $dlpj->tanggal_pembatalan;
			$row[] 	= $dlpj->pegawai_save;
			$row[] 	= $dlpj->tanggal_pembuatan;
			$row[] 	= $dlpj->pegawai_edit;
			$row[] 	= $dlpj->tanggal_pembaharuan;
			$data[]	= $row;
			$no_penjualan = $dlpj->no_penjualan;
		}

		$output = array(	
			"draw"            => $_POST['draw'],
			"recordsTotal"    => $this->penjualan_master->count_all('vamr4846_vama.penjualan_detail_batal AS pd',$tanggal_awal, $tanggal_akhir),
			"recordsFiltered" => $this->penjualan_master->count_filtered(
									'_get_laporan_query_batal',
									$tanggal_awal, $tanggal_akhir, $jenis_perintah, ''
								),
			"data"            => $data
						);
		echo json_encode($output);
	}

	public function ajax_list_debit()
	{
		// Cek user acces menu
		$id_pegawai             = $this->session->userdata('id_pegawai');
		$cek_useraccess_laporan = $this->useraccess_pusat->cek_access($id_pegawai, '39');
		if($cek_useraccess_laporan->act_read == '0' or $cek_useraccess_laporan->act_read == '-'){
			redirect(base_url().'dashboard');
		}

		$tanggal_filter = $this->input->post('tanggal_filter');
		if($tanggal_filter == ''){
			redirect(base_url().'laporan_penjualan_pusat');
			exit();
		}

		$status_penjualan       = $this->input->post('status_penjualan');
		$jenis_perintah         = $this->input->post('jenis_perintah');
		$tanggal_filter         = $this->input->post('tanggal_filter');
		$tanggal_awal           = substr($tanggal_filter, 0, 10);
		$tanggal_akhir          = substr($tanggal_filter, 18, 27);
		$data_laporan_penjualan = $this->penjualan_master->get_laporan('_get_laporan_query_debit',
																		   $tanggal_awal, $tanggal_akhir,
																		   $jenis_perintah, $status_penjualan);

		$data = array();
		$no   = $_POST['start'];

		foreach ($data_laporan_penjualan as $dlpj){
			$no++;
			$row 	= array();
			$row[] 	= $no;
			$row[] 	= '<span class="text-dark">'.$dlpj->no_penjualan.'</label>';
			$row[] 	= date('d-m-Y', strtotime($dlpj->tanggal));
			$row[] 	= '<span class="text-dark">'.$dlpj->nama_customer.'</label>';
			$row[] 	= '<span class="text-dark">'.$dlpj->no_kartu_edc.'</label>';
			$row[] 	= '<span class="text-dark">'.$dlpj->nama_bank.'</label>';
			$row[] 	= '<span class="text-dark">'.$dlpj->no_kartu_customer.'</label>';
			$row[] 	= '<span class="pull-right">'.number_format($dlpj->jumlah_pembayaran,'0',',','.').'</span>';

			$row[] 	= $dlpj->pegawai_save;
			$row[] 	= $dlpj->tanggal_pembuatan;
			$row[] 	= $dlpj->pegawai_edit;
			$row[] 	= $dlpj->tanggal_pembaharuan;
			$data[]	= $row;
		}

		$output = array(	
			"draw"            => $_POST['draw'],
			"recordsTotal"    => $this->penjualan_master->count_all('vamr4846_vama.penjualan_detail_kartu AS pdk', $tanggal_awal, $tanggal_akhir),
			"recordsFiltered" => $this->penjualan_master->count_filtered(
									'_get_laporan_query_debit',
									$tanggal_awal, $tanggal_akhir,
									$jenis_perintah, $status_penjualan
								),
			"data"            => $data
						);
		echo json_encode($output);
	}

	public function ajax_list_perbarang()
	{
		// Cek user acces menu
		$id_pegawai             = $this->session->userdata('id_pegawai');
		$cek_useraccess_laporan = $this->useraccess_pusat->cek_access($id_pegawai, '39');
		if($cek_useraccess_laporan->act_read == '0' or $cek_useraccess_laporan->act_read == '-'){
			redirect(base_url().'dashboard');
		}

		$tanggal_filter = $this->input->post('tanggal_filter');
		if($tanggal_filter == ''){
			redirect(base_url().'laporan_penjualan_pusat');
			exit();
		}
		
		$status_penjualan       = $this->input->post('status_penjualan');
		$jenis_perintah         = $this->input->post('jenis_perintah');
		$tanggal_filter         = $this->input->post('tanggal_filter');
		$tanggal_awal           = substr($tanggal_filter, 0, 10);
		$tanggal_akhir          = substr($tanggal_filter, 18, 27);
		$data_laporan_penjualan = $this->penjualan_master->get_laporan(
			'_get_laporan_query_perbarang',
			$tanggal_awal, $tanggal_akhir,
			$jenis_perintah, $status_penjualan
		);

		$data = array();
		$no   = $_POST['start'];

		foreach ($data_laporan_penjualan as $dlpj){
			$no++;
			$row 	= array();
			$row[] 	= $no;

			$row[] 	= '<span class="text-dark">'.$dlpj->sku.'</label>';
			$row[] 	= '<span class="text-dark">'.$dlpj->nama_barang.'</label>';

			$row[] 	= '<span class="pull-right">'.number_format($dlpj->transaksi_jual,'0',',','.').'</span>';
			$row[] 	= '<span class="pull-right">'.number_format($dlpj->transaksi_retur,'0',',','.').'</span>';
			$row[] 	= '<span class="pull-right">'.number_format($dlpj->transaksi_fix,'0',',','.').'</span>';

			$row[] 	= '<span class="pull-right">'.number_format($dlpj->jml_jual,'0',',','.').'</span>';
			$row[] 	= '<span class="pull-right">'.number_format($dlpj->jml_retur,'0',',','.').'</span>';
			$row[] 	= '<span class="pull-right">'.number_format($dlpj->jml_fix,'0',',','.').'</span>';

			$row[] 	= '<span class="pull-right">'.number_format($dlpj->total_jual,'0',',','.').'</span>';
			$row[] 	= '<span class="pull-right">'.number_format($dlpj->total_retur,'0',',','.').'</span>';
			$row[] 	= '<span class="pull-right">'.number_format($dlpj->total_fix,'0',',','.').'</span>';

			$row[] 	= $dlpj->pegawai_save;
			$row[] 	= $dlpj->tanggal_pembuatan;
			$row[] 	= $dlpj->pegawai_edit;
			$row[] 	= $dlpj->tanggal_pembaharuan;
			$data[]	= $row;
		}

		$output = array(	
			"draw"            => $_POST['draw'],
			"recordsTotal"    => $this->penjualan_master->count_all('vamr4846_vama.penjualan_detail AS pd2', $tanggal_awal, $tanggal_akhir),
			"recordsFiltered" => $this->penjualan_master->count_filtered(
									'_get_laporan_query_perbarang',
									$tanggal_awal, $tanggal_akhir,
									$jenis_perintah, $status_penjualan
								),
			"data"            => $data
		);
		echo json_encode($output);
	}

	public function ambil_total()
	{
		// Cek user acces menu
		$id_pegawai     = $this->session->userdata('id_pegawai');
		$cek_useraccess = $this->useraccess_pusat->cek_access($id_pegawai, '39');
		if($cek_useraccess->act_read == '0' or $cek_useraccess->act_read == '-'){
			redirect(base_url().'dashboard');
		}

		$tanggal_filter = $this->input->post('tanggal_filter');
		if($tanggal_filter == ''){
			redirect(base_url().'laporan_penjualan_pusat');
			exit();
		}

		$jenis_perintah = $this->input->post('jenis_perintah');
		$tanggal_filter = $this->input->post('tanggal_filter');
		$tanggal_awal   = substr($tanggal_filter, 0, 10);
		$tanggal_akhir  = substr($tanggal_filter, 18, 27);

		// Ambil jumlah transaksi, total tunai, debit dan total uang masuk
		$data_uang_masuk = $this->penjualan_master->get_uang($tanggal_awal, $tanggal_akhir,'SELESAI', $jenis_perintah);
		if ($data_uang_masuk){
			$total_debit      = number_format($data_uang_masuk->total_debit,'0', ',', '.');
			$total_tunai      = number_format($data_uang_masuk->total_tunai,'0', ',', '.');
			$total_uang_masuk = number_format($data_uang_masuk->total_transaksi,'0', ',', '.');
			$total_ppn        = number_format($data_uang_masuk->total_ppn,'0', ',', '.');
			$total_biaya_lain = number_format($data_uang_masuk->total_biaya_lain,'0', ',', '.');
			$total_bersih     = number_format($data_uang_masuk->total_bersih,'0', ',', '.');
		}else{
			$total_debit      = '0';
			$total_tunai      = '0';
			$total_uang_masuk = '0';
			$total_ppn        = '0';
			$total_biaya_lain = '0';
			$total_bersih     = '0';
		}

		// Ambil data penjualan menunggu
		$data_menunggu = $this->penjualan_master->get_uang($tanggal_awal, $tanggal_akhir,'MENUNGGU', $jenis_perintah);
		if ($data_menunggu){
			$total_menunggu = number_format($data_menunggu->total_transaksi,'0', ',', '.');
		}else{
			$total_menunggu = '0';
		}

		// Ambil data penjualan disiapkan
		$data_disiapkan = $this->penjualan_master->get_uang($tanggal_awal, $tanggal_akhir,'DISIAPKAN', $jenis_perintah);
		if ($data_disiapkan){
			$total_disiapkan = number_format($data_disiapkan->total_transaksi,'0', ',', '.');
		}else{
			$total_menunggu = '0';
		}

		// Ambil data penjualan dikirim
		$data_dikirim = $this->penjualan_master->get_uang($tanggal_awal, $tanggal_akhir,'DIKIRIM', $jenis_perintah);
		if ($data_dikirim){
			$total_dikirim = number_format($data_dikirim->total_transaksi,'0', ',', '.');
		}else{
			$total_menunggu = '0';
		}

		// Ambil data modal dan keuntungan
		$data_modal = $this->penjualan_master->get_modal($tanggal_awal, $tanggal_akhir, $jenis_perintah);
		if ($data_modal){
			$total_modal      = number_format($data_modal->total_modal,'0', ',', '.');
			$total_keuntungan = number_format($data_modal->total_keuntungan,'0', ',', '.');
		}else{
			$total_modal      = '0';
			$total_keuntungan = '0';
		}

		// Ambil data retur penjualan
		$data_retur = $this->retur_penjualan_master->get_grand_total(
			$tanggal_awal, $tanggal_akhir, $jenis_perintah, 'SELESAI'
		);
		if ($data_retur){
			$total_retur = number_format($data_retur->grand_total_retur,'0', ',', '.');
		}else{
			$total_retur = '0';
		}

		if($this->session->userdata('usergroup_name') == 'Super Admin'){
			echo json_encode(array(	
				"status"           => 1,
				"total_menunggu"   => $total_menunggu,
				"total_disiapkan"  => $total_disiapkan,
				"total_dikirim"    => $total_dikirim,
				"total_debit"      => $total_debit,
				"total_tunai"      => $total_tunai,
				"total_uang_masuk" => $total_uang_masuk,
				"total_retur"      => $total_retur,
				"total_ppn"        => $total_ppn,
				"total_biaya_lain" => $total_biaya_lain,
				"total_bersih"     => $total_bersih,
				"total_modal"      => $total_modal,
				"total_keuntungan" => $total_keuntungan
			));
		}else if($this->session->userdata('usergroup_name') == 'Admin'){
			echo json_encode(array(	
				"status"           => 1,
				"total_menunggu"   => $total_menunggu,
				"total_disiapkan"  => $total_disiapkan,
				"total_dikirim"    => $total_dikirim,
				"total_debit"      => $total_debit,
				"total_tunai"      => $total_tunai,
				"total_uang_masuk" => $total_uang_masuk,
				"total_retur"      => $total_retur,
				"total_ppn"        => $total_ppn,
				"total_biaya_lain" => $total_biaya_lain,
				"total_bersih"     => $total_bersih
			));
		}
	}

	public function ajax_verifikasi_hapus_transaksi()
	{
		if($this->input->is_ajax_request()){
			$id_penjualan_m = $this->input->post('id_penjualan_m');
			$id_pegawai     = $this->session->userdata('id_pegawai');

			// Cek user acces menu
			$cek_useraccess_transaksi = $this->useraccess_pusat->cek_access($id_pegawai, '27');
			$cek_useraccess_laporan   = $this->useraccess_pusat->cek_access($id_pegawai, '39');
			
			// Validasi apakah penjualan sudah diretur
			$data_retur = $this->retur_penjualan_master->get_by_id_penjualan($id_penjualan_m);
			if($data_retur){
				echo json_encode(array(	
					"status" 		=> 0,
				  	"info_pesan" 	=> "Maaf transaksi penjualan ini sudah tidak bisa di hapus,
										Karna transaksi ini sudah di retur",
					"pesan" 		=> "No. Penjualan : ".$data_retur->no_penjualan."
										Nama Customer : ".$data_retur->nama_customer_pusat."

										No. Retur Penjualan : ".$data_retur->no_retur_penjualan."
										Total Retur : Rp. ".number_format($data_retur->total_retur,'0',',','.')."
										",
					"tipe_pesan" 	=> "error"
				));
				exit();
			}

			// Validasi status penjualan
			$data                = $this->penjualan_master->get_master($id_penjualan_m);
			$kode_customer_pusat = $data->kode_customer_pusat;
			$tipe_customer_pusat = $data->tipe_customer_pusat;

			if($data){
				if($data->status_penjualan == 'SELESAI'){
					if($cek_useraccess_laporan->act_delete == '0' or $cek_useraccess_laporan->act_delete == '-'){
						echo json_encode(array(	
							"status" 		=> 0,
						  	"info_pesan" 	=> "Maaf anda tidak diberikan izin untuk menghapus transaksi penjualan,
												dengan status penjualan : SELESAI !",
							"pesan" 		=> "No. Penjualan : ".$data->no_penjualan."
												Nama Customer : ".$data->nama_customer_pusat."
												Status Penjualan : ".$data->status_penjualan."
												",
							"tipe_pesan" 	=> "error"
						));
						exit();
					}
				}else{
					if($cek_useraccess_transaksi->act_delete == '0' or $cek_useraccess_transaksi->act_delete == '-'){
						echo json_encode(array(	
							"status" 		=> 0,
						  	"info_pesan" 	=> "Maaf anda tidak diberikan izin untuk menghapus transaksi penjualan!",
							"pesan" 		=> "No. Penjualan : ".$data->no_penjualan."
												Nama Customer : ".$data->nama_customer_pusat."
												Status Penjualan : ".$data->status_penjualan."
												",
							"tipe_pesan" 	=> "error"
						));
						exit();
					}
				}

				// Awal validasi stok
				if($tipe_customer_pusat == 'MRC'){
					$data_penjualan_detail = $this->penjualan_detail->get_penjualan_detail_all($id_penjualan_m);
					foreach($data_penjualan_detail AS $dpd){
						$id_barang_pusat = $dpd->id_barang_pusat;
						$jumlah_beli     = $dpd->jumlah_beli;

						// Ambil data barang dari master barang pusat
						$data_barang = $this->barang_toko->get_barang_toko($id_barang_pusat, $kode_customer_pusat);
						if($data_barang){
							$sku           = $data_barang->sku;
							$nama_barang   = $data_barang->nama_barang;
							$stok_sekarang = $data_barang->total_stok;
							$validasi_stok = $stok_sekarang - $jumlah_beli + 0;

							if($validasi_stok < 0){
								echo json_encode(array(
									"status" 		=> 0,
								  	"info_pesan" 	=> "Gagal!",
									"pesan" 		=> "
															Maaf anda tidak bisa menghapus transaksi penjualan ini !
															Karna akan menyebabkan stok barang ini menjadi minus : 
															Nama Customer : ".$data->nama_customer_pusat."
															SKU : ".$sku."
															Nama Barang : ".$nama_barang."
															Stok Sekarang : ".$stok_sekarang."
															Total Minus : ".$validasi_stok."
														",
									"tipe_pesan" 	=> "error",
									"gaya_tombol"	=> "btn-danger btn-md waves-effect waves-light"
								));
								exit();
							}
						}else{
							echo json_encode(array(
								"status" 		=> 0,
							  	"info_pesan" 	=> "Gagal!",
								"pesan" 		=> "
														Data barang tidak ditemukan 
														Nama Customer : ".$data->nama_customer_pusat."
														ID Barang : ".$id_barang_pusat."
														Jumlah Beli : ".$jumlah_beli."
													",
								"tipe_pesan" 	=> "error",
								"gaya_tombol"	=> "btn-danger btn-md waves-effect waves-light"
							));
							exit();
						}
					}
				}
				// Akhir validasi stok
			}

			if($data){
				$grand_total = number_format($data->grand_total,'0',',','.');
				echo json_encode(array(
					'status' 	=>	1,
					'pesan' 	=>	"
									<small>No. Penjualan : </small><br/>
									<b class='text-dark'>$data->no_penjualan </b><br/><br/>

									<small>Nama Customer : </small> <br/>
									<b class='text-dark'>$data->nama_customer_pusat </b> </br><br/>

									<small>Grandtotal : </small> <br/>
									<h2 class='text-danger'>Rp. $grand_total </h2>

									<textarea name='keterangan_batal' id='keterangan_batal' class='form-control input-md' placeholder='Keterangan batal' style='resize: vertical;'></textarea>
									",

					'footer'	=> 	"<button onclick='hapus_penjualan($data->id_penjualan_m)' type='button' class='btn btn-danger
									 waves-effect waves-light' data-dismiss='modal' autofocus>Iya, Hapus</button>
									<button type='button' class='btn btn-default waves-effect' data-dismiss='modal'>Batal</button>"
				));
			}else{
				echo json_encode(array(	
					"status"     => 0,
					"info_pesan" => "Oops!",
					"pesan"      => "Data transaksi penjualan tidak ditemukan",
					"tipe_pesan" => "error"
				));
				exit();
			}
		}else{
			redirect(base_url().'laporan_penjualan_pusat');
		}
	}

	public function ajax_hapus_transaksi()
	{
		if($this->input->is_ajax_request()){
			$id_penjualan_m           = $this->input->post('id_penjualan_m');
			$id_pegawai               = $this->session->userdata('id_pegawai');
			
			// Cek user acces menu
			$cek_useraccess_transaksi = $this->useraccess_pusat->cek_access($id_pegawai, '27');
			$cek_useraccess_laporan   = $this->useraccess_pusat->cek_access($id_pegawai, '39');
			
			// Validasi apakah penjualan sudah diretur
			$data_retur               = $this->retur_penjualan_master->get_by_id_penjualan($id_penjualan_m);
			if($data_retur){
				echo json_encode(array(	
					"status" 		=> 0,
				  	"info_pesan" 	=> "Maaf transaksi penjualan ini sudah tidak bisa di hapus,
										Karna transaksi ini sudah di retur",
					"pesan" 		=> "No. Penjualan : ".$data_retur->no_penjualan."
										Nama Customer : ".$data_retur->nama_customer_pusat."

										No. Retur Penjualan : ".$data_retur->no_retur_penjualan."
										Total Retur : Rp. ".number_format($data_retur->total_retur,'0',',','.')."
										"
				));
				exit();
			}

			$master              = $this->penjualan_master->get_master($id_penjualan_m);
			$kode_customer_pusat = $master->kode_customer_pusat;
			$tipe_customer_pusat = $master->tipe_customer_pusat;
			$asal_customer_pusat = $master->asal_customer_pusat;

			if($master){
				if($master->status_penjualan == 'SELESAI'){
					if($cek_useraccess_laporan->act_delete == '0' or $cek_useraccess_laporan->act_delete == '-'){
						echo json_encode(array(	
							"status" 		=> 0,
						  	"info_pesan" 	=> "Maaf anda tidak diberikan izin untuk menghapus transaksi penjualan,
												dengan status penjualan : SELESAI !",
							"pesan" 		=> "No. Penjualan : ".$master->no_penjualan."
												Nama Customer : ".$master->nama_customer_pusat."
												Status Penjualan : ".$master->status_penjualan."
												"
						));
						exit();
					}
				}else{
					if($cek_useraccess_transaksi->act_delete == '0' or $cek_useraccess_transaksi->act_delete == '-'){
						echo json_encode(array(	
							"status" 		=> 0,
						  	"info_pesan" 	=> "Maaf anda tidak diberikan izin untuk menghapus transaksi penjualan!",
							"pesan" 		=> "No. Penjualan : ".$master->no_penjualan."
												Nama Customer : ".$master->nama_customer_pusat."
												Status Penjualan : ".$master->status_penjualan."
												"
						));
						exit();
					}
				}

				if($tipe_customer_pusat == 'MRC'){
					// Awal validasi stok
					$data_penjualan_detail = $this->penjualan_detail->get_penjualan_detail_all($id_penjualan_m);
					foreach($data_penjualan_detail AS $dpd){
						$id_barang_pusat = $dpd->id_barang_pusat;
						$jumlah_beli     = $dpd->jumlah_beli;

						// Ambil data barang dari master barang pusat
						$data_barang = $this->barang_toko->get_barang_toko($id_barang_pusat, $master->kode_customer_pusat);
						if($data_barang){
							$sku           = $data_barang->sku;
							$nama_barang   = $data_barang->nama_barang;
							$stok_sekarang = $data_barang->total_stok;
							$validasi_stok = $stok_sekarang - $jumlah_beli + 0;

							if($validasi_stok < 0){
								echo json_encode(array(
									"status" 		=> 0,
								  	"info_pesan" 	=> "Gagal!",
									"pesan" 		=> "
															Maaf anda tidak bisa menghapus transaksi penjualan ini !
															Karna akan menyebabkan stok barang ini menjadi minus : 
															Nama Customer : ".$master->nama_customer_pusat."
															SKU : ".$sku."
															Nama Barang : ".$nama_barang."
															Stok Sekarang : ".$stok_sekarang."
															Total Minus : ".$validasi_stok."
														",
									"tipe_pesan" 	=> "error",
									"gaya_tombol"	=> "btn-danger btn-md waves-effect waves-light"
								));
								exit();
							}
						}else{
							echo json_encode(array(
								"status" 		=> 0,
							  	"info_pesan" 	=> "Gagal!",
								"pesan" 		=> "Data barang tidak ditemukan 
													Nama Customer : ".$master->nama_customer_pusat."
													ID Barang : ".$id_barang_pusat."
													Jumlah Beli : ".$jumlah_beli."",
								"tipe_pesan" 	=> "error",
								"gaya_tombol"	=> "btn-danger btn-md waves-effect waves-light"
							));
							exit();
						}
					}
					// Akhir validasi stok
				}
			}

			if($master){
				$no_penjualan        = $master->no_penjualan;
				$id_customer_pusat   = $master->id_customer_pusat;
				$kode_toko           = $master->kode_customer_pusat;
				$tipe_customer_pusat = $master->tipe_customer_pusat;
				$status_penjualan    = $master->status_penjualan;
				$keterangan_batal    = $this->input->post('keterangan_batal');
				$hapus               = $this->penjualan_master->hapus_transaksi(
					$id_penjualan_m, $no_penjualan, $id_customer_pusat, 
					$keterangan_batal, $id_pegawai,
					$kode_toko, $tipe_customer_pusat, $status_penjualan
				);
				if($hapus){
					if($asal_customer_pusat == 'ONLINE'){
						$cek_poin = $this->penjualan_master->cek_poin($master->id_penjualan_m, $master->no_penjualan);
						if($cek_poin){
							$this->penjualan_master->delete_poin($master->id_penjualan_m, $master->no_penjualan);
						}
					}

					echo json_encode(array(	
						"status" 		=> 1,
						"no_penjualan"	=> $master->no_penjualan,
						"info_pesan" 	=> "Berhasil!",											
						"pesan" 		=> "No. Penjualan : ".$master->no_penjualan."
											Nama Customer : ".$master->nama_customer_pusat."
											Berhasil dihapus."
					));
				}else{
					echo json_encode(array(	
						"status"     => 0,
						"info_pesan" => "Oops!",
						"pesan"      => "Terjadi kesalahan, coba lagi !"
					));
				}
			}else{
				echo json_encode(array(	
					"status"     => 0,
					"info_pesan" => "Oops!",
					"pesan"      => "Data transaksi penjualan tidak ditemukan"
				));
			}
		}else{
			redirect(base_url().'laporan_penjualan_pusat');
		}
	}

	public function ajax_selesai_transaksi(){
		$id_penjualan_m = $this->input->post('id_penjualan_m');
		if($id_penjualan_m ==''){
			redirect(base_url().'laporan_penjualan_pusat');
			exit();
		}

		// Cek user acces menu
		$id_pegawai 				= $this->session->userdata('id_pegawai');
		$cek_useraccess_transaksi 	= $this->useraccess_pusat->cek_access($id_pegawai, '27');
		$cek_useraccess_laporan 	= $this->useraccess_pusat->cek_access($id_pegawai, '39');
		$data_master 				= $this->penjualan_master->get_master($id_penjualan_m);
		if($data_master){
			$status 				= $data_master->status_penjualan;
		}else{
			echo json_encode(array(	"status" 		=> 0,
								  	"info_pesan" 	=> "Oops!",
									"pesan" 		=> "No. penjualan tidak terdaftar.",
									"tipe_pesan" 	=> "error"
								  )
						   	);
			exit();
		}

		if(	$cek_useraccess_laporan->act_update == 1 OR  $cek_useraccess_laporan->act_create == 1 OR
			$cek_useraccess_transaksi->act_update == 1 OR  $cek_useraccess_transaksi->act_create == 1 ){
			if($status == "DISIAPKAN"){
				if($data_master->tipe_customer_pusat == "MRC"){
					$status_sekarang = "DIKIRIM";
				}else{
					$status_sekarang = "SELESAI";
				}

				$data = array(	
					'tanggal'                => date('Y-m-d'),
					'status_penjualan'       => $status_sekarang,
					'id_pegawai_pembaharuan' => $this->session->userdata('id_pegawai')
				);
				$this->penjualan_master->update_status(array('id_penjualan_m' => $id_penjualan_m), $data);

				echo json_encode(array(	"status" 		=> 1,
									  	"info_pesan" 	=> "Transaksi penjualan berhasil diselesaikan,
									  						Apakah anda ingin mencetak faktur penjualannya sekarang ?",
										"pesan" 		=> "No. Penjualan : ".$data_master->no_penjualan."
															Nama Customer : ".$data_master->nama_customer_pusat."
															Status Penjualan : ".$status_sekarang."
															",
										"url_cetak"		=> "faktur_penjualan"
									  )
						   		);
			}else{
				echo json_encode(array(	"status" 		=> 0,
									  	"info_pesan" 	=> "Maaf proses ini tidak bisa dilanjutkan karna status penjualan : + ".$status."",
										"pesan" 		=> "No. Penjualan : ".$data_master->no_penjualan."
															Nama Customer : ".$data_master->nama_customer_pusat."
															Status Penjualan : ".$data_master->status_penjualan."
															",
										"error"			=> "error"
									  )
						   		);
			}
		}else{
			echo json_encode(array(	"status" 		=> 0,
								  	"info_pesan" 	=> "Maaf anda tidak diberikan izin untuk menyelesaikan transaksi penjualan,
														dengan status penjualan : SELESAI !",
									"pesan" 		=> "No. Penjualan : ".$data_master->no_penjualan."
														Nama Customer : ".$data_master->nama_customer_pusat."
														Status Penjualan : ".$status."
														",
									"tipe_pesan" 	=> "error"
								  )
						   	);
		}
	}
}
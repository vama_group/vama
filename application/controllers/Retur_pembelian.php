<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Retur_pembelian extends MY_Controller 
{
	function __construct()
	{
		parent::__construct();
		$this->CI =& get_instance();
		$this->load->model('pegawai_pusat_model','pegawai_pusat');
        $this->load->model('useraccess_pusat_model','useraccess_pusat');
		$this->load->model('supplier_model','supplier');
		$this->load->model('barang_pusat_model','barang_pusat');
		$this->load->model('pembelian_master_model','pembelian_master');
		$this->load->model('pembelian_detail_model','pembelian_detail');
		$this->load->model('retur_pembelian_master_model','retur_pembelian_master');
		$this->load->model('retur_pembelian_detail_model','retur_pembelian_detail');
	}

	public function index()
	{
		// Cek user acces menu
		$id_pegawai = $this->session->userdata('id_pegawai');
		if($id_pegawai == ''){
			redirect(base_url().'login');
		}
		
		$cek_useraccess = $this->useraccess_pusat->cek_access($id_pegawai, '26');
		if($cek_useraccess){
			if($cek_useraccess->act_read == '0' or $cek_useraccess->act_read == '-'){
				redirect(base_url().'dashboard');
			}					
		}else{
			redirect(base_url().'dashboard');
		}
	
		$data_pegawai = $this->pegawai_pusat->get_by_id($id_pegawai);
		if($data_pegawai){
			$cek_useraccess_laporan       = $this->useraccess_pusat->cek_access($id_pegawai, '37');			
			$data['data_pegawai']         = $data_pegawai;
			$data['data_induk_menu']      = $this->useraccess_pusat->get_induk_menu($id_pegawai);
			$data['access_create']        = $cek_useraccess->act_create;
			$data['access_update']        = $cek_useraccess->act_update;
			$data['access_laporan']       = $cek_useraccess_laporan->act_read;
			
			$data['atribut_halaman']      = 'Retur Pembelian Pusat';
			$data['id_retur_pembelian_m'] = '0';
			$data['halaman_transaksi']    = $this->load->view('admin/transaksi/retur_pembelian/transaksi',$data,true);
			$data['halaman_plugin']       = $this->load->view('admin/transaksi/retur_pembelian/plugin',$data,true);
			$data['isi_halaman']          = $this->load->view('admin/transaksi/retur_pembelian/index',$data,true);
			$this->load->view('admin/layout',$data);
		}else{
			redirect(base_url().'login');
		}		
	}

	public function transaksi()
	{		
		$id_retur_pembelian_m = $this->input->get('id');
		$data_retur_pembelian = $this->retur_pembelian_master->get_master2($id_retur_pembelian_m);
		if($data_retur_pembelian->id_retur_pembelian_m == ''){
			redirect(base_url().'retur_pembelian');
			exit();
		}

		// Cek user acces menu
		$id_pegawai             = $this->session->userdata('id_pegawai');
		$cek_useraccess_laporan = $this->useraccess_pusat->cek_access($id_pegawai, '37');
		$data['access_laporan'] = $cek_useraccess_laporan->act_read;
		
		if($cek_useraccess_laporan->act_update == '1' or $data_retur_pembelian->status_retur == 'MENUNGGU'){		
			$cek_useraccess        = $this->useraccess_pusat->cek_access($id_pegawai, '26');
			$data['access_create'] = $cek_useraccess->act_create;
			$data['access_update'] = $cek_useraccess->act_update;

			// cari data pegawai
			$id_pegawai   = $this->session->userdata('id_pegawai');
			$data_pegawai = $this->pegawai_pusat->get_by_id($id_pegawai);
			$this->CI->session->set_userdata('usergroup_name', $data_pegawai->usergroup_name);
			$data['data_pegawai']         = $data_pegawai;
			$data['data_induk_menu']      = $this->useraccess_pusat->get_induk_menu($id_pegawai);
			
			$data['atribut_halaman']      = 'Retur Pembelian';
			$data['id_retur_pembelian_m'] = $id_retur_pembelian_m;
			$data['no_retur_pembelian']   = $data_retur_pembelian->no_retur_pembelian;
			$data['halaman_transaksi']    = $this->load->view('admin/transaksi/retur_pembelian/transaksi',$data,true);
			$data['halaman_plugin']       = $this->load->view('admin/transaksi/retur_pembelian/plugin',$data,true);
			$data['isi_halaman']          = $this->load->view('admin/transaksi/retur_pembelian/index',$data,true);
			$this->load->view('admin/layout',$data);
		}else{
			redirect(base_url().'retur_pembelian');
			exit();
		}
	}

	public function simpan_transaksi()
	{
		if( ! empty($_POST['id_retur_pembelian_m'])){
			// Cek user acces menu
			$url                  = $this->input->post('url');
			$id_pegawai           = $this->session->userdata('id_pegawai');
			$cek_useraccess       = $this->useraccess_pusat->cek_access($id_pegawai, '26');
			$id_retur_pembelian_m = $this->input->post('id_retur_pembelian_m');

			if($cek_useraccess->act_create == '1' or $cek_useraccess->act_update == '1'){
				$no_retur_pembelian = $this->input->post('no_retur_pembelian');
				$no_pembelian       = $this->input->post('no_pembelian');
				$id_pegawai         = $this->session->userdata('id_pegawai');
				$id_supplier        = $this->input->post('id_supplier');
				$catatan            = $this->clean_tag_input($this->input->post('catatan'));
				$biaya_lain         = $this->input->post('biaya_lain');
				if($biaya_lain == ''){
					$biaya_lain = '0';
				}

				$data_total         = $this->retur_pembelian_detail->get_total($id_retur_pembelian_m);
				if($data_total){
					$ttl           = $data_total->total;
					// $ppn           = $ttl*(10/100);
					$ppn           = '0';
					// $grand_total   = $ttl+$ppn+$biaya_lain;
					$grand_total   = $ttl;
					$jumlah_barang = $data_total->jumlah_barang;

					// Update total dan grandtotal di retur_pembelian master
					$master = $this->retur_pembelian_master->update_master(
						$id_retur_pembelian_m, $no_pembelian, $id_supplier, 
						$ttl, $biaya_lain, $ppn, $grand_total, 
						$catatan, $id_pegawai, 'SELESAI'
					);
				}

				if($master){
					echo json_encode(array('status'	 	=> 1, 
										   'pesan' 		=> "	Transaksi berhasil disimpan,
										   						No. Retur Pembelian : ".$no_retur_pembelian.",
										   						Apakah anda ingin mencetak fakturnya ?
										   					"
									));
				}else{
					echo json_encode(array(	'status' 	=> 0,
											'pesan'		=> 'Terjadi kesalah saat menyimpan, harap coba kembali.',
											'url' 		=> 'retur_pembelian/transaksi/?&id='.$id_pembelian_m
									));
				}
			}else{
				echo json_encode(array(	'status' 	=> 0,
										'pesan'		=> 'Maaf anda tidak diizinkan untuk menyimpan transaksi retur pembelian.',
										'url' 		=> 'retur_pembelian/transaksi/?&id='.$id_pembelian_m
								));
			}
		}else{
			redirect(base_url().'retur_pembelian');
		}
	}
	
	public function tahan_transaksi()
	{
		if( ! empty($_POST['id_retur_pembelian_m'])){
			// Cek user acces menu
			$url                  = $this->input->post('url');
			$id_pegawai           = $this->session->userdata('id_pegawai');
			$cek_useraccess       = $this->useraccess_pusat->cek_access($id_pegawai, '26');
			$id_retur_pembelian_m = $this->input->post('id_retur_pembelian_m');

			if($cek_useraccess->act_create == '1' or $cek_useraccess->act_update == '1'){
				$no_retur_pembelian = $this->input->post('no_retur_pembelian');
				$no_pembelian       = $this->input->post('no_pembelian');
				$id_pegawai         = $this->session->userdata('id_pegawai');
				$id_supplier        = $this->input->post('id_supplier');
				$catatan            = $this->clean_tag_input($this->input->post('catatan'));
				$biaya_lain         = $this->input->post('biaya_lain');
				if($biaya_lain == ''){
					$biaya_lain = 0;
				}

				$data_total         = $this->retur_pembelian_detail->get_total($id_retur_pembelian_m);
				if($data_total){
					$ttl           = $data_total->total;
					// $ppn           = $ttl*(10/100);
					$ppn           = 0;
					// $grand_total   = $ttl+$ppn+$biaya_lain;
					$grand_total   = $ttl;
					$jumlah_barang = $data_total->jumlah_barang;

					// Update total dan grandtotal di retur_pembelian master
					$master = $this->retur_pembelian_master->update_master(
						$id_retur_pembelian_m, $no_pembelian, $id_supplier, 
						$ttl, $biaya_lain, $ppn, $grand_total, 
						$catatan, $id_pegawai, 'MENUNGGU'
					);
				}

				if($master){
					echo json_encode(array('status'	 	=> 1, 
										   'pesan' 		=> "	Transaksi berhasil ditahan,
										   						dengan no. retur pembelian : ".$no_retur_pembelian."
										   				   "
									));
				}else{
					echo json_encode(array(	'status' 	=> 0,
											'pesan'		=> 'Terjadi kesalah saat menahan transaksi, harap coba kembali.',
											'url' 		=> 'retur_pembelian/transaksi/?&id='.$id_pembelian_m
									));
				}
			}else{
				echo json_encode(array(	'status' 	=> 0,
										'pesan'		=> 'Maaf anda tidak diizinkan untuk menahan transaksi retur pembelian.',
										'url' 		=> 'retur_pembelian/transaksi/?&id='.$id_pembelian_m
								));
			}
		}else{
			redirect(base_url().'retur_pembelian');
		}
	}

	private function no_retur_pembelian_baru($id_pegawai){
		if($id_pegawai == ''){
			redirect(base_url().'pembelian');
			exit();
		}

		$tanggal_sekarang = date('Y-m-d');
		$bulan_sekarang   = date('m');
		$tahun_sekarang   = date('Y');
		$tahun_sekarang_2 = date('y'); 

		$inisial = "RPB";
		$akhir   = $this->retur_pembelian_master->no_retur_pembelian_baru($tahun_sekarang, $bulan_sekarang, $id_pegawai);
	    if($akhir){
			$no_baru = $akhir->no_baru;
	    }else{
			$no_baru = 1;	
	    }

	    if($no_baru < 10){
			$depan = '000';
	    }elseif($no_baru< 100) {
	      $depan      		= '00';
	    }elseif($no_baru< 1000) {
			$depan = '0';
	    }else{
			$depan = '';
	    }

		$jumlah                  = $no_baru;
		$no_retur_pembelian_baru = $inisial.'-'.$bulan_sekarang.$tahun_sekarang_2.'-'.$id_pegawai.'-'.$depan.$jumlah;
		return $no_retur_pembelian_baru;
	}

	public function edit_transaksi()
	{
		$id_retur_pembelian_m         = $this->input->get('id');
		$data_retur_pembelian         = $this->retur_pembelian_master->get_master1($id_retur_pembelian_m);
		
		// cari data pegawai
		$id_pegawai                   = $this->session->userdata('id_pegawai');
		$data_pegawai                 = $this->pegawai_pusat->get_by_id($id_pegawai);
		$data['data_pegawai']         = $data_pegawai;
		$data['data_induk_menu']      = $this->useraccess_pusat->get_induk_menu($id_pegawai);
		
		$data['atribut_halaman']      = 'Retur Pembelian';
		$data['id_retur_pembelian_m'] = $id_retur_pembelian_m;
		$data['no_retur_pembelian']   = $data_retur_pembelian->no_retur_pembelian;
		$data['halaman_transaksi']    = $this->load->view('admin/transaksi/retur_pembelian/transaksi',$data,true);
		$data['halaman_plugin']       = $this->load->view('admin/transaksi/retur_pembelian/plugin',$data,true);
		$data['isi_halaman']          = $this->load->view('admin/transaksi/retur_pembelian/index',$data,true);
		$this->load->view('admin/layout',$data);
	}

	public function ambil_data()
	{
		// Cek user acces menu
		$id_pegawai     = $this->session->userdata('id_pegawai');
		$cek_useraccess = $this->useraccess_pusat->cek_access($id_pegawai, '26');
		if($cek_useraccess->act_read == '0' or $cek_useraccess->act_read == '-'){
			redirect(base_url().'retur_pembelian');
			exit();
		}
		
		$id_retur_pembelian_m = $this->input->post('id_retur_pembelian_m');
		if($id_retur_pembelian_m == ''){
			redirect(base_url().'retur_pembelian');
			exit();
		}

		if($this->session->userdata('usergroup_name') == 'Super Admin'){
			$data_induk = $this->retur_pembelian_master->get_master1($id_retur_pembelian_m);
		}else{
			$data_induk = $this->retur_pembelian_master->get_master2($id_retur_pembelian_m);
		}

		$data_batal = $this->retur_pembelian_master->get_jumlah_batal($id_retur_pembelian_m);
		echo json_encode(array(
			"data_induk" => $data_induk,
			"data_batal" => $data_batal
		));
	}

	public function simpan_detail()
	{
		// Cek user acces menu
		$id_pegawai     = $this->session->userdata('id_pegawai');
		$cek_useraccess = $this->useraccess_pusat->cek_access($id_pegawai, '26');

		$id_barang_pusat = $this->input->post('id_barang_pusat');
		if($id_barang_pusat == ''){
			redirect(base_url().'retur_pembelian');
			exit();
		}

		// Ambil data retur_pembelian master
		$id_retur_pembelian_m = $this->clean_tag_input($this->input->post('id_retur_pembelian_m'));
		$no_retur_pembelian   = $this->clean_tag_input($this->input->post('no_retur_pembelian'));
		$no_pembelian         = $this->clean_tag_input($this->input->post('no_pembelian'));
		$id_supplier          = $this->input->post('id_supplier');
		$tanggal              = date('Y-m-d H:i:s');
		$id_pegawai           = $this->session->userdata('id_pegawai');
		$catatan              = $this->clean_tag_input($this->input->post('catatan'));
		$biaya_lain           = $this->input->post('biaya_lain');
		$ppn                  = $this->input->post('ppn');

		// Ambil detail barang
		$dari_stok        = $this->input->post('dari_stok');
		$jumlah_retur     = $this->input->post('jumlah_retur');
		$keterangan_retur = $this->input->post('keterangan_retur');

		// Validasi retur_pembelian master sudah ada atau belum
		$data_retur_pembelian_master = $this->retur_pembelian_master->get_by_id($id_retur_pembelian_m);
		if($data_retur_pembelian_master){
			if($cek_useraccess->act_create == '1' or $cek_useraccess->act_update == '1'){
				$id_master   = $data_retur_pembelian_master->id_retur_pembelian_m;
				if($id_supplier <> $data_retur_pembelian_master->id_supplier){
					echo json_encode(array(	
						'status'        => 2,
						'pesan'         => 'Maaf satu no faktur hanya bisa untuk satu supplier.',
						'url'           => 'retur_pembelian/transaksi/?&id='.$id_retur_pembelian_m
					));
					exit();
				}
			}else{
				echo json_encode(array(	'status' 	=> 2,
										'pesan'		=> 'Maaf anda tidak diizinkan untuk menyimpan data retur pembelian barang.',
										'url' 		=> 'retur_pembelian/transaksi/?&id='.$id_retur_pembelian_m
									  )
								);
				exit();
			}
		}else{
			if($cek_useraccess->act_create == '1' or $cek_useraccess->act_update == '1'){
				$this->validate_stok($id_barang_pusat, $dari_stok, '-', $jumlah_retur, '0');

				// Insert retur_pembelian master
				$no_retur_pembelian = $this->no_retur_pembelian_baru($id_pegawai);
				$master 			= $this->retur_pembelian_master->insert_master(
					$no_retur_pembelian, $no_pembelian, $id_supplier, 
			 	   '0', '0', '0', '0', 
			 	   $catatan, $tanggal, $id_pegawai
			 	);

				$data_retur_pembelian_master = $this->retur_pembelian_master->get_by_no_trans($no_retur_pembelian);
				$id_master                   = $data_retur_pembelian_master->id_retur_pembelian_m;
			}else{
				echo json_encode(array(	
					'status' => 2,
					'pesan'  => 'Maaf anda tidak diizinkan untuk menyimpan data retur pembelian barang.',
					'url'    => 'retur_pembelian/transaksi/?&id='.$id_retur_pembelian_m
				));
				exit();
			}
		}

		if($id_master){
			// Ambil data barang dari master barang pusat
			$data_barang = $this->barang_pusat->get_id($id_barang_pusat);
			if($data_barang){
				$sku           = $data_barang->sku;
				$nama_barang   = $data_barang->nama_barang;
				$harga_satuan  = $data_barang->modal;
				$subtotal      = $harga_satuan * $jumlah_retur;
				$stok_sekarang = $data_barang->total_stok;
			}

			// Ambil data barang dari inputan
			if($this->session->userdata('usergroup_name') == 'Super Admin'){
				$harga_satuan = $this->input->post('harga_beli');
				$subtotal     = $this->input->post('subtotal');
			}

			// Validasi insert atas update di retur_pembelian detail
			$data_retur_pembelian_detail = $this->retur_pembelian_detail->get_id($id_master, $id_barang_pusat);
			if ($data_retur_pembelian_detail){
				if($cek_useraccess->act_update == '1'){
					$id_detail         = $data_retur_pembelian_detail->id_retur_pembelian_d;
					$dari_stok_lama    = $data_retur_pembelian_detail->dari_stok;
					$jumlah_retur_lama = $data_retur_pembelian_detail->jumlah_retur;
					$this->validate_stok($id_barang_pusat, $dari_stok, $dari_stok_lama, $jumlah_retur, $jumlah_retur_lama);

					// update dan kembalikan stok sebelumnya
					$simpan_data = $this->retur_pembelian_detail->update_detail( 
						$id_master, $id_barang_pusat,  
						$dari_stok, $jumlah_retur, $harga_satuan, $subtotal, $keterangan_retur, 
						$id_pegawai, $jumlah_retur_lama, $dari_stok_lama
					);
				}else{
					echo json_encode(array(	
						'status' => 2,
						'pesan'  => 'Maaf anda tidak diizinkan untuk mengedit data retur pembelian barang.',
						'url'    => 'retur_pembelian/transaksi/?&id='.$id_retur_pembelian_m
					));
					exit();
				}
			}else{
				if($cek_useraccess->act_create == '1'){
					$id_detail 			= '';
					$this->validate_stok($id_barang_pusat, $dari_stok, '-', $jumlah_retur, '0');

					// insert
					$simpan_data = $this->retur_pembelian_detail->insert_detail( 
										$id_master, $id_barang_pusat,  
										$dari_stok, $jumlah_retur, $harga_satuan, $subtotal, $keterangan_retur, $id_pegawai
									);
				}else{
					echo json_encode(array(	
						'status' => 2,
						'pesan'  => 'Maaf anda tidak diizinkan untuk menambahkan data retur pembelian barang.',
						'url'    => 'retur_pembelian/transaksi/?&id='.$id_retur_pembelian_m
					));
					exit();
				}
			}

			if($simpan_data){
				$data_total = $this->retur_pembelian_detail->get_total($id_master);
				if ($data_total){
					$ttl           = $data_total->total;
					// $ppn           = $ttl*(10/100);
					$ppn           = 0;
					// $grand_total   = $ttl+$ppn+$biaya_lain;
					$grand_total   = $ttl;
					$jumlah_barang = $data_total->jumlah_barang;

					// Update total dan grandtotal di retur_pembelian master
					$master = $this->retur_pembelian_master->update_master(
						$id_master, $no_pembelian, $id_supplier, 
						$ttl, $biaya_lain, $ppn, $grand_total, 
						$catatan, $id_pegawai, 'MENUNGGU'
					);
				}else{
					$ttl           = '0';
					$grand_total   = '0';
					$jumlah_barang = '0';
				}

				$data_batal = $this->retur_pembelian_master->get_jumlah_batal($id_master);
				if($data_batal){
					$jumlah_barang_batal = $data_batal->jumlah_barang;
				}else{
					$jumlah_barang_batal = '0';
				}

				// Tambah stok dan update modal, resistensi dan modal bersih
				if($dari_stok=='JUAL'){
					$this->barang_pusat->update_stok($id_barang_pusat, $jumlah_retur);
				}else if($dari_stok=='RUSAK'){
					$this->barang_pusat->update_stok_rusak($id_barang_pusat, $jumlah_retur);
				}

				if($this->session->userdata('usergroup_name') == "Super Admin"){
					echo json_encode(array(
						'status'              => 1,
						'pesan'               => "Data retur pembelian barang berhasil disimpan!",
						'url'                 => 'retur_pembelian/transaksi/?&id='.$id_master,
						'id_rpm'              => $id_master,
						'jumlah_barang'       => $jumlah_barang,
						'jumlah_barang_batal' => $jumlah_barang_batal,
						'total'               => $ttl,
						'ppn'                 => $ppn,
						'grand_total'         => $grand_total
					));
				}else{
					echo json_encode(array(
						'status'              => 1,
						'pesan'               => "Data retur pembelian barang berhasil disimpan!",
						'url'                 => 'retur_pembelian/transaksi/?&id='.$id_master,
						'id_rpm'              => $id_master,
						'jumlah_barang'       => $jumlah_barang,
						'jumlah_barang_batal' => $jumlah_barang_batal,
					));
				}
			}else{
				echo json_encode(array(
					'status' => 2,
					'url'    => 'retur_pembelian/transaksi/?&id='.$id_master
				));
			}
		}
	}

	public function ajax_list()
	{
		// Cek user acces menu
		$id_pegawai     = $this->session->userdata('id_pegawai');
		$cek_useraccess = $this->useraccess_pusat->cek_access($id_pegawai, '26');
		if($cek_useraccess->act_read == '0' or $cek_useraccess->act_read == '-'){
			redirect(base_url().'dashboard');
		}

		$id_retur_pembelian_m = $this->clean_tag_input($this->input->post('id_retur_pembelian_m'));
		if($id_retur_pembelian_m == ''){
			redirect(base_url().'retur_pembelian');
		}

		$list = $this->retur_pembelian_detail->get_datatables('_get_datatables_query', $id_retur_pembelian_m, '');
		$data = array();
		$no   = $_POST['start'];

		foreach ($list as $rpd) {
			$no++;
			$row = array();
			$row[] = $no;

			//add html for action
			if($cek_useraccess->act_update == 1){
				$tombol_edit = '<a class="btn btn-rounded btn-default btn-xs" onclick="edit_detail('."'".$rpd->id_retur_pembelian_d."'".')">
									<i class="fa fa-pencil" style="color:blue;"></i>
								</a>';
			}else{
				$tombol_edit = '';
			}

			if($cek_useraccess->act_delete == 1){
				$tombol_hapus = '<a class="btn btn-rounded btn-default btn-xs" onclick="verifikasi_hapus_detail('."'".$rpd->id_retur_pembelian_d."'".')">
									<i class="fa fa-times" style="color:red;"></i>
								</a>';
			}else{
				$tombol_hapus = '';
			}

			if($tombol_edit == '' AND $tombol_hapus == ''){
				$tombol_keterangan = '<b class="btn btn-rounded btn-default btn-xs text-muted text-center">Tidak ada akses</b>';
			}else{
				$tombol_keterangan = '';
			}

			$row[] =	'
							'.$tombol_edit.'
							'.$tombol_hapus.'
							'.$tombol_keterangan.'
				  		';

			$row[] 	= $rpd->sku;
			$row[] 	= $rpd->nama_barang;

			if ($rpd->dari_stok=='JUAL'){
				$row[] 	= '<span class="badge badge-success text-center">'.$rpd->dari_stok.'</span>';
			}else if ($rpd->dari_stok=='RUSAK'){
				$row[] 	= '<span class="badge badge-danger text-center">'.$rpd->dari_stok.'</span>';				
			}else{
				$row[] 	= '<span class="badge badge-warning text-center">'.$rpd->dari_stok.'</span>';
			}

			$row[] 	= '<span class="pull-right">'.number_format($rpd->jumlah_retur,'0',',','.').'</span>';
			if($this->session->userdata('usergroup_name') == 'Super Admin'){
				$row[] 	= '<span class="pull-right">'.number_format($rpd->harga_satuan,'0',',','.').'</span>';
				$row[] 	= '<span class="pull-right">'.number_format($rpd->subtotal,'0',',','.').'</span>';
			}

			$row[] 	= $rpd->keterangan;
			$row[] 	= $rpd->pegawai_save;
			$row[] 	= $rpd->tanggal_pembuatan;	
			$row[] 	= $rpd->pegawai_edit;
			$row[] 	= $rpd->tanggal_pembaharuan;		
			$data[]	= $row;
		}

		$output = array(
			"draw"            => $_POST['draw'],
			"recordsTotal"    => $this->retur_pembelian_detail->count_all('vamr4846_vama.retur_pembelian_detail', $id_retur_pembelian_m, ''),
			"recordsFiltered" => $this->retur_pembelian_detail->count_filtered('_get_datatables_query', $id_retur_pembelian_m, ''),
			"data"            => $data
		);		
		echo json_encode($output);
	}

	public function ajax_list_batal()
	{
		// Cek user acces menu
		$id_pegawai     = $this->session->userdata('id_pegawai');
		$cek_useraccess = $this->useraccess_pusat->cek_access($id_pegawai, '26');
		if($cek_useraccess->act_read == '0' or $cek_useraccess->act_read == '-'){
			redirect(base_url().'dashboard');
		}

		$id_retur_pembelian_m 		= $this->clean_tag_input($this->input->post('id_retur_pembelian_m'));
		if($id_retur_pembelian_m == ''){
			redirect(base_url().'retur_pembelian');
		}

		$list = $this->retur_pembelian_detail->get_datatables('_get_datatables_query_batal', $id_retur_pembelian_m, '');
		$data = array();
		$no   = $_POST['start'];

		foreach ($list as $rpd) {
			$no++;
			$row   = array();
			$row[] = $no;
			$row[] = $rpd->sku;
			$row[] = $rpd->nama_barang;

			if ($rpd->dari_stok=='JUAL'){
				$row[] 	= '<span class="badge badge-success text-center">'.$rpd->dari_stok.'</span>';
			}else if ($rpd->dari_stok=='RUSAK'){
				$row[] 	= '<span class="badge badge-danger text-center">'.$rpd->dari_stok.'</span>';				
			}else{
				$row[] 	= '<span class="badge badge-warning text-center">'.$rpd->dari_stok.'</span>';
			}

			$row[] 	= '<span class="pull-right">'.number_format($rpd->jumlah_retur,'0',',','.').'</span>';
			if($this->session->userdata('usergroup_name') == 'Super Admin'){
				$row[] 	= '<span class="pull-right">'.number_format($rpd->harga_satuan,'0',',','.').'</span>';
				$row[] 	= '<span class="pull-right">'.number_format($rpd->subtotal,'0',',','.').'</span>';
			}

			$row[] 	= $rpd->keterangan;
			$row[] 	= $rpd->keterangan_batal;
			$row[] 	= $rpd->pegawai_pembatalan;
			$row[] 	= $rpd->tanggal_pembatalan;	
			$data[]	= $row;
		}

		$output = array(
			"draw"            => $_POST['draw'],
			"recordsTotal"    => $this->retur_pembelian_detail->count_all('vamr4846_vama.retur_pembelian_detail_batal', $id_retur_pembelian_m, ''),
			"recordsFiltered" => $this->retur_pembelian_detail->count_filtered('_get_datatables_query_batal', $id_retur_pembelian_m, ''),
			"data"            => $data
		);		
		echo json_encode($output);
	}

	public function ajax_list_barang()
	{
		// Cek user acces menu
		$id_pegawai     = $this->session->userdata('id_pegawai');
		$cek_useraccess = $this->useraccess_pusat->cek_access($id_pegawai, '26');
		if($cek_useraccess->act_read == '0' or $cek_useraccess->act_read == '-'){
			redirect(base_url().'dashboard');
			exit();
		}

		$id_retur_pembelian_m 	= $this->clean_tag_input($this->input->post('id_retur_pembelian_m'));
		if($id_retur_pembelian_m == ''){
			redirect(base_url().'retur_pembelian');
			exit();
		}

		$id_supplier   = $this->clean_tag_input($this->input->post('id_supplier'));
		$data_supplier = $this->supplier->get_by_id($id_supplier);
		if($data_supplier){
			$kode_supplier = $data_supplier->kode_supplier;
		}else{
			$kode_supplier = '0';
		}

		$data_retur_pembelian_master = $this->retur_pembelian_master->get_by_id($id_retur_pembelian_m);
		if($data_retur_pembelian_master){
			$id_master   = $data_retur_pembelian_master->id_retur_pembelian_m;
			$id_supplier = $data_retur_pembelian_master->id_supplier;
		}else{
			$id_master   = '0';
			$id_supplier = '0';
		}

		$list = $this->retur_pembelian_detail->get_datatables('_get_datatables_query_barang', $id_master, $kode_supplier);
		$data = array();
		$no   = $_POST['start'];

		foreach ($list as $bp){
			$no++;
			$row   = array();
			$row[] = $no;
			$row[] = $bp->sku;
			$row[] = $bp->nama_barang;
			$row[] 	= '<span class="pull-right">'.number_format($bp->total_stok,'0',',','.').'</span>';
			$row[] 	= '<span class="pull-right">'.number_format($bp->total_stok_rusak,'0',',','.').'</span>';

			if($this->session->userdata('usergroup_name') == 'Super Admin'){
				$row[] 	= '<span class="pull-right">'.number_format($bp->modal,'0',',','.').'</span>';
			}

			$row[] 	= '<a class="btn btn-primary btn-xs" onclick="proses_barang('."'".$bp->sku."'".')">
					   	<i class="fa fa-check"></i> PILIH
					   </a>';
			$data[]	= $row;
		}

		$output = array(
			"draw"            => $_POST['draw'],
			"recordsTotal"    => $this->retur_pembelian_detail->count_all('vamr4846_vama.barang_pusat', $id_master, $kode_supplier),
			"recordsFiltered" => $this->retur_pembelian_detail->count_filtered('_get_datatables_query_barang', $id_master, $kode_supplier),
			"data"            => $data
		);
		echo json_encode($output);
	}

	public function ajax_edit()
	{
		// Cek user acces menu
		$id_pegawai 		= $this->session->userdata('id_pegawai');
		$cek_useraccess 	= $this->useraccess_pusat->cek_access($id_pegawai, '26');
		if($cek_useraccess->act_update == '0' or $cek_useraccess->act_update == '-'){
			redirect(base_url()."retur_pembelian");
			exit();
		}

		$id_retur_pembelian_d 	= $this->input->post('id_retur_pembelian_d');
		$url 					= $this->input->post('url');
		if($id_retur_pembelian_d == ''){
			if($url == ''){
				redirect(base_url()."retur_pembelian");					
			}else{
				redirect(base_url().$url);
			}
		}else{
			if($this->session->userdata('usergroup_name') == "Super Admin"){
				$data = $this->retur_pembelian_detail->get_retur_pembelian_detail_1($id_retur_pembelian_d);
				echo json_encode(array(
										"status" 	=> 1,
										"data" 		=> $data
									  )
								);
			}else{
				$data = $this->retur_pembelian_detail->get_retur_pembelian_detail_2($id_retur_pembelian_d);
				echo json_encode(array(
										"status" 	=> 1,
										"data" 		=> $data
									  )
								);				
			}
		}
	}

	public function ajax_verifikasi_hapus_detail()
	{
		// Cek user acces menu
		$id_pegawai 		= $this->session->userdata('id_pegawai');
		$cek_useraccess 	= $this->useraccess_pusat->cek_access($id_pegawai, '26');
		if($cek_useraccess->act_delete == '0' or $cek_useraccess->act_delete == '-'){
			redirect(base_url()."retur_pembelian");
			exit();
		}

		$id_retur_pembelian_d 	= $this->input->post('id_retur_pembelian_d');
		$url 					= $this->input->post('url');
		if($id_retur_pembelian_d == ''){
			if($url == ''){
				redirect(base_url()."retur_pembelian");					
			}else{
				redirect(base_url().$url);
			}
		}

		$data = $this->retur_pembelian_detail->get_retur_pembelian_detail_1($id_retur_pembelian_d);
		if($data){
			echo json_encode(array(	"status"	=> 1,
									"pesan" 	=>	"<table class='table' id='detail_hapus'>
														<thead></thead>
														<tbody>
															<tr>
																<td>
																	<small>SKU : </small>
																	<b class='text-dark'>$data->sku </b><br/>
																	
																	<small>Jumlah Retur : </small>
																	<b class='text-dark'>$data->jumlah_retur</b><br/><br/>

																	<small>Nama Barang : </small> <br/>	
																	<b class='text-dark'>$data->nama_barang</b></br><br/>

																	<small>Dari Stok : </small> <br/>	
																	<b class='text-dark'>$data->dari_stok</b></br><br/>

																	<small>Jumlah Retur : </small><br/>	
																	<h2 class='text-dark'>$data->jumlah_retur</h2>

																	<textarea name='keterangan_batal' id='keterangan_batal' class='form-control input-md' placeholder='Keterangan batal' style='resize: vertical;'></textarea>
																</td>
															</tr>
														</tbody>
													</table>",
									"footer"	=> 	"<button onclick='hapus_detail($data->id_retur_pembelian_d)' type='button' class='btn btn-primary waves-effect waves-light' 
														data-dismiss='modal' autofocus>
														Iya, Hapus
													</button> 
													<button type='button' class='btn btn-default waves-effect' data-dismiss='modal'>Batal</button>"
								)
							);
		}else{
			redirect(base_url().'pembelian');
		}
	}

	public function ajax_hapus_detail()
	{
		if($this->input->is_ajax_request()){
			$id_pegawai_pembatalan = $this->session->userdata('id_pegawai');
			$keterangan_batal      = $this->input->post('keterangan_batal');
			$id_supplier           = $this->input->post('id_supplier');
			$catatan               = $this->clean_tag_input($this->input->post('catatan'));
			$biaya_lain            = $this->input->post('biaya_lain');
			
			if ($biaya_lain==''){
				$biaya_lain='0';
			}

			// Cek user acces menu
			$id_pegawai     = $this->session->userdata('id_pegawai');
			$cek_useraccess = $this->useraccess_pusat->cek_access($id_pegawai, '26');
			if($cek_useraccess->act_delete == '0' or $cek_useraccess->act_delete == '-'){
				redirect(base_url()."retur_pembelian");
				exit();
			}

			$id_retur_pembelian_d = $this->input->post('id_retur_pembelian_d');
			$url                  = $this->input->post('url');
			if($url == ''){
				$url = 'retur_pembelian';					
			}

			if($id_retur_pembelian_d == ''){
				redirect(base_url()."retur_pembelian");
				exit();
			}

			// Ambil data retur_pembelian detail
			$dt           = $this->retur_pembelian_detail->get_retur_pembelian_detail_1($id_retur_pembelian_d);
			$no_pembelian = $dt->no_pembelian;
			if($dt){
				$id_master = $dt->id_retur_pembelian_m;
				$hapus     = $this->retur_pembelian_detail->hapus_retur_pembelian_detail(
								$dt->id_retur_pembelian_d,
								$dt->id_retur_pembelian_m,
								$dt->no_retur_pembelian,
								$dt->no_pembelian,
								$dt->id_supplier,

								$dt->id_barang_pusat,
								$dt->harga_satuan,
								$dt->jumlah_beli,
								$dt->jumlah_retur,
								$dt->subtotal,
								$dt->dari_stok,
								$dt->keterangan,

								$dt->id_pegawai_pembuatan,
								$dt->id_pegawai_pembaharuan,
								$id_pegawai_pembatalan,
								$dt->tanggal_pembuatan,
								$dt->tanggal_pembaharuan,
								$keterangan_batal
							);
			}
			
			if($hapus){
				$data_total = $this->retur_pembelian_detail->get_total($id_master);				
				if ($data_total){
					$ttl           = $data_total->total;
					$ppn           = 0;
					// $grand_total   = $ttl+$ppn+$biaya_lain;
					$grand_total   = $ttl;
					$jumlah_barang = $data_total->jumlah_barang;

					if($jumlah_barang == '0'){
						$hapus 		= $this->retur_pembelian_master->hapus_transaksi(
							$id_master, $id_supplier, $dt->no_retur_pembelian, $dt->no_pembelian, 
							$keterangan_batal, $id_pegawai_pembatalan
						);
						echo json_encode(array(
							'status'     => 0,
							'judul'      => 'Berhasil!',
							'pesan'      => 'Data pembelian barang berhasil dihapus.',
							'tipe_pesan' => 'success',
							'url'        => 'retur_pembelian'
						));
						exit();
					}

					// Update total dan grandtotal di retur_pembelian master
					$master = $this->retur_pembelian_master->update_master(
						$id_master, $no_pembelian, $id_supplier, 
					   $ttl, $biaya_lain, $ppn, $grand_total, 
					   $catatan, $id_pegawai_pembatalan, 'MENUNGGU'
					);
				}else{
					$ttl           = '0';
					$grand_total   = '0';
					$jumlah_barang = '0';
				}

				$data_batal = $this->retur_pembelian_master->get_jumlah_batal($id_master);
				if($data_batal){
					$jumlah_barang_batal = $data_batal->jumlah_barang;
				}else{
					$jumlah_barang_batal = '0';
				}

				// Tampilkan informasi hapus sesuai dengan user group
				if($this->session->userdata('usergroup_name') == "Super Admin"){
					echo json_encode(array(
						'status'              => '1',
						'id_pm'               => $id_master,
						'jumlah_barang'       => $jumlah_barang,
						'jumlah_barang_batal' => $jumlah_barang_batal,
						'total'               => $ttl,
						'ppn'                 => $ppn,
						'grand_total'         => $grand_total,
						'pesan'               =>	"Data berhasil dihapus</br>"
					));
				}else{
					echo json_encode(array(
						'status'              => '1',
						'id_pm'               => $id_master,
						'jumlah_barang'       => $jumlah_barang,
						'jumlah_barang_batal' => $jumlah_barang_batal,
						'pesan'               =>	"Data berhasil dihapus</br>"
					));
				}
			}else{
				echo json_encode(array(
					'status'     => 0,
					'judul'      => 'Gagal!',
					'pesan'      => 'Gagal menghapus data dari ajax.',
					'tipe_pesan' => 'error',
					'url'        => $url
				));
			}		
		}else{
			redirect(base_url().'retur_pembelian');
		}
	}

	public function cek_nota($nota)
	{
		$this->load->model('retur_pembelian_master_model');
		$cek = $this->retur_pembelian_master_model->cek_nota_validasi($nota);
		if($cek->num_rows() > 0){
			return FALSE;
		}
		return TRUE;
	}

	public function ajax_kode()
	{
		if($this->input->is_ajax_request()){
			$keyword       = $this->input->post('keyword');
			$id_rpm        = $this->input->post('id_rpm');
			$kode_supplier = $this->input->post('kode_supplier');
			$barang        = $this->barang_pusat->cari_kode_by_supplier_retur($keyword, $id_rpm, $kode_supplier);

			if($barang->num_rows() > 0){
				$json['kode_supplier'] 	= $kode_supplier;
				$json['status'] 		= 1;
				$json['datanya'] 		= "<ul id='daftar-autocomplete' class='list-group user-list'>";
				
				foreach($barang->result() as $b){
					if($b->foto!=""){
						$json['datanya'] .= "
			                <li class='user-list-item'>
			                	<div class='avatar avatar-sm-box'>
                                    <img src='assets/upload/image/barang/thumbs/$b->foto'>
			                	</div>";
			        }else{
						$nama_barang 	  = $b->nama_barang;
						$inisial 		  = substr($nama_barang, 0, 1);
						$json['datanya'] .= "
			                <li class='user-list-item'>
			                	<div class='avatar avatar-sm-box'>
                                    <span class='avatar-sm-box bg-info'>".$inisial."</span>
			                	</div>";
					}

					$json['datanya'] .= "
		                	<div class='user-desc'>
								<span id='barangnya' class='name'><b>".$b->nama_barang."</b></span>
								<span id='kode_supplier' class='name'>".$b->nama_supplier."</span>
								<span id='skunya' class='name'>".$b->sku."</span>
								<span id='total_stok' class='name text-primary'>STOK JUAL : ".$b->total_stok."</span>
								<span id='total_stok_rusak' class='name text-danger'>STOK RUSAK : ".$b->total_stok_rusak."</span>								
								<span id='id_barang' style='display :none;'>".$b->id_barang_pusat."</span>
					";

					if($this->session->userdata('usergroup_name') == 'Super Admin'){
						$json['datanya'] .= "
								<span id='harga_modal' class='name'>MODAL : Rp. ".$b->harga_modal."</span>
								<span id='modal' style='display:none;'>".$b->modal."</span>
						";
					}

					$json['datanya'] .= "	
								</div>
							</li>
					";
				}

				$json['datanya'] .= "</ul>";
			}else{
				$json['status'] 		= 0;
			}

			echo json_encode($json);
		}
	}

	public function cek_kode_barang($sku)
	{
		$cek_kode = $this->barang_pusat->cek_kode($sku);

		if($cek_kode->num_rows() > 0)
		{
			return TRUE;
		}
		return FALSE;
	}

	private function _validate()
	{
		$data = array();
		$data['error_string'] = array();
		$data['inputerror'] = array();
		$data['status'] = TRUE;

		if($this->input->post('nama_customer_pusat') == ''){
			$data['inputerror'][] = 'nama_customer_pusat';
			$data['error_string'][] = 'Nama customer pusat wajib diisi';
			$data['status'] = FALSE;
		}

		if($this->input->post('alamat_customer_pusat') == ''){
			$data['inputerror'][] = 'alamat_customer_pusat';
			$data['error_string'][] = 'Alamat customer pusat wajib diisi';
			$data['status'] = FALSE;
		}

		if($this->input->post('email') == ''){
			$data['inputerror'][] = 'email';
			$data['error_string'][] = 'Email customer pusat wajib diisi';
			$data['status'] = FALSE;
		}

		if($this->input->post('handphone1') == ''){
			$data['inputerror'][] = 'handphone1';
			$data['error_string'][] = 'Handphone-1 customer pusat wajib diisi';
			$data['status'] = FALSE;
		}

		if($data['status'] === FALSE){
			echo json_encode($data);
			exit();
		}
	}

	private function validate_stok($id_barang_pusat, $dari_stok, $dari_stok_lama, $jumlah_retur, $jumlah_retur_lama)
	{
		$data 			= array();
		$data['status'] = TRUE;

		$data_cek_stok 	= $this->barang_pusat->cek_stok_retur_pembelian($id_barang_pusat, $dari_stok, $dari_stok_lama, $jumlah_retur_lama);
		if($jumlah_retur > $data_cek_stok->total_stok){
			echo json_encode(array(
				'status' 			=> 3,
				'pesan'				=> "STOK ".$dari_stok." UNTUK : </br>
										SKU </br>
										<b class='text-dark'>".$data_cek_stok->sku."</b></br></br>

										Nama Barang </br>
										<b class='text-dark'>".$data_cek_stok->nama_barang."</b></br></br> 
										
										Saat ini hanya tersisa  : </br>
										<h2 class='text-danger'>".$data_cek_stok->total_stok."</h2>",
				'jumlah_retur_lama' => $jumlah_retur_lama
			));
			exit();
		}
	}
}
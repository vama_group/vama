<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Toko extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('toko_model','toko');
		$this->load->model('pegawai_toko_model','pegawai_toko');
	}

	public function index()
	{
		$data['atribut_halaman'] 	= 'Toko';
		$data['list_pegawai_toko'] 	= $this->pegawai_toko->listing();
		// $data['list_toko'] 			= $this->toko->listing();
		$data['halaman_list'] 		= $this->load->view('admin/master_data/toko/list',$data,true);
		$data['halaman_form'] 		= $this->load->view('admin/master_data/toko/form',$data,true);
		$data['halaman_plugin'] 	= $this->load->view('admin/master_data/toko/plugin',$data,true);
		$data['isi_halaman'] 		= $this->load->view('admin/master_data/toko/index',$data,true);
		$this->load->view('admin/layout',$data);

		// $this->load->helper('url');
		// $this->load->view('toko_view');
	}

	public function ajax_list()
	{
		$list = $this->toko->get_datatables();
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $toko) {
			$no++;
			$row = array();
			$row[] = $toko->kode_toko;
			$row[] = $toko->nama_toko;
			$row[] = $toko->nama_pegawai_toko;			
			$row[] = $toko->email;
			$row[] = $toko->pin_bb1;
			$row[] = $toko->telephone1;
			$row[] = $toko->handphone1;

			//add html for action
			$row[] = '
					<a class="btn btn-xs btn-success" href="javascript:void(0)" title="Edit" onclick="view_toko('."'".$toko->id_toko."'".')"><i class="fa fa-eye"></i>
					</a>
					<a class="btn btn-xs btn-primary" href="javascript:void(0)" title="Edit" onclick="edit_toko('."'".$toko->id_toko."'".')"><i class="glyphicon glyphicon-pencil"></i>
					</a>
				  	<a class="btn btn-xs btn-danger" href="javascript:void(0)" title="Hapus" onclick="delete_toko('."'".$toko->id_toko."'".')"><i class="glyphicon glyphicon-trash"></i>
				  	</a>
				  	';
		
			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->toko->count_all(),
						"recordsFiltered" => $this->toko->count_filtered(),
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}

	public function ajax_edit($id_toko)
	{
		$data = $this->toko->get_by_id($id_toko);
		// $data->dob = ($data->dob == '0000-00-00') ? '' : $data->dob; // if 0000-00-00 set tu empty for datepicker compatibility
		echo json_encode($data);
	}

	public function ajax_add()
	{
		$akhir				= $this->toko->akhir();

		// Buat kode baru
		$inisial			= "TK";
		if(count($akhir) > 0) {
			$id_toko	= $akhir->id_toko;
		}else{
			$id_toko	= 0;
		}

		
		if ($id_toko < 10){
			$depan 			= '00';
		}

		elseif($id_toko< 100) {
			$depan			= '0';
		}

		else{
			$depan			= '';
		}

		$jumlah				= $id_toko+1;
		$kode_toko			= $inisial.$depan.$jumlah;
		// Akhir buat kode baru

		$this->_validate();
		$data = array(
						'kode_toko' 			=> $kode_toko,
						'nama_toko' 			=> $this->input->post('nama_toko'),
						'kepala_toko' 			=> $this->input->post('kepala_toko'),
						'alamat_toko' 			=> $this->input->post('alamat_toko'),
						'pin_bb1' 				=> $this->input->post('pin_bb1'),
						'pin_bb2' 				=> $this->input->post('pin_bb2'),
						'telephone1' 			=> $this->input->post('telephone1'),
						'telephone2' 			=> $this->input->post('telephone2'),
						'handphone1' 			=> $this->input->post('handphone1'),
						'handphone2' 			=> $this->input->post('handphone2'),
						'fax'	 				=> $this->input->post('fax'),
						'email' 				=> $this->input->post('email'),
						'operator_pembuatan'	=> $this->session->userdata('username'),
						'tanggal_pembuatan'		=> date('Y-m-d H:i:s')
					);
		$insert = $this->toko->save($data);
		echo json_encode(array("status" => TRUE));
	}

	public function ajax_update()
	{
		$this->_validate();
		$data = array(
						'nama_toko' 			=> $this->input->post('nama_toko'),
						'kepala_toko' 			=> $this->input->post('kepala_toko'),
						'alamat_toko' 			=> $this->input->post('alamat_toko'),
						'pin_bb1' 				=> $this->input->post('pin_bb1'),
						'pin_bb2' 				=> $this->input->post('pin_bb2'),
						'telephone1' 			=> $this->input->post('telephone1'),
						'telephone2' 			=> $this->input->post('telephone2'),
						'handphone1' 			=> $this->input->post('handphone1'),
						'handphone2' 			=> $this->input->post('handphone2'),
						'fax'	 				=> $this->input->post('fax'),
						'email' 				=> $this->input->post('email'),
						'operator_pembaharuan'	=> $this->session->userdata('username')
					);
		$this->toko->update(array('id_toko' => $this->input->post('id_toko')), $data);
		echo json_encode(array("status" => TRUE));
	}

	public function ajax_delete($id_toko)
	{
		$this->toko->delete_by_id($id_toko);
		echo json_encode(array("status" => TRUE));
	}

	private function _validate()
	{
		$data = array();
		$data['error_string'] = array();
		$data['inputerror'] = array();
		$data['status'] = TRUE;

		if($this->input->post('nama_toko') == '')
		{
			$data['inputerror'][] = 'nama_toko';
			$data['error_string'][] = 'Nama toko wajib diisi';
			$data['status'] = FALSE;
		}

		if($this->input->post('alamat_toko') == '')
		{
			$data['inputerror'][] = 'alamat_toko';
			$data['error_string'][] = 'Alamat toko wajib diisi';
			$data['status'] = FALSE;
		}

		if($data['status'] === FALSE)
		{
			echo json_encode($data);
			exit();
		}
	}

}

<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Faktur extends MY_Controller {
	function __construct()
	{
		parent::__construct();
		$this->CI =& get_instance();
		$this->load->model('pegawai_pusat_model', 'pegawai_pusat');
		$this->load->model('useraccess_pusat_model', 'useraccess_pusat');
		// -------------------------------------------------------------------------------------------------------------------------
		$this->load->model('customer_pusat_model', 'customer_pusat');
		$this->load->model('barang_pusat_model', 'barang_pusat');
		$this->load->model('barang_toko_model', 'barang_toko');
		// -------------------------------------------------------------------------------------------------------------------------
		$this->load->model('penjualan_master_model', 'penjualan_master');
		$this->load->model('penjualan_detail_model', 'penjualan_detail');
		// -------------------------------------------------------------------------------------------------------------------------
		$this->load->model('retur_penjualan_master_model', 'retur_penjualan_master');
		$this->load->model('retur_penjualan_detail_model', 'retur_penjualan_detail');
		// -------------------------------------------------------------------------------------------------------------------------
		$this->load->model('retur_pembelian_master_model', 'retur_pembelian_master');
		$this->load->model('retur_pembelian_detail_model', 'retur_pembelian_detail');
		// -------------------------------------------------------------------------------------------------------------------------
		$this->load->model('hadiah_master_model', 'hadiah_master');
		$this->load->model('hadiah_detail_model', 'hadiah_detail');
		// -------------------------------------------------------------------------------------------------------------------------	
	}

	public function index() {
		// Cek user acces menu
		$id_pegawai               = $this->session->userdata('id_pegawai');
		$cek_useraccess_transaksi = $this->useraccess_pusat->cek_access($id_pegawai, '27');
		$cek_useraccess_laporan   = $this->useraccess_pusat->cek_access($id_pegawai, '39');
		if($cek_useraccess_laporan){
			if($cek_useraccess_laporan->act_read == '0' or $cek_useraccess_laporan->act_read == '-'){
				redirect(base_url().'login');
				exit();
			}else{
				redirect(base_url().'dashboard');
			}
		}else{
			redirect(base_url().'login');
		}
	}

	public function faktur_penjualan()
	{
		// Cek user acces menu
		$id_pegawai               = $this->session->userdata('id_pegawai');
		$cek_useraccess_transaksi = $this->useraccess_pusat->cek_access($id_pegawai, '27');
		if($cek_useraccess_transaksi->act_read == '0' or $cek_useraccess_transaksi->act_read == '-'){
			redirect(base_url().'penjualan');
			exit();
		}

		// Validasi id penjualan master
		$id_penjualan_m = $this->input->get('id_penjualan_m');
		if($id_penjualan_m == ''){
			redirect(base_url().'penjualan');
			exit();
		}		

		$mpdf = new \Mpdf\Mpdf([
			'mode'          => 'utf-8',
			'format'        => [140, 215],
			'orientation'   => 'L',
			'margin_left'   => 8,
			'margin_right'  => 8,
			'margin_top'    => 50,
			'margin_bottom' => 32,
			'margin_header' => 6,
			'margin_footer' => 4
		]);

		$data_master = $this->penjualan_master->get_master($id_penjualan_m);
		$data_detail = $this->penjualan_detail->get_detail_faktur($id_penjualan_m);

		// Validasi data
		if($data_master->id_penjualan_m == ''){
			redirect(base_url().'penjualan');
			exit();
		}

		$mpdf->SetHTMLHeader('
			<table width="100%" style="vertical-align: bottom; font-family: serif; color: #000000;">
				<tr><td style="text-align: left; font-size: 10pt; font-weight: bold;">FAKTUR PENJUALAN - VAMA GROUP</td></tr>
				<tr><td style="text-align: left; font-size: 8pt;">
					JL. RAYA BOGOR NO. 5, KM 31, CIMANGGIS - DEPOK
				</td></tr>
				<tr><td style="text-align: left; font-size: 8pt;">
					HP : 0812-1013-3100, BBM : D9AF91CA
				</td></tr>
			</table>
			<hr>
			<table width="100%" style="vertical-align: bottom; font-family: serif; color: #000000;">
				<tr>
					<td width="12%" style="text-align: left; font-size: 8pt; font-weight: bold;">No. Penjualan</td>
					<td width="3%" style="text-align: left; font-size: 8pt; font-weight: bold;">:</td>
					<td width="50%" style="text-align: left; font-size: 8pt; ">'.$data_master->no_penjualan.'</td>
					<td width="12%" style="text-align: left; font-size: 8pt; font-weight: bold;">Operator</td>
					<td width="3%" style="text-align: left; font-size: 8pt; font-weight: bold;">:</td>
					<td width="20%" style="text-align: left; font-size: 8pt;">'.$data_master->nama_pegawai.'</td>
				</tr>
				
				<tr>
					<td width="12%" style="text-align: left; font-size: 8pt; font-weight: bold;">Customer</td>
					<td width="3%" style="text-align: left; font-size: 8pt; font-weight: bold;">:</td>
					<td width="50%" style="text-align: left; font-size: 8pt;">
						'.$data_master->kode_customer_pusat.' - '.$data_master->nama_customer_pusat.'
					</td>
					<td width="12%" style="text-align: left; font-size: 8pt; font-weight: bold;">Tanggal</td>
					<td width="3%" style="text-align: left; font-size: 8pt; font-weight: bold;">:</td>
					<td width="20%" style="text-align: left; font-size: 8pt;">'.$data_master->tanggal_pembaharuan.'</td>					
				</tr>

				<tr>
					<td width="12%" style="text-align: left; font-size: 8pt; font-weight: bold;">Alamat</td>
					<td width="3%" style="text-align: left; font-size: 8pt; font-weight: bold;">:</td>
					<td width="50%" style="text-align: left; font-size: 8pt;">'.$data_master->alamat_customer_pusat.'</td>
					<td width="12%" style="text-align: left; font-size: 8pt; font-weight: bold;">Keterangan</td>
					<td width="3%" style="text-align: left; font-size: 8pt; font-weight: bold;">:</td>
					<td width="20%" style="text-align: left; font-size: 8pt;">'.$data_master->keterangan_lain.'</td>					
				</tr>
			</table>
		');

		$mpdf->SetHTMLFooter('
			<table width="100%" style="vertical-align: bottom; font-family: serif; color: #000000;">
				<tr>
					<td width="25%" style="text-align: center; font-size: 8pt;">Hormat Kami</td>
					<td width="5%" style="text-align: left; font-size: 8pt;"></td>
					<td width="25%" style="text-align: center; font-size: 8pt;">Penerima</td>
					<td width="5%" style="text-align: left; font-size: 8pt;">Total 1</td>
					<td width="2%" style="text-align: left; font-size: 8pt;">: Rp. </td>
					<td width="10%" style="text-align: right; font-size: 8pt;">
						'.number_format($data_master->total,'0',',','.').'
					</td>
				</tr>
				
				<tr>
					<td width="25%" style="text-align: center; font-size: 8pt;"></td>
					<td width="5%" style="text-align: left; font-size: 8pt;"></td>
					<td width="25%" style="text-align: left; font-size: 8pt;"></td>
					<td width="5%" style="text-align: left; font-size: 8pt;">PPN</td>
					<td width="2%" style="text-align: left; font-size: 8pt;">: Rp. </td>
					<td width="10%" style="text-align: right; font-size: 8pt;">
						'.number_format($data_master->ppn,'0',',','.').'
					</td>
				</tr>
				
				<tr>
					<td width="25%" style="text-align: center; font-size: 8pt;"></td>
					<td width="5%" style="text-align: left; font-size: 8pt;"></td>
					<td width="25%" style="text-align: left; font-size: 8pt;"></td>
					<td width="5%" style="text-align: left; font-size: 8pt;">Biaya Lain</td>
					<td width="2%" style="text-align: left; font-size: 8pt;">: Rp. </td>
					<td width="10%" style="text-align: right; font-size: 8pt;">
						'.number_format($data_master->biaya_lain,'0',',','.').'
					</td>
				</tr>

				<tr>
					<td width="25%" style="text-align: center; font-size: 8pt;">('.$data_master->nama_pegawai.')</td>
					<td width="5%" style="text-align: left; font-size: 8pt;"></td>
					<td width="25%" style="text-align: center; font-size: 8pt;">('.$data_master->nama_customer_pusat.')</td>
					<td width="5%" style="text-align: left; font-size: 11pt; font-weight: bold;">Total 2</td>
					<td width="2%" style="text-align: left; font-size:11pt; font-weight: bold;">: Rp. </td>
					<td width="10%" style="text-align: right; font-size: 11pt; font-weight: bold;">
						'.number_format($data_master->grand_total,'0',',','.').
					'</td>
				</tr>
				
			</table>

			<table width="100%" style="vertical-align: bottom; font-family: serif; color: #000000;">
				<tr>
					<td width="8%" style="text-align: left; font-size: 8pt; font-weight: bold;">CATATAN : </td>
					<td width="72%" style="text-align: left; font-size: 8pt;">
						Barang yang sudah di beli tidak dapat di kembalikan lagi (kecuali dengan perjanjian)
					</td>
					<td width="10%" style="text-align: right; font-size: 8pt; font-weight: bold;">
						Page : {PAGENO} dari {nbpg}
					</td>
				</tr>
			</table>
		');

		$data = array(
			"data_master" => $data_master,
			"data_detail" => $data_detail
		);

		$html = $this->load->view('admin/transaksi/penjualan/faktur_penjualan', $data, true);
		$mpdf->WriteHTML($html);
		$mpdf->Output();
	}

	public function faktur_penyiapan_barang()
	{
		// Cek user acces menu
		$id_pegawai 				= $this->session->userdata('id_pegawai');
		$cek_useraccess_transaksi 	= $this->useraccess_pusat->cek_access($id_pegawai, '27');
		if($cek_useraccess_transaksi->act_read == '0' or $cek_useraccess_transaksi->act_read == '-'){
			redirect(base_url().'penjualan');
			exit();
		}

		// Validasi id penjualan master
		$id_penjualan_m = $this->input->get('id_penjualan_m');
		if($id_penjualan_m == ''){
			redirect(base_url().'penjualan');
			exit();
		}		

		$mpdf = new \Mpdf\Mpdf([
			'mode'          => 'utf-8',
			'format'        => [140, 215],
			'orientation'   => 'L',
			'margin_left'   => 8,
			'margin_right'  => 8,
			'margin_top'    => 30,
			'margin_bottom' => 15,
			'margin_header' => 6,
			'margin_footer' => 4
		]);

		$data_master    = $this->penjualan_master->get_master($id_penjualan_m);
		$data_detail    = $this->penjualan_detail->get_detail_faktur($id_penjualan_m);

		// Validasi data
		if($data_master->id_penjualan_m == ''){
			redirect(base_url().'penjualan');
			exit();
		}

		$mpdf->SetHTMLHeader('
			<table width="100%" style="vertical-align: bottom; font-family: serif; color: #000000;">
				<tr><td style="text-align: left; font-size: 10pt; font-weight: bold;">FAKTUR PENYIAPAN BARANG - VAMA GROUP</td></tr>
			</table>
			<hr>
			<table width="100%" style="vertical-align: bottom; font-family: serif; color: #000000;">
				<tr>
					<td width="12%" style="text-align: left; font-size: 8pt; font-weight: bold;">No. Penjualan</td>
					<td width="3%" style="text-align: left; font-size: 8pt; font-weight: bold;">:</td>
					<td width="50%" style="text-align: left; font-size: 8pt; ">'.$data_master->no_penjualan.'</td>
					<td width="12%" style="text-align: left; font-size: 8pt; font-weight: bold;">Operator</td>
					<td width="3%" style="text-align: left; font-size: 8pt; font-weight: bold;">:</td>
					<td width="20%" style="text-align: left; font-size: 8pt;">'.$data_master->nama_pegawai.'</td>
				</tr>
				
				<tr>
					<td width="12%" style="text-align: left; font-size: 8pt; font-weight: bold;">Customer</td>
					<td width="3%" style="text-align: left; font-size: 8pt; font-weight: bold;">:</td>
					<td width="50%" style="text-align: left; font-size: 8pt;">
						'.$data_master->kode_customer_pusat.' - '.$data_master->nama_customer_pusat.'
					</td>
					<td width="12%" style="text-align: left; font-size: 8pt; font-weight: bold;">Tanggal</td>
					<td width="3%" style="text-align: left; font-size: 8pt; font-weight: bold;">:</td>
					<td width="20%" style="text-align: left; font-size: 8pt;">'.$data_master->tanggal_pembaharuan.'</td>					
				</tr>
			</table>
		');

		$mpdf->SetHTMLFooter('
			<table width="100%" style="vertical-align: bottom; font-family: serif; color: #000000;">
				<tr>
					<td width="8%" style="text-align: left; font-size: 8pt; font-weight: bold;">Isikan nama checker disini : </td>
					<td width="20%" style="text-align: right; font-size: 8pt; font-weight: bold;">
						Page : {PAGENO} dari {nbpg}
					</td>
				</tr>
			</table>
		');

		$data = array(
			"data_master" => $data_master,
			"data_detail" => $data_detail
		);

		$html = $this->load->view('admin/transaksi/penjualan/faktur_penyiapan_barang', $data, true);
		$mpdf->WriteHTML($html);
		$mpdf->Output();
	}

	public function faktur_retur_penjualan()
	{
		// Cek user acces menu
		$id_pegawai 				= $this->session->userdata('id_pegawai');
		$cek_useraccess_transaksi 	= $this->useraccess_pusat->cek_access($id_pegawai, '27');
		if($cek_useraccess_transaksi->act_read == '0' or $cek_useraccess_transaksi->act_read == '-'){
			redirect(base_url().'retur_penjualan');
			exit();
		}

		// Validasi id penjualan master
		$id_retur_penjualan_m = $this->input->get('id_retur_penjualan_m');
		if($id_retur_penjualan_m == ''){
			redirect(base_url().'retur_penjualan');
			exit();
		}		

		$mpdf = new \Mpdf\Mpdf([
			'mode'          => 'utf-8',
			'format'        => [140, 215],
			'orientation'   => 'L',
			'margin_left'   => 8,
			'margin_right'  => 8,
			'margin_top'    => 50,
			'margin_bottom' => 32,
			'margin_header' => 6,
			'margin_footer' => 4
		]);

		$data_master = $this->retur_penjualan_master->get_master($id_retur_penjualan_m);
		$data_detail = $this->retur_penjualan_detail->get_detail_faktur($id_retur_penjualan_m);

		// Validasi data
		if($data_master->id_retur_penjualan_m == ''){
			redirect(base_url().'retur_penjualan');
			exit();
		}

		$mpdf->SetHTMLHeader('
			<table width="100%" style="vertical-align: bottom; font-family: serif; color: #000000;">
				<tr><td style="text-align: left; font-size: 10pt; font-weight: bold;">FAKTUR RETUR PENJUALAN - VAMA GROUP</td></tr>
				<tr><td style="text-align: left; font-size: 8pt;">
					JL. RAYA BOGOR NO. 5, KM 31, CIMANGGIS - DEPOK
				</td></tr>
				<tr><td style="text-align: left; font-size: 8pt;">
					HP : 0812-1013-3100, BBM : D9AF91CA
				</td></tr>
			</table>
			<hr>
			<table width="100%" style="vertical-align: bottom; font-family: serif; color: #000000;">
				<tr>
					<td width="12%" style="text-align: left; font-size: 8pt; font-weight: bold;">No. Retur</td>
					<td width="3%" style="text-align: left; font-size: 8pt; font-weight: bold;">:</td>
					<td width="50%" style="text-align: left; font-size: 8pt; ">'.$data_master->no_retur_penjualan.'</td>
					<td width="12%" style="text-align: left; font-size: 8pt; font-weight: bold;">Operator</td>
					<td width="3%" style="text-align: left; font-size: 8pt; font-weight: bold;">:</td>
					<td width="20%" style="text-align: left; font-size: 8pt;">'.$data_master->nama_pegawai.'</td>
				</tr>
				
				<tr>
					<td width="12%" style="text-align: left; font-size: 8pt; font-weight: bold;">No. Penjualan</td>
					<td width="3%" style="text-align: left; font-size: 8pt; font-weight: bold;">:</td>
					<td width="50%" style="text-align: left; font-size: 8pt;">
						'.$data_master->no_penjualan.'
					</td>
					<td width="12%" style="text-align: left; font-size: 8pt; font-weight: bold;">Tanggal</td>
					<td width="3%" style="text-align: left; font-size: 8pt; font-weight: bold;">:</td>
					<td width="20%" style="text-align: left; font-size: 8pt;">'.$data_master->tanggal_retur.'</td>					
				</tr>

				<tr>
					<td width="12%" style="text-align: left; font-size: 8pt; font-weight: bold;">Customer</td>
					<td width="3%" style="text-align: left; font-size: 8pt; font-weight: bold;">:</td>
					<td width="50%" style="text-align: left; font-size: 8pt;">
						'.$data_master->kode_customer_pusat.'  -  '.$data_master->nama_customer_pusat.'
					</td>
					<td width="12%" style="text-align: left; font-size: 8pt; font-weight: bold;">Keterangan</td>
					<td width="3%" style="text-align: left; font-size: 8pt; font-weight: bold;">:</td>
					<td width="20%" style="text-align: left; font-size: 8pt;">'.$data_master->keterangan_lain.'</td>					
				</tr>
			</table>
		');

		$mpdf->SetHTMLFooter('
			<table width="100%" style="vertical-align: bottom; font-family: serif; color: #000000;">
				<tr>
					<td width="25%" style="text-align: center; font-size: 8pt;">Hormat Kami</td>
					<td width="5%" style="text-align: left; font-size: 8pt;"></td>
					<td width="25%" style="text-align: center; font-size: 8pt;">Penerima</td>
					<td width="12%" style="text-align: left; font-size: 8pt;">Total Potongan</td>
					<td width="5%" style="text-align: left; font-size: 8pt;">: Rp. </td>
					<td width="18%" style="text-align: right; font-size: 8pt;">
						'.number_format($data_master->total_potongan,'0',',','.').'
					</td>
				</tr>
				
				<tr>
					<td width="25%" style="text-align: center; font-size: 8pt;"></td>
					<td width="5%" style="text-align: left; font-size: 8pt;"></td>
					<td width="25%" style="text-align: left; font-size: 8pt;"></td>
					<td width="12%" style="text-align: left; font-size: 8pt;">Total Saldo</td>
					<td width="5%" style="text-align: left; font-size: 8pt;">: Rp. </td>
					<td width="18%" style="text-align: right; font-size: 8pt;">
						'.number_format($data_master->total_saldo,'0',',','.').'
					</td>
				</tr>
				
				<tr>
					<td width="25%" style="text-align: center; font-size: 8pt;"></td>
					<td width="5%" style="text-align: left; font-size: 8pt;"></td>
					<td width="25%" style="text-align: left; font-size: 8pt;"></td>
					<td width="12%" style="text-align: left; font-size: 11pt; font-weight: bold;">Total Retur</td>
					<td width="5%" style="text-align: left; font-size: 11pt; font-weight: bold;">: Rp. </td>
					<td width="18%" style="text-align: right; font-size: 11pt; font-weight: bold;">
						'.number_format($data_master->total_retur,'0',',','.').'
					</td>
				</tr>

				<tr>
					<td width="25%" style="text-align: center; font-size: 8pt;">('.$data_master->nama_pegawai.')</td>
					<td width="5%" style="text-align: left; font-size: 8pt;"></td>
					<td width="25%" style="text-align: center; font-size: 8pt;">('.$data_master->nama_customer_pusat.')</td>
					<td width="12%" style="text-align: left; font-size: 11pt; font-weight: bold;"></td>
					<td width="5%" style="text-align: left; font-size:11pt; font-weight: bold;"></td>
					<td width="18%" style="text-align: right; font-size: 11pt; font-weight: bold;"></td>
				</tr>
				
			</table>

			<table width="100%" style="vertical-align: bottom; font-family: serif; color: #000000;">
				<tr>
					<td width="8%" style="text-align: left; font-size: 8pt; font-weight: bold;"></td>
					<td width="72%" style="text-align: left; font-size: 8pt;"></td>
					<td width="25%" style="text-align: right; font-size: 8pt; font-weight: bold;">
						Page : {PAGENO} dari {nbpg}
					</td>
				</tr>
			</table>
		');

		$data = array(
			"data_master" => $data_master,
			"data_detail" => $data_detail
		);

		$html = $this->load->view('admin/transaksi/retur_penjualan/faktur_retur_penjualan', $data, true);
		$mpdf->WriteHTML($html);
		$mpdf->Output();
	}

	public function faktur_retur_pembelian()
	{
		// Cek user acces menu
		$id_pegawai 				= $this->session->userdata('id_pegawai');
		$cek_useraccess_transaksi 	= $this->useraccess_pusat->cek_access($id_pegawai, '27');
		if($cek_useraccess_transaksi->act_read == '0' or $cek_useraccess_transaksi->act_read == '-'){
			redirect(base_url().'retur_pembelian');
			exit();
		}

		// Validasi id penjualan master
		$id_retur_pembelian_m = $this->input->get('id_retur_pembelian_m');
		if($id_retur_pembelian_m == ''){
			redirect(base_url().'retur_pembelian');
			exit();
		}		

		$mpdf = new \Mpdf\Mpdf([
			'mode'          => 'utf-8',
			'format'        => [140, 215],
			'orientation'   => 'L',
			'margin_left'   => 8,
			'margin_right'  => 8,
			'margin_top'    => 45,
			'margin_bottom' => 32,
			'margin_header' => 6,
			'margin_footer' => 4
		]);

		$data_master = $this->retur_pembelian_master->get_master2($id_retur_pembelian_m);
		$data_detail = $this->retur_pembelian_detail->get_detail_faktur($id_retur_pembelian_m);

		// Validasi data
		if($data_master->id_retur_pembelian_m == ''){
			redirect(base_url().'retur_pembelian');
			exit();
		}

		$mpdf->SetHTMLHeader('
			<table width="100%" style="vertical-align: bottom; font-family: serif; color: #000000;">
				<tr><td style="text-align: left; font-size: 10pt; font-weight: bold;">FAKTUR RETUR PEMBELIAN - VAMA GROUP</td></tr>
				<tr><td style="text-align: left; font-size: 8pt;">
					JL. RAYA BOGOR NO. 5, KM 31, CIMANGGIS - DEPOK
				</td></tr>
				<tr><td style="text-align: left; font-size: 8pt;">
					HP : 0812-1013-3100, BBM : D9AF91CA
				</td></tr>
			</table>
			<hr>
			<table width="100%" style="vertical-align: bottom; font-family: serif; color: #000000;">
				<tr>
					<td width="12%" style="text-align: left; font-size: 8pt; font-weight: bold;">No. Retur</td>
					<td width="3%" style="text-align: left; font-size: 8pt; font-weight: bold;">:</td>
					<td width="50%" style="text-align: left; font-size: 8pt; ">'.$data_master->no_retur_pembelian.'</td>
					<td width="12%" style="text-align: left; font-size: 8pt; font-weight: bold;">Operator</td>
					<td width="3%" style="text-align: left; font-size: 8pt; font-weight: bold;">:</td>
					<td width="20%" style="text-align: left; font-size: 8pt;">'.$data_master->nama_pegawai.'</td>
				</tr>
				
				<tr>
					<td width="12%" style="text-align: left; font-size: 8pt; font-weight: bold;">No. Pembelian</td>
					<td width="3%" style="text-align: left; font-size: 8pt; font-weight: bold;">:</td>
					<td width="50%" style="text-align: left; font-size: 8pt;">
						'.$data_master->no_pembelian.'
					</td>
					<td width="12%" style="text-align: left; font-size: 8pt; font-weight: bold;">Tanggal</td>
					<td width="3%" style="text-align: left; font-size: 8pt; font-weight: bold;">:</td>
					<td width="20%" style="text-align: left; font-size: 8pt;">'.$data_master->tanggal_retur.'</td>					
				</tr>

				<tr>
					<td width="12%" style="text-align: left; font-size: 8pt; font-weight: bold;">Supplier</td>
					<td width="3%" style="text-align: left; font-size: 8pt; font-weight: bold;">:</td>
					<td width="50%" style="text-align: left; font-size: 8pt;">
						'.$data_master->kode_supplier.'  -  '.$data_master->nama_supplier.'
					</td>
					<td width="12%" style="text-align: left; font-size: 8pt; font-weight: bold;">Keterangan</td>
					<td width="3%" style="text-align: left; font-size: 8pt; font-weight: bold;">:</td>
					<td width="20%" style="text-align: left; font-size: 8pt;">'.$data_master->keterangan_lain.'</td>					
				</tr>
			</table>
		');

		$mpdf->SetHTMLFooter('
			<table width="100%" style="vertical-align: bottom; font-family: serif; color: #000000;">
				<tr>
					<td width="8%" style="text-align: left; font-size: 8pt; font-weight: bold;"></td>
					<td width="72%" style="text-align: left; font-size: 8pt;"></td>
					<td width="20%" style="text-align: right; font-size: 8pt; font-weight: bold;">
						Halaman : {PAGENO} dari {nbpg}
					</td>
				</tr>
			</table>
		');

		$data = array(
			"data_master" => $data_master,
			"data_detail" => $data_detail
		);

		$html = $this->load->view('admin/transaksi/retur_pembelian/faktur_retur_pembelian', $data, true);
		$mpdf->WriteHTML($html);
		$mpdf->Output();
	}

	public function faktur_hadiah()
	{
		// Cek user acces menu
		$id_pegawai 				= $this->session->userdata('id_pegawai');
		$cek_useraccess_transaksi 	= $this->useraccess_pusat->cek_access($id_pegawai, '27');
		if($cek_useraccess_transaksi->act_read == '0' or $cek_useraccess_transaksi->act_read == '-'){
			redirect(base_url().'hadiah');
			exit();
		}

		// Validasi id penjualan master
		$id_hadiah_m = $this->input->get('id_hadiah_m');
		if($id_hadiah_m == ''){
			redirect(base_url().'hadiah');
			exit();
		}		

		$mpdf = new \Mpdf\Mpdf([
			'mode'          => 'utf-8',
			'format'        => [140, 215],
			'orientation'   => 'L',
			'margin_left'   => 8,
			'margin_right'  => 8,
			'margin_top'    => 45,
			'margin_bottom' => 30,
			'margin_header' => 6,
			'margin_footer' => 4
		]);

		$data_master = $this->hadiah_master->get_master($id_hadiah_m);
		$data_detail = $this->hadiah_detail->get_detail_faktur($id_hadiah_m);

		// Validasi data
		if($data_master->id_hadiah_m == ''){
			redirect(base_url().'hadiah');
			exit();
		}

		$mpdf->SetHTMLHeader('
			<table width="100%" style="vertical-align: bottom; font-family: serif; color: #000000;">
				<tr><td style="text-align: left; font-size: 10pt; font-weight: bold;">FAKTUR HADIAH - VAMA GROUP</td></tr>
				<tr><td style="text-align: left; font-size: 8pt;">
					JL. RAYA BOGOR NO. 5, KM 31, CIMANGGIS - DEPOK
				</td></tr>
				<tr><td style="text-align: left; font-size: 8pt;">
					HP : 0812-1013-3100, BBM : D9AF91CA
				</td></tr>
			</table>
			<hr>
			<table width="100%" style="vertical-align: bottom; font-family: serif; color: #000000;">
				<tr>
					<td width="12%" style="text-align: left; font-size: 8pt; font-weight: bold;">No. Hadiah</td>
					<td width="3%" style="text-align: left; font-size: 8pt; font-weight: bold;">:</td>
					<td width="50%" style="text-align: left; font-size: 8pt; ">'.$data_master->no_hadiah.'</td>
					<td width="12%" style="text-align: left; font-size: 8pt; font-weight: bold;">Operator</td>
					<td width="3%" style="text-align: left; font-size: 8pt; font-weight: bold;">:</td>
					<td width="20%" style="text-align: left; font-size: 8pt;">'.$data_master->nama_pegawai.'</td>
				</tr>
				
				<tr>
					<td width="12%" style="text-align: left; font-size: 8pt; font-weight: bold;">Nama Hadiah</td>
					<td width="3%" style="text-align: left; font-size: 8pt; font-weight: bold;">:</td>
					<td width="50%" style="text-align: left; font-size: 8pt;">
						'.$data_master->nama_hadiah.'
					</td>
					<td width="12%" style="text-align: left; font-size: 8pt; font-weight: bold;">Tanggal</td>
					<td width="3%" style="text-align: left; font-size: 8pt; font-weight: bold;">:</td>
					<td width="20%" style="text-align: left; font-size: 8pt;">'.$data_master->tanggal.'</td>					
				</tr>

				<tr>
					<td width="12%" style="text-align: left; font-size: 8pt; font-weight: bold;">Customer</td>
					<td width="3%" style="text-align: left; font-size: 8pt; font-weight: bold;">:</td>
					<td width="50%" style="text-align: left; font-size: 8pt;">
						'.$data_master->kode_customer_pusat.'  -  '.$data_master->nama_customer_pusat.'
					</td>
					<td width="12%" style="text-align: left; font-size: 8pt; font-weight: bold;">Keterangan</td>
					<td width="3%" style="text-align: left; font-size: 8pt; font-weight: bold;">:</td>
					<td width="20%" style="text-align: left; font-size: 8pt;">'.$data_master->keterangan_lain.'</td>					
				</tr>
			</table>
		');

		$mpdf->SetHTMLFooter('
			<table width="100%" style="vertical-align: bottom; font-family: serif; color: #000000;">
				<tr>
					<td width="25%" style="text-align: center; font-size: 8pt;">Hormat Kami</td>
					<td width="5%" style="text-align: left; font-size: 8pt;"></td>
					<td width="25%" style="text-align: center; font-size: 8pt;">Penerima</td>
					<td width="12%" style="text-align: left; font-size: 11pt; font-weight: bold;"></td>
					<td width="5%" style="text-align: left; font-size: 11pt; font-weight: bold;"></td>
					<td width="18%" style="text-align: right; font-size: 11pt; font-weight: bold;"></td>
				</tr>
				
				<tr>
					<td width="25%" style="text-align: center; font-size: 8pt;"></td>
					<td width="5%" style="text-align: left; font-size: 8pt;"></td>
					<td width="25%" style="text-align: left; font-size: 8pt;"></td>
					<td width="12%" style="text-align: left; font-size: 11pt; font-weight: bold;">Total</td>
					<td width="5%" style="text-align: left; font-size: 11pt; font-weight: bold;">: Rp. </td>
					<td width="18%" style="text-align: right; font-size: 11pt; font-weight: bold;">
						'.number_format($data_master->total,'0',',','.').'
					</td>
				</tr>
				
				<tr>
					<td width="25%" style="text-align: center; font-size: 8pt;"></td>
					<td width="5%" style="text-align: left; font-size: 8pt;"></td>
					<td width="25%" style="text-align: left; font-size: 8pt;"></td>
					<td width="12%" style="text-align: left; font-size: 11pt; font-weight: bold;"></td>
					<td width="5%" style="text-align: left; font-size: 11pt; font-weight: bold;"></td>
					<td width="18%" style="text-align: right; font-size: 11pt; font-weight: bold;"></td>
				</tr>

				<tr>
					<td width="25%" style="text-align: center; font-size: 8pt;">('.$data_master->nama_pegawai.')</td>
					<td width="5%" style="text-align: left; font-size: 8pt;"></td>
					<td width="25%" style="text-align: center; font-size: 8pt;">('.$data_master->nama_customer_pusat.')</td>
					<td width="12%" style="text-align: left; font-size: 11pt; font-weight: bold;"></td>
					<td width="5%" style="text-align: left; font-size:11pt; font-weight: bold;"></td>
					<td width="18%" style="text-align: right; font-size: 11pt; font-weight: bold;"></td>
				</tr>
				
			</table>

			<table width="100%" style="vertical-align: bottom; font-family: serif; color: #000000;">
				<tr>
					<td width="8%" style="text-align: left; font-size: 8pt; font-weight: bold;"></td>
					<td width="72%" style="text-align: left; font-size: 8pt;"></td>
					<td width="20%" style="text-align: right; font-size: 8pt; font-weight: bold;">
						Halaman : {PAGENO} dari {nbpg}
					</td>
				</tr>
			</table>
		');

		$data = array(
			"data_master" => $data_master,
			"data_detail" => $data_detail
		);

		$html = $this->load->view('admin/transaksi/hadiah/faktur_hadiah', $data, true);
		$mpdf->WriteHTML($html);
		$mpdf->Output();
	}
}

<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends MY_Controller {
	
	public function __construct() {
        parent::__construct();
        $this->CI =& get_instance();
        $this->load->model('pegawai_pusat_model','pegawai_pusat');
        $this->load->model('useraccess_pusat_model','useraccess_pusat');
        $this->load->model('customer_pusat_model', 'customer_pusat');
        $this->load->model('penjualan_master_model', 'penjualan_master');
        $this->load->model('retur_penjualan_master_model', 'retur_penjualan_master');
        $this->load->model('pembelian_master_model', 'pembelian_master');
        $this->load->model('retur_pembelian_master_model', 'retur_pembelian_master');
        $this->load->model('stok_opname_pusat_model', 'stok_opname_pusat');
        $this->load->model('perakitan_master_model', 'perakitan_master');
        $this->load->model('hadiah_master_model', 'hadiah_master');
        $this->load->model('retur_barang_dari_toko_master_model', 'retur_barang_dari_toko_master');
        $this->load->helper('tgl_indo');
    }

	function mac(){
        ob_start();
        system('ipconfig/all');
        $mycom = ob_get_contents();
        ob_clean();

        $findme = "Physical";
        $pmac   = strpos($mycom, $findme);
        $mac    = substr($mycom, ($pmac+36),17);
        return $mac;
	}
	
	public function index()
	{
		$mac = $this->mac();
		// Cek user acces menu
		// $id_pegawai = $this->session->userdata('id_pegawai_'.$mac.'');
		$id_pegawai = $this->session->userdata('id_pegawai');
		if($id_pegawai == ''){
			redirect(base_url().'login');
			exit();
		}

		$data_pegawai   = $this->pegawai_pusat->get_by_id($id_pegawai);
		if($data_pegawai){
			$data_pegawai              = $this->pegawai_pusat->get_by_id($id_pegawai);		
			$data['data_pegawai']      = $data_pegawai;
			$data['data_induk_menu']   = $this->useraccess_pusat->get_induk_menu($id_pegawai);		
			$data['atribut_halaman']   = 'Dashboard Pusat';
			$data['halaman_dashboard'] = $this->load->view('admin/dashboard/dashboard_pusat/dashboard_view', $data, true);
			$data['halaman_plugin']    = $this->load->view('admin/dashboard/dashboard_pusat/plugin', $data, true);
			$data['isi_halaman']       = $this->load->view('admin/dashboard/dashboard_pusat/index', $data, true);
			$this->load->view('admin/layout',$data);
		}else{
			redirect(base_url().'login');
			exit();
		}
	}

	// Awal daftar transaksi
	public function ambil_penjualan_by_perintah(){
		$tanggal_awal  = $this->input->post('tanggal_awal');
		$tanggal_akhir = $this->input->post('tanggal_akhir');
		if($tanggal_awal == '' AND $tanggal_akhir == ''){
			redirect(base_url().'dashboard');
			exit();
		}
		
		$jenis_perintah   = $this->input->post('jenis_perintah');
		$status_penjualan = $this->input->post('status_penjualan');
		if($jenis_perintah == '' AND $status_penjualan == ''){
			redirect(base_url().'dashboard');
			exit();
		}
		
		$data_penjualan_perbulan = $this->penjualan_master->get_penjualan_by_perintah(
			$jenis_perintah, $status_penjualan, $tanggal_awal, $tanggal_akhir
		);

		if($data_penjualan_perbulan->num_rows() > 0){
			$hasil['status']  = 1;			
			$hasil['datanya'] = "<div>";

			$no = 0;
			foreach($data_penjualan_perbulan->result() as $d){
				$no++;
				if($d->foto!=""){
					$hasil['datanya']  .= "
		                <a onclick='edit_penjualan(".$d->id_penjualan_m.")' class='list_data'>
		                	<div class='inbox-item'>
			                	<div class='inbox-item-img'>
			                        <img src='".base_url()."assets/upload/image/customer_pusat/thumbs/$d->foto' class='img-circle' alt=''>
			                	</div>";
		        }else{
					$nama_customer                = $d->nama_customer_pusat;
					$inisial                      = substr($nama_customer, 0, 1);
					if($d->status_penjualan       == 'MENUNGGU'){
						$link                     = 'edit_penjualan('.$d->id_penjualan_m.')';
					}else if($d->status_penjualan == 'DISIAPKAN'){
						$link                     = 'selesai_transaksi("'.$d->id_penjualan_m.'", 
																	   "'.$d->no_penjualan.'", "'.$d->nama_customer_pusat.'")';
					}else{
						$link 					  = '';
					}

					$hasil['datanya']  .= "
		                <a onclick='".$link."' class='list_data'>
		                	<div class='inbox-item'>
			                	<div class='inbox-item-img'>
			                        <img src='".base_url()."assets/upload/image/avatar/avatar.jpg' class='img-circle' alt=''>
			                	</div>";
				}

				$hasil['datanya'] .= "
							<p id='nama_customer' class='inbox-item-author'><b>".$no.". ".$d->nama_customer_pusat." | ".$d->no_penjualan."</b></p>
							<p id='skunya' class='inbox-item-text text-dark'>Rp. ".$d->grand_total."</p>
							<p id='total_stok' class='inbox-item-date'>".$d->tanggal."</p>
						</div>
					</a>
				";
			}

			$hasil['datanya'] 	.= "</div>";

		}else{
			$hasil['status'] 		= 0;
			$hasil['datanya']		= "<p class='inbox-item text-dark'>
										<i class='fa fa-check'></i>
									   	Tidak ada penjualan pusat yang tertahan
									   </p>";
		}

		echo json_encode($hasil);
	}

	public function ambil_data_retur(){
		$tanggal_awal  = $this->input->post('tanggal_awal');
		$tanggal_akhir = $this->input->post('tanggal_akhir');
		if($tanggal_awal == '' AND $tanggal_akhir == ''){
			redirect(base_url().'dashboard');
			exit();
		}
		
		// if($this->session->userdata('usergroup_name') == 'Super Admin' OR $this->session->userdata('usergroup_name') == 'Admin'){
			$daftar_toko      = $this->customer_pusat->listing_toko();
			$no               = 0;
			$hasil['datanya'] = "<div class='user-list'>";
			if(count($daftar_toko) > 0){
	        	foreach ($daftar_toko as $daftar_toko){ 
					$kode_toko  = $daftar_toko->kode_customer_pusat;
					$nama_toko  = $daftar_toko->nama_customer_pusat;
					$data_retur = $this->retur_barang_dari_toko_master->get_retur_tertahan($tanggal_awal, $tanggal_akhir, $kode_toko);
					if($data_retur->num_rows() > 0){
						// $hasil['status']  = 1;
						// if($no == '0'){}			
						foreach($data_retur->result() as $d){
							$no++;
							$nama_toko = $d->nama_toko;
							$inisial   = substr($nama_toko, 0, 1);
							$link = 'edit_retur(
								"'.$d->id_retur_pembelian_m.'", 
								"'.$kode_toko.'"
							)';
							$hasil['datanya'] .= "
				                <a onclick='".$link."' class='list_data'>
				                	<div class='inbox-item'>
					                	<div class='inbox-item-img'>
					                        <img src='".base_url()."assets/upload/image/avatar/avatar.jpg' class='img-circle' alt=''>
					                	</div>";

							$hasil['datanya'] .= "
										<p id='nama_customer' class='inbox-item-author'>
											<b>".$no.". ".$d->nama_toko." | ".$d->no_retur_pembelian."</b>
										</p>
										<p id='jumlah_data' class='inbox-item-text text-dark'>
											<b>
												<a class='text-primary'>JML RR : ".$d->jml_barang_retur."</a> |
												<a class='text-success'>JML MS : ".$d->jml_barang_masuk."</a> |
												<a class='text-danger'>JML MU : ".$d->jml_barang_menunggu."</a>
											</b>
										</p>
										<p id='total_stok' class='inbox-item-text'>
											Tanggal Retur : ".$d->tanggal_retur."
										</p>
									</div>
								</a>
							";
						}
						$hasil['datanya'] .= "</div>";
					}else{
						// $hasil['status']  = 0;
						$hasil['datanya'] .= "<p class='inbox-item text-dark'>
											 <i class ='fa fa-check'></i>
											 Tidak ada data retur barang dari toko : ".$nama_toko."
											 </p>";
					}
				}
			}else{
				// $hasil['status']  = 0;
				$hasil['datanya'] = "<p class='inbox-item text-dark'>
									 <i class ='fa fa-check'></i>
									 Tidak ada data toko
									 </p>";
			}
			echo json_encode(array(
				'data_retur' => $hasil,
				'query'      => $this->db->last_query()
			));
		// }
	}

	public function ambil_top_customer(){
		$tanggal_awal      = $this->input->post('tanggal_awal');
		$tanggal_akhir     = $this->input->post('tanggal_akhir');
		if($tanggal_awal == '' AND $tanggal_akhir == ''){
			redirect(base_url().'dashboard');
			exit();
		}
		
		if($this->session->userdata('usergroup_name') == 'Super Admin' OR $this->session->userdata('usergroup_name') == 'Admin'){
			$data_top_customer = $this->penjualan_master->get_top_customer($tanggal_awal, $tanggal_akhir);
			if($data_top_customer->num_rows() > 0){
				$hasil['status']  = 1;			
				$hasil['datanya'] = "<div class='user-list'>";
				$no               = 0;
				foreach($data_top_customer->result() as $d){
					$no++;
					
					if($d->foto!=""){
						$hasil['datanya']  .= "
			                <a class='list_data'>
			                	<div class='inbox-item'>
				                	<div class='inbox-item-img'>
				                        <img src='".base_url()."assets/upload/image/customer_pusat/thumbs/$d->foto' class='img-circle' alt=''>
				                	</div>";
			        }else{
						$nama_customer 	   = $d->nama_customer_pusat;
						$inisial 		   = substr($nama_customer, 0, 1);
						$hasil['datanya']  .= "
			                <a class='list_data'>
			                	<div class='inbox-item'>
				                	<div class='inbox-item-img'>
				                        <img src='".base_url()."assets/upload/image/avatar/avatar.jpg' class='img-circle' alt=''>
				                	</div>";
					}

					$hasil['datanya'] .= "
								<p id='nama_customer' class='inbox-item-author'><b>".$no.". ".$d->nama_customer_pusat."</b></p>
								<p id='skunya' class='inbox-item-text text-dark'>
									Rp. ".number_format($d->total,'0',',','.')."
									- dari : ".$d->jml_transaksi." Transaksi
								</p>
								<p id='total_stok' class='inbox-item-text'>
									Periode :
									".$d->tanggal_awal." sampai ".$d->tanggal_akhir."
								</p>
							</div>
						</a>
					";
				}
				$hasil['datanya'] 	.= "</div>";
			}else{
				$hasil['status']  = 0;
				$hasil['datanya'] = "<p class='inbox-item text-dark'>
									 <i class          ='fa fa-check'></i>
									 Tidak ada top customer
									 </p>";
			}
			echo json_encode($hasil);
		}
	}

	public function ambil_top_produk(){
		$tanggal_awal    = $this->input->post('tanggal_awal');
		$tanggal_akhir   = $this->input->post('tanggal_akhir');
		if($tanggal_awal == '' AND $tanggal_akhir == ''){
			redirect(base_url().'dashboard');
			exit();
		}
		
		if($this->session->userdata('usergroup_name') == 'Super Admin' OR $this->session->userdata('usergroup_name') == 'Admin'){
			$data_top_produk = $this->penjualan_master->get_top_produk($tanggal_awal, $tanggal_akhir);
			if($data_top_produk->num_rows() > 0){
				$hasil['status']  = 1;			
				$hasil['datanya'] = "<div class='user-list'>";
				$no               = 0;
				foreach($data_top_produk->result() as $d){
					$no++;
					
					if($d->foto!=""){
						$hasil['datanya']  .= "
			                <a class='list_data'>
			                	<div class='inbox-item'>
				                	<div class='inbox-item-img'>
				                        <img src='".base_url()."assets/upload/image/barang/thumbs/$d->foto' class='img-circle' alt=''>
				                	</div>";
			        }else{
						$nama_barang 	   = $d->nama_barang;
						$inisial 		   = substr($nama_barang, 0, 1);
						$hasil['datanya']  .= "
			                <a class='list_data'>
			                	<div class='inbox-item'>
				                	<div class='inbox-item-img'>
				                        <img src='".base_url()."assets/upload/image/avatar/default-barang.jpg' class='img-circle' alt=''>
				                	</div>";
					}

					$hasil['datanya'] .= "
								<p id='nama_barang' class='inbox-item-author'><b>".$no.". ".$d->sku." - ".$d->nama_barang."</b></p>
								<p id='jumlah' class='inbox-item-text text-dark'>
									Rp. ".number_format($d->total,'0',',','.')." - Dari : ".$d->jml_transaksi." Transaksi, QTY : ".$d->jml_qty."
								</p>
								<p id='total_stok' class='inbox-item-text'>
									Periode : ".$d->tanggal_awal." sampai ".$d->tanggal_akhir."
								</p>
							</div>
						</a>
					";
				}
				$hasil['datanya'] 	.= "</div>";
			}else{
				$hasil['status']  = 0;
				$hasil['datanya'] = "<p class='inbox-item text-dark'>
									 <i class='fa fa-check'></i>
									 Tidak ada top produk
									 </p>";
			}
			echo json_encode($hasil);
		}
	}
	// Akhir daftar transaksi

	// Awal total transaksi
	public function ambil_total(){
		$tanggal_awal    = $this->input->post('tanggal_awal');
		$tanggal_akhir   = $this->input->post('tanggal_akhir');
		if($tanggal_awal == '' AND $tanggal_akhir == ''){
			redirect(base_url().'dashboard');
			exit();
		}
		
		if($this->session->userdata('usergroup_name') == 'Super Admin' OR $this->session->userdata('usergroup_name') == 'Admin'){
			// Penjualan dan retur umum
			$data_penjualan_selesai_umum   = $this->penjualan_master->get_total_penjualan_perbulan($tanggal_awal, $tanggal_akhir, '!=', 'SELESAI');
			$data_penjualan_menunggu_umum  = $this->penjualan_master->get_total_penjualan_perbulan($tanggal_awal, $tanggal_akhir, '!=', 'MENUNGGU');
			$data_penjualan_disiapkan_umum = $this->penjualan_master->get_total_penjualan_perbulan($tanggal_awal, $tanggal_akhir, '!=', 'DISIAPKAN');
			$data_retur_penjualan_umum     = $this->retur_penjualan_master->get_total_perbulan($tanggal_awal, $tanggal_akhir, '!=');
			
			// Pennjualan dan retur mrc
			$data_penjualan_selesai_mrc    = $this->penjualan_master->get_total_penjualan_perbulan($tanggal_awal, $tanggal_akhir, '=', 'SELESAI');
			$data_penjualan_menunggu_mrc   = $this->penjualan_master->get_total_penjualan_perbulan($tanggal_awal, $tanggal_akhir, '=', 'MENUNGGU');
			$data_penjualan_disiapkan_mrc  = $this->penjualan_master->get_total_penjualan_perbulan($tanggal_awal, $tanggal_akhir, '=', 'DISIAPKAN');
			$data_penjualan_dikirim_mrc    = $this->penjualan_master->get_total_penjualan_perbulan($tanggal_awal, $tanggal_akhir, '=', 'DIKIRIM');
			$data_retur_penjualan_mrc      = $this->retur_penjualan_master->get_total_perbulan($tanggal_awal, $tanggal_akhir, '=');
			
			$data_pembelian_selesai        = $this->pembelian_master->get_total_selesai_perbulan($tanggal_awal, $tanggal_akhir);
			$data_pembelian_menunggu       = $this->pembelian_master->get_total_menunggu_perbulan($tanggal_awal, $tanggal_akhir);
			$data_retur_pembelian          = $this->retur_pembelian_master->get_total_perbulan($tanggal_awal, $tanggal_akhir);
			$data_stok_opname              = $this->stok_opname_pusat->get_total_opname_perbulan($tanggal_awal, $tanggal_akhir);
			
			$data_perakitan_selesai        = $this->perakitan_master->get_total_perakitan_selesai($tanggal_awal, $tanggal_akhir);
			$data_komponen_selesai         = $this->perakitan_master->get_total_komponen_selesai($tanggal_awal, $tanggal_akhir);
			$data_perakitan_menunggu       = $this->perakitan_master->get_total_perakitan_menunggu($tanggal_awal, $tanggal_akhir);
			$data_komponen_menunggu        = $this->perakitan_master->get_total_komponen_menunggu($tanggal_awal, $tanggal_akhir);
			$data_hadiah_selesai           = $this->hadiah_master->get_total_selesai_perbulan($tanggal_awal, $tanggal_akhir);
			$data_hadiah_menunggu          = $this->hadiah_master->get_total_menunggu_perbulan($tanggal_awal, $tanggal_akhir);

			echo json_encode(array(
				// Baris 1                        = Penjualan Umum dan Retur Penjualan Umum
				"total_penjualan_selesai_umum"    => $data_penjualan_selesai_umum->grand_total,
				"jumlah_penjualan_selesai_umum"   => $data_penjualan_selesai_umum->jml_transaksi,
				"tl_penjualan_selesai_umum"       => $data_penjualan_selesai_umum->tanggal_awal,
				"tr_penjualan_selesai_umum"       => $data_penjualan_selesai_umum->tanggal_akhir,
				
				"total_penjualan_menunggu_umum"   => $data_penjualan_menunggu_umum->grand_total,
				"jumlah_penjualan_menunggu_umum"  => $data_penjualan_menunggu_umum->jml_transaksi,
				"tl_penjualan_menunggu_umum"      => $data_penjualan_menunggu_umum->tanggal_awal,
				"tr_penjualan_menunggu_umum"      => $data_penjualan_menunggu_umum->tanggal_akhir,
				
				"total_penjualan_disiapkan_umum"  => $data_penjualan_disiapkan_umum->grand_total,
				"jumlah_penjualan_disiapkan_umum" => $data_penjualan_disiapkan_umum->jml_transaksi,
				"tl_penjualan_disiapkan_umum"     => $data_penjualan_disiapkan_umum->tanggal_awal,
				"tr_penjualan_disiapkan_umum"     => $data_penjualan_disiapkan_umum->tanggal_akhir,
				
				"total_retur_penjualan_umum"      => $data_retur_penjualan_umum->grand_total_retur,
				"jumlah_retur_penjualan_umum"     => $data_retur_penjualan_umum->jml_transaksi,
				"tl_retur_penjualan_umum"         => $data_retur_penjualan_umum->tanggal_awal,
				"tr_retur_penjualan_umum"         => $data_retur_penjualan_umum->tanggal_akhir,
				
				// Baris 2                        = Penjualan MRC dan Retur Penjualan MRC
				"total_penjualan_selesai_mrc"     => $data_penjualan_selesai_mrc->grand_total,
				"jumlah_penjualan_selesai_mrc"    => $data_penjualan_selesai_mrc->jml_transaksi,
				"tl_penjualan_selesai_mrc"        => $data_penjualan_selesai_mrc->tanggal_awal,
				"tr_penjualan_selesai_mrc"        => $data_penjualan_selesai_mrc->tanggal_akhir,
				
				"total_penjualan_menunggu_mrc"    => $data_penjualan_menunggu_mrc->grand_total,
				"jumlah_penjualan_menunggu_mrc"   => $data_penjualan_menunggu_mrc->jml_transaksi,
				"tl_penjualan_menunggu_mrc"       => $data_penjualan_menunggu_mrc->tanggal_awal,
				"tr_penjualan_menunggu_mrc"       => $data_penjualan_menunggu_mrc->tanggal_akhir,
				
				"total_penjualan_disiapkan_mrc"   => $data_penjualan_disiapkan_mrc->grand_total,
				"jumlah_penjualan_disiapkan_mrc"  => $data_penjualan_disiapkan_mrc->jml_transaksi,
				"tl_penjualan_disiapkan_mrc"      => $data_penjualan_disiapkan_mrc->tanggal_awal,
				"tr_penjualan_disiapkan_mrc"      => $data_penjualan_disiapkan_mrc->tanggal_akhir,
				
				"total_penjualan_dikirim_mrc"     => $data_penjualan_dikirim_mrc->grand_total,
				"jumlah_penjualan_dikirim_mrc"    => $data_penjualan_dikirim_mrc->jml_transaksi,
				"tl_penjualan_dikirim_mrc"        => $data_penjualan_dikirim_mrc->tanggal_awal,
				"tr_penjualan_dikirim_mrc"        => $data_penjualan_dikirim_mrc->tanggal_akhir,
				
				"total_retur_penjualan_mrc"       => $data_retur_penjualan_mrc->grand_total_retur,
				"jumlah_retur_penjualan_mrc"      => $data_retur_penjualan_mrc->jml_transaksi,
				"tl_retur_penjualan_mrc"          => $data_retur_penjualan_mrc->tanggal_awal,
				"tr_retur_penjualan_mrc"          => $data_retur_penjualan_mrc->tanggal_akhir,
				
				// Baris 3
				"total_pembelian_selesai"         => $data_pembelian_selesai->grand_total,
				"jumlah_pembelian_selesai"        => $data_pembelian_selesai->jml_transaksi,
				"tl_pembelian_s"                  => $data_pembelian_selesai->tanggal_awal,
				"tr_pembelian_s"                  => $data_pembelian_selesai->tanggal_akhir,
				
				"total_pembelian_menunggu"        => $data_pembelian_menunggu->grand_total,
				"jumlah_pembelian_menunggu"       => $data_pembelian_menunggu->jml_transaksi,
				"tl_pembelian_m"                  => $data_pembelian_menunggu->tanggal_awal,
				"tr_pembelian_m"                  => $data_pembelian_menunggu->tanggal_akhir,								
				
				"total_retur_pembelian"           => $data_retur_pembelian->grand_total,
				"jumlah_retur_pembelian"          => $data_retur_pembelian->jml_transaksi,
				"tl_retur_pembelian"              => $data_retur_pembelian->tanggal_awal,
				"tr_retur_pembelian"              => $data_retur_pembelian->tanggal_akhir,
				
				"total_barang_opname"             => $data_stok_opname->total_barang_opname,
				"total_qty_opname"                => $data_stok_opname->total_qty_opname,
				"tl_so"                           => $data_stok_opname->tanggal_awal,
				"tr_so"                           => $data_stok_opname->tanggal_akhir,
				
				// Baris 4							
				"total_barang_perakitan_selesai"  => $data_perakitan_selesai->total_barang_perakitan,
				"total_qty_perakitan_selesai"     => $data_perakitan_selesai->total_qty_perakitan,
				"total_barang_komponen_selesai"   => $data_komponen_selesai->total_barang_komponen,
				"total_qty_komponen_selesai"      => $data_komponen_selesai->total_qty_komponen,
				"tl_perakitan_s"                  => $data_perakitan_selesai->tanggal_awal,
				"tr_perakitan_s"                  => $data_perakitan_selesai->tanggal_akhir,
				
				"total_barang_perakitan_menunggu" => $data_perakitan_menunggu->total_barang_perakitan,
				"total_qty_perakitan_menunggu"    => $data_perakitan_menunggu->total_qty_perakitan,
				"total_barang_komponen_menunggu"  => $data_komponen_menunggu->total_barang_komponen,
				"total_qty_komponen_menunggu"     => $data_komponen_menunggu->total_qty_komponen,
				"tl_perakitan_m"                  => $data_perakitan_menunggu->tanggal_awal,
				"tr_perakitan_m"                  => $data_perakitan_menunggu->tanggal_akhir,								
				
				"total_hadiah_selesai"            => $data_hadiah_selesai->grand_total,
				"jumlah_hadiah_selesai"           => $data_hadiah_selesai->jml_transaksi,
				"tl_hadiah_s"                     => $data_hadiah_selesai->tanggal_awal,
				"tr_hadiah_s"                     => $data_hadiah_selesai->tanggal_akhir,
				
				"total_hadiah_menunggu"           => $data_hadiah_menunggu->grand_total,
				"jumlah_hadiah_menunggu"          => $data_hadiah_menunggu->jml_transaksi,
				"tl_hadiah_m"                     => $data_hadiah_menunggu->tanggal_awal,
				"tr_hadiah_m"                     => $data_hadiah_menunggu->tanggal_akhir,
				
				"tanggal_awal"                    => $tanggal_awal,
				"tanggal_akhir"                   => $tanggal_akhir
				// "query"                        => $this->db->last_query()
			));
		}
	}
	// Akhir total transaksi
}
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Referensi_penukaran_poin extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('pegawai_pusat_model','pegawai_pusat');
        $this->load->model('useraccess_pusat_model','useraccess_pusat');

		$this->load->model('periode_stok_pusat_model','periode_stok_pusat');
		$this->load->model('referensi_penukaran_poin_model','referensi_penukaran_poin');
		$this->load->model('barang_pusat_model','barang_pusat');
	}

	public function index()
	{
		// Cek user acces menu
		$id_pegawai = $this->session->userdata('id_pegawai');
		if($id_pegawai == ''){
			redirect(base_url().'login');
		}
		
		$cek_useraccess = $this->useraccess_pusat->cek_access($id_pegawai, '48');
		if($cek_useraccess){
			if($cek_useraccess->act_read == '0' or $cek_useraccess->act_read == '-'){
				redirect(base_url().'dashboard');
			}					
		}else{
			redirect(base_url().'dashboard');
		}

		$data_pegawai = $this->pegawai_pusat->get_by_id($id_pegawai);
		if($data_pegawai){
			$data['access_create']     = $cek_useraccess->act_create;
			$data['access_update']     = $cek_useraccess->act_update;		
			$data['data_pegawai']      = $data_pegawai;
			$data['data_induk_menu']   = $this->useraccess_pusat->get_induk_menu($id_pegawai);
			$data['atribut_halaman']   = 'Referensi Penukaran Poin';
			$data['halaman_transaksi'] = $this->load->view('admin/master_data/referensi_penukaran_poin/transaksi',$data,true);
			$data['halaman_plugin']    = $this->load->view('admin/master_data/referensi_penukaran_poin/plugin',$data,true);
			$data['isi_halaman']       = $this->load->view('admin/master_data/referensi_penukaran_poin/index',$data,true);
			$this->load->view('admin/layout',$data);			
		}else{
			redirect(base_url().'login');
		}
	}

	public function ajax_list()
	{
		// Cek user acces menu
		$id_pegawai 	= $this->session->userdata('id_pegawai');
		$cek_useraccess = $this->useraccess_pusat->cek_access($id_pegawai, '48');
		if($cek_useraccess->act_read == '0' or $cek_useraccess->act_read == '0'){
			redirect(base_url().'dashboard');
			exit();
		}

		$list = $this->referensi_penukaran_poin->get_datatables();
		$data = array();
		$no   = $_POST['start']; 
		foreach ($list as $referensi_penukaran_poin){
			$no++;
			$row = array();

			$row[] = $no;

			//Tombol
			if($cek_useraccess->act_update == 1){
				$tombol_edit 	 = '<a class="btn btn-rounded btn-default btn-xs" href="javascript:void(0)" title="Edit" 
										onclick="edit_referensi('."'".$referensi_penukaran_poin->id_referensi."'".')">
										<i class="fa fa-pencil" style="color:blue;"></i>
									</a>';
			}else{$tombol_edit 	 = '';}

			if($cek_useraccess->act_delete == 1){
				$tombol_hapus 	 = '<a class="btn btn-rounded btn-default btn-xs" href="javascript:void(0)" title="Hapus" 
										onclick="verifikasi_hapus('."'".$referensi_penukaran_poin->id_referensi."'".')">
				  						<i class="fa fa-times" style="color:red;"></i>
				  					</a>';
			}else{$tombol_hapus  = '';}

			if($tombol_edit == '' AND $tombol_hapus == ''){
				$tombol_keterangan = '<b class="btn btn-rounded btn-default btn-xs text-muted text-center">Tidak ada akses</b>';
			}else{
				$tombol_keterangan = '';
			}

			$row[] 	= '	'.$tombol_edit.'
						'.$tombol_hapus.'
						'.$tombol_keterangan.'
				  	  ';
			
			$row[]	= '<span>'.$referensi_penukaran_poin->sku.'</span>';
			$row[]	= '<span>'.$referensi_penukaran_poin->nama_barang.'</span>';
			$row[] 	= '<span class="pull-right">'.number_format($referensi_penukaran_poin->jumlah_poin,'0',',','.').'</span>';
			$row[] 	= '<span class="pull-right">'.number_format($referensi_penukaran_poin->qty,'0',',','.').'</span>';
			$row[] 	= '<span>'.$referensi_penukaran_poin->keterangan_lain.'</span>';
			$row[] 	= '<span>'.$referensi_penukaran_poin->tanggal_pembuatan.'</span>';
			$row[] 	= '<span>'.$referensi_penukaran_poin->tanggal_pembaharuan.'</span>';
			$data[] = $row;
		}

		$output = array(
			"draw"            => $_POST['draw'],
			"recordsTotal"    => $this->referensi_penukaran_poin->count_all('vamr4846_vama.referensi_penukaran_poin'),
			"recordsFiltered" => $this->referensi_penukaran_poin->count_filtered('_get_datatables_query'),
			"data"            => $data
		);
		echo json_encode($output);
	}

	public function ajax_kode()
	{
		if($this->input->is_ajax_request()){
			$keyword = $this->input->post('keyword');
			$barang  = $this->barang_pusat->cari_barang_sku_nama($keyword);				

			if($barang->num_rows() > 0){
				$json['status'] 	= 1;
				$json['datanya'] 	= "<ul id='daftar-autocomplete' class='list-group user-list'>";
				
				foreach($barang->result() as $b){
					if($b->foto!=""){
						$json['datanya'] .= "
			                <li class='user-list-item'>
			                	<div class='avatar avatar-sm-box'>
                                    <img src='assets/upload/image/barang/thumbs/$b->foto'>
			                	</div>";
			        }else{
						$nama_barang 	  = $b->nama_barang;
						$inisial 		  = substr($nama_barang, 0, 1);
						$json['datanya'] .= "
			                <li class='user-list-item'>
			                	<div class='avatar avatar-sm-box'>
                                    <span class='avatar-sm-box bg-info'>".$inisial."</span>
			                	</div>";
					}

					$json['datanya'] .= "
		                	<div class='user-desc'>
		                		<span id='id_barang' style='display: none;'>".$b->id_barang_pusat."</span>
								<span id='barangnya' class='name'><b>".$b->nama_barang."</b></span>
								<span id='skunya' class='name'>".$b->sku."</span>
								<span id='harga_eceran' class='name'>EC : Rp. ".$b->eceran."</span>
								<span id='eceran' style='display:none;'>".$b->harga_eceran."</span>
							</div>
						</li>
					";
				}

				$json['datanya'] .= "</ul>";
			}else{
				$json['status'] = 0;
			}
			echo json_encode($json);
		}
	}

	public function ajax_edit()
	{
		// Cek user acces menu
		$id_pegawai 	= $this->session->userdata('id_pegawai');
		$cek_useraccess = $this->useraccess_pusat->cek_access($id_pegawai, '48');
		if($cek_useraccess->act_update == '0' or $cek_useraccess->act_update == '-'){
			redirect(base_url().'referensi_penukaran_poin');
			exit();
		}

		$id_referensi = $this->input->post('id_referensi');
		if($id_referensi == ''){
			redirect(base_url().'referensi_penukaran_poin');
			exit();
		}else{
			$data = $this->referensi_penukaran_poin->get_by_id($id_referensi);
			echo json_encode($data);
		}
	}

	public function ajax_simpan()
	{
		$id_pegawai 	= $this->session->userdata('id_pegawai');
		$cek_useraccess = $this->useraccess_pusat->cek_access($id_pegawai, '48');

		$id_periode_stok_pusat = $this->input->post('id_periode_stok_pusat');
		if($id_periode_stok_pusat == ''){
			redirect(base_url().'referensi_penukaran_poin');
			exit();
		}

		$data_periode = $this->periode_stok_pusat->akhir();
		if($data_periode){
			$id_periode_stok_pusat = $data_periode->id_periode_stok_pusat;
		}

		$id_barang_pusat = $this->input->post('id_barang_pusat');
		$jumlah_poin     = $this->input->post('jumlah_poin');
		$qty             = $this->input->post('qty');
		$id_pegawai      = $this->session->userdata('id_pegawai');
		$catatan         = $this->input->post('catatan');

		// Validasi apakah data sudah ada atau belum
		$data_referensi = $this->referensi_penukaran_poin->get_id_barang($id_barang_pusat);
		if($data_referensi){
			// Cek user access menu
			if($cek_useraccess->act_update == '0' or $cek_useraccess->act_update == '-'){
				echo json_encode(array(
					"status" => 0,
					"pesan"  => "Maaf anda tidak di izinkan untuk mengedit referensi penukaran poin"
				));
				exit();
			}

			// Perbaharui referensi penukaran poin 
			$id_referensi = $data_referensi->id_referensi;
			$this->_validate();
			$data = array(
				'id_barang_pusat'      => $id_barang_pusat,
				'jumlah_poin'          => $jumlah_poin,
				'qty'                  => $qty,
				'keterangan_lain'      => $catatan,
				'id_pegawai_pembuatan' => $id_pegawai,
				'tanggal_pembuatan'    => date('Y-m-d H:i:s')
			);
			
			$this->referensi_penukaran_poin->update(array(
				'id_referensi' => $id_referensi
			   	), $data
			);
			echo json_encode(array("status" => 1));

		}else{
			// Cek user acces menu
			if($cek_useraccess->act_create == '0' or $cek_useraccess->act_create == '-'){
				echo json_encode(array(
						"status" => 0,
						"pesan"  => "<b class='text-danger'>Maaf anda tidak di izinkan untuk menyimpan stok opname </b>"
					)
			    );
				exit();
			}

			// Simpan data stok opname dan Update stok
			$this->_validate();
			$data = array(
				'id_barang_pusat'      => $id_barang_pusat,
				'jumlah_poin'          => $jumlah_poin,
				'qty'                  => $qty,
				'keterangan_lain'      => $catatan,
				'id_pegawai_pembuatan' => $id_pegawai,
				'tanggal_pembuatan'    => date('Y-m-d H:i:s')
			);
			$insert = $this->referensi_penukaran_poin->save($data);
			echo json_encode(array("status" => 1));

		}
	}

	public function ajax_verifikasi_hapus()
	{
		// Cek user acces menu
		$id_pegawai 	= $this->session->userdata('id_pegawai');
		$cek_useraccess = $this->useraccess_pusat->cek_access($id_pegawai, '48');
		if($cek_useraccess->act_delete == '0' or $cek_useraccess->act_delete == '-'){
			redirect(base_url().'referensi_penukaran_poin');
			exit();
		}

		$id_referensi = $this->input->post('id_referensi');
		if($id_referensi == ''){
			redirect(base_url().'referensi_penukaran_poin');
			exit();
		}
		
		$data = $this->referensi_penukaran_poin->get_by_id($id_referensi);		
		echo json_encode(array(
			'pesan' 	=> "<b class='text-danger'>Yakin ingin menghapus referensi penukaran poin ini ? </b></br></br>
							Nama Barang : </br>
							<b class='text-dark'>".$data->nama_barang."</b></br></br>
							
							Jumlah Poin : </br>
							<h2 class='text-dark'>".$data->jumlah_poin."</h2></br>
							
							Qty : </br>
							<h2 class='text-dark'>".$data->qty."</h2>",
			'footer'	=> 	"<button onclick='hapus_referensi($id_referensi)' 
								type='button' class='btn btn-primary waves-effect waves-light' 
								data-dismiss='modal' autofocus>Iya, Hapus</button> 
							<button type='button' class='btn btn-default waves-effect' 
								data-dismiss='modal'>Batal</button>"
	  	));
	}

	public function ajax_hapus()
	{
		$id_pegawai 	= $this->session->userdata('id_pegawai');
		$cek_useraccess = $this->useraccess_pusat->cek_access($id_pegawai, '48');
		if($cek_useraccess->act_delete == '0' or $cek_useraccess->act_delete == '-'){
			redirect(base_url().'referensi_penukaran_poin');
			exit();
		}

		$id_referensi = $this->input->post('id_referensi');
		if($id_referensi == ''){
			redirect(base_url().'referensi_penukaran_poin');
			exit();
		}

		$id_pegawai = $this->session->userdata('id_pegawai');
		$this->referensi_penukaran_poin->update_status_hapus($id_referensi, $id_pegawai);
		echo json_encode(array("status" => TRUE));
	}

	private function _validate()
	{
		$data = array();
		$data['error_string'] = array();
		$data['inputerror'] = array();
		$data['status'] = TRUE;

		if($this->input->post('id_barang_pusat') == ''){
			$data['inputerror'][] = 'pencarian_kode';
			$data['pesan'][]      = 'Belum ada barang yang anda pilih';
			$data['status']       = FALSE;
		}

		if($this->input->post('jumlah_poin') == ''){
			$data['inputerror'][] = 'jumlah_poin';
			$data['pesan'][]      = 'Jumlah poin wajib diisi';
			$data['status']       = FALSE;
		}

		if($this->input->post('qty') == ''){
			$data['inputerror'][] = 'qty';
			$data['pesan'][]      = 'Qty wajib diisi';
			$data['status']       = FALSE;
		}

		if($data['status'] === FALSE){
			echo json_encode($data);
			exit();
		}
	}

}

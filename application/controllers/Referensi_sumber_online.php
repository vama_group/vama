<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Referensi_sumber_online extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('pegawai_pusat_model','pegawai_pusat');
        $this->load->model('useraccess_pusat_model','useraccess_pusat');
		$this->load->model('referensi_sumber_online_model','referensi_sumber_online');
	}

	public function index()
	{
		// Cek user acces menu
		$id_pegawai = $this->session->userdata('id_pegawai');
		if($id_pegawai == ''){
			redirect(base_url().'login');
		}
		
		$cek_useraccess = $this->useraccess_pusat->cek_access($id_pegawai, '49');
		if($cek_useraccess){
			if($cek_useraccess->act_read == '0' or $cek_useraccess->act_read == '-'){
				redirect(base_url().'dashboard');
			}					
		}else{
			redirect(base_url().'dashboard');
		}

		$data_pegawai = $this->pegawai_pusat->get_by_id($id_pegawai);
		if($data_pegawai){
			$data['access_create']   = $cek_useraccess->act_create;
			$data['access_update']   = $cek_useraccess->act_update;
			$data['access_delete']   = $cek_useraccess->act_delete;
			$data['data_pegawai']    = $data_pegawai;
			$data['data_induk_menu'] = $this->useraccess_pusat->get_induk_menu($id_pegawai);
			$data['atribut_halaman'] = 'Referensi Sumber Online';
			$data['halaman_list']    = $this->load->view('admin/master_data/referensi_sumber_online/list',$data,true);
			$data['halaman_form']    = $this->load->view('admin/master_data/referensi_sumber_online/form',$data,true);
			$data['halaman_plugin']  = $this->load->view('admin/master_data/referensi_sumber_online/plugin',$data,true);
			$data['isi_halaman']     = $this->load->view('admin/master_data/referensi_sumber_online/index',$data,true);
			$this->load->view('admin/layout',$data);
		}else{
			redirect(base_url().'login');
		}
	}

	public function ajax_list()
	{
		// Cek user acces menu
		$id_pegawai     = $this->session->userdata('id_pegawai');
		$cek_useraccess = $this->useraccess_pusat->cek_access($id_pegawai, '49');
		if($cek_useraccess->act_read == '0' or $cek_useraccess->act_read == '0'){
			redirect(base_url().'dashboard');
			exit();
		}

		$list 	= $this->referensi_sumber_online->get_datatables();
		$data 	= array();
		$no 	= $_POST['start'];
		foreach ($list as $referensi) {
			$no++;
			$row 	= array();
			$row[] 	= $no;
			
			//add html for action
			if($cek_useraccess->act_update == 1){
				$tombol_edit 	 = '<a class="btn btn-rounded btn-xs btn-default" href="javascript:void(0)" 
										title="Edit" onclick="edit_referensi('."'".$referensi->id_referensi."'".')">
										<i class="fa fa-pencil" style="color: blue;"></i>
									</a>';
			}else{$tombol_edit 	 = '';}

			if($cek_useraccess->act_delete == 1){
				$tombol_hapus 	 = '<a class="btn btn-rounded btn-xs btn-default" href="javascript:void(0)" 
										title="Hapus" onclick="verifikasi_delete('."'".$referensi->id_referensi."'".')">
										<i class="fa fa-remove" style="color: red;"></i>
				  					</a>';
			}else{$tombol_hapus  = '';}

			if($cek_useraccess->act_update == 1 or $cek_useraccess->act_delete == 1){
				$row[] = '					
						  	'.$tombol_edit.'
						  	'.$tombol_hapus.'
					  	 ';
			}

			$row[] 	= $referensi->nama_sumber_online;
			$row[]	= $referensi->pegawai_save;
			$row[]	= $referensi->pegawai_edit;
			$row[] 	= $referensi->tanggal_pembuatan;
			$row[] 	= $referensi->tanggal_pembaharuan;
			$data[] = $row;
		}

		$output = array(	
			"draw"            => $_POST['draw'],
			"recordsTotal"    => $this->referensi_sumber_online->count_all(),
			"recordsFiltered" => $this->referensi_sumber_online->count_filtered(),
			"data"            => $data,
		);

		echo json_encode($output);
	}

	public function ajax_add()
	{
		// Cek user acces menu
		$id_pegawai 	= $this->session->userdata('id_pegawai');
		$cek_useraccess = $this->useraccess_pusat->cek_access($id_pegawai, '49');
		if($cek_useraccess->act_create == '0' or $cek_useraccess->act_create == '-'){
			redirect(base_url().'referensi_sumber_online');
			exit();
		}

		$this->_validate('0');
		$data = array(
			'nama_sumber_online'   => $this->input->post('nama_sumber_online'),
			'id_pegawai_pembuatan' => $this->session->userdata('id_pegawai'),
			'tanggal_pembuatan'    => date('Y-m-d H:i:s')
		);
		$insert = $this->referensi_sumber_online->save($data);
		echo json_encode(array("status" => TRUE));
	}
	
	public function ajax_edit()
	{
		// Cek user acces menu
		$id_pegawai 	= $this->session->userdata('id_pegawai');
		$cek_useraccess = $this->useraccess_pusat->cek_access($id_pegawai, '49');
		if($cek_useraccess->act_update == '0' or $cek_useraccess->act_update == '-'){
			redirect(base_url().'referensi_sumber_online');
			exit();
		}

		$id_referensi = $this->input->post('id_referensi');
		if($id_referensi == ''){
			redirect(base_url().'referensi_sumber_online');
			exit();
		}else{
			$data = $this->referensi_sumber_online->get_by_id($id_referensi);
			echo json_encode($data);			
		}
	}

	public function ajax_update()
	{
		$id_pegawai 	= $this->session->userdata('id_pegawai');
		$cek_useraccess = $this->useraccess_pusat->cek_access($id_pegawai, '49');
		if($cek_useraccess->act_update == '0' or $cek_useraccess->act_update == '0'){
			redirect(base_url().'referensi_sumber_online');
			exit();
		}

		$id_referensi = $this->input->post('id_referensi');
		if($id_referensi == ''){
			redirect(base_url().'referensi_sumber_online');
			exit();
		}

		$this->_validate($id_referensi);
		$data = array(
			'nama_sumber_online'     => $this->input->post('nama_sumber_online'),
			'id_pegawai_pembaharuan' => $this->session->userdata('id_pegawai')
		);
		$this->referensi_sumber_online->update(array('id_referensi' => $this->input->post('id_referensi')), $data);
		echo json_encode(array("status" => TRUE));
	}

	public function ajax_verifikasi_delete()
	{
		// Cek user acces menu
		$id_pegawai 	= $this->session->userdata('id_pegawai');
		$cek_useraccess = $this->useraccess_pusat->cek_access($id_pegawai, '49');
		if($cek_useraccess->act_delete == '0' or $cek_useraccess->act_delete == '-'){
			redirect(base_url().'referensi_sumber_online');
			exit();
		}

		$id_referensi = $this->input->post('id_referensi');
		if($id_referensi == ''){
			redirect(base_url().'referensi_sumber_online');
			exit();
		}
		
		$data = $this->referensi_sumber_online->get_by_id($id_referensi);		
		echo json_encode(array(
								'pesan' 	=> "<b class='text-danger'>Yakin ingin menghapus referensi sumber online ini ? </b></br></br>
												Jenis Barang : </br>
												<h2 class='text-dark'>".$data->nama_sumber_online."</h2>",
								'footer'	=> 	"<button onclick='delete_referensi($id_referensi)' 
													type='button' class='btn btn-primary waves-effect waves-light' 
													data-dismiss='modal' autofocus>Iya, Hapus</button> 
												<button type='button' class='btn btn-default waves-effect' 
													data-dismiss='modal'>Batal</button>"
							  	)
						);
	}

	public function ajax_delete()
	{
		$id_pegawai 	= $this->session->userdata('id_pegawai');
		$cek_useraccess = $this->useraccess_pusat->cek_access($id_pegawai, '49');
		if($cek_useraccess->act_delete == '0' or $cek_useraccess->act_delete == '-'){
			redirect(base_url().'referensi_sumber_online');
			exit();
		}

		$id_referensi = $this->input->post('id_referensi');
		if($id_referensi == ''){
			redirect(base_url().'referensi_sumber_online');
			exit();
		}

		$this->referensi_sumber_online->update_status_hapus($id_referensi, $id_pegawai);
		echo json_encode(array("status" => TRUE));
	}

	private function _validate($id_referensi)
	{
		$data                 = array();
		$data['error_string'] = array();
		$data['inputerror']   = array();
		$data['status']       = TRUE;
		$nama_sumber_online   = $this->input->post('nama_sumber_online');

		if($nama_sumber_online == ''){
			$data['inputerror'][] 	= 'nama_sumber_online';
			$data['error_string'][] = 'Nama sumber online wajib diisi';
			$data['status'] 		= FALSE;
		}

		$validasi_nama = $this->referensi_sumber_online->get_by_nama($nama_sumber_online);
		if($validasi_nama){
			if($id_referensi !== $validasi_nama->id_referensi){
				$data['inputerror'][] 	= 'nama_sumber_online';
				$data['error_string'][] = 'Nama sumber online sudah digunakan, harap ganti nama sumber online';
				$data['status'] 		= FALSE;
			}
		}

		if($data['status'] === FALSE){
			echo json_encode($data);
			exit();
		}
	}
}

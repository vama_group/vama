<?php
defined('BASEPATH') OR exit('No direct script access allowed');

// Load library phpspreadsheet
require('./assets/excel/vendor/autoload.php');
use PhpOffice\PhpSpreadsheet\Helper\Sample;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;

class Laporan_stok_opname_pusat extends MY_Controller 
{
	function __construct()
	{
		parent::__construct();
		$this->CI =& get_instance();
		$this->load->model('pegawai_pusat_model','pegawai_pusat');
        $this->load->model('useraccess_pusat_model','useraccess_pusat');
		$this->load->model('periode_stok_pusat_model','periode_stok_pusat');
		$this->load->model('stok_opname_pusat_model','stok_opname_pusat');
		$this->load->model('barang_pusat_model','barang_pusat');
	}

	public function index()
	{
		// Cek user acces menu
		$id_pegawai = $this->session->userdata('id_pegawai');
		if($id_pegawai == ''){
			redirect(base_url().'login');
		}
		
		$cek_useraccess = $this->useraccess_pusat->cek_access($id_pegawai, '34');
		if($cek_useraccess){
			if($cek_useraccess->act_read == '0' or $cek_useraccess->act_read == '-'){
				redirect(base_url().'dashboard');
			}					
		}else{
			redirect(base_url().'dashboard');
		}

		$data_pegawai = $this->pegawai_pusat->get_by_id($id_pegawai);
		if($data_pegawai){
			// Ambil kode dan id periode stok terbaru
			$periode_sekarang              = $this->periode_stok_pusat->akhir();
			$data_periode                  = $this->periode_stok_pusat->listing();
			
			$data['access_create']         = $cek_useraccess->act_create;
			$data['data_pegawai']          = $data_pegawai;
			$data['data_induk_menu']       = $this->useraccess_pusat->get_induk_menu($id_pegawai);
			
			$data['data_periode_sekarang'] = $periode_sekarang;
			$data['data_periode']          = $data_periode;
			
			$data['atribut_halaman']       = 'Laporan Stok Opname Pusat';
			$data['halaman_laporan']       = $this->load->view('admin/laporan/stok_opname_pusat/laporan',$data,true);
			$data['halaman_plugin']        = $this->load->view('admin/laporan/stok_opname_pusat/plugin',$data,true);
			$data['isi_halaman']           = $this->load->view('admin/laporan/stok_opname_pusat/index',$data,true);
			$this->load->view('admin/layout',$data);
		}else{
			redirect(base_url().'login');
		}
	}

	public function ajax_list()
	{
		// Cek user acces menu
		$id_pegawai 	= $this->session->userdata('id_pegawai');
		$cek_useraccess = $this->useraccess_pusat->cek_access($id_pegawai, '34');
		if($cek_useraccess->act_read == '0' or $cek_useraccess->act_read == '0'){
			redirect(base_url().'dashboard');
			exit();
		}

		$id_periode_stok_pusat = $this->input->post('id_periode_stok_pusat');
		if($id_periode_stok_pusat == ''){
			redirect(base_url().'laporan_stok_opname_pusat');
			exit();
		}

		$list 	= $this->stok_opname_pusat->get_datatables($id_periode_stok_pusat);
		$data 	= array();
		$no 	= $_POST['start'];
		foreach ($list as $stok_opname_pusat){
			$no++;
			$row = array();

			$row[] = $no;

			// //Tombol
			// if($cek_useraccess->act_update == 1){
			// 	$tombol_edit 	 = '<a class="btn btn-rounded btn-default btn-xs" href="javascript:void(0)" title="Edit" 
			// 							onclick="edit_stok_opname_pusat('."'".$stok_opname_pusat->id_stok_opname_pusat."'".')">
			// 							<i class="fa fa-pencil" style="color:blue;"></i>
			// 						</a>';
			// }else{$tombol_edit 	 = '';}

			// if($cek_useraccess->act_delete == 1){
			// 	$tombol_hapus 	 = '<a class="btn btn-rounded btn-default btn-xs" href="javascript:void(0)" title="Hapus" 
			// 							onclick="verifikasi_hapus('."'".$stok_opname_pusat->id_stok_opname_pusat."'".')">
			// 	  						<i class="fa fa-times" style="color:red;"></i>
			// 	  					</a>';
			// }else{$tombol_hapus  = '';}

			// if($tombol_edit == '' AND $tombol_hapus == ''){
			// 	$tombol_keterangan = '<b class="btn btn-rounded btn-default btn-xs text-muted text-center">Tidak ada akses</b>';
			// }else{
			// 	$tombol_keterangan = '';
			// }

			// $row[] 	= '	'.$tombol_edit.'
			// 			'.$tombol_hapus.'
			// 			'.$tombol_keterangan.'
			// 	  	  ';
			
			if($stok_opname_pusat->jumlah_so == $stok_opname_pusat->jumlah_so_sebelumnya AND 
			   $stok_opname_pusat->tanggal_pembuatan == $stok_opname_pusat->tanggal_pembaharuan){
				$warna_text = 'text-danger';	
			}else{
				$warna_text = 'text-success';
			}

			$row[]	= '<span class="'.$warna_text.'">'.$stok_opname_pusat->sku.'</span>';
			$row[]	= '<span class="'.$warna_text.'">'.$stok_opname_pusat->nama_barang.'</span>';
			$row[] 	= '<span class="'.$warna_text.' pull-right">'.number_format($stok_opname_pusat->harga_eceran,'0',',','.').'</span>';
			$row[] 	= '<span class="'.$warna_text.' pull-right">'.number_format($stok_opname_pusat->jumlah_so_sebelumnya,'0',',','.').'</span>';
			$row[] 	= '<span class="'.$warna_text.' pull-right">'.number_format($stok_opname_pusat->jumlah_so,'0',',','.').'</span>';
			$row[] 	= '<span class="'.$warna_text.'">'.$stok_opname_pusat->masuk_stok.'</span>';
			$row[] 	= '<span class="'.$warna_text.'">'.$stok_opname_pusat->catatan.'</span>';
			$row[] 	= '<span class="'.$warna_text.'">'.$stok_opname_pusat->pegawai_save.'</span>';
			$row[] 	= '<span class="'.$warna_text.'">'.$stok_opname_pusat->tanggal_pembuatan.'</span>';
			$row[] 	= '<span class="'.$warna_text.'">'.$stok_opname_pusat->pegawai_edit.'</span>';
			$row[] 	= '<span class="'.$warna_text.'">'.$stok_opname_pusat->tanggal_pembaharuan.'</span>';
			$data[] = $row;
		}

		$output = array(
			"draw"            => $_POST['draw'],
			"recordsTotal"    => $this->stok_opname_pusat->count_all('stok_opname_pusat'),
			"recordsFiltered" => $this->stok_opname_pusat->count_filtered('_get_datatables_query', $id_periode_stok_pusat),
			"data"            => $data
		);
		echo json_encode($output);
	}

	public function ajax_list_riwayat()
	{
		// Cek user acces menu
		$id_pegawai 	= $this->session->userdata('id_pegawai');
		$cek_useraccess = $this->useraccess_pusat->cek_access($id_pegawai, '34');
		if($cek_useraccess->act_read == '0' or $cek_useraccess->act_read == '0'){
			redirect(base_url().'dashboard');
			exit();
		}

		$id_periode_stok_pusat = $this->input->post('id_periode_stok_pusat');
		if($id_periode_stok_pusat == ''){
			redirect(base_url().'laporan_stok_opname_pusat');
			exit();
		}

		$list = $this->stok_opname_pusat->get_datatables_riwayat($id_periode_stok_pusat);
		$data = array();
		$no   = $_POST['start'];
		foreach ($list as $stok_opname_pusat) {
			$no++;
			$row    = array();
			$row[]  = $no;
			$row[]  = $stok_opname_pusat->sku;
			$row[]  = $stok_opname_pusat->nama_barang;
			$row[]  = number_format($stok_opname_pusat->jumlah_so_sebelumnya,'0',',','.');
			$row[]  = number_format($stok_opname_pusat->jumlah_so,'0',',','.');
			$row[]  = $stok_opname_pusat->masuk_stok;
			$row[]  = $stok_opname_pusat->catatan;
			$row[]  = $stok_opname_pusat->nama_pegawai;
			$row[]  = $stok_opname_pusat->tanggal_pembuatan;
			$data[] = $row;
		}

		$output = array(
			"draw"            => $_POST['draw'],
			"recordsTotal"    => $this->stok_opname_pusat->count_all('riwayat_stok_opname_pusat'),
			"recordsFiltered" => $this->stok_opname_pusat->count_filtered('_get_datatables_query_riwayat_so', $id_periode_stok_pusat),
			"data"            => $data,
		);
		echo json_encode($output);
	}	

	public function ajax_list_batal()
	{
		// Cek user acces menu
		$id_pegawai 	= $this->session->userdata('id_pegawai');
		$cek_useraccess = $this->useraccess_pusat->cek_access($id_pegawai, '34');
		if($cek_useraccess->act_read == '0' or $cek_useraccess->act_read == '0'){
			redirect(base_url().'dashboard');
			exit();
		}

		$id_periode_stok_pusat = $this->input->post('id_periode_stok_pusat');
		if($id_periode_stok_pusat == ''){
			redirect(base_url().'laporan_stok_opname_pusat');
			exit();
		}

		$list = $this->stok_opname_pusat->get_datatables_batal($id_periode_stok_pusat);
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $stok_opname_pusat) {
			$no++;
			$row    = array();
			$row[]  = $no;
			$row[]  = $stok_opname_pusat->sku;
			$row[]  = $stok_opname_pusat->nama_barang;
			$row[]  = number_format($stok_opname_pusat->jumlah_so,'0',',','.');
			$row[]  = $stok_opname_pusat->masuk_stok;
			$row[]  = $stok_opname_pusat->keterangan_batal;
			$row[]  = $stok_opname_pusat->pegawai_batal;
			$row[]  = $stok_opname_pusat->tanggal_pembatalan;
			$row[]  = $stok_opname_pusat->pegawai_save;
			$row[]  = $stok_opname_pusat->tanggal_pembuatan;
			$row[]  = $stok_opname_pusat->pegawai_edit;
			$row[]  = $stok_opname_pusat->tanggal_pembaharuan;			
			$data[] = $row;
		}

		$output = array(
			"draw"            => $_POST['draw'],
			"recordsTotal"    => $this->stok_opname_pusat->count_all('stok_opname_pusat_batal'),
			"recordsFiltered" => $this->stok_opname_pusat->count_filtered('_get_datatables_query_batal_so', $id_periode_stok_pusat),
			"data"            => $data,
		);
		echo json_encode($output);
	}

	public function ajax_cetak_ke_excel()
	{
		// Cek user acces menu
		$id_pegawai 	= $this->session->userdata('id_pegawai');
		$cek_useraccess = $this->useraccess_pusat->cek_access($id_pegawai, '34');
		if($cek_useraccess->act_read == '0' or $cek_useraccess->act_read == '0'){
			redirect(base_url().'dashboard');
			exit();
		}

		// Ambil periode stok pusat terakhir
		$data_periode = $this->periode_stok_pusat->akhir();
		if($data_periode){
			$id_periode_stok_pusat = $data_periode->id_periode_stok_pusat;
		}else{
			redirect(base_url().'laporan_stok_opname_pusat');
			exit();
		}

		$data_so     = $this->stok_opname_pusat->daftar_so_per_periode($id_periode_stok_pusat);
		$spreadsheet = new Spreadsheet();

		// Set document properties
		$spreadsheet->getProperties()->setCreator('Laporan - Stok Opname Pusat')
		->setLastModifiedBy('Ali')
		->setTitle('Office 2007 XLSX Test Document')
		->setSubject('Office 2007 XLSX Test Document')
		->setDescription('Test document for Office 2007 XLSX, generated using PHP classes.')
		->setKeywords('office 2007 openxml php')
		->setCategory('Test result file');

		// Add some data
		$spreadsheet->setActiveSheetIndex(0)
			->setCellValue('A1', 'SKU')
			->setCellValue('B1', 'NAMA BARANG')
			->setCellValue('C1', 'HRG ECERAN')
			->setCellValue('D1', 'QTY SO')
			->setCellValue('E1', 'TGL SAVE')
			->setCellValue('F1', 'PEGAWAI SAVE')
			->setCellValue('G1', 'TGL EDIT')
			->setCellValue('H1', 'PEGAWAI EDIT')
		;

		// Miscellaneous glyphs, UTF-8
		$i=2; foreach($data_so as $data_so) {
		$spreadsheet->setActiveSheetIndex(0)
			->setCellValue('A'.$i, $data_so->sku)
			->setCellValue('B'.$i, $data_so->nama_barang)
			->setCellValue('C'.$i, $data_so->harga_eceran)
			->setCellValue('D'.$i, $data_so->jumlah_so)
			->setCellValue('E'.$i, $data_so->pegawai_save)
			->setCellValue('F'.$i, $data_so->tanggal_pembuatan)
			->setCellValue('G'.$i, $data_so->tanggal_pembaharuan)
			->setCellValue('H'.$i, $data_so->pegawai_edit);
			$i++;
		}

		// Rename worksheet
		$spreadsheet->getActiveSheet()->setTitle('Report Excel '.date('d-m-Y H'));

		// Set active sheet index to the first sheet, so Excel opens this as the first sheet
		$spreadsheet->setActiveSheetIndex(0);

		// Redirect output to a client’s web browser (Xlsx)
		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment;filename="Report Excel.xlsx"');
		header('Cache-Control: max-age=0');
		// If you're serving to IE 9, then the following may be needed
		header('Cache-Control: max-age=1');

		// If you're serving to IE over SSL, then the following may be needed
		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
		header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
		header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
		header('Pragma: public'); // HTTP/1.0

		$writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
		$writer->save('php://output');
		exit;
	}
}
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Periode_stok_toko extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->CI =& get_instance();
		$this->load->model('pegawai_pusat_model','pegawai_pusat');
        $this->load->model('useraccess_pusat_model','useraccess_pusat');

		$this->load->model('periode_stok_toko_model','periode_stok_toko');
		$this->load->model('customer_pusat_model','customer_pusat');
	}

	public function index()
	{		
		// Cek user acces menu
		$id_pegawai = $this->session->userdata('id_pegawai');
		if($id_pegawai == ''){
			redirect(base_url().'login');
		}
		
		$cek_useraccess = $this->useraccess_pusat->cek_access($id_pegawai, '8');
		if($cek_useraccess){
			if($cek_useraccess->act_read == '0' or $cek_useraccess->act_read == '-'){
				redirect(base_url().'dashboard');
			}					
		}else{
			redirect(base_url().'dashboard');
		}

		$data_pegawai = $this->pegawai_pusat->get_by_id($id_pegawai);
		if($data_pegawai){
			$data['data_pegawai']    = $data_pegawai;
			$data['data_induk_menu'] = $this->useraccess_pusat->get_induk_menu($id_pegawai);
			$data['access_create']   = $cek_useraccess->act_create;
			$data['atribut_halaman'] = 'Periode Stok Toko';
			$data['list_toko']       = $this->customer_pusat->listing_toko();
			$data['halaman_list']    = $this->load->view('admin/master_data/periode_stok_toko/list',$data,true);
			$data['halaman_form']    = $this->load->view('admin/master_data/periode_stok_toko/form',$data,true);
			$data['halaman_plugin']  = $this->load->view('admin/master_data/periode_stok_toko/plugin',$data,true);
			$data['isi_halaman']     = $this->load->view('admin/master_data/periode_stok_toko/index',$data,true);
			$this->load->view('admin/layout',$data);
		}else{
			redirect(base_url().'login');
		}
	}

	public function ajax_list()
	{
		// Cek user acces menu
		$id_pegawai     = $this->session->userdata('id_pegawai');
		$data_pegawai   = $this->pegawai_pusat->get_by_id($id_pegawai);
		$cek_useraccess = $this->useraccess_pusat->cek_access($id_pegawai, '8');
		$this->CI->session->set_userdata('usergroup_name', $data_pegawai->usergroup_name);
		if($cek_useraccess->act_read == '0' or $cek_useraccess->act_read == '-'){
			redirect(base_url().'dashboard');
		}

		$list = $this->periode_stok_toko->get_datatables();
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $periode_stok_toko) {
			$no++;
			$row = array();

			$row[] = $no;
			$row[] = $periode_stok_toko->nama_toko;			
			$row[] = $periode_stok_toko->kode_periode_stok_toko;
			$row[] = $periode_stok_toko->tanggal_periode_awal;
			$row[] = $periode_stok_toko->tanggal_periode_akhir;			
			$row[] = $periode_stok_toko->pegawai_save;
			$row[] = $periode_stok_toko->tanggal_pembuatan;		

			//add html for action
			if($cek_useraccess->act_update == '0' or $cek_useraccess->act_update == '-'){
				$tombol_edit = '<b class="btn btn-rounded btn-xs btn-default text-muted text-center">Tidak ada akses</b>';
			}else{
				$tombol_edit = '<a class="btn btn-rounded btn-xs btn-primary" href="javascript:void(0)" title="Edit" 
									onclick="edit_periode_stok_toko('."'".$periode_stok_toko->id_periode_stok_toko."'".')">
									<i class="fa fa-pencil"></i>
								</a>';
			}

			$row[]  = $tombol_edit;
			$data[] = $row;
		}

		$output = array(
					"draw"            => $_POST['draw'],
					"recordsTotal"    => $this->periode_stok_toko->count_all(),
					"recordsFiltered" => $this->periode_stok_toko->count_filtered(),
					"data"            => $data,
				);
		//output to json format
		echo json_encode($output);
	}

	public function ajax_edit($id_periode_stok_toko)
	{
		// Cek user acces menu
		$id_pegawai 	= $this->session->userdata('id_pegawai');
		$cek_useraccess = $this->useraccess_pusat->cek_access($id_pegawai, '8');
		if($cek_useraccess->act_update == '0' or $cek_useraccess->act_update == '-'){
			redirect(base_url().'periode_stok_toko');
			exit();
		}

		$data = $this->periode_stok_toko->get_by_id($id_periode_stok_toko);
		echo json_encode($data);
	}

	public function ajax_add()
	{
		// Buat kode baru
		$id_toko   = $this->input->post('id_toko');
		$data_toko = $this->customer_pusat->get_by_id($id_toko);
		if($data_toko){
			$kode_toko = $data_toko->kode_customer_pusat;
		}else{
			echo json_encode(array(
				"status"  => '0',
				"id_toko" => $id_toko
			));
			exit();
		}

		$akhir   = $this->periode_stok_toko->akhir($id_toko);
		$inisial = "PS";

		if($akhir){
			$id_periode_stok_toko = $akhir->id_periode_stok_toko;
		}else{
			$id_periode_stok_toko = 0;
		}

		$jumlah       = $id_periode_stok_toko + 1;
		$kode_periode = $inisial.$kode_toko.$jumlah;
		// Akhir buat kode baru		

		$this->_validate();
		
		$data = array(
			'kode_periode_stok_toko' => $kode_periode,
			'id_toko'                => $id_toko,
			'tanggal_periode_awal'   => $this->input->post('tanggal_periode_awal'),
			'tanggal_periode_akhir'  => $this->input->post('tanggal_periode_akhir'),
			'id_pegawai_pembuatan'   => $this->session->userdata('id_pegawai'),
			'tanggal_pembuatan'      => date('Y-m-d H:i:s')
		);
		$insert = $this->periode_stok_toko->save($data);
		echo json_encode(array("status" => '1'));
	}

	public function ajax_update()
	{
		// Cek user acces menu
		$id_pegawai 	= $this->session->userdata('id_pegawai');
		$cek_useraccess = $this->useraccess_pusat->cek_access($id_pegawai, '8');
		if($cek_useraccess->act_update == '0' or $cek_useraccess->act_update == '-'){
			redirect(base_url().'periode_stok_toko');
			exit();
		}

		$this->_validate();
		$data = array(
					'tanggal_periode_awal'   => $this->input->post('tanggal_periode_awal'),
					'tanggal_periode_akhir'  => $this->input->post('tanggal_periode_akhir'),
					'id_pegawai_pembaharuan' => $this->session->userdata('id_pegawai')
				);
		$this->periode_stok_toko->update(array(
											'id_periode_stok_toko' => $this->input->post('id_periode_stok_toko')), 
											$data
										);
		echo json_encode(array("status" => '1'));
	}

	public function ajax_delete($id_periode_stok_toko)
	{
		$this->periode_stok_toko->delete_by_id($id_periode_stok_toko);
		echo json_encode(array("status" => TRUE));
	}

	private function _validate()
	{
		$data                 = array();
		$data['error_string'] = array();
		$data['inputerror']   = array();
		$data['status']       = TRUE;

		if($this->input->post('id_toko') == ''){
			$data['inputerror'][] = 'id_toko';
			$data['error_string'][] = 'Toko wajib dipilih';
			$data['status'] = FALSE;
		}

		if($this->input->post('tanggal_periode_awal') == ''){
			$data['inputerror'][] = 'tanggal_awal_periode';
			$data['error_string'][] = 'Tanggal awal periode wajib diisi';
			$data['status'] = FALSE;
		}

		if($this->input->post('tanggal_periode_akhir') == ''){
			$data['inputerror'][] = 'tanggal_periode_akhir';
			$data['error_string'][] = 'Tanggal akhir periode wajib diisi';
			$data['status'] = FALSE;
		}

		if($data['status'] === FALSE){
			echo json_encode($data);
			exit();
		}
	}

}

<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Laporan_retur_penjualan_pusat extends MY_Controller 
{
	function __construct()
	{
		parent::__construct();
		$this->CI =& get_instance();
		$this->load->model('pegawai_pusat_model','pegawai_pusat');
        $this->load->model('useraccess_pusat_model','useraccess_pusat');
		$this->load->model('customer_pusat_model','customer_pusat');
		$this->load->model('barang_pusat_model','barang_pusat');
		$this->load->model('retur_penjualan_master_model','retur_penjualan_master');
		$this->load->model('retur_penjualan_detail_model','retur_penjualan_detail');
	}

	public function index()
	{
		// Cek user acces menu
		$id_pegawai = $this->session->userdata('id_pegawai');
		if($id_pegawai == ''){
			redirect(base_url().'login');
		}

		$id_pegawai   = $this->session->userdata('id_pegawai');
		$data_pegawai = $this->pegawai_pusat->get_by_id($id_pegawai);
		if($data_pegawai){
			$cek_useraccess = $this->useraccess_pusat->cek_access($id_pegawai, '42');
			if($cek_useraccess){
				if($cek_useraccess->act_read == '0' or $cek_useraccess->act_read == '-'){
					redirect(base_url().'dashboard');
					exit();
				}
			}else{
				redirect(base_url().'dashboard');
			}
			
			$data['access_create']   = $cek_useraccess->act_create;
			$data['data_pegawai']    = $data_pegawai;
			$data['data_induk_menu'] = $this->useraccess_pusat->get_induk_menu($id_pegawai);
			
			$data['atribut_halaman'] = 'Laporan Retur Penjualan Pusat';
			$data['halaman_laporan'] = $this->load->view('admin/laporan/retur_penjualan/laporan',$data,true);
			$data['halaman_plugin']  = $this->load->view('admin/laporan/retur_penjualan/plugin',$data,true);
			$data['isi_halaman']     = $this->load->view('admin/laporan/retur_penjualan/index',$data,true);
			$this->load->view('admin/layout',$data);
		}else{
			redirect(base_url().'login');
		}
	}

	public function ajax_list()
	{
		// Cek user acces menu
		$id_pegawai               = $this->session->userdata('id_pegawai');
		$cek_useraccess_transaksi = $this->useraccess_pusat->cek_access($id_pegawai, '28');
		$cek_useraccess_laporan   = $this->useraccess_pusat->cek_access($id_pegawai, '42');
		if($cek_useraccess_laporan->act_read == '0' or $cek_useraccess_laporan->act_read == '-'){
			redirect(base_url().'dashboard');
		}

		$tanggal_filter 			= $this->input->post('tanggal_filter');
		if($tanggal_filter == ''){
			redirect(base_url().'laporan_retur_penjualan_pusat');
			exit();
		}

		$tanggal_awal  = substr($tanggal_filter, 0, 10);
		$tanggal_akhir = substr($tanggal_filter, 18, 27);
		$data_laporan_retur_penjualan = $this->retur_penjualan_master->get_laporan('_get_laporan_query', $tanggal_awal, $tanggal_akhir);
		
		$data = array();
		$no   = $_POST['start'];
		foreach ($data_laporan_retur_penjualan as $dlpj){
			$no++;
			$row   = array();
			$row[] = $no;

			// Awal tombol
			if($cek_useraccess_laporan->act_update == 1){
				$tombol_edit 	 = '<a class="btn btn-default btn-rounded btn-xs" 
										onclick="edit_retur_penjualan('."'".$dlpj->id_retur_penjualan_m."'".')">
								   		<i class="fa fa-pencil" style="color:blue;"></i>
								    </a>';
			}else{
				if($cek_useraccess_transaksi->act_update == 1 AND $dlpj->status_retur == "MENUNGGU" 
					OR $cek_useraccess_transaksi->act_create == 1 AND $dlpj->status_retur == "MENUNGGU"){
					$tombol_edit = '<a class="btn btn-default btn-rounded btn-xs" 
										onclick="edit_retur_penjualan('."'".$dlpj->id_retur_penjualan_m."'".')">
										<i class="fa fa-pencil" style="color:blue;"></i>
									</a>';
				}else{
					$tombol_edit = '';
				}
			}

			if($cek_useraccess_laporan->act_delete == 1){
				$tombol_hapus = '<a class="btn btn-default btn-rounded btn-xs" 
									onclick="verifikasi_hapus_transaksi('."'".$dlpj->id_retur_penjualan_m."'".')">
								 	<i class="fa fa-times" style="color:red;"></i>
								 </a>';
			}else{
				$tombol_hapus = '';
				if($cek_useraccess_transaksi->act_delete == 1 AND $dlpj->status_retur == "MENUNGGU"){
					$tombol_hapus = '<a class="btn btn-default btn-rounded btn-xs" 
										onclick="verifikasi_hapus_transaksi('."'".$dlpj->id_retur_penjualan_m."'".')">
									 	<i class="fa fa-times" style="color:red;"></i>
								 	 </a>';
				}else{
					$tombol_hapus = '';
				}
			}

			$row[] = '<a class="btn btn-default btn-rounded btn-xs" onclick="cetak_faktur('."'".$dlpj->id_retur_penjualan_m."'".')">
						<i class="fa fa-print"></i>
					  </a>
					  '.$tombol_edit.'
					  '.$tombol_hapus.'
				  	  ';
			// Akhir tombol

			$row[] 	= '<span class="text-dark">'.$dlpj->no_retur_penjualan.'</span><br/>';
			$row[] 	= '<span class="text-dark">'.$dlpj->no_penjualan.'</span><br/>';

			if($dlpj->status_retur == "MENUNGGU"){
				$row[] 	= '<span class="btn btn-danger btn-xs btn-block btn-rounded text-center">'.$dlpj->status_retur.'</span>';
			}elseif($dlpj->status_retur == "SELESAI"){
				$row[] 	= '<span class="btn btn-success btn-xs btn-block btn-rounded text-center">'.$dlpj->status_retur.'</span>';
			}

			$row[] 	= '<span class="text-dark">'.date('d-m-Y', strtotime($dlpj->tanggal_retur)).'</span><br/>';
			$row[] 	= '<span class="text-dark">'.$dlpj->nama_customer.'</label>';
			$row[] 	= '<span class="pull-right">'.number_format($dlpj->total_retur, '0', ',', '.').'</span>';
			$row[] 	= '<span class="pull-right">'.number_format($dlpj->total_potongan, '0', ',', '.').'</span>';
			$row[] 	= '<span class="pull-right">'.number_format($dlpj->total_saldo, '0', ',', '.').'</span>';
			
			$row[] 	= '<span class="text-dark">'.$dlpj->keterangan_lain.'</span><br/>';
			$row[] 	= '<span class="text-dark">'.$dlpj->pegawai_save.'</span><br/>';
			$row[] 	= '<span class="text-dark">'.$dlpj->tanggal_pembuatan.'</span><br/>';
			$row[] 	= '<span class="text-dark">'.$dlpj->pegawai_edit.'</span><br/>';
			$row[] 	= '<span class="text-dark">'.$dlpj->tanggal_pembaharuan.'</span><br/>';
			$data[]	= $row;
		}

		$output['draw']            = $_POST['draw'];
		$output['recordsTotal']    = $this->retur_penjualan_master->count_all('vamr4846_vama.retur_penjualan_detail', $tanggal_awal, $tanggal_akhir);
		$output['recordsFiltered'] =  $this->retur_penjualan_master->count_filtered('_get_laporan_query', $tanggal_awal, $tanggal_akhir);
		$output['data']            = $data;
		echo json_encode($output);
	}

	public function ajax_list_detail()
	{
		// Cek user acces menu
		$id_pegawai               = $this->session->userdata('id_pegawai');
		$cek_useraccess_transaksi = $this->useraccess_pusat->cek_access($id_pegawai, '28');
		$cek_useraccess_laporan   = $this->useraccess_pusat->cek_access($id_pegawai, '42');
		if($cek_useraccess_laporan->act_read == '0' or $cek_useraccess_laporan->act_read == '-'){
			redirect(base_url().'dashboard');
		}

		$tanggal_filter = $this->input->post('tanggal_filter');
		if($tanggal_filter == ''){
			redirect(base_url().'laporan_retur_penjualan_pusat');
			exit();
		}

		$tanggal_awal                 = substr($tanggal_filter, 0, 10);
		$tanggal_akhir                = substr($tanggal_filter, 18, 27);
		$data_laporan_retur_penjualan = $this->retur_penjualan_master->get_laporan(
			'_get_laporan_query_detail', $tanggal_awal, $tanggal_akhir
		);
		
		$data               = array();
		$no                 = $_POST['start'];
		$no_retur_penjualan = '0';

		foreach ($data_laporan_retur_penjualan as $dlpj){
			$no++;
			$row   = array();
			$row[] = $no;

			// Awal tombol
			if($cek_useraccess_laporan->act_update == 1){
				$tombol_edit 	 = '<a class="btn btn-default btn-rounded btn-xs" 
										onclick="edit_retur_penjualan('."'".$dlpj->id_retur_penjualan_m."'".')">
								   		<i class="fa fa-pencil" style="color:blue;"></i>
								    </a>';
			}else{
				if($cek_useraccess_transaksi->act_update == 1 AND $dlpj->status_retur == "MENUNGGU" 
					OR $cek_useraccess_transaksi->act_create == 1 AND $dlpj->status_retur == "MENUNGGU"){
					$tombol_edit = '<a class="btn btn-default btn-rounded btn-xs" 
										onclick="edit_retur_penjualan('."'".$dlpj->id_retur_penjualan_m."'".')">
										<i class="fa fa-pencil" style="color:blue;"></i>
									</a>';
				}else{
					$tombol_edit = '';
				}
			}

			if($cek_useraccess_laporan->act_delete == 1){
				$tombol_hapus = '<a class="btn btn-default btn-rounded btn-xs" 
									onclick="verifikasi_hapus_transaksi('."'".$dlpj->id_retur_penjualan_m."'".')">
								 	<i class="fa fa-times" style="color:red;"></i>
								 </a>';
			}else{
				$tombol_hapus = '';
				if($cek_useraccess_transaksi->act_delete == 1 AND $dlpj->status_retur == "MENUNGGU"){
					$tombol_hapus = '<a class="btn btn-default btn-rounded btn-xs" 
										onclick="verifikasi_hapus_transaksi('."'".$dlpj->id_retur_penjualan_m."'".')">
									 	<i class="fa fa-times" style="color:red;"></i>
								 	 </a>';
				}else{
					$tombol_hapus = '';
				}
			}

			$row[] = '<a class="btn btn-default btn-rounded btn-xs" onclick="cetak_faktur('."'".$dlpj->id_retur_penjualan_m."'".')">
						<i class="fa fa-print"></i>
					  </a>
					  '.$tombol_edit.'
					  '.$tombol_hapus.'
				  	  ';
			// Akhir tombol

			if($no_retur_penjualan <> $dlpj->no_retur_penjualan){
				$row[] 	= '<span class="btn btn-success btn-block btn-rounded btn-xs">'.$dlpj->no_retur_penjualan.'</span>';
				$row[] 	= '<span class="btn btn-success btn-block btn-rounded btn-xs">'.$dlpj->no_penjualan.'</span>';
			}else{
				$row[] 	= '<span class="btn btn-default btn-block btn-rounded btn-xs">'.$dlpj->no_retur_penjualan.'</span>';
				$row[] 	= '<span class="btn btn-default btn-block btn-rounded btn-xs">'.$dlpj->no_penjualan.'</span>';
			}

			$row[] 	= '<span class="text-dark">'.$dlpj->nama_customer.'</label>';
			if($dlpj->masuk_stok == "RUSAK"){
				$row[] 	= '<span class="btn btn-danger btn-xs btn-block btn-rounded text-center">'.$dlpj->masuk_stok.'</span>';
			}elseif($dlpj->masuk_stok == "JUAL"){
				$row[] 	= '<span class="btn btn-success btn-xs btn-block btn-rounded text-center">'.$dlpj->masuk_stok.'</span>';
			}
			$row[] 	= '<span class="text-dark">'.$dlpj->sku.'</label>';
			$row[] 	= '<span class="text-dark">'.$dlpj->nama_barang.'</label>';
			$row[] 	= '<span class="pull-right">'.number_format($dlpj->harga_satuan, '0', ',', '.').'</span>';
			$row[] 	= '<span class="pull-right">'.number_format($dlpj->jumlah_beli, '0', ',', '.').'</span>';
			$row[] 	= '<span class="pull-right">'.number_format($dlpj->jumlah_retur, '0', ',', '.').'</span>';

			$row[] 	= '<span class="pull-right">'.number_format($dlpj->potongan_harga, '0', ',', '.').'</span>';
			$row[] 	= '<span class="pull-right">'.number_format($dlpj->subtotal_retur, '0', ',', '.').'</span>';
			$row[] 	= '<span class="pull-right">'.number_format($dlpj->subtotal_potongan, '0', ',', '.').'</span>';
			$row[] 	= '<span class="pull-right">'.number_format($dlpj->subtotal_saldo, '0', ',', '.').'</span>';
			
			$row[] 	= '<span class="pull-right text-dark"> '.$dlpj->keterangan.'</label>';
			if($dlpj->status_retur == "MENUNGGU"){
				$row[] 	= '<span class="pull-right text-danger">'.$dlpj->status_retur.'</span>';
			}elseif($dlpj->status_retur == "SELESAI"){
				$row[] 	= '<span class="pull-right text-success">'.$dlpj->status_retur.'</span>';
			}

			$row[]              = '<span class="pull-right text-dark">'.$dlpj->pegawai_save.'</span><br/>';
			$row[]              = '<span class="pull-right text-dark">'.$dlpj->tanggal_pembuatan.'</span><br/>';
			$row[]              = '<span class="pull-right text-dark">'.$dlpj->pegawai_edit.'</span><br/>';
			$row[]              = '<span class="pull-right text-dark">'.$dlpj->tanggal_pembaharuan.'</span><br/>';
			$data[]             = $row;
			$no_retur_penjualan = $dlpj->no_retur_penjualan;
		}

		$output['draw']            = $_POST['draw'];
		$output['recordsTotal']    = $this->retur_penjualan_master->count_all('vamr4846_vama.retur_penjualan_detail', $tanggal_awal, $tanggal_akhir);
		$output['recordsFiltered'] =  $this->retur_penjualan_master->count_filtered('_get_laporan_query_detail', $tanggal_awal, $tanggal_akhir);
		$output['data']            = $data;
		echo json_encode($output);
	}

	public function ajax_list_detail_batal()
	{
		// Cek user acces menu
		$id_pegawai 				= $this->session->userdata('id_pegawai');
		$cek_useraccess_transaksi 	= $this->useraccess_pusat->cek_access($id_pegawai, '28');
		$cek_useraccess_laporan 	= $this->useraccess_pusat->cek_access($id_pegawai, '42');
		if($cek_useraccess_laporan->act_read == '0' or $cek_useraccess_laporan->act_read == '-'){
			redirect(base_url().'dashboard');
		}

		$tanggal_filter 			= $this->input->post('tanggal_filter');
		if($tanggal_filter == ''){
			redirect(base_url().'laporan_retur_penjualan_pusat');
			exit();
		}

		$tanggal_awal                 = substr($tanggal_filter, 0, 10);
		$tanggal_akhir                = substr($tanggal_filter, 18, 27);
		$data_laporan_retur_penjualan = $this->retur_penjualan_master->get_laporan(
			'_get_laporan_query_detail_batal', $tanggal_awal, $tanggal_akhir
		);
		
		$data               = array();
		$no                 = $_POST['start'];
		$no_retur_penjualan = '0';

		foreach ($data_laporan_retur_penjualan as $dlpj){
			$no++;
			$row   = array();
			$row[] = $no;

			if($no_retur_penjualan <> $dlpj->no_retur_penjualan){
				$row[] = '<span class="btn btn-success btn-block btn-rounded btn-xs">'.$dlpj->no_retur_penjualan.'</span>';
				$row[] = '<span class="btn btn-success btn-block btn-rounded btn-xs">'.$dlpj->no_penjualan.'</span>';
			}else{
				$row[] = '<span class="btn btn-default btn-block btn-rounded btn-xs">'.$dlpj->no_retur_penjualan.'</span>';
				$row[] = '<span class="btn btn-default btn-block btn-rounded btn-xs">'.$dlpj->no_penjualan.'</span>';
			}
			
			$row[] = '<span class="text-dark">'.$dlpj->nama_customer.'</label>';
			if($dlpj->masuk_stok      == "RUSAK"){
				$row[] = '<span class="btn btn-danger btn-xs btn-block btn-rounded text-center">'.$dlpj->masuk_stok.'</span>';
			}elseif($dlpj->masuk_stok == "JUAL"){
				$row[] = '<span class="btn btn-success btn-xs btn-block btn-rounded text-center">'.$dlpj->masuk_stok.'</span>';
			}

			$row[]  = '<span class="text-dark">'.$dlpj->sku.'</label>';
			$row[]  = '<span class="text-dark">'.$dlpj->nama_barang.'</label>';
			$row[]  = '<span class="pull-right">'.number_format($dlpj->harga_satuan, '0', ',', '.').'</span>';
			$row[]  = '<span class="pull-right">'.number_format($dlpj->jumlah_beli, '0', ',', '.').'</span>';
			$row[]  = '<span class="pull-right">'.number_format($dlpj->jumlah_retur, '0', ',', '.').'</span>';
			
			$row[]  = '<span class="pull-right">'.number_format($dlpj->potongan_harga, '0', ',', '.').'</span>';
			$row[]  = '<span class="pull-right">'.number_format($dlpj->subtotal_retur, '0', ',', '.').'</span>';
			$row[]  = '<span class="pull-right">'.number_format($dlpj->subtotal_potongan, '0', ',', '.').'</span>';
			$row[]  = '<span class="pull-right">'.number_format($dlpj->subtotal_saldo, '0', ',', '.').'</span>';			
			$row[]  = '<span class="pull-right text-dark"> '.$dlpj->keterangan.'</label>';
			$row[]  = '<span class="pull-right text-dark"> '.$dlpj->keterangan_batal.'</label>';
			
			$row[]  = '<span class="pull-right text-dark">'.$dlpj->pegawai_batal.'</span><br/>';
			$row[]  = '<span class="pull-right text-dark">'.$dlpj->tanggal_pembatalan.'</span><br/>';
			$row[]  = '<span class="pull-right text-dark">'.$dlpj->pegawai_save.'</span><br/>';
			$row[]  = '<span class="pull-right text-dark">'.$dlpj->tanggal_pembuatan.'</span><br/>';
			$row[]  = '<span class="pull-right text-dark">'.$dlpj->pegawai_edit.'</span><br/>';
			$row[]  = '<span class="pull-right text-dark">'.$dlpj->tanggal_pembaharuan.'</span><br/>';
			$data[] = $row;
			$no_retur_penjualan = $dlpj->no_retur_penjualan;
		}

		$output['draw']            = $_POST['draw'];
		$output['recordsTotal']    = $this->retur_penjualan_master->count_all('vamr4846_vama.retur_penjualan_detail_batal', $tanggal_awal, $tanggal_akhir);
		$output['recordsFiltered'] =  $this->retur_penjualan_master->count_filtered('_get_laporan_query_detail_batal', $tanggal_awal, $tanggal_akhir);
		$output['data']            = $data;
		echo json_encode($output);
	}

	public function ambil_total()
	{
		// Cek user acces menu
		$id_pegawai     = $this->session->userdata('id_pegawai');
		$cek_useraccess = $this->useraccess_pusat->cek_access($id_pegawai, '42');
		if($cek_useraccess->act_read == '0' or $cek_useraccess->act_read == '-'){
			redirect(base_url().'dashboard');
		}

		$tanggal_filter = $this->input->post('tanggal_filter');
		if($tanggal_filter == ''){
			redirect(base_url().'laporan_retur_penjualan_pusat');
			exit();
		}

		if($this->session->userdata('usergroup_name') == 'User' or
		   $this->session->userdata('usergroup_name') == ''){
			exit();
		}

		$tanggal_awal  = substr($tanggal_filter, 0, 10);
		$tanggal_akhir = substr($tanggal_filter, 18, 27);

		// Ambil jumlah transaksi, total tunai, debit dan total uang masuk
		$data_uang_masuk = $this->retur_penjualan_master->get_grand_total($tanggal_awal, $tanggal_akhir, '!=', 'SELESAI');
		if ($data_uang_masuk){
			$jml_transaksi			= number_format($data_uang_masuk->jml_transaksi,'0', ',', '.');
			$grand_total_potongan 	= '<span>Rp. <p class="pull-right">
										'.number_format($data_uang_masuk->grand_total_potongan,'0', ',', '.').'</p>
									   </span>';
			$grand_total_retur 	 	= '<span>Rp. <p class="pull-right">
										'.number_format($data_uang_masuk->grand_total_retur,'0', ',', '.').'</p>
									   </span>';
		}else{
			$jml_transaksi        = '0';
			$grand_total_potongan = '0';
			$grand_total_retur    = '0';
		}
		
		$json['status']               = 1;
		$json['jml_transaksi']        = $jml_transaksi;
		$json['grand_total_potongan'] = $grand_total_potongan;
		$json['grand_total_retur']    = $grand_total_retur;
		echo json_encode($json);
	}

	public function ajax_verifikasi_hapus_transaksi()
	{
		if($this->input->is_ajax_request()){
			$id_retur_penjualan_m = $this->input->post('id_retur_penjualan_m');
			$id_pegawai           = $this->session->userdata('id_pegawai');

			// Cek user acces menu
			$cek_useraccess_transaksi = $this->useraccess_pusat->cek_access($id_pegawai, '28');
			$cek_useraccess_laporan   = $this->useraccess_pusat->cek_access($id_pegawai, '42');

			// Validasi status retur penjualan
			$data = $this->retur_penjualan_master->get_master($id_retur_penjualan_m);
			if($data){
				if($data->status_retur == 'SELESAI'){
					if($cek_useraccess_laporan->act_delete == '0' or $cek_useraccess_laporan->act_delete == '-'){
						echo json_encode(array(	"status" 		=> 0,
											  	"info_pesan" 	=> "Maaf anda tidak diberikan izin untuk menghapus transaksi retur penjualan,
																	dengan status retur penjualan : SELESAI !",
												"pesan" 		=> "No. Retur Penjualan : ".$data->no_retur_penjualan."
																	No. Penjualan : ".$data->no_penjualan."
																	Nama Customer : ".$data->nama_customer."
																	Status Penjualan : ".$data->status_retur."
																	",
												"tipe_pesan" 	=> "error",
												"gaya_tombol"	=> "btn-danger btn-md waves-effect waves-light"
											 )
									   );
						exit();
					}
				}else{
					if($cek_useraccess_transaksi->act_delete == '0' or $cek_useraccess_transaksi->act_delete == '-'){
						echo json_encode(array(	"status" 		=> 0,
											  	"info_pesan" 	=> "Maaf anda tidak diberikan izin untuk menghapus transaksi retur penjualan!",
												"pesan" 		=> "No. Retur Penjualan : ".$data->no_retur_penjualan."
																	No. Penjualan : ".$data->no_penjualan."
																	Nama Customer : ".$data->nama_customer."
																	Status Penjualan : ".$data->status_retur."
																	",
												"tipe_pesan" 	=> "error",
												"gaya_tombol"	=> "btn-danger btn-md waves-effect waves-light"
											 )
									   );
						exit();
					}
				}

				// Awal validasi stok
				$data_detail = $this->retur_penjualan_detail->get_retur_penjualan_detail_all($id_retur_penjualan_m);
				foreach($data_detail AS $dpd){
					$id_barang_pusat = $dpd->id_barang_pusat;
					$jumlah_retur    = $dpd->jumlah_retur;

					// Ambil data barang dari master barang pusat
					$data_barang = $this->barang_pusat->get_id($id_barang_pusat);
					if($data_barang){
						$sku           = $data_barang->sku;
						$nama_barang   = $data_barang->nama_barang;
						if($dpd->masuk_stok == 'JUAL'){
							$keterangan_stok = 'Stok Sekarang';
							$stok_sekarang   = $data_barang->total_stok;
						}else{
							$keterangan_stok = 'Stok Rusak Sekarang';
							$stok_sekarang   = $data_barang->total_stok_rusak;
						}
						$validasi_stok = $stok_sekarang - $jumlah_retur + 0;

						if($validasi_stok < 0){
							echo json_encode(array(
													"status" 		=> 0,
												  	"info_pesan" 	=> "Gagal!",
													"pesan" 		=> "
																			Maaf anda tidak bisa menghapus transaksi retur penjualan ini !
																			Karna akan menyebabkan stok barang ini menjadi minus : 
																			SKU : ".$sku."
																			Nama Barang : ".$nama_barang."
																			".$keterangan_stok." : ".$stok_sekarang."
																			Jumlah Retur : ".$jumlah_retur."
																			Total Minus : ".$validasi_stok."
																		",
													"tipe_pesan" 	=> "error",
													"gaya_tombol"	=> "btn-danger btn-md waves-effect waves-light"
												 )
										   );
							exit();
						}
					}else{
						echo json_encode(array(
												"status" 		=> 0,
											  	"info_pesan" 	=> "Gagal!",
												"pesan" 		=> "
																		Data barang tidak ditemukan 
																		ID Barang : ".$id_barang_pusat."
																		Jumlah Retur : ".$jumlah_retur."
																	",
												"tipe_pesan" 	=> "error",
												"gaya_tombol"	=> "btn-danger btn-md waves-effect waves-light"
											 )
									   );
						exit();
					}
				}
				// Akhir validasi stok
			}

			if($data){
				$total_retur = number_format($data->total_retur,'0',',','.');
				echo json_encode(array(
					'status' 	=>	1,
					'pesan' 	=>	"
									<small>No. Retur Penjualan : </small><br/>
									<b class='text-dark'>$data->no_retur_penjualan </b><br/><br/>

									<small>No. Penjualan : </small><br/>
									<b class='text-dark'>$data->no_penjualan </b><br/><br/>

									<small>Nama Customer : </small> <br/>
									<b class='text-dark'>$data->nama_customer </b> </br><br/>

									<small>Total Retur : </small> <br/>
									<h2 class='text-danger'>Rp. $total_retur </h2>

									<textarea name='keterangan_batal' id='keterangan_batal' class='form-control input-md' placeholder='Keterangan batal' style='resize: vertical;'></textarea>
									",

					'footer'	=> 	"<button onclick='hapus_transaksi($data->id_retur_penjualan_m)' type='button' class='btn btn-danger
									 waves-effect waves-light' data-dismiss='modal' autofocus>Iya, Hapus</button>
									<button type='button' class='btn btn-default waves-effect' data-dismiss='modal'>Batal</button>"
				));
			}else{
				echo json_encode(array(	"status" 		=> 0,
									  	"info_pesan" 	=> "Oops!",
										"pesan" 		=> "Data transaksi retur penjualan tidak ditemukan",
										"tipe_pesan" 	=> "error",
										"gaya_tombol"	=> "btn-danger btn-md waves-effect waves-light"
								));
						exit();
			}
		}else{
			redirect(base_url().'laporan_retur_penjualan_pusat');
		}
	}

	public function ajax_hapus_transaksi()
	{
		if($this->input->is_ajax_request()){
			$id_retur_penjualan_m = $this->input->post('id_retur_penjualan_m');
			$id_pegawai           = $this->session->userdata('id_pegawai');
			
			// Cek user acces menu
			$cek_useraccess_transaksi = $this->useraccess_pusat->cek_access($id_pegawai, '28');
			$cek_useraccess_laporan   = $this->useraccess_pusat->cek_access($id_pegawai, '42');
			
			$master = $this->retur_penjualan_master->get_master($id_retur_penjualan_m);
			if($master){
				if($master->status_retur == 'SELESAI'){
					if($cek_useraccess_laporan->act_delete == '0' or $cek_useraccess_laporan->act_delete == '-'){
						echo json_encode(array(	"status" 		=> 0,
											  	"info_pesan" 	=> "Maaf anda tidak diberikan izin untuk menghapus transaksi retur penjualan,
																	dengan status retur penjualan : SELESAI !",
												"pesan" 		=> "No. Retur Penjualan : ".$master->no_retur_penjualan."
																	No. Penjualan : ".$master->no_penjualan."
																	Nama Customer : ".$master->nama_customer."
																	Status Penjualan : ".$master->status_retur."
																	",
												"tipe_pesan" 	=> "error",
												"gaya_tombol"	=> "btn-danger btn-md waves-effect waves-light"
											 )
									   );
						exit();
					}
				}else{
					if($cek_useraccess_transaksi->act_delete == '0' or $cek_useraccess_transaksi->act_delete == '-'){
						echo json_encode(array(	"status" 		=> 0,
											  	"info_pesan" 	=> "Maaf anda tidak diberikan izin untuk menghapus transaksi retur penjualan!",
												"pesan" 		=> "No. Retur Penjualan : ".$master->no_retur_penjualan."
																	No. Penjualan : ".$master->no_penjualan."
																	Nama Customer : ".$master->nama_customer."
																	Status Penjualan : ".$master->status_retur."
																	",
												"tipe_pesan" 	=> "error",
												"gaya_tombol"	=> "btn-danger btn-md waves-effect waves-light"
											 )
									   );
						exit();
					}
				}

				// Awal validasi stok
				$data_detail = $this->retur_penjualan_detail->get_retur_penjualan_detail_all($id_retur_penjualan_m);
				foreach($data_detail AS $dpd){
					$id_barang_pusat = $dpd->id_barang_pusat;
					$jumlah_retur    = $dpd->jumlah_retur;

					// Ambil data barang dari master barang pusat
					$data_barang = $this->barang_pusat->get_id($id_barang_pusat);
					if($data_barang){
						$sku           = $data_barang->sku;
						$nama_barang   = $data_barang->nama_barang;
						if($dpd->masuk_stok == 'JUAL'){
							$keterangan_stok = 'Stok Sekarang';
							$stok_sekarang   = $data_barang->total_stok;
						}else{
							$keterangan_stok = 'Stok Rusak Sekarang';
							$stok_sekarang   = $data_barang->total_stok_rusak;
						}
						$validasi_stok = $stok_sekarang - $jumlah_retur + 0;

						if($validasi_stok < 0){
							echo json_encode(array(
													"status" 		=> 0,
												  	"info_pesan" 	=> "Gagal!",
													"pesan" 		=> "
																			Maaf anda tidak bisa menghapus transaksi retur penjualan ini !
																			Karna akan menyebabkan stok barang ini menjadi minus : 
																			SKU : ".$sku."
																			Nama Barang : ".$nama_barang."
																			".$keterangan_stok." : ".$stok_sekarang."
																			Jumlah Retur : ".$jumlah_retur."
																			Total Minus : ".$validasi_stok."
																		",
													"tipe_pesan" 	=> "error",
													"gaya_tombol"	=> "btn-danger btn-md waves-effect waves-light"
												 )
										   );
							exit();
						}
					}else{
						echo json_encode(array(
												"status" 		=> 0,
											  	"info_pesan" 	=> "Gagal!",
												"pesan" 		=> "
																		Data barang tidak ditemukan 
																		ID Barang : ".$id_barang_pusat."
																		Jumlah Retur : ".$jumlah_retur."
																	",
												"tipe_pesan" 	=> "error",
												"gaya_tombol"	=> "btn-danger btn-md waves-effect waves-light"
											 )
									   );
						exit();
					}
				}
				// Akhir validasi stok
			}

			if($master){
				$no_penjualan     = $master->no_penjualan;
				$keterangan_batal = $this->input->post('keterangan_batal');
				$hapus            = $this->retur_penjualan_master->hapus_transaksi($id_retur_penjualan_m, $id_pegawai, $keterangan_batal);
				if($hapus){
					echo json_encode(array(	"status" 		=> 1,
											"no_penjualan"	=> $master->no_penjualan,
											"info_pesan" 	=> "Berhasil!",	
											"tipe_pesan" 	=> "success",
											"gaya_tombol"	=> "btn-success btn-md waves-effect waves-light",
											"pesan" 		=> "
																No. Retur Penjualan : ".$master->no_retur_penjualan."
																No. Penjualan : ".$master->no_penjualan."
																Nama Customer : ".$master->nama_customer."
																Berhasil dihapus.
																"
									));
				}else{
					echo json_encode(array(	"status"		=> 0,
											"info_pesan" 	=> "Oops!",
											"pesan" 		=> "Terjadi kesalahan, coba lagi !",
											"tipe_pesan" 	=> "error",
											"gaya_tombol"	=> "btn-danger btn-md waves-effect waves-light"
									));
				}
			}else{
				echo json_encode(array(	"status" 		=> 0,
									  	"info_pesan" 	=> "Oops!",
										"pesan" 		=> "Data transaksi retur penjualan tidak ditemukan",
										"tipe_pesan" 	=> "error",
										"gaya_tombol"	=> "btn-danger btn-md waves-effect waves-light"
								));
			}
		}else{
			redirect(base_url().'laporan_penjualan_pusat');
		}
	}	
}
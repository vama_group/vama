<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Usergroup_model extends CI_Model {

	// Load database
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	// Get data
	public function get_by_id($id_usergroup){
		$this->db->select('up.*');
		$this->db->from('vamr4846_vama.usergroup AS up');
		$this->db->where('up.id_usergroup', $id_usergroup);
		$query=$this->db->get();
		return $query->row();
	}

	// Listing data
	public function listing(){
		$this->db->select('up.*');
		$this->db->from('vamr4846_vama.usergroup AS up');
		$this->db->order_by('up.id_usergroup', 'DESC');
		$query=$this->db->get();
		return $query->result();
	}

	// Detail data
	public function detail($id_usergroup){
		$this->db->select('up.*');
		$this->db->from('vamr4846_vama.usergroup AS up');
		$this->db->where('up.id_usergroup', $id_usergroup);
		$this->db->order_by('id_usergroup','DESC');
		$query=$this->db->get();
		return $query->result();
	}	

	// Insert data
	public function tambah(){
		$this->db->insert('vamr4846_vama.usergroup', $data);
	}	

	// Update data
	public function edit(){
		$this->db->where('id_usergroup', $data['id_usergroup']);
		$this->db->update('vamr4846_vama.usergroup', $data);
	}	

	// Delete data
	public function delete(){
		$this->db->where('id_usergroup', $data['id_usergroup']);
		$this->db->delete('vamr4846_vama.usergroup', $data);
	}	
}

/* End of file Pegaai_model.php */
/* Location: ./application/models/Pegaai_model.php */
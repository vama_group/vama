<?php
class Hadiah_detail_model extends CI_Model
{
	// Hadiah induk
	var $column_order         = array('', '', 'bp.sku','bp.nama_barang', 'hd.harga_satuan', 'hd.jumlah_hadiah', 'hd.subtotal');
	var $column_search        = array('bp.kode_barang','bp.sku','bp.nama_barang'); 
	var $order                = array('hd.tanggal_pembaharuan' => 'desc'); 
	
	// Hadiah batal
	var $column_order_batal   = array('', '', 'bp.sku','bp.nama_barang', 'hdb.harga_satuan', 'hdb.jumlah_hadiah', 
									  'hdb.subtotal', 'hdb.keterangan_batal');
	var $column_search_batal  = array('bp.kode_barang','bp.sku','bp.nama_barang', 'hdb.keterangan_batal'); 
	var $order_batal          = array('hdb.tanggal_pembatalan' => 'desc'); 
	
	// Cari barang
	var $column_order_barang  = array('', '', 'bp.sku','bp.nama_barang','bp.total_stok');
	var $column_search_barang = array('bp.kode_barang','bp.sku','bp.nama_barang'); 
	var $order_barang         = array('bp.nama_barang' => 'desc');

	private function _get_datatables_query($id_hadiah_m)
	{		
		$this->db->select('
			hd.id_hadiah_d, hd.harga_satuan, hd.jumlah_hadiah, hd.subtotal,
			bp.sku, bp.kode_barang, bp.nama_barang,
			pp1.nama_pegawai AS pegawai_save, pp2.nama_pegawai AS pegawai_edit, 
			hd.tanggal_pembuatan, hd.tanggal_pembaharuan
		');
		$this->db->from('vamr4846_vama.hadiah_detail AS hd');
		$this->db->join('vamr4846_vama.barang_pusat as bp','bp.id_barang_pusat=hd.id_barang_pusat','LEFT');
		$this->db->join('vamr4846_vama.pegawai_pusat as pp1', 'pp1.id_pegawai=hd.id_pegawai_pembuatan', 'LEFT');
		$this->db->join('vamr4846_vama.pegawai_pusat as pp2', 'pp2.id_pegawai=hd.id_pegawai_pembaharuan', 'LEFT');
		$this->db->where('id_hadiah_m', $id_hadiah_m);

		$i = 0;
		foreach ($this->column_search as $item){
			if($_POST['search']['value']){
				if($i===0){
					$this->db->group_start();
					$this->db->like($item, $_POST['search']['value']);
				}else{
					$this->db->or_like($item, $_POST['search']['value']);
				}

				if(count($this->column_search) - 1 == $i)
					$this->db->group_end();
			}
			$i++;
		}
		
		if(isset($_POST['order'])){
			$this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		}else if(isset($this->order)){
			$order = $this->order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}

	private function _get_datatables_query_batal($id_hadiah_m)
	{		
		$this->db->select('
			hdb.id_hadiah_b, hdb.id_hadiah_m, hdb.id_hadiah_d, hdb.harga_satuan, hdb.jumlah_hadiah, hdb.subtotal,
			bp.sku, bp.kode_barang, bp.nama_barang,
			pp1.nama_pegawai AS pegawai_save, pp2.nama_pegawai AS pegawai_edit, pp3.nama_pegawai AS pegawai_batal,  
			hdb.tanggal_pembuatan, hdb.tanggal_pembaharuan, hdb.tanggal_pembatalan, hdb.keterangan_batal
		');
		$this->db->from('vamr4846_vama.hadiah_detail_batal AS hdb');
		$this->db->join('vamr4846_vama.barang_pusat as bp','bp.id_barang_pusat=hdb.id_barang_pusat','LEFT');
		$this->db->join('vamr4846_vama.pegawai_pusat as pp1', 'pp1.id_pegawai=hdb.id_pegawai_pembuatan', 'LEFT');
		$this->db->join('vamr4846_vama.pegawai_pusat as pp2', 'pp2.id_pegawai=hdb.id_pegawai_pembaharuan', 'LEFT');
		$this->db->join('vamr4846_vama.pegawai_pusat as pp3', 'pp3.id_pegawai=hdb.id_pegawai_pembatalan', 'LEFT');
		$this->db->where('id_hadiah_m', $id_hadiah_m);

		$i = 0;
		foreach ($this->column_search_batal as $item){
			if($_POST['search']['value']){
				if($i===0){
					$this->db->group_start();
					$this->db->like($item, $_POST['search']['value']);
				}else{
					$this->db->or_like($item, $_POST['search']['value']);
				}

				if(count($this->column_search_batal) - 1 == $i)
					$this->db->group_end();
			}
			$i++;
		}
		
		if(isset($_POST['order'])){
			$this->db->order_by($this->column_order_batal[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		}else if(isset($this->order_batal)){
			$order = $this->order_batal;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}

	private function _get_datatables_query_barang($id_hadiah_m)
	{		
		$ambil_pembelian = $this->db->query("SELECT id_barang_pusat 
											 FROM vamr4846_vama.hadiah_detail 
											 WHERE id_hadiah_m =".$id_hadiah_m."");
		$hd_id_barang = array();
		foreach ($ambil_pembelian->result() as $row){
			$hd_id_barang[] = $row->id_barang_pusat; 
		}

		$id_barang = implode(",", $hd_id_barang);
		$idh       = explode(",", $id_barang);
		
		$this->db->select('
			bp.id_barang_pusat, bp.sku, bp.kode_barang, bp.nama_barang, bp.harga_eceran, bp.total_stok
		');
		$this->db->from('vamr4846_vama.barang_pusat as bp');
		$this->db->where('total_stok >', 0);
		$this->db->where_not_in('bp.id_barang_pusat', $idh);
		$i = 0;
	
		foreach ($this->column_search_barang as $item){
			if($_POST['search']['value']){
				if($i===0){
					$this->db->group_start();
					$this->db->like($item, $_POST['search']['value']);
				}else{
					$this->db->or_like($item, $_POST['search']['value']);
				}

				if(count($this->column_search_barang) - 1 == $i)
				$this->db->group_end();
			}
			$i++;
		}
		
		if(isset($_POST['order'])){
			$this->db->order_by($this->column_order_barang[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		}else if(isset($this->order_barang)){
			$order = $this->order_barang;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}

	function get_datatables($perintah, $id)
	{
		$this->$perintah($id);
		if($_POST['length'] != -1)
		$this->db->limit($_POST['length'], $_POST['start']);
		$query = $this->db->get();
		return $query->result();
	}

	function count_filtered($perintah, $id)
	{
		$this->$perintah($id);
		$query = $this->db->get();
		return $query->num_rows();
	}

	public function count_all($table, $id)
	{
		$this->db->from($table);
		if($table == 'vamr4846_vama.barang_pusat'){
			// Ambil semua id barang hadiah per id_hadiah_m 
			$ambil_hadiah = $this->db->query("
				SELECT id_barang_pusat 
				FROM vamr4846_vama.hadiah_detail 
				WHERE id_hadiah_m =".$id."
			");
			$hd_id_barang = array();
			foreach ($ambil_hadiah->result() as $row){
				$hd_id_barang[] = $row->id_barang_pusat; 
			}
			
			$id_barang = implode(",", $hd_id_barang);
			$idh       = explode(",", $id_barang);
			$this->db->where('total_stok >', 0);
			$this->db->where_not_in('id_barang_pusat', $idh);
		}else{
			$this->db->where('id_hadiah_m', $id);
		}
		return $this->db->count_all_results();
	}

	function get_hadiah_detail($id_hadiah_d)
	{
		$this->db->select('hd.*, 
						   hm.no_hadiah, hm.nama_hadiah, hm.id_customer_pusat, hm.status_hadiah,
						   bp.sku, bp.nama_barang');
		$this->db->from('vamr4846_vama.hadiah_detail as hd');
		$this->db->join('vamr4846_vama.hadiah_master as hm','hm.id_hadiah_m=hd.id_hadiah_m','LEFT');
		$this->db->join('vamr4846_vama.barang_pusat as bp','bp.id_barang_pusat=hd.id_barang_pusat','LEFT');
		$this->db->where('id_hadiah_d', $id_hadiah_d);
		$query = $this->db->get();
		return $query->row();
	}

	function get_id($id_hadiah_m, $id_barang_pusat)
	{
		$this->db->select('*');
		$this->db->from('vamr4846_vama.hadiah_detail');
		$this->db->where(array(  
			'id_hadiah_m'     => $id_hadiah_m,
			'id_barang_pusat' => $id_barang_pusat
		));
		$query = $this->db->get();
		return $query->row();
	}	

	function insert_detail($id_hadiah_m, $id_barang_pusat, $jumlah_hadiah, 
						   $modal, $harga_satuan, $subtotal, $id_pegawai)
	{ 
		$dt = array(
			'id_hadiah_m'          => $id_hadiah_m,
			'id_barang_pusat'      => $id_barang_pusat,
			'jumlah_hadiah'        => $jumlah_hadiah,
			'modal'                => $modal,
			'harga_satuan'         => $harga_satuan,
			'subtotal'             => $subtotal,
			'id_pegawai_pembuatan' => $id_pegawai,
			'tanggal_pembuatan'    => date('Y-m-d H:i:s')
		);
		return $this->db->insert('vamr4846_vama.hadiah_detail', $dt);
	}

	function update_detail($id_hadiah_m, $id_barang_pusat, $jumlah_hadiah, 
						   $modal, $harga_satuan, $subtotal, $jumlah_hadiah_lama, $id_pegawai)
	{
		$sql = "UPDATE vamr4846_vama.barang_pusat SET total_stok = total_stok + ".$jumlah_hadiah_lama." 
				WHERE id_barang_pusat = '".$id_barang_pusat."'";
		$this->db->query($sql);
		
		$dt = array(
			'jumlah_hadiah'          => $jumlah_hadiah,
			'modal'                  => $modal,
			'harga_satuan'           => $harga_satuan,
			'subtotal'               => $subtotal,
			'id_pegawai_pembaharuan' => $id_pegawai
		);

		$where = array(
			'id_hadiah_m'     => $id_hadiah_m,
			'id_barang_pusat' => $id_barang_pusat
		);
		return $this->db->update('vamr4846_vama.hadiah_detail', $dt, $where);
	}

	function get_detail_faktur($id_hadiah_m)
	{
		$this->db->select('
			hd.id_hadiah_d, 
			hd.harga_satuan, hd.jumlah_hadiah, hd.subtotal,
			bp.sku, bp.kode_barang, bp.nama_barang
		');
		$this->db->from('vamr4846_vama.hadiah_detail AS hd');
		$this->db->join('vamr4846_vama.barang_pusat as bp', 'bp.id_barang_pusat=hd.id_barang_pusat');
		$this->db->where('hd.id_hadiah_m', $id_hadiah_m);
		$this->db->order_by('bp.nama_barang','ASC');
		$query = $this->db->get();
		return $query->result();
	}

	function get_total($id_hadiah_m)
	{
		$this->db->select('
			hd.id_hadiah_m,
			IFNULL(COUNT(hd.id_hadiah_d),0) jumlah_barang,
			IFNULL(ROUND(SUM(hd.harga_satuan*hd.jumlah_hadiah),0),0) total,
			hm.status_hadiah
		');
		$this->db->from('vamr4846_vama.hadiah_detail AS hd');
		$this->db->join('vamr4846_vama.hadiah_master AS hm', 'hd.id_hadiah_m=hm.id_hadiah_m');
		$this->db->where('hd.id_hadiah_m', $id_hadiah_m);
		$query = $this->db->get();
		return $query->row();
	}

	function hapus_hadiah_detail($id_hadiah_d, $id_hadiah_m, $id_customer_pusat, $no_hadiah, $nama_hadiah, $id_barang_pusat, 
								 $modal, $harga_satuan, $jumlah_hadiah, $subtotal,
								 $id_pegawai_pembuatan, $id_pegawai_pembaharuan, $id_pegawai, 
								 $tanggal_pembuatan, $tanggal_pembaharuan, $keterangan_batal)
	{
		// Kembalikan stok berdasarkan jumlah beli
		$sql = "UPDATE vamr4846_vama.barang_pusat SET total_stok = total_stok + ".$this->db->escape_like_str($jumlah_hadiah)." 
				WHERE id_barang_pusat = '".$this->db->escape_like_str($id_barang_pusat)."' ";
		$this->db->query($sql);

		// Simpan hadiah detail ke hadiah detail batal
		$sql = "INSERT INTO `vamr4846_vama.hadiah_detail_batal`(
					id_hadiah_d, id_hadiah_m, id_customer_pusat, no_hadiah, nama_hadiah, id_barang_pusat, 
					modal, harga_satuan, jumlah_hadiah, subtotal, 
					id_pegawai_pembuatan, id_pegawai_pembaharuan, id_pegawai_pembatalan, 
					tanggal_pembuatan, tanggal_pembaharuan, keterangan_batal
				) 
				VALUES(
					'".$id_hadiah_d."', '".$id_hadiah_m."', '".$id_customer_pusat."', '".$no_hadiah."', '".$nama_hadiah."', 
					'".$id_barang_pusat."', '".$modal."', '".$harga_satuan."', '".$jumlah_hadiah."', '".$subtotal."', 
					'".$id_pegawai_pembuatan."', '".$id_pegawai_pembaharuan."', '".$id_pegawai."', 
					'".$tanggal_pembuatan."', '".$tanggal_pembaharuan."', '".$keterangan_batal."'
				)";
		$this->db->query($sql);

		// Hapus hadiah detail
		return $this->db
					->where('id_hadiah_d', $id_hadiah_d)
					->delete('vamr4846_vama.hadiah_detail');
	}
}
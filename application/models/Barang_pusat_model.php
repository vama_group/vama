<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Barang_pusat_model extends MY_Model {
	var $table = 'vamr4846_vama.barang_pusat';

	// ---------------------------------------------------------------------------------------------------------------------------------
	// Master Barang
	var $column_order 		= array('bp.id_barang_pusat', 'bp.id_barang_pusat', 'bp.sku', 'bp.nama_barang', 
							  		'bp.total_stok', 'bp.total_stok_rusak', 'bp.harga_eceran','bp.harga_grosir1','bp.harga_grosir2', 'bp.status', 
							  	    'pp1.nama_pegawai', 'bp.tanggal_pembuatan', 'pp2.nama_pegawai', 'bp.tanggal_pembaharuan');
	var $column_search 		= array('bp.sku', 'bp.nama_barang', 'sp.nama_supplier', 'bp.kode_supplier');
	var $order 				= array('bp.nama_barang' => 'ASC');
	// ---------------------------------------------------------------------------------------------------------------------------------
	
	// Daftar Transaksi Jual	
	var $column_order_tr 	= array('bp.id_barang_pusat','sku','bp.nama_barang','bp.harga_eceran', 
								    'so', 'pb', 'dp', 'rpjt', 'rbdt', 'pj', 'hd', 'up', 'rpbt', 'bp.total_stok');
	var $column_search_tr	= array('bp.sku', 'bp.nama_barang', 'sp.nama_supplier');
	var $order_tr 			= array('bp.nama_barang' => 'ASC');
	// ---------------------------------------------------------------------------------------------------------------------------------

	// Daftar Transaksi Rusak
	var $column_order_trr 	= array('bp.id_barang_pusat','bp.sku','bp.nama_barang', 'bp.harga_eceran', 
								    'rpr','rbdtr', 'rpbr', 'bp.total_stok_rusak');
	var $column_search_trr	= array('bp.sku', 'bp.nama_barang', 'sp.nama_supplier');
	var $order_trr 			= array('bp.nama_barang' => 'ASC');
	// ---------------------------------------------------------------------------------------------------------------------------------

	// Pembaharuan Barang
	var $column_order_pb 	= array('kode_barang','sku','nama_barang','total_stok','harga_eceran');
	var $column_search_pb	= array('bp.sku', 'bp.nama_barang','bp.total_stok','bp.harga_eceran','sp.nama_supplier');
	var $order_pb 			= array('bp.nama_barang' => 'ASC');
	// ---------------------------------------------------------------------------------------------------------------------------------

	// Periksa stok
	var $column_order_ps  = array('bp.id_barang_pusat', 'bp.id_barang_pusat',
								  'bt.sku', 'bp.nama_barang', 'bt.harga_eceran',
								  'total_stok_fix', 'bp.total_stok', 'koreksi',
								  'bp.status');
	var $column_search_ps = array('bp.sku', 'bp.nama_barang'); 
	var $order_ps         = array('koreksi' => 'ASC');
	// ---------------------------------------------------------------------------------------------------------------------------------

	// Awal periksa stok
	public function periksa_stok($tanggal_awal, $tanggal_akhir)
	{		
		$this->db->select('
			nama_supplier, bp.id_barang_pusat, sku, nama_barang, bp.foto, status, status2,
			modal, resistensi_modal, modal_bersih,
			harga_eceran, harga_grosir1, harga_grosir2,
			IFNULL(total_stok,0) AS total_stok, IFNULL(total_stok_rusak,0) AS total_stok_rusak,
			bp.tanggal_pembuatan, bp.tanggal_pembaharuan,
			pp1.nama_pegawai AS pegawai_save, pp2.nama_pegawai As pegawai_edit,
			IFNULL(
				IFNULL(dsoj.jumlah_so_jual,0) + 
				IFNULL(dpb.jumlah_beli,0) + 
				IFNULL(ddp.jumlah_perakitan,0) + 
				IFNULL(drpjt.jumlah_retur,0) +
				IFNULL(drbdt.jumlah_masuk,0) -
				IFNULL(hd.jumlah_hadiah,0) -
				IFNULL(dup.jumlah_komponen,0) - 
				IFNULL(dpj.jumlah_beli, 0) -
				IFNULL(drpbt.jumlah_retur,0)
			, 0) AS total_stok_fix,
			IFNULL(
				IFNULL(dsoj.jumlah_so_jual,0) + 
				IFNULL(dpb.jumlah_beli,0) + 
				IFNULL(ddp.jumlah_perakitan,0) + 
				IFNULL(drpjt.jumlah_retur,0) +
				IFNULL(drbdt.jumlah_masuk,0) -
				IFNULL(hd.jumlah_hadiah,0) -
				IFNULL(dup.jumlah_komponen,0) - 
				IFNULL(dpj.jumlah_beli, 0) -
				IFNULL(drpbt.jumlah_retur,0)
			, 0) - bp.total_stok AS koreksi,
			IFNULL(
				IFNULL(dsor.jumlah_so_rusak,0) +
				IFNULL(rpr.jumlah_retur,0) - 
				IFNULL(rpbr.jumlah_retur,0)
			, 0) AS total_stok_rusak_fix
		');
		$this->db->from('vamr4846_vama.barang_pusat AS bp');
		$this->db->join('vamr4846_vama.supplier AS sp','sp.kode_supplier=bp.kode_supplier','LEFT');
		// ------------------------------------------------------------------------------------------------------------------------
		// Untuk stok jual
		$this->db->join('(SELECT id_barang_pusat, SUM(jumlah_so) AS jumlah_so_jual FROM vamr4846_vama.stok_opname_pusat  
						  WHERE DATE(tanggal_pembuatan) BETWEEN "'.$tanggal_awal.'" AND "'.$tanggal_akhir.'" 
						  AND masuk_stok="JUAL"
						  GROUP BY id_barang_pusat) AS dsoj', 'dsoj.id_barang_pusat=bp.id_barang_pusat', 'LEFT');
		$this->db->join('(SELECT id_barang_pusat, SUM(jumlah_beli) AS jumlah_beli FROM vamr4846_vama.pembelian_detail 
						  WHERE DATE(tanggal_pembuatan) BETWEEN "'.$tanggal_awal.'" AND "'.$tanggal_akhir.'" 
						  GROUP BY id_barang_pusat) AS dpb', 'dpb.id_barang_pusat=bp.id_barang_pusat', 'LEFT');
		$this->db->join('(SELECT id_barang_pusat, SUM(jumlah_perakitan) AS jumlah_perakitan FROM vamr4846_vama.perakitan_master 
						  WHERE DATE(tanggal_pembuatan) BETWEEN "'.$tanggal_awal.'" AND "'.$tanggal_akhir.'" 
						  GROUP BY id_barang_pusat) AS ddp', 'ddp.id_barang_pusat=bp.id_barang_pusat', 'LEFT');
		$this->db->join('(SELECT id_barang_pusat, SUM(jumlah_retur) AS jumlah_retur FROM vamr4846_vama.retur_penjualan_detail 
						  WHERE masuk_stok="JUAL" AND DATE(tanggal_pembuatan) BETWEEN "'.$tanggal_awal.'" AND "'.$tanggal_akhir.'" 
						  GROUP BY id_barang_pusat) AS drpjt', 'drpjt.id_barang_pusat=bp.id_barang_pusat', 'LEFT');
		$this->db->join('(SELECT id_barang_pusat, SUM(jumlah_hadiah) AS jumlah_hadiah FROM vamr4846_vama.hadiah_detail 
						  WHERE DATE(tanggal_pembuatan) BETWEEN "'.$tanggal_awal.'" AND "'.$tanggal_akhir.'" 
						  GROUP BY id_barang_pusat) AS hd', 'hd.id_barang_pusat=bp.id_barang_pusat', 'LEFT');
		$this->db->join('(SELECT id_barang_pusat, SUM(jumlah_komponen) AS jumlah_komponen FROM vamr4846_vama.perakitan_detail 
						  WHERE DATE(tanggal_pembuatan) BETWEEN "'.$tanggal_awal.'" AND "'.$tanggal_akhir.'" 
						  GROUP BY id_barang_pusat) AS dup', 'dup.id_barang_pusat=bp.id_barang_pusat', 'LEFT');
		$this->db->join('(SELECT id_barang_pusat, SUM(jumlah_beli) AS jumlah_beli FROM vamr4846_vama.penjualan_detail 
						  WHERE DATE(tanggal_pembuatan) BETWEEN "'.$tanggal_awal.'" AND "'.$tanggal_akhir.'" 
						  GROUP BY id_barang_pusat) AS dpj', 'dpj.id_barang_pusat=bp.id_barang_pusat', 'LEFT');
		$this->db->join('(SELECT id_barang_pusat, SUM(jumlah_retur) AS jumlah_retur FROM vamr4846_vama.retur_pembelian_detail 
						  WHERE dari_stok="JUAL" AND DATE(tanggal_pembuatan) BETWEEN "'.$tanggal_awal.'" AND "'.$tanggal_akhir.'" 
						  GROUP BY id_barang_pusat) AS drpbt', 'drpbt.id_barang_pusat=bp.id_barang_pusat', 'LEFT');

		$this->db->join('(SELECT id_barang_pusat, SUM(jumlah_masuk) AS jumlah_masuk FROM vamr4846_vama.retur_dari_toko 
						  WHERE kondisi_barang="BAIK" AND DATE(tanggal_pembuatan) BETWEEN "'.$tanggal_awal.'" AND "'.$tanggal_akhir.'" 
						  GROUP BY id_barang_pusat) AS drbdt', 'drbdt.id_barang_pusat=bp.id_barang_pusat', 'LEFT');

		$this->db->join('vamr4846_vama.pegawai_pusat AS pp1', 'pp1.id_pegawai=bp.id_pegawai_pembuatan', 'LEFT');
		$this->db->join('vamr4846_vama.pegawai_pusat AS pp2', 'pp2.id_pegawai=bp.id_pegawai_pembaharuan', 'LEFT');
		// ------------------------------------------------------------------------------------------------------------------------
		// Untuk stok rusak
		$this->db->join('(SELECT id_barang_pusat, SUM(jumlah_so) AS jumlah_so_rusak FROM vamr4846_vama.stok_opname_pusat  
						  WHERE DATE(tanggal_pembuatan) BETWEEN "'.$tanggal_awal.'" AND "'.$tanggal_akhir.'" 
						  AND masuk_stok="RUSAK"
						  GROUP BY id_barang_pusat) AS dsor', 'dsor.id_barang_pusat=bp.id_barang_pusat', 'LEFT');
		$this->db->join('(SELECT id_barang_pusat, SUM(jumlah_masuk) AS jumlah_masuk FROM vamr4846_vama.retur_dari_toko 
						  WHERE kondisi_barang="BAIK" AND DATE(tanggal_pembuatan) BETWEEN "'.$tanggal_awal.'" AND "'.$tanggal_akhir.'" 
						  GROUP BY id_barang_pusat) AS drbdtr', 'drbdtr.id_barang_pusat=bp.id_barang_pusat', 'LEFT');
		$this->db->join('(SELECT id_barang_pusat, SUM(jumlah_retur) AS jumlah_retur FROM vamr4846_vama.retur_penjualan_detail 
						  WHERE masuk_stok="RUSAK" AND DATE(tanggal_pembuatan) BETWEEN "'.$tanggal_awal.'" AND "'.$tanggal_akhir.'" 
						  GROUP BY id_barang_pusat) AS rpr', 'rpr.id_barang_pusat=bp.id_barang_pusat', 'LEFT');
		$this->db->join('(SELECT id_barang_pusat, SUM(jumlah_retur) AS jumlah_retur FROM vamr4846_vama.retur_pembelian_detail 
						  WHERE dari_stok="RUSAK" AND DATE(tanggal_pembuatan) BETWEEN "'.$tanggal_awal.'" AND "'.$tanggal_akhir.'" 
						  GROUP BY id_barang_pusat) AS rpbr', 'rpbr.id_barang_pusat=bp.id_barang_pusat', 'LEFT');
		// ------------------------------------------------------------------------------------------------------------------------
		$this->db->where('bp.status_hapus','TIDAK');
		$this->db->where('
			IFNULL(
				IFNULL(dsoj.jumlah_so_jual,0) + 
				IFNULL(dpb.jumlah_beli,0) + 
				IFNULL(ddp.jumlah_perakitan,0) + 
				IFNULL(drpjt.jumlah_retur,0) +
				IFNULL(drbdt.jumlah_masuk,0) -
				IFNULL(hd.jumlah_hadiah,0) -
				IFNULL(dup.jumlah_komponen,0) - 
				IFNULL(dpj.jumlah_beli, 0) -
				IFNULL(drpbt.jumlah_retur,0)
			, 0) - bp.total_stok <>
		', '0');

		$query = $this->db->get();
		return $query->result();
	}
	// Akhir periksa stok

	// Awal datatable periksa stok
	private function _get_datatables_query_periksa($tanggal_awal, $tanggal_akhir)
	{		
		$this->db->select('
			nama_supplier, bp.id_barang_pusat, sku, nama_barang, bp.foto, status, status2,
			modal, resistensi_modal, modal_bersih,
			harga_eceran, harga_grosir1, harga_grosir2,
			IFNULL(total_stok,0) AS total_stok, IFNULL(total_stok_rusak,0) AS total_stok_rusak,
			bp.tanggal_pembuatan, bp.tanggal_pembaharuan,
			pp1.nama_pegawai AS pegawai_save, pp2.nama_pegawai As pegawai_edit,
			IFNULL(
				IFNULL(dsoj.jumlah_so_jual,0) + 
				IFNULL(dpb.jumlah_beli,0) + 
				IFNULL(ddp.jumlah_perakitan,0) + 
				IFNULL(drpjt.jumlah_retur,0) +
				IFNULL(drbdt.jumlah_masuk,0) -
				IFNULL(hd.jumlah_hadiah,0) -
				IFNULL(dup.jumlah_komponen,0) - 
				IFNULL(dpj.jumlah_beli, 0) -
				IFNULL(drpbt.jumlah_retur,0)
			, 0) AS total_stok_fix,
			IFNULL(
				IFNULL(dsoj.jumlah_so_jual,0) + 
				IFNULL(dpb.jumlah_beli,0) + 
				IFNULL(ddp.jumlah_perakitan,0) + 
				IFNULL(drpjt.jumlah_retur,0) +
				IFNULL(drbdt.jumlah_masuk,0) -
				IFNULL(hd.jumlah_hadiah,0) -
				IFNULL(dup.jumlah_komponen,0) - 
				IFNULL(dpj.jumlah_beli, 0) -
				IFNULL(drpbt.jumlah_retur,0)
			, 0) - bp.total_stok AS koreksi,
			IFNULL(
				IFNULL(dsor.jumlah_so_rusak,0) +
				IFNULL(rpr.jumlah_retur,0) +
				IFNULL(drbdtr.jumlah_masuk,0) - 
				IFNULL(rpbr.jumlah_retur,0)
			, 0) AS total_stok_rusak_fix
		');
		$this->db->from('vamr4846_vama.barang_pusat AS bp');
		$this->db->join('vamr4846_vama.supplier AS sp','sp.kode_supplier=bp.kode_supplier','LEFT');
		// ------------------------------------------------------------------------------------------------------------------------
		// Untuk stok jual
		$this->db->join('(SELECT id_barang_pusat, SUM(jumlah_so) AS jumlah_so_jual FROM vamr4846_vama.stok_opname_pusat  
						  WHERE DATE(tanggal_pembuatan) BETWEEN "'.$tanggal_awal.'" AND "'.$tanggal_akhir.'" 
						  AND masuk_stok="JUAL"
						  GROUP BY id_barang_pusat) AS dsoj', 'dsoj.id_barang_pusat=bp.id_barang_pusat', 'LEFT');
		$this->db->join('(SELECT id_barang_pusat, SUM(jumlah_beli) AS jumlah_beli FROM vamr4846_vama.pembelian_detail 
						  WHERE DATE(tanggal_pembuatan) BETWEEN "'.$tanggal_awal.'" AND "'.$tanggal_akhir.'" 
						  GROUP BY id_barang_pusat) AS dpb', 'dpb.id_barang_pusat=bp.id_barang_pusat', 'LEFT');
		$this->db->join('(SELECT id_barang_pusat, SUM(jumlah_perakitan) AS jumlah_perakitan FROM vamr4846_vama.perakitan_master 
						  WHERE DATE(tanggal_pembuatan) BETWEEN "'.$tanggal_awal.'" AND "'.$tanggal_akhir.'" 
						  GROUP BY id_barang_pusat) AS ddp', 'ddp.id_barang_pusat=bp.id_barang_pusat', 'LEFT');
		$this->db->join('(SELECT id_barang_pusat, SUM(jumlah_retur) AS jumlah_retur FROM vamr4846_vama.retur_penjualan_detail 
						  WHERE masuk_stok="JUAL" AND DATE(tanggal_pembuatan) BETWEEN "'.$tanggal_awal.'" AND "'.$tanggal_akhir.'" 
						  GROUP BY id_barang_pusat) AS drpjt', 'drpjt.id_barang_pusat=bp.id_barang_pusat', 'LEFT');
		$this->db->join('(SELECT id_barang_pusat, SUM(jumlah_hadiah) AS jumlah_hadiah FROM vamr4846_vama.hadiah_detail 
						  WHERE DATE(tanggal_pembuatan) BETWEEN "'.$tanggal_awal.'" AND "'.$tanggal_akhir.'" 
						  GROUP BY id_barang_pusat) AS hd', 'hd.id_barang_pusat=bp.id_barang_pusat', 'LEFT');
		$this->db->join('(SELECT id_barang_pusat, SUM(jumlah_komponen) AS jumlah_komponen FROM vamr4846_vama.perakitan_detail 
						  WHERE DATE(tanggal_pembuatan) BETWEEN "'.$tanggal_awal.'" AND "'.$tanggal_akhir.'" 
						  GROUP BY id_barang_pusat) AS dup', 'dup.id_barang_pusat=bp.id_barang_pusat', 'LEFT');
		$this->db->join('(SELECT id_barang_pusat, SUM(jumlah_beli) AS jumlah_beli FROM vamr4846_vama.penjualan_detail 
						  WHERE DATE(tanggal_pembuatan) BETWEEN "'.$tanggal_awal.'" AND "'.$tanggal_akhir.'" 
						  GROUP BY id_barang_pusat) AS dpj', 'dpj.id_barang_pusat=bp.id_barang_pusat', 'LEFT');
		$this->db->join('(SELECT id_barang_pusat, SUM(jumlah_retur) AS jumlah_retur FROM vamr4846_vama.retur_pembelian_detail 
						  WHERE dari_stok="JUAL" AND DATE(tanggal_pembuatan) BETWEEN "'.$tanggal_awal.'" AND "'.$tanggal_akhir.'" 
						  GROUP BY id_barang_pusat) AS drpbt', 'drpbt.id_barang_pusat=bp.id_barang_pusat', 'LEFT');

		$this->db->join('(SELECT id_barang_pusat, SUM(jumlah_masuk) AS jumlah_masuk FROM vamr4846_vama.retur_dari_toko 
						  WHERE kondisi_barang="BAIK" AND DATE(tanggal_pembuatan) BETWEEN "'.$tanggal_awal.'" AND "'.$tanggal_akhir.'" 
						  GROUP BY id_barang_pusat) AS drbdt', 'drbdt.id_barang_pusat=bp.id_barang_pusat', 'LEFT');

		$this->db->join('vamr4846_vama.pegawai_pusat AS pp1', 'pp1.id_pegawai=bp.id_pegawai_pembuatan', 'LEFT');
		$this->db->join('vamr4846_vama.pegawai_pusat AS pp2', 'pp2.id_pegawai=bp.id_pegawai_pembaharuan', 'LEFT');
		// ------------------------------------------------------------------------------------------------------------------------
		// Untuk stok rusak
		$this->db->join('(SELECT id_barang_pusat, SUM(jumlah_so) AS jumlah_so_rusak FROM vamr4846_vama.stok_opname_pusat  
						  WHERE DATE(tanggal_pembuatan) BETWEEN "'.$tanggal_awal.'" AND "'.$tanggal_akhir.'" 
						  AND masuk_stok="RUSAK"
						  GROUP BY id_barang_pusat) AS dsor', 'dsor.id_barang_pusat=bp.id_barang_pusat', 'LEFT');
		$this->db->join('(SELECT id_barang_pusat, SUM(jumlah_retur) AS jumlah_retur FROM vamr4846_vama.retur_penjualan_detail 
						  WHERE masuk_stok="RUSAK" AND DATE(tanggal_pembuatan) BETWEEN "'.$tanggal_awal.'" AND "'.$tanggal_akhir.'" 
						  GROUP BY id_barang_pusat) AS rpr', 'rpr.id_barang_pusat=bp.id_barang_pusat', 'LEFT');
		$this->db->join('(SELECT id_barang_pusat, SUM(jumlah_retur) AS jumlah_retur FROM vamr4846_vama.retur_pembelian_detail 
						  WHERE dari_stok="RUSAK" AND DATE(tanggal_pembuatan) BETWEEN "'.$tanggal_awal.'" AND "'.$tanggal_akhir.'" 
						  GROUP BY id_barang_pusat) AS rpbr', 'rpbr.id_barang_pusat=bp.id_barang_pusat', 'LEFT');
		$this->db->join('(SELECT id_barang_pusat, SUM(jumlah_masuk) AS jumlah_masuk FROM vamr4846_vama.retur_dari_toko 
						  WHERE kondisi_barang="RUSAK" AND DATE(tanggal_pembuatan) BETWEEN "'.$tanggal_awal.'" AND "'.$tanggal_akhir.'" 
						  GROUP BY id_barang_pusat) AS drbdtr', 'drbdtr.id_barang_pusat=bp.id_barang_pusat', 'LEFT');
		// ------------------------------------------------------------------------------------------------------------------------
		$this->db->where('bp.status_hapus','TIDAK');
		$this->db->where('
			IFNULL(
				IFNULL(dsoj.jumlah_so_jual,0) + 
				IFNULL(dpb.jumlah_beli,0) + 
				IFNULL(ddp.jumlah_perakitan,0) + 
				IFNULL(drpjt.jumlah_retur,0) +
				IFNULL(drbdt.jumlah_masuk,0) -
				IFNULL(hd.jumlah_hadiah,0) -
				IFNULL(dup.jumlah_komponen,0) - 
				IFNULL(dpj.jumlah_beli, 0) -
				IFNULL(drpbt.jumlah_retur,0)
			, 0) - bp.total_stok <>
		', '0');
		// $this->db->where('bp.status','AKTIF');

		$i = 0;
	
		foreach ($this->column_search_ps as $item){
			if($_POST['search']['value']){
				if($i===0){
					$this->db->group_start();
					$this->db->like($item, $_POST['search']['value']);
				}else{
					$this->db->or_like($item, $_POST['search']['value']);
				}

				if(count($this->column_search_ps) - 1 == $i)
				$this->db->group_end();
			}
			$i++;
		}
		
		if(isset($_POST['order'])){
			$this->db->order_by($this->column_order_ps[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		}else if(isset($this->order_ps)){
			$order = $this->order_ps;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}
	// Akhir datatable periksa stok

	// Awal query tidak realtime stok
	private function _get_datatables_query($tanggal_awal, $tanggal_akhir)
	{		
		$this->db->select('
			nama_supplier, bp.id_barang_pusat, sku, nama_barang, bp.foto, status, status2,
			modal, resistensi_modal, modal_bersih,
			harga_eceran, harga_grosir1, harga_grosir2,
			IFNULL(total_stok,0) AS total_stok, IFNULL(total_stok_rusak,0) AS total_stok_rusak,
			bp.tanggal_pembuatan, bp.tanggal_pembaharuan,
			pp1.nama_pegawai AS pegawai_save, pp2.nama_pegawai As pegawai_edit
		');
		$this->db->from('vamr4846_vama.barang_pusat AS bp');
		$this->db->join('vamr4846_vama.supplier AS sp','sp.kode_supplier=bp.kode_supplier','LEFT');
		$this->db->join('vamr4846_vama.pegawai_pusat AS pp1', 'pp1.id_pegawai=bp.id_pegawai_pembuatan', 'LEFT');
		$this->db->join('vamr4846_vama.pegawai_pusat AS pp2', 'pp2.id_pegawai=bp.id_pegawai_pembaharuan', 'LEFT');
		$this->db->where('bp.status_hapus','TIDAK');
		// $this->db->where('bp.status','AKTIF');

		$i = 0;
		foreach ($this->column_search as $item){
			if($_POST['search']['value']){
				if($i===0){
					$this->db->group_start();
					$this->db->like($item, $_POST['search']['value']);
				}else{
					$this->db->or_like($item, $_POST['search']['value']);
				}

				if(count($this->column_search) - 1 == $i)
				$this->db->group_end();
			}
			$i++;
		}
		
		if(isset($_POST['order'])){
			$this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		}else if(isset($this->order)){
			$order = $this->order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}
	// Akhir query tidka realtime stok
	// Akhir datatable master barang

	// Awal datatable daftar transaksi
	private function _get_datatables_query_transaksi($tanggal_awal, $tanggal_akhir)
	{		
		$this->db->select('
			bp.id_barang_pusat, bp.sku, bp.nama_barang, bp.harga_eceran, 
			IFNULL(dso.jumlah_so,0) AS so, 
			IFNULL(dpb.jumlah_beli,0) AS pb, 
			IFNULL(ddp.jumlah_perakitan,0) AS dp, 
			IFNULL(drpjt.jumlah_retur,0) AS rpjt,
			IFNULL(hd.jumlah_hadiah,0) AS hd, 
			IFNULL(drbdt.jumlah_masuk,0) AS rbdt, 
			IFNULL(dup.jumlah_komponen,0) AS up, 
			IFNULL(dpj.jumlah_beli,0) AS pj,
			IFNULL(drpbt.jumlah_retur,0) AS rpbt,
			bp.total_stok, bp.total_stok_rusak,
			IFNULL(
				IFNULL(dso.jumlah_so,0) + 
				IFNULL(dpb.jumlah_beli,0) + 
				IFNULL(ddp.jumlah_perakitan,0) + 
				IFNULL(drpjt.jumlah_retur,0) +
				IFNULL(drbdt.jumlah_masuk,0) -
				IFNULL(hd.jumlah_hadiah,0) -
				IFNULL(dup.jumlah_komponen,0) - 
				IFNULL(dpj.jumlah_beli, 0) -
				IFNULL(drpbt.jumlah_retur,0)
			, 0) AS total_stok_fix
		');
		$this->db->from('vamr4846_vama.barang_pusat AS bp');
		$this->db->join('vamr4846_vama.supplier AS sp', 'sp.kode_supplier=bp.kode_supplier');
		$this->db->join('(SELECT id_barang_pusat, SUM(jumlah_so) AS jumlah_so FROM vamr4846_vama.stok_opname_pusat  
						  WHERE DATE(tanggal_pembuatan) BETWEEN "'.$tanggal_awal.'" AND "'.$tanggal_akhir.'" 
						  GROUP BY id_barang_pusat) AS dso', 'dso.id_barang_pusat=bp.id_barang_pusat', 'LEFT');
		$this->db->join('(SELECT id_barang_pusat, SUM(jumlah_beli) AS jumlah_beli FROM vamr4846_vama.pembelian_detail 
						  WHERE DATE(tanggal_pembuatan) BETWEEN "'.$tanggal_awal.'" AND "'.$tanggal_akhir.'" 
						  GROUP BY id_barang_pusat) AS dpb', 'dpb.id_barang_pusat=bp.id_barang_pusat', 'LEFT');
		$this->db->join('(SELECT id_barang_pusat, SUM(jumlah_perakitan) AS jumlah_perakitan FROM vamr4846_vama.perakitan_master 
						  WHERE DATE(tanggal_pembuatan) BETWEEN "'.$tanggal_awal.'" AND "'.$tanggal_akhir.'" 
						  GROUP BY id_barang_pusat) AS ddp', 'ddp.id_barang_pusat=bp.id_barang_pusat', 'LEFT');
		$this->db->join('(SELECT id_barang_pusat, SUM(jumlah_retur) AS jumlah_retur FROM vamr4846_vama.retur_penjualan_detail 
						  WHERE masuk_stok="JUAL" AND DATE(tanggal_pembuatan) BETWEEN "'.$tanggal_awal.'" AND "'.$tanggal_akhir.'" 
						  GROUP BY id_barang_pusat) AS drpjt', 'drpjt.id_barang_pusat=bp.id_barang_pusat', 'LEFT');

		$this->db->join('(SELECT id_barang_pusat, SUM(jumlah_masuk) AS jumlah_masuk FROM vamr4846_vama.retur_dari_toko 
						  WHERE kondisi_barang="BAIK" AND DATE(tanggal_pembuatan) BETWEEN "'.$tanggal_awal.'" AND "'.$tanggal_akhir.'" 
						  GROUP BY id_barang_pusat) AS drbdt', 'drbdt.id_barang_pusat=bp.id_barang_pusat', 'LEFT');

		$this->db->join('(SELECT id_barang_pusat, SUM(jumlah_hadiah) AS jumlah_hadiah FROM vamr4846_vama.hadiah_detail 
						  WHERE DATE(tanggal_pembuatan) BETWEEN "'.$tanggal_awal.'" AND "'.$tanggal_akhir.'" 
						  GROUP BY id_barang_pusat) AS hd', 'hd.id_barang_pusat=bp.id_barang_pusat', 'LEFT');
		$this->db->join('(SELECT id_barang_pusat, SUM(jumlah_komponen) AS jumlah_komponen FROM vamr4846_vama.perakitan_detail 
						  WHERE DATE(tanggal_pembuatan) BETWEEN "'.$tanggal_awal.'" AND "'.$tanggal_akhir.'" 
						  GROUP BY id_barang_pusat) AS dup', 'dup.id_barang_pusat=bp.id_barang_pusat', 'LEFT');
		$this->db->join('(SELECT id_barang_pusat, SUM(jumlah_beli) AS jumlah_beli FROM vamr4846_vama.penjualan_detail 
						  WHERE DATE(tanggal_pembuatan) BETWEEN "'.$tanggal_awal.'" AND "'.$tanggal_akhir.'" 
						  GROUP BY id_barang_pusat) AS dpj', 'dpj.id_barang_pusat=bp.id_barang_pusat', 'LEFT');
		$this->db->join('(SELECT id_barang_pusat, SUM(jumlah_retur) AS jumlah_retur FROM vamr4846_vama.retur_pembelian_detail 
						  WHERE dari_stok="JUAL" AND DATE(tanggal_pembuatan) BETWEEN "'.$tanggal_awal.'" AND "'.$tanggal_akhir.'" 
						  GROUP BY id_barang_pusat) AS drpbt', 'drpbt.id_barang_pusat=bp.id_barang_pusat', 'LEFT');
		$this->db->where('bp.status_hapus','TIDAK');

		$i = 0;
	
		foreach ($this->column_search_tr as $item){
			if($_POST['search']['value']){
				if($i===0){
					$this->db->group_start();
					$this->db->like($item, $_POST['search']['value']);
				}else{
					$this->db->or_like($item, $_POST['search']['value']);
				}

				if(count($this->column_search_tr) - 1 == $i)
				$this->db->group_end();
			}
			$i++;
		}
		
		if(isset($_POST['order'])){
			$this->db->order_by($this->column_order_tr[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		}else if(isset($this->order_tr)){
			$order = $this->order_tr;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}
	// Akhir datatable daftar transaksi

	// Awal datatable daftar transaksi rusak
	private function _get_datatables_query_transaksi_rusak($tanggal_awal, $tanggal_akhir)
	{		
		$this->db->select('
			bp.id_barang_pusat, bp.sku, bp.nama_barang, bp.harga_eceran, 
			IFNULL(rpr.jumlah_retur,0) AS rpr, 
			IFNULL(drbdtr.jumlah_masuk,0) AS rbdtr, 
			IFNULL(rpbr.jumlah_retur,0) AS rpbr,
			bp.total_stok, bp.total_stok_rusak,
			IFNULL(
				IFNULL(drbdtr.jumlah_masuk,0) +
				IFNULL(rpr.jumlah_retur,0) - 
				IFNULL(rpbr.jumlah_retur,0)
			, 0) AS total_stok_rusak_fix
		');
		$this->db->from('vamr4846_vama.barang_pusat AS bp');
		$this->db->join('vamr4846_vama.supplier AS sp', 'sp.kode_supplier=bp.kode_supplier');
		$this->db->join('(SELECT id_barang_pusat, SUM(jumlah_retur) AS jumlah_retur FROM vamr4846_vama.retur_penjualan_detail 
						  WHERE masuk_stok="RUSAK" AND DATE(tanggal_pembuatan) BETWEEN "'.$tanggal_awal.'" AND "'.$tanggal_akhir.'" 
						  GROUP BY id_barang_pusat) AS rpr', 'rpr.id_barang_pusat=bp.id_barang_pusat', 'LEFT');
		$this->db->join('(SELECT id_barang_pusat, SUM(jumlah_retur) AS jumlah_retur FROM vamr4846_vama.retur_pembelian_detail 
						  WHERE dari_stok="RUSAK" AND DATE(tanggal_pembuatan) BETWEEN "'.$tanggal_awal.'" AND "'.$tanggal_akhir.'" 
						  GROUP BY id_barang_pusat) AS rpbr', 'rpbr.id_barang_pusat=bp.id_barang_pusat', 'LEFT');
		$this->db->join('(SELECT id_barang_pusat, SUM(jumlah_masuk) AS jumlah_masuk FROM vamr4846_vama.retur_dari_toko 
						  WHERE kondisi_barang="RUSAK" AND DATE(tanggal_pembuatan) BETWEEN "'.$tanggal_awal.'" AND "'.$tanggal_akhir.'" 
						  GROUP BY id_barang_pusat) AS drbdtr', 'drbdtr.id_barang_pusat=bp.id_barang_pusat', 'LEFT');
		$this->db->where('bp.status_hapus','TIDAK');

		$i = 0;
	
		foreach ($this->column_search_trr as $item){
			if($_POST['search']['value']){
				if($i===0){
					$this->db->group_start();
					$this->db->like($item, $_POST['search']['value']);
				}else{
					$this->db->or_like($item, $_POST['search']['value']);
				}

				if(count($this->column_search_trr) - 1 == $i)
				$this->db->group_end();
			}
			$i++;
		}
		
		if(isset($_POST['order'])){
			$this->db->order_by($this->column_order_trr[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		}else if(isset($this->order_trr)){
			$order = $this->order_trr;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}
	// Akhir datatable daftar transaksi rusak

	// Awal datatable pembaharuan barang
	private function _get_datatables_query_pembaharuan_barang($tanggal_awal, $tanggal_akhir)
	{		
		$this->db->select('nama_supplier, id_barang_pusat, sku, nama_barang, bp.foto, STATUS, status2,
						   harga_eceran, harga_grosir1, harga_grosir2,
						   IFNULL(total_stok,0) AS total_stok, IFNULL(total_stok_rusak,0) AS total_stok_rusak,
						   bp.tanggal_pembuatan, bp.tanggal_pembaharuan, 
						   pp1.nama_pegawai AS pegawai_save, pp2.nama_pegawai As pegawai_edit');
		$this->db->from('vamr4846_vama.barang_pusat AS bp');
		$this->db->join('vamr4846_vama.supplier AS sp','sp.kode_supplier=bp.kode_supplier','LEFT');
		$this->db->join('vamr4846_vama.pegawai_pusat AS pp1', 'pp1.id_pegawai=bp.id_pegawai_pembuatan', 'LEFT');
		$this->db->join('vamr4846_vama.pegawai_pusat AS pp2', 'pp2.id_pegawai=bp.id_pegawai_pembaharuan', 'LEFT');
		$this->db->where(array(
			'bp.status_hapus' 					=> 'TIDAK',
			'DATE(bp.tanggal_pembaharuan) >='	=> $tanggal_awal,
			'DATE(bp.tanggal_pembaharuan) <='	=> $tanggal_akhir 
		));
		$i = 0;
	
		foreach ($this->column_search_pb as $item){
			if($_POST['search']['value']){
				if($i===0){
					$this->db->group_start();
					$this->db->like($item, $_POST['search']['value']);
				}else{
					$this->db->or_like($item, $_POST['search']['value']);
				}

				if(count($this->column_search_pb) - 1 == $i)
				$this->db->group_end();
			}
			$i++;
		}
		
		if(isset($_POST['order'])){
			$this->db->order_by($this->column_order_pb[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		}else if(isset($this->order_pb)){
			$order = $this->order_pb;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}
	// Akhir datatable pembaharuan barang

	function get_datatables($query, $tanggal_awal, $tanggal_akhir)
	{
		$this->$query($tanggal_awal, $tanggal_akhir);
		if($_POST['length'] != -1)
		$this->db->limit($_POST['length'], $_POST['start']);
		$query = $this->db->get();
		return $query->result();
	}

	function count_filtered($query, $tanggal_awal, $tanggal_akhir)
	{
		$this->$query($tanggal_awal, $tanggal_akhir);
		$query = $this->db->get();
		return $query->num_rows();
	}

	public function count_all()
	{
		$this->db->from('vamr4846_vama.barang_pusat');
		$this->db->where('status_hapus','TIDAK');
		return $this->db->count_all_results();
	}

	// Awal update stok
	public function update_stok_pusat($id_barang_pusat, $stok_fix, $id_pegawai){
		$sql = "
				UPDATE 
					vamr4846_vama.barang_pusat 
				SET 
					total_stok = '".$stok_fix."',
					id_pegawai_pembaharuan = '".$id_pegawai."'
				WHERE 
					id_barang_pusat = '".$id_barang_pusat."'";
		return $this->db->query($sql);
	}
	// Akhir update stok

	public function ambil_daftar_foto($sku)
	{
		$sql = "
			SELECT *
			FROM vamr4846_vama.foto
			WHERE (sku = '".$this->db->escape_like_str($sku)."') 
		";
		return $this->db->query($sql);
	}

	public function update_foto_profile($sku, $nama_foto)
	{
		$sql = "UPDATE vamr4846_vama.barang_pusat
				SET foto = '".$this->db->escape_like_str($nama_foto)."'
				WHERE sku = '".$this->db->escape_like_str($sku)."'";
		return $this->db->query($sql);
	}

	public function ambil_nama_foto($id_foto)
	{
		$this->db->select('*');
		$this->db->from('vamr4846_vama.foto');
		$this->db->where('id_foto', $id_foto);
		$query = $this->db->get();
		return $query->row();
	}

	function cari_kode($keyword, $id_pm)
	{
		$sql = "
			SELECT 
				bp.id_barang_pusat, bp.kode_barang, bp.sku, bp.nama_barang, bp.foto, bp.total_stok,
				bp.harga_eceran, bp.harga_grosir1, bp.harga_grosir2,
				FORMAT(bp.harga_eceran,0)eceran, FORMAT(bp.harga_grosir1,0)grosir1, FORMAT(bp.harga_grosir2,0)grosir2
			FROM 
				vamr4846_vama.barang_pusat AS bp
			WHERE 
				id_barang_pusat NOT IN (SELECT id_barang_pusat 
										FROM vamr4846_vama.penjualan_detail AS PD
										WHERE id_penjualan_m='".$this->db->escape_like_str($id_pm)."') 
				AND bp.status_hapus = 'tidak'
				AND bp.status 		= 'Aktif'
				AND bp.total_stok 	> 0 
				AND ( 
					bp.sku LIKE '%".$this->db->escape_like_str($keyword)."%' 
					OR bp.nama_barang LIKE '%".$this->db->escape_like_str($keyword)."%' 
				)
			LIMIT 10
		";
		return $this->db->query($sql);
	}

	function cari_kode_hadiah($keyword, $id_hm)
	{
		$sql = "
			SELECT 
				bp.id_barang_pusat, bp.kode_barang, bp.sku, bp.nama_barang, bp.foto, bp.total_stok,
				bp.harga_eceran, FORMAT(bp.harga_eceran,0) AS eceran
			FROM 
				vamr4846_vama.barang_pusat AS bp
			WHERE 
				id_barang_pusat NOT IN (SELECT id_barang_pusat 
										FROM vamr4846_vama.hadiah_detail AS HD 
										WHERE id_hadiah_m='".$this->db->escape_like_str($id_hm)."') 
				AND bp.status_hapus = 'tidak'
				AND bp.status 		= 'Aktif'
				AND bp.total_stok 	> 0 
				AND ( 
						bp.sku LIKE '%".$this->db->escape_like_str($keyword)."%' 
						OR bp.nama_barang LIKE '%".$this->db->escape_like_str($keyword)."%' 
				)
			LIMIT 50 
		";

		return $this->db->query($sql);
	}

	function cari_barang_sku_nama($keyword)
	{
		$sql = "
			SELECT 
				bp.id_barang_pusat, bp.kode_barang, bp.sku, bp.nama_barang, bp.foto, bp.total_stok,
				bp.harga_eceran, FORMAT(bp.harga_eceran,0) AS eceran, bp.status
			FROM 
				vamr4846_vama.barang_pusat AS bp
			WHERE 
				bp.status_hapus = 'TIDAK'
				AND ( 
					bp.sku LIKE '%".$this->db->escape_like_str($keyword)."%' 
					OR bp.nama_barang LIKE '%".$this->db->escape_like_str($keyword)."%' 
				) 
			LIMIT 50
		";

		return $this->db->query($sql);
	}

	function cari_kode_stok_opname($keyword, $id_periode_stok_pusat)
	{
		$sql = "
			SELECT 
				bp.id_barang_pusat, bp.kode_barang, bp.sku, bp.nama_barang, bp.foto, bp.total_stok,
				bp.harga_eceran, FORMAT(bp.harga_eceran,0) AS eceran, bp.status,
				IFNULL(so.jumlah_so, '0') AS jumlah_so
			FROM 
				vamr4846_vama.barang_pusat AS bp
			LEFT JOIN 
				vamr4846_vama.stok_opname_pusat AS so 
				ON so.id_barang_pusat = bp.id_barang_pusat AND 
				so.id_periode_stok_pusat = '".$id_periode_stok_pusat."'
			WHERE 
				bp.status_hapus = 'TIDAK'
				AND ( 
					bp.sku LIKE '%".$this->db->escape_like_str($keyword)."%' 
					OR bp.nama_barang LIKE '%".$this->db->escape_like_str($keyword)."%' 
				) 
			LIMIT 50
		";

		return $this->db->query($sql);
	}

	function cari_kode_by_supplier($keyword, $id_pm, $kode_supplier)
	{
		$sql = "
			SELECT 
				bp.id_barang_pusat,
				bp.kode_barang, bp.kode_supplier, sp.nama_supplier, bp.sku, bp.nama_barang, bp.foto, bp.total_stok,
				bp.modal, bp.resistensi_modal, bp.modal_bersih,
				FORMAT(bp.modal,0)harga_modal, FORMAT(bp.modal_bersih,0)harga_modal_bersih
			FROM 
				vamr4846_vama.barang_pusat AS bp
			LEFT JOIN 
				vamr4846_vama.supplier AS sp ON sp.kode_supplier=bp.kode_supplier
			WHERE 
				id_barang_pusat NOT IN (SELECT id_barang_pusat 
										FROM vamr4846_vama.pembelian_detail AS PD 
										WHERE id_pembelian_m='".$this->db->escape_like_str($id_pm)."') 
				AND bp.kode_supplier 	= '".$this->db->escape_like_str($kode_supplier)."'
				AND bp.status_hapus 	= 'TIDAK'
				AND ( 
						bp.sku LIKE '%".$this->db->escape_like_str($keyword)."%' 
						OR bp.nama_barang LIKE '%".$this->db->escape_like_str($keyword)."%' 
					) 
			LIMIT 50
		";

		return $this->db->query($sql);
	}

	function cari_kode_by_perakitan($keyword, $id_pm)
	{
		$sql = "
			SELECT 
				bp.id_barang_pusat,  bp.kode_barang, bp.sku, bp.nama_barang, bp.foto, 
				bp.harga_eceran, bp.total_stok
			FROM 
				vamr4846_vama.barang_pusat AS bp
			WHERE 
				id_barang_pusat NOT IN (SELECT id_barang_pusat 
										FROM vamr4846_vama.perakitan_detail AS PD 
										WHERE id_perakitan_m='".$this->db->escape_like_str($id_pm)."') 
				AND bp.status_hapus = 'tidak'
				AND bp.status 		= 'Aktif'
				AND ( 
						bp.sku LIKE '%".$this->db->escape_like_str($keyword)."%' 
						OR bp.nama_barang LIKE '%".$this->db->escape_like_str($keyword)."%' 
				)
			LIMIT 50 
		";

		return $this->db->query($sql);
	}

	function cari_kode_by_komponen($keyword, $id_pm, $id_barang_perakitan)
	{
		$sql = "
			SELECT 
				bp.id_barang_pusat, bp.kode_barang, bp.sku, bp.nama_barang, bp.foto, 
				bp.harga_eceran, bp.total_stok
			FROM 
				vamr4846_vama.barang_pusat AS bp
			WHERE 
				id_barang_pusat NOT IN (SELECT id_barang_pusat 
										FROM vamr4846_vama.perakitan_detail AS PD 
										WHERE id_perakitan_m='".$this->db->escape_like_str($id_pm)."') 
				AND id_barang_pusat NOT IN (SELECT id_barang_pusat 
											FROM vamr4846_vama.perakitan_master AS PD 
											WHERE id_perakitan_m='".$this->db->escape_like_str($id_pm)."')
				AND not bp.id_barang_pusat 	= '".$id_barang_perakitan."'
				AND bp.status_hapus 		= 'tidak'
				AND bp.status 				= 'Aktif'
				AND bp.total_stok 			> 0 
				AND ( 
						bp.sku LIKE '%".$this->db->escape_like_str($keyword)."%' 
						OR bp.nama_barang LIKE '%".$this->db->escape_like_str($keyword)."%' 
				)
			LIMIT 50 
		";

		return $this->db->query($sql);
	}

	function cari_kode_by_supplier_retur($keyword, $id_rpm, $kode_supplier)
	{
		$sql = "
			SELECT 
				bp.id_barang_pusat,
				bp.kode_barang, bp.kode_supplier, sp.nama_supplier, bp.sku, bp.nama_barang, bp.foto, 
				bp.total_stok, bp.total_stok_rusak, 
				bp.modal, bp.resistensi_modal, bp.modal_bersih,
				FORMAT(bp.modal,0)harga_modal, FORMAT(bp.modal_bersih,0)harga_modal_bersih
			FROM 
				vamr4846_vama.barang_pusat AS bp
			LEFT JOIN 
				vamr4846_vama.supplier AS sp ON sp.kode_supplier=bp.kode_supplier
			WHERE 
				bp.status_hapus 	= 'TIDAK'
				AND bp.status 			= 'AKTIF'
				AND ( 	bp.sku LIKE '%".$this->db->escape_like_str($keyword)."%' 
						OR bp.nama_barang LIKE '%".$this->db->escape_like_str($keyword)."%') 
				AND (	bp.total_stok > 0 
						OR bp.total_stok_rusak > 0)
				AND id_barang_pusat NOT IN (SELECT id_barang_pusat 
										FROM vamr4846_vama.retur_pembelian_detail AS PD 
										WHERE id_retur_pembelian_m='".$this->db->escape_like_str($id_rpm)."') 
				AND bp.kode_supplier 	= '".$this->db->escape_like_str($kode_supplier)."'
			LIMIT 50
		";
		return $this->db->query($sql);
	}

	// cari barang pusat berdasarkan sku
	public function cari_harga_barang_pusat($sku) {
		$this->db->select('
			id_barang_pusat, sku, nama_barang, 
			harga_eceran, harga_grosir1, harga_grosir2,
			FORMAT(harga_eceran,0)eceran, FORMAT(harga_grosir1,0)grosir1, FORMAT(harga_grosir2,0)grosir2'
		);
		$this->db->from('vamr4846_vama.barang_pusat');
		$this->db->where('sku',$sku);
		$query=$this->db->get();
		return $query->row();
	}

	public function get_sku($sku)
	{
		$this->db->select('id_barang_pusat, sku, nama_barang, modal_bersih, harga_eceran, foto');
		$this->db->from('vamr4846_vama.barang_pusat');
		$this->db->where('sku', $sku);
		$query = $this->db->get();
		return $query->row();
	}

	public function lihat_stok($id_barang_pusat)
	{
		$this->db->select("nama_barang, total_stok");
		$this->db->from('vamr4846_vama.barang_pusat');
		$this->db->where('id_barang_pusat', $id_barang_pusat);
		$query = $this->db->get();
		return $query->row();
	}

	// Cek stok
	public function get_stok($sku, $jumlah_beli)
	{
		$this->db->select("id_barang_pusat, sku, nama_barang, (total_stok + '".$jumlah_beli."')as total_stok");
		$this->db->from('vamr4846_vama.barang_pusat');
		$this->db->where('sku',$sku);
		$query=$this->db->get();
		return $query->row();
	}

	// Cek stok dengan id barang pusat, cara baru agar lebih valid
	public function get_stok_id($id_barang_pusat, $jumlah)
	{
		$this->db->select("nama_barang, (total_stok + '".$jumlah."')as total_stok");
		$this->db->from('vamr4846_vama.barang_pusat');
		$this->db->where('id_barang_pusat', $id_barang_pusat);
		$query=$this->db->get();
		return $query->row();
	}

	public function cek_stok_retur_pembelian($id_barang_pusat, $dari_stok, $dari_stok_lama, $jumlah_retur_lama)
	{
		if($dari_stok == 'JUAL'){
			if($dari_stok_lama == 'RUSAK'){
				$this->db->select('sku, nama_barang, total_stok');
			}else{
				$this->db->select('sku, nama_barang, (total_stok + '.$jumlah_retur_lama.') AS total_stok');
			}
		}else if($dari_stok == 'RUSAK'){
			if($dari_stok_lama == 'JUAL'){
				$this->db->select('sku, nama_barang, total_stok_rusak AS total_stok');
			}else{
				$this->db->select('sku, nama_barang, (total_stok_rusak + '.$jumlah_retur_lama.') AS total_stok');
			}
		}

		$this->db->from('vamr4846_vama.barang_pusat');
		$this->db->where('id_barang_pusat', $id_barang_pusat);
		$query=$this->db->get();
		return $query->row();
	}

	public function update_stok($id_barang_pusat, $jumlah)
	{
		$sql = "UPDATE vamr4846_vama.barang_pusat SET total_stok = total_stok - ".$jumlah." WHERE id_barang_pusat = '".$id_barang_pusat."'";
		return $this->db->query($sql);
	}

	public function update_stok_rusak($id_barang_pusat, $jumlah)
	{
		$sql = "
			UPDATE vamr4846_vama.barang_pusat 
			SET 
				total_stok_rusak = total_stok_rusak - ".$jumlah." 
			WHERE id_barang_pusat = '".$id_barang_pusat."'";
		return $this->db->query($sql);
	}

	public function update_tambah_stok($id_barang_pusat, $jumlah)
	{
		$sql = "
			UPDATE vamr4846_vama.barang_pusat 
			SET 
				total_stok = total_stok + ".$jumlah.",
				status = 'AKTIF' 
			WHERE id_barang_pusat = '".$id_barang_pusat."'";
		return $this->db->query($sql);
	}

	public function update_modal($id_barang_pusat, $harga, $resistensi_harga, $harga_bersih, $id_pegawai)
	{
		$sql = "UPDATE vamr4846_vama.barang_pusat 
				SET modal = '".$harga."', 
					resistensi_modal = '".$resistensi_harga."', 
					modal_bersih = '".$harga_bersih."',
					id_pegawai_pembaharuan = '".$id_pegawai."'
				WHERE id_barang_pusat = '".$id_barang_pusat."'";
		return $this->db->query($sql);
	}

	public function update_tambah_stok_rusak($id_barang_pusat, $jumlah)
	{
		$sql = "UPDATE vamr4846_vama.barang_pusat SET total_stok_rusak = total_stok_rusak + ".$jumlah." WHERE id_barang_pusat = '".$id_barang_pusat."'";
		return $this->db->query($sql);
	}

	public function cek_kode($sku)
	{
		return $this->db
			->select('id_barang_pusat')
			->where('sku', $sku)
			->where('status_hapus', 'TIDAK')
			->limit(1)
			->get('vamr4846_vama.barang_pusat');
	}		

	// Listing data
	public function listing() {
		$this->db->select('bp.*, sp.nama_supplier');
		$this->db->from('vamr4846_vama.barang_pusat AS bp');
		$this->db->join('vamr4846_vama.supplier AS sp','sp.kode_supplier=bp.kode_supplier','LEFT');
		$this->db->where('bp.status_hapus','TIDAK');
		$this->db->order_by('bp.kode_barang','DESC');
		$query=$this->db->get();
		return $query->result();
	}

	// Listing data filter supplier
	public function barang_supplier($kode_supplier) {
		$this->db->select('bp.*, sp.nama_supplier');
		$this->db->from('vamr4846_vama.barang_pusat AS bp');
		$this->db->join('vamr4846_vama.supplier AS sp','sp.kode_supplier=bp.kode_supplier','LEFT');
		$this->db->where(array(
			'bp.kode_supplier' => $kode_supplier,
			'bp.status_hapus'  => 'TIDAK'
		));
		$this->db->order_by('kode_barang','DESC');
		$query=$this->db->get();
		return $query->result();
	}

	// Detail data
	public function cari_barang($sku) {
		$this->db->select('bp*, jb.nama_jenis, kb.nama_kategori, sp.nama_supplier');
		$this->db->from('vamr4846_vama.barang_pusat AS bp');
		$this->db->join('vamr4846_vama.jenis_barang AS jb','jb.kode_jenis=bp.kode_jenis');
		$this->db->join('vamr4846_vama.kategori_barang AS kb','kb.kode_kategori=bp.kode_kategori');
		$this->db->join('vamr4846_vama.supplier AS sp','sp.kode_supplier=sp.kode_supplier');				
		$this->db->where('sku', $sku);
		$this->db->where('bp.status_hapus','TIDAK');
		$this->db->order_by('bp.kode_barang','DESC');
		$query = $this->db->get();
		return $query->row();
	}	

	// Cek kode barang barau
	public function ambil_kode_barang_baru($kode_supplier,$kode_jenis,$kode_kategori) {
		$this->db->select('
			RIGHT(kode_barang, 4)+1 AS kode_barang_baru, 
			kode_barang, kode_supplier, kode_jenis, kode_kategori
		');
		$this->db->from('vamr4846_vama.barang_pusat');
		$this->db->where(array(  
			'kode_supplier' => $kode_supplier,
			'kode_jenis'    => $kode_jenis,
			'kode_kategori' => $kode_kategori
		));
		$this->db->order_by('kode_barang','DESC');
		$query = $this->db->get();
		return $query->row();
	}

	// Cek sku barang barau
	public function ambil_sku_baru($kode_jenis) {
		$this->db->select('
			RIGHT(SKU, 4)+1 AS sku_baru,
			kode_supplier, kode_jenis, kode_kategori
		');
		$this->db->from('vamr4846_vama.barang_pusat');
		$this->db->where('kode_jenis',$kode_jenis);
		$this->db->order_by('sku','DESC');
		$query = $this->db->get();
		return $query->row();
	}
	  
	public function get_id($id_barang_pusat)
	{
		$this->db->select('*');		
		$this->db->from('vamr4846_vama.barang_pusat');
		$this->db->where('id_barang_pusat', $id_barang_pusat);
		$query = $this->db->get();
		return $query->row();
	}

	public function get_by_id($id_barang_pusat)
	{
		$this->db->select('bp.*, sp.nama_supplier, jb.nama_jenis, kb.nama_kategori');		
		$this->db->from('vamr4846_vama.barang_pusat AS bp');
		$this->db->join('vamr4846_vama.supplier AS sp','sp.kode_supplier=bp.kode_supplier','LEFT');		
		$this->db->join('vamr4846_vama.jenis_barang AS jb','jb.kode_jenis=bp.kode_jenis','LEFT');		
		$this->db->join('vamr4846_vama.kategori_barang AS kb','kb.kode_jenis=jb.kode_jenis AND kb.kode_kategori=bp.kode_kategori','LEFT');		
		$this->db->where(array(
			'id_barang_pusat' => $id_barang_pusat,
			'bp.status_hapus' => 'TIDAK'
		));
		$query = $this->db->get();
		return $query->row();
	}

	public function get_detail_transaksi($id_barang_pusat, $id_periode_stok_pusat, $tanggal_awal, $tanggal_akhir)
	{
		$this->db->select('
			bp.id_barang_pusat, bp.sku, bp.kode_barang, bp.nama_barang, bp.foto,
			bp.harga_eceran, bp.harga_grosir1, bp.harga_grosir2,
			sp.nama_supplier, jb.nama_jenis, kb.nama_kategori, 
			IFNULL(dso.jumlah_so, 0) AS jumlah_so, IFNULL(dpb.jumlah_beli,0) AS jumlah_beli, 
			IFNULL(ddp.jumlah_perakitan, 0) AS jumlah_perakitan, IFNULL(drpjt.jumlah_retur_penjualan, 0) AS jumlah_retur_penjualan,
			IFNULL(hd.jumlah_hadiah, 0) AS jumlah_hadiah, IFNULL(dup.jumlah_komponen, 0) AS jumlah_komponen, 
			IFNULL(dpju.jumlah_jual_umum, 0) AS jumlah_jual_umum, IFNULL(dpjt.jumlah_jual_toko, 0) AS jumlah_jual_toko, 
			IFNULL(drpbt.jumlah_retur_pembelian, 0) AS jumlah_retur_pembelian, IFNULL(drbdt.jumlah_masuk,0) AS jumlah_retur_toko,
			(IFNULL(dso.jumlah_so, 0) + IFNULL(dpb.jumlah_beli,0) + IFNULL(ddp.jumlah_perakitan, 0) + IFNULL(drbdt.jumlah_masuk,0) +
			 IFNULL(drpjt.jumlah_retur_penjualan, 0) - IFNULL(hd.jumlah_hadiah, 0) - IFNULL(dup.jumlah_komponen, 0) -
			 IFNULL(dpju.jumlah_jual_umum, 0) - IFNULL(dpjt.jumlah_jual_toko, 0) - IFNULL(drpbt.jumlah_retur_pembelian, 0)
			)AS stok_jual
		');		
		$this->db->from('vamr4846_vama.barang_pusat AS bp');
		$this->db->join('vamr4846_vama.supplier AS sp','sp.kode_supplier=bp.kode_supplier','LEFT');		
		$this->db->join('vamr4846_vama.jenis_barang AS jb','jb.kode_jenis=bp.kode_jenis','LEFT');		
		$this->db->join('vamr4846_vama.kategori_barang AS kb','kb.kode_jenis=jb.kode_jenis AND kb.kode_kategori=bp.kode_kategori','LEFT');
		$this->db->join('(SELECT id_barang_pusat, SUM(jumlah_so) AS jumlah_so FROM vamr4846_vama.stok_opname_pusat  
						  WHERE DATE(tanggal_pembuatan) BETWEEN "'.$tanggal_awal.'" AND "'.$tanggal_akhir.'" 
						  GROUP BY id_barang_pusat) AS dso', 'dso.id_barang_pusat=bp.id_barang_pusat', 'LEFT');
		$this->db->join('(SELECT id_barang_pusat, SUM(jumlah_beli) AS jumlah_beli FROM vamr4846_vama.pembelian_detail 
						  WHERE DATE(tanggal_pembuatan) BETWEEN "'.$tanggal_awal.'" AND "'.$tanggal_akhir.'" 
						  GROUP BY id_barang_pusat) AS dpb', 'dpb.id_barang_pusat=bp.id_barang_pusat', 'LEFT');
		$this->db->join('(SELECT id_barang_pusat, SUM(jumlah_perakitan) AS jumlah_perakitan FROM vamr4846_vama.perakitan_master 
						  WHERE DATE(tanggal_pembuatan) BETWEEN "'.$tanggal_awal.'" AND "'.$tanggal_akhir.'" 
						  GROUP BY id_barang_pusat) AS ddp', 'ddp.id_barang_pusat=bp.id_barang_pusat', 'LEFT');
		$this->db->join('(SELECT id_barang_pusat, SUM(jumlah_retur) AS jumlah_retur_penjualan FROM vamr4846_vama.retur_penjualan_detail 
						  WHERE masuk_stok="JUAL" AND DATE(tanggal_pembuatan) BETWEEN "'.$tanggal_awal.'" AND "'.$tanggal_akhir.'" 
						  GROUP BY id_barang_pusat) AS drpjt', 'drpjt.id_barang_pusat=bp.id_barang_pusat', 'LEFT');
		$this->db->join('(SELECT id_barang_pusat, SUM(jumlah_hadiah) AS jumlah_hadiah FROM vamr4846_vama.hadiah_detail 
						  WHERE DATE(tanggal_pembuatan) BETWEEN "'.$tanggal_awal.'" AND "'.$tanggal_akhir.'" 
						  GROUP BY id_barang_pusat) AS hd', 'hd.id_barang_pusat=bp.id_barang_pusat', 'LEFT');
		$this->db->join('(SELECT id_barang_pusat, SUM(jumlah_komponen) AS jumlah_komponen FROM vamr4846_vama.perakitan_detail 
						  WHERE DATE(tanggal_pembuatan) BETWEEN "'.$tanggal_awal.'" AND "'.$tanggal_akhir.'" 
						  GROUP BY id_barang_pusat) AS dup', 'dup.id_barang_pusat=bp.id_barang_pusat', 'LEFT');
		$this->db->join('(SELECT id_barang_pusat, SUM(jumlah_beli) AS jumlah_jual_umum FROM vamr4846_vama.penjualan_detail 
						  WHERE DATE(tanggal_pembuatan) BETWEEN "'.$tanggal_awal.'" AND "'.$tanggal_akhir.'" AND jenis_harga !="MRC" 
						  GROUP BY id_barang_pusat) AS dpju', 'dpju.id_barang_pusat=bp.id_barang_pusat', 'LEFT');
		$this->db->join('(SELECT id_barang_pusat, SUM(jumlah_beli) AS jumlah_jual_toko FROM vamr4846_vama.penjualan_detail 
						  WHERE DATE(tanggal_pembuatan) BETWEEN "'.$tanggal_awal.'" AND "'.$tanggal_akhir.'" AND jenis_harga ="MRC" 
						  GROUP BY id_barang_pusat) AS dpjt', 'dpjt.id_barang_pusat=bp.id_barang_pusat', 'LEFT');
		$this->db->join('(SELECT id_barang_pusat, SUM(jumlah_retur) AS jumlah_retur_pembelian FROM vamr4846_vama.retur_pembelian_detail 
						  WHERE dari_stok="JUAL" AND DATE(tanggal_pembuatan) BETWEEN "'.$tanggal_awal.'" AND "'.$tanggal_akhir.'" 
						  GROUP BY id_barang_pusat) AS drpbt', 'drpbt.id_barang_pusat=bp.id_barang_pusat', 'LEFT');

		$this->db->join('(SELECT id_barang_pusat, SUM(jumlah_masuk) AS jumlah_masuk FROM vamr4846_vama.retur_dari_toko 
						  WHERE kondisi_barang="BAIK" AND DATE(tanggal_pembuatan) BETWEEN "'.$tanggal_awal.'" AND "'.$tanggal_akhir.'" 
						  GROUP BY id_barang_pusat) AS drbdt', 'drbdt.id_barang_pusat=bp.id_barang_pusat', 'LEFT');

		$this->db->where('bp.id_barang_pusat', $id_barang_pusat);
		$query = $this->db->get();
		return $query->row();
	}

	public function get_detail_transaksi_rusak($id_barang_pusat, $id_periode_stok_pusat, $tanggal_awal, $tanggal_akhir)
	{
		$this->db->select('
			bp.id_barang_pusat, bp.sku, bp.kode_barang, bp.nama_barang, bp.foto,
			IFNULL(rpr.jumlah_retur,0) AS rpr, 
			IFNULL(drbdtr.jumlah_masuk,0) AS rbdtr, 
			0 AS ps,
			IFNULL(rpbr.jumlah_retur,0) AS rpbr,
			(IFNULL(rpr.jumlah_retur, 0) + IFNULL(drbdtr.jumlah_masuk,0) - IFNULL(rpbr.jumlah_retur, 0)) AS stok_rusak, 
		');		
		$this->db->from('vamr4846_vama.barang_pusat AS bp');
		$this->db->join('(SELECT id_barang_pusat, SUM(jumlah_retur) AS jumlah_retur FROM vamr4846_vama.retur_penjualan_detail 
						  WHERE masuk_stok="RUSAK" AND DATE(tanggal_pembuatan) BETWEEN "'.$tanggal_awal.'" AND "'.$tanggal_akhir.'" 
						  GROUP BY id_barang_pusat) AS rpr', 'rpr.id_barang_pusat=bp.id_barang_pusat', 'LEFT');
		$this->db->join('(SELECT id_barang_pusat, SUM(jumlah_retur) AS jumlah_retur FROM vamr4846_vama.retur_pembelian_detail 
						  WHERE dari_stok="RUSAK" AND DATE(tanggal_pembuatan) BETWEEN "'.$tanggal_awal.'" AND "'.$tanggal_akhir.'" 
						  GROUP BY id_barang_pusat) AS rpbr', 'rpbr.id_barang_pusat=bp.id_barang_pusat', 'LEFT');
		$this->db->join('(SELECT id_barang_pusat, SUM(jumlah_masuk) AS jumlah_masuk FROM vamr4846_vama.retur_dari_toko 
						  WHERE kondisi_barang="RUSAK" AND DATE(tanggal_pembuatan) BETWEEN "'.$tanggal_awal.'" AND "'.$tanggal_akhir.'" 
						  GROUP BY id_barang_pusat) AS drbdtr', 'drbdtr.id_barang_pusat=bp.id_barang_pusat', 'LEFT');
		$this->db->where('bp.id_barang_pusat', $id_barang_pusat);
		$query = $this->db->get();
		return $query->row();
	}

	public function get_by_nama($nama_barang)
	{
		$this->db->select('*');		
		$this->db->from('vamr4846_vama.barang_pusat AS bp');
		$this->db->where(array(
			'nama_barang'     => $nama_barang,
			'bp.status_hapus' => 'TIDAK'
		));
		$query = $this->db->get();
		return $query->row();
	}

	public function save($data)
	{
		$this->db->insert($this->table, $data);
		return $this->db->insert_id();
	}

	public function save_foto($data)
	{
		$this->db->insert('foto', $data);
		return $this->db->insert_id();
	}

	public function update($where, $data)
	{
		$this->db->update($this->table, $data, $where);
		return $this->db->affected_rows();
	}

	public function delete_by_id($id_barang_pusat)
	{
		$this->db->where('id_barang_pusat', $id_barang_pusat);
		$this->db->delete($this->table);
	}

	public function delete_foto($id_foto)
	{
		$this->db->where('id_foto', $id_foto);
		$this->db->delete('foto');
	}

	public function update_status_hapus($id_barang_pusat, $id_pegawai)
	{
		$sql = "UPDATE vamr4846_vama.barang_pusat
				SET status_hapus = 'IYA', id_pegawai_pembaharuan = '".$id_pegawai."' 
				WHERE id_barang_pusat = '".$id_barang_pusat."'";
		return $this->db->query($sql);
	}

	function ambil_barang_untuk_penjualan($sku, $jenis_harga)
	{
		$sql = "
				SELECT 
					bp.id_barang_pusat, bp.kode_barang, bp.sku, bp.nama_barang, bp.foto, bp.total_stok,
					bp.harga_".$jenis_harga." AS harga_satuan
				FROM 
					vamr4846_vama.barang_pusat AS bp
				WHERE 
					bp.status_hapus 	= 'tidak'
					AND bp.status 		= 'Aktif'
					AND bp.total_stok 	> '0' 
					AND bp.sku 			= '".$this->db->escape_like_str($sku)."' 
		";
		return $this->db->query($sql);
	}

	function ambil_barang_untuk_transaksi($sku)
	{
		$sql = "
			SELECT 
				bp.id_barang_pusat, bp.kode_barang, bp.sku, bp.nama_barang, bp.foto, 
				bp.total_stok, bp.status,
				bp.harga_eceran, bp.harga_grosir1, bp.harga_grosir2
			FROM 
				vamr4846_vama.barang_pusat AS bp
			WHERE 
				bp.status_hapus = 'tidak'
				AND bp.sku      = '".$sku."' 
		";

		return $this->db->query($sql);
	}

	function ambil_barang_untuk_pembelian($sku, $kode_supplier)
	{
		$this->db->select('bp.id_barang_pusat, bp.kode_barang, bp.sku, bp.nama_barang, bp.foto, bp.total_stok,
						   bp.modal, bp.resistensi_modal, bp.modal_bersih, bp.kode_supplier, sp.nama_supplier');
		$this->db->from('vamr4846_vama.barang_pusat AS bp');
		$this->db->join('vamr4846_vama.supplier AS sp', 'sp.kode_supplier=bp.kode_supplier');
		$this->db->where(array(
			'bp.status_hapus'  => 'TIDAK',
			// 'bp.status'        => 'AKTIF',
			'bp.sku'           => $sku,
			'sp.kode_supplier' => $kode_supplier 
		));
		$query = $this->db->get();
		return $query->row();
	}

	function ambil_barang_pusat_lama()
	{
		$sql = "SELECT * FROM vamr4846_mrc.master_barang";
		return $this->db->query($sql);
	}

	function validasi_barang($sku, $kode_barang)
	{
		$this->db->select('sku, kode_barang');
		$this->db->from('vamr4846_vama.barang_pusat');
		$this->db->where(array(
			'sku'         => $sku,
			'kode_barang' => $kode_barang
		));
		$query = $this->db->get();
		return $query->row();
	}

	function validasi_jenis($kode_jenis)
	{
		$this->db->select('kode_jenis');
		$this->db->from('vamr4846_vama.jenis_barang');
		$this->db->where('kode_jenis', $kode_jenis);
		$query = $this->db->get();
		return $query->row();
	}

	function validasi_kategori($kode_jenis, $kode_kategori)
	{
		$this->db->select('kode_kategori');
		$this->db->from('vamr4846_vama.kategori_barang');
		$this->db->where(array(
			'kode_jenis'    => $kode_jenis,
			'kode_kategori' => $kode_kategori
		));
		$query = $this->db->get();
		return $query->row();
	}

	function validasi_supplier($kode_supplier)
	{
		$this->db->select('kode_supplier');
		$this->db->from('vamr4846_vama.supplier');
		$this->db->where('kode_supplier', $kode_supplier);
		$query = $this->db->get();
		return $query->row();
	}

	function ambil_jenis_barang($kode_jenis)
	{
		$this->db->select('*');
		$this->db->from('vamr4846_mrc.jenis_barang');
		$this->db->where('Kode_Jenis', $kode_jenis);
		$query = $this->db->get();
		return $query->row();
	}

	function ambil_kategori_barang($kode_jenis, $kode_kategori)
	{
		$this->db->select('*');
		$this->db->from('vamr4846_mrc.kategori_barang');
		$this->db->where(array(
			'kode_jenis'    => $kode_jenis,
			'kode_kategori' => $kode_kategori
		));
		$query = $this->db->get();
		return $query->row();
	}

	function ambil_supplier($kode_suppleir)
	{
		$this->db->select('*');
		$this->db->from('vamr4846_mrc.master_supplier');
		$this->db->where('Kode_Supplier', $kode_suppleir);
		$query = $this->db->get();
		return $query->row();
	}

	function import_barang_pusat()
	{
		$sql_insert_batal = "
				INSERT INTO vamr4846_vama.barang_pusat (
					kode_barang, sku, nomor_urut, 
					total_stok, total_stok_rusak, nama_barang, 
					kode_supplier, kode_jenis, kode_kategori, 
					modal, resistensi_modal, modal_bersih, 
					resistensi_harga_eceran, harga_eceran, harga_grosir1, harga_grosir2,
					foto, status, status2, status_hapus,
					tanggal_pembuatan, tanggal_pembaharuan
				)  
				VALUES (
					'".$b->Kode_Barang."', '".$b->SKU."', '".$no."', 
					'0', '0', '".$b->Nama_Barang."',
					'".$b->Kode_Supplier."', '".$b->Kode_Jenis."', '".$b->Kode_Kategori."', 
					'".$b->Modal."', '".$b->Resistensi_Modal."', '".$b->Modal_Bersih."', 
					'".$b->Resistensi_Harga_Eceran."', '".$b->Harga_Eceran."', 
					'".$b->Harga_Grosir."', '".$b->Harga_Bengkel."',
					'avatar.JPG', '".$b->Status."', '".$b->Status2."', '".$b->Status_Hapus."',
					'".$b->Tanggal_Pembuatan."', '".$b->Tanggal_Pembaharuan."'
				)
		";
		$this->db->query($sql_insert_batal);
	}
}

<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Supplier_model extends MY_Model {

	var $table         = 'vamr4846_vama.supplier AS sp';
	var $column_order  = array(
		'','sp.kode_supplier','sp.nama_supplier','sp.tipe_bisnis','sp.asal_supplier', 
		'pp1.nama_pegawai', 'sp.tanggal_pembuatan', 'pp2.nama_pegawai', 'pp2.tanggal_pembaharuan'); 
	var $column_search = array('kode_supplier','nama_supplier','email','telephone1','handphone1','fax','asal_supplier'); 
	var $order         = array('id_supplier' => 'desc');

	private function _get_datatables_query()
	{
		$this->db->select('sp.*, pp1.nama_pegawai AS pegawai_save, pp2.nama_pegawai As pegawai_edit');
		$this->db->from('vamr4846_vama.supplier AS sp');
		$this->db->join('vamr4846_vama.pegawai_pusat AS pp1', 'pp1.id_pegawai=sp.id_pegawai_pembuatan', 'LEFT');
		$this->db->join('vamr4846_vama.pegawai_pusat AS pp2', 'pp2.id_pegawai=sp.id_pegawai_pembaharuan', 'LEFT');
		$this->db->where('sp.status_hapus', 'TIDAK');

		$i = 0;
	
		foreach ($this->column_search as $item){
			if($_POST['search']['value']){
				if($i===0){
					$this->db->group_start();
					$this->db->like($item, $_POST['search']['value']);
				}else{
					$this->db->or_like($item, $_POST['search']['value']);
				}

				if(count($this->column_search) - 1 == $i)
				$this->db->group_end();
			}
			$i++;
		}
		
		if(isset($_POST['order'])){
			$this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		}else if(isset($this->order)){
			$order = $this->order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}

	function get_datatables()
	{
		$this->_get_datatables_query();
		if($_POST['length'] != -1)
		$this->db->limit($_POST['length'], $_POST['start']);
		$query = $this->db->get();
		return $query->result();
	}

	function count_filtered()
	{
		$this->_get_datatables_query();
		$query = $this->db->get();
		return $query->num_rows();
	}

	public function count_all()
	{
		$this->db->from($this->table);
		return $this->db->count_all_results();
	}

	// Listing data
	public function listing() {
		$this->db->select('*');
		$this->db->from('vamr4846_vama.supplier AS sp');
		$this->db->order_by('nama_supplier','ASC');
		$query=$this->db->get();
		return $query->result();
	}

	public function ambil_supplier($keyword)
	{
		$sql = "
			SELECT kode_supplier, nama_supplier
			FROM 
				`vamr4846_vama.supplier AS sp` 
			WHERE 
				( 
					`kode_supplier` LIKE '%".$this->db->escape_like_str($keyword)."%' 
					OR `nama_supplier` LIKE '%".$this->db->escape_like_str($keyword)."%' 
				) 
		";
		return $this->db->query($sql);
	}
	
	public function cari_supplier($cari_kode_supplier) {
		$this->db->select('*');
		$this->db->from('vamr4846_vama.supplier AS sp');
		$this->db->where('kode_supplier',$cari_kode_supplier);
		$this->db->order_by('id_supplier','DESC');
		$query=$this->db->get();
		return $query->row();
	}

	public function cari_supplier_1($keyword)
	{
		$sql = "
			SELECT *
			FROM 
				vamr4846_vama.supplier AS sp 
			WHERE 
				( 
					`kode_supplier` LIKE '%".$this->db->escape_like_str($keyword)."%' 
					OR `nama_supplier` LIKE '%".$this->db->escape_like_str($keyword)."%' 
				) 
		";
		return $this->db->query($sql);
	}

	function cari_supplier_2($keyword)
	{
		$this->db->select('*');
		$this->db->from('vamr4846_vama.supplier AS sp');
		$this->db->like('nama_supplier',$keyword);
		$this->db->order_by('nama_supplier','ASC');
		$query=$this->db->get();
		return $query->result();
	}

	// Dapatkan kode terakhir
	public function akhir() {
		$this->db->select('COUNT(id_supplier)+1 AS id_supplier_baru');
		$this->db->from('vamr4846_vama.supplier AS sp');
		$this->db->order_by('id_supplier','DESC');
		$query = $this->db->get();
		return $query->row();
	}

	public function get_by_id($id_supplier)
	{
		$this->db->from($this->table);
		$this->db->where('id_supplier',$id_supplier);
		$query = $this->db->get();
		return $query->row();
	}

	public function get_by_nama($nama_supplier)
	{
		$this->db->from($this->table);
		$this->db->where('nama_supplier',$nama_supplier);
		$query = $this->db->get();
		return $query->row();
	}

	public function save($data)
	{
		$this->db->insert($this->table, $data);
		return $this->db->insert_id();
	}

	public function update($where, $data)
	{
		$this->db->update($this->table, $data, $where);
		return $this->db->affected_rows();
	}

	public function delete_by_id($id_supplier)
	{
		$this->db->where('id_supplier', $id_supplier);
		$this->db->delete($this->table);
	}

	public function update_status_hapus($id_supplier, $id_pegawai)
	{
		$sql = "UPDATE vamr4846_vama.supplier 
				SET status_hapus = 'IYA', id_pegawai_pembaharuan = '".$id_pegawai."' 
				WHERE id_supplier = '".$id_supplier."'";
		return $this->db->query($sql);
	}
}

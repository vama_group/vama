<?php
class Pembelian_detail_model extends CI_Model
{
	// Pembelian induk
	var $column_order         = array('', '', 'bp.sku','bp.nama_barang', 'pd.jumlah_beli');
	var $column_search        = array('bp.kode_barang','bp.sku','bp.nama_barang'); 
	var $order                = array('pd.tanggal_pembaharuan' => 'desc');
	
	// Pembelian batal
	var $column_order_batal   = array('', '', 'bp.sku','bp.nama_barang', 'pbd.jumlah_beli', 'pbd.subtotal');
	var $column_search_batal  = array('bp.kode_barang','bp.sku','bp.nama_barang'); 
	var $order_batal          = array('pbd.tanggal_pembatalan' => 'desc');
	
	// Cari barang
	var $column_order_barang  = array('', '', 'bp.sku','bp.nama_barang','bp.total_stok');
	var $column_search_barang = array('bp.kode_barang','bp.sku','bp.nama_barang'); 
	var $order_barang         = array('bp.nama_barang' => 'desc');

	private function _get_datatables_query($id_pembelian_m)
	{		
		$this->db->select('	pd.id_pembelian_d, pd.id_pembelian_m,
							bp.sku, bp.kode_barang, bp.nama_barang, 
							pd.harga, pd.resistensi_harga, pd.harga_bersih, 
							pd.jumlah_beli, pd.subtotal,
							pp1.nama_pegawai AS pegawai_save, IFNULL(pp2.nama_pegawai, "-") AS pegawai_edit, 
							pd.tanggal_pembuatan, pd.tanggal_pembaharuan
						 ');
		$this->db->from('vamr4846_vama.pembelian_detail AS pd');
		$this->db->join('vamr4846_vama.barang_pusat as bp','bp.id_barang_pusat=pd.id_barang_pusat','LEFT');
		$this->db->join('vamr4846_vama.pegawai_pusat as pp1','pp1.id_pegawai=pd.id_pegawai_pembuatan','LEFT');
		$this->db->join('vamr4846_vama.pegawai_pusat as pp2','pp2.id_pegawai=pd.id_pegawai_pembaharuan','LEFT');
		$this->db->where('id_pembelian_m', $id_pembelian_m);

		$i = 0;
	
		foreach ($this->column_search as $item){
			if($_POST['search']['value']){
				if($i===0){
					$this->db->group_start();
					$this->db->like($item, $_POST['search']['value']);
				}else{
					$this->db->or_like($item, $_POST['search']['value']);
				}

				if(count($this->column_search) - 1 == $i)
				$this->db->group_end();
			}
			$i++;
		}
		
		if(isset($_POST['order'])){
			$this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		}else if(isset($this->order)){
			$order = $this->order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}

	private function _get_datatables_query_batal($id_pembelian_m)
	{		
		$this->db->select('	pbd.id_pembelian_d, pbd.id_pembelian_m,
							bp.sku, bp.kode_barang, bp.nama_barang, 
							pbd.harga, pbd.resistensi_harga, pbd.harga_bersih, 
							pbd.jumlah_beli, pbd.subtotal, pbd.keterangan_batal, pp.nama_pegawai, pbd.tanggal_pembatalan
						 ');
		$this->db->from('vamr4846_vama.pembelian_detail_batal AS pbd');
		$this->db->join('vamr4846_vama.barang_pusat as bp','bp.id_barang_pusat=pbd.id_barang_pusat','LEFT');
		$this->db->join('vamr4846_vama.pegawai_pusat as pp','pp.id_pegawai=pbd.id_pegawai_pembatalan','LEFT');
		$this->db->where('id_pembelian_m',$id_pembelian_m);

		$i = 0;
	
		foreach ($this->column_search_batal as $item){
			if($_POST['search']['value']){
				if($i===0){
					$this->db->group_start();
					$this->db->like($item, $_POST['search']['value']);
				}else{
					$this->db->or_like($item, $_POST['search']['value']);
				}

				if(count($this->column_search_batal) - 1 == $i)
				$this->db->group_end();
			}
			$i++;
		}
		
		if(isset($_POST['order'])){
			$this->db->order_by($this->column_order_batal[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		}else if(isset($this->order_batal)){
			$order = $this->order_batal;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}

	private function _get_datatables_query_barang($id_pembelian_m, $kode_supplier)
	{		
		$ambil_pembelian = $this->db->query("
			SELECT id_barang_pusat 
			FROM vamr4846_vama.pembelian_detail 
			WHERE id_pembelian_m =".$id_pembelian_m."
		");
		$pd_id_barang = array();
		foreach ($ambil_pembelian->result() as $row){
			$pd_id_barang[] = $row->id_barang_pusat; 
		}
		$id_barang 	= implode(",", $pd_id_barang);
		$idp 		= explode(",", $id_barang);
		
		$this->db->select('
							bp.id_barang_pusat, bp.sku, bp.kode_barang, bp.nama_barang, bp.total_stok,
							bp.modal, bp.resistensi_modal, bp.modal_bersih
						 ');
		$this->db->from('vamr4846_vama.barang_pusat as bp');
		$this->db->where('kode_supplier', $kode_supplier);
		$this->db->where_not_in('bp.id_barang_pusat', $idp);
		$i = 0;
	
		foreach ($this->column_search_barang as $item){
			if($_POST['search']['value']){
				if($i===0){
					$this->db->group_start();
					$this->db->like($item, $_POST['search']['value']);
				}else{
					$this->db->or_like($item, $_POST['search']['value']);
				}

				if(count($this->column_search_barang) - 1 == $i)
				$this->db->group_end();
			}
			$i++;
		}
		
		if(isset($_POST['order'])){
			$this->db->order_by($this->column_order_barang[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		}else if(isset($this->order_barang)){
			$order = $this->order_barang;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}

	function get_datatables($perintah, $id_pembelian_m, $kode_supplier)
	{
		$this->$perintah($id_pembelian_m, $kode_supplier);
		if($_POST['length'] != -1)
		$this->db->limit($_POST['length'], $_POST['start']);
		$query = $this->db->get();
		return $query->result();
	}

	function count_filtered($perintah, $id_pembelian_m, $kode_supplier)
	{
		$this->$perintah($id_pembelian_m, $kode_supplier);
		$query = $this->db->get();
		return $query->num_rows();
	}

	public function count_all($table, $id_pembelian_m, $kode_supplier)
	{
		$this->db->from($table);
		if($table == 'vamr4846_vama.pembelian_detail' or $table == 'vamr4846_vama.pembelian_detail_batal'){
			$this->db->where('id_pembelian_m', $id_pembelian_m);
		}else{
			$this->db->where('kode_supplier', $kode_supplier);
		}
		return $this->db->count_all_results();
	}

	function get_pembelian_detail_1($id_pembelian_d)
	{
		$this->db->select('pd.*, 
						   pm.id_supplier, pm.no_pembelian, pm.no_faktur, 
						   bp.sku, bp.nama_barang');
		$this->db->from('vamr4846_vama.pembelian_detail as pd');
		$this->db->join('vamr4846_vama.pembelian_master as pm','pm.id_pembelian_m=pd.id_pembelian_m','LEFT');
		$this->db->join('vamr4846_vama.barang_pusat as bp','bp.id_barang_pusat=pd.id_barang_pusat','LEFT');
		$this->db->where('id_pembelian_d', $id_pembelian_d);
		$query = $this->db->get();
		return $query->row();
	}

	function get_pembelian_detail_2($id_pembelian_d)
	{
		$this->db->select('pd.id_pembelian_d, pd.id_pembelian_m, pd.id_barang_pusat, pd.jumlah_beli, 
						   pm.id_supplier, pm.no_faktur, 
						   bp.sku, bp.nama_barang');
		$this->db->from('vamr4846_vama.pembelian_detail as pd');
		$this->db->join('vamr4846_vama.pembelian_master as pm','pm.id_pembelian_m=pd.id_pembelian_m','LEFT');
		$this->db->join('vamr4846_vama.barang_pusat as bp','bp.id_barang_pusat=pd.id_barang_pusat','LEFT');
		$this->db->where('id_pembelian_d', $id_pembelian_d);
		$query = $this->db->get();
		return $query->row();
	}

	function get_pembelian_detail_all($id_pembelian_m)
	{
		$this->db->select('pd.*, pm.no_pembelian, pm.no_faktur');
		$this->db->from('vamr4846_vama.pembelian_detail AS pd');
		$this->db->join('vamr4846_vama.pembelian_master as pm','pm.id_pembelian_m=pd.id_pembelian_m','LEFT');
		$this->db->join('vamr4846_vama.barang_pusat as bp','bp.id_barang_pusat=pd.id_barang_pusat','LEFT');
		$this->db->where('pd.id_pembelian_m', $id_pembelian_m);
		$query = $this->db->get();
		return $query->result();
	}

	function get_id($id_pembelian_m, $id_barang_pusat)
	{
		$this->db->select('*');
		$this->db->from('vamr4846_vama.pembelian_detail');
		$this->db->where(array(  
			'id_pembelian_m'  => $id_pembelian_m,
			'id_barang_pusat' => $id_barang_pusat
		));
		$query = $this->db->get();
		return $query->row();
	}	

	function insert_detail($id_pembelian_m, $id_barang_pusat,  
						   $harga, $resistensi_harga, $harga_bersih, $jumlah_beli, $subtotal, $id_pegawai)
	{ 
		$dt = array(
			'id_pembelian_m'       => $id_pembelian_m,
			'id_barang_pusat'      => $id_barang_pusat,
			'harga'                => $harga,
			'resistensi_harga'     => $resistensi_harga,
			'harga_bersih'         => $harga_bersih,
			'jumlah_beli'          => $jumlah_beli,
			'subtotal'             => $subtotal,
			'id_pegawai_pembuatan' => $id_pegawai,
			'tanggal_pembuatan'    => date('Y-m-d H:i:s') 		
		);
		return $this->db->insert('vamr4846_vama.pembelian_detail', $dt);
	}

	function update_detail($id_pembelian_m, $id_barang_pusat,  
						   $harga, $resistensi_harga, $harga_bersih, $jumlah_beli, $subtotal, $id_pegawai, $jumlah_beli_lama)
	{
		$sql = "UPDATE vamr4846_vama.barang_pusat SET total_stok = total_stok - ".$this->db->escape_like_str($jumlah_beli_lama)." 
				WHERE id_barang_pusat = '".$this->db->escape_like_str($id_barang_pusat)."'";
		$this->db->query($sql);
		
		$dt = array(
			'harga'                  => $harga,
			'resistensi_harga'       => $resistensi_harga,
			'harga_bersih'           => $harga_bersih,
			'jumlah_beli'            => $jumlah_beli,
			'subtotal'               => $subtotal,
			'id_pegawai_pembaharuan' => $id_pegawai
		);

		$where = array(
			'id_pembelian_m'  => $id_pembelian_m,
			'id_barang_pusat' => $id_barang_pusat
		);

		return $this->db->update('vamr4846_vama.pembelian_detail', $dt, $where);
	}

	function get_detail_faktur($id_pembelian_m)
	{
		$this->db->select('
			pd.id_pembelian_d, pd.harga_satuan, 
			pd.discount, pd.jumlah_beli, pd.subtotal,
			bp.sku, bp.kode_barang, bp.nama_barang
		');
		$this->db->from('vamr4846_vama.pembelian_detail AS pd');
		$this->db->join('vamr4846_vama.barang_pusat as bp','bp.id_barang_pusat=pd.id_barang_pusat');
		$this->db->where('pd.id_pembelian_m',$id_pembelian_m);
		$this->db->order_by('bp.nama_barang','ASC');
		$query = $this->db->get();
		return $query->result();
	}

	function get_total($id_pembelian_m)
	{
		$this->db->select('
			pm.id_pembelian_m,
			IFNULL(COUNT(pd.id_pembelian_d),0) jumlah_barang,
			IFNULL(ROUND(SUM((harga*jumlah_beli)),0),0) total,
			pm.biaya_lain, pm.ppn
		');
		$this->db->from('vamr4846_vama.pembelian_detail AS pd');
		$this->db->join('vamr4846_vama.pembelian_master AS pm', 'pd.id_pembelian_m=pm.id_pembelian_m', 'LEFT');
		$this->db->where(array(  
								'pd.id_pembelian_m' => $id_pembelian_m
		            	));
		$query = $this->db->get();
		return $query->row();
	}

	function hapus_pembelian_detail($id_pembelian_d, $id_pembelian_m, $id_supplier, $no_pembelian, $no_faktur,
								    $id_barang_pusat, $harga, $resistensi_harga, $harga_bersih, $jumlah_beli, $subtotal,
									$id_pegawai_pembuatan, $id_pegawai_pembaharuan, $id_pegawai_pembatalan, 
									$tanggal_pembuatan, $tanggal_pembaharuan, $keterangan_batal)
	{
		
		// Kembalikan stok berdasarkan jumlah beli
		$sql = "UPDATE vamr4846_vama.barang_pusat SET total_stok = total_stok - ".$jumlah_beli." 
				WHERE id_barang_pusat = '".$id_barang_pusat."'";
		$this->db->query($sql);

		// Simpan pembelian detail ke pembelian detail batal
		$sql = "
				INSERT INTO vamr4846_vama.pembelian_detail_batal 
				(id_pembelian_d, id_pembelian_m, id_supplier, no_pembelian, no_faktur, id_barang_pusat, harga, resistensi_harga, harga_bersih, 
				 jumlah_beli, subtotal, id_pegawai_pembuatan, id_pegawai_pembaharuan, id_pegawai_pembatalan, 
				 tanggal_pembuatan, tanggal_pembaharuan, keterangan_batal) 
				VALUES
				('".$id_pembelian_d."', '".$id_pembelian_m."', '".$id_supplier."', '".$no_pembelian."', '".$no_faktur."', '".$id_barang_pusat."', 
				 '".$harga."', '".$resistensi_harga."', '".$harga_bersih."', '".$jumlah_beli."', '".$subtotal."', 
				 '".$id_pegawai_pembuatan."', '".$id_pegawai_pembaharuan."', '".$id_pegawai_pembatalan."', 
				 '".$tanggal_pembuatan."', '".$tanggal_pembaharuan."', '".$keterangan_batal."') 
				";
		$this->db->query($sql);

		// Hapus pembelian detail
		return $this->db
					->where(array(
									'id_pembelian_d' 	=> $id_pembelian_d,
						 		 )
						   )
					->delete('vamr4846_vama.pembelian_detail');
	}
}
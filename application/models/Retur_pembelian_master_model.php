<?php
class Retur_pembelian_master_model extends CI_Model
{
	// Retur pembelian induk
	var $column_order_a 		= array('rpm.id_retur_pembelian_m', 'rpm.id_retur_pembelian_m', 'rpm.no_retur_pembelian',
										'rpm.no_pembelian', 'rpm.status_retur', 'rpm.tanggal_retur', 'sp.nama_supplier', '', 
										'rpm.total', 'ppn', 'biaya_lain', 'grand_total');

	var $column_order_b 		= array('rpm.id_retur_pembelian_m', 'rpm.id_retur_pembelian_m', 'rpm.no_retur_pembelian',
										'rpm.no_pembelian', 'rpm.status_retur', 'rpm.tanggal_retur', 
										'sp.nama_supplier', '');

	var $column_search 			= array('rpm.no_retur_pembelian', 'rpm.no_pembelian', 'sp.nama_supplier', 
										'rpm.total', 'rpm.ppn', 'rpm.biaya_lain', 'rpm.grand_total'); 

	var $order 					= array('id_retur_pembelian_m' => 'desc');

	// Retur pembelian detail
	var $column_order_detail_a 	= array( 'rpd.id_retur_pembelian_d', 'rpd.id_retur_pembelian_d', 
										 'rpm.no_retur_pembelian', 'rpm.status_retur', 'rpm.tanggal_pembaharuan', 'sp.nama_supplier',
								  		 'bp.sku', 'bp.nama_barang', 'rpd.dari_stok', 'rpd.jumlah_retur', 
								  		 'rpd.harga_satuan', 'rpd.subtotal', 'rpd.keterangan'); 

	var $column_order_detail_b 	= array( 'rpd.id_retur_pembelian_d', 'rpd.id_retur_pembelian_d', 
										 'rpm.no_retur_pembelian', 'rpm.status_retur', 'rpm.tanggal_pembaharuan', 'sp.nama_supplier',
								  		 'bp.sku', 'bp.nama_barang', 'rpd.dari_stok', 'rpd.jumlah_retur', 'rpd.keterangan');	

	var $column_search_detail 	= array( 'rpd.id_retur_pembelian_d', 'rpd.id_retur_pembelian_d', 'rpm.no_retur_pembelian', 
										 'rpm.no_pembelian', 'sp.nama_supplier', 'rpd.keterangan',
								  		 'bp.sku', 'bp.nama_barang', 'rpd.jumlah_beli', 'rpd.jumlah_retur', 'rpd.harga_satuan', 'rpd.subtotal'); 
								  		 
	var $order_detail 			= array('id_retur_pembelian_d' => 'DESC');

	// Retur pembelian batal
	var $column_order_batal_a 	= array( 'rpdb.id_retur_pembelian_b', 'rpdb.id_retur_pembelian_d', 'rpdb.no_retur_pembelian', 
										 'rpdb.tanggal_pembuatan', 'sp.nama_supplier',
								  		 'bp.sku', 'bp.nama_barang', 'rpdb.dari_stok', 'rpdb.jumlah_retur', 
								  		 'rpdb.harga_satuan', 'rpdb.subtotal', 'rpdb.keterangan_batal');

	var $column_order_batal_b 	= array( 'rpdb.id_retur_pembelian_b', 'rpdb.id_retur_pembelian_d', 'rpdb.no_retur_pembelian', 
										 'rpdb.tanggal_pembuatan', 'sp.nama_supplier',
								  		 'bp.sku', 'bp.nama_barang', 'rpdb.dari_stok', 'rpdb.jumlah_retur', 
								  		 'rpdb.keterangan_batal');

	var $column_search_batal 	= array( 'rpdb.id_retur_pembelian_b', 'rpdb.id_retur_pembelian_d', 'rpdb.no_pembelian', 'sp.nama_supplier',
								  		 'bp.sku', 'bp.nama_barang', 'rpdb.jumlah_beli', 'rpdb.jumlah_retur', 'rpdb.harga_satuan', 'rpdb.subtotal', 'rpdb.keterangan_batal'); 
	
	var $order_batal 			= array('id_retur_pembelian_b' => 'DESC');

	private function _get_laporan_query($tanggal_awal, $tanggal_akhir)
	{		
		$this->db->select('
							rpm.id_retur_pembelian_m, no_retur_pembelian, no_pembelian, sp.foto, 
							IFNULL(sp.nama_supplier,"-") AS nama_supplier,
							rpm.tanggal_retur, total, ppn, biaya_lain, grand_total, 
							count(id_retur_pembelian_d) AS jml_barang, rpm.status_retur,
							rpm.keterangan_lain, pp1.nama_pegawai AS pegawai_save, pp2.nama_pegawai AS pegawai_edit, 
							rpm.tanggal_pembuatan, rpm.tanggal_pembaharuan
						 ');
		$this->db->from('vamr4846_vama.retur_pembelian_master AS rpm');
		$this->db->join('vamr4846_vama.retur_pembelian_detail AS rpd','rpd.id_retur_pembelian_m=rpm.id_retur_pembelian_m','LEFT');
		$this->db->join('vamr4846_vama.supplier as sp','sp.id_supplier=rpm.id_supplier','LEFT');
		$this->db->join('vamr4846_vama.pegawai_pusat as pp1','pp1.id_pegawai=rpm.id_pegawai_pembuatan','LEFT');
		$this->db->join('vamr4846_vama.pegawai_pusat as pp2','pp2.id_pegawai=rpm.id_pegawai_pembaharuan','LEFT');
		$this->db->where('DATE(rpm.tanggal_retur) 
						 BETWEEN "'.date('Y-m-d', strtotime($tanggal_awal)).'" 
						 AND "'.date('Y-m-d', strtotime($tanggal_akhir)).'"');		
		$this->db->group_by('rpm.id_retur_pembelian_m');

		$i = 0;
	
		foreach ($this->column_search as $item){
			if($_POST['search']['value']){
				if($i===0){
					$this->db->group_start();
					$this->db->like($item, $_POST['search']['value']);
				}else{
					$this->db->or_like($item, $_POST['search']['value']);
				}

				if(count($this->column_search) - 1 == $i)
					$this->db->group_end();
			}
			$i++;
		}

		if(isset($_POST['order'])){
			if($this->session->userdata('usergroup_name') == 'Super Admin'){
				$this->db->order_by($this->column_order_a[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
			}else{
				$this->db->order_by($this->column_order_b[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
			}
		}else if(isset($this->order)){
			$order = $this->order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}

	private function _get_laporan_query_detail($tanggal_awal, $tanggal_akhir)
	{		
		$this->db->select('
							rpm.id_retur_pembelian_m, rpd.id_retur_pembelian_d, rpm.no_retur_pembelian, rpm.no_pembelian,
							sp.foto, IFNULL(sp.nama_supplier,"-") AS nama_supplier, DATE(rpm.tanggal_pembuatan) AS tanggal_retur, rpm.status_retur,
							bp.sku, bp.nama_barang, rpd.jumlah_beli, rpd.jumlah_retur, rpd.harga_satuan, rpd.subtotal, rpd.dari_stok, rpd.keterangan,
							pp1.nama_pegawai AS pegawai_save, IFNULL(pp2.nama_pegawai, "-") AS pegawai_edit, rpd.tanggal_pembuatan, rpd.tanggal_pembaharuan
						 ');
		$this->db->from('vamr4846_vama.retur_pembelian_detail AS rpd');
		$this->db->join('vamr4846_vama.retur_pembelian_master as rpm','rpd.id_retur_pembelian_m=rpm.id_retur_pembelian_m','LEFT');
		$this->db->join('vamr4846_vama.barang_pusat as bp','bp.id_barang_pusat=rpd.id_barang_pusat','LEFT');
		$this->db->join('vamr4846_vama.supplier as sp','sp.id_supplier=rpm.id_supplier','LEFT');
		$this->db->join('vamr4846_vama.pegawai_pusat as pp1','pp1.id_pegawai=rpd.id_pegawai_pembuatan','LEFT');
		$this->db->join('vamr4846_vama.pegawai_pusat as pp2','pp2.id_pegawai=rpd.id_pegawai_pembaharuan','LEFT');
		$this->db->where('DATE(rpd.tanggal_pembuatan) 
						  BETWEEN "'. date('Y-m-d', strtotime($tanggal_awal)). '" 
						  AND "'. date('Y-m-d', strtotime($tanggal_akhir)).'"');

		$i = 0;	
		foreach ($this->column_search_detail as $item){
			if($_POST['search']['value']){
				if($i===0){
					$this->db->group_start();
					$this->db->like($item, $_POST['search']['value']);
				}else{
					$this->db->or_like($item, $_POST['search']['value']);
				}

				if(count($this->column_search_detail) - 1 == $i)
					$this->db->group_end();
			}
			$i++;
		}
		
		if(isset($_POST['order'])){
			if($this->session->userdata('usergroup_name') == 'Super Admin'){
				$this->db->order_by($this->column_order_detail_a[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
			}else{
				$this->db->order_by($this->column_order_detail_b[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
			}
		}else if(isset($this->order_detail)){
			$order = $this->order_detail;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}

	private function _get_laporan_query_batal($tanggal_awal, $tanggal_akhir)
	{		
		$this->db->select('
							rpdb.id_retur_pembelian_m, rpdb.id_retur_pembelian_d, rpdb.no_retur_pembelian, rpdb.no_pembelian, 
							sp.foto, IFNULL(sp.nama_supplier,"-") AS nama_supplier, DATE(rpdb.tanggal_pembuatan) AS tanggal_retur,
							bp.sku, bp.nama_barang, rpdb.jumlah_retur, rpdb.harga_satuan, rpdb.subtotal, rpdb.dari_stok, 
							rpdb.keterangan, rpdb.keterangan_batal,
							pp1.nama_pegawai AS pegawai_save, IFNULL(pp2.nama_pegawai, "-") AS pegawai_edit, IFNULL(pp3.nama_pegawai, "-") AS pegawai_pembatalan,
							rpdb.tanggal_pembuatan, rpdb.tanggal_pembaharuan, rpdb.tanggal_pembatalan
						 ');
		$this->db->from('vamr4846_vama.retur_pembelian_detail_batal AS rpdb');
		$this->db->join('vamr4846_vama.barang_pusat as bp','bp.id_barang_pusat=rpdb.id_barang_pusat','LEFT');
		$this->db->join('vamr4846_vama.supplier as sp','sp.id_supplier=rpdb.id_supplier','LEFT');
		$this->db->join('vamr4846_vama.pegawai_pusat as pp1','pp1.id_pegawai=rpdb.id_pegawai_pembuatan','LEFT');
		$this->db->join('vamr4846_vama.pegawai_pusat as pp2','pp2.id_pegawai=rpdb.id_pegawai_pembaharuan','LEFT');
		$this->db->join('vamr4846_vama.pegawai_pusat as pp3','pp3.id_pegawai=rpdb.id_pegawai_pembatalan','LEFT');
		$this->db->where('DATE(rpdb.tanggal_pembatalan) 
						  BETWEEN "'. date('Y-m-d', strtotime($tanggal_awal)). '" 
						  AND "'. date('Y-m-d', strtotime($tanggal_akhir)).'"');

		$i = 0;	
		foreach ($this->column_search_batal as $item){
			if($_POST['search']['value']){
				if($i===0){
					$this->db->group_start();
					$this->db->like($item, $_POST['search']['value']);
				}else{
					$this->db->or_like($item, $_POST['search']['value']);
				}

				if(count($this->column_search_batal) - 1 == $i)
					$this->db->group_end();
			}
			$i++;
		}
		
		if(isset($_POST['order'])){
			if($this->session->userdata('usergroup_name') == 'Super Admin'){
				$this->db->order_by($this->column_order_batal_a[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
			}else{
				$this->db->order_by($this->column_order_batal_b[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
			}
		}else if(isset($this->order_batal)){
			$order = $this->order_batal;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}

	function get_laporan($perintah, $tanggal_awal, $tanggal_akhir)
	{
		$this->$perintah($tanggal_awal, $tanggal_akhir);
		if($_POST['length'] != -1)
		$this->db->limit($_POST['length'], $_POST['start']);
		$query = $this->db->get();
		return $query->result();
	}

	function count_filtered($perintah, $tanggal_awal, $tanggal_akhir)
	{
		$this->$perintah($tanggal_awal, $tanggal_akhir);
		$query = $this->db->get();
		return $query->num_rows();
	}

	public function count_all($table, $tanggal_awal, $tanggal_akhir)
	{
		$this->db->from($table);
		if($table == 'vamr4846_vama.retur_pembelian_master' or $table == 'retur_pembelian_detail'){
			$this->db->where('DATE(tanggal_pembuatan) 
							  BETWEEN "'. date('Y-m-d', strtotime($tanggal_awal)). '" and "'. date('Y-m-d', strtotime($tanggal_akhir)).'"');
		}else if($table == 'vamr4846_vama.retur_pembelian_batal'){
			$this->db->where('DATE(tanggal_pembatalan) 
							  BETWEEN "'. date('Y-m-d', strtotime($tanggal_awal)). '" and "'. date('Y-m-d', strtotime($tanggal_akhir)).'"');
		}
		return $this->db->count_all_results();
	}

	function no_retur_pembelian_baru($tahun_sekarang, $bulan_sekarang, $id_pegawai) {
	    $this->db->select('CAST(RIGHT(no_retur_pembelian, 4)AS INT) + 1 AS no_baru');
	    $this->db->from('vamr4846_vama.retur_pembelian_master');
	    $this->db->where(array(  
			'YEAR(tanggal_retur)'  => $tahun_sekarang,
			'MONTH(tanggal_retur)' => $bulan_sekarang,
			'id_pegawai_pembuatan' => $id_pegawai
	    ));
	    $this->db->order_by('tanggal_pembuatan','DESC');
	    $query = $this->db->get();
	    return $query->row();
	}

	function no_retur_pembelian_detail_baru($id_retur_pembelian_m) {
		$this->db->select('CAST(RIGHT(no_retur_pembelian, 4)AS INT) + 1 AS no_retur_pembelian_detail_baru');
		$this->db->from('vamr4846_vama.retur_pembelian_detail');
		$this->db->where('id_retur_pembelian_m',$id_retur_pembelian_m);
		$this->db->order_by('tanggal_pembuatan','DESC');
		$query = $this->db->get();
		return $query->row();
	}

	function insert_master($no_retur_pembelian, $no_pembelian, $id_supplier, $total, $biaya_lain, $ppn, $grand_total, $catatan, $tanggal, $id_pegawai){
		$dt = array(
			'no_retur_pembelian'   => $no_retur_pembelian,
			'no_pembelian'         => $no_pembelian,
			'id_supplier'          => $id_supplier,
			'total'                => $total,
			'biaya_lain'           => $biaya_lain,
			'ppn'                  => $ppn,
			'grand_total'          => $grand_total,
			'keterangan_lain'      => $catatan,
			'tanggal_retur'        => $tanggal,
			'id_pegawai_pembuatan' => (empty($id_pegawai)) ? NULL : $id_pegawai,
			'tanggal_pembuatan'    => date('Y-m-d H:i:s')
		);
		return $this->db->insert('vamr4846_vama.retur_pembelian_master', $dt);
	}

	function update_master($id_retur_pembelian_m, $no_pembelian, $id_supplier, $total, $biaya_lain, $ppn, $grand_total, 
						   $catatan, $id_pegawai, $status_retur)
	{
		$dt = array(
			'no_pembelian'           => $no_pembelian,
			'id_supplier'            => $id_supplier,
			'total'                  => $total,
			'biaya_lain'             => $biaya_lain,
			'ppn'                    => $ppn,
			'grand_total'            => $grand_total,
			'keterangan_lain'        => $catatan,
			'status_retur'           => $status_retur,
			'id_pegawai_pembaharuan' => (empty($id_pegawai)) ? NULL : $id_pegawai
		);
		$where = array('id_retur_pembelian_m' => $id_retur_pembelian_m);
		return $this->db->update('vamr4846_vama.retur_pembelian_master', $dt, $where);
	}

	function get_by_no_trans($no_retur_pembelian)
	{
		$this->db->select('*');
		$this->db->from('vamr4846_vama.retur_pembelian_master');
		$this->db->where('no_retur_pembelian', $no_retur_pembelian);
		$query = $this->db->get();
		return $query->row();
	}

	function get_by_id($id_retur_pembelian_m){
		$this->db->select('rpm.*, sp.kode_supplier, sp.nama_supplier, sp.tipe_supplier');
		$this->db->from('vamr4846_vama.retur_pembelian_master AS rpm');
		$this->db->join('vamr4846_vama.supplier AS sp', 'sp.id_supplier=rpm.id_supplier', 'LEFT');
		$this->db->where('id_retur_pembelian_m', $id_retur_pembelian_m);
		$query = $this->db->get();
		return $query->row();
	}

	function get_jumlah_jual($id_retur_pembelian_m){
		$this->db->select('count(*) as jumlah_jual');
		$this->db->from('vamr4846_vama.retur_pembelian_detail');
		$this->db->where('id_retur_pembelian_m', $id_retur_pembelian_m);
		$query = $this->db->get();
		return $query->row();
	}

	function get_master1($id_retur_pembelian_m){
		$this->db->select('
			rpm.*, 
			COUNT(id_retur_pembelian_d) AS jumlah_barang, format(grand_total,0) AS grand_total_tampil,
			IFNULL(sp.kode_supplier,"-") AS kode_supplier, IFNULL(sp.nama_supplier,"-") AS nama_supplier,
			IFNULL(sp.email,"-") AS email_supplier, IFNULL(sp.tipe_supplier,"-") AS tipe_supplier, status_retur');
		$this->db->from('vamr4846_vama.retur_pembelian_master AS rpm');
		$this->db->join('vamr4846_vama.retur_pembelian_detail AS rpd', 'rpm.id_retur_pembelian_m=rpd.id_retur_pembelian_m', 'LEFT');		
		$this->db->join('vamr4846_vama.supplier as sp', 'rpm.id_supplier=sp.id_supplier', 'LEFT');
		$this->db->where('rpm.id_retur_pembelian_m', $id_retur_pembelian_m);
		$query = $this->db->get();
		return $query->row();
	}

	function get_master2($id_retur_pembelian_m){
		$this->db->select('
			rpm.id_retur_pembelian_m, rpm.no_retur_pembelian, rpm.no_pembelian, count(id_retur_pembelian_d) as jumlah_barang, 
			rpm.id_supplier, IFNULL(sp.kode_supplier,"-") AS kode_supplier, IFNULL(sp.nama_supplier,"-") AS nama_supplier,
			IFNULL(sp.email,"-") AS email_supplier, IFNULL(sp.tipe_supplier,"-") AS tipe_supplier, 
			rpm.status_retur, rpm.keterangan_lain, rpm.tanggal_retur, pp.nama_pegawai
		');
		$this->db->from('vamr4846_vama.retur_pembelian_master AS rpm');
		$this->db->join('vamr4846_vama.retur_pembelian_detail AS rpd', 'rpm.id_retur_pembelian_m=rpd.id_retur_pembelian_m', 'LEFT');		
		$this->db->join('vamr4846_vama.supplier as sp', 'rpm.id_supplier=sp.id_supplier', 'LEFT');
		$this->db->join('vamr4846_vama.pegawai_pusat as pp', 'rpm.id_pegawai_pembaharuan=pp.id_pegawai', 'LEFT');
		$this->db->where('rpm.id_retur_pembelian_m', $id_retur_pembelian_m);
		$query = $this->db->get();
		return $query->row();
	}

	function get_jumlah_batal($id_retur_pembelian_m)
	{
		$this->db->select('id_retur_pembelian_m, count(id_retur_pembelian_b) as jumlah_barang');
		$this->db->from('vamr4846_vama.retur_pembelian_detail_batal');
		$this->db->where('id_retur_pembelian_m', $id_retur_pembelian_m);
		$query = $this->db->get();
		return $query->row();
	}

	function hapus_transaksi($id_retur_pembelian_m, $id_supplier, $no_retur_pembelian, $no_pembelian, $keterangan_batal, $id_pegawai_pembatalan)
	{
		// Filter data retur_pembelian detail berdasarkan id retur_pembelian master
		$loop = $this->db
			->select('*')
			->where('id_retur_pembelian_m', $id_retur_pembelian_m)
			->get('vamr4846_vama.retur_pembelian_detail');

		foreach($loop->result() as $a){
			// Kembalikan stok berdasarkan pengambilan stok dari stok jual atau rusak
			if($a->dari_stok == 'JUAL'){
				$sql_update_stok = "UPDATE vamr4846_vama.barang_pusat SET total_stok = total_stok + ".$a->jumlah_retur." 
					WHERE id_barang_pusat = '".$a->id_barang_pusat."'";
			}else if($a->dari_stok == 'RUSAK'){
				$sql_update_stok = "UPDATE vamr4846_vama.barang_pusat SET total_stok_rusak = total_stok_rusak + ".$a->jumlah_retur." 
					WHERE id_barang_pusat = '".$a->id_barang_pusat."'";
			}
			$this->db->query($sql_update_stok);

			// Simpan retur_pembelian detail ke retur_pembelian detail batal
			$sql_insert_batal = "
					INSERT INTO vamr4846_vama.retur_pembelian_detail_batal 
						(id_retur_pembelian_d, id_retur_pembelian_m, id_supplier, no_retur_pembelian, no_pembelian,
						 id_barang_pusat, harga_satuan, jumlah_beli, jumlah_retur, subtotal, dari_stok, keterangan, 
						 id_pegawai_pembuatan, id_pegawai_pembaharuan, id_pegawai_pembatalan, 
						 tanggal_pembuatan, tanggal_pembaharuan, keterangan_batal) 
					VALUES
						('".$a->id_retur_pembelian_d."', '".$a->id_retur_pembelian_m."', 
						 '".$id_supplier."', '".$no_retur_pembelian."', '".$no_pembelian."',
						 '".$a->id_barang_pusat."', '".$a->harga_satuan."', '".$a->jumlah_beli."', '".$a->jumlah_retur."', 
						 '".$a->subtotal."', '".$a->dari_stok."', '".$a->keterangan."',   
						 '".$a->id_pegawai_pembuatan."', '".$a->id_pegawai_pembaharuan."', '".$id_pegawai_pembatalan."', 
						 '".$a->tanggal_pembuatan."', '".$a->tanggal_pembaharuan."', '".$keterangan_batal."') 
						";
			$this->db->query($sql_insert_batal);
		}

		// Hapus retur_pembelian detail dan retur_pembelian master
		$this->db
			->where('id_retur_pembelian_m', $id_retur_pembelian_m)
			->delete('vamr4846_vama.retur_pembelian_detail');
			
		return $this->db
			->where('id_retur_pembelian_m', $id_retur_pembelian_m)
			->delete('vamr4846_vama.retur_pembelian_master');
	}

	function get_grand_total($tanggal_awal, $tanggal_akhir)
	{
		$this->db->select('
			COUNT(id_retur_pembelian_m) AS jml_transaksi, 
			SUM(grand_total) AS grand_total_retur_pembelian
		');
		$this->db->from('vamr4846_vama.retur_pembelian_master');
		$this->db->where('DATE(tanggal_retur) BETWEEN "'. date('Y-m-d', strtotime($tanggal_awal)). '" and "'. date('Y-m-d', strtotime($tanggal_akhir)).'"');
		$query = $this->db->get();
		return $query->row();
	}

	function get_total_perbulan($tanggal_awal, $tanggal_akhir)
	{
		$this->db->select('
			IFNULL(MIN(DATE_FORMAT(tanggal_retur, "%d-%m-%y")),"00-00-00") AS tanggal_awal,
			IFNULL(MAX(DATE_FORMAT(tanggal_retur, "%d-%m-%y")),"00-00-00") AS tanggal_akhir,
			COUNT(id_retur_pembelian_m) AS jml_transaksi, 
			IFNULL(SUM(grand_total),0) AS grand_total
		');
		$this->db->from('vamr4846_vama.retur_pembelian_master');
		$this->db->where(array(
			'tanggal_retur >' => $tanggal_awal,
			'tanggal_retur <=' => $tanggal_akhir
		));
		$query = $this->db->get();
		return $query->row();
	}
}
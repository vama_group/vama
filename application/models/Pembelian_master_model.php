<?php
class Pembelian_master_model extends CI_Model
{
	// Pembelian induk
	var $column_order         = array('pm.id_pembelian_m', 'pm.id_pembelian_m', 'pm.no_pembelian', 'pm.no_faktur', 
									  'pm.tanggal', 'sp.nama_supplier', '', 'pm.total', 'ppn', 'biaya_lain', 
									  'grand_total', 'pm.status_pembelian');
	var $column_search        = array('pm.no_pembelian', 'pm.no_faktur', 'sp.nama_supplier', 
									  'pm.total', 'pm.ppn', 'pm.biaya_lain', 'pm.grand_total', 'pm.status_pembelian'); 
	var $order                = array('id_pembelian_m' => 'DESC');
	// --------------------------------------------------------------------------------------------------------------------------------------
	
	// Pembelian detail
	var $column_order_detail  = array('pd.id_pembelian_d', 'pd.id_pembelian_d', 'pm.no_pembelian', 'pm.status_pembelian', 
									  'pm.tanggal', 'sp.nama_supplier', 'bp.sku', 'bp.nama_barang', 'pd.jumlah_beli',
									  'pd.harga', 'pd.resistensi_harga', 'pd.harga_bersih', 'pd.subtotal');
	var $column_search_detail = array('pd.id_pembelian_d', 'pm.no_pembelian', 'pm.no_faktur', 'sp.nama_supplier',
									  'bp.sku', 'bp.nama_barang', 'pm.status_pembelian'); 
	var $order_detail         = array('pd.id_pembelian_m' => 'DESC',
									  'pd.id_pembelian_d' => 'DESC');
	// --------------------------------------------------------------------------------------------------------------------------------------
	
	// Pembelian batal
	var $column_order_batal   = array('pdb.id_pembelian_b', 'pdb.id_pembelian_d', 'pdb.no_pembelian', 'pdb.no_faktur', 
									  'sp.nama_supplier', 'bp.sku', 'bp.nama_barang', 'pdb.jumlah_beli', 
									  'pdb.harga', 'pdb.resistensi_harga', 'pdb.harga_bersih', 'pdb.subtotal', 
									  'pdb.keterangan_batal', 'pp3.nama_pegawai');
	var $column_search_batal  = array('pdb.id_pembelian_b', 'pdb.no_pembelian', 'pdb.no_faktur', 
									  'sp.nama_supplier', 'bp.sku', 'bp.nama_barang', 'pdb.keterangan_batal', 'pp3.nama_pegawai'); 
	var $order_batal          = array('pdb.no_pembelian' 	=> 'DESC',
									  'pdb.id_pembelian_b' 	=> 'DESC');
	// --------------------------------------------------------------------------------------------------------------------------------------
	
	private function _get_laporan_query($tanggal_awal, $tanggal_akhir)
	{		
		$this->db->select('
							pm.id_pembelian_m, no_pembelian, no_faktur, sp.foto, IFNULL(sp.nama_supplier,"-") AS nama_supplier,
							pm.tanggal, total, ppn, biaya_lain, grand_total, status_pembelian, count(id_pembelian_d) AS jml_barang,
							pm.keterangan_lain, pp1.nama_pegawai AS pegawai_save, 
							pp2.nama_pegawai AS pegawai_edit, pm.tanggal_pembuatan, pm.tanggal_pembaharuan
						 ');
		$this->db->from('vamr4846_vama.pembelian_master AS pm');
		$this->db->join('vamr4846_vama.pembelian_detail as pd','pd.id_pembelian_m=pm.id_pembelian_m','LEFT');
		$this->db->join('vamr4846_vama.supplier as sp','sp.id_supplier=pm.id_supplier','LEFT');
		$this->db->join('vamr4846_vama.pegawai_pusat as pp1','pp1.id_pegawai=pm.id_pegawai_pembuatan','LEFT');
		$this->db->join('vamr4846_vama.pegawai_pusat as pp2','pp2.id_pegawai=pm.id_pegawai_pembaharuan','LEFT');
		$this->db->where('DATE(pm.tanggal) 
						  BETWEEN "'.date('Y-m-d', strtotime($tanggal_awal)).'" 
						  AND "'.date('Y-m-d', strtotime($tanggal_akhir)).'"');
		$this->db->group_by('pm.id_pembelian_m');

		$i = 0;	
		foreach ($this->column_search as $item){
			if($_POST['search']['value']){
				if($i===0){
					$this->db->group_start();
					$this->db->like($item, $_POST['search']['value']);
				}else{
					$this->db->or_like($item, $_POST['search']['value']);
				}

				if(count($this->column_search) - 1 == $i)
					$this->db->group_end();
			}
			$i++;
		}
		
		if(isset($_POST['order'])){
			$this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		}else if(isset($this->order)){
			$order = $this->order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}

	private function _get_laporan_query_detail($tanggal_awal, $tanggal_akhir)
	{		
		$this->db->select('
							pm.id_pembelian_m, pd.id_pembelian_d, pm.no_pembelian, pm.no_faktur, 
							sp.foto, IFNULL(sp.nama_supplier,"-") AS nama_supplier, pm.tanggal, pm.status_pembelian,
							bp.sku, bp.nama_barang, pd.jumlah_beli, pd.harga, pd.resistensi_harga, pd.harga_bersih, pd.subtotal,
							pp1.nama_pegawai AS pegawai_save, pp2.nama_pegawai AS pegawai_edit, pd.tanggal_pembuatan, pd.tanggal_pembaharuan
						 ');
		$this->db->from('vamr4846_vama.pembelian_detail AS pd');
		$this->db->join('vamr4846_vama.pembelian_master as pm','pd.id_pembelian_m=pm.id_pembelian_m','LEFT');
		$this->db->join('vamr4846_vama.barang_pusat as bp','bp.id_barang_pusat=pd.id_barang_pusat','LEFT');
		$this->db->join('vamr4846_vama.supplier as sp','sp.id_supplier=pm.id_supplier','LEFT');
		$this->db->join('vamr4846_vama.pegawai_pusat as pp1','pp1.id_pegawai=pd.id_pegawai_pembuatan','LEFT');
		$this->db->join('vamr4846_vama.pegawai_pusat as pp2','pp2.id_pegawai=pd.id_pegawai_pembaharuan','LEFT');
		$this->db->where('DATE(pd.tanggal_pembuatan) 
						  BETWEEN "'. date('Y-m-d', strtotime($tanggal_awal)). '" and "'. date('Y-m-d', strtotime($tanggal_akhir)).'"');

		$i = 0;	
		foreach ($this->column_search_detail as $item){
			if($_POST['search']['value']){
				if($i===0){
					$this->db->group_start();
					$this->db->like($item, $_POST['search']['value']);
				}else{
					$this->db->or_like($item, $_POST['search']['value']);
				}

				if(count($this->column_search_detail) - 1 == $i)
					$this->db->group_end();
			}
			$i++;
		}
		
		if(isset($_POST['order'])){
			$this->db->order_by($this->column_order_detail[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		}else if(isset($this->order_detail)){
			$order = $this->order_detail;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}

	private function _get_laporan_query_batal($tanggal_awal, $tanggal_akhir)
	{		
		$this->db->select('
							pdb.id_pembelian_m, pdb.id_pembelian_d, pdb.no_pembelian, pdb.no_faktur, 
							sp.foto, IFNULL(sp.nama_supplier,"-") AS nama_supplier, pdb.keterangan_batal,
							bp.sku, bp.nama_barang, pdb.jumlah_beli, pdb.harga, pdb.resistensi_harga, pdb.harga_bersih, pdb.subtotal,
							pp1.nama_pegawai AS pegawai_save, pp2.nama_pegawai AS pegawai_edit, pp3.nama_pegawai AS pegawai_pembatalan, 
							pdb.tanggal_pembuatan, pdb.tanggal_pembaharuan, pdb.tanggal_pembatalan
						 ');
		$this->db->from('vamr4846_vama.pembelian_detail_batal AS pdb');
		$this->db->join('vamr4846_vama.barang_pusat as bp','bp.id_barang_pusat=pdb.id_barang_pusat','LEFT');
		$this->db->join('vamr4846_vama.supplier as sp','sp.id_supplier=pdb.id_supplier','LEFT');
		$this->db->join('vamr4846_vama.pegawai_pusat as pp1','pp1.id_pegawai=pdb.id_pegawai_pembuatan','LEFT');
		$this->db->join('vamr4846_vama.pegawai_pusat as pp2','pp2.id_pegawai=pdb.id_pegawai_pembaharuan','LEFT');
		$this->db->join('vamr4846_vama.pegawai_pusat as pp3','pp3.id_pegawai=pdb.id_pegawai_pembatalan','LEFT');
		$this->db->where('DATE(pdb.tanggal_pembatalan) 
						  BETWEEN "'. date('Y-m-d', strtotime($tanggal_awal)). '" and "'. date('Y-m-d', strtotime($tanggal_akhir)).'"');

		$i = 0;	
		foreach ($this->column_search_batal as $item){
			if($_POST['search']['value']){
				if($i===0){
					$this->db->group_start();
					$this->db->like($item, $_POST['search']['value']);
				}else{
					$this->db->or_like($item, $_POST['search']['value']);
				}

				if(count($this->column_search_batal) - 1 == $i)
					$this->db->group_end();
			}
			$i++;
		}
		
		if(isset($_POST['order'])){
			$this->db->order_by($this->column_order_batal[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		}else if(isset($this->order_batal)){
			$order = $this->order_batal;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}

	function get_laporan($perintah, $tanggal_awal, $tanggal_akhir)
	{
		$this->$perintah($tanggal_awal, $tanggal_akhir);
		if($_POST['length'] != -1)
		$this->db->limit($_POST['length'], $_POST['start']);
		$query = $this->db->get();
		return $query->result();
	}

	function count_filtered($perintah, $tanggal_awal, $tanggal_akhir)
	{
		$this->$perintah($tanggal_awal, $tanggal_akhir);
		$query = $this->db->get();
		return $query->num_rows();
	}

	public function count_all($table, $tanggal_awal, $tanggal_akhir)
	{
		$this->db->from($table);
		if($table == 'vamr4846_vama.pembelian_master'){
			$this->db->where('DATE(tanggal) 
							  BETWEEN "'. date('Y-m-d', strtotime($tanggal_awal)). '" and "'. date('Y-m-d', strtotime($tanggal_akhir)).'"');
		}else if($table == 'vamr4846_vama.pembelian_detail'){
			$this->db->where('DATE(tanggal_pembuatan) 
							  BETWEEN "'. date('Y-m-d', strtotime($tanggal_awal)). '" and "'. date('Y-m-d', strtotime($tanggal_akhir)).'"');
		}else if($table == 'vamr4846_vama.pembelian_detail_batal'){
			$this->db->where('DATE(tanggal_pembatalan) 
							  BETWEEN "'. date('Y-m-d', strtotime($tanggal_awal)). '" and "'. date('Y-m-d', strtotime($tanggal_akhir)).'"');
		}
		return $this->db->count_all_results();
	}

	function no_pembelian_baru($tahun_sekarang, $bulan_sekarang, $id_pegawai) {
	    $this->db->select('CAST(RIGHT(no_pembelian,4)AS INT) + 1 AS no_baru');
	    $this->db->from('vamr4846_vama.pembelian_master');
	    $this->db->where(array(  
			'YEAR(tanggal)'        => $tahun_sekarang,
			'MONTH(tanggal)'       => $bulan_sekarang,
			'id_pegawai_pembuatan' => $id_pegawai
		));
	    $this->db->order_by('no_baru', 'DESC');
	    $query = $this->db->get();
	    return $query->row();
	}

	function no_pembelian_detail_baru($id_pembelian_m) {
		$this->db->select('COUNT(*)+1 AS no_pembelian_detail_baru');
		$this->db->from('vamr4846_vama.pembelian_detail');
		$this->db->where('id_pembelian_m', $id_pembelian_m);
		$this->db->order_by('tanggal_pembuatan','DESC');
		$query = $this->db->get();
		return $query->row();
	}

	function insert_master($no_pembelian, $no_faktur, $id_supplier, $total, $biaya_lain, $ppn, $grand_total, $catatan, $tanggal, $id_pegawai)
	{
		$dt = array(
			'no_pembelian'         => $no_pembelian,
			'no_faktur'            => $no_faktur,
			'id_supplier'          => $id_supplier,
			'total'                => $total,
			'biaya_lain'           => $biaya_lain,
			'ppn'                  => $ppn,
			'grand_total'          => $grand_total,
			'keterangan_lain'      => $catatan,
			'tanggal'              => $tanggal,
			'id_pegawai_pembuatan' => (empty($id_pegawai)) ? NULL : $id_pegawai,
			'tanggal_pembuatan'    => date('Y-m-d H:i:s')
		);

		return $this->db->insert('vamr4846_vama.pembelian_master', $dt);
	}

	function update_master($id_pembelian_m, $no_faktur, $id_supplier, $total, $biaya_lain, $ppn, $grand_total, 
						   $catatan, $id_pegawai, $status_pembelian)
	{
		$dt = array(
			'no_faktur'              => $no_faktur,
			'id_supplier'            => $id_supplier,
			'total'                  => $total,
			'biaya_lain'             => $biaya_lain,
			'ppn'                    => $ppn,
			'grand_total'            => $grand_total,
			'keterangan_lain'        => $catatan,
			'status_pembelian'       => $status_pembelian,
			'id_pegawai_pembaharuan' => (empty($id_pegawai)) ? NULL : $id_pegawai
		);

		$where = array('id_pembelian_m'	=> $id_pembelian_m);

		return $this->db->update('vamr4846_vama.pembelian_master', $dt, $where);
	}

	function get_by_id($id_pembelian_m)
	{
		$this->db->select('*');
		$this->db->from('vamr4846_vama.pembelian_master');
		$this->db->where('id_pembelian_m', $id_pembelian_m);
		$query = $this->db->get();
		return $query->row();
	}

	function get_by_no_trans($no_pembelian)
	{
		$this->db->select('*');
		$this->db->from('vamr4846_vama.pembelian_master');
		$this->db->where('no_pembelian', $no_pembelian);
		$query = $this->db->get();
		return $query->row();
	}

	function get_jumlah_jual($id_pembelian_m)
	{
		$this->db->select('count(*) as jumlah_jual');
		$this->db->from('vamr4846_vama.pembelian_detail');
		$this->db->where('id_pembelian_m', $id_pembelian_m);
		$query = $this->db->get();
		return $query->row();
	}

	function get_master1($id_pembelian_m)
	{
		$this->db->select('
			pm.*, 
			count(pd.id_pembelian_d) as jumlah_barang, 
			format(pm.grand_total,0) AS grand_total_tampil,
			IFNULL(sp.id_supplier,"0") AS id_supplier, 
			IFNULL(sp.nama_supplier,"-") AS nama_supplier,
			IFNULL(sp.tipe_supplier,"-") AS tipe_supplier, 
			IFNULL(sp.email,"-") AS email_supplier,
			IFNULL(sp.kode_supplier,"-") AS kode_supplier
		');
		$this->db->from('vamr4846_vama.pembelian_master AS pm');
		$this->db->join('vamr4846_vama.pembelian_detail as pd', 'pm.id_pembelian_m=pd.id_pembelian_m', 'LEFT');		
		$this->db->join('vamr4846_vama.supplier as sp', 'pm.id_supplier=sp.id_supplier', 'LEFT');
		$this->db->where('pm.id_pembelian_m', $id_pembelian_m);
		$query = $this->db->get();
		return $query->row();
	}

	function get_master2($id_pembelian_m)
	{
		$this->db->select('
			pm.id_pembelian_m, 
			pm.no_pembelian, 
			pm.no_faktur, 
			pm.id_supplier, 
			pm.status_pembelian,
			count(pd.id_pembelian_d) as jumlah_barang,
			IFNULL(sp.nama_supplier,"-") AS nama_supplier, 
			IFNULL(sp.tipe_supplier,"-") AS tipe_supplier, 
			IFNULL(sp.email,"-") AS email_supplier, 
			IFNULL(sp.kode_supplier,"-") AS kode_supplier,
			pm.keterangan_lain
		');
		$this->db->from('vamr4846_vama.pembelian_master AS pm');
		$this->db->join('vamr4846_vama.pembelian_detail as pd', 'pm.id_pembelian_m=pd.id_pembelian_m', 'LEFT');		
		$this->db->join('vamr4846_vama.supplier as sp', 'pm.id_supplier=sp.id_supplier', 'LEFT');
		$this->db->where('pm.id_pembelian_m', $id_pembelian_m);
		$query = $this->db->get();
		return $query->row();
	}

	function get_jumlah_batal($id_pembelian_m)
	{
		$this->db->select('id_pembelian_m, count(id_pembelian_b) as jumlah_barang');
		$this->db->from('vamr4846_vama.pembelian_detail_batal');
		$this->db->where('id_pembelian_m', $id_pembelian_m);
		$query = $this->db->get();
		return $query->row();
	}

	function hapus_transaksi($id_pembelian_m, $id_supplier, $no_pembelian, $no_faktur, $keterangan_batal, $id_pegawai_pembatalan)
	{
		// Filter data pembelian detail berdasarkan id pembelian master
		$loop = $this->db
					 ->select('*')
					 ->where('id_pembelian_m', $id_pembelian_m)
					 ->get('vamr4846_vama.pembelian_detail');

		foreach($loop->result() as $b)
		{
			// Kembalikan stok
			$sql_update_stok = "UPDATE `vamr4846_vama.barang_pusat` SET `total_stok` = `total_stok` - ".$b->jumlah_beli." 
					WHERE `id_barang_pusat` = '".$b->id_barang_pusat."'";
			$this->db->query($sql_update_stok);

			// Simpan pembelian detail ke pembelian detail batal
			$sql_insert_batal = "
					INSERT INTO vamr4846_vama.pembelian_detail_batal 
					(id_pembelian_d, id_pembelian_m, id_supplier, no_pembelian, no_faktur, id_barang_pusat, harga, resistensi_harga, harga_bersih, 
					 jumlah_beli, subtotal, id_pegawai_pembuatan, id_pegawai_pembaharuan, id_pegawai_pembatalan, 
					 tanggal_pembuatan, tanggal_pembaharuan, keterangan_batal) 
					VALUES
					('".$b->id_pembelian_d."', '".$b->id_pembelian_m."', '".$id_supplier."', '".$no_pembelian."', '".$no_faktur."', '".$b->id_barang_pusat."', 
					 '".$b->harga."', '".$b->resistensi_harga."', '".$b->harga_bersih."', '".$b->jumlah_beli."', '".$b->subtotal."', 
					 '".$b->id_pegawai_pembuatan."', '".$b->id_pegawai_pembaharuan."', '".$id_pegawai_pembatalan."', 
					 '".$b->tanggal_pembuatan."', '".$b->tanggal_pembaharuan."', '".$keterangan_batal."') 
					";
			$this->db->query($sql_insert_batal);
		}

		// Hapus pembelian detail dan pembelian master
		$this->db->where('id_pembelian_m', $id_pembelian_m)->delete('vamr4846_vama.pembelian_detail');
		return $this->db
					->where('id_pembelian_m', $id_pembelian_m)
					->delete('vamr4846_vama.pembelian_master');
	}

	function get_grand_total($tanggal_awal, $tanggal_akhir)
	{
		$this->db->select('
			COUNT(id_pembelian_m) AS jml_transaksi, 
			SUM(grand_total) AS grand_total_pembelian
		');
		$this->db->from('vamr4846_vama.pembelian_master');
		$this->db->where('DATE(tanggal) 
						  BETWEEN "'. date('Y-m-d', strtotime($tanggal_awal)). '" and "'. date('Y-m-d', strtotime($tanggal_akhir)).'"');
		$query = $this->db->get();
		return $query->row();
	}

	function get_total_selesai_perbulan($tanggal_awal, $tanggal_akhir)
	{
		$this->db->select('
			IFNULL(MIN(DATE_FORMAT(tanggal, "%d-%m-%y")),"00-00-00") AS tanggal_awal,
			IFNULL(MAX(DATE_FORMAT(tanggal, "%d-%m-%y")),"00-00-00") AS tanggal_akhir,
			COUNT(id_pembelian_m) AS jml_transaksi, 
			IFNULL(SUM(grand_total),0) AS grand_total
		');
		$this->db->from('vamr4846_vama.pembelian_master');
		$this->db->where(array(
			'tanggal >' 		=> $tanggal_awal,
			'tanggal <=' 		=> $tanggal_akhir,
			'status_pembelian' 	=> 'SELESAI'
		));		
		$query = $this->db->get();
		return $query->row();
	}

	function get_total_menunggu_perbulan($tanggal_awal, $tanggal_akhir)
	{
		$this->db->select('
			IFNULL(MIN(DATE_FORMAT(tanggal, "%d-%m-%y")),"00-00-00") AS tanggal_awal,
			IFNULL(MAX(DATE_FORMAT(tanggal, "%d-%m-%y")),"00-00-00") AS tanggal_akhir,
			COUNT(id_pembelian_m) AS jml_transaksi, 
			IFNULL(SUM(grand_total),0) AS grand_total
		');
		$this->db->from('vamr4846_vama.pembelian_master');
		$this->db->where('status_pembelian', 'MENUNGGU');		
		$query = $this->db->get();
		return $query->row();
	}
}
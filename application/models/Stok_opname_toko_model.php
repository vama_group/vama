<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Stok_opname_toko_model extends MY_Model {

	var $table = 'stok_opname_toko';
	var $column_order = array('stok_opname_toko.kode_barang','barang_pusat.nama_barang','qty'); 	
	var $column_search = array('stok_opname_toko.kode_barang','barang_pusat.nama_barang','qty'); 	
	var $order = array('tanggal_pembaharuan' => 'desc');

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	// Listing data
	public function listing() {
		$this->db->select('stok_opname_toko.*,barang_pusat.sku,barang_pusat.nama_barang,barang_pusat.foto');
		$this->db->from('stok_opname_toko');
		$this->db->join('barang_pusat','barang_pusat.kode_barang=stok_opname_toko.kode_barang','LEFT');
		$this->db->order_by('id_stok_opname_toko','DESC');
		$query=$this->db->get();
		return $query->result();
	}

	private function _get_datatables_query($table)
	{
		
		// $this->db->from($this->table);
		$this->db->from($table);

		$i = 0;
	
		foreach ($this->column_search as $item) // loop column 
		{
			if($_POST['search']['value']) // if datatable send POST for search
			{
				if($i===0) // first loop
				{
					$this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
					$this->db->like($item, $_POST['search']['value']);
				}
				else
				{
					$this->db->or_like($item, $_POST['search']['value']);
				}

				if(count($this->column_search) - 1 == $i) //last loop
					$this->db->group_end(); //close bracket
			}
			$i++;
		}
		
		if(isset($_POST['order'])) // here order processing
		{
			$this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		} 
		else if(isset($this->order))
		{
			$order = $this->order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}

	function get_datatables($table)
	{
		$this->_get_datatables_query($table);
		$this->db->select('stok_opname_toko.*,barang_pusat.sku,barang_pusat.nama_barang,barang_pusat.foto');
		$this->db->join('barang_pusat','barang_pusat.kode_barang=stok_opname_toko.kode_barang','LEFT');
		if($_POST['length'] != -1)
		$this->db->limit($_POST['length'], $_POST['start']);
		$query = $this->db->get();
		return $query->result();
	}

	function get_datatables_riwayat($table)
	{
		$this->_get_datatables_query($table);
		$this->db->select('riwayat_stok_opname_toko.*,barang_pusat.sku,barang_pusat.nama_barang,barang_pusat.foto');
		$this->db->join('barang_pusat','barang_pusat.kode_barang=riwayat_stok_opname_toko.kode_barang','LEFT');
		if($_POST['length'] != -1)
		$this->db->limit($_POST['length'], $_POST['start']);
		$query = $this->db->get();
		return $query->result();
	}

	function count_filtered($table)
	{
		$this->_get_datatables_query($table);
		$query = $this->db->get();
		return $query->num_rows();
	}

	public function count_all($table)
	{
		$this->db->from($table);
		return $this->db->count_all_results();
	}

	public function get_by_id($id_stok_opname_toko)
	{
		$this->db->from($this->table);
		$this->db->select('stok_opname_toko.*,barang_pusat.sku,barang_pusat.nama_barang,barang_pusat.foto');
		$this->db->join('barang_pusat','barang_pusat.kode_barang=stok_opname_toko.kode_barang','LEFT');
		$this->db->where('id_stok_opname_toko',$id_stok_opname_toko);
		$query = $this->db->get();

		return $query->row();
	}

	public function save($data,$table)
	{
		$this->db->insert($table, $data);
		return $this->db->insert_id();
	}

	public function update($where, $data)
	{
		$this->db->update($this->table, $data, $where);
		return $this->db->affected_rows();
	}

	public function delete_by_id($id_stok_opname_toko)
	{
		$this->db->where('id_stok_opname_toko', $id_stok_opname_toko);
		$this->db->delete($this->table);
	}
}

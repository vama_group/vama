<?php
class Perakitan_detail_model extends CI_Model
{
	// Perakitan detail
	var $column_order         = array('', '', 'bp.sku','bp.nama_barang', 'pd.harga_eceran', 'pd.jumlah_komponen');
	var $column_search        = array('bp.kode_barang','bp.sku','bp.nama_barang', 'pd.jumlah_komponen'); 
	var $order                = array('pd.tanggal_pembaharuan' => 'desc'); 
	// -------------------------------------------------------------------------------------------------------------------------------------
	
	// Perakitan batal
	var $column_order_batal   = array('', '', 'bp.sku','bp.nama_barang', 'pdb.keterangan_batal', 'pdb.harga_eceran', 
									  'pdb.jumlah_komponen');
	var $column_search_batal  = array('bp.kode_barang','bp.sku','bp.nama_barang'); 
	var $order_batal          = array('pdb.tanggal_pembatalan' => 'desc'); 
	// -------------------------------------------------------------------------------------------------------------------------------------
	
	// Cari barang komponen
	var $column_order_barang  = array('', '', 'bp.sku','bp.nama_barang', 'bp.total_stok', 'bp.harga_satuan');
	var $column_search_barang = array('bp.kode_barang','bp.sku','bp.nama_barang'); 
	var $order_barang         = array('bp.nama_barang' => 'asc'); 
	// -------------------------------------------------------------------------------------------------------------------------------------

	private function _get_datatables_query($id_perakitan_m)
	{		
		$this->db->select('
			pd.id_perakitan_d, pd.id_perakitan_m, pd.id_barang_pusat,
			bp.sku, bp.kode_barang, bp.nama_barang, bp.harga_eceran, pd.jumlah_komponen,
			pp1.nama_pegawai AS pegawai_save, pp2.nama_pegawai AS pegawai_edit, 
			pd.tanggal_pembuatan, pd.tanggal_pembaharuan
		');
		$this->db->from('vamr4846_vama.perakitan_detail AS pd');
		$this->db->join('vamr4846_vama.barang_pusat as bp', 'bp.id_barang_pusat=pd.id_barang_pusat', 'LEFT');
		$this->db->join('vamr4846_vama.pegawai_pusat as pp1', 'pp1.id_pegawai=pd.id_pegawai_pembuatan', 'LEFT');
		$this->db->join('vamr4846_vama.pegawai_pusat as pp2', 'pp2.id_pegawai=pd.id_pegawai_pembaharuan', 'LEFT');
		$this->db->where('id_perakitan_m', $id_perakitan_m);

		$i = 0;
		foreach ($this->column_search as $item){
			if($_POST['search']['value']){
				if($i===0){
					$this->db->group_start();
					$this->db->like($item, $_POST['search']['value']);
				}else{
					$this->db->or_like($item, $_POST['search']['value']);
				}

				if(count($this->column_search) - 1 == $i)
					$this->db->group_end();
			}
			$i++;
		}
		
		if(isset($_POST['order'])){
			$this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		}else if(isset($this->order)){
			$order = $this->order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}

	private function _get_datatables_query_batal($id_perakitan_m)
	{		
		$this->db->select(' 
			pdb.*,
			bp.sku, bp.kode_barang, bp.nama_barang, bp.harga_eceran,
			pp1.nama_pegawai As pegawai_save, pp2.nama_pegawai AS pegawai_edit, pp3.nama_pegawai AS pegawai_pembatalan
		');
		$this->db->from('vamr4846_vama.perakitan_detail_batal AS pdb');
		$this->db->join('vamr4846_vama.barang_pusat as bp', 'bp.id_barang_pusat=pdb.id_barang_pusat', 'LEFT');
		$this->db->join('vamr4846_vama.pegawai_pusat as pp1', 'pp1.id_pegawai=pdb.id_pegawai_pembuatan', 'LEFT');
		$this->db->join('vamr4846_vama.pegawai_pusat as pp2', 'pp2.id_pegawai=pdb.id_pegawai_pembaharuan', 'LEFT');
		$this->db->join('vamr4846_vama.pegawai_pusat as pp3', 'pp3.id_pegawai=pdb.id_pegawai_pembatalan', 'LEFT');
		$this->db->where('id_perakitan_m', $id_perakitan_m);

		$i = 0;
		foreach ($this->column_search_batal as $item){
			if($_POST['search']['value']){
				if($i===0){
					$this->db->group_start();
					$this->db->like($item, $_POST['search']['value']);
				}else{
					$this->db->or_like($item, $_POST['search']['value']);
				}

				if(count($this->column_search_batal) - 1 == $i)
					$this->db->group_end();
			}
			$i++;
		}
		
		if(isset($_POST['order'])){
			$this->db->order_by($this->column_order_batal[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		}else if(isset($this->order_batal)){
			$order = $this->order_batal;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}

	private function _get_datatables_query_barang($id_perakitan_m, $id_barang_perakitan)
	{				
		$ambil_komponen = $this->db->query("
			SELECT id_barang_pusat 
			FROM vamr4846_vama.perakitan_detail 
			WHERE id_perakitan_m =".$id_perakitan_m."
		");
		$pd_id_barang = array();
		foreach ($ambil_komponen->result() as $row){
			$pd_id_barang[] = $row->id_barang_pusat; 
		}
		$id_barang = implode(",", $pd_id_barang);
		$idp       = explode(",", $id_barang);

		$this->db->select('
							bp.id_barang_pusat, bp.sku, bp.kode_barang, bp.nama_barang, 
							bp.total_stok, bp.harga_eceran
						');
		$this->db->from('vamr4846_vama.barang_pusat as bp');
		$this->db->where_not_in('bp.id_barang_pusat', $id_barang_perakitan);
		$this->db->where_not_in('bp.id_barang_pusat', $idp);
		$i = 0;
	
		foreach ($this->column_search_barang as $item){
			if($_POST['search']['value']){
				if($i===0){
					$this->db->group_start();
					$this->db->like($item, $_POST['search']['value']);
				}else{
					$this->db->or_like($item, $_POST['search']['value']);
				}

				if(count($this->column_search_barang) - 1 == $i)
				$this->db->group_end();
			}
			$i++;
		}
		
		if(isset($_POST['order'])){
			$this->db->order_by($this->column_order_barang[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		}else if(isset($this->order_barang)){
			$order = $this->order_barang;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}

	function get_datatables($perintah, $id_perakitan_m, $id_barang_perakitan)
	{
		$this->$perintah($id_perakitan_m, $id_barang_perakitan);
		if($_POST['length'] != -1)
		$this->db->limit($_POST['length'], $_POST['start']);
		$query = $this->db->get();
		return $query->result();
	}

	function count_filtered($perintah, $id_perakitan_m, $id_barang_perakitan)
	{
		$this->$perintah($id_perakitan_m, $id_barang_perakitan);
		$query = $this->db->get();
		return $query->num_rows();
	}

	public function count_all($table, $id, $id_barang_perakitan)
	{
		$this->db->from($table);
		if($table == 'vamr4846_vama.barang_pusat'){
			// Ambil semua id barang komponen per id_perakitan_m 
			$ambil_komponen = $this->db->query("SELECT id_barang_pusat 
												FROM vamr4846_vama.perakitan_detail 
												WHERE id_perakitan_m =".$id."");
			$pd_id_barang = array();
			foreach ($ambil_komponen->result() as $row){
				$pd_id_barang[] = $row->id_barang_pusat; 
			}
			
			$id_barang = implode(",", $pd_id_barang);
			$idp       = explode(",", $id_barang);

			$this->db->where_not_in('id_barang_pusat', $idp);
			$this->db->where_not_in('id_barang_pusat', $id_barang_perakitan);
		}else{
			$this->db->where('id_perakitan_m', $id);
		}
		return $this->db->count_all_results();
	}

	function get_perakitan_detail($id_perakitan_d)
	{
		$this->db->select('pd.*, pm.no_perakitan, 
						   pm.id_barang_pusat AS id_barang_perakitan, 
						   bpm.sku AS sku_perakitan, bpm.nama_barang AS nama_barang_perakitan, pm.jumlah_perakitan,
						   bpd.sku AS sku_komponen, bpd.nama_barang AS nama_barang_komponen, format(bpd.harga_eceran,0) AS harga_eceran');
		$this->db->from('vamr4846_vama.perakitan_detail as pd');
		$this->db->join('vamr4846_vama.perakitan_master as pm','pm.id_perakitan_m=pd.id_perakitan_m','LEFT');
		$this->db->join('vamr4846_vama.barang_pusat as bpm','bpm.id_barang_pusat=pm.id_barang_pusat','LEFT');
		$this->db->join('vamr4846_vama.barang_pusat as bpd','bpd.id_barang_pusat=pd.id_barang_pusat','LEFT');
		$this->db->where('id_perakitan_d', $id_perakitan_d);
		$query = $this->db->get();
		return $query->row();
	}

	function get_id($id_perakitan_m, $id_barang_komponen)
	{
		$this->db->select('*');
		$this->db->from('vamr4846_vama.perakitan_detail');
		$this->db->where(array(  
			'id_perakitan_m'  => $id_perakitan_m,
			'id_barang_pusat' => $id_barang_komponen
		));
		$query = $this->db->get();
		return $query->row();
	}

	function get_detail_faktur($id_perakitan_m)
	{
		$this->db->select('
			pd.id_perakitan_d, pd.harga_satuan, 
			pd.discount, pd.jumlah_beli, pd.subtotal,
			bp.sku, bp.kode_barang, bp.nama_barang
		');
		$this->db->from('vamr4846_vama.perakitan_detail AS pd');
		$this->db->join('vamr4846_vama.barang_pusat as bp','bp.id_barang_pusat=pd.id_barang_pusat');
		$this->db->where('pd.id_perakitan_m',$id_perakitan_m);
		$this->db->order_by('bp.nama_barang','ASC');
		$query = $this->db->get();
		return $query->result();
	}

	function get_total($id_perakitan_m)
	{
		$this->db->select('
			pm.id_perakitan_m, pm.id_barang_pusat, pm.jumlah_perakitan, 
			IFNULL(COUNT(pd.id_perakitan_d),0) AS jumlah_barang, 
			IFNULL(pdb.jumlah_batal,0) AS jumlah_barang_batal 
		');
		$this->db->from('vamr4846_vama.perakitan_master AS pm');
		$this->db->join('vamr4846_vama.perakitan_detail AS pd', 'pm.id_perakitan_m=pd.id_perakitan_m', 'LEFT');
		$this->db->join('(SELECT id_perakitan_m, COUNT(id_perakitan_b) AS jumlah_batal 
	   					 FROM vamr4846_vama.perakitan_detail_batal GROUP BY id_perakitan_m) AS pdb', 'pm.id_perakitan_m=pdb.id_perakitan_m', 'LEFT');
		$this->db->where('pm.id_perakitan_m', $id_perakitan_m);
		$query = $this->db->get();
		return $query->row();
	}

	function get_detail($id_perakitan_m){
		$this->db->select('*');
		$this->db->from('vamr4846_vama.perakitan_detail');
		$this->db->where('id_perakitan_m', $id_perakitan_m);
		$query = $this->db->get();
		return $query->result();
	}

	function insert_detail($id_perakitan_m, $id_barang_komponen, $jumlah_perakitan, $id_pegawai)
	{ 
		$sql = "UPDATE vamr4846_vama.barang_pusat SET total_stok = total_stok - ".$jumlah_perakitan." 
				WHERE id_barang_pusat = '".$id_barang_komponen."'";
		$this->db->query($sql);

		$dt = array(	
			'id_perakitan_m'       => $id_perakitan_m,
			'id_barang_pusat'      => $id_barang_komponen,
			'jumlah_komponen'      => $jumlah_perakitan,
			'id_pegawai_pembuatan' => $id_pegawai,
			'tanggal_pembuatan'    => date('Y-m-d H:i:s') 		
		);
		return $this->db->insert('vamr4846_vama.perakitan_detail', $dt);
	}

	function update_detail($id_perakitan_m, $id_barang_komponen,  
						   $jumlah_perakitan, $id_pegawai, $jumlah_komponen_lama)
	{
		$sql = "UPDATE vamr4846_vama.barang_pusat SET total_stok = total_stok + ".$jumlah_komponen_lama." 
				WHERE id_barang_pusat = '".$id_barang_komponen."'";
		$this->db->query($sql);

		$sql = "UPDATE vamr4846_vama.barang_pusat SET total_stok = total_stok - ".$jumlah_perakitan." 
				WHERE id_barang_pusat = '".$id_barang_komponen."'";
		$this->db->query($sql);
		
		$dt = array(
			'jumlah_komponen'        => $jumlah_perakitan,
			'id_pegawai_pembaharuan' => $id_pegawai
		);

		$where = array(
			'id_perakitan_m'  => $id_perakitan_m,
			'id_barang_pusat' => $id_barang_komponen
		);

		return $this->db->update('vamr4846_vama.perakitan_detail', $dt, $where);
	}
	
	function hapus_perakitan_detail($id_perakitan_d, $id_perakitan_m, $id_barang_perakitan, $no_perakitan,
								    $id_barang_pusat, $jumlah_komponen,
									$id_pegawai_pembuatan, $id_pegawai_pembaharuan, $id_pegawai_pembatalan, 
									$tanggal_pembuatan, $tanggal_pembaharuan, $keterangan_batal)
	{
		
		// Kembalikan stok berdasarkan jumlah beli
		$sql = "UPDATE vamr4846_vama.barang_pusat SET total_stok = total_stok + ".$jumlah_komponen." 
				WHERE id_barang_pusat = '".$id_barang_pusat."'";
		$this->db->query($sql);

		// Simpan perakitan detail ke perakitan detail batal
		$sql = "
				INSERT INTO vamr4846_vama.perakitan_detail_batal 
					(id_perakitan_d, id_perakitan_m, no_perakitan, id_barang_perakitan, 
					 id_barang_pusat, jumlah_komponen, 
					 id_pegawai_pembuatan, id_pegawai_pembaharuan, id_pegawai_pembatalan, 
					 tanggal_pembuatan, tanggal_pembaharuan, keterangan_batal) 
				VALUES
					('".$id_perakitan_d."', '".$id_perakitan_m."', '".$no_perakitan."', '".$id_barang_perakitan."', 
					 '".$id_barang_pusat."', '".$jumlah_komponen."', 
					 '".$id_pegawai_pembuatan."', '".$id_pegawai_pembaharuan."', '".$id_pegawai_pembatalan."', 
					 '".$tanggal_pembuatan."', '".$tanggal_pembaharuan."', '".$keterangan_batal."') 
					";
		$this->db->query($sql);

		// Hapus perakitan detail
		return $this->db
					->where('id_perakitan_d', $id_perakitan_d)
					->delete('vamr4846_vama.perakitan_detail');
	}
}
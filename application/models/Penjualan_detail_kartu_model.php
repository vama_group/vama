<?php
class Penjualan_detail_kartu_model extends CI_Model
{
	var $column_order = array(
		'pdk.id_penjualan_m', 'pdk.id_kartu_edc_pusat', 'kep.nama_bank', 'pdk.no_kartu_customer', 'pdk.jumlah_pembayaran', 'pdk.tanggal_pembaharuan'
	);
	var $column_search = array(
		'pdk.id_penjualan_m', 'pdk.id_kartu_edc_pusat', 'kep.nama_bank', 'pdk.no_kartu_customer', 'pdk.jumlah_pembayaran', 'pdk.tanggal_pembaharuan'
	); 
	var $order = array('pdk.tanggal_pembaharuan' => 'desc'); 

	function get_penjualan_detail_debit($id_penjualan_debit)
	{
		$this->db->select('*');
		$this->db->from('vamr4846_vama.penjualan_detail_kartu');
		$this->db->where(array(
							'id_penjualan_debit'=>$id_penjualan_debit
		            	));
		$query = $this->db->get();
		return $query->row();
	}

	function get_id($id_penjualan_m, $id_kartu_edc_pusat)
	{
		$this->db->select('*');
		$this->db->from('vamr4846_vama.penjualan_detail_kartu');
		$this->db->where(array(  
						 	'id_penjualan_m'		=> $id_penjualan_m,
						 	'id_kartu_edc_pusat' 	=> $id_kartu_edc_pusat
		            	));
		$query = $this->db->get();
		return $query->row();
	}	

	function insert_detail_debit($id_penjualan_m, $id_kartu_edc_pusat, $no_kartu_customer, $jumlah_pembayaran, $id_pegawai)
	{ 
		$dt = array(
				'id_penjualan_m' 			=> $id_penjualan_m,
				'id_kartu_edc_pusat' 		=> $id_kartu_edc_pusat,
				'no_kartu_customer'			=> $no_kartu_customer,
				'jumlah_pembayaran'			=> $jumlah_pembayaran,
				'id_pegawai_pembuatan'		=> $id_pegawai,
				'tanggal_pembuatan'			=> date('Y-m-d H:i:s') 		
			);
		return $this->db->insert('vamr4846_vama.penjualan_detail_kartu', $dt);
	}

	function update_detail_debit($id_penjualan_m, $id_penjualan_debit, $id_kartu_edc_pusat, $no_kartu_customer, $jumlah_pembayaran, $id_pegawai)
	{		
		$dt = array(
				'id_kartu_edc_pusat' 		=> $id_kartu_edc_pusat,
				'no_kartu_customer'			=> $no_kartu_customer,
				'jumlah_pembayaran'			=> $jumlah_pembayaran,
				'id_pegawai_pembaharuan' 	=> $id_pegawai
			);

		$where = array(
					'id_penjualan_m'		=> $id_penjualan_m,
					'id_penjualan_debit'	=> $id_penjualan_debit
				);

		return $this->db->update('vamr4846_vama.penjualan_detail_kartu', $dt, $where);
	}

	function get_total($id_penjualan_m)
	{
		$this->db->select('
							id_penjualan_m,
							IFNULL(COUNT(id_penjualan_debit),0) jumlah_kartu,
							IFNULL(ROUND(SUM(jumlah_pembayaran),0),0) total_pembayaran
						');
		$this->db->from('vamr4846_vama.penjualan_detail_kartu');
		$this->db->where(array(  
							'id_penjualan_m'=>$id_penjualan_m
		            	));
		$query = $this->db->get();
		return $query->row();
	}

	private function _get_datatables_query()
	{		
		$this->db->select('
							pdk.id_penjualan_m, pdk.id_penjualan_debit, pdk.id_kartu_edc_pusat, kep.nama_bank, 
							pdk.no_kartu_customer, pdk.jumlah_pembayaran, pdk.tanggal_pembaharuan
						');
		$this->db->from('vamr4846_vama.penjualan_detail_kartu AS pdk');
		$this->db->join('vamr4846_vama.kartu_edc_pusat as kep','kep.id_kartu_edc_pusat=pdk.id_kartu_edc_pusat');

		$i = 0;
	
		foreach ($this->column_search as $item){
			if($_POST['search']['value']){
				if($i===0){
					$this->db->group_start();
					$this->db->like($item, $_POST['search']['value']);
				}else{
					$this->db->or_like($item, $_POST['search']['value']);
				}

				if(count($this->column_search) - 1 == $i)
					$this->db->group_end();
			}
			$i++;
		}
		
		if(isset($_POST['order'])){
			$this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		}else if(isset($this->order)){
			$order = $this->order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}

	function get_datatables($id_penjualan_m)
	{
		$this->_get_datatables_query();
		$this->db->where('id_penjualan_m',$id_penjualan_m);
		if($_POST['length'] != -1)
		$this->db->limit($_POST['length'], $_POST['start']);
		$query = $this->db->get();
		return $query->result();
	}

	function count_filtered($id_penjualan_m)
	{
		$this->_get_datatables_query();
		$this->db->where('id_penjualan_m',$id_penjualan_m);
		$query = $this->db->get();
		return $query->num_rows();
	}

	public function count_all($id_penjualan_m)
	{
		$this->db->from('vamr4846_vama.penjualan_detail_kartu');
		$this->db->where('id_penjualan_m',$id_penjualan_m);
		return $this->db->count_all_results();
	}

	public function hapus_debit($id_penjualan_debit)
	{
		return $this->db->where(array(
							'id_penjualan_debit' => $id_penjualan_debit,
						 ))
					    ->delete('vamr4846_vama.penjualan_detail_kartu');
	}
}
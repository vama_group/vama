<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kartu_edc_toko_model extends MY_Model {
	var $table			= 'vamr4846_toko_mrc.kartu_edc_toko';
	var $column_order 	= array(
		'id_kartu_edc_toko','id_kartu_edc_toko','cp.nama_customer_pusat','no_kartu_edc','nama_bank','atas_nama',
		'pp1.nama_pegawai','ket.tanggal_pembuatan','pp2.nama_pegawai','pp2.nama_pegawai'
	);
	var $column_search 	= array('no_kartu_edc','nama_bank','atas_nama'); 
	var $order 			= array('id_kartu_edc_toko' => 'desc');

	private function _get_datatables_query()
	{
		$this->db->select('ket.*, cp.nama_customer_pusat AS nama_toko,
						   pp1.nama_pegawai AS pegawai_save, pp2.nama_pegawai As pegawai_edit');		
		$this->db->from('vamr4846_toko_mrc.kartu_edc_toko AS ket');
		$this->db->join('vamr4846_vama.customer_pusat AS cp', 'cp.kode_customer_pusat=ket.kode_toko', 'LEFT');
		$this->db->join('vamr4846_vama.pegawai_pusat AS pp1', 'pp1.id_pegawai=ket.id_pegawai_pembuatan', 'LEFT');
		$this->db->join('vamr4846_vama.pegawai_pusat AS pp2', 'pp2.id_pegawai=ket.id_pegawai_pembaharuan', 'LEFT');
		$this->db->where('ket.status_hapus', 'TIDAK');		

		$i = 0;
		foreach ($this->column_search as $item){
			if($_POST['search']['value']){
				if($i===0){
					$this->db->group_start();
					$this->db->like($item, $_POST['search']['value']);
				}else{
					$this->db->or_like($item, $_POST['search']['value']);
				}

				if(count($this->column_search) - 1 == $i)
					$this->db->group_end();
			}
			$i++;
		}
		
		if(isset($_POST['order'])){
			$this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		}else if(isset($this->order)){
			$order = $this->order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}

	function get_datatables()
	{
		$this->_get_datatables_query();
		if($_POST['length'] != -1)
		$this->db->limit($_POST['length'], $_POST['start']);
		$query = $this->db->get();
		return $query->result();
	}

	function count_filtered()
	{
		$this->_get_datatables_query();
		$query = $this->db->get();
		return $query->num_rows();
	}

	public function count_all()
	{
		$this->db->from($this->table);
		$this->db->where('status_hapus', 'TIDAK');
		return $this->db->count_all_results();
	}

	public function listing(){
		$this->db->select('kartu_edc_toko.*, toko.nama_toko');
		$this->db->from('vamr4846_toko_mrc.kartu_edc_toko');
		$this->db->join('vamr4846_toko_mrc.toko','toko.kode_toko=kartu_edc_toko.kode_toko','LEFT');
		$this->db->order_by('id_kartu_edc_toko','DESC');
		$query=$this->db->get();
		return $query->result();
	}

	public function get_by_id($id_kartu_edc_toko)
	{
		$this->db->select('ket.*, cp.nama_customer_pusat AS nama_toko');
		$this->db->from('vamr4846_toko_mrc.kartu_edc_toko AS ket');
		$this->db->join('vamr4846_vama.customer_pusat AS cp', 'cp.kode_customer_pusat=ket.kode_toko', 'LEFT');
		$this->db->where('id_kartu_edc_toko', $id_kartu_edc_toko);		
		$query = $this->db->get();
		return $query->row();
	}

	public function get_by_no($no_kartu_edc)
	{
		$this->db->select('ket.*, cp.nama_customer_pusat AS nama_toko');
		$this->db->from('vamr4846_toko_mrc.kartu_edc_toko AS ket');
		$this->db->join('vamr4846_vama.customer_pusat AS cp', 'cp.kode_customer_pusat=ket.kode_toko', 'LEFT');
		$this->db->where('no_kartu_edc', $no_kartu_edc);		
		$query = $this->db->get();
		return $query->row();
	}

	public function get_by_nama_bank($nama_bank, $kode_toko)
	{
		$this->db->select('ket.*, cp.nama_customer_pusat AS nama_toko');
		$this->db->from('vamr4846_toko_mrc.kartu_edc_toko AS ket');
		$this->db->join('vamr4846_vama.customer_pusat AS cp', 'cp.kode_customer_pusat=ket.kode_toko', 'LEFT');
		$this->db->where(array(
			'nama_bank'        => $nama_bank,
			'kode_toko'        => $kode_toko,
			'ket.status_hapus' => 'TIDAK'
		));		
		$query = $this->db->get();
		return $query->row();
	}

	public function save($data)
	{
		$this->db->insert($this->table, $data);
		return $this->db->insert_id();
	}

	public function update($where, $data)
	{
		$this->db->update($this->table, $data, $where);
		return $this->db->affected_rows();
	}

	public function delete_by_id($id_kartu_edc_toko)
	{
		$this->db->where('id_kartu_edc_toko', $id_kartu_edc_toko);
		$this->db->delete($this->table);
	}

	public function update_status_hapus($id_kartu_edc_toko, $id_pegawai)
	{
		$sql = "UPDATE vamr4846_toko_mrc.kartu_edc_toko 
				SET status_hapus = 'IYA', id_pegawai_pembaharuan = '".$id_pegawai."' 
				WHERE id_kartu_edc_toko = '".$id_kartu_edc_toko."'";
		return $this->db->query($sql);
	}
}

<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Barang_toko_model extends MY_Model {

	var $table = 'vamr4846_toko_mrc.barang_toko';
	public function __construct(){
		parent::__construct();
	}

	// Master Barang	
	var $column_order 		= array('bt.id_barang_toko', 'bt.id_barang_toko', 'cp.nama_customer_pusat', 
									'bt.sku', 'bp.nama_barang', 'bt.total_stok', 'bt.total_stok_rusak', 
							  		'bt.harga_eceran', 'bt.status', 'bt.status2', 'pp1.nama_pegawai', 
							  		'bt.tanggal_pembuatan', 'pp2.nama_pegawai');
	var $column_search 		= array('bp.foto', 'bt.sku', 'bp.nama_barang', 'bt.total_stok', 'bt.total_stok_rusak', 
							   		'bt.harga_eceran', 'bt.status', 'bt.status2', 'cp.nama_customer_pusat'); 
	var $order 				= array('bp.nama_barang' => 'ASC');
	// ---------------------------------------------------------------------------------------------------------------------------------
	
	// Daftar Transaksi Jual
	var $column_order_tr 	= array('bt.kode_barang','bt.sku','bp.nama_barang','bt.total_stok', 'bt.harga_eceran');
	var $column_search_tr	= array('bt.kode_barang', 'bt.sku', 'bp.nama_barang','bt.total_stok','bt.harga_eceran');
	var $order_tr 			= array('bp.nama_barang' => 'ASC');
	// ---------------------------------------------------------------------------------------------------------------------------------
	
	// Daftar Transaksi Rusak
	var $column_order_trr 	= array('bt.kode_barang','bt.sku','bp.nama_barang','bt.total_stok','bt.harga_eceran');
	var $column_search_trr	= array('bt.kode_barang', 'bt.sku', 'bp.nama_barang','bt.total_stok','bt.harga_eceran');
	var $order_trr 			= array('bp.nama_barang' => 'ASC');
	// ---------------------------------------------------------------------------------------------------------------------------------
	
	// Pembaharuan Barang
	var $column_order_pb 	= array('bt.kode_barang','bt.sku','bp.nama_barang','bt.total_stok','bt.harga_eceran');
	var $column_search_pb	= array('bt.kode_barang', 'bt.sku', 'bp.nama_barang','bt.total_stok','bt.harga_eceran');
	var $order_pb 			= array('bp.nama_barang' => 'ASC');
	// ---------------------------------------------------------------------------------------------------------------------------------

	// Periksa stok
	var $column_order_ps  = array('bt.id_barang_toko', 'bt.id_barang_toko', 'cp.nama_customer_pusat', 
								  'bt.sku', 'bp.nama_barang', 'bt.harga_eceran',
								  'total_stok_fix', 'bt.total_stok', 'koreksi',
								  'bt.status');
	var $column_search_ps = array('bp.foto', 'bt.sku', 'bp.nama_barang'); 
	var $order_ps         = array('koreksi' => 'ASC');
	// ---------------------------------------------------------------------------------------------------------------------------------
	
	// Periksa harga eceran
	var $column_order_hs  = array('bt.id_barang_toko', 'bt.id_barang_toko', 'cp.nama_customer_pusat', 
								  'bt.sku', 'bp.nama_barang', 
								  'bp.harga_eceran', 'bt.harga_eceran', 'harga_eceran_koreksi', 
								  'total_stok_fix', 'bt.status');
	var $column_search_hs = array('bp.foto', 'bt.sku', 'bp.nama_barang'); 
	var $order_hs         = array('harga_eceran_koreksi' => 'ASC');
	// ---------------------------------------------------------------------------------------------------------------------------------

	// Ambil master barang toko
	public function ambil_master_barang($kode_customer_pusat, $id_customer_pusat, $tanggal_awal, $tanggal_akhir){
		$this->db->select('
			bt.*, bp.nama_barang, cp.nama_customer_pusat, bp.foto,
			IFNULL(
				IFNULL(dsoj.jumlah_so_jual, 0) +
				IFNULL(dbm.jumlah_bm, 0) -
				IFNULL(dpj.jumlah_beli, 0) + 
				IFNULL(drpjt.jumlah_retur, 0) - 
				IFNULL(drpbt.jumlah_retur, 0)
			, 0) AS total_stok_fix,
			IFNULL(
				IFNULL(dsor.jumlah_so_rusak, 0) +
				IFNULL(drpjr.jumlah_retur, 0) - 
				IFNULL(drpbr.jumlah_retur, 0)
			, 0) AS total_stok_rusak_fix,
			pp1.nama_pegawai AS pegawai_save, pp2.nama_pegawai As pegawai_edit');
		$this->db->from('vamr4846_toko_mrc.barang_toko_'.$kode_customer_pusat.' AS bt');
		$this->db->join('vamr4846_vama.barang_pusat AS bp','bp.kode_barang=bt.kode_barang','LEFT');

		// Untuk stok jual
		$this->db->join('(SELECT id_barang_pusat, SUM(jumlah_so) AS jumlah_so_jual FROM vamr4846_toko_mrc.stok_opname_toko_'.$kode_customer_pusat.'  
						  WHERE DATE(tanggal_pembuatan) BETWEEN "'.$tanggal_awal.'" AND "'.$tanggal_akhir.'" 
						  AND masuk_stok="JUAL"
						  GROUP BY id_barang_pusat) AS dsoj', 'dsoj.id_barang_pusat=bp.id_barang_pusat', 'LEFT');
		$this->db->join('(SELECT pd.id_barang_pusat, SUM(pd.jumlah_masuk) AS jumlah_bm
						  FROM vamr4846_vama.penjualan_detail AS pd
						  LEFT JOIN vamr4846_vama.penjualan_master AS pm ON pm.id_penjualan_m=pd.id_penjualan_m
						  WHERE pm.id_customer_pusat LIKE "%'.$id_customer_pusat.'%" AND 
						  		DATE(pd.tanggal_pembuatan) BETWEEN "'.$tanggal_awal.'" AND "'.$tanggal_akhir.'"
						  GROUP BY pd.id_barang_pusat) AS dbm', 'dbm.id_barang_pusat=bp.id_barang_pusat', 'LEFT');
		$this->db->join('(SELECT id_barang_pusat, SUM(jumlah_beli) AS jumlah_beli FROM vamr4846_toko_mrc.penjualan_detail_'.$kode_customer_pusat.'  
						  WHERE DATE(tanggal_pembuatan) BETWEEN "'.$tanggal_awal.'" AND "'.$tanggal_akhir.'" 
						  GROUP BY id_barang_pusat) AS dpj', 'dpj.id_barang_pusat=bp.id_barang_pusat', 'LEFT');
		$this->db->join('(SELECT id_barang_pusat, SUM(jumlah_retur) AS jumlah_retur FROM vamr4846_toko_mrc.retur_penjualan_detail_'.$kode_customer_pusat.'  
						  WHERE masuk_stok="JUAL" AND DATE(tanggal_pembuatan) BETWEEN "'.$tanggal_awal.'" AND "'.$tanggal_akhir.'" 
						  GROUP BY id_barang_pusat) AS drpjt', 'drpjt.id_barang_pusat=bp.id_barang_pusat', 'LEFT');
		$this->db->join('(SELECT rpd.id_barang_pusat, SUM(rpd.jumlah_retur) AS jumlah_retur
						  FROM vamr4846_toko_mrc.retur_pembelian_detail_'.$kode_customer_pusat.' AS rpd
						  WHERE rpd.dari_stok="JUAL" AND 
						  		DATE(rpd.tanggal_pembuatan) BETWEEN "'.$tanggal_awal.'" AND "'.$tanggal_akhir.'"
						  GROUP BY rpd.id_barang_pusat) AS drpbt', 'drpbt.id_barang_pusat=bp.id_barang_pusat', 'LEFT');
		// ---------------------------------------------------------------------------------------------------------------------------------------

		// Untuk stok rusak
		$this->db->join('(SELECT id_barang_pusat, SUM(jumlah_so) AS jumlah_so_rusak FROM vamr4846_toko_mrc.stok_opname_toko_'.$kode_customer_pusat.'  
						  WHERE DATE(tanggal_pembuatan) BETWEEN "'.$tanggal_awal.'" AND "'.$tanggal_akhir.'" 
						  AND masuk_stok="RUSAK"
						  GROUP BY id_barang_pusat) AS dsor', 'dsor.id_barang_pusat=bp.id_barang_pusat', 'LEFT');
		$this->db->join('(SELECT id_barang_pusat, SUM(jumlah_retur) AS jumlah_retur FROM vamr4846_toko_mrc.retur_penjualan_detail_'.$kode_customer_pusat.'  
						  WHERE masuk_stok="RUSAK" AND DATE(tanggal_pembuatan) BETWEEN "'.$tanggal_awal.'" AND "'.$tanggal_akhir.'" 
						  GROUP BY id_barang_pusat) AS drpjr', 'drpjr.id_barang_pusat=bp.id_barang_pusat', 'LEFT');
		$this->db->join('(SELECT rpd.id_barang_pusat, SUM(rpd.jumlah_retur) AS jumlah_retur
						  FROM vamr4846_toko_mrc.retur_pembelian_detail_'.$kode_customer_pusat.' AS rpd
						  WHERE rpd.dari_stok="RUSAK" AND 
						  		DATE(rpd.tanggal_pembuatan) BETWEEN "'.$tanggal_awal.'" AND "'.$tanggal_akhir.'"
						  GROUP BY rpd.id_barang_pusat) AS drpbr', 'drpbr.id_barang_pusat=bp.id_barang_pusat', 'LEFT');
		// ---------------------------------------------------------------------------------------------------------------------------------------

		$this->db->join('vamr4846_vama.customer_pusat AS cp','cp.kode_customer_pusat=bt.kode_toko','LEFT');
		$this->db->join('vamr4846_vama.pegawai_pusat AS pp1', 'pp1.id_pegawai=bt.id_pegawai_pembuatan', 'LEFT');
		$this->db->join('vamr4846_vama.pegawai_pusat AS pp2', 'pp2.id_pegawai=bt.id_pegawai_pembaharuan', 'LEFT');

		$this->db->where(array (
			'bt.status_hapus'     => 'TIDAK'
			// 'bt.status'           => 'AKTIF',
		));	
		$this->db->like('cp.kode_customer_pusat', $kode_customer_pusat);
		$query=$this->db->get();
		return $query->result();
	}
	// Akhir master barang toko

	// Awal periksa stok toko
	public function periksa_stok_toko($kode_customer_pusat, $id_customer_pusat, $tanggal_awal, $tanggal_akhir)
	{		
		$this->db->select('
			bt.*, bp.nama_barang, cp.nama_customer_pusat, bp.foto, cp.kode_customer_pusat AS kode_toko,
			IFNULL(
				IFNULL(dsoj.jumlah_so_jual, 0) +
				IFNULL(dbm.jumlah_bm, 0) -
				IFNULL(dpj.jumlah_beli, 0) + 
				IFNULL(drpjt.jumlah_retur, 0) - 
				IFNULL(drpbt.jumlah_retur, 0)
			, 0) AS total_stok_fix,
			IFNULL(
				IFNULL(dsor.jumlah_so_rusak, 0) +
				IFNULL(drpjr.jumlah_retur, 0) - 
				IFNULL(drpbr.jumlah_retur, 0)
			, 0) AS total_stok_rusak_fix,
			IFNULL(
				IFNULL(dsoj.jumlah_so_jual, 0) +
				IFNULL(dbm.jumlah_bm, 0) -
				IFNULL(dpj.jumlah_beli, 0) + 
				IFNULL(drpjt.jumlah_retur, 0) - 
				IFNULL(drpbt.jumlah_retur, 0)
			, 0) - bt.total_stok AS koreksi, 
			pp1.nama_pegawai AS pegawai_save, pp2.nama_pegawai As pegawai_edit
		');
		$this->db->from('vamr4846_toko_mrc.barang_toko_'.$kode_customer_pusat.' AS bt');
		$this->db->join('vamr4846_vama.barang_pusat AS bp','bp.kode_barang=bt.kode_barang','LEFT');

		// Untuk stok jual
		$this->db->join('(SELECT id_barang_pusat, SUM(jumlah_so) AS jumlah_so_jual FROM vamr4846_toko_mrc.stok_opname_toko_'.$kode_customer_pusat.'  
						  WHERE DATE(tanggal_pembuatan) BETWEEN "'.$tanggal_awal.'" AND "'.$tanggal_akhir.'" 
						  AND masuk_stok="JUAL"
						  GROUP BY id_barang_pusat) AS dsoj', 'dsoj.id_barang_pusat=bp.id_barang_pusat', 'LEFT');
		$this->db->join('(SELECT pd.id_barang_pusat, SUM(pd.jumlah_masuk) AS jumlah_bm
						  FROM vamr4846_vama.penjualan_detail AS pd
						  LEFT JOIN vamr4846_vama.penjualan_master AS pm ON pm.id_penjualan_m=pd.id_penjualan_m
						  WHERE pm.id_customer_pusat LIKE "%'.$id_customer_pusat.'%" AND 
						  		DATE(pd.tanggal_pembuatan) BETWEEN "'.$tanggal_awal.'" AND "'.$tanggal_akhir.'"
						  GROUP BY pd.id_barang_pusat) AS dbm', 'dbm.id_barang_pusat=bp.id_barang_pusat', 'LEFT');
		$this->db->join('(SELECT id_barang_pusat, SUM(jumlah_beli) AS jumlah_beli FROM vamr4846_toko_mrc.penjualan_detail_'.$kode_customer_pusat.'  
						  WHERE DATE(tanggal_pembuatan) BETWEEN "'.$tanggal_awal.'" AND "'.$tanggal_akhir.'" 
						  GROUP BY id_barang_pusat) AS dpj', 'dpj.id_barang_pusat=bp.id_barang_pusat', 'LEFT');
		$this->db->join('(SELECT id_barang_pusat, SUM(jumlah_retur) AS jumlah_retur FROM vamr4846_toko_mrc.retur_penjualan_detail_'.$kode_customer_pusat.'  
						  WHERE masuk_stok="JUAL" AND DATE(tanggal_pembuatan) BETWEEN "'.$tanggal_awal.'" AND "'.$tanggal_akhir.'" 
						  GROUP BY id_barang_pusat) AS drpjt', 'drpjt.id_barang_pusat=bp.id_barang_pusat', 'LEFT');
		$this->db->join('(SELECT rpd.id_barang_pusat, SUM(rpd.jumlah_retur) AS jumlah_retur
						  FROM vamr4846_toko_mrc.retur_pembelian_detail_'.$kode_customer_pusat.' AS rpd
						  WHERE rpd.dari_stok="JUAL" AND 
						  		DATE(rpd.tanggal_pembuatan) BETWEEN "'.$tanggal_awal.'" AND "'.$tanggal_akhir.'"
						  GROUP BY rpd.id_barang_pusat) AS drpbt', 'drpbt.id_barang_pusat=bp.id_barang_pusat', 'LEFT');
		// ---------------------------------------------------------------------------------------------------------------------------------------

		// Untuk stok rusak
		$this->db->join('(SELECT id_barang_pusat, SUM(jumlah_so) AS jumlah_so_rusak FROM vamr4846_toko_mrc.stok_opname_toko_'.$kode_customer_pusat.'  
						  WHERE DATE(tanggal_pembuatan) BETWEEN "'.$tanggal_awal.'" AND "'.$tanggal_akhir.'" 
						  AND masuk_stok="RUSAK"
						  GROUP BY id_barang_pusat) AS dsor', 'dsor.id_barang_pusat=bp.id_barang_pusat', 'LEFT');
		$this->db->join('(SELECT id_barang_pusat, SUM(jumlah_retur) AS jumlah_retur FROM vamr4846_toko_mrc.retur_penjualan_detail_'.$kode_customer_pusat.'  
						  WHERE masuk_stok="RUSAK" AND DATE(tanggal_pembuatan) BETWEEN "'.$tanggal_awal.'" AND "'.$tanggal_akhir.'" 
						  GROUP BY id_barang_pusat) AS drpjr', 'drpjr.id_barang_pusat=bp.id_barang_pusat', 'LEFT');
		$this->db->join('(SELECT rpd.id_barang_pusat, SUM(rpd.jumlah_retur) AS jumlah_retur
						  FROM vamr4846_toko_mrc.retur_pembelian_detail_'.$kode_customer_pusat.' AS rpd
						  WHERE rpd.dari_stok="RUSAK" AND 
						  		DATE(rpd.tanggal_pembuatan) BETWEEN "'.$tanggal_awal.'" AND "'.$tanggal_akhir.'"
						  GROUP BY rpd.id_barang_pusat) AS drpbr', 'drpbr.id_barang_pusat=bp.id_barang_pusat', 'LEFT');
		// ---------------------------------------------------------------------------------------------------------------------------------------

		$this->db->join('vamr4846_vama.customer_pusat AS cp','cp.kode_customer_pusat=bt.kode_toko','LEFT');
		$this->db->join('vamr4846_vama.pegawai_pusat AS pp1', 'pp1.id_pegawai=bt.id_pegawai_pembuatan', 'LEFT');
		$this->db->join('vamr4846_vama.pegawai_pusat AS pp2', 'pp2.id_pegawai=bt.id_pegawai_pembaharuan', 'LEFT');

		$this->db->where('bt.status_hapus', 'TIDAK');
		$this->db->where('
			IFNULL(
				IFNULL(dsoj.jumlah_so_jual, 0) +
				IFNULL(dbm.jumlah_bm, 0) -
				IFNULL(dpj.jumlah_beli, 0) + 
				IFNULL(drpjt.jumlah_retur, 0) - 
				IFNULL(drpbt.jumlah_retur, 0)
			, 0) - bt.total_stok <>
		', '0');
		$this->db->like('cp.kode_customer_pusat', $kode_customer_pusat);

		$query = $this->db->get();
		return $query->result();
	}
	// Akhir periksa stok toko

	// Awal periksa stok toko
	public function periksa_harga_ecean_toko($kode_customer_pusat, $id_customer_pusat, $tanggal_awal, $tanggal_akhir)
	{		
		$this->db->select('
			bt.*, 
			bp.nama_barang, bp.harga_eceran AS harga_eceran_pusat,
			IFNULL(bp.harga_eceran-bt.harga_eceran,0) AS harga_eceran_koreksi,
			cp.nama_customer_pusat, bp.foto, cp.kode_customer_pusat AS kode_toko,
			IFNULL(
				IFNULL(dsoj.jumlah_so_jual, 0) +
				IFNULL(dbm.jumlah_bm, 0) -
				IFNULL(dpj.jumlah_beli, 0) + 
				IFNULL(drpjt.jumlah_retur, 0) - 
				IFNULL(drpbt.jumlah_retur, 0)
			, 0) AS total_stok_fix,
			IFNULL(
				IFNULL(dsor.jumlah_so_rusak, 0) +
				IFNULL(drpjr.jumlah_retur, 0) - 
				IFNULL(drpbr.jumlah_retur, 0)
			, 0) AS total_stok_rusak_fix,
			IFNULL(
				IFNULL(dsoj.jumlah_so_jual, 0) +
				IFNULL(dbm.jumlah_bm, 0) -
				IFNULL(dpj.jumlah_beli, 0) + 
				IFNULL(drpjt.jumlah_retur, 0) - 
				IFNULL(drpbt.jumlah_retur, 0)
			, 0) - bt.total_stok AS koreksi, 
			pp1.nama_pegawai AS pegawai_save, pp2.nama_pegawai As pegawai_edit
		');
		$this->db->from('vamr4846_toko_mrc.barang_toko_'.$kode_customer_pusat.' AS bt');
		$this->db->join('vamr4846_vama.barang_pusat AS bp','bp.kode_barang=bt.kode_barang','LEFT');

		// Untuk stok jual
		$this->db->join('(SELECT id_barang_pusat, SUM(jumlah_so) AS jumlah_so_jual FROM vamr4846_toko_mrc.stok_opname_toko_'.$kode_customer_pusat.'  
						  WHERE DATE(tanggal_pembuatan) BETWEEN "'.$tanggal_awal.'" AND "'.$tanggal_akhir.'" 
						  AND masuk_stok="JUAL"
						  GROUP BY id_barang_pusat) AS dsoj', 'dsoj.id_barang_pusat=bp.id_barang_pusat', 'LEFT');
		$this->db->join('(SELECT pd.id_barang_pusat, SUM(pd.jumlah_masuk) AS jumlah_bm
						  FROM vamr4846_vama.penjualan_detail AS pd
						  LEFT JOIN vamr4846_vama.penjualan_master AS pm ON pm.id_penjualan_m=pd.id_penjualan_m
						  WHERE pm.id_customer_pusat LIKE "%'.$id_customer_pusat.'%" AND 
						  		DATE(pd.tanggal_pembuatan) BETWEEN "'.$tanggal_awal.'" AND "'.$tanggal_akhir.'"
						  GROUP BY pd.id_barang_pusat) AS dbm', 'dbm.id_barang_pusat=bp.id_barang_pusat', 'LEFT');
		$this->db->join('(SELECT id_barang_pusat, SUM(jumlah_beli) AS jumlah_beli FROM vamr4846_toko_mrc.penjualan_detail_'.$kode_customer_pusat.'  
						  WHERE DATE(tanggal_pembuatan) BETWEEN "'.$tanggal_awal.'" AND "'.$tanggal_akhir.'" 
						  GROUP BY id_barang_pusat) AS dpj', 'dpj.id_barang_pusat=bp.id_barang_pusat', 'LEFT');
		$this->db->join('(SELECT id_barang_pusat, SUM(jumlah_retur) AS jumlah_retur FROM vamr4846_toko_mrc.retur_penjualan_detail_'.$kode_customer_pusat.'  
						  WHERE masuk_stok="JUAL" AND DATE(tanggal_pembuatan) BETWEEN "'.$tanggal_awal.'" AND "'.$tanggal_akhir.'" 
						  GROUP BY id_barang_pusat) AS drpjt', 'drpjt.id_barang_pusat=bp.id_barang_pusat', 'LEFT');
		$this->db->join('(SELECT rpd.id_barang_pusat, SUM(rpd.jumlah_retur) AS jumlah_retur
						  FROM vamr4846_toko_mrc.retur_pembelian_detail_'.$kode_customer_pusat.' AS rpd
						  WHERE rpd.dari_stok="JUAL" AND 
						  		DATE(rpd.tanggal_pembuatan) BETWEEN "'.$tanggal_awal.'" AND "'.$tanggal_akhir.'"
						  GROUP BY rpd.id_barang_pusat) AS drpbt', 'drpbt.id_barang_pusat=bp.id_barang_pusat', 'LEFT');
		// ---------------------------------------------------------------------------------------------------------------------------------------

		// Untuk stok rusak
		$this->db->join('(SELECT id_barang_pusat, SUM(jumlah_so) AS jumlah_so_rusak FROM vamr4846_toko_mrc.stok_opname_toko_'.$kode_customer_pusat.'  
						  WHERE DATE(tanggal_pembuatan) BETWEEN "'.$tanggal_awal.'" AND "'.$tanggal_akhir.'" 
						  AND masuk_stok="RUSAK"
						  GROUP BY id_barang_pusat) AS dsor', 'dsor.id_barang_pusat=bp.id_barang_pusat', 'LEFT');
		$this->db->join('(SELECT id_barang_pusat, SUM(jumlah_retur) AS jumlah_retur FROM vamr4846_toko_mrc.retur_penjualan_detail_'.$kode_customer_pusat.'  
						  WHERE masuk_stok="RUSAK" AND DATE(tanggal_pembuatan) BETWEEN "'.$tanggal_awal.'" AND "'.$tanggal_akhir.'" 
						  GROUP BY id_barang_pusat) AS drpjr', 'drpjr.id_barang_pusat=bp.id_barang_pusat', 'LEFT');
		$this->db->join('(SELECT rpd.id_barang_pusat, SUM(rpd.jumlah_retur) AS jumlah_retur
						  FROM vamr4846_toko_mrc.retur_pembelian_detail_'.$kode_customer_pusat.' AS rpd
						  WHERE rpd.dari_stok="RUSAK" AND 
						  		DATE(rpd.tanggal_pembuatan) BETWEEN "'.$tanggal_awal.'" AND "'.$tanggal_akhir.'"
						  GROUP BY rpd.id_barang_pusat) AS drpbr', 'drpbr.id_barang_pusat=bp.id_barang_pusat', 'LEFT');
		// ---------------------------------------------------------------------------------------------------------------------------------------

		$this->db->join('vamr4846_vama.customer_pusat AS cp','cp.kode_customer_pusat=bt.kode_toko','LEFT');
		$this->db->join('vamr4846_vama.pegawai_pusat AS pp1', 'pp1.id_pegawai=bt.id_pegawai_pembuatan', 'LEFT');
		$this->db->join('vamr4846_vama.pegawai_pusat AS pp2', 'pp2.id_pegawai=bt.id_pegawai_pembaharuan', 'LEFT');

		$this->db->where('bt.status_hapus', 'TIDAK');
		$this->db->where('bt.harga_eceran <> bp.harga_eceran');
		$this->db->like('cp.kode_customer_pusat', $kode_customer_pusat);

		$query = $this->db->get();
		return $query->result();
	}
	// Akhir periksa harga eceran toko

	// Awal update stok toko
	public function update_stok_toko($id_barang_pusat, $kode_toko, $stok_fix, $id_pegawai){
		$sql = "
				UPDATE 
					vamr4846_toko_mrc.barang_toko_".$kode_toko." 
				SET 
					total_stok = '".$stok_fix."',
					id_pegawai_pembaharuan = '".$id_pegawai."'
				WHERE 
					id_barang_pusat = '".$id_barang_pusat."'";
		return $this->db->query($sql);
	}
	// Akhir update stok toko

	// Awal update harga eceran toko
	public function update_harga_eceran_toko($id_barang_pusat, $kode_toko, $harga_eceran_pusat, $id_pegawai){
		$sql = "
				UPDATE 
					vamr4846_toko_mrc.barang_toko_".$kode_toko." 
				SET 
					harga_eceran = '".$harga_eceran_pusat."',
					id_pegawai_pembaharuan = '".$id_pegawai."'
				WHERE 
					id_barang_pusat = '".$id_barang_pusat."'";
		return $this->db->query($sql);
	}
	// Akhir update harga eceran toko

	// Awal datatable periksa stok toko
	private function _get_datatables_query_periksa($kode_customer_pusat, $id_customer_pusat, $tanggal_awal, $tanggal_akhir)
	{		
		$this->db->select('
			bt.*, bp.nama_barang, cp.nama_customer_pusat, bp.foto, cp.kode_customer_pusat AS kode_toko,
			IFNULL(
				IFNULL(dsoj.jumlah_so_jual, 0) +
				IFNULL(dbm.jumlah_bm, 0) -
				IFNULL(dpj.jumlah_beli, 0) + 
				IFNULL(drpjt.jumlah_retur, 0) - 
				IFNULL(drpbt.jumlah_retur, 0)
			, 0) AS total_stok_fix,
			IFNULL(
				IFNULL(dsor.jumlah_so_rusak, 0) +
				IFNULL(drpjr.jumlah_retur, 0) - 
				IFNULL(drpbr.jumlah_retur, 0)
			, 0) AS total_stok_rusak_fix,
			IFNULL(
				IFNULL(dsoj.jumlah_so_jual, 0) +
				IFNULL(dbm.jumlah_bm, 0) -
				IFNULL(dpj.jumlah_beli, 0) + 
				IFNULL(drpjt.jumlah_retur, 0) - 
				IFNULL(drpbt.jumlah_retur, 0)
			, 0) - bt.total_stok AS koreksi, 
			pp1.nama_pegawai AS pegawai_save, pp2.nama_pegawai As pegawai_edit');
		$this->db->from('vamr4846_toko_mrc.barang_toko_'.$kode_customer_pusat.' AS bt');
		$this->db->join('vamr4846_vama.barang_pusat AS bp','bp.kode_barang=bt.kode_barang','LEFT');

		// Untuk stok jual
		$this->db->join('(SELECT id_barang_pusat, SUM(jumlah_so) AS jumlah_so_jual FROM vamr4846_toko_mrc.stok_opname_toko_'.$kode_customer_pusat.'  
						  WHERE DATE(tanggal_pembuatan) BETWEEN "'.$tanggal_awal.'" AND "'.$tanggal_akhir.'" 
						  AND masuk_stok="JUAL"
						  GROUP BY id_barang_pusat) AS dsoj', 'dsoj.id_barang_pusat=bp.id_barang_pusat', 'LEFT');
		$this->db->join('(SELECT pd.id_barang_pusat, SUM(pd.jumlah_masuk) AS jumlah_bm
						  FROM vamr4846_vama.penjualan_detail AS pd
						  LEFT JOIN vamr4846_vama.penjualan_master AS pm ON pm.id_penjualan_m=pd.id_penjualan_m
						  WHERE pm.id_customer_pusat LIKE "%'.$id_customer_pusat.'%" AND 
						  		DATE(pd.tanggal_pembuatan) BETWEEN "'.$tanggal_awal.'" AND "'.$tanggal_akhir.'"
						  GROUP BY pd.id_barang_pusat) AS dbm', 'dbm.id_barang_pusat=bp.id_barang_pusat', 'LEFT');
		$this->db->join('(SELECT id_barang_pusat, SUM(jumlah_beli) AS jumlah_beli FROM vamr4846_toko_mrc.penjualan_detail_'.$kode_customer_pusat.'  
						  WHERE DATE(tanggal_pembuatan) BETWEEN "'.$tanggal_awal.'" AND "'.$tanggal_akhir.'" 
						  GROUP BY id_barang_pusat) AS dpj', 'dpj.id_barang_pusat=bp.id_barang_pusat', 'LEFT');
		$this->db->join('(SELECT id_barang_pusat, SUM(jumlah_retur) AS jumlah_retur FROM vamr4846_toko_mrc.retur_penjualan_detail_'.$kode_customer_pusat.'  
						  WHERE masuk_stok="JUAL" AND DATE(tanggal_pembuatan) BETWEEN "'.$tanggal_awal.'" AND "'.$tanggal_akhir.'" 
						  GROUP BY id_barang_pusat) AS drpjt', 'drpjt.id_barang_pusat=bp.id_barang_pusat', 'LEFT');
		$this->db->join('(SELECT rpd.id_barang_pusat, SUM(rpd.jumlah_retur) AS jumlah_retur
						  FROM vamr4846_toko_mrc.retur_pembelian_detail_'.$kode_customer_pusat.' AS rpd
						  WHERE rpd.dari_stok="JUAL" AND 
						  		DATE(rpd.tanggal_pembuatan) BETWEEN "'.$tanggal_awal.'" AND "'.$tanggal_akhir.'"
						  GROUP BY rpd.id_barang_pusat) AS drpbt', 'drpbt.id_barang_pusat=bp.id_barang_pusat', 'LEFT');
		// ---------------------------------------------------------------------------------------------------------------------------------------

		// Untuk stok rusak
		$this->db->join('(SELECT id_barang_pusat, SUM(jumlah_so) AS jumlah_so_rusak FROM vamr4846_toko_mrc.stok_opname_toko_'.$kode_customer_pusat.'  
						  WHERE DATE(tanggal_pembuatan) BETWEEN "'.$tanggal_awal.'" AND "'.$tanggal_akhir.'" 
						  AND masuk_stok="RUSAK"
						  GROUP BY id_barang_pusat) AS dsor', 'dsor.id_barang_pusat=bp.id_barang_pusat', 'LEFT');
		$this->db->join('(SELECT id_barang_pusat, SUM(jumlah_retur) AS jumlah_retur FROM vamr4846_toko_mrc.retur_penjualan_detail_'.$kode_customer_pusat.'  
						  WHERE masuk_stok="RUSAK" AND DATE(tanggal_pembuatan) BETWEEN "'.$tanggal_awal.'" AND "'.$tanggal_akhir.'" 
						  GROUP BY id_barang_pusat) AS drpjr', 'drpjr.id_barang_pusat=bp.id_barang_pusat', 'LEFT');
		$this->db->join('(SELECT rpd.id_barang_pusat, SUM(rpd.jumlah_retur) AS jumlah_retur
						  FROM vamr4846_toko_mrc.retur_pembelian_detail_'.$kode_customer_pusat.' AS rpd
						  WHERE rpd.dari_stok="RUSAK" AND 
						  		DATE(rpd.tanggal_pembuatan) BETWEEN "'.$tanggal_awal.'" AND "'.$tanggal_akhir.'"
						  GROUP BY rpd.id_barang_pusat) AS drpbr', 'drpbr.id_barang_pusat=bp.id_barang_pusat', 'LEFT');
		// ---------------------------------------------------------------------------------------------------------------------------------------

		$this->db->join('vamr4846_vama.customer_pusat AS cp','cp.kode_customer_pusat=bt.kode_toko','LEFT');
		$this->db->join('vamr4846_vama.pegawai_pusat AS pp1', 'pp1.id_pegawai=bt.id_pegawai_pembuatan', 'LEFT');
		$this->db->join('vamr4846_vama.pegawai_pusat AS pp2', 'pp2.id_pegawai=bt.id_pegawai_pembaharuan', 'LEFT');

		$this->db->where('bt.status_hapus', 'TIDAK');
		$this->db->where('
			IFNULL(
				IFNULL(dsoj.jumlah_so_jual, 0) +
				IFNULL(dbm.jumlah_bm, 0) -
				IFNULL(dpj.jumlah_beli, 0) + 
				IFNULL(drpjt.jumlah_retur, 0) - 
				IFNULL(drpbt.jumlah_retur, 0)
			, 0) - bt.total_stok <>
		', '0');
		$this->db->like('cp.kode_customer_pusat', $kode_customer_pusat);

		$i = 0;
		foreach ($this->column_search_ps as $item){
			if($_POST['search']['value']){
				if($i===0){
					$this->db->group_start();
					$this->db->like($item, $_POST['search']['value']);
				}else{
					$this->db->or_like($item, $_POST['search']['value']);
				}

				if(count($this->column_search_ps) - 1 == $i)
					$this->db->group_end();
			}
			$i++;
		}
		
		if(isset($_POST['order'])){
			$this->db->order_by($this->column_order_ps[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		}else if(isset($this->order_ps)){
			$order = $this->order_ps;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}
	// Akhir datatable periksa stok toko

	// Awal datatable periksa harga eceran toko
	private function _get_datatables_query_periksa_harga($kode_customer_pusat, $id_customer_pusat, $tanggal_awal, $tanggal_akhir)
	{		
		$this->db->select('
			bt.*, 
			bp.nama_barang, bp.harga_eceran AS harga_eceran_pusat,
			IFNULL(bp.harga_eceran-bt.harga_eceran,0) AS harga_eceran_koreksi,
			cp.nama_customer_pusat, bp.foto, cp.kode_customer_pusat AS kode_toko,
			IFNULL(
				IFNULL(dsoj.jumlah_so_jual, 0) +
				IFNULL(dbm.jumlah_bm, 0) -
				IFNULL(dpj.jumlah_beli, 0) + 
				IFNULL(drpjt.jumlah_retur, 0) - 
				IFNULL(drpbt.jumlah_retur, 0)
			, 0) AS total_stok_fix,
			IFNULL(
				IFNULL(dsor.jumlah_so_rusak, 0) +
				IFNULL(drpjr.jumlah_retur, 0) - 
				IFNULL(drpbr.jumlah_retur, 0)
			, 0) AS total_stok_rusak_fix,
			IFNULL(
				IFNULL(dsoj.jumlah_so_jual, 0) +
				IFNULL(dbm.jumlah_bm, 0) -
				IFNULL(dpj.jumlah_beli, 0) + 
				IFNULL(drpjt.jumlah_retur, 0) - 
				IFNULL(drpbt.jumlah_retur, 0)
			, 0) - bt.total_stok AS koreksi, 
			pp1.nama_pegawai AS pegawai_save, pp2.nama_pegawai As pegawai_edit');
		$this->db->from('vamr4846_toko_mrc.barang_toko_'.$kode_customer_pusat.' AS bt');
		$this->db->join('vamr4846_vama.barang_pusat AS bp','bp.kode_barang=bt.kode_barang','LEFT');

		// Untuk stok jual
		$this->db->join('(SELECT id_barang_pusat, SUM(jumlah_so) AS jumlah_so_jual FROM vamr4846_toko_mrc.stok_opname_toko_'.$kode_customer_pusat.'  
						  WHERE DATE(tanggal_pembuatan) BETWEEN "'.$tanggal_awal.'" AND "'.$tanggal_akhir.'" 
						  AND masuk_stok="JUAL"
						  GROUP BY id_barang_pusat) AS dsoj', 'dsoj.id_barang_pusat=bp.id_barang_pusat', 'LEFT');
		$this->db->join('(SELECT pd.id_barang_pusat, SUM(pd.jumlah_masuk) AS jumlah_bm
						  FROM vamr4846_vama.penjualan_detail AS pd
						  LEFT JOIN vamr4846_vama.penjualan_master AS pm ON pm.id_penjualan_m=pd.id_penjualan_m
						  WHERE pm.id_customer_pusat LIKE "%'.$id_customer_pusat.'%" AND 
						  		DATE(pd.tanggal_pembuatan) BETWEEN "'.$tanggal_awal.'" AND "'.$tanggal_akhir.'"
						  GROUP BY pd.id_barang_pusat) AS dbm', 'dbm.id_barang_pusat=bp.id_barang_pusat', 'LEFT');
		$this->db->join('(SELECT id_barang_pusat, SUM(jumlah_beli) AS jumlah_beli FROM vamr4846_toko_mrc.penjualan_detail_'.$kode_customer_pusat.'  
						  WHERE DATE(tanggal_pembuatan) BETWEEN "'.$tanggal_awal.'" AND "'.$tanggal_akhir.'" 
						  GROUP BY id_barang_pusat) AS dpj', 'dpj.id_barang_pusat=bp.id_barang_pusat', 'LEFT');
		$this->db->join('(SELECT id_barang_pusat, SUM(jumlah_retur) AS jumlah_retur FROM vamr4846_toko_mrc.retur_penjualan_detail_'.$kode_customer_pusat.'  
						  WHERE masuk_stok="JUAL" AND DATE(tanggal_pembuatan) BETWEEN "'.$tanggal_awal.'" AND "'.$tanggal_akhir.'" 
						  GROUP BY id_barang_pusat) AS drpjt', 'drpjt.id_barang_pusat=bp.id_barang_pusat', 'LEFT');
		$this->db->join('(SELECT rpd.id_barang_pusat, SUM(rpd.jumlah_retur) AS jumlah_retur
						  FROM vamr4846_toko_mrc.retur_pembelian_detail_'.$kode_customer_pusat.' AS rpd
						  WHERE rpd.dari_stok="JUAL" AND 
						  		DATE(rpd.tanggal_pembuatan) BETWEEN "'.$tanggal_awal.'" AND "'.$tanggal_akhir.'"
						  GROUP BY rpd.id_barang_pusat) AS drpbt', 'drpbt.id_barang_pusat=bp.id_barang_pusat', 'LEFT');
		// ---------------------------------------------------------------------------------------------------------------------------------------

		// Untuk stok rusak
		$this->db->join('(SELECT id_barang_pusat, SUM(jumlah_so) AS jumlah_so_rusak FROM vamr4846_toko_mrc.stok_opname_toko_'.$kode_customer_pusat.'  
						  WHERE DATE(tanggal_pembuatan) BETWEEN "'.$tanggal_awal.'" AND "'.$tanggal_akhir.'" 
						  AND masuk_stok="RUSAK"
						  GROUP BY id_barang_pusat) AS dsor', 'dsor.id_barang_pusat=bp.id_barang_pusat', 'LEFT');
		$this->db->join('(SELECT id_barang_pusat, SUM(jumlah_retur) AS jumlah_retur FROM vamr4846_toko_mrc.retur_penjualan_detail_'.$kode_customer_pusat.'  
						  WHERE masuk_stok="RUSAK" AND DATE(tanggal_pembuatan) BETWEEN "'.$tanggal_awal.'" AND "'.$tanggal_akhir.'" 
						  GROUP BY id_barang_pusat) AS drpjr', 'drpjr.id_barang_pusat=bp.id_barang_pusat', 'LEFT');
		$this->db->join('(SELECT rpd.id_barang_pusat, SUM(rpd.jumlah_retur) AS jumlah_retur
						  FROM vamr4846_toko_mrc.retur_pembelian_detail_'.$kode_customer_pusat.' AS rpd
						  WHERE rpd.dari_stok="RUSAK" AND 
						  		DATE(rpd.tanggal_pembuatan) BETWEEN "'.$tanggal_awal.'" AND "'.$tanggal_akhir.'"
						  GROUP BY rpd.id_barang_pusat) AS drpbr', 'drpbr.id_barang_pusat=bp.id_barang_pusat', 'LEFT');
		// ---------------------------------------------------------------------------------------------------------------------------------------

		$this->db->join('vamr4846_vama.customer_pusat AS cp','cp.kode_customer_pusat=bt.kode_toko','LEFT');
		$this->db->join('vamr4846_vama.pegawai_pusat AS pp1', 'pp1.id_pegawai=bt.id_pegawai_pembuatan', 'LEFT');
		$this->db->join('vamr4846_vama.pegawai_pusat AS pp2', 'pp2.id_pegawai=bt.id_pegawai_pembaharuan', 'LEFT');

		$this->db->where('bt.status_hapus', 'TIDAK');
		$this->db->where('bt.harga_eceran <> bp.harga_eceran');
		$this->db->like('cp.kode_customer_pusat', $kode_customer_pusat);

		$i = 0;
		foreach ($this->column_search_hs as $item){
			if($_POST['search']['value']){
				if($i===0){
					$this->db->group_start();
					$this->db->like($item, $_POST['search']['value']);
				}else{
					$this->db->or_like($item, $_POST['search']['value']);
				}

				if(count($this->column_search_hs) - 1 == $i)
					$this->db->group_end();
			}
			$i++;
		}
		
		if(isset($_POST['order'])){
			$this->db->order_by($this->column_order_hs[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		}else if(isset($this->order_hs)){
			$order = $this->order_hs;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}
	// Akhir datatable periksa harga eceran barang toko

	// Awal datatable master barang toko
	// Awal query real time stok toko
	// private function _get_datatables_query($kode_barang, $kode_customer_pusat, $id_customer_pusat, $tanggal_awal, $tanggal_akhir)
	// {		
	// 	$this->db->select('
	// 		bt.*, bp.nama_barang, cp.nama_customer_pusat, bp.foto, cp.kode_customer_pusat AS kode_toko,
	// 		IFNULL(
	// 			IFNULL(dsoj.jumlah_so_jual, 0) +
	// 			IFNULL(dbm.jumlah_bm, 0) -
	// 			IFNULL(dpj.jumlah_beli, 0) + 
	// 			IFNULL(drpjt.jumlah_retur, 0) - 
	// 			IFNULL(drpbt.jumlah_retur, 0)
	// 		, 0) AS total_stok_fix,
	// 		IFNULL(
	// 			IFNULL(dsor.jumlah_so_rusak, 0) +
	// 			IFNULL(drpjr.jumlah_retur, 0) - 
	// 			IFNULL(drpbr.jumlah_retur, 0)
	// 		, 0) AS total_stok_rusak_fix,
	// 		pp1.nama_pegawai AS pegawai_save, pp2.nama_pegawai As pegawai_edit');
	// 	$this->db->from('vamr4846_toko_mrc.barang_toko_'.$kode_customer_pusat.' AS bt');
	// 	$this->db->join('vamr4846_vama.barang_pusat AS bp','bp.kode_barang=bt.kode_barang','LEFT');

	// 	// Untuk stok jual
	// 	$this->db->join('(SELECT id_barang_pusat, SUM(jumlah_so) AS jumlah_so_jual FROM vamr4846_toko_mrc.stok_opname_toko_'.$kode_customer_pusat.'  
	// 					  WHERE DATE(tanggal_pembuatan) BETWEEN "'.$tanggal_awal.'" AND "'.$tanggal_akhir.'" 
	// 					  AND masuk_stok="JUAL"
	// 					  GROUP BY id_barang_pusat) AS dsoj', 'dsoj.id_barang_pusat=bp.id_barang_pusat', 'LEFT');
	// 	$this->db->join('(SELECT pd.id_barang_pusat, SUM(pd.jumlah_masuk) AS jumlah_bm
	// 					  FROM vamr4846_vama.penjualan_detail AS pd
	// 					  LEFT JOIN vamr4846_vama.penjualan_master AS pm ON pm.id_penjualan_m=pd.id_penjualan_m
	// 					  WHERE pm.id_customer_pusat LIKE "%'.$id_customer_pusat.'%" AND 
	// 					  		DATE(pd.tanggal_pembuatan) BETWEEN "'.$tanggal_awal.'" AND "'.$tanggal_akhir.'"
	// 					  GROUP BY pd.id_barang_pusat) AS dbm', 'dbm.id_barang_pusat=bp.id_barang_pusat', 'LEFT');
	// 	$this->db->join('(SELECT id_barang_pusat, SUM(jumlah_beli) AS jumlah_beli FROM vamr4846_toko_mrc.penjualan_detail_'.$kode_customer_pusat.'  
	// 					  WHERE DATE(tanggal_pembuatan) BETWEEN "'.$tanggal_awal.'" AND "'.$tanggal_akhir.'" 
	// 					  GROUP BY id_barang_pusat) AS dpj', 'dpj.id_barang_pusat=bp.id_barang_pusat', 'LEFT');
	// 	$this->db->join('(SELECT id_barang_pusat, SUM(jumlah_retur) AS jumlah_retur FROM vamr4846_toko_mrc.retur_penjualan_detail_'.$kode_customer_pusat.'  
	// 					  WHERE masuk_stok="JUAL" AND DATE(tanggal_pembuatan) BETWEEN "'.$tanggal_awal.'" AND "'.$tanggal_akhir.'" 
	// 					  GROUP BY id_barang_pusat) AS drpjt', 'drpjt.id_barang_pusat=bp.id_barang_pusat', 'LEFT');
	// 	$this->db->join('(SELECT rpd.id_barang_pusat, SUM(rpd.jumlah_retur) AS jumlah_retur
	// 					  FROM vamr4846_toko_mrc.retur_pembelian_detail_'.$kode_customer_pusat.' AS rpd
	// 					  WHERE rpd.dari_stok="JUAL" AND 
	// 					  		DATE(rpd.tanggal_pembuatan) BETWEEN "'.$tanggal_awal.'" AND "'.$tanggal_akhir.'"
	// 					  GROUP BY rpd.id_barang_pusat) AS drpbt', 'drpbt.id_barang_pusat=bp.id_barang_pusat', 'LEFT');
	// 	// ---------------------------------------------------------------------------------------------------------------------------------------

	// 	// Untuk stok rusak
	// 	$this->db->join('(SELECT id_barang_pusat, SUM(jumlah_so) AS jumlah_so_rusak FROM vamr4846_toko_mrc.stok_opname_toko_'.$kode_customer_pusat.'  
	// 					  WHERE DATE(tanggal_pembuatan) BETWEEN "'.$tanggal_awal.'" AND "'.$tanggal_akhir.'" 
	// 					  AND masuk_stok="RUSAK"
	// 					  GROUP BY id_barang_pusat) AS dsor', 'dsor.id_barang_pusat=bp.id_barang_pusat', 'LEFT');
	// 	$this->db->join('(SELECT id_barang_pusat, SUM(jumlah_retur) AS jumlah_retur FROM vamr4846_toko_mrc.retur_penjualan_detail_'.$kode_customer_pusat.'  
	// 					  WHERE masuk_stok="RUSAK" AND DATE(tanggal_pembuatan) BETWEEN "'.$tanggal_awal.'" AND "'.$tanggal_akhir.'" 
	// 					  GROUP BY id_barang_pusat) AS drpjr', 'drpjr.id_barang_pusat=bp.id_barang_pusat', 'LEFT');
	// 	$this->db->join('(SELECT rpd.id_barang_pusat, SUM(rpd.jumlah_retur) AS jumlah_retur
	// 					  FROM vamr4846_toko_mrc.retur_pembelian_detail_'.$kode_customer_pusat.' AS rpd
	// 					  WHERE rpd.dari_stok="RUSAK" AND 
	// 					  		DATE(rpd.tanggal_pembuatan) BETWEEN "'.$tanggal_awal.'" AND "'.$tanggal_akhir.'"
	// 					  GROUP BY rpd.id_barang_pusat) AS drpbr', 'drpbr.id_barang_pusat=bp.id_barang_pusat', 'LEFT');
	// 	// ---------------------------------------------------------------------------------------------------------------------------------------

	// 	$this->db->join('vamr4846_vama.customer_pusat AS cp','cp.kode_customer_pusat=bt.kode_toko','LEFT');
	// 	$this->db->join('vamr4846_vama.pegawai_pusat AS pp1', 'pp1.id_pegawai=bt.id_pegawai_pembuatan', 'LEFT');
	// 	$this->db->join('vamr4846_vama.pegawai_pusat AS pp2', 'pp2.id_pegawai=bt.id_pegawai_pembaharuan', 'LEFT');

	// 	$this->db->where(array (
	// 		'bt.status_hapus'     => 'TIDAK',
	// 		'bt.kode_barang LIKE' => '%'.$kode_barang.'%'
	// 	));	
	// 	$this->db->like('cp.kode_customer_pusat', $kode_customer_pusat);

	// 	$i = 0;
	// 	foreach ($this->column_search as $item){
	// 		if($_POST['search']['value']){
	// 			if($i===0){
	// 				$this->db->group_start();
	// 				$this->db->like($item, $_POST['search']['value']);
	// 			}else{
	// 				$this->db->or_like($item, $_POST['search']['value']);
	// 			}

	// 			if(count($this->column_search) - 1 == $i)
	// 				$this->db->group_end();
	// 		}
	// 		$i++;
	// 	}
		
	// 	if(isset($_POST['order'])){
	// 		$this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
	// 	}else if(isset($this->order)){
	// 		$order = $this->order;
	// 		$this->db->order_by(key($order), $order[key($order)]);
	// 	}
	// }
	// Akhir query real time stok toko

	// Awal query tidak real time stok toko
	private function _get_datatables_query($kode_barang, $kode_customer_pusat, $id_customer_pusat, $tanggal_awal, $tanggal_akhir)
	{		
		$this->db->select('
			bt.*, bp.nama_barang, cp.nama_customer_pusat, bp.foto, cp.kode_customer_pusat AS kode_toko,
			pp1.nama_pegawai AS pegawai_save, pp2.nama_pegawai As pegawai_edit');
		$this->db->from('vamr4846_toko_mrc.barang_toko_'.$kode_customer_pusat.' AS bt');
		$this->db->join('vamr4846_vama.barang_pusat AS bp','bp.kode_barang=bt.kode_barang','LEFT');
		$this->db->join('vamr4846_vama.customer_pusat AS cp','cp.kode_customer_pusat=bt.kode_toko','LEFT');
		$this->db->join('vamr4846_vama.pegawai_pusat AS pp1', 'pp1.id_pegawai=bt.id_pegawai_pembuatan', 'LEFT');
		$this->db->join('vamr4846_vama.pegawai_pusat AS pp2', 'pp2.id_pegawai=bt.id_pegawai_pembaharuan', 'LEFT');

		$this->db->where(array (
			'bt.status_hapus'     => 'TIDAK',
			'bt.kode_barang LIKE' => '%'.$kode_barang.'%'
		));	
		$this->db->like('cp.kode_customer_pusat', $kode_customer_pusat);

		$i = 0;
		foreach ($this->column_search as $item){
			if($_POST['search']['value']){
				if($i===0){
					$this->db->group_start();
					$this->db->like($item, $_POST['search']['value']);
				}else{
					$this->db->or_like($item, $_POST['search']['value']);
				}

				if(count($this->column_search) - 1 == $i)
					$this->db->group_end();
			}
			$i++;
		}
		
		if(isset($_POST['order'])){
			$this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		}else if(isset($this->order)){
			$order = $this->order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}
	// Akhir query tidak real time stok toko
	// Akhir datatable master barang toko

	// Awal datatable daftar transaksi
	private function _get_datatables_query_transaksi($kode_customer_pusat, $id_customer_pusat, $tanggal_awal, $tanggal_akhir)
	{		
		$this->db->select('
			cp.nama_customer_pusat, bt.id_barang_toko, bt.sku, bp.nama_barang, bt.harga_eceran, 
			IFNULL(dsoj.jumlah_so_jual, 0) AS so,
			IFNULL(dbm.jumlah_bm, 0) AS bm, 
			IFNULL(dpj.jumlah_beli, 0) AS pj, 
			IFNULL(drpjt.jumlah_retur, 0) AS rpt,
			IFNULL(drpbt.jumlah_retur, 0) AS rbkpt,
			0 AS pp,
			IFNULL(
				IFNULL(dsoj.jumlah_so_jual, 0) +
				IFNULL(dbm.jumlah_bm, 0) -
				IFNULL(dpj.jumlah_beli, 0) + 
				IFNULL(drpjt.jumlah_retur, 0)+
				IFNULL(drpbt.jumlah_retur, 0)
			, 0) AS total_stok_fix,
			bt.total_stok, bt.total_stok_rusak
		');
		$this->db->from('vamr4846_toko_mrc.barang_toko_'.$kode_customer_pusat.' AS bt');
		$this->db->join('vamr4846_vama.barang_pusat AS bp', 'bp.kode_barang=bt.kode_barang', 'LEFT');
		$this->db->join('vamr4846_vama.customer_pusat AS cp', 'cp.kode_customer_pusat=bt.kode_toko', 'LEFT');
		
		// Untuk stok jual
		$this->db->join('(SELECT id_barang_pusat, SUM(jumlah_so) AS jumlah_so_jual FROM vamr4846_toko_mrc.stok_opname_toko_'.$kode_customer_pusat.'  
						  WHERE DATE(tanggal_pembuatan) BETWEEN "'.$tanggal_awal.'" AND "'.$tanggal_akhir.'" 
						  AND masuk_stok="JUAL"
						  GROUP BY id_barang_pusat) AS dsoj', 'dsoj.id_barang_pusat=bp.id_barang_pusat', 'LEFT');
		$this->db->join('(SELECT pd.id_barang_pusat, SUM(pd.jumlah_masuk) AS jumlah_bm
						  FROM vamr4846_vama.penjualan_detail AS pd
						  LEFT JOIN vamr4846_vama.penjualan_master AS pm ON pm.id_penjualan_m=pd.id_penjualan_m
						  WHERE pm.id_customer_pusat LIKE "%'.$id_customer_pusat.'%" AND 
						  		DATE(pd.tanggal_pembuatan) BETWEEN "'.$tanggal_awal.'" AND "'.$tanggal_akhir.'"
						  GROUP BY pd.id_barang_pusat) AS dbm', 'dbm.id_barang_pusat=bp.id_barang_pusat', 'LEFT');
		$this->db->join('(SELECT id_barang_pusat, SUM(jumlah_beli) AS jumlah_beli FROM vamr4846_toko_mrc.penjualan_detail_'.$kode_customer_pusat.'  
						  WHERE DATE(tanggal_pembuatan) BETWEEN "'.$tanggal_awal.'" AND "'.$tanggal_akhir.'" 
						  GROUP BY id_barang_pusat) AS dpj', 'dpj.id_barang_pusat=bp.id_barang_pusat', 'LEFT');
		$this->db->join('(SELECT id_barang_pusat, SUM(jumlah_retur) AS jumlah_retur FROM vamr4846_toko_mrc.retur_penjualan_detail_'.$kode_customer_pusat.'  
						  WHERE masuk_stok="JUAL" AND DATE(tanggal_pembuatan) BETWEEN "'.$tanggal_awal.'" AND "'.$tanggal_akhir.'" 
						  GROUP BY id_barang_pusat) AS drpjt', 'drpjt.id_barang_pusat=bp.id_barang_pusat', 'LEFT');
		$this->db->join('(SELECT rpd.id_barang_pusat, SUM(rpd.jumlah_retur) AS jumlah_retur
						  FROM vamr4846_toko_mrc.retur_pembelian_detail_'.$kode_customer_pusat.' AS rpd
						  WHERE rpd.dari_stok="JUAL" AND 
						  		DATE(rpd.tanggal_pembuatan) BETWEEN "'.$tanggal_awal.'" AND "'.$tanggal_akhir.'"
						  GROUP BY rpd.id_barang_pusat) AS drpbt', 'drpbt.id_barang_pusat=bp.id_barang_pusat', 'LEFT');
		// ---------------------------------------------------------------------------------------------------------------------------------------
		$this->db->where('bt.status_hapus','TIDAK');
		$this->db->like('bt.kode_toko', $kode_customer_pusat);

		$i = 0;
		foreach ($this->column_search_tr as $item){
			if($_POST['search']['value']){
				if($i===0){
					$this->db->group_start();
					$this->db->like($item, $_POST['search']['value']);
				}else{
					$this->db->or_like($item, $_POST['search']['value']);
				}

				if(count($this->column_search_tr) - 1 == $i)
				$this->db->group_end();
			}
			$i++;
		}
		
		if(isset($_POST['order'])){
			$this->db->order_by($this->column_order_tr[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		}else if(isset($this->order_tr)){
			$order = $this->order_tr;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}
	// Akhir datatable daftar transaksi

	// Awal datatable daftar transaksi rusak
	private function _get_datatables_query_transaksi_rusak($kode_customer_pusat, $id_customer_pusat, $tanggal_awal, $tanggal_akhir)
	{		
		$this->db->select('
			cp.nama_customer_pusat, bt.id_barang_toko, bt.sku, bp.nama_barang, bt.harga_eceran, 
			IFNULL(dsor.jumlah_so_rusak, 0)  AS sor,
			IFNULL(drpjr.jumlah_retur, 0) AS rpr, 
			IFNULL(drpbr.jumlah_retur, 0) AS rbkpr, 
			bt.total_stok_rusak,
			IFNULL(
				IFNULL(dsor.jumlah_so_rusak, 0) +
				IFNULL(drpjr.jumlah_retur, 0) - 
				IFNULL(drpbr.jumlah_retur, 0)
			, 0) AS total_stok_rusak_fix
		');
		$this->db->from('vamr4846_toko_mrc.barang_toko_'.$kode_customer_pusat.' AS bt');
		$this->db->join('vamr4846_vama.barang_pusat AS bp','bp.kode_barang=bt.kode_barang','LEFT');
		$this->db->join('vamr4846_vama.customer_pusat AS cp','cp.kode_customer_pusat=bt.kode_toko','LEFT');

		// Untuk stok rusak
		$this->db->join('(SELECT id_barang_pusat, SUM(jumlah_so) AS jumlah_so_rusak FROM vamr4846_toko_mrc.stok_opname_toko_'.$kode_customer_pusat.'  
						  WHERE DATE(tanggal_pembuatan) BETWEEN "'.$tanggal_awal.'" AND "'.$tanggal_akhir.'" 
						  AND masuk_stok="RUSAK"
						  GROUP BY id_barang_pusat) AS dsor', 'dsor.id_barang_pusat=bp.id_barang_pusat', 'LEFT');
		$this->db->join('(SELECT id_barang_pusat, SUM(jumlah_retur) AS jumlah_retur FROM vamr4846_toko_mrc.retur_penjualan_detail_'.$kode_customer_pusat.'  
						  WHERE masuk_stok="RUSAK" AND DATE(tanggal_pembuatan) BETWEEN "'.$tanggal_awal.'" AND "'.$tanggal_akhir.'" 
						  GROUP BY id_barang_pusat) AS drpjr', 'drpjr.id_barang_pusat=bp.id_barang_pusat', 'LEFT');
		$this->db->join('(SELECT rpd.id_barang_pusat, SUM(rpd.jumlah_retur) AS jumlah_retur
						  FROM vamr4846_toko_mrc.retur_pembelian_detail_'.$kode_customer_pusat.' AS rpd
						  WHERE rpd.dari_stok="RUSAK" AND 
						  		DATE(rpd.tanggal_pembuatan) BETWEEN "'.$tanggal_awal.'" AND "'.$tanggal_akhir.'"
						  GROUP BY rpd.id_barang_pusat) AS drpbr', 'drpbr.id_barang_pusat=bp.id_barang_pusat', 'LEFT');
		// ---------------------------------------------------------------------------------------------------------------------------------------

		$this->db->where('bt.status_hapus','TIDAK');
		$this->db->like('bt.kode_toko', $kode_customer_pusat);

		$i = 0;
	
		foreach ($this->column_search_trr as $item){
			if($_POST['search']['value']){
				if($i===0){
					$this->db->group_start();
					$this->db->like($item, $_POST['search']['value']);
				}else{
					$this->db->or_like($item, $_POST['search']['value']);
				}

				if(count($this->column_search_trr) - 1 == $i)
				$this->db->group_end();
			}
			$i++;
		}
		
		if(isset($_POST['order'])){
			$this->db->order_by($this->column_order_trr[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		}else if(isset($this->order_trr)){
			$order = $this->order_trr;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}
	// Akhir datatable daftar transaksi rusak

	// Awal datatable pembaharuan barang
	private function _get_datatables_query_pembaharuan_barang($kode_customer_pusat, $tanggal_awal, $tanggal_akhir)
	{		
		$this->db->select('cp.nama_customer_pusat, bt.id_barang_toko, bt.sku, bp.nama_barang, bt.harga_eceran, bp.foto, 
						   bt.status, bt.status2, IFNULL(bt.total_stok,0) AS total_stok, 
						   IFNULL(bt.total_stok_rusak,0) AS total_stok_rusak,
						   bt.tanggal_pembuatan, bt.tanggal_pembaharuan, 
						   pp1.nama_pegawai AS pegawai_save, pp2.nama_pegawai As pegawai_edit');
		$this->db->from('vamr4846_toko_mrc.barang_toko_'.$kode_customer_pusat.' AS bt');
		$this->db->join('vamr4846_vama.barang_pusat AS bp','bp.kode_barang=bt.kode_barang','LEFT');
		$this->db->join('vamr4846_vama.customer_pusat AS cp','cp.kode_customer_pusat=bt.kode_toko','LEFT');
		$this->db->join('vamr4846_vama.pegawai_pusat AS pp1', 'pp1.id_pegawai=bt.id_pegawai_pembuatan', 'LEFT');
		$this->db->join('vamr4846_vama.pegawai_pusat AS pp2', 'pp2.id_pegawai=bt.id_pegawai_pembaharuan', 'LEFT');
		$this->db->where(array(
			'bt.status_hapus' 					=> 'TIDAK',
			'DATE(bt.tanggal_pembaharuan) >='	=> $tanggal_awal,
			'DATE(bt.tanggal_pembaharuan) <='	=> $tanggal_akhir 
		));
		$this->db->like('bt.kode_toko', $kode_customer_pusat);

		$i = 0;
		foreach ($this->column_search_pb as $item){
			if($_POST['search']['value']){
				if($i===0){
					$this->db->group_start();
					$this->db->like($item, $_POST['search']['value']);
				}else{
					$this->db->or_like($item, $_POST['search']['value']);
				}

				if(count($this->column_search_pb) - 1 == $i)
				$this->db->group_end();
			}
			$i++;
		}
		
		if(isset($_POST['order'])){
			$this->db->order_by($this->column_order_pb[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		}else if(isset($this->order_pb)){
			$order = $this->order_pb;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}
	// Akhir datatable pembaharuan barang

	function get_datatables($kode_barang, $kode_customer_pusat, $id_customer_pusat, $query, $tanggal_awal, $tanggal_akhir){
		if($query == '_get_datatables_query'){
			$this->$query($kode_barang, $kode_customer_pusat, $id_customer_pusat, $tanggal_awal, $tanggal_akhir);	
		}else if($query == '_get_datatables_query_pembaharuan_barang'){
			$this->$query($kode_customer_pusat, $tanggal_awal, $tanggal_akhir);
		}else{
			$this->$query($kode_customer_pusat, $id_customer_pusat, $tanggal_awal, $tanggal_akhir);
		}
		
		if($_POST['length'] != -1)
		$this->db->limit($_POST['length'], $_POST['start']);
		$query = $this->db->get();
		return $query->result();
	}

	function count_filtered($kode_barang, $kode_customer_pusat, $id_customer_pusat, $query, $tanggal_awal, $tanggal_akhir){
		if($query == '_get_datatables_query'){
			$this->$query($kode_barang, $kode_customer_pusat, $id_customer_pusat, $tanggal_awal, $tanggal_akhir);	
		}else if($query == '_get_datatables_query_pembaharuan_barang'){
			$this->$query($kode_customer_pusat, $tanggal_awal, $tanggal_akhir);
		}else{
			$this->$query($kode_customer_pusat, $id_customer_pusat, $tanggal_awal, $tanggal_akhir);
		}
		$query = $this->db->get();
		return $query->num_rows();
	}

	public function count_all($kode_barang, $kode_customer_pusat){
		$this->db->from('vamr4846_toko_mrc.barang_toko_'.$kode_customer_pusat.' AS bt');
		$this->db->join('vamr4846_vama.customer_pusat AS cp', 'cp.kode_customer_pusat=bt.kode_toko', 'LEFT');
		$this->db->WHERE(array(
			'bt.status_hapus'  => 'TIDAK',
			'kode_barang LIKE' => '%'.$kode_barang.'%'
		));
		$this->db->like('cp.kode_customer_pusat', $kode_customer_pusat);
		return $this->db->count_all_results();
	}

	public function ambil_data_toko($kode_toko){
		$this->db->select('*');
		$this->db->from('mrc.master_toko ');
		$this->db->where(array(
			'Kode_Toko' => $kode_toko
		));
		$query = $this->db->get();
		return $query->row();
	}

	function validasi_stok_opname($id_barang_pusat, $kode_toko)
	{
		$this->db->select('id_barang_pusat, jumlah_so, id_periode_stok_toko');
		$this->db->from('vamr4846_toko_mrc.stok_opname_toko_'.$kode_toko.'');
		$this->db->where(array(
			'id_barang_pusat' => $id_barang_pusat
		));
		$query = $this->db->get();
		return $query->row();
	}

	function ambil_barang_toko_lama_v1($kode_toko, $tanggal_awal, $tanggal_akhir)
	{
		$sql = "
			SELECT 
				bp.id_barang_pusat, MBT.Kode_Barang, MB.SKU, MBT.No_Induk, MB.Nama_Barang, 
				IFNULL(sot.total_so,0) AS total_so,
				IFNULL(bkd.total_barang_keluar,0) AS total_barang_keluar,
				IFNULL(rpjdt.total_retur_jual,0) AS total_retur_jual,
				IFNULL(pjdt.total_jual,0) AS total_jual,
				IFNULL(rbt.total_retur_barang,0) AS total_retur_barang,
				IFNULL(pp.total_penukaran_poin,0) AS total_penukaran_poin,
				
				(IFNULL(sot.total_so,0)+
				IFNULL(bkd.total_barang_keluar,0)+
				IFNULL(rpjdt.total_retur_jual,0)-
				IFNULL(pjdt.total_jual,0)-
				IFNULL(rbt.total_retur_barang,0)-
				IFNULL(pp.total_penukaran_poin,0)) AS Stok, 
				(IFNULL(rpjdt2.total_retur_jual_rusak,0)-IFNULL(rbt2.total_retur_barang_rusak,0)) AS Stok_Rusak, 
				MB.Harga_Eceran AS Eceran_Pusat, MBT.Harga_Eceran AS Eceran_Toko, 
				MBT.Status AS Status1, MBT.Status2, MBT.Operator , MBT.Nama_Komputer, MBT.Tanggal_Pembuatan, MBT.Tanggal_Pembaharuan 
			FROM mrc.Master_Barang_Toko AS MBT
			LEFT JOIN vamr4846_vama.barang_pusat AS bp ON MBT.Kode_Barang=bp.kode_barang
			LEFT JOIN mrc.Master_Barang AS MB ON MBT.Kode_Barang=MB.Kode_Barang 
			LEFT JOIN mrc.Master_Toko AS MT ON MBT.Kode_Toko=MT.Kode_Toko 
			LEFT JOIN (
				SELECT sot.*, SUM(qty) AS total_so 
				FROM mrc.Stok_Opname_Toko AS sot 
				WHERE Kode_Toko='".$kode_toko."' AND DATE(Tanggal_Pembuatan) BETWEEN '".$tanggal_awal."' AND '".$tanggal_akhir."' GROUP BY kode_barang
			) AS SOT ON SOT.Kode_Barang=MBT.Kode_Barang 
			LEFT JOIN (
				SELECT bkd.*, SUM(qty_masuk) AS total_barang_keluar, bk.tanggal_barang_keluar 
				FROM mrc.Barang_Keluar AS bk, mrc.barang_keluar_detail AS bkd 
				WHERE bk.No_Barang_Keluar=bkd.No_Barang_Keluar AND 
				bk.Kode_Toko='".$kode_toko."' AND DATE(BKD.Tanggal_Pembaharuan) BETWEEN '".$tanggal_awal."' AND '".$tanggal_akhir."' GROUP BY kode_barang
			) AS BKD ON BKD.Kode_Barang=MBT.Kode_Barang 
			LEFT JOIN (
				SELECT *, SUM(qty) AS total_jual 
				FROM mrc.Penjualan_Detail_Toko 
				WHERE Kode_Toko='".$kode_toko."' AND DATE(Tanggal_Pembuatan) BETWEEN '".$tanggal_awal."' AND '".$tanggal_akhir."' GROUP BY kode_barang
			) AS PJDT ON PJDT.Kode_Barang=MB.Kode_Barang 
			LEFT JOIN (
				SELECT *, SUM(qty_retur) AS total_retur_jual 
				FROM mrc.Retur_Penjualan_Detail_Toko 
				WHERE Kode_Toko='".$kode_toko."' AND Masuk_Stok='JUAL' AND DATE(Tanggal_Pembuatan) BETWEEN '".$tanggal_awal."' AND '".$tanggal_akhir."' GROUP BY kode_barang
			) AS RPJDT ON RPJDT.Kode_Barang=MBT.Kode_Barang 
			LEFT JOIN (
				SELECT *, SUM(qty_retur) AS total_retur_jual_rusak 
				FROM mrc.Retur_Penjualan_Detail_Toko 
				WHERE Kode_Toko='".$kode_toko."' AND Masuk_Stok='RUSAK' AND DATE(Tanggal_Pembuatan) BETWEEN '".$tanggal_awal."' AND '".$tanggal_akhir."' GROUP BY kode_barang
			) AS RPJDT2 ON RPJDT2.Kode_Barang=MB.Kode_Barang 
			LEFT JOIN (
				SELECT *, SUM(qty_retur) AS total_retur_barang 
				FROM mrc.Retur_Barang_Toko 
				WHERE Kode_Toko='".$kode_toko."' AND Dari_Stok='JUAL' AND DATE(Tanggal_Pembuatan) BETWEEN '".$tanggal_awal."' AND '".$tanggal_akhir."' GROUP BY kode_barang
			) AS RBT ON RBT.Kode_Barang=MBT.Kode_Barang 
			LEFT JOIN (
				SELECT *, SUM(qty_retur) AS total_retur_barang_rusak 
				FROM mrc.Retur_Barang_Toko 
				WHERE Kode_Toko='".$kode_toko."' AND Dari_Stok='RUSAK' AND DATE(Tanggal_Pembuatan) BETWEEN '".$tanggal_awal."' AND '".$tanggal_akhir."' GROUP BY kode_barang
			) AS RBT2 ON RBT2.Kode_Barang=MBT.Kode_Barang 
			LEFT JOIN (
				SELECT rf.kode_barang, SUM(pp.qty) AS total_penukaran_poin, pp.kode_penukaran, pp.kode_toko, pp.kode_customer, pp.jumlah_poin, pp.qty, pp.tanggal_penukaran 
				FROM mrc.Penukaran_Poin AS pp 
				LEFT JOIN mrc.Referensi_Penukaran AS rf ON rf.kode_referensi_penukaran=pp.kode_referensi_penukaran AND PP.Kode_Toko='".$kode_toko."' 
				WHERE DATE(pp.Tanggal_Penukaran) BETWEEN '".$tanggal_awal."' AND '".$tanggal_akhir."' GROUP BY kode_barang
			) AS PP ON PP.Kode_Barang=MB.Kode_Barang 
			WHERE MT.Kode_Toko='".$kode_toko."' AND MBT.Status_Hapus='TIDAK' ORDER BY NAMA_BARANG ASC
		";
		return $this->db->query($sql);
	}

	function ambil_barang_toko_lama_v2($kode_toko)
	{
		$sql = "
			SELECT
				bp.id_barang_pusat, MB.SKU, MBT.Kode_Barang, 
				MB.Harga_Eceran AS Eceran_Pusat, MBT.Harga_Eceran AS Eceran_Toko, 
				MBT.Status AS Status1, MBT.Status2, MBT.Status_Hapus, 
				MBT.Operator , MBT.Nama_Komputer, MBT.Tanggal_Pembuatan, MBT.Tanggal_Pembaharuan 
			FROM vamr4846_mrc.Master_Barang_Toko AS MBT
			LEFT JOIN vamr4846_mrc.Master_Barang AS MB ON MBT.Kode_Barang=MB.Kode_Barang 
			LEFT JOIN vamr4846_vama.barang_pusat AS bp ON MBT.Kode_Barang=bp.kode_barang AND MB.SKU=bp.sku
			LEFT JOIN vamr4846_mrc.Master_Toko AS MT ON MBT.Kode_Toko=MT.Kode_Toko
			WHERE MT.Kode_Toko='".$kode_toko."' ORDER BY MB.Nama_Barang ASC
		";
		return $this->db->query($sql);
	}

	function ambil_barang_untuk_penjualan($sku, $kode_toko)
	{
		$sql = "
				SELECT 
					bp.id_barang_pusat, bt.id_barang_toko, bp.kode_barang, bp.sku, bp.nama_barang, bp.foto, 
					bt.total_stok, bt.harga_eceran AS harga_satuan
				FROM 
					vamr4846_vama.barang_pusat AS bp
				LEFT JOIN 
					vamr4846_toko_mrc.barang_toko_".$kode_toko." AS bt ON bt.id_barang_pusat=bp.id_barang_pusat
				WHERE 
					bt.status_hapus 	= 'TIDAK'
					AND bt.status 		= 'AKTIF'
					AND bt.sku 			= '".$this->db->escape_like_str($sku)."' 
		";
		return $this->db->query($sql);
	}

	function validasi_barang($sku, $kode_barang, $kode_toko)
	{
		$this->db->select('sku, kode_barang');
		$this->db->from('vamr4846_toko_mrc.barang_toko_'.$kode_toko.'');
		$this->db->where(array(
			'sku'         => $sku,
			'kode_barang' => $kode_barang,
			'kode_toko'   => $kode_toko
		));
		$query = $this->db->get();
		return $query->row();
	}

	public function daftar_toko(){
		$sql = "SELECT * FROM vamr4846_mrc.master_toko ORDER BY kode_toko ASC";
		return $this->db->query($sql);
	}

	function cari_barang_toko($keyword, $id_pm, $kode_toko){
		$sql = "
			SELECT 
				bp.id_barang_pusat, bp.kode_barang, bp.sku, bp.nama_barang, bp.foto, 
				bp.total_stok AS total_stok_pusat, bt.total_stok As total_stok_toko,
				bp.harga_eceran AS harga_eceran_pusat, FORMAT(bp.harga_eceran,0) AS eceran_pusat,
				bt.harga_eceran AS harga_eceran_toko, FORMAT(bt.harga_eceran,0) AS eceran_toko, bt.harga_eceran AS harga_satuan
			FROM 
				vamr4846_vama.barang_pusat AS bp
			LEFT JOIN 
				vamr4846_toko_mrc.barang_toko_".$kode_toko." as bt ON bp.sku=bt.sku and bt.kode_toko='".$this->db->escape_like_str($kode_toko)."'
			WHERE 
				bt.id_barang_pusat NOT IN (SELECT id_barang_pusat 
										  FROM vamr4846_vama.penjualan_detail AS PD 
										  WHERE id_penjualan_m='".$this->db->escape_like_str($id_pm)."') 
				AND bt.kode_toko 	= '".$this->db->escape_like_str($kode_toko)."'
				AND bp.status_hapus = 'tidak'
				AND bp.status 		= 'Aktif'
				AND bp.total_stok 	> 0 
				AND ( 
					bp.sku LIKE '%".$this->db->escape_like_str($keyword)."%' 
					OR bp.nama_barang LIKE '%".$this->db->escape_like_str($keyword)."%' 
				)
			LIMIT 10
		";

		return $this->db->query($sql);
	}

	function cari_stok_barang_toko($keyword, $id_pm, $kode_toko, $id_toko, $tanggal_awal, $tanggal_akhir){
		$sql = "
			SELECT 
				bp.id_barang_pusat, bp.kode_barang, bp.sku, bp.nama_barang, bp.foto, 
				bp.total_stok AS total_stok_pusat, bt.total_stok As total_stok_toko,
				bp.harga_eceran AS harga_eceran_pusat, FORMAT(bp.harga_eceran,0) AS eceran_pusat,
				bt.harga_eceran AS harga_eceran_toko, FORMAT(bt.harga_eceran,0) AS eceran_toko, bt.harga_eceran AS harga_satuan,
				IFNULL(
					IFNULL(dsoj.jumlah_so_jual, 0) +
					IFNULL(dbm.jumlah_bm, 0) -
					IFNULL(dpj.jumlah_beli, 0) + 
					IFNULL(drpjt.jumlah_retur, 0) - 
					IFNULL(drpbt.jumlah_retur, 0)
				, 0) AS total_stok_toko_fix
			FROM 
				vamr4846_vama.barang_pusat AS bp
			LEFT JOIN 
				vamr4846_toko_mrc.barang_toko_".$kode_toko." as bt 
				ON bp.sku=bt.sku and bt.kode_toko='".$this->db->escape_like_str($kode_toko)."'
			LEFT JOIN 
			(
				SELECT id_barang_pusat, SUM(jumlah_so) AS jumlah_so_jual FROM vamr4846_toko_mrc.stok_opname_toko_".$kode_toko."  
				WHERE DATE(tanggal_pembuatan) BETWEEN '".$tanggal_awal."' AND '".$tanggal_akhir."' 
				AND masuk_stok='JUAL'
				GROUP BY id_barang_pusat
			) AS dsoj ON dsoj.id_barang_pusat=bp.id_barang_pusat
			LEFT JOIN
			(
				SELECT pd.id_barang_pusat, SUM(pd.jumlah_masuk) AS jumlah_bm
				FROM vamr4846_vama.penjualan_detail AS pd
				LEFT JOIN vamr4846_vama.penjualan_master AS pm ON pm.id_penjualan_m=pd.id_penjualan_m
				WHERE pm.id_customer_pusat LIKE '%".$id_toko."%' AND 
				DATE(pd.tanggal_pembuatan) BETWEEN '".$tanggal_awal."' AND '".$tanggal_akhir."'
				GROUP BY pd.id_barang_pusat
			) AS dbm ON dbm.id_barang_pusat=bp.id_barang_pusat
			LEFT JOIN
			(
				SELECT id_barang_pusat, SUM(jumlah_beli) AS jumlah_beli FROM vamr4846_toko_mrc.penjualan_detail_".$kode_toko."  
				WHERE DATE(tanggal_pembuatan) BETWEEN '".$tanggal_awal."' AND '".$tanggal_akhir."' 
				GROUP BY id_barang_pusat
			) AS dpj ON dpj.id_barang_pusat=bp.id_barang_pusat
			LEFT JOIN
			(
				SELECT id_barang_pusat, SUM(jumlah_retur) AS jumlah_retur FROM vamr4846_toko_mrc.retur_penjualan_detail_".$kode_toko."  
				WHERE masuk_stok='JUAL' AND DATE(tanggal_pembuatan) BETWEEN '".$tanggal_awal."' AND '".$tanggal_akhir."' 
				GROUP BY id_barang_pusat
			) AS drpjt ON drpjt.id_barang_pusat=bp.id_barang_pusat
			LEFT JOIN
			(
				SELECT rpd.id_barang_pusat, SUM(rpd.jumlah_retur) AS jumlah_retur
				FROM vamr4846_toko_mrc.retur_pembelian_detail_".$kode_toko." AS rpd
				WHERE rpd.dari_stok='JUAL' AND 
				DATE(rpd.tanggal_pembuatan) BETWEEN '".$tanggal_awal."' AND '".$tanggal_akhir."'
				GROUP BY rpd.id_barang_pusat
			) AS drpbt ON drpbt.id_barang_pusat=bp.id_barang_pusat
			WHERE 
				bt.id_barang_pusat NOT IN (SELECT id_barang_pusat 
										  FROM vamr4846_vama.penjualan_detail AS PD 
										  WHERE id_penjualan_m='".$this->db->escape_like_str($id_pm)."') 
				AND bt.kode_toko 	= '".$this->db->escape_like_str($kode_toko)."'
				AND bp.status_hapus = 'tidak'
				AND bp.status 		= 'Aktif'
				AND bp.total_stok 	> 0 
				AND ( 
					bp.sku LIKE '%".$this->db->escape_like_str($keyword)."%' 
					OR bp.nama_barang LIKE '%".$this->db->escape_like_str($keyword)."%' 
				)
			LIMIT 50
		";

		return $this->db->query($sql);
	}

	function cari_kode_paket_promo($keyword, $id_master, $kode_toko)
	{
		$sql = "
			SELECT 
				bt.id_barang_pusat, bt.id_barang_toko,
				bp.kode_barang, bp.sku, bp.nama_barang, bp.foto, 
				bt.total_stok, bt.total_stok_rusak, 
				bt.harga_eceran, FORMAT(bt.harga_eceran,0) AS harga
			FROM 
				vamr4846_toko_mrc.barang_toko_".$kode_toko." AS bt
			LEFT JOIN 
				vamr4846_vama.barang_pusat AS bp ON bp.id_barang_pusat=bt.id_barang_pusat
			WHERE 
				bt.status_hapus = 'TIDAK'
				AND bt.status   = 'AKTIF'
				AND ( 	
					bp.sku LIKE '%".$this->db->escape_like_str($keyword)."%' 
					OR bp.nama_barang LIKE '%".$this->db->escape_like_str($keyword)."%'
				) 
				AND (bt.total_stok > 0 OR bt.total_stok_rusak > 0)
				AND bt.id_barang_pusat NOT IN (
					SELECT id_barang_pusat 
					FROM vamr4846_toko_mrc.paket_detail AS PD 
					WHERE id_paket_promo_m='".$this->db->escape_like_str($id_master)."'
				)
			LIMIT 50
		";
		return $this->db->query($sql);
	}

	public function get_stok($sku, $kode_toko, $jumlah_beli){
		$this->db->select('nama_barang, (total_stok + ".$jumlah_beli.")as total_stok');
		$this->db->from('vamr4846_toko_mrc.barang_toko_'.$kode_toko.'');
		$this->db->where(array(
			'sku'       => $sku,
			'kode_toko' => $kode_toko
		));
		$query=$this->db->get();
		return $query->row();
	}

	public function update_harga_by_id($id_barang_pusat, $kode_toko, $harga_satuan, $id_pegawai){
		$sql = "
				UPDATE 
					vamr4846_toko_mrc.barang_toko_".$kode_toko." 
				SET 
					harga_eceran = '".$harga_satuan."',
					id_pegawai_pembaharuan = '".$id_pegawai."'
				WHERE 
					id_barang_pusat = '".$id_barang_pusat."'";
		return $this->db->query($sql);
	}

	public function update_harga_barang_toko($sku, $kode_toko, $harga_satuan){
		$sql = "UPDATE vamr4846_toko_mrc.barang_toko_".$kode_toko." SET harga_eceran = '".$harga_satuan."' 
				WHERE sku = '".$sku."' AND kode_toko = '".$kode_toko."'";
		return $this->db->query($sql);
	}

	public function update_stok($sku, $kode_toko, $jumlah_beli, $harga_satuan){
		$sql = "UPDATE vamr4846_toko_mrc.barang_toko_".$kode_toko." SET total_stok = total_stok + ".$jumlah_beli.", harga_eceran = '".$harga_satuan."' 
				WHERE sku = '".$sku."' AND kode_toko = '".$kode_toko."'";
		return $this->db->query($sql);
	}

	public function update_tambah_stok($sku, $kode_toko, $jumlah){
		$sql = "UPDATE vamr4846_toko_mrc.barang_toko_".$kode_toko." SET total_stok = total_stok + ".$jumlah." 
				WHERE sku = '".$sku."' AND kode_toko = '".$kode_toko."'";
		return $this->db->query($sql);
	}

	public function update_tambah_stok_rusak($sku, $kode_toko, $jumlah){
		$sql = "UPDATE vamr4846_toko_mrc.barang_toko_".$kode_toko." SET total_stok_rusak = total_stok_rusak + ".$jumlah." 
				WHERE sku = '".$sku."' AND kode_tok = '".$kode_toko."'" ;
		return $this->db->query($sql);
	}

	public function listing(){
		$this->db->select('bt.*, bp.nama_barang, cp.nama_customer_pusat, bp.foto');
		$this->db->from('vamr4846_toko_mrc.barang_toko AS bt');
		$this->db->join('vamr4846_vama.barang_pusat AS bp','bp.kode_barang=bt.kode_barang','LEFT');
		$this->db->join('vamr4846_vama.customer_pusat AS cp','cp.kode_customer_pusat=bt.kode_toko','LEFT');
		$this->db->order_by('id_barang_toko','DESC');
		$query=$this->db->get();
		return $query->result();
	}		

	public function tambah($data, $table){
		$this->db->insert($table, $data);
	}	

	public function cek_stok($id_barang_pusat, $kode_toko){
		$this->db->select('bp.nama_barang, bp.sku, bp.nama_barang, bt.total_stok');
		$this->db->from('vamr4846_toko_mrc.barang_toko_'.$kode_toko.' AS bt');
		$this->db->join('vamr4846_vama.barang_pusat AS bp', 'bp.id_barang_pusat=bt.id_barang_pusat', 'LEFT');
		$this->db->where(array(
			'bp.id_barang_pusat' => $id_barang_pusat
		));
		$query=$this->db->get();
		return $query->row();
	}
	
	public function get_by_id_barang_pusat($id_barang_pusat, $kode_toko){
		$this->db->select('bt.*, bp.nama_barang, cp.nama_customer_pusat, bp.foto');
		$this->db->from('vamr4846_toko_mrc.barang_toko_'.$kode_toko.' AS bt');
		$this->db->join('vamr4846_vama.barang_pusat AS bp','bp.kode_barang=bt.kode_barang','LEFT');
		$this->db->join('vamr4846_vama.customer_pusat AS cp','cp.kode_customer_pusat=bt.kode_toko','LEFT');
		$this->db->where('bt.id_barang_pusat', $id_barang_pusat);
		$query = $this->db->get();
		return $query->row();
	}

	public function get_by_id($id_barang_toko, $kode_toko){
		$this->db->select('bt.*, bp.nama_barang, cp.nama_customer_pusat, bp.foto');
		$this->db->from('vamr4846_toko_mrc.barang_toko_'.$kode_toko.' AS bt');
		$this->db->join('vamr4846_vama.barang_pusat AS bp','bp.kode_barang=bt.kode_barang','LEFT');
		$this->db->join('vamr4846_vama.customer_pusat AS cp','cp.kode_customer_pusat=bt.kode_toko','LEFT');
		$this->db->where('bt.id_barang_toko', $id_barang_toko);
		$query = $this->db->get();
		return $query->row();
	}

	public function get_barang_toko($id_barang_pusat, $kode_toko){
		$this->db->select('bt.*, bp.nama_barang, cp.nama_customer_pusat, bp.foto');
		$this->db->from('vamr4846_toko_mrc.barang_toko_'.$kode_toko.' AS bt');
		$this->db->join('vamr4846_vama.barang_pusat AS bp','bp.kode_barang=bt.kode_barang','LEFT');
		$this->db->join('vamr4846_vama.customer_pusat AS cp','cp.kode_customer_pusat=bt.kode_toko','LEFT');
		$this->db->where(array(	
			'bt.id_barang_pusat' => $id_barang_pusat,
			'bt.kode_toko'       => $kode_toko
		));
		$query = $this->db->get();
		return $query->row();		
	}

	public function save($data){
		$this->db->insert($this->table, $data);
		return $this->db->insert_id();
	}

	public function update($where, $data, $table){
		$this->db->update($table, $data, $where);
		return $this->db->affected_rows();
	}

	public function delete_by_id($id_barang_toko){
		$this->db->where('id_barang_toko', $id_barang_toko);
		$this->db->delete($this->table);
	}

	public function update_status_hapus($id_barang_toko, $id_pegawai)
	{
		$sql = "UPDATE vamr4846_toko_mrc.barang_toko 
				SET status_hapus = 'IYA', id_pegawai_pembaharuan = '".$id_pegawai."' 
				WHERE id_barang_toko = '".$id_barang_toko."'";
		return $this->db->query($sql);
	}
}
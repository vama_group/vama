<?php
class Retur_barang_dari_toko_detail_model extends CI_Model
{
	// Retur penjualan induk
	var $column_order        = array('bp.sku','bp.nama_barang');
	var $column_search       = array('bp.sku','bp.nama_barang'); 
	var $order               = array('bp.nama_barang' => 'ASC');
	// -------------------------------------------------------------------------------------------------------------------------------
	
	// Retur penjualan batal
	var $column_order_batal  = array('bp.sku','bp.nama_barang');
	var $column_search_batal = array('bp.sku','bp.nama_barang'); 
	var $order_batal         = array('bp.nama_barang' => 'ASC'); 
	// -------------------------------------------------------------------------------------------------------------------------------

	private function _get_datatables_query($kode_toko, $id_retur_pembelian_m)
	{		
		$this->db->select('
			rpd.*, rpm.status_retur,
			bp.sku, bp.nama_barang, bt.harga_eceran, 
			pp1.nama_pegawai_toko AS pegawai_save, IFNULL(pp2.nama_pegawai_toko, "-") AS pegawai_edit, IFNULL(pp3.nama_pegawai, "-") AS pegawai_masuk
		');
		$this->db->from('vamr4846_toko_mrc.retur_pembelian_detail_'.$kode_toko.' AS rpd');
		$this->db->join('vamr4846_toko_mrc.retur_pembelian_master_'.$kode_toko.' AS rpm', 
			'rpm.id_retur_pembelian_m=rpd.id_retur_pembelian_m', 'LEFT');
		$this->db->join('vamr4846_vama.barang_pusat as bp', 'bp.id_barang_pusat=rpd.id_barang_pusat', 'LEFT');
		$this->db->join('vamr4846_toko_mrc.barang_toko_'.$kode_toko.' as bt', 'bt.id_barang_pusat=rpd.id_barang_pusat', 'LEFT');
		$this->db->join('vamr4846_toko_mrc.pegawai_toko as pp1', 'pp1.id_pegawai_toko=rpd.id_pegawai_pembuatan', 'LEFT');
		$this->db->join('vamr4846_toko_mrc.pegawai_toko as pp2', 'pp2.id_pegawai_toko=rpd.id_pegawai_pembaharuan', 'LEFT');
		$this->db->join('vamr4846_vama.pegawai_pusat as pp3', 'pp3.id_pegawai=rpd.id_pegawai_masuk', 'LEFT');
		$this->db->where('rpd.id_retur_pembelian_m', $id_retur_pembelian_m);

		$i = 0;	
		foreach ($this->column_search as $item){
			if($_POST['search']['value']){
				if($i===0){
					$this->db->group_start();
					$this->db->like($item, $_POST['search']['value']);
				}else{
					$this->db->or_like($item, $_POST['search']['value']);
				}

				if(count($this->column_search) - 1 == $i)
					$this->db->group_end();
			}
			$i++;
		}
		
		if(isset($_POST['order'])){
			$this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		}else if(isset($this->order)){
			$order = $this->order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}

	private function _get_datatables_query_batal($kode_toko, $id_retur_pembelian_m)
	{		
		$this->db->select('
			rpdb.*,
			bp.sku, bp.nama_barang, bt.harga_eceran, 
			pp1.nama_pegawai_toko AS pegawai_save, IFNULL(pp2.nama_pegawai_toko, "-") AS pegawai_edit, 
			IFNULL(pp3.nama_pegawai, "-") AS pegawai_masuk, IFNULL(pp4.nama_pegawai_toko, "-") AS pegawai_pembatalan
		');
		$this->db->from('vamr4846_toko_mrc.retur_pembelian_detail_batal_'.$kode_toko.' AS rpdb');
		$this->db->join('vamr4846_vama.barang_pusat AS bp','bp.id_barang_pusat=rpdb.id_barang_pusat', 'LEFT');
		$this->db->join('vamr4846_toko_mrc.barang_toko_'.$kode_toko.' as bt', 'bt.id_barang_pusat=rpdb.id_barang_pusat', 'LEFT');
		$this->db->join('vamr4846_toko_mrc.pegawai_toko as pp1', 'pp1.id_pegawai_toko=rpdb.id_pegawai_pembuatan', 'LEFT');
		$this->db->join('vamr4846_toko_mrc.pegawai_toko as pp2', 'pp2.id_pegawai_toko=rpdb.id_pegawai_pembaharuan', 'LEFT');
		$this->db->join('vamr4846_vama.pegawai_pusat as pp3', 'pp3.id_pegawai=rpdb.id_pegawai_masuk', 'LEFT');
		$this->db->join('vamr4846_toko_mrc.pegawai_toko as pp4', 'pp4.id_pegawai_toko=rpdb.id_pegawai_pembatalan', 'LEFT');
		$this->db->where('rpdb.id_retur_pembelian_m', $id_retur_pembelian_m);

		$i = 0;	
		foreach ($this->column_search_batal as $item){
			if($_POST['search']['value']){
				if($i===0){
					$this->db->group_start();
					$this->db->like($item, $_POST['search']['value']);
				}else{
					$this->db->or_like($item, $_POST['search']['value']);
				}

				if(count($this->column_search_batal) - 1 == $i)
					$this->db->group_end();
			}
			$i++;
		}
		
		if(isset($_POST['order'])){
			$this->db->order_by($this->column_order_batal[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		}else if(isset($this->order_batal)){
			$order = $this->order_batal;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}

	function get_datatables($perintah, $kode_toko, $id_retur_pembelian_m)
	{
		$this->$perintah($kode_toko, $id_retur_pembelian_m);
		if($_POST['length'] != -1)
		$this->db->limit($_POST['length'], $_POST['start']);
		$query = $this->db->get();
		return $query->result();
	}

	function count_filtered($perintah, $kode_toko, $id_retur_pembelian_m)
	{
		$this->$perintah($kode_toko, $id_retur_pembelian_m);
		$query = $this->db->get();
		return $query->num_rows();
	}

	public function count_all($table, $kode_toko, $id_retur_pembelian_m)
	{
		$this->db->from($table);
		$this->db->where('id_retur_pembelian_m', $id_retur_pembelian_m);
		return $this->db->count_all_results();
	}

	function validasi_retur_barang($id_retur_pembelian_d, $id_retur_pembelian_m, $kode_toko)
	{
		$this->db->select('
			rdt.*,
			bp.sku, bp.nama_barang, bp.total_stok, bp.total_stok_rusak
		');
		$this->db->from('vamr4846_toko_mrc.retur_dari_toko AS rdt');
		$this->db->join('vamr4846_vama.barang_pusat AS bp', 'bp.id_barang_pusat=rpd.id_barang_pusat', 'LEFT');
		$this->db->where(array(
			'rdt.id_retur_pembelian_m' => $id_retur_pembelian_m,
			'rdt.id_retur_pembelian_d' => $id_retur_pembelian_d
		));
		$query = $this->db->get();
		return $query->row();
	}

	function get_retur_penjualan_detail($id_retur_pembelian_d, $id_retur_pembelian_m, $kode_toko)
	{
		$this->db->select('
			rpd.*, rpm.no_retur_pembelian, rdt.id_retur,
			bp.sku, bp.nama_barang, bp.total_stok, bp.total_stok_rusak
		');
		$this->db->from('vamr4846_toko_mrc.retur_pembelian_detail_'.$kode_toko.' AS rpd');
		$this->db->join('vamr4846_toko_mrc.retur_pembelian_master_'.$kode_toko.' AS rpm', 'rpd.id_retur_pembelian_m=rpm.id_retur_pembelian_m', 'LEFT');
		$this->db->join(
			'vamr4846_vama.retur_dari_toko AS rdt', 
			'rdt.id_retur_pembelian_m=rpm.id_retur_pembelian_m AND rdt.id_retur_pembelian_d=rpd.id_retur_pembelian_d AND rdt.no_retur_pembelian=rpm.no_retur_pembelian', 
			'LEFT'
		);
		$this->db->join('vamr4846_vama.barang_pusat AS bp', 'bp.id_barang_pusat=rpd.id_barang_pusat', 'LEFT');
		$this->db->where(array(
			'rpd.id_retur_pembelian_m' => $id_retur_pembelian_m,
			'rpd.id_retur_pembelian_d' => $id_retur_pembelian_d
		));
		$query = $this->db->get();
		return $query->row();
	}

	function get_retur_penjualan_detail_all($id_retur_penjualan_m)
	{
		$this->db->select('
			rpd.*, rpm.no_retur_penjualan, rpm.status_retur,
			pm.id_penjualan_m, pm.no_penjualan,
			rpm.id_customer_pusat, cp.nama_customer_pusat, rpm.tipe_customer_pusat,
			bp.id_barang_pusat, bp.sku, bp.nama_barang, bp.total_stok, bp.total_stok_rusak
		');
		$this->db->from('vamr4846_vama.retur_penjualan_detail as rpd');
		$this->db->join('vamr4846_vama.retur_penjualan_master as rpm', 'rpd.id_retur_penjualan_m=rpm.id_retur_penjualan_m', 'LEFT');
		$this->db->join('vamr4846_vama.penjualan_master as pm', 'pm.id_penjualan_m=rpm.id_penjualan_m', 'LEFT');
		$this->db->join('vamr4846_vama.customer_pusat as cp','cp.id_customer_pusat=rpm.id_customer_pusat', 'LEFT');
		$this->db->join('vamr4846_vama.barang_pusat as bp','bp.id_barang_pusat=rpd.id_barang_pusat', 'LEFT');
		$this->db->where('rpd.id_retur_penjualan_m', $id_retur_penjualan_m);
		$query = $this->db->get();
		return $query->result();
	}

	function get_jml_barang($id_retur_penjualan_m)
	{
		$this->db->select('COUNT(id_retur_penjualan_d) AS jumlah_barang');
		$this->db->from('vamr4846_vama.retur_penjualan_detail');
		$this->db->where('id_retur_penjualan_m', $id_retur_penjualan_m);
		$query = $this->db->get();
		return $query->row();
	}

	function get_detail($id_retur_penjualan_d)
	{
		$this->db->select('rpd.*, rpm.id_penjualan_m,
						  rpm.no_retur_penjualan, rpm.no_penjualan, rpm.id_customer_pusat, rpm.tipe_customer_pusat,
						  bp.nama_barang, bp.sku, bp.total_stok, bp.total_stok_rusak, rpm.status_retur');
		$this->db->from('vamr4846_vama.retur_penjualan_detail AS rpd');
		$this->db->join('vamr4846_vama.retur_penjualan_master AS rpm', 'rpm.id_retur_penjualan_m=rpd.id_retur_penjualan_m', 'LEFT');
		$this->db->join('vamr4846_vama.barang_pusat AS bp', 'bp.id_barang_pusat=rpd.id_barang_pusat', 'LEFT');
		$this->db->where('id_retur_penjualan_d', $id_retur_penjualan_d);
		$query = $this->db->get();
		return $query->row();
	}

	function simpan_retur_masuk(
		$id_retur_pembelian_d, $id_retur_pembelian_m, $no_retur_pembelian,
		$id_barang_pusat, $harga_satuan, $jumlah_retur,
		$subtotal, $dari_stok, $kondisi_barang, $keterangan,
		$id_pegawai, $tanggal, $kode_toko){
		if($kondisi_barang == 'BAIK'){
			$sql = "UPDATE vamr4846_vama.barang_pusat SET total_stok = total_stok + ".$jumlah_retur." 
						 WHERE id_barang_pusat = '".$id_barang_pusat."' ";
			$this->db->query($sql);	
		}elseif($kondisi_barang == 'RUSAK'){
			$sql = "UPDATE vamr4846_vama.barang_pusat SET total_stok_rusak = total_stok_rusak + ".$jumlah_retur." 
						 WHERE id_barang_pusat = '".$id_barang_pusat."' ";
			$this->db->query($sql);	
		}

		$sql = "
			INSERT vamr4846_vama.retur_dari_toko(
				id_retur_pembelian_d, id_retur_pembelian_m, kode_toko, no_retur_pembelian,
				id_barang_pusat, harga_satuan, jumlah_masuk,
				subtotal, dari_stok, kondisi_barang, keterangan,
				id_pegawai_pembuatan, id_pegawai_pembaharuan, tanggal_pembuatan
			) 
			VALUES(
				'".$id_retur_pembelian_d."', '".$id_retur_pembelian_m."', '".$kode_toko."', '".$no_retur_pembelian."',
				'".$id_barang_pusat."', '".$harga_satuan."', '".$jumlah_retur."',
				'".$subtotal."', '".$dari_stok."', '".$kondisi_barang."', '".$keterangan."',
				'".$id_pegawai."', '".$id_pegawai."', '".$tanggal."'
			)
		";
		$this->db->query($sql);	

		$dt = array(
			'jumlah_masuk'     => $jumlah_retur,
			'id_pegawai_masuk' => $id_pegawai,
			'tanggal_masuk'    => $tanggal
		);
		
		$where = array(
			'id_retur_pembelian_m' => $id_retur_pembelian_m,
			'id_retur_pembelian_d' => $id_retur_pembelian_d,
			'id_barang_pusat'      => $id_barang_pusat
		);
		return $this->db->update('vamr4846_toko_mrc.retur_pembelian_detail_'.$kode_toko.'', $dt, $where);
	}

	function get_detail_faktur($id_retur_penjualan_m)
	{
		$this->db->select('
			rpd.*,
			bp.sku, bp.kode_barang, bp.nama_barang
		');
		$this->db->from('vamr4846_vama.retur_penjualan_detail AS rpd');
		$this->db->join('vamr4846_vama.barang_pusat as bp','bp.id_barang_pusat=rpd.id_barang_pusat');
		$this->db->where('rpd.id_retur_penjualan_m', $id_retur_penjualan_m);
		$this->db->order_by('bp.nama_barang','ASC');
		$query = $this->db->get();
		return $query->result();
	}

	function get_total($id_retur_penjualan_m)
	{
		$this->db->select('
			IFNULL(id_retur_penjualan_m,"-") AS id_retur_penjualan_m, COUNT(id_retur_penjualan_d)AS jumlah_barang,
			IFNULL(SUM(subtotal_potongan),0) AS total_potongan, IFNULL(SUM(subtotal_saldo),0) AS total_saldo, 
			IFNULL(SUM(subtotal_retur),0) AS total_retur
		');
		$this->db->from('vamr4846_vama.retur_penjualan_detail');
		$this->db->where('id_retur_penjualan_m', $id_retur_penjualan_m);
		$query = $this->db->get();
		return $query->row();
	}

	function hapus_retur_barang_detail(
		$id_retur, $id_retur_pembelian_d, $id_retur_pembelian_m, $no_retur_pembelian,
		$id_barang_pusat, $harga_satuan, $jumlah_retur, $jumlah_masuk, 
		$subtotal, $dari_stok, $kondisi_barang, $keterangan, 
		$id_pegawai_pembuatan, $id_pegawai_pembaharuan, $id_pegawai,
		$tanggal_pembuatan, $tanggal_pembaharuan, $keterangan_batal, $kode_toko
	){
		// Kembalikan stok berdasarkan jumlah beli
		if($kondisi_barang == 'BAIK'){
			$v_kondisi_barang = 'total_stok';
		}elseif($kondisi_barang == 'RUSAK'){
			$v_kondisi_barang = 'total_stok_rusak';
		}
		
		$sql = "UPDATE vamr4846_vama.barang_pusat SET ".$v_kondisi_barang." = ".$v_kondisi_barang." - ".$jumlah_retur." 
				WHERE id_barang_pusat = '".$id_barang_pusat."'";
		$this->db->query($sql);

		// Simpan penjualan detail ke penjualan detail batal
		$sql = "
			INSERT INTO vamr4846_vama.retur_dari_toko_batal(
				id_retur, no_retur_pembelian, kode_toko, id_retur_pembelian_d, id_retur_pembelian_m,
				id_barang_pusat, harga_satuan, jumlah_retur, jumlah_masuk, 
				subtotal, dari_stok, kondisi_barang, keterangan, 
				id_pegawai_pembuatan, id_pegawai_pembaharuan, id_pegawai_pembatalan,
				tanggal_pembuatan, tanggal_pembaharuan, keterangan_batal
			)
			VALUES(
				'".$id_retur."', '".$no_retur_pembelian."', '".$kode_toko."', '".$id_retur_pembelian_d."', '".$id_retur_pembelian_m."',
				'".$id_barang_pusat."', '".$harga_satuan."', '".$jumlah_retur."', '".$jumlah_masuk."', 
				'".$subtotal."', '".$dari_stok."', '".$kondisi_barang."', '".$keterangan."', 
				'".$id_pegawai_pembuatan."', '".$id_pegawai_pembaharuan."', '".$id_pegawai."',
				'".$tanggal_pembuatan."', '".$tanggal_pembaharuan."', '".$keterangan_batal."'
			) 
		";
		$this->db->query($sql);

		$sql_delete = "
			DELETE FROM vamr4846_vama.retur_dari_toko 
			WHERE 
				id_retur_pembelian_m = '".$id_retur_pembelian_m."' AND
				id_retur_pembelian_d = '".$id_retur_pembelian_d."' AND
				no_retur_pembelian   = '".$no_retur_pembelian."' AND
				kode_toko            = '".$kode_toko."'
		";
		$this->db->query($sql_delete);

		// Update jumlah masuk menjadi 0
		$sql = "UPDATE vamr4846_toko_mrc.retur_pembelian_detail_".$kode_toko." SET jumlah_masuk='0' 
				WHERE id_barang_pusat = '".$id_barang_pusat."' AND id_retur_pembelian_d='".$id_retur_pembelian_d."'";
		return $this->db->query($sql);
	}
}
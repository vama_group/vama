<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Useraccess_pusat_model extends MY_Model {
	var $table         = 'vamr4846_vama.useraccess_pusat';
	var $column_order  = array('id_useraccess','nama_menu','act_read','act_create','act_update','act_update');
	var $column_search = array('id_useraccess','nama_menu','act_read','act_create','act_update','act_update'); 
	var $order         = array('orders' => 'ASC'); 

	public function __construct(){
		parent::__construct();
		$this->load->database();
	}

	private function _get_datatables_query($id_pegawai)
	{
		$this->db->select('IFNULL(up.id_pegawai,"-") AS id_pegawai, 
						   IFNULL(mp.id_menu,"-") AS id_menu, IFNULL(mp.id_menu_induk,"-") AS id_menu_induk, 
						   mp.nama_menu, mp.nama_lengkap_menu, mp.url, 
						   IFNULL(up.act_read,"-") AS act_read, IFNULL(up.act_create,"-") AS act_create,
						   IFNULL(up.act_update,"-") AS act_update, IFNULL(up.act_delete, "-") AS act_delete');
		$this->db->from('vamr4846_vama.useraccess_pusat AS up');
		$this->db->join('vamr4846_vama.menu_pusat AS mp','up.id_menu=mp.id_menu');
		$this->db->where(array(
				'id_pegawai'    => $id_pegawai,
				'mp.id_menu <>' => '1',
				'mp.url <>'     => 'sub_menu'
		));

		$i = 0;
	
		foreach ($this->column_search as $item){
			if($_POST['search']['value']){
				if($i===0){
					$this->db->group_start();
					$this->db->like($item, $_POST['search']['value']);
				}else{
					$this->db->or_like($item, $_POST['search']['value']);
				}

				if(count($this->column_search) - 1 == $i)
					$this->db->group_end();
			}
			$i++;
		}
		
		if(isset($_POST['order'])){
			$this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		}else if(isset($this->order)){
			$order = $this->order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}

	function get_datatables($id_pegawai)
	{
		$this->_get_datatables_query($id_pegawai);
		if($_POST['length'] != -1)
		$this->db->limit($_POST['length'], $_POST['start']);
		$query = $this->db->get();
		return $query->result();
	}

	function count_filtered($id_pegawai)
	{
		$this->_get_datatables_query($id_pegawai);
		$query = $this->db->get();
		return $query->num_rows();
	}

	public function count_all($id_pegawai)
	{
		$this->db->from('vamr4846_vama.useraccess_pusat AS up');
		$this->db->join('vamr4846_vama.menu_pusat AS mp', 'mp.id_menu=up.id_menu');
		$this->db->where(array(
			'up.id_pegawai' => $id_pegawai,
			'mp.id_menu <>' => '1',
			'mp.url <>'     => 'sub_menu'
		));
		return $this->db->count_all_results();
	}

	public function save($data)
	{
		$this->db->insert($this->table, $data);
		return $this->db->insert_id();
	}

	public function update($where, $data)
	{
		$this->db->update($this->table, $data, $where);
		return $this->db->affected_rows();
	}

	public function delete_by_id($id_useraccess)
	{
		$this->db->where('id_useraccess', $id_useraccess);
		$this->db->delete($this->table);
	}

	// ----------------------------------------------------------------------------------------------------------------------------
	// Awal menu dan useraccess
	public function cek_menu_induk($id_menu)
	{
		$this->db->select('mp.id_menu, mp.nama_menu, mp.id_menu_induk');
		$this->db->from('vamr4846_vama.menu_pusat AS mp');
		$this->db->where('id_menu', $id_menu);
		$query = $this->db->get();
		return $query->row();
	}

	public function cek_jumlah_useraccess($id_pegawai, $id_menu)
	{
		$this->db->select('up.id_pegawai, mp.id_menu, id_menu_induk, nama_menu, SUM(up.act_read)AS jumlah_access');
		$this->db->from('vamr4846_vama.menu_pusat AS mp');
		$this->db->join('vamr4846_vama.useraccess_pusat AS up', 'up.id_menu=mp.id_menu');
		$this->db->where(array(
			'up.id_pegawai'    => $id_pegawai,
			'mp.id_menu_induk' => $id_menu
		));
		$query = $this->db->get();
		return $query->row();
	}

	public function cek_access($id_pegawai, $id_menu){
		$this->db->select('id_pegawai, id_menu, act_read, act_create, act_update, act_delete');
		$this->db->from('vamr4846_vama.useraccess_pusat');
		$this->db->where(array(
			'id_pegawai' => $id_pegawai,
			'id_menu'    => $id_menu
		));
		$query=$this->db->get();
		return $query->row();
	}

	public function get_induk_menu($id_pegawai)
	{
		$sql = "SELECT
					IFNULL(up.id_pegawai,'-') AS id_pegawai, 
					IFNULL(mp.id_menu,'-') AS id_menu, IFNULL(mp.id_menu_induk,'-') AS id_menu_induk, 
					mp.nama_menu, mp.url, mp.icon
				FROM 
					vamr4846_vama.useraccess_pusat AS up
				LEFT JOIN
					vamr4846_vama.menu_pusat As mp ON mp.id_menu=up.id_menu 
				WHERE 
					id_pegawai 		 = '".$id_pegawai."' AND 
					mp.id_menu    	!= '1' AND
					id_menu_induk 	 = '0' AND 
					act_read 		 = '1'
				";

		return $this->db->query($sql);
	}

	public function get_sub_menu($id_pegawai, $id_menu_induk)
	{
		$sql = "SELECT
					IFNULL(up.id_pegawai,'-') AS id_pegawai, 
					IFNULL(mp.id_menu,'-') AS id_menu, IFNULL(mp.id_menu_induk,'-') AS id_menu_induk, 
					mp.nama_menu, mp.url, mp.icon
				FROM 
					vamr4846_vama.useraccess_pusat AS up
				LEFT JOIN
					vamr4846_vama.menu_pusat As mp ON mp.id_menu=up.id_menu 
				WHERE 
					id_pegawai 		 = '".$id_pegawai."' AND 
					act_read 		 = '1' AND
					id_menu_induk 	 = '".$id_menu_induk."'
				";

		return $this->db->query($sql);
	}

	public function get_anak_menu($id_pegawai, $id_menu_induk)
	{
		$sql = "SELECT
					IFNULL(up.id_pegawai,'-') AS id_pegawai, 
					IFNULL(mp.id_menu,'-') AS id_menu, IFNULL(mp.id_menu_induk,'-') AS id_menu_induk, 
					mp.nama_menu, mp.url, mp.icon
				FROM 
					vamr4846_vama.useraccess_pusat AS up
				LEFT JOIN
					vamr4846_vama.menu_pusat As mp ON mp.id_menu=up.id_menu 
				WHERE 
					id_pegawai 		 = '".$id_pegawai."' AND 
					act_read 		 = '1' AND
					id_menu_induk 	 = '".$id_menu_induk."'
				";

		return $this->db->query($sql);
	}
	// Awal menu dan useraccess
	// ----------------------------------------------------------------------------------------------------------------------------
}

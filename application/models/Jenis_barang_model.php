<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Jenis_barang_model extends MY_Model {

	var $table         = 'vamr4846_vama.jenis_barang';
	var $column_order  = array(
		'jb.id_jenis_barang','jb.id_jenis_barang','jb.kode_jenis','jb.nama_jenis',
		'pp1.nama_pegawai','jb.tanggal_pembuatan','pp2.nama_pegawai','jb.tanggal_pembaharuan');
	var $column_search = array('jb.kode_jenis','nama_jenis');
	var $order         = array('jb.id_jenis_barang' => 'desc');

	private function _get_datatables_query()
	{
		$this->db->select('jb.*, pp1.nama_pegawai AS pegawai_save, pp2.nama_pegawai As pegawai_edit');
		$this->db->from('vamr4846_vama.jenis_barang AS jb');
		$this->db->join('vamr4846_vama.pegawai_pusat AS pp1', 'pp1.id_pegawai=jb.id_pegawai_pembuatan', 'LEFT');
		$this->db->join('vamr4846_vama.pegawai_pusat AS pp2', 'pp2.id_pegawai=jb.id_pegawai_pembaharuan', 'LEFT');
		$this->db->where('jb.status_hapus', 'TIDAK');

		$i = 0;
		foreach ($this->column_search as $item){
			if($_POST['search']['value']){
				if($i===0){
					$this->db->group_start();
					$this->db->like($item, $_POST['search']['value']);
				}else{
					$this->db->or_like($item, $_POST['search']['value']);
				}

				if(count($this->column_search) - 1 == $i)
					$this->db->group_end();
			}
			$i++;
		}
		
		if(isset($_POST['order'])){
			$this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		}else if(isset($this->order)){
			$order = $this->order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}

	function get_datatables()
	{
		$this->_get_datatables_query();
		if($_POST['length'] != -1)
		$this->db->limit($_POST['length'], $_POST['start']);
		$query = $this->db->get();
		return $query->result();
	}

	function count_filtered()
	{
		$this->_get_datatables_query();
		$query = $this->db->get();
		return $query->num_rows();
	}

	public function count_all()
	{
		$this->db->from($this->table);
		$this->db->where('status_hapus', 'TIDAK');
		return $this->db->count_all_results();
	}

	public function ambil_jenis_barang($keyword)
	{
		$sql = "
			SELECT kode_jenis, nama_jenis
			FROM 
				vamr4846_vama.jenis_barang 
			WHERE 
				( 
					nama_jenis LIKE '%".$this->db->escape_like_str($keyword)."%' AND 
					status_hapus = 'TIDAK'
				) 
		";

		return $this->db->query($sql);
	}

	public function cari_jenis_barang($nama_jenis) {
		$this->db->select('kode_jenis, nama_jenis');
		$this->db->from('vamr4846_vama.jenis_barang');
		$this->db->like('nama_jenis', $nama_jenis);
		$this->db->order_by('nama_jenis','ASC');
		$query=$this->db->get();
		return $query->result();
	}

	// Listing data
	public function listing() {
		$this->db->select('*');
		$this->db->from('vamr4846_vama.jenis_barang');
		$this->db->order_by('id_jenis_barang','DESC');
		$query=$this->db->get();
		return $query->result();
	}

	// Dapatkan kode terakhir
	public function akhir() {
		$this->db->select('*');
		$this->db->from('vamr4846_vama.jenis_barang');
		$this->db->order_by('id_jenis_barang','DESC');
		$query=$this->db->get();
		return $query->row();
	}

	public function get_by_id($id_jenis_barang)
	{
		$this->db->from($this->table);
		$this->db->where('id_jenis_barang',$id_jenis_barang);
		$query = $this->db->get();

		return $query->row();
	}

	public function get_by_nama($nama_jenis_barang)
	{
		$this->db->select('id_jenis_barang, nama_jenis');
		$this->db->from($this->table);
		$this->db->where('nama_jenis', $nama_jenis_barang);
		
		$query = $this->db->get();
		return $query->row();
	}

	public function save($data)
	{
		$this->db->insert($this->table, $data);
		return $this->db->insert_id();
	}

	public function update($where, $data)
	{
		$this->db->update($this->table, $data, $where);
		return $this->db->affected_rows();
	}

	public function delete_by_id($id_jenis_barang)
	{
		$this->db->where('id_jenis_barang', $id_jenis_barang);
		$this->db->delete($this->table);
	}

	public function update_status_hapus($id_jenis_barang, $id_pegawai)
	{
		$sql = "UPDATE vamr4846_vama.jenis_barang 
				SET status_hapus = 'IYA', id_pegawai_pembaharuan = '".$id_pegawai."' 
				WHERE id_jenis_barang = '".$id_jenis_barang."'";
		return $this->db->query($sql);
	}
}

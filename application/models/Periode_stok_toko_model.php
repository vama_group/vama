<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Periode_stok_toko_model extends MY_Model {

	var $table = 'vamr4846_toko_mrc.periode_stok_toko';

	var $column_order = array(
		'kode_periode_stok_toko','tanggal_awal_periode','tanggal_akhir_periode','operator_pembuatan','operator_pembaharuan'
	); 
	
	var $column_search = array(
		'kode_periode_stok_toko','tanggal_awal_periode','tanggal_akhir_periode','operator_pembuatan','operator_pembaharuan'
	); 
	
	var $order = array('id_periode_stok_toko' => 'desc');

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	private function _get_datatables_query()
	{
		$this->db->select('pst.*, cp.nama_customer_pusat AS nama_toko, pp.nama_pegawai AS pegawai_save');
		$this->db->from('vamr4846_toko_mrc.periode_stok_toko AS pst');
		$this->db->join('vamr4846_vama.customer_pusat AS cp', 'cp.id_customer_pusat=pst.id_toko');
		$this->db->join('vamr4846_vama.pegawai_pusat AS pp', 'pp.id_pegawai=pst.id_pegawai_pembuatan', 'LEFT');

		$i = 0;
	
		foreach ($this->column_search as $item){
			if($_POST['search']['value']){
				if($i===0){
					$this->db->group_start();
					$this->db->like($item, $_POST['search']['value']);
				}else{
					$this->db->or_like($item, $_POST['search']['value']);
				}

				if(count($this->column_search) - 1 == $i)
					$this->db->group_end();
			}
			$i++;
		}
		
		if(isset($_POST['order'])){
			$this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		}else if(isset($this->order)){
			$order = $this->order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}

	function get_datatables()
	{
		$this->_get_datatables_query();
		if($_POST['length'] != -1)
		$this->db->limit($_POST['length'], $_POST['start']);
		$query = $this->db->get();
		return $query->result();
	}

	function count_filtered()
	{
		$this->_get_datatables_query();
		$query = $this->db->get();
		return $query->num_rows();
	}

	public function count_all()
	{
		$this->db->from($this->table);
		return $this->db->count_all_results();
	}

	// Dapatkan kode terakhir
	public function akhir($id_toko){
		$this->db->select('*');
		$this->db->from('vamr4846_toko_mrc.periode_stok_toko AS pst');
		$this->db->where('pst.id_toko', $id_toko);
		$this->db->order_by('pst.id_periode_stok_toko','DESC');
		$query=$this->db->get();
		return $query->row();
	}

	public function get_by_id($id_periode_stok_toko)
	{
		$this->db->select('pst.*, cp.nama_customer_pusat');
		$this->db->from('vamr4846_toko_mrc.periode_stok_toko AS pst');
		$this->db->join('vamr4846_vama.customer_pusat AS cp','cp.id_customer_pusat=pst.id_toko');
		$this->db->where('id_periode_stok_toko',$id_periode_stok_toko);
		$query = $this->db->get();
		return $query->row();
	}

	public function get_by_id_toko($id_toko)
	{
		$this->db->select('pst.*, cp.nama_customer_pusat');
		$this->db->from('vamr4846_toko_mrc.periode_stok_toko AS pst');
		$this->db->join('vamr4846_vama.customer_pusat AS cp', 'cp.id_customer_pusat=pst.id_toko');
		$this->db->where('pst.id_toko', $id_toko);
		$this->db->order_by('id_periode_stok_toko', 'DESC');
		$query = $this->db->get();
		return $query;
		// return $query->result();
	}

	public function save($data)
	{
		$this->db->insert($this->table, $data);
		return $this->db->insert_id();
	}

	public function update($where, $data)
	{
		$this->db->update($this->table, $data, $where);
		return $this->db->affected_rows();
	}

	public function delete_by_id($id_periode_stok_toko)
	{
		$this->db->where('id_periode_stok_toko', $id_periode_stok_toko);
		$this->db->delete($this->table);
	}
}

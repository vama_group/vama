<?php
class Penjualan_detail_model extends CI_Model
{
	// Penjualan induk
	var $column_order          = array('', '', 'bp.sku','bp.nama_barang', 'pd.jenis_harga', 'pd.harga_satuan', 'pd.discount', 
									   'pd.jumlah_beli', 'pd.subtotal');
	var $column_search         = array('bp.kode_barang','bp.sku','bp.nama_barang'); 
	var $order                 = array('pd.tanggal_pembaharuan' => 'desc'); 
	
	// Penjualan batal
	var $column_order_batal    = array('', '', 'bp.sku','bp.nama_barang', 'pdb.jumlah_beli', 'pdb.subtotal');
	var $column_search_batal   = array('bp.kode_barang','bp.sku','bp.nama_barang', 'pdb.jumlah_beli'); 
	var $order_batal           = array('pdb.tanggal_pembatalan' => 'desc');
	
	// Cari barang pusat
	var $column_order_barang1  = array('', '', 'bp.sku','bp.nama_barang','bp.total_stok');
	var $column_search_barang1 = array('bp.kode_barang','bp.sku','bp.nama_barang'); 
	var $order_barang1         = array('bp.nama_barang' => 'desc');
	
	// Cari barang toko
	var $column_order_barang2  = array('', '', 'bp.sku','bp.nama_barang','bt.total_stok');
	var $column_search_barang2 = array('bp.kode_barang','bp.sku','bp.nama_barang'); 
	var $order_barang2         = array('bp.nama_barang' => 'desc');

	private function _get_datatables_query($id_penjualan_m)
	{		
		$this->db->select('	pd.id_penjualan_d, pd.jenis_harga, pd.harga_satuan, 
							(pd.discount * pd.harga_satuan) AS discount, pd.jumlah_beli, pd.jumlah_masuk, pd.subtotal,
							bp.sku, bp.kode_barang, bp.nama_barang,
							pp1.nama_pegawai AS pegawai_save, IFNULL(pp2.nama_pegawai, "-") AS pegawai_edit, 
							pd.tanggal_pembuatan, pd.tanggal_pembaharuan, 
							IFNULL(rpm.id_retur_penjualan_m, "-") AS id_retur_penjualan_m, 
							IFNULL(rpd.jumlah_retur, "0") AS jumlah_retur,
						 ');
		$this->db->from('vamr4846_vama.penjualan_detail AS pd');
		$this->db->join('vamr4846_vama.penjualan_master as pm', 'pd.id_penjualan_m=pm.id_penjualan_m', 'LEFT');
		$this->db->join('vamr4846_vama.barang_pusat as bp','bp.id_barang_pusat=pd.id_barang_pusat','LEFT');
		$this->db->join('vamr4846_vama.retur_penjualan_master as rpm','rpm.id_penjualan_m=pm.id_penjualan_m', 'LEFT');
		$this->db->join('vamr4846_vama.retur_penjualan_detail as rpd','rpd.id_retur_penjualan_m=rpm.id_retur_penjualan_m AND 
														 rpd.id_barang_pusat=pd.id_barang_pusat', 'LEFT');
		$this->db->join('vamr4846_vama.pegawai_pusat as pp1','pp1.id_pegawai=pd.id_pegawai_pembuatan','LEFT');
		$this->db->join('vamr4846_vama.pegawai_pusat as pp2','pp2.id_pegawai=pd.id_pegawai_pembaharuan','LEFT');
		$this->db->where('pd.id_penjualan_m', $id_penjualan_m);

		$i = 0;
		foreach ($this->column_search as $item){
			if($_POST['search']['value']){
				if($i===0){
					$this->db->group_start();
					$this->db->like($item, $_POST['search']['value']);
				}else{
					$this->db->or_like($item, $_POST['search']['value']);
				}

				if(count($this->column_search) - 1 == $i)
					$this->db->group_end();
			}
			$i++;
		}
		
		if(isset($_POST['order'])){
			$this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		}else if(isset($this->order)){
			$order = $this->order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}

	private function _get_datatables_query_batal($id_penjualan_m)
	{		
		$this->db->select('	pdb.id_penjualan_b, pdb.id_penjualan_d, pdb.id_barang_pusat, pdb.jenis_harga, pdb.harga_satuan, 
							(pdb.discount * pdb.harga_satuan) AS discount, pdb.jumlah_beli, pdb.subtotal,
							bp.sku, bp.kode_barang, bp.nama_barang, keterangan_batal, pp.nama_pegawai, pdb.tanggal_pembatalan
						 ');
		$this->db->from('vamr4846_vama.penjualan_detail_batal AS pdb');
		$this->db->join('vamr4846_vama.barang_pusat as bp','bp.id_barang_pusat=pdb.id_barang_pusat','LEFT');
		$this->db->join('vamr4846_vama.pegawai_pusat as pp','pp.id_pegawai=pdb.id_pegawai_pembatalan','LEFT');
		$this->db->where('pdb.id_penjualan_m', $id_penjualan_m);

		$i = 0;
		foreach ($this->column_search_batal as $item){
			if($_POST['search']['value']){
				if($i===0){
					$this->db->group_start();
					$this->db->like($item, $_POST['search']['value']);
				}else{
					$this->db->or_like($item, $_POST['search']['value']);
				}

				if(count($this->column_search_batal) - 1 == $i)
					$this->db->group_end();
			}
			$i++;
		}
		
		if(isset($_POST['order'])){
			$this->db->order_by($this->column_order_batal[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		}else if(isset($this->order_batal)){
			$order = $this->order_batal;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}

	private function _get_datatables_query_barang($id_penjualan_m)
	{		
		$ambil_penjualan	= $this->db->query("SELECT id_barang_pusat 
												FROM vamr4846_vama.penjualan_detail 
												WHERE id_penjualan_m =".$id_penjualan_m."");
  		$pd_id_barang 		= array();
		foreach ($ambil_penjualan->result() as $row){
			$pd_id_barang[] = $row->id_barang_pusat; 
		}
		$id_barang 			= implode(",", $pd_id_barang);
		$idp 				= explode(",", $id_barang);
		
		$this->db->select('
			bp.id_barang_pusat, bp.sku, bp.kode_barang, bp.nama_barang, bp.total_stok AS total_stok_pusat,
			bp.harga_eceran AS harga_eceran_pusat, bp.harga_grosir1, bp.harga_grosir2
		');
		$this->db->from('vamr4846_vama.barang_pusat as bp');
		$this->db->where_not_in('bp.id_barang_pusat', $idp);
		$this->db->where('bp.total_stok >', 0);
		$this->db->where('bp.status', 'AKTIF');

		$i = 0;
	
		foreach ($this->column_search_barang1 as $item){
			if($_POST['search']['value']){
				if($i===0){
					$this->db->group_start();
					$this->db->like($item, $_POST['search']['value']);
				}else{
					$this->db->or_like($item, $_POST['search']['value']);
				}

				if(count($this->column_search_barang1) - 1 == $i)
				$this->db->group_end();
			}
			$i++;
		}
		
		if(isset($_POST['order'])){
			$this->db->order_by($this->column_order_barang1[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		}else if(isset($this->order_barang1)){
			$order = $this->order_barang1;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}

	private function _get_datatables_query_barang_toko($id_penjualan_m, $kode_customer)
	{		
		$ambil_penjualan	= $this->db->query("SELECT id_barang_pusat 
												FROM vamr4846_vama.penjualan_detail 
												WHERE id_penjualan_m =".$id_penjualan_m."");
  		$pd_id_barang 		= array();
		foreach ($ambil_penjualan->result() as $row){
			$pd_id_barang[] = $row->id_barang_pusat; 
		}
		$id_barang 			= implode(",", $pd_id_barang);
		$idp 				= explode(",", $id_barang);
		
		$this->db->select('bp.id_barang_pusat, bt.id_barang_pusat, bp.sku, bp.kode_barang, bp.nama_barang, 
						   bt.total_stok AS total_stok_toko, bp.total_stok AS total_stok_pusat, 
						   bt.harga_eceran As harga_eceran_toko, bp.harga_eceran AS harga_eceran_pusat');
		$this->db->from('vamr4846_vama.barang_pusat as bp');
		$this->db->join('(SELECT id_barang_pusat, total_stok, harga_eceran, kode_toko
						 FROM vamr4846_toko_mrc.barang_toko_'.$kode_customer.') AS bt', 
						'bt.id_barang_pusat=bp.id_barang_pusat AND bt.kode_toko="'.$kode_customer.'"', 'LEFT');
		$this->db->where('bp.total_stok >', '0');
		$this->db->where_not_in('bp.id_barang_pusat', $idp);

		$i = 0;
		foreach ($this->column_search_barang2 as $item){
			if($_POST['search']['value']){
				if($i === 0){
					$this->db->group_start();
					$this->db->like($item, $_POST['search']['value']);
				}else{
					$this->db->or_like($item, $_POST['search']['value']);
				}

				if(count($this->column_search_barang2) - 1 == $i)
				$this->db->group_end();
			}
			$i++;
		}
		
		if(isset($_POST['order'])){
			$this->db->order_by($this->column_order_barang2[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		}else if(isset($this->order_barang2)){
			$order = $this->order_barang2;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}

	function get_datatables($perintah, $id_penjualan_m, $kode_customer)
	{
		$this->$perintah($id_penjualan_m, $kode_customer);
		if($_POST['length'] != -1)
		$this->db->limit($_POST['length'], $_POST['start']);
		$query = $this->db->get();
		return $query->result();
	}

	function count_filtered($perintah, $id_penjualan_m, $kode_customer)
	{
		$this->$perintah($id_penjualan_m, $kode_customer);
		$query = $this->db->get();
		return $query->num_rows();
	}

	public function count_all($table, $id_penjualan_m, $kode_customer)
	{
		$this->db->from($table);
		if($table == 'vamr4846_vama.penjualan_detail' or $table == 'vamr4846_vama.penjualan_detail_batal'){
			$this->db->where('id_penjualan_m',$id_penjualan_m);
		}else if($table == 'vamr4846_toko_mrc.barang_toko_'.$kode_customer.''){
			$this->db->where('kode_toko', $kode_customer);
		}
		return $this->db->count_all_results();
	}

	function get_penjualan_detail($id_penjualan_d)
	{
		$this->db->select('pd.*, pm.no_penjualan, pm.id_customer_pusat, pm.tipe_customer_pusat, 
						   pm.status_penjualan, bp.sku, bp.nama_barang');
		$this->db->from('vamr4846_vama.penjualan_detail as pd');
		$this->db->join('vamr4846_vama.penjualan_master as pm','pm.id_penjualan_m=pd.id_penjualan_m','LEFT');
		$this->db->join('vamr4846_vama.barang_pusat as bp','bp.id_barang_pusat=pd.id_barang_pusat','LEFT');
		$this->db->where('id_penjualan_d', $id_penjualan_d);
		$query = $this->db->get();
		return $query->row();
	}

	function get_penjualan_detail_all($id_penjualan_m)
	{
		$this->db->select('pd.*, pm.no_penjualan, pm.status_penjualan');
		$this->db->from('vamr4846_vama.penjualan_detail AS pd');
		$this->db->join('vamr4846_vama.penjualan_master as pm','pm.id_penjualan_m=pd.id_penjualan_m','LEFT');
		$this->db->join('vamr4846_vama.barang_pusat as bp','bp.id_barang_pusat=pd.id_barang_pusat','LEFT');
		$this->db->where('pd.id_penjualan_m', $id_penjualan_m);
		$query = $this->db->get();
		return $query->result();
	}

	function get_penjualan_batal($id_penjualan_b)
	{
		$this->db->select('pdb.*, bp.nama_barang, bp.sku');
		$this->db->from('vamr4846_vama.penjualan_detail_batal AS pdb');
		$this->db->join('vamr4846_vama.barang_pusat AS bp', 'pdb.id_barang_pusat=bp.id_barang_pusat');
		$this->db->where('id_penjualan_b', $id_penjualan_b);
		$query = $this->db->get();
		return $query->row();
	}

	function get_id($id_penjualan_m, $id_barang_pusat)
	{
		$this->db->select('*');
		$this->db->from('vamr4846_vama.penjualan_detail');
		$this->db->where(array(  
			'id_penjualan_m'  => $id_penjualan_m,
			'id_barang_pusat' => $id_barang_pusat
		));
		$query = $this->db->get();
		return $query->row();
	}	

	function insert_detail($id_penjualan_m, $id_barang_pusat, $jumlah_beli, $modal, $jenis_harga, $harga_satuan, $discount, $subtotal, $id_pegawai)
	{ 
		$dt = array(	
			'id_penjualan_m'       => $id_penjualan_m,
			'id_barang_pusat'      => $id_barang_pusat,
			'jumlah_beli'          => $jumlah_beli,
			'modal'                => $modal,
			'jenis_harga'          => $jenis_harga,
			'harga_satuan'         => $harga_satuan,
			'discount'             => $discount,
			'subtotal'             => $subtotal,
			'id_pegawai_pembuatan' => $id_pegawai,
			'tanggal_pembuatan'    => date('Y-m-d H:i:s') 		
		);
		return $this->db->insert('vamr4846_vama.penjualan_detail', $dt);
	}

	function update_detail($id_penjualan_m, $id_barang_pusat, $jumlah_beli, $modal, $jenis_harga, $harga_satuan, $discount, $subtotal, $id_pegawai, $jumlah_beli_lama, $tipe_customer_pusat, $kode_toko)
	{
		$sql = "UPDATE vamr4846_vama.barang_pusat SET total_stok = total_stok + ".$jumlah_beli_lama." 
				WHERE id_barang_pusat = '".$id_barang_pusat."'";
		$this->db->query($sql);
		
		$dt = array(	
			'jumlah_beli'            => $jumlah_beli,
			'modal'                  => $modal,
			'jenis_harga'            => $jenis_harga,
			'harga_satuan'           => $harga_satuan,
			'discount'               => $discount,
			'subtotal'               => $subtotal,
			'id_pegawai_pembaharuan' => $id_pegawai
		);
		
		$where = array(	
			'id_penjualan_m'  => $id_penjualan_m,
			'id_barang_pusat' => $id_barang_pusat
		);

		return $this->db->update('vamr4846_vama.penjualan_detail', $dt, $where);
	}

	function get_detail_faktur($id_penjualan_m)
	{
		$this->db->select('	
			pd.id_penjualan_d, pd.harga_satuan, pd.discount, 
			(harga_satuan*discount) AS discount_harga, 
			harga_satuan-(harga_satuan*discount) AS harga_bersih,
			pd.jumlah_beli, pd.subtotal,
			bp.sku, bp.kode_barang, bp.nama_barang
		');
		$this->db->from('vamr4846_vama.penjualan_detail AS pd');
		$this->db->join('vamr4846_vama.barang_pusat as bp','bp.id_barang_pusat=pd.id_barang_pusat');
		$this->db->where('pd.id_penjualan_m',$id_penjualan_m);
		$this->db->order_by('bp.nama_barang','ASC');
		$query = $this->db->get();
		return $query->result();
	}

	function get_total($id_penjualan_m)
	{
		$this->db->select('	
			id_penjualan_m,
			IFNULL(COUNT(id_penjualan_d),0) jumlah_barang,
			IFNULL(ROUND(SUM(((harga_satuan-(harga_satuan*discount))*jumlah_beli)),0),0) total
		');
		$this->db->from('vamr4846_vama.penjualan_detail');
		$this->db->where('id_penjualan_m', $id_penjualan_m);
		$query = $this->db->get();
		return $query->row();
	}

	function hapus_penjualan_detail($id_penjualan_d, $id_penjualan_m, $no_penjualan, $id_customer_pusat, $tipe_customer_pusat, 
									$id_barang_pusat, $modal, $jenis_harga, $harga_satuan, $discount, $jumlah_beli, $subtotal,
									$id_pegawai_pembuatan, $id_pegawai_pembaharuan, $id_pegawai, $tanggal_pembuatan, $tanggal_pembaharuan, 
									$keterangan_batal, $kode_toko, $status_penjualan)
	{
		// Kembalikan stok berdasarkan jumlah beli
		$sql = "UPDATE vamr4846_vama.barang_pusat SET total_stok = total_stok + ".$jumlah_beli." 
				WHERE id_barang_pusat = '".$id_barang_pusat."' ";
		$this->db->query($sql);

		// Kurangi stok toko, jika tipe customer "MRC"
		if($tipe_customer_pusat ==='MRC' AND $status_penjualan == 'SELESAI'){
			$sql = "UPDATE vamr4846_toko_mrc.barang_toko_".$kode_toko." SET total_stok = total_stok - ".$jumlah_beli." 
					WHERE id_barang_pusat = '".$id_barang_pusat."' AND kode_toko = '".$kode_toko."' ";
			$this->db->query($sql);			
		}

		// Simpan penjualan detail ke penjualan detail batal
		$sql = "INSERT INTO vamr4846_vama.penjualan_detail_batal(
					id_penjualan_d, id_penjualan_m, no_penjualan, id_barang_pusat, id_customer_pusat, tipe_customer_pusat, 
					modal, jenis_harga, harga_satuan, discount, jumlah_beli, subtotal, 
					id_pegawai_pembuatan, id_pegawai_pembaharuan, id_pegawai_pembatalan, tanggal_pembuatan, 
				 	tanggal_pembaharuan, keterangan_batal) 
				VALUES(	'".$id_penjualan_d."', '".$id_penjualan_m."', '".$no_penjualan."', '".$id_barang_pusat."', 
						'".$id_customer_pusat."', '".$tipe_customer_pusat."', 
				 		'".$modal."', '".$jenis_harga."', '".$harga_satuan."', '".$discount."', '".$jumlah_beli."', '".$subtotal."', 
				 		'".$id_pegawai_pembuatan."', '".$id_pegawai_pembaharuan."', '".$id_pegawai."', '".$tanggal_pembuatan."', 
				 		'".$tanggal_pembaharuan."', '".$keterangan_batal."')";
		$this->db->query($sql);

		// Hapus penjualan detail
		return $this->db
					->where('id_penjualan_d', $id_penjualan_d)
					->delete('vamr4846_vama.penjualan_detail');
	}
}
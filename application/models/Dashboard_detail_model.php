<?php
class Dashboard_detail_model extends CI_Model
{
	// Penjualan perhari pusat
	var $column_order_ppp = array(
		'pm.tanggal', 'pm.tanggal', 'jml_transaksi', 'tunai', 'debit', 'total', 
		'total_retur', 'total_bersih', 'total_modal', 'total_keuntungan'
	);
	var $column_search_ppp = array('pm.tanggal'); 
	var $order_ppp         = array('pm.tanggal' => 'desc');
	//--------------------------------------------------------------------------------------------------------------------------------------
	
	// Top customer pusat
	var $column_order_tcp = array(
		'pd.id_penjualan_d', 'cp.kode_customer_pusat', 'cp.nama_customer_pusat', 'pm.tanggal',
		'jml_transaksi', 'total_jual', 'biaya_lain', 'ppn', 'total_bersih', 'total_retur', 'total_fix'
	);
	var $column_search_tcp = array('cp.kode_customer_pusat', 'cp.nama_customer_pusat', 'cp.email_customer_pusat'); 
	var $order_tcp         = array('total_fix' => 'desc');
	//--------------------------------------------------------------------------------------------------------------------------------------

	// Top produk pusat
	var $column_order_tpp = array(
		'pd.id_barang_pusat', 'bp.sku', 'bp.nama_barang', 'pd.harga_satuan',
		'pm.tanggal', 'jml_transaksi', 'jml_qty', 
		'total_jual', 'total_disc', 'total_bersih'
	);
	var $column_search_tpp = array('bp.sku', 'bp.nama_barang'); 
	var $order_tpp         = array('total_bersih' => 'desc');
	//--------------------------------------------------------------------------------------------------------------------------------------

	// Top produk per customer
	var $column_order_tpc = array(
		'pd.id_barang_pusat', 'cp.nama_customer_pusat', 'bp.sku', 'bp.nama_barang', 'pd.harga_satuan',
		'pm.tanggal', 'jml_transaksi', 'jml_qty', 
		'total_jual', 'total_disc', 'total_bersih'
	);
	var $column_search_tpc = array('cp.nama_customer_pusat', 'bp.sku', 'bp.nama_barang'); 
	var $order_tpc         = array('cp.nama_customer_pusat' => 'desc');
	//--------------------------------------------------------------------------------------------------------------------------------------

	// Top customer toko
	var $column_order_tct = array(
		'pd.id_penjualan_d', 'ct.kode_customer_toko', 'ct.nama_customer_toko', 'pm.tanggal',
		'jml_transaksi', 'total_jual', 'biaya_lain', 'ppn', 'total_bersih', 'total_retur', 'total_fix'
	);
	var $column_search_tct = array('ct.kode_customer_toko', 'ct.nama_customer_toko', 'ct.email'); 
	var $order_tct         = array('total_fix' => 'desc');
	//--------------------------------------------------------------------------------------------------------------------------------------

	// Awal pusat
	private function _get_laporan_query_penjualan_perhari_pusat($tanggal_awal, $tanggal_akhir, $jenis_perintah)
	{		
		$this->db->select('	
			pm.tanggal, COUNT(pm.id_penjualan_m) AS jml_transaksi, 
			SUM(pm.grand_total) AS total, SUM(pm.debit) AS debit, SUM(pm.tunai) AS tunai,
			SUM(rpm.total_retur) AS total_retur,
			SUM((pm.grand_total - IFNULL(rpm.total_retur, 0))) AS total_bersih,
			IFNULL(SUM(pd.sub_modal), 0) AS total_modal, 
			SUM((pm.grand_total - IFNULL(rpm.total_retur, 0))) - IFNULL(SUM(pd.sub_modal), 0) As total_keuntungan
		');
		$this->db->from('vamr4846_vama.penjualan_master AS pm');
		$this->db->join('(SELECT id_penjualan_m, SUM(modal*jumlah_beli) as sub_modal 
						  FROM vamr4846_vama.penjualan_detail
						  GROUP BY id_penjualan_m
						) AS pd', 'pd.id_penjualan_m=pm.id_penjualan_m', 'LEFT');
		$this->db->join('vamr4846_vama.customer_pusat as cp','cp.id_customer_pusat=pm.id_customer_pusat', 'LEFT');
		$this->db->join('vamr4846_vama.retur_penjualan_master as rpm',
						'rpm.id_penjualan_m=pm.id_penjualan_m AND rpm.status_retur="SELESAI"', 'LEFT');
		$this->db->where('DATE(pm.tanggal) BETWEEN "'. date('Y-m-d', strtotime($tanggal_awal)). '" AND 
												   "'. date('Y-m-d', strtotime($tanggal_akhir)).'" AND 
						  pm.tipe_customer_pusat '.$jenis_perintah.' "MRC" AND
						  pm.status_penjualan = "SELESAI"
						');
		$this->db->group_by('pm.tanggal');


		$i = 0;
		foreach ($this->column_search_ppp as $item){
			if($_POST['search']['value']){
				if($i===0){
					$this->db->group_start();
					$this->db->like($item, $_POST['search']['value']);
				}else{
					$this->db->or_like($item, $_POST['search']['value']);
				}

				if(count($this->column_search_ppp) - 1 == $i)
					$this->db->group_end();
			}
			$i++;
		}

		if(isset($_POST['order'])){
			$this->db->order_by($this->column_order_ppp[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		}else if(isset($this->order_ppp)){
			$order = $this->order_ppp;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}

	private function _get_laporan_query_top_customer_pusat($tanggal_awal, $tanggal_akhir, $jenis_perintah)
	{		
		$this->db->select('  
			IFNULL(cp.kode_customer_pusat,"-") AS kode_customer,
			IFNULL(cp.nama_customer_pusat,"-") AS nama_customer,  cp.foto, 
			MIN(tanggal) AS tanggal_awal, MAX(tanggal) AS tanggal_akhir,
			count(pm.id_penjualan_m) AS jml_transaksi,
			IFNULL(SUM(pm.grand_total),0) AS total_jual,
			IFNULL(SUM(rpm.total_retur),0) AS total_retur, 
			IFNULL(SUM(pm.biaya_lain),0) AS biaya_lain, 
			IFNULL(SUM(pm.ppn),0) AS ppn, 
			IFNULL(
				IFNULL(SUM(pm.grand_total),0) -
				IFNULL(SUM(pm.biaya_lain),0) -
				IFNULL(SUM(pm.ppn),0)
			,0) AS total_bersih,
			IFNULL(
				IFNULL(SUM(pm.grand_total),0) -
				IFNULL(SUM(pm.biaya_lain),0) -
				IFNULL(SUM(pm.ppn),0)
			,0) - 
			IFNULL(SUM(rpm.total_retur),0) AS total_fix
		');
		$this->db->from('vamr4846_vama.penjualan_master as pm');
		$this->db->join('vamr4846_vama.customer_pusat as cp','cp.id_customer_pusat=pm.id_customer_pusat', 'LEFT');
		$this->db->join('vamr4846_vama.retur_penjualan_master as rpm',
						'rpm.id_penjualan_m=pm.id_penjualan_m AND rpm.status_retur="SELESAI"', 'LEFT');
		$this->db->where('DATE(pm.tanggal) BETWEEN "'. date('Y-m-d', strtotime($tanggal_awal)). '" AND 
												   "'. date('Y-m-d', strtotime($tanggal_akhir)).'" AND 
						  pm.tipe_customer_pusat '.$jenis_perintah.' "MRC" AND
						  pm.status_penjualan = "SELESAI"
						');
		$this->db->group_by('cp.kode_customer_pusat');

		$i = 0;
		foreach ($this->column_search_tcp as $item){
			if($_POST['search']['value']){
				if($i===0){
					$this->db->group_start();
					$this->db->like($item, $_POST['search']['value']);
				}else{
					$this->db->or_like($item, $_POST['search']['value']);
				}

				if(count($this->column_search_tcp) - 1 == $i)
					$this->db->group_end();
			}
			$i++;
		}

		if(isset($_POST['order'])){
			$this->db->order_by($this->column_order_tcp[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		}else if(isset($this->order_tcp)){
			$order = $this->order_tcp;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}

	private function _get_laporan_query_top_barang_pusat($tanggal_awal, $tanggal_akhir, $jenis_perintah)
	{		
		$this->db->select('	
			bp.sku, bp.nama_barang, 
			pd.harga_satuan, SUM(pd.harga_satuan * pd.jumlah_beli) AS total_jual, 
			SUM((pd.discount * pd.harga_satuan) * pd.jumlah_beli) AS total_disc,
			SUM((pd.harga_satuan - (pd.discount * pd.harga_satuan)) * pd.jumlah_beli) AS total_bersih, 
			SUM(pd.jumlah_beli) AS jml_qty,
			COUNT(pd.id_barang_pusat) AS jml_transaksi, 
			MIN(pm.tanggal) AS tanggal_awal, MAX(pm.tanggal) AS tanggal_akhir
		');
		$this->db->from('vamr4846_vama.penjualan_detail AS pd');
		$this->db->join('vamr4846_vama.penjualan_master AS pm', 'pm.id_penjualan_m=pd.id_penjualan_m', 'LEFT');
		$this->db->join('vamr4846_vama.barang_pusat as bp','bp.id_barang_pusat=pd.id_barang_pusat', 'LEFT');
		$this->db->where('DATE(pm.tanggal) BETWEEN "'. date('Y-m-d', strtotime($tanggal_awal)). '" AND 
															   "'. date('Y-m-d', strtotime($tanggal_akhir)).'" 
						  AND pm.tipe_customer_pusat '.$jenis_perintah.' "MRC" AND
						  pm.status_penjualan = "SELESAI"
						');
		$this->db->group_by('bp.id_barang_pusat');

		$i = 0;
		foreach ($this->column_search_tpp as $item){
			if($_POST['search']['value']){
				if($i===0){
					$this->db->group_start();
					$this->db->like($item, $_POST['search']['value']);
				}else{
					$this->db->or_like($item, $_POST['search']['value']);
				}

				if(count($this->column_search_tpp) - 1 == $i)
					$this->db->group_end();
			}
			$i++;
		}

		if(isset($_POST['order'])){
			$this->db->order_by($this->column_order_tpp[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		}else if(isset($this->order_tpp)){
			$order = $this->order_tpp;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}

	private function _get_laporan_query_top_barang_percustomer($tanggal_awal, $tanggal_akhir, $jenis_perintah)
	{		
		$this->db->select('	
			IFNULL(cp.nama_customer_pusat, "-") AS nama_customer_pusat,
			bp.sku, bp.nama_barang, 
			pd.harga_satuan, SUM(pd.harga_satuan * pd.jumlah_beli) AS total_jual, 
			SUM((pd.discount * pd.harga_satuan) * pd.jumlah_beli) AS total_disc,
			SUM((pd.harga_satuan - (pd.discount * pd.harga_satuan)) * pd.jumlah_beli) AS total_bersih, 
			SUM(pd.jumlah_beli) AS jml_qty,
			COUNT(pd.id_barang_pusat) AS jml_transaksi, 
			MIN(pm.tanggal) AS tanggal_awal, MAX(pm.tanggal) AS tanggal_akhir
		');
		$this->db->from('vamr4846_vama.penjualan_detail AS pd');
		$this->db->join('vamr4846_vama.penjualan_master AS pm', 'pm.id_penjualan_m=pd.id_penjualan_m', 'LEFT');
		$this->db->join('vamr4846_vama.barang_pusat as bp','bp.id_barang_pusat=pd.id_barang_pusat', 'LEFT');
		$this->db->join('vamr4846_vama.customer_pusat as cp','cp.id_customer_pusat=pm.id_customer_pusat', 'LEFT');
		$this->db->where('DATE(pm.tanggal) BETWEEN "'. date('Y-m-d', strtotime($tanggal_awal)). '" AND 
															   "'. date('Y-m-d', strtotime($tanggal_akhir)).'" 
						  AND pm.tipe_customer_pusat '.$jenis_perintah.' "MRC" AND
						  pm.status_penjualan = "SELESAI"
						');
		$this->db->group_by('bp.id_barang_pusat, cp.id_customer_pusat');

		$i = 0;
		foreach ($this->column_search_tpc as $item){
			if($_POST['search']['value']){
				if($i===0){
					$this->db->group_start();
					$this->db->like($item, $_POST['search']['value']);
				}else{
					$this->db->or_like($item, $_POST['search']['value']);
				}

				if(count($this->column_search_tpc) - 1 == $i)
					$this->db->group_end();
			}
			$i++;
		}

		if(isset($_POST['order'])){
			$this->db->order_by($this->column_order_tpc[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		}else if(isset($this->order_tpc)){
			$order = $this->order_tpc;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}
	// Akhir pusat

	// Awal mrc
	private function _get_laporan_query_penjualan_perhari_toko($tanggal_awal, $tanggal_akhir, $kode_toko)
	{		
		$this->db->select('	
			IFNULL(cp.nama_customer_pusat, "-") AS nama_toko,
			pm.tanggal, COUNT(pm.id_penjualan_m) AS jml_transaksi, 
			SUM(pm.grand_total) AS total, SUM(pm.debit) AS debit, SUM(pm.tunai) AS tunai,
			SUM(rpm.total_retur) AS total_retur,
			SUM((pm.grand_total - IFNULL(rpm.total_retur, 0))) AS total_bersih,
			IFNULL(SUM(pd.sub_modal), 0) AS total_modal, 
			SUM((pm.grand_total - IFNULL(rpm.total_retur, 0))) - IFNULL(SUM(pd.sub_modal), 0) As total_keuntungan
		');
		$this->db->from('vamr4846_toko_mrc.penjualan_master_'.$kode_toko.' AS pm');
		$this->db->join('(SELECT id_penjualan_m, SUM(modal*jumlah_beli) as sub_modal 
						  FROM vamr4846_toko_mrc.penjualan_detail_'.$kode_toko.'
						  GROUP BY id_penjualan_m
						) AS pd', 'pd.id_penjualan_m=pm.id_penjualan_m', 'LEFT');
		$this->db->join('vamr4846_toko_mrc.retur_penjualan_master_'.$kode_toko.' as rpm',
						'rpm.id_penjualan_m=pm.id_penjualan_m AND rpm.status_retur="SELESAI"', 'LEFT');
		$this->db->join('vamr4846_vama.customer_pusat AS cp', 'cp.kode_customer_pusat="'.$kode_toko.'"', 'LEFT');
		$this->db->where('DATE(pm.tanggal) BETWEEN "'. date('Y-m-d', strtotime($tanggal_awal)). '" AND 
												   "'. date('Y-m-d', strtotime($tanggal_akhir)).'" AND
						  pm.status_penjualan = "SELESAI"
						');
		$this->db->group_by('pm.tanggal');


		$i = 0;
		foreach ($this->column_search_ppp as $item){
			if($_POST['search']['value']){
				if($i===0){
					$this->db->group_start();
					$this->db->like($item, $_POST['search']['value']);
				}else{
					$this->db->or_like($item, $_POST['search']['value']);
				}

				if(count($this->column_search_ppp) - 1 == $i)
					$this->db->group_end();
			}
			$i++;
		}

		if(isset($_POST['order'])){
			$this->db->order_by($this->column_order_ppp[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		}else if(isset($this->order_ppp)){
			$order = $this->order_ppp;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}

	private function _get_laporan_query_top_customer_toko($tanggal_awal, $tanggal_akhir, $kode_toko)
	{		
		$this->db->select('  
			IFNULL(ct.kode_customer_toko,"-") AS kode_customer,
			IFNULL(ct.nama_customer_toko,"-") AS nama_customer, ct.foto, 
			MIN(tanggal) AS tanggal_awal, MAX(tanggal) AS tanggal_akhir,
			count(pm.id_penjualan_m) AS jml_transaksi,
			IFNULL(SUM(pm.grand_total),0) AS total_jual,
			IFNULL(SUM(rpm.total_retur),0) AS total_retur, 
			IFNULL(SUM(pm.biaya_lain),0) AS biaya_lain, 
			IFNULL(SUM(pm.ppn),0) AS ppn, 
			IFNULL(
				IFNULL(SUM(pm.grand_total),0) -
				IFNULL(SUM(pm.biaya_lain),0) -
				IFNULL(SUM(pm.ppn),0)
			,0) AS total_bersih,
			IFNULL(
				IFNULL(SUM(pm.grand_total),0) -
				IFNULL(SUM(pm.biaya_lain),0) -
				IFNULL(SUM(pm.ppn),0)
			,0) - 
			IFNULL(SUM(rpm.total_retur),0) AS total_fix
		');
		$this->db->from('vamr4846_toko_mrc.penjualan_master_'.$kode_toko.' as pm');
		$this->db->join('vamr4846_toko_mrc.customer_toko as ct','ct.id_customer_toko=pm.id_customer_toko', 'LEFT');
		$this->db->join('vamr4846_toko_mrc.retur_penjualan_master_'.$kode_toko.' as rpm',
						'rpm.id_penjualan_m=pm.id_penjualan_m AND rpm.status_retur="SELESAI"', 'LEFT');
		$this->db->where('DATE(pm.tanggal) BETWEEN "'. date('Y-m-d', strtotime($tanggal_awal)). '" AND 
												   "'. date('Y-m-d', strtotime($tanggal_akhir)).'" AND
						  pm.status_penjualan = "SELESAI"
						');
		$this->db->group_by('ct.kode_customer_toko');

		$i = 0;
		foreach ($this->column_search_tct as $item){
			if($_POST['search']['value']){
				if($i===0){
					$this->db->group_start();
					$this->db->like($item, $_POST['search']['value']);
				}else{
					$this->db->or_like($item, $_POST['search']['value']);
				}

				if(count($this->column_search_tct) - 1 == $i)
					$this->db->group_end();
			}
			$i++;
		}

		if(isset($_POST['order'])){
			$this->db->order_by($this->column_order_tct[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		}else if(isset($this->order_tct)){
			$order = $this->order_tct;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}

	private function _get_laporan_query_top_barang_toko($tanggal_awal, $tanggal_akhir, $kode_toko)
	{		
		$this->db->select('	
			bp.sku, bp.nama_barang, 
			pd.harga_satuan, SUM(pd.harga_satuan * pd.jumlah_beli) AS total_jual, 
			SUM((pd.discount * pd.harga_satuan) * pd.jumlah_beli) AS total_disc,
			SUM((pd.harga_satuan - (pd.discount * pd.harga_satuan)) * pd.jumlah_beli) AS total_bersih, 
			SUM(pd.jumlah_beli) AS jml_qty,
			COUNT(pd.id_barang_pusat) AS jml_transaksi, 
			MIN(pm.tanggal) AS tanggal_awal, MAX(pm.tanggal) AS tanggal_akhir
		');
		$this->db->from('vamr4846_toko_mrc.penjualan_detail_'.$kode_toko.' AS pd');
		$this->db->join('vamr4846_toko_mrc.penjualan_master_'.$kode_toko.' AS pm', 'pm.id_penjualan_m=pd.id_penjualan_m', 'LEFT');
		$this->db->join('vamr4846_vama.barang_pusat as bp','bp.id_barang_pusat=pd.id_barang_pusat', 'LEFT');
		$this->db->where('DATE(pm.tanggal) BETWEEN "'. date('Y-m-d', strtotime($tanggal_awal)). '" AND 
												   "'. date('Y-m-d', strtotime($tanggal_akhir)).'" AND
						  pm.status_penjualan = "SELESAI"
						');
		$this->db->group_by('bp.id_barang_pusat');

		$i = 0;
		foreach ($this->column_search_tpp as $item){
			if($_POST['search']['value']){
				if($i===0){
					$this->db->group_start();
					$this->db->like($item, $_POST['search']['value']);
				}else{
					$this->db->or_like($item, $_POST['search']['value']);
				}

				if(count($this->column_search_tpp) - 1 == $i)
					$this->db->group_end();
			}
			$i++;
		}

		if(isset($_POST['order'])){
			$this->db->order_by($this->column_order_tpp[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		}else if(isset($this->order_tpp)){
			$order = $this->order_tpp;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}
	// Akhir mrc

	function get_laporan($perintah, $tanggal_awal, $tanggal_akhir, $jenis_perintah)
	{
		$this->$perintah($tanggal_awal, $tanggal_akhir, $jenis_perintah);
		if($_POST['length'] != -1)
		$this->db->limit($_POST['length'], $_POST['start']);
		$query = $this->db->get();
		return $query->result();
	}

	function count_filtered($perintah, $tanggal_awal, $tanggal_akhir, $jenis_perintah)
	{
		$this->$perintah($tanggal_awal, $tanggal_akhir, $jenis_perintah);
		$query = $this->db->get();
		return $query->num_rows();
	}

	public function count_all($table, $tanggal_awal, $tanggal_akhir)
	{
		$this->db->from($table);
		if($table == 'vamr4846_vama.penjualan_master'){
			$this->db->where('DATE(tanggal) BETWEEN "'. date('Y-m-d', strtotime($tanggal_awal)). '" AND 
							  		  				"'. date('Y-m-d', strtotime($tanggal_akhir)).'"');
		}else if($table == 'vamr4846_vama.penjualan_detail AS pd'){
			$this->db->join('vamr4846_vama.penjualan_master AS pm', 'pm.id_penjualan_m=pd.id_penjualan_m', 'LEFT');
			$this->db->where('DATE(pm.tanggal) BETWEEN "'. date('Y-m-d', strtotime($tanggal_awal)). '" AND 
													   "'. date('Y-m-d', strtotime($tanggal_akhir)).'"');
		}else if($table == 'vamr4846_vama.penjualan_detail_batal'){
			$this->db->where('DATE(tanggal_pembatalan) BETWEEN "'. date('Y-m-d', strtotime($tanggal_awal)). '" AND 
															   "'. date('Y-m-d', strtotime($tanggal_akhir)).'"');
		}else if($table == 'vamr4846_vama.penjualan_detail_kartu AS pdk'){
			$this->db->join('vamr4846_vama.penjualan_master AS pm', 'pm.id_penjualan_m=pdk.id_penjualan_m', 'LEFT');
			$this->db->where('DATE(pm.tanggal) BETWEEN "'. date('Y-m-d', strtotime($tanggal_awal)). '" AND 
													   "'. date('Y-m-d', strtotime($tanggal_akhir)).'"');
		}else if($table == 'vamr4846_vama.penjualan_detail AS pd2'){
			$this->db->select('pd2.id_barang_pusat');
			$this->db->join('vamr4846_vama.penjualan_master AS pm', 'pm.id_penjualan_m=pd2.id_penjualan_m', 'LEFT');
			$this->db->where('DATE(pm.tanggal) BETWEEN "'. date('Y-m-d', strtotime($tanggal_awal)). '" AND 
													   "'. date('Y-m-d', strtotime($tanggal_akhir)).'"');
			$this->db->group_by('pd2.id_barang_pusat');
		}
		return $this->db->count_all_results();
	}

	function get_laporan_mrc($perintah, $tanggal_awal, $tanggal_akhir, $kode_toko)
	{
		$this->$perintah($tanggal_awal, $tanggal_akhir, $kode_toko);
		if($_POST['length'] != -1)
		$this->db->limit($_POST['length'], $_POST['start']);
		$query = $this->db->get();
		return $query->result();
	}

	function count_filtered_mrc($perintah, $tanggal_awal, $tanggal_akhir, $kode_toko)
	{
		$this->$perintah($tanggal_awal, $tanggal_akhir, $kode_toko);
		$query = $this->db->get();
		return $query->num_rows();
	}

	public function count_all_mrc($table, $tanggal_awal, $tanggal_akhir, $kode_toko)
	{
		$this->db->from($table);
		if($table == 'vamr4846_toko_mrc.penjualan_master_'.$kode_toko.''){
			$this->db->where('DATE(tanggal) BETWEEN "'. date('Y-m-d', strtotime($tanggal_awal)). '" AND 
							  		  				"'. date('Y-m-d', strtotime($tanggal_akhir)).'"');
		}else if($table == 'vamr4846_toko_mrc.penjualan_detail_'.$kode_toko.' AS pd'){
			$this->db->join('vamr4846_toko_mrc.penjualan_master_'.$kode_toko.' AS pm', 'pm.id_penjualan_m=pd.id_penjualan_m', 'LEFT');
			$this->db->where('DATE(pm.tanggal) BETWEEN "'. date('Y-m-d', strtotime($tanggal_awal)). '" AND 
													   "'. date('Y-m-d', strtotime($tanggal_akhir)).'"');
		}else if($table == 'vamr4846_toko_mrc.penjualan_detail_batal_'.$kode_toko.''){
			$this->db->where('DATE(tanggal_pembatalan) BETWEEN "'. date('Y-m-d', strtotime($tanggal_awal)). '" AND 
															   "'. date('Y-m-d', strtotime($tanggal_akhir)).'"');
		}else if($table == 'vamr4846_toko_mrc.penjualan_detail_kartu_'.$kode_toko.' AS pdk'){
			$this->db->join('vamr4846_toko_mrc.penjualan_master_'.$kode_toko.' AS pm', 'pm.id_penjualan_m=pdk.id_penjualan_m', 'LEFT');
			$this->db->where('DATE(pm.tanggal) BETWEEN "'. date('Y-m-d', strtotime($tanggal_awal)). '" AND 
													   "'. date('Y-m-d', strtotime($tanggal_akhir)).'"');
		}else if($table == 'vamr4846_toko_mrc.penjualan_detail_'.$kode_toko.' AS pd2'){
			$this->db->select('pd2.id_barang_pusat');
			$this->db->join('vamr4846_toko_mrc.penjualan_master_'.$kode_toko.' AS pm', 'pm.id_penjualan_m=pd2.id_penjualan_m', 'LEFT');
			$this->db->where('DATE(pm.tanggal) BETWEEN "'. date('Y-m-d', strtotime($tanggal_awal)). '" AND 
													   "'. date('Y-m-d', strtotime($tanggal_akhir)).'"');
			$this->db->group_by('pd2.id_barang_pusat');
		}
		return $this->db->count_all_results();
	}

	function get_uang_mrc($tanggal_awal, $tanggal_akhir, $status_penjualan, $kode_toko)
	{
		$this->db->select('	
			COUNT(pm.id_penjualan_m)AS jml_transaksi, 
			SUM(pm.debit) AS total_debit, SUM(pm.tunai) AS total_tunai, SUM(pm.grand_total) AS total_transaksi,
			SUM(pm.ppn)AS total_ppn, SUM(pm.biaya_lain) AS total_biaya_lain, 
			(SUM(pm.grand_total) - (SUM(pm.biaya_lain)+SUM(pm.ppn)) - IFNULL(SUM(rpm.total_retur),0)) AS total_bersih
		');
		$this->db->from('vamr4846_toko_mrc.penjualan_master_'.$kode_toko.' as pm');
		$this->db->join('vamr4846_toko_mrc.retur_penjualan_master_'.$kode_toko.' as rpm',
						'rpm.id_penjualan_m=pm.id_penjualan_m AND rpm.status_retur="SELESAI"','LEFT');
		$this->db->join('vamr4846_toko_mrc.customer_toko as cp', 'cp.id_customer_toko=pm.id_customer_toko', 'LEFT');
		$this->db->where('DATE(pm.tanggal) BETWEEN "'. date('Y-m-d', strtotime($tanggal_awal)). '" AND 
												"'. date('Y-m-d', strtotime($tanggal_akhir)).'"
						');
		$this->db->where('pm.status_penjualan', 'SELESAI');
		$this->db->where('cp.nama_customer_toko <>','ONLINE');
		$query = $this->db->get();
		return $query->row();
	}

	function get_modal_mrc($tanggal_awal, $tanggal_akhir, $kode_toko)
	{
		$this->db->select('
			ROUND(SUM(pd.harga_satuan*pd.jumlah_beli),0) AS total_harga_satuan,
			ROUND(SUM((pd.harga_satuan*pd.discount)*pd.jumlah_beli),0) AS total_discount,
			ROUND(SUM((pd.harga_satuan-(pd.harga_satuan*pd.discount))*pd.jumlah_beli),0) AS total_harga_fix,
			ROUND(SUM(pd.modal*(pd.jumlah_beli-IFNULL(rpd.jumlah_retur,0))),0) AS total_modal,
			ROUND(
				SUM(
					(pd.harga_satuan-(pd.harga_satuan*pd.discount))*(pd.jumlah_beli-IFNULL(rpd.jumlah_retur,0))
				)-
				SUM(pd.modal*(pd.jumlah_beli-IFNULL(rpd.jumlah_retur,0)))
			,0) AS total_keuntungan
		');
		$this->db->from('vamr4846_toko_mrc.penjualan_master_'.$kode_toko.' AS pm');
		$this->db->join('vamr4846_toko_mrc.penjualan_detail_'.$kode_toko.' AS pd', 'pm.id_penjualan_m=pd.id_penjualan_m', 'LEFT');
		$this->db->join('vamr4846_toko_mrc.retur_penjualan_master_'.$kode_toko.' as rpm',
						'rpm.id_penjualan_m=pm.id_penjualan_m AND rpm.status_retur="SELESAI"','LEFT');
		$this->db->join('vamr4846_toko_mrc.retur_penjualan_detail_'.$kode_toko.' as rpd','rpd.id_penjualan_d=pd.id_penjualan_d','LEFT');
		$this->db->join('vamr4846_toko_mrc.customer_toko as cp', 'cp.id_customer_toko=pm.id_customer_toko', 'LEFT');
		$this->db->where('DATE(pm.tanggal) BETWEEN "'. date('Y-m-d', strtotime($tanggal_awal)). '" AND 
												"'. date('Y-m-d', strtotime($tanggal_akhir)).'"
						');
		$this->db->where('pm.status_penjualan','SELESAI');
		$this->db->where('cp.nama_customer_toko <>','ONLINE');
		$query = $this->db->get();
		return $query->row();
	}

	function get_asset_mrc($kode_toko)
	{
		$this->db->select('
			SUM(bt.total_stok*bp.modal_bersih) AS total_asset
		');
		$this->db->from('vamr4846_toko_mrc.barang_toko_'.$kode_toko.' AS bt');
		$this->db->join('vamr4846_vama.barang_pusat AS bp', 'bp.id_barang_pusat=bt.id_barang_pusat', 'LEFT');
		$query = $this->db->get();
		return $query->row();
	}

	function get_asset_pusat()
	{
		$this->db->select('
			SUM(bp.total_stok*bp.modal_bersih) AS total_asset
		');
		$this->db->from('vamr4846_vama.barang_pusat AS bp');
		$query = $this->db->get();
		return $query->row();
	}

	function get_uang_online($tanggal_awal, $tanggal_akhir, $status_penjualan, $kode_toko)
	{
		$this->db->select('	
			COUNT(pm.id_penjualan_m)AS jml_transaksi, 
			SUM(pm.debit) AS total_debit, SUM(pm.tunai) AS total_tunai, SUM(pm.grand_total) AS total_transaksi,
			SUM(pm.ppn)AS total_ppn, SUM(pm.biaya_lain) AS total_biaya_lain, 
			(SUM(pm.grand_total) - (SUM(pm.biaya_lain)+SUM(pm.ppn)) - IFNULL(SUM(rpm.total_retur),0)) AS total_bersih
		');
		$this->db->from('vamr4846_toko_mrc.penjualan_master_'.$kode_toko.' as pm');
		$this->db->join('vamr4846_toko_mrc.retur_penjualan_master_'.$kode_toko.' as rpm',
						'rpm.id_penjualan_m=pm.id_penjualan_m AND rpm.status_retur="SELESAI"','LEFT');
		$this->db->join('vamr4846_toko_mrc.customer_toko as cp', 'cp.id_customer_toko=pm.id_customer_toko', 'LEFT');
		$this->db->where('DATE(pm.tanggal) BETWEEN "'. date('Y-m-d', strtotime($tanggal_awal)). '" AND 
												"'. date('Y-m-d', strtotime($tanggal_akhir)).'"
						');
		$this->db->where(array(
			'pm.status_penjualan'   => 'SELESAI',
			'cp.nama_customer_toko' => 'ONLINE'
		));
		$query = $this->db->get();
		return $query->row();
	}

	function get_modal_online($tanggal_awal, $tanggal_akhir, $kode_toko)
	{
		$this->db->select('
			ROUND(SUM(pd.harga_satuan*pd.jumlah_beli),0) AS total_harga_satuan,
			ROUND(SUM((pd.harga_satuan*pd.discount)*pd.jumlah_beli),0) AS total_discount,
			ROUND(SUM((pd.harga_satuan-(pd.harga_satuan*pd.discount))*pd.jumlah_beli),0) AS total_harga_fix,
			ROUND(SUM(pd.modal*(pd.jumlah_beli-IFNULL(rpd.jumlah_retur,0))),0) AS total_modal,
			ROUND(
				SUM(
					(pd.harga_satuan-(pd.harga_satuan*pd.discount))*(pd.jumlah_beli-IFNULL(rpd.jumlah_retur,0))
				)-
				SUM(pd.modal*(pd.jumlah_beli-IFNULL(rpd.jumlah_retur,0)))
			,0) AS total_keuntungan
		');
		$this->db->from('vamr4846_toko_mrc.penjualan_master_'.$kode_toko.' AS pm');
		$this->db->join('vamr4846_toko_mrc.penjualan_detail_'.$kode_toko.' AS pd', 'pm.id_penjualan_m=pd.id_penjualan_m', 'LEFT');
		$this->db->join('vamr4846_toko_mrc.retur_penjualan_master_'.$kode_toko.' as rpm',
						'rpm.id_penjualan_m=pm.id_penjualan_m AND rpm.status_retur="SELESAI"','LEFT');
		$this->db->join('vamr4846_toko_mrc.retur_penjualan_detail_'.$kode_toko.' as rpd','rpd.id_penjualan_d=pd.id_penjualan_d','LEFT');
		$this->db->join('vamr4846_toko_mrc.customer_toko as cp', 'cp.id_customer_toko=pm.id_customer_toko', 'LEFT');
		$this->db->where('DATE(pm.tanggal) BETWEEN "'. date('Y-m-d', strtotime($tanggal_awal)). '" AND 
												"'. date('Y-m-d', strtotime($tanggal_akhir)).'"
						');
		$this->db->where(array(
			'pm.status_penjualan'   => 'SELESAI',
			'cp.nama_customer_toko' => 'ONLINE'
		));
		$query = $this->db->get();
		return $query->row();
	}
}
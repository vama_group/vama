<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pegawai_toko_model extends MY_Model {

	var $table 			= 'vamr4846_toko_mrc.pegawai_toko';
	var $column_order 	= array('pt.kode_pegawai_toko','pt.nama_pegawai_toko','pt.email');
	var $column_search 	= array('pt.kode_pegawai_toko','pt.nama_pegawai_toko','pt.email');
	var $order 			= array('pt.id_pegawai_toko' => 'DESC');

	private function _get_datatables_query()
	{
		
		$this->db->select('pt.*, ut.usergroup_name, pp1.nama_pegawai AS pegawai_save, pp2.nama_pegawai AS pegawai_edit,
						   cp.nama_customer_pusat AS nama_toko');
		$this->db->from('vamr4846_toko_mrc.pegawai_toko AS pt');				
		$this->db->join('vamr4846_toko_mrc.usergroup_toko AS ut','ut.id_usergroup=pt.id_usergroup','LEFT');
		$this->db->join('vamr4846_vama.customer_pusat AS cp','cp.kode_customer_pusat=pt.kode_toko','LEFT');
		$this->db->join('vamr4846_vama.pegawai_pusat AS pp1', 'pp1.id_pegawai=pt.id_pegawai_pembuatan', 'LEFT');
		$this->db->join('vamr4846_vama.pegawai_pusat AS pp2', 'pp2.id_pegawai=pt.id_pegawai_pembaharuan', 'LEFT');
		$this->db->where('pt.status_hapus', 'TIDAK');	
		$this->db->order_by('pt.kode_toko, pt.id_pegawai_toko', 'ASC');

		$i = 0;
		foreach ($this->column_search as $item){
			if($_POST['search']['value']){
				if($i===0){
					$this->db->group_start();
					$this->db->like($item, $_POST['search']['value']);
				}else{
					$this->db->or_like($item, $_POST['search']['value']);
				}

				if(count($this->column_search) - 1 == $i)
					$this->db->group_end();
			}
			$i++;
		}
		
		if(isset($_POST['order'])){
			$this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		}else if(isset($this->order)){
			$order = $this->order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}

	function get_datatables()
	{
		$this->_get_datatables_query();
		if($_POST['length'] != -1)
		$this->db->limit($_POST['length'], $_POST['start']);
		$query = $this->db->get();
		return $query->result();
	}

	function count_filtered()
	{
		$this->_get_datatables_query();
		$query = $this->db->get();
		return $query->num_rows();
	}

	public function count_all()
	{
		$this->db->from($this->table);
		return $this->db->count_all_results();
	}

	public function listing() {
		$this->db->select('pt.*, ug.usergroup_name, tk.nama_toko, pt.email');
		$this->db->from('vamr4846_toko_mrc.pegawai_toko AS pt');
		$this->db->join('vamr4846_toko_mrc.usergroup_toko AS ug','ug.id_usergroup=pt.id_usergroup');
		$this->db->join('vamr4846_vama.customer_pusat AS cp', 'cp.kode_customer_pusat=pt.kode_toko AND cp.tipe_customer_pusat="MRC"');
		$this->db->order_by('pt.id_pegawai_toko','DESC');
		$query=$this->db->get();
		return $query->result();
	}

	public function akhir($kode_toko) {
		$this->db->select('COUNT(pt.id_pegawai_toko)+1 AS id_pegawai_toko_baru');
		$this->db->from('vamr4846_toko_mrc.pegawai_toko AS pt');
		$this->db->where('pt.kode_toko', $kode_toko);
		$this->db->order_by('pt.id_pegawai_toko','DESC');
		$query=$this->db->get();
		return $query->row();
	}

	public function get_by_id($id_pegawai_toko)
	{
		$this->db->select('
			pt.id_pegawai_toko, pt.kode_toko, pt.nama_pegawai_toko, pt.email, pt.handphone, pt.alamat_pegawai_toko,
			pt.id_usergroup, pt.status_blokir, pt.foto,
			ut.usergroup_name, cp.nama_customer_pusat AS nama_toko');
		$this->db->from('vamr4846_toko_mrc.pegawai_toko AS pt');
		$this->db->join('vamr4846_toko_mrc.usergroup_toko AS ut','ut.id_usergroup=pt.id_usergroup','LEFT');
		$this->db->join('vamr4846_vama.customer_pusat AS cp','cp.kode_customer_pusat=pt.kode_toko','LEFT');
		$this->db->where('pt.id_pegawai_toko',$id_pegawai_toko);
		$query = $this->db->get();
		return $query->row();
	}

	public function get_by_email($email)
	{
		$this->db->select('pt.*, ut.usergroup_name, cp.nama_customer_pusat AS nama_toko');
		$this->db->from('vamr4846_toko_mrc.pegawai_toko AS pt');
		$this->db->join('vamr4846_toko_mrc.usergroup_toko AS ut','ut.id_usergroup=pt.id_usergroup','LEFT');
		$this->db->join('vamr4846_vama.customer_pusat AS cp','cp.kode_customer_pusat=pt.kode_toko','LEFT');
		$this->db->where('pt.email',$email);
		$query = $this->db->get();
		return $query->row();
	}

	public function get_by_nama($nama_pegawai_toko)
	{
		$this->db->select('pt.*, ut.usergroup_name, cp.nama_customer_pusat AS nama_toko');
		$this->db->from('vamr4846_toko_mrc.pegawai_toko AS pt');
		$this->db->join('vamr4846_toko_mrc.usergroup_toko AS ut','ut.id_usergroup=pt.id_usergroup','LEFT');
		$this->db->join('vamr4846_vama.customer_pusat AS cp','cp.kode_customer_pusat=pt.kode_toko','LEFT');
		$this->db->where('pt.nama_pegawai_toko',$nama_pegawai_toko);
		$query = $this->db->get();
		return $query->row();
	}

	public function save($data)
	{
		$this->db->insert($this->table, $data);
		return $this->db->insert_id();
	}

	public function create_useraccess_toko($id_pegawai_baru, $id_pegawai_save, $tanggal_pembuatan){
		$sql_query = $this->db->query("
			CALL vamr4846_vama.InsertUserAccessToko('".$id_pegawai_baru."', '".$id_pegawai_save."', '".$tanggal_pembuatan."')
		");
	}

	public function update($where, $data)
	{
		$this->db->update($this->table, $data, $where);
		return $this->db->affected_rows();
	}

	public function delete_by_id($id_pegawai_toko)
	{
		$this->db->where('id_pegawai_toko', $id_pegawai_toko);
		$this->db->delete('vamr4846_toko_mrc.pegawai_toko');
	}

	public function update_status_hapus($id_pegawai_save, $id_pegawai)
	{
		$sql = "UPDATE vamr4846_toko_mrc.pegawai_toko 
				SET status_hapus = 'IYA', id_pegawai_pembaharuan = '".$id_pegawai."' 
				WHERE id_pegawai_toko = '".$id_pegawai_save."'";
		return $this->db->query($sql);
	}

	// ----------------------------------------------------------------------------------------------------------------------
	// Awal query foto
	public function ambil_daftar_foto($id_pegawai)
	{
		$sql = "SELECT *
				FROM 
					vamr4846_toko_mrc.foto_pegawai_toko AS pt
				WHERE 
					(pt.id_pegawai = '".$id_pegawai."') 
				";
		return $this->db->query($sql);
	}

	public function update_foto_profile($id_pegawai, $nama_foto)
	{
		$sql = "UPDATE vamr4846_toko_mrc.pegawai_toko AS pt 
				SET pt.foto = '".$nama_foto."', pt.id_pegawai_pembaharuan = '".$this->session->userdata('id_pegawai')."'
				WHERE pt.id_pegawai_toko = '".$id_pegawai."'";
		return $this->db->query($sql);
	}

	public function ambil_nama_foto($id_foto)
	{
		$this->db->select('fpt.*');
		$this->db->from('vamr4846_toko_mrc.foto_pegawai_toko AS fpt');
		$this->db->where(array(  
		            			'fpt.id_foto' => $id_foto
		            	));
		$query = $this->db->get();
		return $query->row();
	}
	// Akhir query foto
	// ----------------------------------------------------------------------------------------------------------------------
}

<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Customer_toko_model extends MY_Model {
	// Table customer toko
	var $table 			= 'vamr4846_toko_mrc.customer_toko';
	var $column_order	= array('cp.kode_customer_toko', 'cp.nama_customer_toko', 'cp.email', 
								'cp.telephone1', 'cp.handphone1','cp.fax','cp.asal_customer_toko','cp.tipe_customer_toko'); 
	var $column_search 	= array('cp.kode_customer_toko', 'cp.nama_customer_toko', 'cp.email', 
								'cp.telephone1', 'cp.handphone1','cp.fax','cp.asal_customer_toko','cp.tipe_customer_toko'); 
	var $order 			= array('cp.id_customer_toko' => 'desc');

	// Table pendapatan poin
	var $column_order_pendapatan_poin  = array('ppo.no_penjualan', 'ppo.total_penjualan', 'ppo.jumlah_poin', 
											   'ppo.tanggal_pembaharuan', 'pp1.nama_pegawai'); 
	var $column_search_pendapatan_poin = array('ppo.no_penjualan', 'ppo.total_penjualan', 'ppo.jumlah_poin'); 
	var $order_pendapatan_poin         = array('ppo.id_poin_online' => 'desc');

	// Table penukaran poin
	var $column_order_penukaran_poin  = array('ppo.no_penukaran_poin', 'bp.nama_barang', 'ppo.qty', 
											  'ppo.tanggal_pembaharuan', 'ppo.nama_pegawai'); 
	var $column_search_penukaran_poin = array('ppo.no_penukaran_poin', 'bp.nama_barang', 'ppo.qty'); 
	var $order_penukaran_poin         = array('ppo.id_poin_online' => 'desc');

	private function _get_datatables_query()
	{		
		$this->db->select('
			cp.*, 
			pp1.nama_pegawai AS pegawai_save, pp2.nama_pegawai As pegawai_edit'
		);
		$this->db->from('vamr4846_toko_mrc.customer_toko AS cp');
		$this->db->join('vamr4846_toko_mrc.pegawai_toko AS pp1', 'pp1.id_pegawai_toko=cp.id_pegawai_pembuatan', 'LEFT');
		$this->db->join('vamr4846_toko_mrc.pegawai_toko AS pp2', 'pp2.id_pegawai_toko=cp.id_pegawai_pembaharuan', 'LEFT');
		$this->db->where('cp.status_hapus', 'TIDAK');

		$i = 0;
		foreach ($this->column_search as $item){
			if($_POST['search']['value']){
				if($i===0){
					$this->db->group_start();
					$this->db->like($item, $_POST['search']['value']);
				}else{
					$this->db->or_like($item, $_POST['search']['value']);
				}

				if(count($this->column_search) - 1 == $i)
					$this->db->group_end();
				}
			$i++;
		}
		
		if(isset($_POST['order'])){
			$this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		}else if(isset($this->order)){
			$order = $this->order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}

	private function _get_datatables_query_pendapatan_poin($id_customer_toko)
	{		
		$this->db->select('
			ppo.*, 
			pp1.nama_pegawai AS pegawai_save, pp2.nama_pegawai As pegawai_edit'
		);
		$this->db->from('vamr4846_vama.poin_penjualan_online AS ppo');
		$this->db->join('vamr4846_toko_mrc.pegawai_toko AS pp1', 'pp1.id_pegawai_toko=ppo.id_pegawai_pembuatan', 'LEFT');
		$this->db->join('vamr4846_toko_mrc.pegawai_toko AS pp2', 'pp2.id_pegawai_toko=ppo.id_pegawai_pembaharuan', 'LEFT');
		$this->db->where('ppo.id_customer_toko', $id_customer_toko);

		$i = 0;
		foreach ($this->column_search_pendapatan_poin as $item){
			if($_POST['search']['value']){
				if($i===0){
					$this->db->group_start();
					$this->db->like($item, $_POST['search']['value']);
				}else{
					$this->db->or_like($item, $_POST['search']['value']);
				}

				if(count($this->column_search_pendapatan_poin) - 1 == $i)
					$this->db->group_end();
				}
			$i++;
		}
		
		if(isset($_POST['order'])){
			$this->db->order_by($this->column_order_pendapatan_poin[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		}else if(isset($this->order_pendapatan_poin)){
			$order = $this->order_pendapatan_poin;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}

	function get_datatables($perintah, $id_customer_toko)
	{
		$this->$perintah($id_customer_toko);
		if($_POST['length'] != -1)
		$this->db->limit($_POST['length'], $_POST['start']);
		$query = $this->db->get();
		return $query->result();
	}

	function count_filtered($perintah, $id_customer_toko)
	{
		$this->$perintah($id_customer_toko);
		$query = $this->db->get();
		return $query->num_rows();
	}

	public function count_all($table, $id_customer_toko)
	{
		$this->db->from($table);
		if($table == 'vamr4846_toko_mrc.customer_toko'){
			$this->db->where('status_hapus', 'TIDAK');			
		}else if($table == 'vamr4846_vama.poin_penjualan_online'){
			$this->db->where('id_customer_toko', $id_customer_toko);						
		}
		return $this->db->count_all_results();
	}

	function cari_customer($keyword)
	{
		$sql = "
			SELECT 
				id_customer_toko, kode_customer_toko, nama_customer_toko,
				IF(handphone1='','Tidak ada',handphone1) AS handphone1, IF(email='','Tidak ada email',email) AS email, foto
			FROM 
				vamr4846_toko_mrc.customer_toko 
			WHERE 
				( 	id_customer_toko LIKE '%".$this->db->escape_like_str($keyword)."%' 
					OR nama_customer_toko LIKE '%".$this->db->escape_like_str($keyword)."%' 
					OR email LIKE '%".$this->db->escape_like_str($keyword)."%' 
				) 
		";

		return $this->db->query($sql);
	}

	public function get_baris($id_customer_toko)
	{
		return $this->db
			->select('id_customer_toko, nama_customer_toko, handphone1, tipe_customer_toko')
			->where('id_customer_toko', $id_customer_toko)
			->limit(1)
			->get('customer_toko');
	}

	public function cari_customer_toko($id_customer_toko) {
		$this->db->select('*');
		$this->db->from('vamr4846_toko_mrc.customer_toko AS cp');
		$this->db->where('id_customer_toko',$id_customer_toko);
		$query=$this->db->get();
		return $query->row();
	}

	public function cari_kode_customer_toko($kode_customer_toko) {
		$this->db->select('*');
		$this->db->from('vamr4846_toko_mrc.customer_toko AS cp');
		$this->db->where('kode_customer_toko',$kode_customer_toko);
		$query=$this->db->get();
		return $query->row();
	}

	public function listing() {
		$this->db->select('*');
		$this->db->from('vamr4846_toko_mrc.customer_toko AS cp');
		$this->db->order_by('id_customer_toko','DESC');
		$query=$this->db->get();
		return $query->result();
	}

	// Listing data mrc
	public function listing_toko() {
		$this->db->select('cp.*');
		$this->db->from('vamr4846_toko_mrc.customer_toko AS cp');
		$this->db->where('cp.tipe_customer_toko', 'MRC');		
		$this->db->order_by('cp.id_customer_toko', 'ASC');
		$query=$this->db->get();
		return $query->result();
	}

	public function akhir($id_toko){
		$this->db->select('count(id_customer_toko)+1 AS jumlah_customer');
		$this->db->from('vamr4846_toko_mrc.customer_toko AS cp');
		$this->db->where('id_toko', $id_toko);
		$this->db->order_by('id_customer_toko','DESC');
		$query=$this->db->get();
		return $query->row();
	}

	public function get_by_id($id_customer_toko)
	{
		$this->db->select('*');
		$this->db->from('vamr4846_toko_mrc.customer_toko AS cp');
		$this->db->where('cp.id_customer_toko', $id_customer_toko);
		$query = $this->db->get();
		return $query->row();
	}

	public function get_by_kode($kode_customer_toko)
	{
		$this->db->select('*');
		$this->db->from('vamr4846_toko_mrc.customer_toko AS cp');
		$this->db->where('cp.kode_customer_toko', $kode_customer_toko);
		$query = $this->db->get();
		return $query->row();
	}
	
	public function get_by_nama($nama_customer_toko)
	{
		$this->db->from($this->table);
		$this->db->where('nama_customer_toko',$nama_customer_toko);
		$query = $this->db->get();
		return $query->row();
	}

	public function get_by_email($email)
	{
		$this->db->from($this->table);
		$this->db->where('email',$email);
		$query = $this->db->get();
		return $query->row();
	}

	function get_customer_by_email($email)
	{
		return $this->db
			->select('id_customer_toko, nama_customer_toko, handphone1')
			->where('email', $email)
			->limit(1)
			->get('vamr4846_toko_mrc.customer_toko');
	}

	public function get_by_handphone1($handphone1)
	{
		$this->db->from($this->table);
		$this->db->where('handphone1',$handphone1);
		$query = $this->db->get();
		return $query->row();
	}

	public function save($data)
	{
		$this->db->insert($this->table, $data);
		return $this->db->insert_id();
	}

	public function update($where, $data)
	{
		$this->db->update($this->table, $data, $where);
		return $this->db->affected_rows();
	}

	public function delete_by_id($id_customer_toko)
	{
		$this->db->where('id_customer_toko', $id_customer_toko);
		$this->db->delete($this->table);
	}

	public function update_status_hapus($id_customer_toko, $id_pegawai)
	{
		$sql = "UPDATE vamr4846_toko_mrc.customer_toko 
				SET status_hapus = 'IYA', id_pegawai_pembaharuan = '".$this->db->escape_like_str($id_pegawai)."' 
				WHERE id_customer_toko = '".$this->db->escape_like_str($id_customer_toko)."'";
		return $this->db->query($sql);
	}
}

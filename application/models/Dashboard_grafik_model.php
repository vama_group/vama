<?php
class Dashboard_grafik_model extends CI_Model
{
	function get_chart_penjualan_toko_perbulan($tanggal_awal, $tanggal_akhir, $kode_toko)
	{
		$this->db->select('
			CONCAT(MONTHNAME(pm.tanggal), " ", YEAR(pm.tanggal)) AS nama_bulan,	
			COUNT(pm.id_penjualan_m)AS jml_transaksi, 
			SUM(pm.debit) AS total_debit, SUM(pm.tunai) AS total_tunai, SUM(pm.grand_total) AS total_transaksi,
			SUM(pm.ppn)AS total_ppn, SUM(pm.biaya_lain) AS total_biaya_lain, 
			(SUM(pm.grand_total) - (SUM(pm.biaya_lain)+SUM(pm.ppn)) - IFNULL(SUM(rpm.total_retur),0)) AS total_bersih,
			ROUND(IFNULL((SUM(pm.grand_total) - (SUM(pm.biaya_lain)+SUM(pm.ppn)) - IFNULL(SUM(rpm.total_retur),0)), "0")/1000000, 2) AS total_fix
		');
		$this->db->from('vamr4846_toko_mrc.penjualan_master_'.$kode_toko.' as pm');
		$this->db->join('vamr4846_toko_mrc.retur_penjualan_master_'.$kode_toko.' as rpm',
						'rpm.id_penjualan_m=pm.id_penjualan_m AND rpm.status_retur="SELESAI"','LEFT');
		$this->db->join('vamr4846_toko_mrc.customer_toko as cp', 'cp.id_customer_toko=pm.id_customer_toko', 'LEFT');
		$this->db->where('DATE(pm.tanggal) BETWEEN "'. date('Y-m-d', strtotime($tanggal_awal)). '" AND 
												   "'. date('Y-m-d', strtotime($tanggal_akhir)).'"
						');
		$this->db->where('pm.status_penjualan', 'SELESAI');
		$this->db->where('cp.nama_customer_toko <>','ONLINE');
		$this->db->group_by('YEAR(pm.tanggal), MONTH(pm.tanggal)');
		$query = $this->db->get();
		return $query->result();
	}

	function get_chart_penjualan_online_perbulan($tanggal_awal, $tanggal_akhir, $kode_toko)
	{
		$this->db->select('
			CONCAT(MONTHNAME(pm.tanggal), " ", YEAR(pm.tanggal)) AS nama_bulan,	
			COUNT(pm.id_penjualan_m)AS jml_transaksi, 
			SUM(pm.debit) AS total_debit, SUM(pm.tunai) AS total_tunai, SUM(pm.grand_total) AS total_transaksi,
			SUM(pm.ppn)AS total_ppn, SUM(pm.biaya_lain) AS total_biaya_lain, 
			(SUM(pm.grand_total) - (SUM(pm.biaya_lain)+SUM(pm.ppn)) - IFNULL(SUM(rpm.total_retur),0)) AS total_bersih,
			ROUND(IFNULL((SUM(pm.grand_total) - (SUM(pm.biaya_lain)+SUM(pm.ppn)) - IFNULL(SUM(rpm.total_retur),0)), "0")/1000000, 2) AS total_fix
		');
		$this->db->from('vamr4846_toko_mrc.penjualan_master_'.$kode_toko.' as pm');
		$this->db->join('vamr4846_toko_mrc.retur_penjualan_master_'.$kode_toko.' as rpm',
						'rpm.id_penjualan_m=pm.id_penjualan_m AND rpm.status_retur="SELESAI"','LEFT');
		$this->db->join('vamr4846_toko_mrc.customer_toko as cp', 'cp.id_customer_toko=pm.id_customer_toko', 'LEFT');
		$this->db->where('DATE(pm.tanggal) BETWEEN "'. date('Y-m-d', strtotime($tanggal_awal)). '" AND 
												   "'. date('Y-m-d', strtotime($tanggal_akhir)).'"
						');
		$this->db->where('pm.status_penjualan', 'SELESAI');
		$this->db->where('cp.nama_customer_toko','ONLINE');
		$this->db->group_by('YEAR(pm.tanggal), MONTH(pm.tanggal)');
		$query = $this->db->get();
		return $query->result();
	}

	function get_chart_penjualan_pusat_perbulan($jenis_perintah, $status_penjualan, $tanggal_awal, $tanggal_akhir)
	{
		$sql ="
				SELECT
					CONCAT(MONTHNAME(pm.tanggal), ' ', YEAR(pm.tanggal)) AS nama_bulan,
					COUNT(pm.id_penjualan_m) AS jml_transaksi, 
					ROUND(IFNULL(SUM(pm.grand_total), '0')/1000000, 2) AS total,
					ROUND(IFNULL((SUM(pm.grand_total) - (SUM(pm.biaya_lain)+SUM(pm.ppn)) - IFNULL(SUM(rpm.total_retur),0)), '0')/1000000, 2) AS total_fix
				FROM vamr4846_vama.penjualan_master AS pm				
				LEFT JOIN vamr4846_vama.retur_penjualan_master AS rpm ON rpm.id_penjualan_m=pm.id_penjualan_m AND rpm.status_retur='SELESAI'
				WHERE 
					pm.tipe_customer_pusat 	$jenis_perintah 'MRC' AND
					pm.status_penjualan 	= '".$this->db->escape_like_str($status_penjualan)."' AND
					DATE(pm.tanggal) BETWEEN '".$tanggal_awal."' AND '".$tanggal_akhir."'
				GROUP BY YEAR(pm.tanggal), MONTH(pm.tanggal)
		";

		$query = $this->db->query($sql);
		return $query->result();
	}

	function get_chart_penjualan_toko_perhari($kode_toko)
	{
		$sql ="
				SELECT 
					DATE_FORMAT(tgl, '%d-%m-%Y') AS tanggal, COUNT(pm.id_penjualan_m) AS jml_transaksi, 
					ROUND(IFNULL(SUM(pm.grand_total), '0')/1000000, 2) AS total,
					ROUND(IFNULL((SUM(pm.grand_total) - (SUM(pm.biaya_lain)+SUM(pm.ppn)) - IFNULL(SUM(rpm.total_retur),0)), '0')/1000000, 2) AS total_fix,
					CASE
						WHEN DATE_FORMAT(tgl, '%w') = 0 THEN 'MINGGU'
						WHEN DATE_FORMAT(tgl, '%w') = 1 THEN 'SENIN'
						WHEN DATE_FORMAT(tgl, '%w') = 2 THEN 'SELASA'
						WHEN DATE_FORMAT(tgl, '%w') = 3 THEN 'RABU'
						WHEN DATE_FORMAT(tgl, '%w') = 4 THEN 'KAMIS'
						WHEN DATE_FORMAT(tgl, '%w') = 5 THEN 'JUMAT'
						WHEN DATE_FORMAT(tgl, '%w') = 6 THEN 'SABTU'
					END AS hari
				FROM
				(
				    SELECT
				        MAKEDATE(YEAR(NOW()),1) +
				        INTERVAL (MONTH(NOW())-1) MONTH +
				        INTERVAL daynum DAY tgl
				    FROM
				    (
				        SELECT t*10+u daynum
				        FROM
				            (SELECT 0 t UNION SELECT 1 UNION SELECT 2 UNION SELECT 3) A,
				            (SELECT 0 u UNION SELECT 1 UNION SELECT 2 UNION SELECT 3
				            UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7
				            UNION SELECT 8 UNION SELECT 9) B
				        ORDER BY daynum
				    ) AA
				) vtgl
				LEFT JOIN (
					SELECT
						p.id_penjualan_m, p.id_customer_toko, p.grand_total, p.biaya_lain, p.ppn, p.tanggal, p.status_penjualan
					FROM vamr4846_toko_mrc.penjualan_master_".$kode_toko." AS p
					LEFT JOIN vamr4846_toko_mrc.customer_toko AS ct ON ct.id_customer_toko=p.id_customer_toko
					WHERE 
						p.status_penjualan = 'SELESAI' AND
						ct.nama_customer_toko != 'ONLINE'
				) AS pm ON vtgl.tgl=pm.tanggal
				LEFT JOIN vamr4846_toko_mrc.retur_penjualan_master_".$kode_toko." AS rpm ON 
					rpm.id_penjualan_m=pm.id_penjualan_m AND 
					rpm.status_retur='SELESAI'
				LEFT JOIN vamr4846_toko_mrc.customer_toko AS ct ON ct.id_customer_toko=pm.id_customer_toko
				WHERE 
					MONTH(tgl) = MONTH(NOW())
				GROUP BY vtgl.tgl
				ORDER BY 1 ASC
		";

		$query = $this->db->query($sql);
		return $query->result();
	}

	function get_chart_penjualan_online_perhari($kode_toko)
	{
		$sql ="
				SELECT 
					DATE_FORMAT(tgl, '%d-%m-%Y') AS tanggal, COUNT(pm.id_penjualan_m) AS jml_transaksi, 
					ROUND(IFNULL(SUM(pm.grand_total), '0')/1000000, 2) AS total,
					ROUND(IFNULL((SUM(pm.grand_total) - (SUM(pm.biaya_lain)+SUM(pm.ppn)) - IFNULL(SUM(rpm.total_retur),0)), '0')/1000000, 2) AS total_fix,
					CASE
						WHEN DATE_FORMAT(tgl, '%w') = 0 THEN 'MINGGU'
						WHEN DATE_FORMAT(tgl, '%w') = 1 THEN 'SENIN'
						WHEN DATE_FORMAT(tgl, '%w') = 2 THEN 'SELASA'
						WHEN DATE_FORMAT(tgl, '%w') = 3 THEN 'RABU'
						WHEN DATE_FORMAT(tgl, '%w') = 4 THEN 'KAMIS'
						WHEN DATE_FORMAT(tgl, '%w') = 5 THEN 'JUMAT'
						WHEN DATE_FORMAT(tgl, '%w') = 6 THEN 'SABTU'
					END AS hari
				FROM
				(
				    SELECT
				        MAKEDATE(YEAR(NOW()),1) +
				        INTERVAL (MONTH(NOW())-1) MONTH +
				        INTERVAL daynum DAY tgl
				    FROM
				    (
				        SELECT t*10+u daynum
				        FROM
				            (SELECT 0 t UNION SELECT 1 UNION SELECT 2 UNION SELECT 3) A,
				            (SELECT 0 u UNION SELECT 1 UNION SELECT 2 UNION SELECT 3
				            UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7
				            UNION SELECT 8 UNION SELECT 9) B
				        ORDER BY daynum
				    ) AA
				) vtgl
				LEFT JOIN (
					SELECT
						p.id_penjualan_m, p.id_customer_toko, p.grand_total, p.biaya_lain, p.ppn, p.tanggal, p.status_penjualan
					FROM vamr4846_toko_mrc.penjualan_master_".$kode_toko." AS p
					LEFT JOIN vamr4846_toko_mrc.customer_toko AS ct ON ct.id_customer_toko=p.id_customer_toko
					WHERE 
						p.status_penjualan = 'SELESAI' AND
						ct.nama_customer_toko = 'ONLINE'
				) AS pm ON vtgl.tgl=pm.tanggal
				LEFT JOIN vamr4846_toko_mrc.retur_penjualan_master_".$kode_toko." AS rpm ON 
					rpm.id_penjualan_m=pm.id_penjualan_m AND 
					rpm.status_retur='SELESAI'
				WHERE 
					MONTH(tgl) = MONTH(NOW())
				GROUP BY vtgl.tgl
				ORDER BY 1 ASC
		";

		$query = $this->db->query($sql);
		return $query->result();
	}
}
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Useraccess_toko_model extends MY_Model {

	var $table = 'vamr4846_toko_mrc.useraccess_toko';
	var $column_order = array('id_useraccess','nama_menu','act_read','act_create','act_update','act_update');
	var $column_search = array('id_useraccess','nama_menu','act_read','act_create','act_update','act_update'); 
	var $order = array('id_useraccess' => 'ASC'); 

	public function __construct(){
		parent::__construct();
		$this->load->database();
	}

	private function _get_datatables_query($id_pegawai)
	{
		$this->db->select('IFNULL(ut.id_pegawai,"-") AS id_pegawai, 
						   IFNULL(mt.id_menu,"-") AS id_menu, IFNULL(mt.id_menu_induk,"-") AS id_menu_induk, 
						   mt.nama_menu, mt.nama_lengkap_menu, mt.url, 
						   IFNULL(ut.act_read,"-") AS act_read, IFNULL(ut.act_create,"-") AS act_create,
						   IFNULL(ut.act_update,"-") AS act_update, IFNULL(ut.act_delete, "-") AS act_delete');
		$this->db->from('vamr4846_toko_mrc.useraccess_toko AS ut');
		$this->db->join('vamr4846_toko_mrc.menu_toko AS mt','ut.id_menu=mt.id_menu');
		$this->db->where(array(
			'id_pegawai'    => $id_pegawai,
			'mt.id_menu <>' => '1',
			'mt.url <>'     => 'sub_menu'
		));

		$i = 0;
	
		foreach ($this->column_search as $item){
			if($_POST['search']['value']){
				if($i===0){
					$this->db->group_start();
					$this->db->like($item, $_POST['search']['value']);
				}else{
					$this->db->or_like($item, $_POST['search']['value']);
				}

				if(count($this->column_search) - 1 == $i)
					$this->db->group_end();
			}
			$i++;
		}
		
		if(isset($_POST['order'])){
			$this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		}else if(isset($this->order)){
			$order = $this->order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}

	function get_datatables($id_pegawai)
	{
		$this->_get_datatables_query($id_pegawai);
		if($_POST['length'] != -1)
		$this->db->limit($_POST['length'], $_POST['start']);
		$query = $this->db->get();
		return $query->result();
	}

	function count_filtered($id_pegawai)
	{
		$this->_get_datatables_query($id_pegawai);
		$query = $this->db->get();
		return $query->num_rows();
	}

	public function count_all($id_pegawai)
	{
		$this->db->from('vamr4846_toko_mrc.useraccess_toko AS ut');
		$this->db->join('vamr4846_toko_mrc.menu_toko AS mt', 'mt.id_menu=ut.id_menu');
		$this->db->where(array(
			'ut.id_pegawai' => $id_pegawai,
			'mt.id_menu <>' => '1',
			'mt.url <>'     => 'sub_menu'
		));
		return $this->db->count_all_results();
	}

	public function save($data)
	{
		$this->db->insert($this->table, $data);
		return $this->db->insert_id();
	}

	public function update($where, $data)
	{
		$this->db->update($this->table, $data, $where);
		return $this->db->affected_rows();
	}

	public function delete_by_id($id_useraccess)
	{
		$this->db->where('id_useraccess', $id_useraccess);
		$this->db->delete($this->table);
	}

	// --------------------------------------------------------------------------------------------------------------------------
	// Awal menu dan useraccess
	public function get_induk_menu($id_pegawai)
	{
		$sql = "SELECT
					IFNULL(ut.id_pegawai,'-') AS id_pegawai, 
					IFNULL(mt.id_menu,'-') AS id_menu, IFNULL(mt.id_menu_induk,'-') AS id_menu_induk, 
					mt.nama_menu, mt.url, mt.icon
				FROM 
					vamr4846_toko_mrc.useraccess_toko AS ut
				LEFT JOIN
					vamr4846_toko_mrc.menu_toko As mt ON mt.id_menu=ut.id_menu 
				WHERE 
					id_pegawai 		 = '".$id_pegawai."' AND 
					mt.id_menu    	!= '1' AND
					id_menu_induk 	 = '0'
				";

		return $this->db->query($sql);
	}

	public function get_sub_menu($id_pegawai, $id_menu_induk)
	{
		$sql = "SELECT
					IFNULL(ut.id_pegawai,'-') AS id_pegawai, 
					IFNULL(mt.id_menu,'-') AS id_menu, IFNULL(mt.id_menu_induk,'-') AS id_menu_induk, 
					mt.nama_menu, mt.url, mt.icon
				FROM 
					vamr4846_toko_mrc.useraccess_toko AS ut
				LEFT JOIN
					vamr4846_toko_mrc.menu_toko As mt ON mt.id_menu=ut.id_menu 
				WHERE 
					id_pegawai 		 = '".$id_pegawai."' AND 
					act_read 		 = '1' AND
					id_menu_induk 	 = '".$id_menu_induk."'
				";

		return $this->db->query($sql);
	}

	public function get_anak_menu($id_pegawai, $id_menu_induk)
	{
		$sql = "SELECT
					IFNULL(ut.id_pegawai,'-') AS id_pegawai, 
					IFNULL(mt.id_menu,'-') AS id_menu, IFNULL(mt.id_menu_induk,'-') AS id_menu_induk, 
					mt.nama_menu, mt.url, mt.icon
				FROM 
					vamr4846_toko_mrc.useraccess_toko AS ut
				LEFT JOIN
					vamr4846_toko_mrc.menu_toko As mt ON mt.id_menu=ut.id_menu 
				WHERE 
					id_pegawai 		 = '".$id_pegawai."' AND 
					act_read 		 = '1' AND
					id_menu_induk 	 = '".$id_menu_induk."'
				";

		return $this->db->query($sql);
	}

	public function cek_menu_induk($id_menu)
	{
		$this->db->select('mt.id_menu, mt.nama_menu, mt.id_menu_induk');
		$this->db->from('vamr4846_toko_mrc.menu_toko AS mt');
		$this->db->where('id_menu', $id_menu);
		$query = $this->db->get();
		return $query->row();
	}

	public function cek_jumlah_useraccess($id_pegawai, $id_menu)
	{
		$this->db->select('ut.id_pegawai, mt.id_menu, id_menu_induk, nama_menu, SUM(ut.act_read)AS jumlah_access');
		$this->db->from('vamr4846_toko_mrc.menu_toko AS mt');
		$this->db->join('vamr4846_toko_mrc.useraccess_toko AS ut', 'ut.id_menu=mt.id_menu');
		$this->db->where(array(
			'ut.id_pegawai'    => $id_pegawai,
			'mt.id_menu_induk' => $id_menu
		));
		$query = $this->db->get();
		return $query->row();
	}

	public function cek_access($id_pegawai, $id_menu){
		$this->db->select('id_pegawai, id_menu, act_read, act_create, act_update, act_delete');
		$this->db->from('vamr4846_toko_mrc.useraccess_toko');
		$this->db->where(array(
			'id_pegawai' => $id_pegawai,
			'id_menu'    => $id_menu
		));
		$query = $this->db->get();
		return $query->row();
	}
	// Akhir menu dan useraccess
	// --------------------------------------------------------------------------------------------------------------------------
}

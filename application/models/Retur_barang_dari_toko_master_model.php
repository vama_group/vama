<?php
class Retur_barang_dari_toko_master_model extends CI_Model
{
	// Induk
	var $column_order         = array('rpm.id_retur_pembelian_m', 'rpm.id_retur_pembelian_m', 
									  'cp.nama_customer_pusat', 'rpm.no_retur_pembelian', 'rpm.tanggal_retur', 'rpm.status_retur',
									  'jml_barang_retur', 'jml_barang_masuk', 'rpm.keterangan_lain',
									  'pp1.nama_pegawai_toko', 'rpm.tanggal_pembuatan', 'pp2.nama_pegawai_toko', 'rpm.tanggal_pembaharuan');
	var $column_search        = array('rpm.no_retur_pembelian', 'cp.nama_customer_pusat'); 
	var $order                = array('rpm.id_retur_pembelian_m' => 'desc');
	// --------------------------------------------------------------------------------------------------------------------------------
	
	// Detail
	var $column_order_detail  = array('rpm.id_retur_pembelian_m', 'rpm.id_retur_pembelian_m', 'cp.nama_customer_pusat', 'rpm.no_retur_pembelian', 
									  'rpm.tanggal_masuk', 'rpm.status_retur', 'bp.sku', 'bp.nama_barang', 'rpd.harga_satuan',
									  'rpd.jumlah_retur', 'rpd.jumlah_masuk','rpd.dari_stok', 'rpd.kondisi_barang', 'rpd.keterangan',
									  'pp1.nama_pegawai_toko', 'rpd.tanggal_pembuatan', 'pp2.nama_pegawai_toko', 'rpd.tanggal_pembaharuan');
	var $column_search_detail = array('rpm.no_retur_pembelian', 'cp.nama_customer_pusat', 'bp.sku', 'bp.nama_barang', 'rpd.keterangan'); 
	var $order_detail         = array('rpm.id_retur_pembelian_m' => 'desc');
	// --------------------------------------------------------------------------------------------------------------------------------
	
	// Batal
	var $column_order_batal   = array('rpbd.id_retur_pembelian_m', 'rpbd.id_retur_pembelian_m', 'cp.nama_customer_pusat', 'rpbd.no_retur_pembelian', 
									  'rpbd.tanggal_retur', 'bp.sku', 'bp.nama_barang', 'rpbd.harga_satuan',
									  'rpbd.jumlah_retur', 'rpbd.jumlah_masuk','rpbd.dari_stok', 'rpbd.kondisi_barang', 'rpbd.keterangan', 'rpdb.keterangan_batal',
									  'pp4.nama_pegawai_toko', 'rpbd.tanggal_pembatalan', 'pp1.nama_pegawai_toko', 'rpbd.tanggal_pembuatan', 
									  'pp2.nama_pegawai_toko', 'rpbd.tanggal_pembaharuan', 'pp3.nama_pegawai', 'rpdb.tanggal_masuk');
	var $column_search_batal  = array('rpdb.no_retur_pembelian', 'cp.nama_customer_pusat', 
									  'rpdb.keterangan', 'rpdb.keterangan_batal');
	var $order_batal          = array('rpdb.id_retur_pembelian_b' => 'desc');
	// --------------------------------------------------------------------------------------------------------------------------------
	
	private function _get_laporan_query($tanggal_awal, $tanggal_akhir, $kode_toko)
	{		
		$this->db->select('
			rpm.*, 
			cp.nama_customer_pusat as nama_toko,
			count(rpd.id_retur_pembelian_d) AS jml_barang_retur,
			rpd2.jml_barang_masuk,
			pp1.nama_pegawai_toko AS pegawai_save, pp2.nama_pegawai_toko AS pegawai_edit, pp3.nama_pegawai AS pegawai_masuk, 
			rpm.tanggal_pembuatan, rpm.tanggal_pembaharuan, rpm.tanggal_masuk
		');
		$this->db->from('vamr4846_toko_mrc.retur_pembelian_master_'.$kode_toko.' AS rpm');
		$this->db->join('vamr4846_toko_mrc.retur_pembelian_detail_'.$kode_toko.' AS rpd', 'rpd.id_retur_pembelian_m=rpm.id_retur_pembelian_m', 'LEFT');
		$this->db->join('
		(
			SELECT id_retur_pembelian_m, count(id_retur_pembelian_d) AS jml_barang_masuk 
			FROM vamr4846_toko_mrc.retur_pembelian_detail_'.$kode_toko.'
			WHERE jumlah_masuk = jumlah_retur
			GROUP BY id_retur_pembelian_m
		)AS rpd2', 
		'rpd2.id_retur_pembelian_m=rpm.id_retur_pembelian_m', 'LEFT');
		$this->db->join('vamr4846_vama.customer_pusat as cp','cp.kode_customer_pusat="'.$kode_toko.'"','LEFT');
		$this->db->join('vamr4846_toko_mrc.pegawai_toko as pp1','pp1.id_pegawai_toko=rpm.id_pegawai_pembuatan','LEFT');
		$this->db->join('vamr4846_toko_mrc.pegawai_toko as pp2','pp2.id_pegawai_toko=rpm.id_pegawai_pembaharuan','LEFT');
		$this->db->join('vamr4846_vama.pegawai_pusat as pp3','pp3.id_pegawai=rpm.id_pegawai_masuk','LEFT');
		$this->db->where('DATE(rpm.tanggal_retur) BETWEEN "'. date('Y-m-d', strtotime($tanggal_awal)). '" AND 
														  "'. date('Y-m-d', strtotime($tanggal_akhir)).'"');
		$this->db->group_by('rpm.id_retur_pembelian_m');
		
		$i = 0;
		foreach ($this->column_search as $item){
			if($_POST['search']['value']){
				if($i===0){
					$this->db->group_start();
					$this->db->like($item, $_POST['search']['value']);
				}else{
					$this->db->or_like($item, $_POST['search']['value']);
				}

				if(count($this->column_search) - 1 == $i) 
					$this->db->group_end();
			}
			$i++;
		}

		if(isset($_POST['order'])){
			$this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		}else if(isset($this->order)){
			$order = $this->order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}

	private function _get_laporan_query_detail($tanggal_awal, $tanggal_akhir, $kode_toko)
	{		
		$this->db->select('
			rpd.*, 
			rpm.no_retur_pembelian,  rpm.tanggal_retur, rpm.status_retur,
			cp.nama_customer_pusat as nama_toko,
			bp.sku, bp.nama_barang, 
			pp1.nama_pegawai_toko AS pegawai_save, pp2.nama_pegawai_toko AS pegawai_edit, pp3.nama_pegawai AS pegawai_masuk
		');
		$this->db->from('vamr4846_toko_mrc.retur_pembelian_master_'.$kode_toko.' AS rpm');
		$this->db->join('vamr4846_toko_mrc.retur_pembelian_detail_'.$kode_toko.' AS rpd', 'rpd.id_retur_pembelian_m=rpm.id_retur_pembelian_m');
		$this->db->join('vamr4846_vama.barang_pusat AS bp', 'bp.id_barang_pusat=rpd.id_barang_pusat');
		$this->db->join('vamr4846_vama.customer_pusat as cp','cp.kode_customer_pusat="'.$kode_toko.'"','LEFT');
		$this->db->join('vamr4846_toko_mrc.pegawai_toko as pp1','pp1.id_pegawai_toko=rpd.id_pegawai_pembuatan','LEFT');
		$this->db->join('vamr4846_toko_mrc.pegawai_toko as pp2','pp2.id_pegawai_toko=rpd.id_pegawai_pembaharuan','LEFT');
		$this->db->join('vamr4846_vama.pegawai_pusat as pp3','pp3.id_pegawai=rpd.id_pegawai_masuk','LEFT');
		$this->db->where('DATE(rpm.tanggal_retur) BETWEEN "'. date('Y-m-d', strtotime($tanggal_awal)). '" AND 
														  "'. date('Y-m-d', strtotime($tanggal_akhir)).'"');
		
		$i = 0;
		foreach ($this->column_search_detail as $item){
			if($_POST['search']['value']){
				if($i===0){
					$this->db->group_start();
					$this->db->like($item, $_POST['search']['value']);
				}else{
					$this->db->or_like($item, $_POST['search']['value']);
				}

				if(count($this->column_search_detail) - 1 == $i) 
					$this->db->group_end();
			}
			$i++;
		}

		if(isset($_POST['order'])){
			$this->db->order_by($this->column_order_detail[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		}else if(isset($this->order_detail)){
			$order = $this->order_detail;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}

	private function _get_laporan_query_detail_batal($tanggal_awal, $tanggal_akhir, $kode_toko)
	{		
		$this->db->select('
			rpdb.*,
			cp.nama_customer_pusat as nama_toko,
			bp.sku, bp.nama_barang, 
			pp1.nama_pegawai_toko AS pegawai_save, pp2.nama_pegawai_toko AS pegawai_edit, 
			pp3.nama_pegawai AS pegawai_masuk, pp4.nama_pegawai_toko AS pegawai_pembatalan 
		');
		$this->db->from('vamr4846_toko_mrc.retur_pembelian_detail_batal_'.$kode_toko.' AS rpdb');
		$this->db->join('vamr4846_vama.barang_pusat AS bp', 'bp.id_barang_pusat=rpdb.id_barang_pusat');
		$this->db->join('vamr4846_vama.customer_pusat as cp','cp.kode_customer_pusat="'.$kode_toko.'"','LEFT');
		$this->db->join('vamr4846_toko_mrc.pegawai_toko as pp1','pp1.id_pegawai_toko=rpdb.id_pegawai_pembuatan','LEFT');
		$this->db->join('vamr4846_toko_mrc.pegawai_toko as pp2','pp2.id_pegawai_toko=rpdb.id_pegawai_pembaharuan','LEFT');
		$this->db->join('vamr4846_vama.pegawai_pusat as pp3','pp3.id_pegawai=rpdb.id_pegawai_masuk','LEFT');
		$this->db->join('vamr4846_toko_mrc.pegawai_toko as pp4','pp4.id_pegawai_toko=rpdb.id_pegawai_pembatalan','LEFT');
		$this->db->where('DATE(rpdb.tanggal_pembatalan) BETWEEN "'. date('Y-m-d', strtotime($tanggal_awal)). '" AND 
														   "'. date('Y-m-d', strtotime($tanggal_akhir)).'"');
		
		$i = 0;
		foreach ($this->column_search_batal as $item){
			if($_POST['search']['value']){
				if($i===0){
					$this->db->group_start();
					$this->db->like($item, $_POST['search']['value']);
				}else{
					$this->db->or_like($item, $_POST['search']['value']);
				}

				if(count($this->column_search_batal) - 1 == $i) 
					$this->db->group_end();
			}
			$i++;
		}

		if(isset($_POST['order'])){
			$this->db->order_by($this->column_order_batal[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		}else if(isset($this->order_batal)){
			$order = $this->order_batal;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}

	function get_laporan($perintah, $tanggal_awal, $tanggal_akhir, $kode_toko)
	{
		$this->$perintah($tanggal_awal, $tanggal_akhir, $kode_toko);
		if($_POST['length'] != -1)
		$this->db->limit($_POST['length'], $_POST['start']);
		$query = $this->db->get();
		return $query->result();
	}

	function count_filtered($perintah, $tanggal_awal, $tanggal_akhir, $kode_toko)
	{
		$this->$perintah($tanggal_awal, $tanggal_akhir, $kode_toko);
		$query = $this->db->get();
		return $query->num_rows();
	}

	public function count_all($table, $tanggal_awal, $tanggal_akhir, $kode_toko)
	{
		$this->db->from($table);
		if($table == 'vamr4846_toko_mrc.retur_pembelian_master_'.$kode_toko.''){
			$this->db->where('DATE(tanggal_retur) BETWEEN "'. date('Y-m-d', strtotime($tanggal_awal)). '" AND 
														  "'. date('Y-m-d', strtotime($tanggal_akhir)).'"');
		}else if($table == 'vamr4846_toko_mrc.retur_pembelian_detail_'.$kode_toko.' AS rpd'){
			$this->db->join('vamr4846_toko_mrc.retur_pembelian_master_'.$kode_toko.' AS rpm', 'rpm.id_retur_pembelian_m=rpd.id_retur_pembelian_m', 'LEFT');
			$this->db->where('DATE(rpm.tanggal_retur) BETWEEN "'. date('Y-m-d', strtotime($tanggal_awal)). '" AND 
													   "'. date('Y-m-d', strtotime($tanggal_akhir)).'"');
		}else if($table == 'vamr4846_toko_mrc.retur_pembelian_detail_batal_'.$kode_toko.''){
			$this->db->where('DATE(tanggal_pembatalan) BETWEEN "'. date('Y-m-d', strtotime($tanggal_awal)). '" AND 
															   "'. date('Y-m-d', strtotime($tanggal_akhir)).'"');
		}
		return $this->db->count_all_results();
	}

	function get_retur_tertahan($tanggal_awal, $tanggal_akhir, $kode_toko)
	{
		$sql = "
			SELECT
				rpm.*, 
				cp.nama_customer_pusat as nama_toko,
				count(rpd.id_retur_pembelian_d) AS jml_barang_retur,
				IFNULL(rpd2.jml_barang_masuk, '0') AS jml_barang_masuk,
				IFNULL(count(rpd.id_retur_pembelian_d) - IFNULL(rpd2.jml_barang_masuk, '0'), '0') AS jml_barang_menunggu,
				rpm.tanggal_pembuatan, rpm.tanggal_pembaharuan, rpm.tanggal_masuk
			FROM
				vamr4846_toko_mrc.retur_pembelian_master_".$kode_toko." AS rpm
			LEFT JOIN
				vamr4846_toko_mrc.retur_pembelian_detail_".$kode_toko." AS rpd ON rpd.id_retur_pembelian_m=rpm.id_retur_pembelian_m
			LEFT JOIN
				(
					SELECT id_retur_pembelian_m, count(id_retur_pembelian_d) AS jml_barang_masuk 
					FROM vamr4846_toko_mrc.retur_pembelian_detail_".$kode_toko."
					WHERE jumlah_masuk = jumlah_retur
					GROUP BY id_retur_pembelian_m
				)AS rpd2 ON rpd2.id_retur_pembelian_m=rpm.id_retur_pembelian_m
			LEFT JOIN
				vamr4846_vama.customer_pusat as cp ON cp.kode_customer_pusat='".$kode_toko."'
			WHERE
				#tanggal_retur BETWEEN 
				#	'". date('Y-m-d', strtotime($tanggal_awal)). "' AND 
				#	'". date('Y-m-d', strtotime($tanggal_akhir))."' AND
				status_retur = 'DIKIRIM'
			GROUP BY
				rpm.id_retur_pembelian_m
		";
		return $this->db->query($sql);
		// $query = $this->db->get();
		// return $query->result();
	}

	function get_grand_total($tanggal_awal, $tanggal_akhir, $jenis_perintah, $status_retur)
	{
		$this->db->select('	
			COUNT(id_retur_pembelian_m) AS jml_transaksi, SUM(total_potongan) AS grand_total_potongan, 
			SUM(total_retur) AS grand_total_retur
		');
		$this->db->from('vamr4846_vama.retur_pembelian_master');
		$this->db->where('DATE(tanggal_retur) BETWEEN "'. date('Y-m-d', strtotime($tanggal_awal)). '" AND 
													  "'. date('Y-m-d', strtotime($tanggal_akhir)).'" AND 
						  tipe_customer_pusat 		  '.$jenis_perintah.'  "MRC" AND
						  status_retur 				  = "'.$status_retur.'"
						');
		$query = $this->db->get();
		return $query->row();
	}

	function get_total_perbulan($tanggal_awal, $tanggal_akhir, $jenis_perintah)
	{
		$this->db->select('
			IFNULL(MIN(DATE_FORMAT(tanggal_retur, "%d-%m-%y")),"00-00-00") AS tanggal_awal,
			IFNULL(MAX(DATE_FORMAT(tanggal_retur, "%d-%m-%y")),"00-00-00") AS tanggal_akhir,
			COUNT(id_retur_pembelian_m) AS jml_transaksi, 
			IFNULL(SUM(total_potongan),0) AS grand_total_potongan, 
			IFNULL(SUM(total_retur),0) AS grand_total_retur
		');
		$this->db->from('vamr4846_vama.retur_pembelian_master');
		$this->db->where('
							DATE(tanggal_retur) BETWEEN "'. date('Y-m-d', strtotime($tanggal_awal)). '" AND 
												  		"'. date('Y-m-d', strtotime($tanggal_akhir)).'" AND
							tipe_customer_pusat 		'.$jenis_perintah.' "MRC"
						');
		$query = $this->db->get();
		return $query->row();
	}

	// No. pembelian baru
	function no_retur_penjualan_baru($tahun_sekarang, $bulan_sekarang, $id_pegawai) {
	    $this->db->select('COUNT(*)+1 AS no_baru');
	    $this->db->from('vamr4846_vama.retur_pembelian_master');
	    $this->db->where(array(  
    						'YEAR(tanggal_retur)'	=> $tahun_sekarang,
               				'MONTH(tanggal_retur)'  => $bulan_sekarang,
    						'id_pegawai_pembuatan'	=> $id_pegawai
	    				));
	    $this->db->order_by('tanggal_pembuatan','DESC');
	    $query = $this->db->get();
	    return $query->row();
	}

	// No. retur penjualan detail detail baru
	function no_retur_pembelian_detail_baru($id_retur_pembelian_m) {
		$this->db->select('COUNT(*)+1 AS no_retur_pembelian_detail_baru');
		$this->db->from('vamr4846_vama.retur_pembelian_detail');
		$this->db->where('id_retur_pembelian_m',$id_retur_pembelian_m);
		$this->db->order_by('tanggal_pembuatan','DESC');
		$query = $this->db->get();
		return $query->row();
	}

	function cari_daftar_transaksi($keyword, $kode_toko)
	{
		$sql = "
			SELECT 
				rpm.id_retur_pembelian_m, rpm.no_retur_pembelian,
				COUNT(rpd.jumlah_retur) AS jml_barang,
				rpm.tanggal_retur, rpm.keterangan_lain
			FROM 
				vamr4846_toko_mrc.retur_pembelian_master_".$kode_toko." AS rpm
			LEFT JOIN 
				vamr4846_toko_mrc.retur_pembelian_detail_".$kode_toko." AS rpd ON rpd.id_retur_pembelian_m=rpm.id_retur_pembelian_m
			WHERE
				rpm.status_retur!='SELESAI' AND
				rpm.no_retur_pembelian LIKE '%".$keyword."%'
			GROUP BY rpm.id_retur_pembelian_m
			LIMIT 10
		";
		return $this->db->query($sql);
	}

	function insert_master($no_retur_penjualan, $id_penjualan_m, $no_penjualan, $id_customer_pusat, $tipe_customer_pusat,
						   $total_potongan, $total_saldo, $total_retur, $tanggal, $catatan, $id_pegawai)
	{
		$dt = array(
			'no_retur_penjualan'   => $no_retur_penjualan,
			'id_penjualan_m'       => $id_penjualan_m,
			'no_penjualan'         => $no_penjualan,
			'id_customer_pusat'    => (empty($id_customer_pusat)) ? NULL : $id_customer_pusat,
			'tipe_customer_pusat'  => $tipe_customer_pusat,
			'total_retur'          => $total_retur,
			'total_potongan'       => $total_potongan,
			'total_saldo'          => $total_saldo,
			'tanggal_retur'        => $tanggal,
			'keterangan_lain'      => $catatan,
			'id_pegawai_pembuatan' => (empty($id_pegawai)) ? NULL : $id_pegawai,
			'tanggal_pembuatan'    => date('Y-m-d H:i:s')
		);
		return $this->db->insert('vamr4846_vama.retur_pembelian_master', $dt);
	}

	function update_master($id_retur_pembelian_m, $kode_toko, $id_pegawai, $status){
		$dt = array(
			'status_retur'           => $status,
			'id_pegawai_masuk'       => (empty($id_pegawai)) ? NULL : $id_pegawai,
			'tanggal_masuk'          => date('Y-m-d H:i:s'),
		);

		$where = array(
			'id_retur_pembelian_m'   => $id_retur_pembelian_m
		);

		return $this->db->update('vamr4846_toko_mrc.retur_pembelian_master_'.$kode_toko.'', $dt, $where);
	}

	function update_master_tanpa_total(
		$id_retur_pembelian_m, $id_penjualan_m, $no_penjualan, $id_customer_pusat, 
		$tanggal, $catatan, $id_pegawai, $status_retur){
		$dt = array(
			'id_penjualan_m'         => $id_penjualan_m,
			'status_retur' 			 => $status_retur,
			'no_penjualan'           => $no_penjualan,
			'id_customer_pusat'      => (empty($id_customer_pusat)) ? NULL : $id_customer_pusat,
			'tanggal_retur'          => $tanggal,
			'keterangan_lain'        => $catatan,
			'id_pegawai_pembaharuan' => (empty($id_pegawai)) ? NULL : $id_pegawai,
			'tanggal_pembuatan'      => date('Y-m-d H:i:s')
		);
		
		$where = array(
			'id_retur_pembelian_m'   => $id_retur_pembelian_m
		);
		return $this->db->update('vamr4846_vama.retur_pembelian_master', $dt, $where);
	}

	function get_master1($id_retur_pembelian_m, $kode_toko){
		$this->db->select('
			rpm.*, 
			COUNT(rpd.id_retur_pembelian_d) AS jumlah_barang
		');
		$this->db->from('vamr4846_toko_mrc.retur_pembelian_master_'.$kode_toko.' AS rpm');
		$this->db->join('vamr4846_toko_mrc.retur_pembelian_detail_'.$kode_toko.' AS rpd', 'rpm.id_retur_pembelian_m=rpd.id_retur_pembelian_m', 'LEFT');		
		$this->db->where('rpm.id_retur_pembelian_m', $id_retur_pembelian_m);
		$query = $this->db->get();
		return $query->row();
	}

	function get_master($id_retur_pembelian_m)
	{
		$this->db->select('rpm.*, count(id_retur_penjualan_d) as jumlah_retur,
						  IFNULL(cp.nama_customer_pusat,"-") AS nama_customer, 
						  IFNULL(cp.kode_customer_pusat,"-") AS kode_customer_pusat, 
						  IFNULL(cp.nama_customer_pusat,"-") AS nama_customer_pusat, 
						  IFNULL(cp.alamat_customer_pusat,"-") AS alamat_customer_pusat, 
						  IFNULL(cp.handphone1,"-") AS handphone1,
						  pm.grand_total, pp.nama_pegawai');
		$this->db->from('vamr4846_vama.retur_pembelian_master AS rpm');
		$this->db->join('vamr4846_vama.retur_pembelian_detail as rpd', 'rpm.id_retur_pembelian_m=rpd.id_retur_pembelian_m', 'LEFT');		
		$this->db->join('vamr4846_vama.penjualan_master as pm', 'pm.id_penjualan_m=rpm.id_penjualan_m', 'LEFT');		
		$this->db->join('vamr4846_vama.customer_pusat as cp', 'rpm.id_customer_pusat=cp.id_customer_pusat', 'LEFT');
		$this->db->join('vamr4846_vama.pegawai_pusat as pp', 'rpm.id_pegawai_pembaharuan=pp.id_pegawai', 'LEFT');
		$this->db->where('rpm.id_retur_pembelian_m', $id_retur_pembelian_m);
		$query = $this->db->get();
		return $query->row();
	}

	function get_jumlah_batal($id_retur_pembelian_m)
	{
		$this->db->select('rpdb.*, count(id_retur_penjualan_b) AS jumlah_barang');
		$this->db->from('vamr4846_vama.retur_pembelian_detail_batal AS rpdb');
		$this->db->where('id_retur_pembelian_m', $id_retur_pembelian_m);
		$query = $this->db->get();
		return $query->row();
	}

	function get_id($no_retur_penjualan)
	{
		$this->db->select('*');
		$this->db->from('vamr4846_vama.retur_pembelian_master');
		$this->db->where('no_retur_penjualan', $no_retur_penjualan);
		$query = $this->db->get();
		return $query->row();
	}

	function get_jumlah_retur($id, $kode_toko)
	{
		$this->db->select('
			rpm.id_retur_pembelian_m, rpm.no_retur_pembelian,
			COUNT(rpd.jumlah_retur) AS jml_barang,
			rpm.tanggal_retur, rpm.keterangan_lain
		');
		$this->db->from('vamr4846_toko_mrc.retur_pembelian_master_'.$kode_toko.' AS rpm');
		$this->db->join('vamr4846_toko_mrc.retur_pembelian_detail_'.$kode_toko.' AS rpd', 'rpd.id_retur_pembelian_m=rpm.id_retur_pembelian_m', 'LEFT');
		$this->db->where(array(
			'rpm.id_retur_pembelian_m' => $id
		));
		$query = $this->db->get();
		return $query->row();
	}

	function get_by_id($id, $kode_toko)
	{
		$this->db->select('
			rpm.id_retur_pembelian_m, rpm.no_retur_pembelian,
			SUM(rpd.jumlah_retur) AS jml_barang_retur,
			SUM(rpd.jumlah_masuk) AS jml_barang_masuk,
			rpm.tanggal_retur, rpm.keterangan_lain, status_retur
		');
		$this->db->from('vamr4846_toko_mrc.retur_pembelian_master_'.$kode_toko.' AS rpm');
		$this->db->join('vamr4846_toko_mrc.retur_pembelian_detail_'.$kode_toko.' AS rpd', 'rpd.id_retur_pembelian_m=rpm.id_retur_pembelian_m', 'LEFT');
		$this->db->where(array(
			'rpm.id_retur_pembelian_m' => $id
		));
		$query = $this->db->get();
		return $query->row();
	}

	function get_by_id_retur($id, $kode_toko)
	{
		$this->db->select('
			rpm.id_retur_pembelian_m, rpm.no_retur_pembelian,
			COUNT(rpd.jumlah_retur) AS jml_barang,
			rpm.tanggal_retur, rpm.keterangan_lain, status_retur
		');
		$this->db->from('vamr4846_toko_mrc.retur_pembelian_master_'.$kode_toko.' AS rpm');
		$this->db->join('vamr4846_toko_mrc.retur_pembelian_detail_'.$kode_toko.' AS rpd', 'rpd.id_retur_pembelian_m=rpm.id_retur_pembelian_m', 'LEFT');
		$this->db->where(array(
			'rpm.id_retur_pembelian_m' => $id,
			'rpd.jumlah_masuk >'       => 0
		));
		$query = $this->db->get();
		return $query->row();
	}

	function get_by_id_retur_batal($id, $kode_toko)
	{
		$this->db->select('
			rpbd.id_retur_pembelian_m, rpbd.no_retur_pembelian,
			COUNT(rpbd.jumlah_retur) AS jml_barang, rpbd.keterangan
		');
		$this->db->from('vamr4846_toko_mrc.retur_pembelian_detail_batal_'.$kode_toko.' AS rpbd');
		$this->db->where('rpbd.id_retur_pembelian_m', $id);
		$query = $this->db->get();
		return $query->row();
	}

	function hapus_transaksi($id_retur_pembelian_m, $id_pegawai_pembatalan, $keterangan_batal)
	{
		// Ambil data retur penjualan detail
		$this->db->select('rpd.*, rpm.no_retur_penjualan, rpm.no_penjualan, rpm.id_penjualan_m, 
			   			   rpm.id_customer_pusat, rpm.tipe_customer_pusat');
		$this->db->from('vamr4846_vama.retur_pembelian_detail As rpd');
		$this->db->join('vamr4846_vama.retur_pembelian_master AS rpm', 'rpm.id_retur_pembelian_m=rpd.id_retur_pembelian_m', 'LEFT');
		$this->db->where('rpd.id_retur_pembelian_m', $id_retur_pembelian_m);
		$loop = $this->db->get();

		foreach($loop->result() as $b){
			if($b->masuk_stok == 'JUAL'){
				$update_stok = 'total_stok';
			}else{
				$update_stok = 'total_stok_rusak';
			}

			// Kembalikan stok berdasarkan jumlah retur dan keterangan masuk stok
			$sql = "UPDATE vamr4846_vama.barang_pusat SET ".$update_stok." = ".$update_stok." - ".$b->jumlah_retur." 
					WHERE id_barang_pusat = '".$b->id_barang_pusat."'";
			$this->db->query($sql);

			// Simpan penjualan detail ke penjualan detail batal
			$sql_insert_batal = "
					INSERT INTO vamr4846_vama.retur_pembelian_detail_batal 
						(id_retur_penjualan_d, id_retur_pembelian_m, id_penjualan_m, id_penjualan_d, no_retur_penjualan,
						 no_penjualan, id_customer_pusat, tipe_customer_pusat, id_barang_pusat, harga_satuan, potongan_harga_value,
						 potongan_harga, jumlah_beli, jumlah_retur, subtotal_potongan, subtotal_saldo, subtotal_retur, masuk_stok, keterangan, 
						 id_pegawai_pembuatan, id_pegawai_pembaharuan, id_pegawai_pembatalan, tanggal_pembuatan, tanggal_pembaharuan, 
						 keterangan_batal) 
					VALUES
						('".$b->id_retur_penjualan_d."', '".$b->id_retur_pembelian_m."', 
						 '".$b->id_penjualan_m."', '".$b->id_penjualan_d."', '".$b->no_retur_penjualan."', '".$b->no_penjualan."',
						 '".$b->id_customer_pusat."', '".$b->tipe_customer_pusat."', '".$b->id_barang_pusat."', '".$b->harga_satuan."', 
						 '".$b->potongan_harga_value."', '".$b->potongan_harga."', '".$b->jumlah_beli."', '".$b->jumlah_retur."', 
						 '".$b->subtotal_potongan."', '".$b->subtotal_saldo."', '".$b->subtotal_retur."', '".$b->masuk_stok."', 
						 '".$b->keterangan."', '".$b->id_pegawai_pembuatan."', '".$b->id_pegawai_pembaharuan."', 
						 '".$id_pegawai_pembatalan."', '".$b->tanggal_pembuatan."', '".$b->tanggal_pembaharuan."', '".$keterangan_batal."') 
					";
			$this->db->query($sql_insert_batal);
		}

		$this->db->where('id_retur_pembelian_m', $id_retur_pembelian_m)->delete('vamr4846_vama.retur_pembelian_detail');
		return $this->db
					->where('id_retur_pembelian_m', $id_retur_pembelian_m)
					->delete('vamr4846_vama.retur_pembelian_master');
	}
}
<?php
class Perakitan_master_model extends CI_Model
{
	// Perakitan induk
	var $column_order         = array('pm.id_perakitan_m', 'pm.id_perakitan_m', 'pm.no_perakitan', 
									  'status_perakitan', 'pm.tanggal', 'bp.nama_barang', 
									  'bp.sku', 'bp.nama_barang', 'pm.jumlah_perakitan', 'jumlah_komponen', 
									  'pm.keterangan_lain', 'pp1.nama_pegawai');
	var $column_search        = array('pm.no_perakitan', 'bp.sku', 'bp.nama_barang', 'status_perakitan', 
									  'pp1.nama_pegawai', 'pp2.nama_pegawai'); 
	var $order                = array('id_perakitan_m' => 'desc');
	// --------------------------------------------------------------------------------------------------------------------------------------
	
	// Perakitan detail
	var $column_order_detail  = array('pm.id_perakitan_m', 'pm.id_perakitan_m', 'pm.no_perakitan', 'pm.status', 'pm.tanggal',
									  'bp1.nama_barang', 'bp2.sku', 'bp2.nama_barang', 'pd.jumlah_komponen', 
									  'pm.catatan', 'pp1.nama_pegawai');
	var $column_search_detail = array('pm.no_perakitan', 'bp1.sku', 'bp1.nama_barang', 'pm.jumlah_perakitan',  'status_perakitan',
									  'pp1.nama_pegawai', 'pp2.nama_pegawai'); 
	var $order_detail         = array('pm.id_perakitan_m' => 'desc', 
									  'pd.id_perakitan_d' => 'desc');
	// --------------------------------------------------------------------------------------------------------------------------------------
	
	// Perakitan batal
	var $column_order_batal   = array('pm.id_perakitan_m', 'pm.id_perakitan_m', 'pm.no_perakitan', 'pm.id_barang_pusat', 
									  'bp.sku', 'bp.nama_barang', 'pm.jumlah_perakitan', 'jumlah_komponen', 
									  'pm.tanggal', 'status_perakitan');
	var $column_search_batal  = array('pm.id_perakitan_m', 'pm.id_perakitan_m', 'pm.no_perakitan', 'pm.id_barang_pusat', 
									  'bp.sku', 'bp.nama_barang', 'pm.jumlah_perakitan',  
									  'pm.tanggal', 'status_perakitan'); 
	var $order_batal          = array('pdb.id_perakitan_b' => 'desc',
									  'pdb.id_perakitan_m' => 'desc');
	// --------------------------------------------------------------------------------------------------------------------------------------

	private function _get_laporan_query($tanggal_awal, $tanggal_akhir)
	{		
		$this->db->select('
			pm.id_perakitan_m, pm.no_perakitan, 
			pm.id_barang_pusat, bp.sku, bp.nama_barang, bp.foto, pm.jumlah_perakitan,
			pm.tanggal, status_perakitan, count(pd.id_perakitan_d) AS jumlah_komponen, SUM(pd.jumlah_komponen) AS total_komponen, 
			pp1.nama_pegawai AS pegawai_save, pp2.nama_pegawai AS pegawai_edit, pm.tanggal_pembuatan, pm.tanggal_pembaharuan, 
			pm.keterangan_lain
		');
		$this->db->from('vamr4846_vama.perakitan_master AS pm');
		$this->db->join('vamr4846_vama.perakitan_detail as pd','pd.id_perakitan_m=pm.id_perakitan_m','LEFT');
		$this->db->join('vamr4846_vama.barang_pusat as bp','bp.id_barang_pusat=pm.id_barang_pusat','LEFT');
		$this->db->join('vamr4846_vama.pegawai_pusat as pp1','pp1.id_pegawai=pm.id_pegawai_pembuatan','LEFT');
		$this->db->join('vamr4846_vama.pegawai_pusat as pp2','pp2.id_pegawai=pm.id_pegawai_pembaharuan','LEFT');
		$this->db->where('DATE(pm.tanggal) BETWEEN "'. date('Y-m-d', strtotime($tanggal_awal)). '" AND 
												   "'. date('Y-m-d', strtotime($tanggal_akhir)).'"');
		$this->db->group_by('pm.id_perakitan_m');

		$i = 0;
		foreach ($this->column_search as $item){
			if($_POST['search']['value']){
				if($i===0){
					$this->db->group_start();
					$this->db->like($item, $_POST['search']['value']);
				}else{
					$this->db->or_like($item, $_POST['search']['value']);
				}

				if(count($this->column_search) - 1 == $i)
					$this->db->group_end();
			}
			$i++;
		}
		
		if(isset($_POST['order'])){
			$this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		}else if(isset($this->order)){
			$order = $this->order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}

	private function _get_laporan_query_detail($tanggal_awal, $tanggal_akhir)
	{		
		$this->db->select('
			pm.id_perakitan_m, pm.no_perakitan, pm.tanggal, status_perakitan,
			pm.id_barang_pusat, bp1.sku AS sku_perakitan, bp1.nama_barang AS nama_barang_perakitan, 
			bp1.foto AS foto_perakitan, pm.jumlah_perakitan,
			pd.id_barang_pusat, bp2.sku AS sku_komponen, bp2.nama_barang AS nama_barang_komponen, 
			bp2.foto AS foto_komponen, pd.jumlah_komponen,
			pp1.nama_pegawai AS pegawai_save, pp2.nama_pegawai AS pegawai_edit, 
			pm.tanggal_pembuatan, pm.tanggal_pembaharuan, pm.keterangan_lain
		');
		$this->db->from('vamr4846_vama.perakitan_detail as pd');
		$this->db->join('vamr4846_vama.perakitan_master AS pm','pd.id_perakitan_m=pm.id_perakitan_m','LEFT');
		$this->db->join('vamr4846_vama.barang_pusat as bp1','bp1.id_barang_pusat=pm.id_barang_pusat','LEFT');
		$this->db->join('vamr4846_vama.barang_pusat as bp2','bp2.id_barang_pusat=pd.id_barang_pusat','LEFT');
		$this->db->join('vamr4846_vama.pegawai_pusat as pp1','pp1.id_pegawai=pd.id_pegawai_pembuatan','LEFT');
		$this->db->join('vamr4846_vama.pegawai_pusat as pp2','pp2.id_pegawai=pd.id_pegawai_pembaharuan','LEFT');
		$this->db->where('DATE(pd.tanggal_pembuatan) BETWEEN "'. date('Y-m-d', strtotime($tanggal_awal)). '" AND 
												  			 "'. date('Y-m-d', strtotime($tanggal_akhir)).'"');
		
		$i = 0;
		foreach ($this->column_search_detail as $item){
			if($_POST['search']['value']){
				if($i===0){
					$this->db->group_start();
					$this->db->like($item, $_POST['search']['value']);
				}else{
					$this->db->or_like($item, $_POST['search']['value']);
				}

				if(count($this->column_search_detail) - 1 == $i)
					$this->db->group_end();
			}
			$i++;
		}
		
		if(isset($_POST['order'])){
			$this->db->order_by($this->column_order_detail[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		}else if(isset($this->order_detail)){
			$order = $this->order_detail;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}

	private function _get_laporan_query_batal($tanggal_awal, $tanggal_akhir)
	{		
		$this->db->select('
			pdb.id_perakitan_m, pdb.no_perakitan,
			pdb.id_barang_perakitan, bp1.sku AS sku_perakitan, 
			bp1.nama_barang AS nama_barang_perakitan, bp1.foto AS foto_perakitan,
			pdb.id_barang_pusat, bp2.sku AS sku_komponen, bp2.nama_barang AS nama_barang_komponen, 
			bp2.foto AS foto_komponen, pdb.jumlah_komponen,
			pp1.nama_pegawai AS pegawai_save, pp2.nama_pegawai AS pegawai_edit, pp3.nama_pegawai AS pegawai_batal,
			pdb.tanggal_pembuatan, pdb.tanggal_pembaharuan, pdb.tanggal_pembatalan, pdb.keterangan_batal
		');
		$this->db->from('vamr4846_vama.perakitan_detail_batal as pdb');
		$this->db->join('vamr4846_vama.barang_pusat as bp1','bp1.id_barang_pusat=pdb.id_barang_perakitan','LEFT');
		$this->db->join('vamr4846_vama.barang_pusat as bp2','bp2.id_barang_pusat=pdb.id_barang_pusat','LEFT');
		$this->db->join('vamr4846_vama.pegawai_pusat as pp1','pp1.id_pegawai=pdb.id_pegawai_pembuatan','LEFT');
		$this->db->join('vamr4846_vama.pegawai_pusat as pp2','pp2.id_pegawai=pdb.id_pegawai_pembaharuan','LEFT');
		$this->db->join('vamr4846_vama.pegawai_pusat as pp3','pp3.id_pegawai=pdb.id_pegawai_pembatalan','LEFT');
		$this->db->where('DATE(pdb.tanggal_pembatalan) BETWEEN "'. date('Y-m-d', strtotime($tanggal_awal)). '" AND 
												  			 "'. date('Y-m-d', strtotime($tanggal_akhir)).'"');
		
		$i = 0;
		foreach ($this->column_search_batal as $item){
			if($_POST['search']['value']){
				if($i===0){
					$this->db->group_start();
					$this->db->like($item, $_POST['search']['value']);
				}else{
					$this->db->or_like($item, $_POST['search']['value']);
				}

				if(count($this->column_search_batal) - 1 == $i)
					$this->db->group_end();
			}
			$i++;
		}
		
		if(isset($_POST['order'])){
			$this->db->order_by($this->column_order_batal[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		}else if(isset($this->order_batal)){
			$order = $this->order_batal;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}

	function get_laporan($perintah, $tanggal_awal, $tanggal_akhir)
	{
		$this->$perintah($tanggal_awal, $tanggal_akhir);
		if($_POST['length'] != -1)
		$this->db->limit($_POST['length'], $_POST['start']);
		$query = $this->db->get();
		return $query->result();
	}

	function count_filtered($perintah, $tanggal_awal, $tanggal_akhir)
	{
		$this->$perintah($tanggal_awal, $tanggal_akhir);
		$query = $this->db->get();
		return $query->num_rows();
	}

	public function count_all($table, $tanggal_awal, $tanggal_akhir)
	{
		$this->db->from($table);
		if($table == 'vamr4846_vama.perakitan_master'){
			$this->db->where('DATE(tanggal) 
							  BETWEEN "'. date('Y-m-d', strtotime($tanggal_awal)). '" and "'. date('Y-m-d', strtotime($tanggal_akhir)).'"');
		}else if($table == 'vamr4846_vama.perakitan_detail'){
			$this->db->where('DATE(tanggal_pembuatan) 
							  BETWEEN "'. date('Y-m-d', strtotime($tanggal_awal)). '" and "'. date('Y-m-d', strtotime($tanggal_akhir)).'"');
		}else if($table == 'vamr4846_vama.perakitan_detail_batal'){
			$this->db->where('DATE(tanggal_pembatalan) 
							  BETWEEN "'. date('Y-m-d', strtotime($tanggal_awal)). '" and "'. date('Y-m-d', strtotime($tanggal_akhir)).'"');
		}
		return $this->db->count_all_results();
	}

	function no_perakitan_baru($tahun_sekarang, $bulan_sekarang, $id_pegawai) {
	    $this->db->select('COUNT(*)+1 AS no_baru');
	    $this->db->from('vamr4846_vama.perakitan_master');
	    $this->db->where(array(  
			'YEAR(tanggal)'        => $tahun_sekarang,
			'MONTH(tanggal)'       => $bulan_sekarang,
			'id_pegawai_pembuatan' => $id_pegawai
		));
	    $this->db->order_by('tanggal_pembuatan','DESC');
	    $query = $this->db->get();
	    return $query->row();
	}

	function no_perakitan_detail_baru($id_perakitan_m) {
		$this->db->select('COUNT(*)+1 AS no_perakitan_detail_baru');
		$this->db->from('vamr4846_vama.perakitan_detail');
		$this->db->where('id_perakitan_m',$id_perakitan_m);
		$this->db->order_by('tanggal_pembuatan','DESC');
		$query = $this->db->get();
		return $query->row();
	}

	function insert_master($no_perakitan, $id_barang_perakitan, $jumlah_perakitan,
						   $catatan, $tanggal, $id_pegawai)
	{
		$sql = "UPDATE vamr4846_vama.barang_pusat SET total_stok = total_stok + ".$jumlah_perakitan." 
				WHERE id_barang_pusat = '".$id_barang_perakitan."'";
		$this->db->query($sql);

		$dt = array(
			'no_perakitan'         => $no_perakitan,
			'id_barang_pusat'      => $id_barang_perakitan,
			'jumlah_perakitan'     => $jumlah_perakitan,
			'keterangan_lain'      => $catatan,
			'tanggal'              => $tanggal,
			'status_perakitan'     => 'MENUNGGU',
			'id_pegawai_pembuatan' => (empty($id_pegawai)) ? NULL : $id_pegawai,
			'tanggal_pembuatan'    => date('Y-m-d H:i:s')
		);

		return $this->db->insert('vamr4846_vama.perakitan_master', $dt);
	}

	function update_master($id_perakitan_m, $catatan, $id_pegawai, $status_perakitan)
	{
		$dt = array(
			'keterangan_lain'        => $catatan,
			'status_perakitan'       => $status_perakitan,
			'id_pegawai_pembaharuan' => (empty($id_pegawai)) ? NULL : $id_pegawai
		);
		$where = array('id_perakitan_m' => $id_perakitan_m);
		return $this->db->update('vamr4846_vama.perakitan_master', $dt, $where);
	}

	function update_stok_master($id_perakitan_m){
		$sql = "UPDATE vamr4846_vama.perakitan_master SET jumlah_perakitan = '0' 
				WHERE id_perakitan_m = '".$id_perakitan_m."'";
		return $this->db->query($sql);
	}

	function get_by_id($id_perakitan_m)
	{
		$this->db->select('pm.*, bp.sku, bp.nama_barang, bp.harga_eceran');
		$this->db->from('vamr4846_vama.perakitan_master AS pm');
		$this->db->join('vamr4846_vama.barang_pusat AS bp', 'pm.id_barang_pusat=bp.id_barang_pusat', 'LEFT');
		$this->db->where('id_perakitan_m', $id_perakitan_m);
		$query = $this->db->get();
		return $query->row();
	}

	function get_by_no_trans($no_perakitan)
	{
		$this->db->select('pm.*, bp.sku, bp.nama_barang, bp.harga_eceran');
		$this->db->from('vamr4846_vama.perakitan_master AS pm');
		$this->db->join('vamr4846_vama.barang_pusat AS bp', 'pm.id_barang_pusat=bp.id_barang_pusat', 'LEFT');
		$this->db->where('no_perakitan', $no_perakitan);
		$query = $this->db->get();
		return $query->row();
	}

	function get_jumlah_jual($id_perakitan_m)
	{
		$this->db->select('count(*) as jumlah_jual');
		$this->db->from('vamr4846_vama.perakitan_detail');
		$this->db->where('id_perakitan_m', $id_perakitan_m);
		$query = $this->db->get();
		return $query->row();
	}

	function get_master($id_perakitan_m)
	{
		$this->db->select('
			pm.*,
			count(pd.id_perakitan_d) AS jumlah_komponen, SUM(pd.jumlah_komponen) AS total_komponen,
			bp.sku, bp.nama_barang, bp.harga_eceran, bp.foto, 
		');
		$this->db->from('vamr4846_vama.perakitan_master AS pm');
		$this->db->join('vamr4846_vama.perakitan_detail as pd','pd.id_perakitan_m=pm.id_perakitan_m','LEFT');
		$this->db->join('vamr4846_vama.barang_pusat as bp','bp.id_barang_pusat=pm.id_barang_pusat','LEFT');
		$this->db->where('pm.id_perakitan_m', $id_perakitan_m);
		$query = $this->db->get();
		return $query->row();
	}

	function update_qty_perakitan($id_perakitan_m, $id_barang_perakitan, $jumlah_perakitan, $jumlah_perakitan_lama, $id_pegawai)
	{
		// Filter data perakitan detail berdasarkan id perakitan master
		$loop = $this->db
			->select('*')
			->where('id_perakitan_m', $id_perakitan_m)
			->get('vamr4846_vama.perakitan_detail');

		foreach($loop->result() as $b)
		{
			// Kembalikan stok komponen sebelumnya
			$sql_tambah_stok = "UPDATE vamr4846_vama.barang_pusat SET total_stok = total_stok + ".$jumlah_perakitan_lama." 
								WHERE id_barang_pusat = '".$b->id_barang_pusat."'";
			$this->db->query($sql_tambah_stok);

			// // Tambahkan stok komponen berdasarkan jumlah perakitan
			$sql_ambil_stok = "UPDATE vamr4846_vama.barang_pusat SET total_stok = total_stok - ".$jumlah_perakitan." 
							   WHERE id_barang_pusat = '".$b->id_barang_pusat."'";
			$this->db->query($sql_ambil_stok);
		}

		// Tambah stok barang perakitan
		$sql_update_stok = "UPDATE vamr4846_vama.barang_pusat SET total_stok = total_stok - ".$jumlah_perakitan_lama." 
				WHERE id_barang_pusat = '".$id_barang_perakitan."'";
		$this->db->query($sql_update_stok);

		// Tambah stok barang perakitan
		$sql_update_stok_perakitan = "UPDATE vamr4846_vama.barang_pusat SET total_stok = total_stok + ".$jumlah_perakitan." 
				WHERE id_barang_pusat = '".$id_barang_perakitan."'";
		$this->db->query($sql_update_stok_perakitan);

		// Update stok komponen berdasarkan jumlah perakitan sekarang
		$sql_update = "UPDATE vamr4846_vama.perakitan_detail 
					   SET jumlah_komponen = ".$jumlah_perakitan.", id_pegawai_pembaharuan = ".$id_pegawai." 
					   WHERE id_perakitan_m = '".$id_perakitan_m."'";
		$this->db->query($sql_update);

		// Update stok perakitan berdasarkan jumlah perakitan sekarang
		$sql_update = "UPDATE vamr4846_vama.perakitan_master 
					   SET jumlah_perakitan = ".$jumlah_perakitan.", id_pegawai_pembaharuan = ".$id_pegawai." 
					   WHERE id_perakitan_m = '".$id_perakitan_m."'";
		$this->db->query($sql_update);
	}

	function hapus_transaksi($id_perakitan_m, $no_perakitan, $id_barang_perakitan, $jumlah_perakitan, 
							 $keterangan_batal, $id_pegawai_pembatalan)
	{
		// Filter data perakitan detail berdasarkan id perakitan master
		$loop = $this->db
			->select('*')
			->where('id_perakitan_m', $id_perakitan_m)
			->get('vamr4846_vama.perakitan_detail');

		foreach($loop->result() as $b)
		{
			// Kembalikan stok komponen
			$sql_update_stok = "UPDATE vamr4846_vama.barang_pusat SET total_stok = total_stok + ".$b->jumlah_komponen." 
								WHERE id_barang_pusat = '".$b->id_barang_pusat."'";
			$this->db->query($sql_update_stok);

			// Simpan perakitan detail ke perakitan detail batal
			$sql_insert_batal = "
								INSERT INTO vamr4846_vama.perakitan_detail_batal 
									(id_perakitan_d, id_perakitan_m, no_perakitan, id_barang_perakitan, 
									 id_barang_pusat, jumlah_komponen, 
									 id_pegawai_pembuatan, id_pegawai_pembaharuan, id_pegawai_pembatalan, 
									 tanggal_pembuatan, tanggal_pembaharuan, keterangan_batal) 
								VALUES
									('".$b->id_perakitan_d."', '".$id_perakitan_m."', '".$no_perakitan."', '".$id_barang_perakitan."', 
									 '".$b->id_barang_pusat."', '".$b->jumlah_komponen."', 
									 '".$b->id_pegawai_pembuatan."', '".$b->id_pegawai_pembaharuan."', '".$id_pegawai_pembatalan."', 
									 '".$b->tanggal_pembuatan."', '".$b->tanggal_pembaharuan."', '".$keterangan_batal."')
									";
			$this->db->query($sql_insert_batal);
		}

		// Kembalikan stok perakitan
		$sql_update_stok = "UPDATE vamr4846_vama.barang_pusat SET total_stok = total_stok - ".$jumlah_perakitan." 
				WHERE id_barang_pusat = '".$id_barang_perakitan."'";
		$this->db->query($sql_update_stok);

		// Hapus perakitan detail dan perakitan master
		$this->db->where('id_perakitan_m', $id_perakitan_m)
		         ->delete('vamr4846_vama.perakitan_detail');
		return $this->db->where('id_perakitan_m', $id_perakitan_m)
			            ->delete('vamr4846_vama.perakitan_master');
	}

	function get_grand_total($tanggal_awal, $tanggal_akhir)
	{
		$this->db->select('COUNT(id_perakitan_m) AS jml_transaksi');
		$this->db->from('vamr4846_vama.perakitan_master');
		$this->db->where('DATE(tanggal) BETWEEN "'. date('Y-m-d', strtotime($tanggal_awal)). '" AND 
											 	"'. date('Y-m-d', strtotime($tanggal_akhir)).'"');
		$query = $this->db->get();
		return $query->row();
	}

	function get_total_perakitan_selesai($tanggal_awal, $tanggal_akhir)
	{
		$this->db->select('
			IFNULL(MIN(DATE_FORMAT(tanggal, "%d-%m-%y")),"00-00-00") AS tanggal_awal,
			IFNULL(MAX(DATE_FORMAT(tanggal, "%d-%m-%y")),"00-00-00") AS tanggal_akhir,
			IFNULL(COUNT(id_perakitan_m),0) AS total_barang_perakitan, 
			IFNULL(SUM(jumlah_perakitan),0) AS total_qty_perakitan
	    ');
		$this->db->from('vamr4846_vama.perakitan_master');
		$this->db->where(array(
			'tanggal >' 		=> $tanggal_awal,
			'tanggal <=' 		=> $tanggal_akhir,
			'status_perakitan' 	=> 'SELESAI'
		));
		$query = $this->db->get();
		return $query->row();
	}

	function get_total_perakitan_menunggu($tanggal_awal, $tanggal_akhir)
	{
		$this->db->select('
			IFNULL(MIN(DATE_FORMAT(tanggal, "%d-%m-%y")),"00-00-00") AS tanggal_awal,
			IFNULL(MAX(DATE_FORMAT(tanggal, "%d-%m-%y")),"00-00-00") AS tanggal_akhir,
			IFNULL(COUNT(id_perakitan_m),0) AS total_barang_perakitan, 
			IFNULL(SUM(jumlah_perakitan),0) AS total_qty_perakitan
		');
		$this->db->from('vamr4846_vama.perakitan_master');
		$this->db->where(array(
			'tanggal >' 		=> $tanggal_awal,
			'tanggal <=' 		=> $tanggal_akhir,
			'status_perakitan' 	=> 'MENUNGGU'
		));
		$query = $this->db->get();
		return $query->row();
	}

	function get_total_komponen_selesai($tanggal_awal, $tanggal_akhir)
	{
		$this->db->select('
							IFNULL(COUNT(id_perakitan_d),0) AS total_barang_komponen, 
							IFNULL(SUM(jumlah_komponen),0) AS total_qty_komponen
					      ');
		$this->db->from('vamr4846_vama.perakitan_detail as pd');
		$this->db->join('vamr4846_vama.perakitan_master AS pm', 'pd.id_perakitan_m=pm.id_perakitan_m', 'LEFT');
		$this->db->where(array(
			'tanggal >' 		=> $tanggal_awal,
			'tanggal <=' 		=> $tanggal_akhir,
			'status_perakitan' 	=> 'SELESAI'
		));
		$query = $this->db->get();
		return $query->row();
	}

	function get_total_komponen_menunggu($tanggal_awal, $tanggal_akhir)
	{
		$this->db->select('
			IFNULL(COUNT(id_perakitan_d),0) AS total_barang_komponen, 
			IFNULL(SUM(jumlah_komponen),0) AS total_qty_komponen
		');
		$this->db->from('vamr4846_vama.perakitan_detail AS pd');
		$this->db->join('vamr4846_vama.perakitan_master AS pm', 'pd.id_perakitan_m=pm.id_perakitan_m', 'LEFT');
		$this->db->where(array(
			'tanggal >' 		=> $tanggal_awal,
			'tanggal <=' 		=> $tanggal_akhir,
			'status_perakitan' 	=> 'MENUNGGU'
		));
		$query = $this->db->get();
		return $query->row();
	}
}
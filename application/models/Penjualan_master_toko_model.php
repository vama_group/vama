<?php
class Penjualan_master_toko_model extends CI_Model
{
	// Penjualan induk umum
	var $column_order            = array('pm.id_penjualan_m', 'pm.no_penjualan', 'rpm.no_retur_penjualan', 
										 'pm.status_penjualan', 'pm.tanggal', 'ct.nama_customer_toko', 
										 'jml_barang', 'pm.grand_total', 'rpm.total_retur', 'total_bersih',
										 'pegawai_save', 'pm.tanggal_pembuatan', 'pegawai_edit', 'pm.tanggal_pembaharuan');
	var $column_search           = array('pm.no_penjualan', 'rpm.no_retur_penjualan', 'ct.nama_customer_toko', 'pm.status_penjualan',
										 'pt1.nama_pegawai_toko', 'pt2.nama_pegawai_toko'); 
	var $order                   = array('pm.id_penjualan_m' => 'desc');
	//--------------------------------------------------------------------------------------------------------------------------------------
	
	// Penjualan induk detail
	var $column_order_detail     = array('pd.id_penjualan_d', 
										 'pm.no_penjualan', 'rpm.no_retur_penjualan', 'ct.nama_customer_toko', 
										 'bp.nama_barang', 'pd.harga_satuan', 'discount_harga', 'harga_bersih',
										 'pd.jumlah_beli', 'rpd.jumlah_retur', 'jumlah_bersih',  
						 				 'pd.subtotal', 'rpd.subtotal_retur', 'subtotal_bersih', 
										 'pegawai_save', 'pd.tanggal_pembuatan', 'pegawai_edit', 'pd.tanggal_pembaharuan');
	var $column_search_detail    = array('pm.no_penjualan', 'rpm.no_retur_penjualan', 'ct.nama_customer_toko', 
										 'bp.sku', 'bp.nama_barang', 'pt1.nama_pegawai_toko', 'pt2.nama_pegawai_toko'); 
	var $order_detail            = array('pm.id_penjualan_m' => 'desc');
	//--------------------------------------------------------------------------------------------------------------------------------------
	
	// Penjualan induk batal
	var $column_order_batal      = array('pdb.id_penjualan_b', 'pdb.id_penjualan_b', 
										 'pdb.no_penjualan', 'ct.nama_customer_toko', 
										 'bp.nama_barang', 'pdb.harga_satuan', 'discount_harga', 'harga_bersih',
										 'pdb.jumlah_beli', 'pdb.subtotal', 
										 'pdb.keterangan_batal', 'pegawai_batal', 'pdb.tanggal_pembatalan',
										 'pegawai_save', 'pdb.tanggal_pembuatan', 'pegawai_edit', 'pdb.tanggal_pembaharuan');
	var $column_search_batal     = array('pdb.no_penjualan', 'ct.nama_customer_toko', 'pdb.keterangan_batal', 
										 'bp.sku', 'bp.nama_barang',  'pt1.nama_pegawai_toko', 
										 'pt2.nama_pegawai_toko', 'pt3.nama_pegawai_toko'); 
	var $order_batal             = array('pdb.id_penjualan_b' => 'desc');
	//--------------------------------------------------------------------------------------------------------------------------------------
	
	// Penjualan debit
	var $column_order_debit      = array('pdk.id_penjualan_debit', 'pm.no_penjualan', 'ct.nama_customer_toko', 
										 'pdk.no_kartu_edc', 'ket.nama_bank', 'pdk.no_kartu_customer', 'pdk.jumlah_pembayaran', 
										 'pegawai_save', 'pdk.tanggal_pembuatan', 'pegawai_edit', 'pdk.tanggal_pembaharuan');
	var $column_search_debit     = array('pm.no_penjualan', 'ct.nama_customer_toko', 
										 'pdk.no_kartu_edc', 'ket.nama_bank', 'pdk.no_kartu_customer', 'pdk.jumlah_pembayaran', 
										 'pegawai_save', 'pdk.tanggal_pembuatan', 'pegawai_edit', 'pdk.tanggal_pembaharuan'); 
	var $order_debit             = array('pdk.id_penjualan_debit' => 'desc');
	//-------------------------------------------------------------------------------------------------------------------------------------
	
	// Penjualan perbarang
	var $column_order_perbarang  = array('bp.sku', 'bp.sku', 'bp.nama_barang', 
										 'transaksi_jual', 'transaksi_retur', 'transaksi_fix',
										 'jml_jual', 'jml_retur', 'jml_fix', 
										 'total_jual', 'total_retur', 'total_bersih', 
										 'pegawai_save', 'pd.tanggal_pembuatan', 'pegawai_edit', 'pd.tanggal_pembaharuan');
	var $column_search_perbarang = array('bp.sku', 'bp.nama_barang'); 
	var $order_perbarang         = array('jml_fix' => 'DESC');
	//--------------------------------------------------------------------------------------------------------------------------------------

	private function _get_laporan_query($tanggal_awal, $tanggal_akhir, $status_penjualan, $kode_toko)
	{		
		$this->db->select('	pm.id_penjualan_m, pm.no_penjualan, pm.tanggal, pm.status_penjualan, count(id_penjualan_d) AS jml_barang, 
							ct.foto, IFNULL(ct.nama_customer_toko,"-") AS nama_customer,
							pm.grand_total AS total, pm.debit, pm.tunai,
							ifnull(rpm.no_retur_penjualan, "-") as no_retur_penjualan, rpm.total_retur,
							(pm.grand_total - ifnull(rpm.total_retur, 0)) AS total_bersih,
							pt1.nama_pegawai_toko AS pegawai_save, pt2.nama_pegawai_toko AS pegawai_edit, 
							pm.tanggal_pembuatan, pm.tanggal_pembaharuan, pm.keterangan_lain 
						 ');
		$this->db->from('vamr4846_toko_mrc.penjualan_master_'.$kode_toko.' AS pm');
		$this->db->join('vamr4846_toko_mrc.penjualan_detail_'.$kode_toko.' as pd', 'pd.id_penjualan_m=pm.id_penjualan_m', 'LEFT');
		$this->db->join('vamr4846_toko_mrc.customer_toko as ct','ct.id_customer_toko=pm.id_customer_toko', 'LEFT');
		$this->db->join('vamr4846_toko_mrc.retur_penjualan_master_'.$kode_toko.' as rpm',
						'rpm.id_penjualan_m=pm.id_penjualan_m AND rpm.status_retur="SELESAI"', 'LEFT');
		$this->db->join('vamr4846_toko_mrc.pegawai_toko as pt1','pt1.id_pegawai_toko=pm.id_pegawai_pembuatan', 'LEFT');
		$this->db->join('vamr4846_toko_mrc.pegawai_toko as pt2','pt2.id_pegawai_toko=pm.id_pegawai_pembaharuan', 'LEFT');
		$this->db->where('DATE(pm.tanggal) BETWEEN "'. date('Y-m-d', strtotime($tanggal_awal)). '" AND 
												   "'. date('Y-m-d', strtotime($tanggal_akhir)).'" AND
						  pm.status_penjualan LIKE "%'.$status_penjualan.'%"
						');
		$this->db->group_by('pm.id_penjualan_m');


		$i = 0;
		foreach ($this->column_search as $item){
			if($_POST['search']['value']){
				if($i===0){
					$this->db->group_start();
					$this->db->like($item, $_POST['search']['value']);
				}else{
					$this->db->or_like($item, $_POST['search']['value']);
				}

				if(count($this->column_search) - 1 == $i)
					$this->db->group_end();
			}
			$i++;
		}

		if(isset($_POST['order'])){
			$this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		}else if(isset($this->order)){
			$order = $this->order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}

	private function _get_laporan_query_detail($tanggal_awal, $tanggal_akhir, $status_penjualan, $kode_toko)
	{		
		$this->db->select('	pm.id_penjualan_m, pm.no_penjualan, ifnull(rpm.no_retur_penjualan, "-") as no_retur_penjualan,  
							IFNULL(ct.nama_customer_toko,"-") AS nama_customer, ct.foto, pm.tanggal, pm.status_penjualan,
							bp.sku, bp.nama_barang, pd.harga_satuan, (pd.discount * pd.harga_satuan) AS discount_harga,
							(pd.harga_satuan - (pd.discount * pd.harga_satuan)) AS harga_bersih, 
							pd.jumlah_beli, rpd.jumlah_retur, pd.jumlah_beli - IFNULL(rpd.jumlah_retur, 0) AS jumlah_bersih,
							pd.subtotal, rpd.subtotal_retur, (pd.subtotal - IFNULL(rpd.subtotal_retur,0)) AS subtotal_bersih, 
							pt1.nama_pegawai_toko AS pegawai_save, pt2.nama_pegawai_toko AS pegawai_edit, 
							pd.tanggal_pembuatan, pd.tanggal_pembaharuan 
						 ');
		$this->db->from('vamr4846_toko_mrc.penjualan_detail_'.$kode_toko.' AS pd');
		$this->db->join('vamr4846_toko_mrc.penjualan_master_'.$kode_toko.' as pm', 'pd.id_penjualan_m=pm.id_penjualan_m', 'LEFT');
		$this->db->join('vamr4846_vama.barang_pusat as bp','bp.id_barang_pusat=pd.id_barang_pusat', 'LEFT');
		$this->db->join('vamr4846_toko_mrc.customer_toko as ct','ct.id_customer_toko=pm.id_customer_toko', 'LEFT');
		$this->db->join('vamr4846_toko_mrc.retur_penjualan_master_'.$kode_toko.' as rpm','rpm.id_penjualan_m=pm.id_penjualan_m', 'LEFT');
		$this->db->join('vamr4846_toko_mrc.retur_penjualan_detail_'.$kode_toko.' as rpd','rpd.id_retur_penjualan_m=rpm.id_retur_penjualan_m AND 
														 rpd.id_barang_pusat=pd.id_barang_pusat', 'LEFT');
		$this->db->join('vamr4846_toko_mrc.pegawai_toko as pt1','pt1.id_pegawai_toko=pm.id_pegawai_pembuatan', 'LEFT');
		$this->db->join('vamr4846_toko_mrc.pegawai_toko as pt2','pt2.id_pegawai_toko=pm.id_pegawai_pembaharuan', 'LEFT');
		$this->db->where('DATE(pm.tanggal) BETWEEN "'. date('Y-m-d', strtotime($tanggal_awal)). '" AND 
												   "'. date('Y-m-d', strtotime($tanggal_akhir)).'" AND
						  pm.status_penjualan LIKE "%'.$status_penjualan.'%"
						');

		$i = 0;
		foreach ($this->column_search_detail as $item){
			if($_POST['search']['value']){
				if($i===0){
					$this->db->group_start();
					$this->db->like($item, $_POST['search']['value']);
				}else{
					$this->db->or_like($item, $_POST['search']['value']);
				}

				if(count($this->column_search_detail) - 1 == $i)
					$this->db->group_end();
			}
			$i++;
		}

		if(isset($_POST['order'])){
			$this->db->order_by($this->column_order_detail[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		}else if(isset($this->order_detail)){
			$order = $this->order_detail;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}

	private function _get_laporan_query_batal($tanggal_awal, $tanggal_akhir, $status_penjualan, $kode_toko)
	{		
		$this->db->select('	pdb.id_penjualan_m, pdb.no_penjualan,   
							IFNULL(ct.nama_customer_toko,"-") AS nama_customer, ct.foto,
							bp.sku, bp.nama_barang, pdb.harga_satuan, (pdb.discount * pdb.harga_satuan) AS discount_harga,
							(pdb.harga_satuan - (pdb.discount * pdb.harga_satuan)) AS harga_bersih, pdb.jumlah_beli, pdb.subtotal,  
							pt1.nama_pegawai_toko AS pegawai_save, pt2.nama_pegawai_toko AS pegawai_edit, 
							pt3.nama_pegawai_toko As pegawai_batal,
							pdb.tanggal_pembuatan, pdb.tanggal_pembaharuan , pdb.tanggal_pembatalan, pdb.keterangan_batal
						 ');
		$this->db->from('vamr4846_toko_mrc.penjualan_detail_batal_'.$kode_toko.' AS pdb');
		$this->db->join('vamr4846_vama.barang_pusat as bp','bp.id_barang_pusat=pdb.id_barang_pusat', 'LEFT');
		$this->db->join('vamr4846_toko_mrc.customer_toko as ct','ct.id_customer_toko=pdb.id_customer_toko', 'LEFT');
		$this->db->join('vamr4846_toko_mrc.pegawai_toko as pt1','pt1.id_pegawai_toko=pdb.id_pegawai_pembuatan', 'LEFT');
		$this->db->join('vamr4846_toko_mrc.pegawai_toko as pt2','pt2.id_pegawai_toko=pdb.id_pegawai_pembaharuan', 'LEFT');
		$this->db->join('vamr4846_toko_mrc.pegawai_toko as pt3','pt3.id_pegawai_toko=pdb.id_pegawai_pembatalan', 'LEFT');
		$this->db->where('DATE(pdb.tanggal_pembatalan) BETWEEN "'. date('Y-m-d', strtotime($tanggal_awal)). '" AND 
															   "'. date('Y-m-d', strtotime($tanggal_akhir)).'"
						');

		$i = 0;
		foreach ($this->column_search_batal as $item){
			if($_POST['search']['value']){
				if($i===0){
					$this->db->group_start();
					$this->db->like($item, $_POST['search']['value']);
				}else{
					$this->db->or_like($item, $_POST['search']['value']);
				}

				if(count($this->column_search_batal) - 1 == $i)
					$this->db->group_end();
			}
			$i++;
		}

		if(isset($_POST['order'])){
			$this->db->order_by($this->column_order_batal[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		}else if(isset($this->order_batal)){
			$order = $this->order_batal;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}

	private function _get_laporan_query_debit($tanggal_awal, $tanggal_akhir, $status_penjualan, $kode_toko)
	{		
		$this->db->select('	pdk.id_penjualan_debit, pm.no_penjualan, pm.tanggal, IFNULL(ct.nama_customer_toko, "-") AS nama_customer, 
							ket.no_kartu_edc, ket.nama_bank, pdk.no_kartu_customer, pdk.jumlah_pembayaran, 
							pt1.nama_pegawai_toko AS pegawai_save, pdk.tanggal_pembuatan, 
							pt2.nama_pegawai_toko AS pegawai_edit, pdk.tanggal_pembaharuan 
						 ');
		$this->db->from('vamr4846_toko_mrc.penjualan_detail_kartu_'.$kode_toko.' AS pdk');
		$this->db->join('vamr4846_toko_mrc.penjualan_master_'.$kode_toko.' as pm', 'pdk.id_penjualan_m=pm.id_penjualan_m', 'LEFT');
		$this->db->join('vamr4846_toko_mrc.kartu_edc_toko as ket','ket.id_kartu_edc_toko=pdk.id_kartu_edc_toko', 'LEFT');
		$this->db->join('vamr4846_toko_mrc.customer_toko as ct','ct.id_customer_toko=pm.id_customer_toko', 'LEFT');
		$this->db->join('vamr4846_toko_mrc.pegawai_toko as pt1','pt1.id_pegawai_toko=pdk.id_pegawai_pembuatan', 'LEFT');
		$this->db->join('vamr4846_toko_mrc.pegawai_toko as pt2','pt2.id_pegawai_toko=pdk.id_pegawai_pembaharuan', 'LEFT');
		$this->db->where('DATE(pm.tanggal) BETWEEN "'. date('Y-m-d', strtotime($tanggal_awal)). '" AND 
												   "'. date('Y-m-d', strtotime($tanggal_akhir)).'" AND
						  pm.status_penjualan LIKE "%'.$status_penjualan.'%"
						');

		$i = 0;
		foreach ($this->column_search_debit as $item){
			if($_POST['search']['value']){
				if($i===0){
					$this->db->group_start();
					$this->db->like($item, $_POST['search']['value']);
				}else{
					$this->db->or_like($item, $_POST['search']['value']);
				}

				if(count($this->column_search_debit) - 1 == $i)
					$this->db->group_end();
			}
			$i++;
		}

		if(isset($_POST['order'])){
			$this->db->order_by($this->column_order_debit[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		}else if(isset($this->order_debit)){
			$order = $this->order_debit;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}

	private function _get_laporan_query_perbarang($tanggal_awal, $tanggal_akhir, $status_penjualan, $kode_toko)
	{		
		$this->db->select('	bp.sku, bp.nama_barang,
							count(pd.id_penjualan_d) AS transaksi_jual, IFNULL(count(rpd.id_retur_penjualan_m), "0") AS transaksi_retur, 
							count(pd.id_penjualan_d) - IFNULL(count(rpd.id_retur_penjualan_m), "0") AS transaksi_fix,
							sum(pd.jumlah_beli) AS jml_jual, IFNULL(sum(rpd.jumlah_retur), "0") AS jml_retur, 
							sum(pd.jumlah_beli) - IFNULL(sum(rpd.jumlah_retur), "0") AS jml_fix, 
							sum(pd.subtotal) AS total_jual, IFNULL(sum(rpd.subtotal_retur), "0") AS total_retur, 
							sum(pd.subtotal) - IFNULL(sum(rpd.subtotal_retur), "0") AS total_fix, 
							pt1.nama_pegawai_toko AS pegawai_save, pd.tanggal_pembuatan, 
							pt2.nama_pegawai_toko AS pegawai_edit, pd.tanggal_pembaharuan 
						 ');
		$this->db->from('vamr4846_toko_mrc.penjualan_detail_'.$kode_toko.' AS pd');
		$this->db->join('vamr4846_toko_mrc.penjualan_master_'.$kode_toko.' as pm', 'pd.id_penjualan_m=pm.id_penjualan_m', 'LEFT');
		$this->db->join('vamr4846_vama.barang_pusat as bp','bp.id_barang_pusat=pd.id_barang_pusat', 'LEFT');
		$this->db->join('vamr4846_toko_mrc.retur_penjualan_master_'.$kode_toko.' as rpm','rpm.id_penjualan_m=pm.id_penjualan_m', 'LEFT');
		$this->db->join('vamr4846_toko_mrc.retur_penjualan_detail_'.$kode_toko.' as rpd','rpd.id_retur_penjualan_m=rpm.id_retur_penjualan_m AND 
														 rpd.id_barang_pusat=pd.id_barang_pusat', 'LEFT');
		$this->db->join('vamr4846_toko_mrc.pegawai_toko as pt1','pt1.id_pegawai_toko=pm.id_pegawai_pembuatan', 'LEFT');
		$this->db->join('vamr4846_toko_mrc.pegawai_toko as pt2','pt2.id_pegawai_toko=pm.id_pegawai_pembaharuan', 'LEFT');
		$this->db->where('DATE(pm.tanggal) BETWEEN "'. date('Y-m-d', strtotime($tanggal_awal)). '" AND 
												   "'. date('Y-m-d', strtotime($tanggal_akhir)).'"
						');
		$this->db->group_by('pd.id_barang_pusat');

		$i = 0;
		foreach ($this->column_search_perbarang as $item){
			if($_POST['search']['value']){
				if($i===0){
					$this->db->group_start();
					$this->db->like($item, $_POST['search']['value']);
				}else{
					$this->db->or_like($item, $_POST['search']['value']);
				}

				if(count($this->column_search_perbarang) - 1 == $i)
					$this->db->group_end();
			}
			$i++;
		}

		if(isset($_POST['order'])){
			$this->db->order_by($this->column_order_perbarang[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		}else if(isset($this->order_perbarang)){
			$order = $this->order_perbarang;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}

	function get_laporan($perintah, $tanggal_awal, $tanggal_akhir, $status_penjualan, $kode_toko)
	{
		$this->$perintah($tanggal_awal, $tanggal_akhir, $status_penjualan, $kode_toko);
		if($_POST['length'] != -1)
		$this->db->limit($_POST['length'], $_POST['start']);
		$query = $this->db->get();
		return $query->result();
	}

	function count_filtered($perintah, $tanggal_awal, $tanggal_akhir, $status_penjualan, $kode_toko)
	{
		$this->$perintah($tanggal_awal, $tanggal_akhir, $status_penjualan, $kode_toko);
		$query = $this->db->get();
		return $query->num_rows();
	}

	public function count_all($table, $tanggal_awal, $tanggal_akhir, $kode_toko)
	{
		$this->db->from($table);
		if($table == 'vamr4846_toko_mrc.penjualan_master_'.$kode_toko.''){
			$this->db->where('DATE(tanggal) BETWEEN "'. date('Y-m-d', strtotime($tanggal_awal)). '" AND 
							  		  				"'. date('Y-m-d', strtotime($tanggal_akhir)).'"');
		}else if($table == 'vamr4846_toko_mrc.penjualan_detail_'.$kode_toko.' AS pd'){
			$this->db->join('vamr4846_toko_mrc.penjualan_master_'.$kode_toko.' AS pm', 'pm.id_penjualan_m=pd.id_penjualan_m', 'LEFT');
			$this->db->where('DATE(pm.tanggal) BETWEEN "'. date('Y-m-d', strtotime($tanggal_awal)). '" AND 
													   "'. date('Y-m-d', strtotime($tanggal_akhir)).'"');
		}else if($table == 'vamr4846_toko_mrc.penjualan_detail_batal_'.$kode_toko.''){
			$this->db->where('DATE(tanggal_pembatalan) BETWEEN "'. date('Y-m-d', strtotime($tanggal_awal)). '" AND 
															   "'. date('Y-m-d', strtotime($tanggal_akhir)).'"');
		}else if($table == 'vamr4846_toko_mrc.penjualan_detail_kartu_'.$kode_toko.' AS pdk'){
			$this->db->join('vamr4846_toko_mrc.penjualan_master_'.$kode_toko.' AS pm', 'pm.id_penjualan_m=pdk.id_penjualan_m', 'LEFT');
			$this->db->where('DATE(pm.tanggal) BETWEEN "'. date('Y-m-d', strtotime($tanggal_awal)). '" AND 
													   "'. date('Y-m-d', strtotime($tanggal_akhir)).'"');
		}else if($table == 'vamr4846_toko_mrc.penjualan_detail_'.$kode_toko.' AS pd2'){
			$this->db->select('pd2.id_barang_pusat');
			$this->db->join('vamr4846_toko_mrc.penjualan_master_'.$kode_toko.' AS pm', 'pm.id_penjualan_m=pd2.id_penjualan_m', 'LEFT');
			$this->db->where('DATE(pm.tanggal) BETWEEN "'. date('Y-m-d', strtotime($tanggal_awal)). '" AND 
													   "'. date('Y-m-d', strtotime($tanggal_akhir)).'"');
			$this->db->group_by('pd2.id_barang_pusat');
		}
		return $this->db->count_all_results();
	}

	function no_penjualan_baru($tahun_sekarang, $bulan_sekarang, $id_pegawai, $kode_toko) {
	    $this->db->select('CAST(RIGHT(no_penjualan, 4)AS INT) + 1 AS no_baru');
	    $this->db->from('vamr4846_toko_mrc.penjualan_master_'.$kode_toko.'');
	    $this->db->where(array(  
			'YEAR(tanggal)'        => $tahun_sekarang,
			'MONTH(tanggal)'       => $bulan_sekarang,
			'id_pegawai_pembuatan' => $id_pegawai
		));
	    $this->db->order_by('tanggal_pembuatan','DESC');
	    $query = $this->db->get();
	    return $query->row();
	}

	function insert_master($no_penjualan, $tanggal, $id_pegawai, $id_customer_toko, 
						   $total, $biaya_lain, $ppn, $grand_total, 
						   $debit, $tunai, $catatan, $kode_toko)
	{
		$dt = array(	
			'no_penjualan'         => $no_penjualan,
			'tanggal'              => $tanggal,
			'total'                => $total,
			'biaya_lain'           => $biaya_lain,
			'ppn'                  => $ppn,
			'grand_total'          => $grand_total,
			'debit'                => $debit,
			'tunai'                => $tunai,
			'keterangan_lain'      => $catatan,
			'id_customer_toko'    => (empty($id_customer_toko)) ? NULL : $id_customer_toko,
			'id_pegawai_pembuatan' => (empty($id_pegawai)) ? NULL : $id_pegawai,
			'tanggal_pembuatan'    => date('Y-m-d H:i:s')
		);
		return $this->db->insert('vamr4846_toko_mrc.penjualan_master_'.$kode_toko.'', $dt);
	}

	function update_master($id_penjualan_m, $id_pegawai, $id_customer_toko,
						   $total, $biaya_lain, $ppn, $grand_total, 
						   $debit, $tunai, $tanggal, $catatan, $status_penjualan, $kode_toko)
	{
		$dt = array(	
			'total'                  => $total,
			'biaya_lain'             => $biaya_lain,
			'ppn'                    => $ppn,
			'grand_total'            => $grand_total,
			'debit'                  => $debit,
			'tunai'                  => $tunai,
			'tanggal'                => $tanggal,
			'keterangan_lain'        => $catatan,
			'status_penjualan'       => $status_penjualan,
			'id_customer_toko'      => (empty($id_customer_toko)) ? NULL : $id_customer_toko,
			'id_pegawai_pembaharuan' => (empty($id_pegawai)) ? NULL : $id_pegawai
		);
		$where = array('id_penjualan_m'	=> $id_penjualan_m);
		return $this->db->update('vamr4846_toko_mrc.penjualan_master_'.$kode_toko.'', $dt, $where);
	}

	public function cek_referensi_poin()
	{
		$this->db->select('*');
		$this->db->from('vamr4846_vama.referensi_jumlah_poin');
		$this->db->limit('1');
		$query = $this->db->get();
		return $query->row();
	}

	public function cek_poin($id_penjualan_m, $no_penjualan)
	{
		$this->db->select('*');
		$this->db->from('vamr4846_vama.poin_penjualan_toko');
		$this->db->where(array(
			'id_penjualan_m' => $id_penjualan_m,
			'no_penjualan'   => $no_penjualan
		));
		$query = $this->db->get();
		return $query->row();
	}

	function insert_poin($id_penjualan_m, $no_penjualan, $id_customer_toko, $total, $jumlah_poin, $id_pegawai){
		$dt = array(	
			'id_penjualan_m'       => $id_penjualan_m,
			'no_penjualan'         => $no_penjualan,
			'id_customer_toko'    => (empty($id_customer_toko)) ? NULL : $id_customer_toko,
			'total'                => $total,
			'jumlah_poin'          => $jumlah_poin,
			'id_pegawai_pembuatan' => (empty($id_pegawai)) ? NULL : $id_pegawai,
			'tanggal_pembuatan'    => date('Y-m-d H:i:s')
		);
		return $this->db->insert('vamr4846_vama.poin_penjualan_toko', $dt);
	}

	function update_poin($id_penjualan_m, $no_penjualan, $id_customer_toko, $total, $jumlah_poin, $id_pegawai)
	{
		$dt = array(	
			'id_customer_toko'      => (empty($id_customer_toko)) ? NULL : $id_customer_toko,
			'total'                  => $total,
			'jumlah_poin'            => $jumlah_poin,
			'id_pegawai_pembaharuan' => (empty($id_pegawai)) ? NULL : $id_pegawai
		);
		$where = array('
			id_penjualan_m' => $id_penjualan_m,
			'no_penjualan'  => $no_penjualan
		);
		return $this->db->update('vamr4846_vama.poin_penjualan_toko', $dt, $where);
	}

	function delete_poin($id_penjualan_m, $no_penjualan)
	{
		return $this->db
			->where(array(
				'id_penjualan_m' => $id_penjualan_m,
				'no_penjualan'   => $no_penjualan
			))
			->delete('vamr4846_vama.poin_penjualan_toko');
	}

	public function update_jumlah_cetak($where, $data, $kode_toko)
	{
		$this->db->update('vamr4846_toko_mrc.penjualan_master_'.$kode_toko.'', $data, $where);
		return $this->db->affected_rows();
	}

	public function update_status($where, $data, $kode_toko)
	{
		$this->db->update('vamr4846_toko_mrc.penjualan_master_'.$kode_toko.'', $data, $where);
		return $this->db->affected_rows();
	}
	
	function update_debit($id_penjualan_m, $id_pegawai, $debit, $tunai, $kode_toko)
	{
		$dt = array(	
			'debit'                  => $debit,
			'tunai'                  => $tunai,
			'id_pegawai_pembaharuan' => (empty($id_pegawai)) ? NULL : $id_pegawai
		);
		$where = array('id_penjualan_m'	=> $id_penjualan_m);
		return $this->db->update('vamr4846_toko_mrc.penjualan_master_'.$kode_toko.'', $dt, $where);
	}

	function get_id($no_penjualan, $kode_toko)
	{
		$this->db->select('*');
		$this->db->from('vamr4846_toko_mrc.penjualan_master_'.$kode_toko.'');
		$this->db->where('no_penjualan', $no_penjualan);
		$query = $this->db->get();
		return $query->row();
	}

	function get_by_id($id_penjualan_m, $kode_toko)
	{
		$this->db->select('pm.*, 
						   IFNULL(ct.kode_customer_toko, "-") AS kode_customer_toko, 
						   IFNULL(ct.nama_customer_toko, "-") AS nama_customer_toko, 
						   IFNULL(ct.handphone1, "-") AS handphone1');
		$this->db->from('vamr4846_toko_mrc.penjualan_master_'.$kode_toko.' As pm');
		$this->db->join('vamr4846_toko_mrc.customer_toko AS ct', 'ct.id_customer_toko=pm.id_customer_toko', 'LEFT');
		$this->db->where('id_penjualan_m', $id_penjualan_m);
		$query = $this->db->get();
		return $query->row();
	}

	function get_by_no_penjualan($no_penjualan, $kode_toko)
	{
		$this->db->select('pm.*, count(pd.id_penjualan_d) AS jumlah_jual,
						   IFNULL(rpm.id_retur_penjualan_m, "") AS id_retur_penjualan_m,
						   IFNULL(rpm.no_retur_penjualan, "-") AS no_retur_penjualan,
						   IFNULL(rpm.total_retur, "0") AS total_retur,
						   IFNULL(ct.kode_customer_toko, "-") AS kode_customer_toko, 
						   IFNULL(ct.nama_customer_toko, "-") AS nama_customer_toko, 
						   IFNULL(ct.tipe_customer_toko, "-") AS tipe_customer_toko, 
						   IFNULL(ct.handphone1, "-") AS handphone1');
		$this->db->from('vamr4846_toko_mrc.penjualan_master_'.$kode_toko.' As pm');
		$this->db->join('vamr4846_toko_mrc.retur_penjualan_master_'.$kode_toko.' As rpm', 'rpm.id_penjualan_m=pm.id_penjualan_m', 'LEFT');
		$this->db->join('vamr4846_toko_mrc.penjualan_detail_'.$kode_toko.' AS pd', 'pm.id_penjualan_m=pd.id_penjualan_m', 'LEFT');
		$this->db->join('vamr4846_toko_mrc.customer_toko AS ct', 'ct.id_customer_toko=pm.id_customer_toko', 'LEFT');
		$this->db->where('pm.no_penjualan', $no_penjualan);
		$query = $this->db->get();
		return $query->row();
	}

	function get_master($id_penjualan_m, $kode_toko)
	{
		$this->db->select('pm.*, 
						  COUNT(pd.id_penjualan_d) AS jumlah_barang,
						  IFNULL(ct.id_customer_toko,"0") AS id_customer_toko,
						  IFNULL(ct.kode_customer_toko,"-") AS kode_customer_toko, 
						  IFNULL(ct.nama_customer_toko,"-") AS nama_customer_toko, 
						  IFNULL(ct.email,"-") AS email_customer_toko, 
						  IFNULL(ct.alamat_customer_toko,"-") AS alamat_customer_toko,
						  IFNULL(ct.handphone1,"-") AS handphone1,
						  pt.nama_pegawai_toko');
		$this->db->from('vamr4846_toko_mrc.penjualan_master_'.$kode_toko.' AS pm');
		$this->db->join('vamr4846_toko_mrc.penjualan_detail_'.$kode_toko.' as pd', 'pm.id_penjualan_m=pd.id_penjualan_m', 'LEFT');
		$this->db->join('vamr4846_toko_mrc.customer_toko as ct', 'pm.id_customer_toko=ct.id_customer_toko', 'LEFT');
		$this->db->join('vamr4846_toko_mrc.pegawai_toko as pt', 'pm.id_pegawai_pembuatan=pt.id_pegawai_toko', 'LEFT');
		$this->db->where('pm.id_penjualan_m', $id_penjualan_m);
		$query = $this->db->get();
		return $query->row();
	}

	function get_jumlah_jual($id_penjualan_m, $kode_toko)
	{
		$this->db->select('count(*) as jumlah_jual');
		$this->db->from('vamr4846_toko_mrc.penjualan_detail_'.$kode_toko.'');
		$this->db->where('id_penjualan_m', $id_penjualan_m);
		$query = $this->db->get();
		return $query->row();
	}
	
	function get_jumlah_batal($id_penjualan_m, $kode_toko)
	{
		$this->db->select('id_penjualan_m, count(id_penjualan_b) AS jumlah_barang');
		$this->db->from('vamr4846_toko_mrc.penjualan_detail_batal_'.$kode_toko.'');
		$this->db->where('id_penjualan_m', $id_penjualan_m);
		$query = $this->db->get();
		return $query->row();
	}

	function hapus_penjualan_master($id_penjualan, $kode_toko)
	{
		return $this->db
			->where('id_penjualan_m', $id_penjualan)
			->delete('vamr4846_toko_mrc.penjualan_master_'.$kode_toko.'');
	}

	function hapus_transaksi($id_penjualan_m, $no_penjualan, $id_customer_toko, $keterangan_batal, $id_pegawai_pembatalan, $kode_toko)
	{
		// Filter data penjualan detail berdasarkan id penjualan master
		$loop = $this->db
					 ->select('*')
					 ->where('id_penjualan_m', $id_penjualan_m)
					 ->get('vamr4846_toko_mrc.penjualan_detail_'.$kode_toko.'');

		foreach($loop->result() as $b){
			// Kembalikan stok
			$sql_update_stok = "UPDATE `vamr4846_toko_mrc.barang_toko_".$kode_toko."` SET `total_stok` = `total_stok` + ".$b->jumlah_beli." 
								WHERE `id_barang_pusat` = '".$b->id_barang_pusat."'";
			$this->db->query($sql_update_stok);

			// Simpan penjualan detail ke penjualan detail batal
			$sql_insert_batal = "
					INSERT INTO vamr4846_toko_mrc.penjualan_detail_batal_".$kode_toko." 
						(id_penjualan_d, id_penjualan_m, no_penjualan, 
						 id_customer_toko, id_barang_pusat, 
						 modal, harga_satuan, 
						 discount, jumlah_beli, subtotal, 
						 id_pegawai_pembuatan, id_pegawai_pembaharuan, id_pegawai_pembatalan, 
						 tanggal_pembuatan, tanggal_pembaharuan, keterangan_batal) 
					VALUES
						('".$b->id_penjualan_d."', '".$b->id_penjualan_m."', '".$no_penjualan."', '".$id_customer_toko."',
						 '".$b->id_barang_pusat."', '".$b->modal."', '".$b->harga_satuan."', 
						 '".$b->discount."', '".$b->jumlah_beli."', '".$b->subtotal."', 
						 '".$b->id_pegawai_pembuatan."', '".$b->id_pegawai_pembaharuan."', '".$id_pegawai_pembatalan."', 
						 '".$b->tanggal_pembuatan."', '".$b->tanggal_pembaharuan."', '".$keterangan_batal."') 
					";
			$this->db->query($sql_insert_batal);
		}

		// Hapus penjualan detail dan penjualan master
		$this->db->where('id_penjualan_m', $id_penjualan_m)->delete('vamr4846_toko_mrc.penjualan_detail_'.$kode_toko.'');
		return $this->db
					->where('id_penjualan_m', $id_penjualan_m)
					->delete('vamr4846_toko_mrc.penjualan_master_'.$kode_toko.'');
	}

	function get_uang($tanggal_awal, $tanggal_akhir, $status_penjualan, $kode_toko)
	{
		$this->db->select('	
			COUNT(pm.id_penjualan_m)AS jml_transaksi, 
			SUM(pm.debit) AS total_debit, SUM(pm.tunai) AS total_tunai, SUM(pm.grand_total) AS total_transaksi,
			SUM(pm.ppn)AS total_ppn, SUM(pm.biaya_lain) AS total_biaya_lain, 
			(SUM(pm.grand_total) - (SUM(pm.biaya_lain)+SUM(pm.ppn)) - IFNULL(SUM(rpm.total_retur),0)) AS total_bersih
		');
		$this->db->from('vamr4846_toko_mrc.penjualan_master_'.$kode_toko.' as pm');
		$this->db->join('vamr4846_toko_mrc.retur_penjualan_master_'.$kode_toko.' as rpm',
						'rpm.id_penjualan_m=pm.id_penjualan_m AND rpm.status_retur="SELESAI"','LEFT');
		$this->db->where('DATE(tanggal) BETWEEN "'. date('Y-m-d', strtotime($tanggal_awal)). '" AND 
												"'. date('Y-m-d', strtotime($tanggal_akhir)).'"
						');
		$this->db->where('pm.status_penjualan', $status_penjualan);
		$query = $this->db->get();
		return $query->row();
	}

	function get_modal($tanggal_awal, $tanggal_akhir, $kode_toko)
	{
		$this->db->select('
			ROUND(SUM(pd.harga_satuan*pd.jumlah_beli),0) AS total_harga_satuan,
			ROUND(SUM((pd.harga_satuan*pd.discount)*pd.jumlah_beli),0) AS total_discount,
			ROUND(SUM((pd.harga_satuan-(pd.harga_satuan*pd.discount))*pd.jumlah_beli),0) AS total_harga_fix,
			ROUND(SUM(pd.modal*pd.jumlah_beli),0) AS total_modal,
			ROUND(SUM((pd.harga_satuan-(pd.harga_satuan*pd.discount))*pd.jumlah_beli)-SUM(pd.modal*pd.jumlah_beli),0) AS total_keuntungan
		');
		$this->db->from('vamr4846_toko_mrc.penjualan_master_'.$kode_toko.' AS pm');
		$this->db->join('vamr4846_toko_mrc.penjualan_detail_'.$kode_toko.' AS pd', 'pm.id_penjualan_m=pd.id_penjualan_m', 'LEFT');
		$this->db->join('vamr4846_toko_mrc.retur_penjualan_master_'.$kode_toko.' as rpm',
						'rpm.id_penjualan_m=pm.id_penjualan_m AND rpm.status_retur="SELESAI"','LEFT');
		$this->db->join('vamr4846_toko_mrc.retur_penjualan_detail_'.$kode_toko.' as rpd','rpd.id_penjualan_d=pd.id_penjualan_d','LEFT');
		$this->db->where('DATE(tanggal) BETWEEN "'. date('Y-m-d', strtotime($tanggal_awal)). '" AND 
												"'. date('Y-m-d', strtotime($tanggal_akhir)).'"
						');
		$this->db->where('status_penjualan','SELESAI');
		$query = $this->db->get();
		return $query->row();
	}

	function get_total_toko_online($tanggal_awal, $tanggal_akhir, $kode_toko, $perintah)
	{
		$this->db->select('	
			COUNT(pm.id_penjualan_m)AS jml_transaksi, 
			SUM(pm.debit) AS total_debit, SUM(pm.tunai) AS total_tunai, SUM(pm.grand_total) AS total_transaksi,
			SUM(pm.ppn)AS total_ppn, SUM(pm.biaya_lain) AS total_biaya_lain, 
			(SUM(pm.grand_total) - (SUM(pm.biaya_lain)+SUM(pm.ppn)) - IFNULL(SUM(rpm.total_retur),0)) AS total_bersih
		');
		$this->db->from('vamr4846_toko_mrc.penjualan_master_'.$kode_toko.' as pm');
		$this->db->join('vamr4846_toko_mrc.retur_penjualan_master_'.$kode_toko.' as rpm',
						'rpm.id_penjualan_m=pm.id_penjualan_m AND rpm.status_retur="SELESAI"','LEFT');
		$this->db->where('DATE(tanggal) BETWEEN "'. date('Y-m-d', strtotime($tanggal_awal)). '" AND 
												"'. date('Y-m-d', strtotime($tanggal_akhir)).'"
						');
		$this->db->where(array(
			'pm.status_penjualan'               => 'SELESAI',
			'pm.id_customer_toko '.$perintah.'' => '7'
		));
		$query = $this->db->get();
		return $query->row();
	}

	function get_total_penjualan_perbulan($tanggal_awal, $tanggal_akhir, $status_penjualan, $kode_toko)
	{
		$this->db->select('
			IFNULL(MIN(DATE_FORMAT(tanggal, "%d-%m-%y")),"00-00-00") AS tanggal_awal,
			IFNULL(MAX(DATE_FORMAT(tanggal, "%d-%m-%y")),"00-00-00") AS tanggal_akhir,
			IFNULL(COUNT(id_penjualan_m),0) AS jml_transaksi, 
			IFNULL(SUM(grand_total),0) AS grand_total
		');
		$this->db->from('vamr4846_toko_mrc.penjualan_master_'.$kode_toko.'');
		$this->db->where('
							DATE(tanggal) BETWEEN 	"'. date('Y-m-d', strtotime($tanggal_awal)). '" AND 
												  	"'. date('Y-m-d', strtotime($tanggal_akhir)).'" AND
							status_penjualan 		= "'.$status_penjualan.'"
						');
		$query = $this->db->get();
		return $query->row();
	}

	function get_penjualan_by_perintah($kode_toko, $status_penjualan, $tanggal_awal, $tanggal_akhir)
	{
		$sql = "
			SELECT 
				id_penjualan_m, no_penjualan, 
				IFNULL(nama_customer_toko,'UMUM') AS nama_customer_toko, 
				ct.foto, IFNULL(ct.email,'-') AS email,
				DATE_FORMAT(tanggal, '%d-%m-%Y') AS tanggal, 
				REPLACE(FORMAT(grand_total,0), ',', '.') AS grand_total, 
				pm.status_penjualan
			FROM 
				vamr4846_toko_mrc.penjualan_master_".$kode_toko." AS pm
			LEFT JOIN 
				vamr4846_toko_mrc.customer_toko AS ct ON pm.id_customer_toko=ct.id_customer_toko
			WHERE 
				pm.status_penjualan = '".$this->db->escape_like_str($status_penjualan)."' AND
				pm.jml_cetak = '0'
			ORDER BY pm.tanggal_pembaharuan DESC 
		";
		return $this->db->query($sql);
	}

	function get_chart_penjualan_perbulan($id_toko, $kode_toko, $status_penjualan, $tanggal_awal, $tanggal_akhir)
	{
		$sql ="
				SELECT DATE_FORMAT(tgl, '%d-%m-%Y') AS tanggal, COUNT(id_penjualan_m) AS jml_transaksi, ROUND(IFNULL(SUM(grand_total), '0')/1000000, 2) AS total
				FROM
				(
				    SELECT
				        MAKEDATE(YEAR(NOW()),1) +
				        INTERVAL (MONTH(NOW())-1) MONTH +
				        INTERVAL daynum DAY tgl
				    FROM
				    (
				        SELECT t*10+u daynum
				        FROM
				            (SELECT 0 t UNION SELECT 1 UNION SELECT 2 UNION SELECT 3) A,
				            (SELECT 0 u UNION SELECT 1 UNION SELECT 2 UNION SELECT 3
				            UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7
				            UNION SELECT 8 UNION SELECT 9) B
				        ORDER BY daynum
				    ) AA
				) vtgl
				LEFT JOIN vamr4846_toko_mrc.penjualan_master_".$kode_toko." AS pm ON 
					vtgl.tgl=pm.tanggal AND
					pm.status_penjualan ='".$this->db->escape_like_str($status_penjualan)."'
				WHERE 
					MONTH(tgl) = MONTH(NOW())
				GROUP BY vtgl.tgl
				ORDER BY 1 ASC
		";

		$query = $this->db->query($sql);
		return $query->result();
	}

	function get_top_customer($tanggal_awal, $tanggal_akhir, $kode_toko)
	{
		$sql = "
			SELECT 
				IFNULL(nama_customer_toko,'UMUM') AS nama_customer_toko,
				foto, IFNULL(email,'-') AS email,
				COUNT(id_penjualan_m) AS jml_transaksi, no_penjualan, 				 
				MIN(DATE_FORMAT(tanggal, '%d-%m-%Y')) AS tanggal_awal,
				MAX(DATE_FORMAT(tanggal, '%d-%m-%Y')) AS tanggal_akhir, 
				SUM(grand_total) AS total
			FROM 
				vamr4846_toko_mrc.penjualan_master_".$kode_toko." AS pm
			LEFT JOIN 
				vamr4846_toko_mrc.customer_toko AS ct 
				ON pm.id_customer_toko=ct.id_customer_toko
			WHERE 
				status_penjualan       = 'SELESAI' AND
				DATE(pm.tanggal) BETWEEN '".$tanggal_awal."' AND '".$tanggal_akhir."'
			GROUP BY pm.id_customer_toko
			ORDER BY total DESC
			LIMIT 10
		";
		return $this->db->query($sql);
	}

	function get_top_produk($tanggal_awal, $tanggal_akhir, $kode_toko)
	{
		$sql = "
			SELECT 
				sku, nama_barang, foto, SUM(pd.jumlah_beli) AS jml_qty,
				COUNT(pd.id_penjualan_d) AS jml_transaksi, no_penjualan, 				 
				MIN(DATE_FORMAT(tanggal, '%d-%m-%Y')) AS tanggal_awal,
				MAX(DATE_FORMAT(tanggal, '%d-%m-%Y')) AS tanggal_akhir, 
				SUM((pd.harga_satuan-(pd.harga_satuan*pd.discount))*pd.jumlah_beli) AS total
			FROM 
				vamr4846_toko_mrc.penjualan_detail_".$kode_toko." AS pd
			LEFT JOIN 
				vamr4846_toko_mrc.penjualan_master_".$kode_toko." AS pm 
				ON pm.id_penjualan_m=pd.id_penjualan_m
			LEFT JOIN 
				vamr4846_vama.barang_pusat AS bp 
				ON pd.id_barang_pusat=bp.id_barang_pusat
			WHERE 
				status_penjualan       = 'SELESAI' AND
				DATE(pm.tanggal) BETWEEN '".$tanggal_awal."' AND '".$tanggal_akhir."'
			GROUP BY pd.id_barang_pusat
			ORDER BY total DESC
			LIMIT 10
		";
		return $this->db->query($sql);
	}
}
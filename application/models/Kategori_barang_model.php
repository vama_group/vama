<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kategori_barang_model extends MY_Model {

	var $table 			= 'vamr4846_vama.kategori_barang';
	var $column_order 	= array(
		'kb.id_kategori_barang','kb.id_kategori_barang','jb.nama_jenis','kb.kode_kategori','kb.nama_kategori',
		'pp1.nama_pegawai','kb.tanggal_pembuatan','pp2.nama_pegawai','kb.tanggal_pembaharuan'
	);
	var $column_search 	= array('kode_kategori','nama_kategori','kb.tanggal_pembuatan','kb.tanggal_pembaharuan');	
	var $order 			= array('id_kategori_barang' => 'desc');

	private function _get_datatables_query()
	{
		$this->db->select('kb.*, jb.nama_jenis, 
						   pp1.nama_pegawai AS pegawai_save, pp2.nama_pegawai As pegawai_edit');
		$this->db->from('vamr4846_vama.kategori_barang AS kb');
		$this->db->join('vamr4846_vama.jenis_barang AS jb','jb.kode_jenis=kb.kode_jenis','LEFT');
		$this->db->join('vamr4846_vama.pegawai_pusat AS pp1', 'pp1.id_pegawai=kb.id_pegawai_pembuatan', 'LEFT');
		$this->db->join('vamr4846_vama.pegawai_pusat AS pp2', 'pp2.id_pegawai=kb.id_pegawai_pembaharuan', 'LEFT');
		$this->db->where('kb.status_hapus', 'TIDAK');

		$i = 0;
		foreach ($this->column_search as $item){
			if($_POST['search']['value']){
				if($i===0){
					$this->db->group_start();
					$this->db->like($item, $_POST['search']['value']);
				}else{
					$this->db->or_like($item, $_POST['search']['value']);
				}

				if(count($this->column_search) - 1 == $i)
					$this->db->group_end();
			}
			$i++;
		}
		
		if(isset($_POST['order'])){
			$this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		}else if(isset($this->order)){
			$order = $this->order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}

	function get_datatables()
	{
		$this->_get_datatables_query();
		if($_POST['length'] != -1)
		$this->db->limit($_POST['length'], $_POST['start']);
		$query = $this->db->get();
		return $query->result();
	}

	function count_filtered()
	{
		$this->_get_datatables_query();
		$query = $this->db->get();
		return $query->num_rows();
	}

	public function count_all()
	{
		$this->db->from($this->table);
		$this->db->where('status_hapus', 'TIDAK');
		return $this->db->count_all_results();
	}

	public function ambil_kategori_barang($keyword, $kodejenis)
	{
		$sql = "
			SELECT kode_kategori, nama_kategori
			FROM 
				vamr4846_vama.kategori_barang 
			WHERE 
				( 
					status_hapus 	= 'TIDAK' AND
					kode_jenis 		= '".$this->db->escape_like_str($kodejenis)."' AND
					nama_kategori LIKE '%".$this->db->escape_like_str($keyword)."%' 
				) 
		";
		return $this->db->query($sql);
	}

	public function cari_kategori_barang($nama_kategori, $kode_jenis){
		$this->db->select('kode_kategori, nama_kategori');
		$this->db->from('vamr4846_vama.kategori_barang AS kb');
		$this->db->where('kb.status_hapus', 'TIDAK');
		$this->db->where('kb.kode_jenis', $kode_jenis);
		$this->db->like('kb.nama_kategori', $nama_kategori);
		$this->db->order_by('kb.nama_kategori','ASC');
		$query=$this->db->get();
		return $query->result();
	}

	public function listing() {
		$this->db->select('kb.*, jb.nama_jenis');
		$this->db->from('vamr4846_vama.kategori_barang AS kb');
		$this->db->join('vamr4846_vama.jenis_barang AS jb','jb.kode_jenis=kb.kode_jenis','LEFT');
		$this->db->where('kb.status_hapus', 'TIDAK');
		$this->db->order_by('id_kategori_barang','DESC');
		$query=$this->db->get();
		return $query->result();
	}

	// Filter kategori barang
	public function filter_kategori() {
		$this->db->select('*');
		$this->db->from('vamr4846_vama.kategori_barang');
		$this->db->where('kode_jenis',$kode_jenis);
		$this->db->order_by('id_kategori_barang','DESC');
		$query=$this->db->get();
		return $query->result();
	}

	// Dapatkan kode terakhir
	public function akhir($kode_jenis) {
		$this->db->select('count(*) as jumlah_jenis');
		$this->db->from('vamr4846_vama.kategori_barang');
		$this->db->where('kode_jenis',$kode_jenis);
		$this->db->order_by('id_kategori_barang','DESC');
		$query=$this->db->get();
		return $query->row();
	}

	public function get_by_id($id_kategori_barang)
	{
		$this->db->select('kb.*, jb.nama_jenis');
		$this->db->from('vamr4846_vama.kategori_barang AS kb');
		$this->db->join('vamr4846_vama.jenis_barang AS jb','jb.kode_jenis=kb.kode_jenis','LEFT');
		$this->db->where(array(
			'id_kategori_barang' => $id_kategori_barang,
			'kb.status_hapus'    => 'TIDAK'
		));
		$query = $this->db->get();
		return $query->row();
	}

	public function get_by_nama($nama_kategori_barang)
	{
		$this->db->select('kb.*, jb.nama_jenis');
		$this->db->from('vamr4846_vama.kategori_barang AS kb');
		$this->db->join('vamr4846_vama.jenis_barang AS jb','jb.kode_jenis=kb.kode_jenis','LEFT');
		$this->db->where(array(
			'kb.nama_kategori' => $nama_kategori_barang,
			'kb.status_hapus'  => 'TIDAK'
		));
		$query = $this->db->get();
		return $query->row();
	}

	public function save($data)
	{
		$this->db->insert($this->table, $data);
		return $this->db->insert_id();
	}

	public function update($where, $data)
	{
		$this->db->update($this->table, $data, $where);
		return $this->db->affected_rows();
	}

	public function delete_by_id($id_kategori_barang)
	{
		$this->db->where('id_kategori_barang', $id_kategori_barang);
		$this->db->delete($this->table);
	}

	public function update_status_hapus($id_kategori_barang, $id_pegawai)
	{
		$sql = "UPDATE vamr4846_vama.kategori_barang 
				SET status_hapus = 'IYA', id_pegawai_pembaharuan = '".$id_pegawai."' 
				WHERE id_kategori_barang = '".$id_kategori_barang."'";
		return $this->db->query($sql);
	}
}

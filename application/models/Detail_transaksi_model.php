<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Detail_transaksi_model extends MY_Model{
	// Awal detail transaksi - stok jual
	// ---------------------------------------------------------------------------------------------------------------------------------
	// Detail transaksi stok opname
	var $column_order_so     = array('so.jumlah_so_sebelumnya', 'so.jumlah_so', 'so.catatan', 'pp1.nama_pegawai', 'pp2.nama_pegawai');
	var $column_search_so    = array('so.catatan', 'pp1.nama_pegawai', 'pp2.nama_pegawai');
	var $order_so            = array('so.id_stok_opname_pusat' => 'desc');
	// ---------------------------------------------------------------------------------------------------------------------------------
	
	// Detail transaksi pembelian
	var $column_order_pb     = array('pm.no_pembelian', 'pm.no_faktur', 'pd.jumlah_beli', 'pm.catatan', 
									 'pp1.nama_pegawai', 'pp2.nama_pegawai');
	var $column_search_pb    = array('pm.no_pembelian', 'pm.no_faktur', 'pm.keterangan_lain', 'pp1.nama_pegawai', 'pp2.nama_pegawai');
	var $order_pb            = array('pm.id_pembelian_m' => 'desc');
	// ---------------------------------------------------------------------------------------------------------------------------------
	
	// Detail transaksi dari perakitan
	var $column_order_dp     = array('pm.no_perakitan', 'pm.jumlah_perakitan', 'pm.catatan', 'pp1.nama_pegawai', 'pp2.nama_pegawai');
	var $column_search_dp    = array('pm.no_perakitan', 'pm.keterangan_lain', 'pp1.nama_pegawai', 'pp2.nama_pegawai');
	var $order_dp            = array('pm.id_perakitan_m' => 'desc');
	// ---------------------------------------------------------------------------------------------------------------------------------
	
	// Detail transaksi retur penjualan tidak rusak
	var $column_order_rpt    = array('rpm.no_retur_penjualan', 'rpm.no_penjualan', 'rpd.jumlah_beli', 'rpd.jumlah_retur', 'rpm.catatan', 
									 'pp1.nama_pegawai', 'pp2.nama_pegawai');
	var $column_search_rpt   = array('rpm.no_retur_penjualan', 'rpm.keterangan_lain', 'pp1.nama_pegawai', 'pp2.nama_pegawai');
	var $order_rpt           = array('rpm.id_retur_penjualan_m' => 'desc');
	// ---------------------------------------------------------------------------------------------------------------------------------
	
	// Detail transaksi retur barang dari toko tidak rusak
	var $column_order_rbdtt  = array('rdt.id_retur', 'rdt.no_retur_pembelian', 'rdt.jumlah_masuk', 'rdt.keterangan', 
									 'pp1.nama_pegawai', 'pp2.nama_pegawai');
	var $column_search_rbdtt = array('rdt.no_retur_pembelian', 'rdt.keterangan_lain', 'pp1.nama_pegawai', 'pp2.nama_pegawai');
	var $order_rbdtt         = array('rdt.id_retur' => 'desc');
	// ---------------------------------------------------------------------------------------------------------------------------------
	
	// Detail transaksi untuk perakitan
	var $column_order_up     = array('pm.no_perakitan', 'pd.jumlah_komponen', 'pm.catatan', 'pp1.nama_pegawai', 'pp2.nama_pegawai');
	var $column_search_up    = array('pm.no_perakitan', 'pm.keterangan_lain', 'pp1.nama_pegawai', 'pp2.nama_pegawai');
	var $order_up            = array('pm.id_perakitan_m' => 'desc');
	// ---------------------------------------------------------------------------------------------------------------------------------
	
	// Detail transaksi penjualan
	var $column_order_pju    = array('pm.no_penjualan', 'cp.nama_customer_pusat', 'pd.jumlah_beli', 'pm.tipe_penjualan', 
									 'pm.catatan', 'pp1.nama_pegawai', 'pp2.nama_pegawai');
	var $column_order_pjt    = array('pm.no_penjualan', 'cp.nama_customer_pusat', 'pd.jumlah_beli', 
									 'pm.catatan', 'pp1.nama_pegawai', 'pp2.nama_pegawai');	
	var $column_search_pj    = array('pm.no_penjualan', 'cp.nama_customer_pusat', 'pm.keterangan_lain', 'pm.tipe_penjualan',
									 'pp1.nama_pegawai', 'pp2.nama_pegawai');
	var $order_pj            = array('pm.id_penjualan_m' => 'desc');
	// ---------------------------------------------------------------------------------------------------------------------------------
	
	// Detail transaksi retur pembelian tidak rusak
	var $column_order_rpbt   = array('rpm.no_retur_pembelian', 'rpm.no_pembelian', 'rpd.jumlah_retur', 
									 'rpm.catatan', 'pp1.nama_pegawai', 'pp2.nama_pegawai');
	var $column_search_rpbt  = array('rpm.no_retur_pembelian', 'rpm.no_pembelian',  'rpm.keterangan_lain', 'pp1.nama_pegawai', 'pp2.nama_pegawai');
	var $order_rpbt          = array('rpm.id_retur_pembelian_m' => 'desc');
	// ---------------------------------------------------------------------------------------------------------------------------------
	
	// Detail transaksi penukaran poin
	var $column_order_pp     = array('ppm.no_penukaran_poin', 'cp.nama_customer_pusat', 'rpd.jumlah_penukaran', 
									 'ppm.catatan', 'pp1.nama_pegawai', 'pp2.nama_pegawai');
	var $column_search_pp    = array('ppm.no_penukaran_poin', 'cp.nama_customer_pusat', 'ppm.keterangan_lain', 'pp1.nama_pegawai', 'pp2.nama_pegawai');
	var $order_pp            = array('ppm.id_penukaran_poin_m' => 'desc');
	// ---------------------------------------------------------------------------------------------------------------------------------
	
	// Detail transaksi hadiah
	var $column_order_hd     = array('hm.no_hadiah', 'hm.nama_hadiah', 'cp.nama_customer_pusat', 'hd.jumlah_hadiah', 
									 'hm.catatan', 'pp1.nama_pegawai', 'pp2.nama_pegawai');
	var $column_search_hd    = array('hm.no_penukaran_poin', 'cp.nama_customer_pusat', 'hm.keterangan_lain', 'pp1.nama_pegawai', 'pp2.nama_pegawai');
	var $order_hd            = array('hm.id_hadiah_m' => 'desc');
	// ---------------------------------------------------------------------------------------------------------------------------------
	
	// Detail transaksi perpindahan stok
	var $column_order_ps     = array('psm.no_pindah_stok', 'psm.jumlah_pindah', 'psm.catatan', 'pp1.nama_pegawai', 'pp2.nama_pegawai');
	var $column_search_ps    = array('psm.no_pindah_stok', 'psm.keterangan_lain', 'pp1.nama_pegawai', 'pp2.nama_pegawai');
	var $order_ps            = array('psm.id_pindah_stok_m' => 'desc');
	// ---------------------------------------------------------------------------------------------------------------------------------

	// Awal detail transaksi - stok rusak
	// ---------------------------------------------------------------------------------------------------------------------------------	
	// Detail transaksi retur penjualan rusak
	var $column_order_rpr   = array('rpm.no_retur_penjualan', 'rpd.jumlah_beli', 'rpd.jumlah_retur', 
									'rpm.no_penjualan', 'cp.nama_customer_pusat', 'rpd.keterangan', 'pp1.nama_pegawai', 'pp2.nama_pegawai');
	var $column_search_rpr  = array('rpm.no_retur_penjualan', 'rpd.keterangan', 'pp1.nama_pegawai', 'pp2.nama_pegawai');
	var $order_rpr          = array('rpm.id_retur_penjualan_m' => 'desc');
	// ---------------------------------------------------------------------------------------------------------------------------------
	
	// Detail transaksi retur barang dari toko rusak
	var $column_order_rbdttr  = array('rdt.id_retur', 'rdt.no_retur_pembelian', 'rdt.jumlah_masuk', 'rdt.keterangan', 
									  'pp1.nama_pegawai', 'pp2.nama_pegawai');
	var $column_search_rbdttr = array('rdt.no_retur_pembelian', 'rdt.keterangan_lain', 'pp1.nama_pegawai', 'pp2.nama_pegawai');
	var $order_rbdttr         = array('rdt.id_retur' => 'desc');
	// ---------------------------------------------------------------------------------------------------------------------------------

	// Detail transaksi retur pembelian rusak
	var $column_order_rpbr  = array('rpm.no_retur_pembelian', 'rpm.no_pembelian', 'rpd.jumlah_retur', 
									'rpm.no_penjualan', 'cp.nama_customer_pusat', 'rpd.keterangan', 'pp1.nama_pegawai', 'pp2.nama_pegawai');
	var $column_search_rpbr = array('rpm.no_retur_penjualan', 'rpd.keterangan', 'pp1.nama_pegawai', 'pp2.nama_pegawai');
	var $order_rpbr         = array('rpm.id_retur_pembelian_m' => 'desc');
	// ---------------------------------------------------------------------------------------------------------------------------------

	// Awal query table - Stok jual
	private function _get_datatables_query_so($id_barang_pusat, $id_periode_stok_pusat)
	{		
		$this->db->select('
			so.*,
			bp.sku, bp.nama_barang, bp.harga_eceran, bp.foto,
			IFNULL(pp1.nama_pegawai, "-") AS pegawai_save, IFNULL(pp1.nama_pegawai, "-") AS pegawai_edit
		');
		$this->db->from('vamr4846_vama.stok_opname_pusat AS so');
		$this->db->join('vamr4846_vama.barang_pusat AS bp','bp.id_barang_pusat=so.id_barang_pusat','LEFT');
		$this->db->join('vamr4846_vama.pegawai_pusat AS pp1','pp1.id_pegawai=so.id_pegawai_pembuatan','LEFT');
		$this->db->join('vamr4846_vama.pegawai_pusat AS pp2','pp2.id_pegawai=so.id_pegawai_pembaharuan','LEFT');
		$this->db->where(array(
			'so.id_periode_stok_pusat' => $id_periode_stok_pusat,
			'so.id_barang_pusat'       => $id_barang_pusat
		));

		$i = 0;
	
		foreach ($this->column_search_so as $item){
			if($_POST['search']['value']){
				if($i===0){
					$this->db->group_start();
					$this->db->like($item, $_POST['search']['value']);
				}else{
					$this->db->or_like($item, $_POST['search']['value']);
				}

				if(count($this->column_search_so) - 1 == $i)
					$this->db->group_end();
			}
			$i++;
		}
		
		if(isset($_POST['order'])){
			$this->db->order_by($this->column_order_so[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		}else if(isset($this->order_so)){
			$order = $this->order_so;
			$this->db->order_by(key($order), $order[key($order)]);
		}	
	}

	private function _get_datatables_query_pb($id_barang_pusat, $tanggal_awal, $tanggal_akhir)
	{		
		$this->db->select('	
			pd.id_pembelian_d, pd.id_pembelian_m,
			pm.no_pembelian, pm.no_faktur, pm.keterangan_lain as catatan,
			pd.id_barang_pusat, bp.sku, bp.kode_barang, bp.nama_barang, pd.jumlah_beli,
			pp1.nama_pegawai AS pegawai_save, IFNULL(pp2.nama_pegawai, "-") AS pegawai_edit, 
			pd.tanggal_pembuatan, pd.tanggal_pembaharuan
		');
		$this->db->from('vamr4846_vama.pembelian_detail AS pd');
		$this->db->join('vamr4846_vama.pembelian_master as pm','pm.id_pembelian_m=pd.id_pembelian_m','LEFT');
		$this->db->join('vamr4846_vama.barang_pusat as bp','bp.id_barang_pusat=pd.id_barang_pusat','LEFT');
		$this->db->join('vamr4846_vama.pegawai_pusat as pp1','pp1.id_pegawai=pd.id_pegawai_pembuatan','LEFT');
		$this->db->join('vamr4846_vama.pegawai_pusat as pp2','pp2.id_pegawai=pd.id_pegawai_pembaharuan','LEFT');
		$this->db->where('pd.id_barang_pusat', $id_barang_pusat);
		$this->db->where('DATE(pm.tanggal) 
						  BETWEEN "'.date('Y-m-d', strtotime($tanggal_awal)).'" 
						  AND "'.date('Y-m-d', strtotime($tanggal_akhir)).'"');

		$i = 0;
	
		foreach ($this->column_search_pb as $item){
			if($_POST['search']['value']){
				if($i===0){
					$this->db->group_start();
					$this->db->like($item, $_POST['search']['value']);
				}else{
					$this->db->or_like($item, $_POST['search']['value']);
				}

				if(count($this->column_search_pb) - 1 == $i)
					$this->db->group_end();
			}
			$i++;
		}
		
		if(isset($_POST['order'])){
			$this->db->order_by($this->column_order_pb[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		}else if(isset($this->order_pb)){
			$order = $this->order_pb;
			$this->db->order_by(key($order), $order[key($order)]);
		}	
	}

	private function _get_datatables_query_dp($id_barang_pusat, $tanggal_awal, $tanggal_akhir)
	{		
		$this->db->select('
			pm.id_perakitan_m, pm.no_perakitan, pm.keterangan_lain AS catatan, pm.tanggal,
			pm.id_barang_pusat, bp.sku, bp.nama_barang, bp.foto, pm.jumlah_perakitan,
			pp1.nama_pegawai AS pegawai_save, pp2.nama_pegawai AS pegawai_edit, 
			pm.tanggal_pembuatan, pm.tanggal_pembaharuan, status_perakitan
		');
		$this->db->from('vamr4846_vama.perakitan_master AS pm');
		$this->db->join('vamr4846_vama.barang_pusat as bp','bp.id_barang_pusat=pm.id_barang_pusat','LEFT');
		$this->db->join('vamr4846_vama.pegawai_pusat as pp1','pp1.id_pegawai=pm.id_pegawai_pembuatan','LEFT');
		$this->db->join('vamr4846_vama.pegawai_pusat as pp2','pp2.id_pegawai=pm.id_pegawai_pembaharuan','LEFT');
		$this->db->where('pm.id_barang_pusat', $id_barang_pusat);
		$this->db->where('DATE(pm.tanggal) 
						  BETWEEN "'.date('Y-m-d', strtotime($tanggal_awal)).'" 
						  AND "'.date('Y-m-d', strtotime($tanggal_akhir)).'"');

		$i = 0;
	
		foreach ($this->column_search_dp as $item){
			if($_POST['search']['value']){
				if($i===0){
					$this->db->group_start();
					$this->db->like($item, $_POST['search']['value']);
				}else{
					$this->db->or_like($item, $_POST['search']['value']);
				}

				if(count($this->column_search_dp) - 1 == $i)
					$this->db->group_end();
			}
			$i++;
		}
		
		if(isset($_POST['order'])){
			$this->db->order_by($this->column_order_dp[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		}else if(isset($this->order_dp)){
			$order = $this->order_dp;
			$this->db->order_by(key($order), $order[key($order)]);
		}	
	}

	private function _get_datatables_query_rpt($id_barang_pusat, $tanggal_awal, $tanggal_akhir)
	{		
		$this->db->select('
			rpm.id_retur_penjualan_m, rpm.no_retur_penjualan, rpm.no_penjualan, rpm.keterangan_lain AS catatan,
			IFNULL(cp.nama_customer_pusat, "-") AS nama_customer_pusat, rpd.jumlah_beli, rpd.jumlah_retur,
			pp1.nama_pegawai AS pegawai_save, IFNULL(pp2.nama_pegawai, "-") AS pegawai_edit, 
			rpd.tanggal_pembuatan, rpd.tanggal_pembaharuan
		');
		$this->db->from('vamr4846_vama.retur_penjualan_detail AS rpd');
		$this->db->join('vamr4846_vama.retur_penjualan_master as rpm', 'rpm.id_retur_penjualan_m=rpd.id_retur_penjualan_m','LEFT');
		$this->db->join('vamr4846_vama.customer_pusat as cp','cp.id_customer_pusat=rpm.id_customer_pusat','LEFT');
		$this->db->join('vamr4846_vama.pegawai_pusat as pp1','pp1.id_pegawai=rpd.id_pegawai_pembuatan','LEFT');
		$this->db->join('vamr4846_vama.pegawai_pusat as pp2','pp2.id_pegawai=rpd.id_pegawai_pembaharuan','LEFT');
		$this->db->where(array(
			'rpd.id_barang_pusat' => $id_barang_pusat,
			'rpd.masuk_stok'      => "JUAL"
		));
		$this->db->where('DATE(rpm.tanggal_retur) 
						  BETWEEN "'.date('Y-m-d', strtotime($tanggal_awal)).'" 
						  AND "'.date('Y-m-d', strtotime($tanggal_akhir)).'"');

		$i = 0;
	
		foreach ($this->column_search_rpt as $item){
			if($_POST['search']['value']){
				if($i===0){
					$this->db->group_start();
					$this->db->like($item, $_POST['search']['value']);
				}else{
					$this->db->or_like($item, $_POST['search']['value']);
				}

				if(count($this->column_search_rpt) - 1 == $i)
					$this->db->group_end();
			}
			$i++;
		}
		
		if(isset($_POST['order'])){
			$this->db->order_by($this->column_order_rpt[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		}else if(isset($this->order_rpt)){
			$order = $this->order_rpt;
			$this->db->order_by(key($order), $order[key($order)]);
		}	
	}

	private function _get_datatables_query_rbdtt($id_barang_pusat, $tanggal_awal, $tanggal_akhir)
	{		
		$this->db->select('
			rdt.*, rdt.keterangan AS catatan,
			IFNULL(cp.nama_customer_pusat, "-") AS nama_toko,
			pp1.nama_pegawai AS pegawai_save, IFNULL(pp2.nama_pegawai, "-") AS pegawai_edit
		');
		$this->db->from('vamr4846_vama.retur_dari_toko AS rdt');
		$this->db->join('vamr4846_vama.customer_pusat as cp','cp.kode_customer_pusat=rdt.kode_toko','LEFT');
		$this->db->join('vamr4846_vama.pegawai_pusat as pp1','pp1.id_pegawai=rdt.id_pegawai_pembuatan','LEFT');
		$this->db->join('vamr4846_vama.pegawai_pusat as pp2','pp2.id_pegawai=rdt.id_pegawai_pembaharuan','LEFT');
		$this->db->where(array(
			'rdt.id_barang_pusat' => $id_barang_pusat,
			'rdt.kondisi_barang'  => "BAIK"
		));
		$this->db->where('DATE(rdt.tanggal_pembuatan) 
						  BETWEEN "'.date('Y-m-d', strtotime($tanggal_awal)).'" 
						  AND "'.date('Y-m-d', strtotime($tanggal_akhir)).'"');

		$i = 0;
	
		foreach ($this->column_search_rbdtt as $item){
			if($_POST['search']['value']){
				if($i===0){
					$this->db->group_start();
					$this->db->like($item, $_POST['search']['value']);
				}else{
					$this->db->or_like($item, $_POST['search']['value']);
				}

				if(count($this->column_search_rbdtt) - 1 == $i)
					$this->db->group_end();
			}
			$i++;
		}
		
		if(isset($_POST['order'])){
			$this->db->order_by($this->column_order_rbdtt[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		}else if(isset($this->order_rbdtt)){
			$order = $this->order_rbdtt;
			$this->db->order_by(key($order), $order[key($order)]);
		}	
	}

	private function _get_datatables_query_up($id_barang_pusat, $tanggal_awal, $tanggal_akhir)
	{		
		$this->db->select('
			pm.no_perakitan, pd.id_perakitan_d, pd.id_perakitan_m, pm.keterangan_lain AS catatan,
			pd.id_barang_pusat, pd.jumlah_komponen,
			pp1.nama_pegawai AS pegawai_save, pp2.nama_pegawai AS pegawai_edit, 
			pd.tanggal_pembuatan, pd.tanggal_pembaharuan
		');
		$this->db->from('vamr4846_vama.perakitan_detail AS pd');
		$this->db->join('vamr4846_vama.perakitan_master as pm','pm.id_perakitan_m=pd.id_perakitan_m','LEFT');
		$this->db->join('vamr4846_vama.pegawai_pusat as pp1','pp1.id_pegawai=pd.id_pegawai_pembuatan','LEFT');
		$this->db->join('vamr4846_vama.pegawai_pusat as pp2','pp2.id_pegawai=pd.id_pegawai_pembaharuan','LEFT');
		$this->db->where('pd.id_barang_pusat', $id_barang_pusat);
		$this->db->where('DATE(pm.tanggal) 
						  BETWEEN "'.date('Y-m-d', strtotime($tanggal_awal)).'" 
						  AND "'.date('Y-m-d', strtotime($tanggal_akhir)).'"');

		$i = 0;
	
		foreach ($this->column_search_up as $item){
			if($_POST['search']['value']){
				if($i===0){
					$this->db->group_start();
					$this->db->like($item, $_POST['search']['value']);
				}else{
					$this->db->or_like($item, $_POST['search']['value']);
				}

				if(count($this->column_search_up) - 1 == $i)
					$this->db->group_end();
			}
			$i++;
		}
		
		if(isset($_POST['order'])){
			$this->db->order_by($this->column_order_up[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		}else if(isset($this->order_up)){
			$order = $this->order_up;
			$this->db->order_by(key($order), $order[key($order)]);
		}	
	}

	private function _get_datatables_query_pj($id_barang_pusat, $tanggal_awal, $tanggal_akhir, $jenis_perintah)
	{		
		$this->db->select('	
			pm.id_penjualan_m, pd.id_penjualan_d, pm.no_penjualan, pm.keterangan_lain AS catatan,
			IFNULL(cp.nama_customer_pusat, "-") AS nama_customer_pusat, pd.jumlah_beli, pm.tipe_penjualan,
			pp1.nama_pegawai AS pegawai_save, IFNULL(pp2.nama_pegawai, "-") AS pegawai_edit, IFNULL(pp3.nama_pegawai_toko, "-") AS pegawai_masuk,
			pd.tanggal_pembuatan, pd.tanggal_pembaharuan, pd.tanggal_masuk
		');
		$this->db->from('vamr4846_vama.penjualan_detail AS pd');
		$this->db->join('vamr4846_vama.penjualan_master as pm', 'pm.id_penjualan_m=pd.id_penjualan_m', 'LEFT');
		$this->db->join('vamr4846_vama.customer_pusat as cp','cp.id_customer_pusat=pm.id_customer_pusat','LEFT');
		$this->db->join('vamr4846_vama.pegawai_pusat as pp1','pp1.id_pegawai=pd.id_pegawai_pembuatan','LEFT');
		$this->db->join('vamr4846_vama.pegawai_pusat as pp2','pp2.id_pegawai=pd.id_pegawai_pembaharuan','LEFT');
		$this->db->join('vamr4846_toko_mrc.pegawai_toko as pp3', 'pp3.id_pegawai_toko=pd.id_pegawai_masuk', 'LEFT');
		$this->db->where('pd.id_barang_pusat', $id_barang_pusat);
		$this->db->where('DATE(pm.tanggal) BETWEEN "'. date('Y-m-d', strtotime($tanggal_awal)). '" AND 
												   "'. date('Y-m-d', strtotime($tanggal_akhir)).'" AND 
						  pm.tipe_customer_pusat '.$jenis_perintah.' "MRC"
						');

		$i = 0;
	
		foreach ($this->column_search_pj as $item){
			if($_POST['search']['value']){
				if($i===0){
					$this->db->group_start();
					$this->db->like($item, $_POST['search']['value']);
				}else{
					$this->db->or_like($item, $_POST['search']['value']);
				}

				if(count($this->column_search_pj) - 1 == $i)
					$this->db->group_end();
			}
			$i++;
		}
		
		if(isset($_POST['order'])){
			if($jenis_perintah == '!='){
				$this->db->order_by($this->column_order_pju[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
			}else if($jenis_perintah == '!='){
				$this->db->order_by($this->column_order_pjt[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
			}
		}else if(isset($this->order_pj)){
			$order = $this->order_pj;
			$this->db->order_by(key($order), $order[key($order)]);
		}	
	}

	private function _get_datatables_query_rpbt($id_barang_pusat, $tanggal_awal, $tanggal_akhir)
	{		
		$this->db->select('
			rpm.no_retur_pembelian, rpm.no_pembelian, rpd.keterangan AS catatan,
			rpd.id_retur_pembelian_d, rpd.id_retur_pembelian_m,
			rpd.dari_stok, rpd.jumlah_retur, rpd.keterangan, rpd.harga_satuan, rpd.subtotal,
			pp1.nama_pegawai AS pegawai_save, IFNULL(pp2.nama_pegawai, "-") AS pegawai_edit, 
			rpd.tanggal_pembuatan, rpd.tanggal_pembaharuan
		');
		$this->db->from('vamr4846_vama.retur_pembelian_detail AS rpd');
		$this->db->join('vamr4846_vama.retur_pembelian_master as rpm', 'rpm.id_retur_pembelian_m=rpd.id_retur_pembelian_m', 'LEFT');
		$this->db->join('vamr4846_vama.pegawai_pusat as pp1','pp1.id_pegawai=rpd.id_pegawai_pembuatan','LEFT');
		$this->db->join('vamr4846_vama.pegawai_pusat as pp2','pp2.id_pegawai=rpd.id_pegawai_pembaharuan','LEFT');
		$this->db->where('rpd.dari_stok', 'JUAL');
		$this->db->where('rpd.id_barang_pusat', $id_barang_pusat);
		$this->db->where('DATE(rpm.tanggal_retur) 
						  BETWEEN "'.date('Y-m-d', strtotime($tanggal_awal)).'" 
						  AND "'.date('Y-m-d', strtotime($tanggal_akhir)).'"
						');

		$i = 0;
		foreach ($this->column_search_rpbt as $item){
			if($_POST['search']['value']){
				if($i===0){
					$this->db->group_start();
					$this->db->like($item, $_POST['search']['value']);
				}else{
					$this->db->or_like($item, $_POST['search']['value']);
				}

				if(count($this->column_search_rpbt) - 1 == $i)
					$this->db->group_end();
			}
			$i++;
		}
		
		if(isset($_POST['order'])){
			$this->db->order_by($this->column_order_rpbt[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		}else if(isset($this->order_rpbt)){
			$order = $this->order_rpbt;
			$this->db->order_by(key($order), $order[key($order)]);
		}	
	}

	private function _get_datatables_query_hd($id_barang_pusat, $tanggal_awal, $tanggal_akhir)
	{		
		$this->db->select('
			hm.id_hadiah_m, hm.no_hadiah, hm.nama_hadiah, cp.nama_customer_pusat, 
			hd.id_hadiah_d, hd.jumlah_hadiah, hm.keterangan_lain AS catatan,
			pp1.nama_pegawai AS pegawai_save, pp2.nama_pegawai AS pegawai_edit, 
			hd.tanggal_pembuatan, hd.tanggal_pembaharuan
		');
		$this->db->from('vamr4846_vama.hadiah_detail AS hd');
		$this->db->join('vamr4846_vama.hadiah_master as hm', 'hm.id_hadiah_m=hd.id_hadiah_m', 'LEFT');
		$this->db->join('vamr4846_vama.customer_pusat as cp','cp.id_customer_pusat=hm.id_customer_pusat','LEFT');
		$this->db->join('vamr4846_vama.pegawai_pusat as pp1','pp1.id_pegawai=hd.id_pegawai_pembuatan','LEFT');
		$this->db->join('vamr4846_vama.pegawai_pusat as pp2','pp2.id_pegawai=hd.id_pegawai_pembaharuan','LEFT');
		$this->db->where('hd.id_barang_pusat', $id_barang_pusat);
		$this->db->where('DATE(hm.tanggal) 
						  BETWEEN "'.date('Y-m-d', strtotime($tanggal_awal)).'" 
						  AND "'.date('Y-m-d', strtotime($tanggal_akhir)).'"
						');

		$i = 0;
		foreach ($this->column_search_hd as $item){
			if($_POST['search']['value']){
				if($i===0){
					$this->db->group_start();
					$this->db->like($item, $_POST['search']['value']);
				}else{
					$this->db->or_like($item, $_POST['search']['value']);
				}

				if(count($this->column_search_hd) - 1 == $i)
					$this->db->group_end();
			}
			$i++;
		}
		
		if(isset($_POST['order'])){
			$this->db->order_by($this->column_order_hd[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		}else if(isset($this->order_hd)){
			$order = $this->order_hd;
			$this->db->order_by(key($order), $order[key($order)]);
		}	
	}
	// Akhir query table - stok jual
	// -------------------------------------------------------------------------------------------------------------------------

	// Awal query table - stok rusak
	// -------------------------------------------------------------------------------------------------------------------------
	private function _get_datatables_query_rpr($id_barang_pusat, $tanggal_awal, $tanggal_akhir)
	{		
		$this->db->select('
			rpd.id_retur_penjualan_m, rpd.id_retur_penjualan_d, rpm.no_retur_penjualan, rpm.no_penjualan,
			rpd.jumlah_beli, rpd.jumlah_retur, IFNULL(rpd.keterangan,"-") AS catatan,
			IFNULL(cp.nama_customer_pusat, "-") AS nama_customer_pusat, 
			pp1.nama_pegawai AS pegawai_save, IFNULL(pp2.nama_pegawai, "-") AS pegawai_edit, 
			rpd.tanggal_pembuatan, rpd.tanggal_pembaharuan
		');
		$this->db->from('vamr4846_vama.retur_penjualan_master as rpm');
		$this->db->join('vamr4846_vama.retur_penjualan_detail as rpd','rpd.id_retur_penjualan_m=rpm.id_retur_penjualan_m');
		$this->db->join('vamr4846_vama.customer_pusat as cp','cp.id_customer_pusat=rpm.id_customer_pusat','LEFT');
		$this->db->join('vamr4846_vama.pegawai_pusat as pp1','pp1.id_pegawai=rpd.id_pegawai_pembuatan','LEFT');
		$this->db->join('vamr4846_vama.pegawai_pusat as pp2','pp2.id_pegawai=rpd.id_pegawai_pembaharuan','LEFT');
		$this->db->where(array(
							'rpd.id_barang_pusat' => $id_barang_pusat,
							'rpd.masuk_stok'      => "RUSAK"
						));
		$this->db->where('DATE(rpm.tanggal_retur) 
						  BETWEEN "'.date('Y-m-d', strtotime($tanggal_awal)).'" 
						  AND "'.date('Y-m-d', strtotime($tanggal_akhir)).'"
						');

		$i = 0;
		foreach ($this->column_search_rpr as $item){
			if($_POST['search']['value']){
				if($i===0){
					$this->db->group_start();
					$this->db->like($item, $_POST['search']['value']);
				}else{
					$this->db->or_like($item, $_POST['search']['value']);
				}

				if(count($this->column_search_rpr) - 1 == $i)
					$this->db->group_end();
			}
			$i++;
		}
		
		if(isset($_POST['order'])){
			$this->db->order_by($this->column_order_rpr[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		}else if(isset($this->order_rpr)){
			$order = $this->order_rpr;
			$this->db->order_by(key($order), $order[key($order)]);
		}	
	}

	private function _get_datatables_query_rbdttr($id_barang_pusat, $tanggal_awal, $tanggal_akhir)
	{		
		$this->db->select('
			rdt.*, rdt.keterangan AS catatan,
			IFNULL(cp.nama_customer_pusat, "-") AS nama_toko,
			pp1.nama_pegawai AS pegawai_save, IFNULL(pp2.nama_pegawai, "-") AS pegawai_edit
		');
		$this->db->from('vamr4846_vama.retur_dari_toko AS rdt');
		$this->db->join('vamr4846_vama.customer_pusat as cp','cp.kode_customer_pusat=rdt.kode_toko','LEFT');
		$this->db->join('vamr4846_vama.pegawai_pusat as pp1','pp1.id_pegawai=rdt.id_pegawai_pembuatan','LEFT');
		$this->db->join('vamr4846_vama.pegawai_pusat as pp2','pp2.id_pegawai=rdt.id_pegawai_pembaharuan','LEFT');
		$this->db->where(array(
			'rdt.id_barang_pusat' => $id_barang_pusat,
			'rdt.kondisi_barang'  => "RUSAK"
		));
		$this->db->where('DATE(rdt.tanggal_pembuatan) 
						  BETWEEN "'.date('Y-m-d', strtotime($tanggal_awal)).'" 
						  AND "'.date('Y-m-d', strtotime($tanggal_akhir)).'"');

		$i = 0;
	
		foreach ($this->column_search_rbdttr as $item){
			if($_POST['search']['value']){
				if($i===0){
					$this->db->group_start();
					$this->db->like($item, $_POST['search']['value']);
				}else{
					$this->db->or_like($item, $_POST['search']['value']);
				}

				if(count($this->column_search_rbdttr) - 1 == $i)
					$this->db->group_end();
			}
			$i++;
		}
		
		if(isset($_POST['order'])){
			$this->db->order_by($this->column_order_rbdttr[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		}else if(isset($this->order_rbdttr)){
			$order = $this->order_rbdttr;
			$this->db->order_by(key($order), $order[key($order)]);
		}	
	}

	private function _get_datatables_query_rpbr($id_barang_pusat, $tanggal_awal, $tanggal_akhir)
	{		
		$this->db->select('
			rpm.no_retur_pembelian, rpm.no_pembelian, rpd.keterangan AS catatan,
			rpd.id_retur_pembelian_d, rpd.id_retur_pembelian_m, rpd.jumlah_retur,
			pp1.nama_pegawai AS pegawai_save, IFNULL(pp2.nama_pegawai, "-") AS pegawai_edit, 
			rpd.tanggal_pembuatan, rpd.tanggal_pembaharuan
		');
		$this->db->from('vamr4846_vama.retur_pembelian_detail AS rpd');
		$this->db->join('vamr4846_vama.retur_pembelian_master as rpm', 'rpm.id_retur_pembelian_m=rpd.id_retur_pembelian_m', 'LEFT');
		$this->db->join('vamr4846_vama.pegawai_pusat as pp1','pp1.id_pegawai=rpd.id_pegawai_pembuatan','LEFT');
		$this->db->join('vamr4846_vama.pegawai_pusat as pp2','pp2.id_pegawai=rpd.id_pegawai_pembaharuan','LEFT');
		$this->db->where('rpd.dari_stok', 'RUSAK');
		$this->db->where('rpd.id_barang_pusat', $id_barang_pusat);
		$this->db->where('DATE(rpm.tanggal_retur) 
						  BETWEEN "'.date('Y-m-d', strtotime($tanggal_awal)).'" 
						  AND "'.date('Y-m-d', strtotime($tanggal_akhir)).'"
						');

		$i = 0;
		foreach ($this->column_search_rpbr as $item){
			if($_POST['search']['value']){
				if($i===0){
					$this->db->group_start();
					$this->db->like($item, $_POST['search']['value']);
				}else{
					$this->db->or_like($item, $_POST['search']['value']);
				}

				if(count($this->column_search_rpbr) - 1 == $i)
					$this->db->group_end();
			}
			$i++;
		}
		
		if(isset($_POST['order'])){
			$this->db->order_by($this->column_order_rpbr[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		}else if(isset($this->order_rpbr)){
			$order = $this->order_rpbr;
			$this->db->order_by(key($order), $order[key($order)]);
		}	
	}
	// Akhir query table - stok rusak
	// -------------------------------------------------------------------------------------------------------------------------
	function get_datatables($query, $id_barang_pusat, $id_periode_stok_pusat, $tanggal_awal, $tanggal_akhir, $jenis_perintah)
	{
		if($query == '_get_datatables_query_so'){
			$this->$query($id_barang_pusat, $id_periode_stok_pusat);
		}else if($query == '_get_datatables_query_pj'){
			$this->$query($id_barang_pusat, $tanggal_awal, $tanggal_akhir, $jenis_perintah);
		}else{
			$this->$query($id_barang_pusat, $tanggal_awal, $tanggal_akhir);
		}

		if($_POST['length'] != -1)
		$this->db->limit($_POST['length'], $_POST['start']);
		$query = $this->db->get();
		return $query->result();
	}

	function count_filtered($query, $id_barang_pusat, $id_periode_stok_pusat, $tanggal_awal, $tanggal_akhir, $jenis_perintah)
	{
		if($query == '_get_datatables_query_so'){
			$this->$query($id_barang_pusat, $id_periode_stok_pusat);
		}else if($query == '_get_datatables_query_pj'){
			$this->$query($id_barang_pusat, $tanggal_awal, $tanggal_akhir, $jenis_perintah);
		}else{
			$this->$query($id_barang_pusat, $tanggal_awal, $tanggal_akhir);
		}
		
		$query = $this->db->get();
		return $query->num_rows();
	}

	public function count_all($table, $id_barang_pusat, $id_periode_stok_pusat, $tanggal_awal, $tanggal_akhir, $jenis_perintah)
	{
		$this->db->from($table);
		if($table == 'vamr4846_vama.penjualan_detail AS pd'){
			$this->db->join('vamr4846_vama.penjualan_master AS pm', 'pm.id_penjualan_m=pd.id_penjualan_m');
			$this->db->where('pd.id_barang_pusat', $id_barang_pusat);
			$this->db->where('DATE(tanggal) BETWEEN "'. date('Y-m-d', strtotime($tanggal_awal)). '" AND 
												   	"'. date('Y-m-d', strtotime($tanggal_akhir)).'" AND
							  pm.tipe_customer_pusat '.$jenis_perintah.' "MRC"
							');
		}else if($table == 'vamr4846_vama.stok_opname_pusat'){
			$this->db->where(array(
				'id_barang_pusat'       => $id_barang_pusat,
				'id_periode_stok_pusat' => $id_periode_stok_pusat
			));			
		}else if($table == "vamr4846_vama.perakitan_master"){
			$this->db->where('id_barang_pusat', $id_barang_pusat);
			$this->db->where('DATE(tanggal) BETWEEN "'. date('Y-m-d', strtotime($tanggal_awal)). '" AND 
												   	"'. date('Y-m-d', strtotime($tanggal_akhir)).'"');
		}else{
			$this->db->where('id_barang_pusat', $id_barang_pusat);
			$this->db->where('DATE(tanggal_pembuatan) BETWEEN "'. date('Y-m-d', strtotime($tanggal_awal)). '" AND 
												   	"'. date('Y-m-d', strtotime($tanggal_akhir)).'"');
		}
		return $this->db->count_all_results();
	}
}

<?php
class Retur_pembelian_detail_model extends CI_Model
{
	// Retur pembelian induk
	var $column_order 			= array('', '', 'bp.sku','bp.nama_barang', 'rpd.dari_stok', 'rpd.jumlah_retur', 
										'rpd.harga_satuan', 'rpd.subtotal');
	var $column_search 			= array('bp.kode_barang','bp.sku','bp.nama_barang'); 
	var $order 					= array('rpd.tanggal_pembaharuan' => 'desc');

	// Retur pembelian batal
	var $column_order_batal 	= array('', '', 'bp.sku','bp.nama_barang', 'rpdb.jumlah_beli', 'rpdb.subtotal');
	var $column_search_batal 	= array('bp.kode_barang','bp.sku','bp.nama_barang'); 
	var $order_batal 			= array('rpdb.tanggal_pembatalan' => 'desc');

	// Cari barang
	var $column_order_barang 	= array('', 'bp.sku','bp.nama_barang','bp.total_stok', 'bp.total_stok_rusak');
	var $column_search_barang 	= array('bp.kode_barang','bp.sku','bp.nama_barang'); 
	var $order_barang 			= array('bp.nama_barang' => 'desc');

	private function _get_datatables_query($id_retur_pembelian_m)
	{		
		$this->db->select('
			rpd.id_retur_pembelian_d, rpd.id_retur_pembelian_m,
			bp.sku, bp.kode_barang, bp.nama_barang, 
			rpd.dari_stok, rpd.jumlah_retur, rpd.keterangan, rpd.harga_satuan, rpd.subtotal,
			pp1.nama_pegawai AS pegawai_save, IFNULL(pp2.nama_pegawai, "-") AS pegawai_edit, 
			rpd.tanggal_pembuatan, rpd.tanggal_pembaharuan
		');
		$this->db->from('vamr4846_vama.retur_pembelian_detail AS rpd');
		$this->db->join('vamr4846_vama.barang_pusat as bp','bp.id_barang_pusat=rpd.id_barang_pusat','LEFT');
		$this->db->join('vamr4846_vama.pegawai_pusat as pp1','pp1.id_pegawai=rpd.id_pegawai_pembuatan','LEFT');
		$this->db->join('vamr4846_vama.pegawai_pusat as pp2','pp2.id_pegawai=rpd.id_pegawai_pembaharuan','LEFT');
		$this->db->where('id_retur_pembelian_m', $id_retur_pembelian_m);

		$i = 0;	
		foreach ($this->column_search as $item){
			if($_POST['search']['value']){
				if($i===0){
					$this->db->group_start();
					$this->db->like($item, $_POST['search']['value']);
				}else{
					$this->db->or_like($item, $_POST['search']['value']);
				}

				if(count($this->column_search) - 1 == $i)
					$this->db->group_end();
			}
			$i++;
		}
		
		if(isset($_POST['order'])){
			$this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		}else if(isset($this->order)){
			$order = $this->order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}

	private function _get_datatables_query_batal($id_retur_pembelian_m)
	{		
		$this->db->select('
							rpdb.id_retur_pembelian_d, rpdb.id_retur_pembelian_m,
							bp.sku, bp.kode_barang, bp.nama_barang, 
							rpdb.dari_stok, rpdb.jumlah_retur, rpdb.keterangan, rpdb.harga_satuan, rpdb.subtotal,
							rpdb.keterangan_batal, pp.nama_pegawai AS pegawai_pembatalan, rpdb.tanggal_pembatalan
						 ');
		$this->db->from('vamr4846_vama.retur_pembelian_detail_batal AS rpdb');
		$this->db->join('vamr4846_vama.barang_pusat as bp','bp.id_barang_pusat=rpdb.id_barang_pusat','LEFT');
		$this->db->join('vamr4846_vama.pegawai_pusat as pp','pp.id_pegawai=rpdb.id_pegawai_pembatalan','LEFT');
		$this->db->where('id_retur_pembelian_m', $id_retur_pembelian_m);

		$i = 0;
		foreach ($this->column_search_batal as $item){
			if($_POST['search']['value']){
				if($i===0){
					$this->db->group_start();
					$this->db->like($item, $_POST['search']['value']);
				}else{
					$this->db->or_like($item, $_POST['search']['value']);
				}

				if(count($this->column_search_batal) - 1 == $i)
				$this->db->group_end();
			}
			$i++;
		}
		
		if(isset($_POST['order'])){
			$this->db->order_by($this->column_order_batal[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		}else if(isset($this->order_batal)){
			$order = $this->order_batal;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}

	private function _get_datatables_query_barang($id_retur_pembelian_m, $kode_supplier)
	{		
		$ambil_retur	= $this->db->query("SELECT id_barang_pusat 
											FROM vamr4846_vama.retur_pembelian_detail 
											WHERE id_retur_pembelian_m =".$id_retur_pembelian_m."");
  		$rpd_id_barang 	= array();
		foreach ($ambil_retur->result() as $row){
			$rpd_id_barang[] = $row->id_barang_pusat; 
		}
		$id_barang 	= implode(",", $rpd_id_barang);
		$idrp 		= explode(",", $id_barang);
		
		$this->db->select('
							bp.id_barang_pusat, bp.sku, bp.kode_barang, bp.nama_barang, bp.total_stok, bp.total_stok_rusak,
							bp.modal, bp.resistensi_modal, bp.modal_bersih
						 ');
		$this->db->from('vamr4846_vama.barang_pusat as bp');
		$this->db->where('kode_supplier', $kode_supplier);
		$this->db->where_not_in('bp.id_barang_pusat', $idrp);
		
		$i = 0;	
		foreach ($this->column_search_barang as $item){
			if($_POST['search']['value']){
				if($i===0){
					$this->db->group_start();
					$this->db->like($item, $_POST['search']['value']);
				}else{
					$this->db->or_like($item, $_POST['search']['value']);
				}

				if(count($this->column_search_barang) - 1 == $i)
				$this->db->group_end();
			}
			$i++;
		}
		
		if(isset($_POST['order'])){
			$this->db->order_by($this->column_order_barang[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		}else if(isset($this->order_barang)){
			$order = $this->order_barang;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}

	function get_datatables($perintah, $id_retur_pembelian_m, $kode_supplier)
	{
		$this->$perintah($id_retur_pembelian_m, $kode_supplier);
		if($_POST['length'] != -1)
		$this->db->limit($_POST['length'], $_POST['start']);
		$query = $this->db->get();
		return $query->result();
	}

	function count_filtered($perintah, $id_retur_pembelian_m, $kode_supplier)
	{
		$this->$perintah($id_retur_pembelian_m, $kode_supplier);
		$query = $this->db->get();
		return $query->num_rows();
	}

	public function count_all($table, $id_retur_pembelian_m, $kode_supplier)
	{
		$this->db->from($table);
		if($table == 'vamr4846_vama.retur_pembelian_detail' or $table == 'vamr4846_vama.retur_pembelian_detail_batal'){
			$this->db->where('id_retur_pembelian_m',$id_retur_pembelian_m);
		}else{
			$this->db->where('kode_supplier', $kode_supplier);
		}
		return $this->db->count_all_results();
	}

	function get_retur_pembelian_detail_1($id_retur_pembelian_d)
	{
		$this->db->select('rpd.*, rpm.id_supplier, rpm.no_pembelian, rpm.no_retur_pembelian,  
						   bp.sku, bp.nama_barang');
		$this->db->from('vamr4846_vama.retur_pembelian_detail as rpd');
		$this->db->join('vamr4846_vama.retur_pembelian_master as rpm','rpm.id_retur_pembelian_m=rpd.id_retur_pembelian_m','LEFT');
		$this->db->join('vamr4846_vama.barang_pusat as bp','bp.id_barang_pusat=rpd.id_barang_pusat','LEFT');
		$this->db->where('id_retur_pembelian_d', $id_retur_pembelian_d);
		$query = $this->db->get();
		return $query->row();
	}

	function get_retur_pembelian_detail_2($id_retur_pembelian_d)
	{
		$this->db->select('rpd.id_retur_pembelian_d, rpd.id_retur_pembelian_m, rpd.id_barang_pusat, 
						   rpd.jumlah_beli, rpd.jumlah_retur, rpd.dari_stok, rpd.keterangan,
						   rpm.id_supplier, rpm.no_pembelian, rpm.no_retur_pembelian,  
						   bp.sku, bp.nama_barang');
		$this->db->from('vamr4846_vama.retur_pembelian_detail as rpd');
		$this->db->join('vamr4846_vama.retur_pembelian_master as rpm','rpm.id_retur_pembelian_m=rpd.id_retur_pembelian_m','LEFT');
		$this->db->join('vamr4846_vama.barang_pusat as bp','bp.id_barang_pusat=rpd.id_barang_pusat','LEFT');
		$this->db->where('id_retur_pembelian_d', $id_retur_pembelian_d);
		$query = $this->db->get();
		return $query->row();
	}

	function get_id($id_retur_pembelian_m, $id_barang_pusat)
	{
		$this->db->select('*');
		$this->db->from('vamr4846_vama.retur_pembelian_detail');
		$this->db->where(array(  
			'id_retur_pembelian_m' => $id_retur_pembelian_m,
			'id_barang_pusat'      => $id_barang_pusat
		));
		$query = $this->db->get();
		return $query->row();
	}	

	function insert_detail($id_retur_pembelian_m, $id_barang_pusat,  
						   $dari_stok, $jumlah_retur, $harga_satuan, $subtotal, $keterangan_retur, $id_pegawai)
	{ 
		$dt = array (
			'id_retur_pembelian_m' => $id_retur_pembelian_m,
			'id_barang_pusat'      => $id_barang_pusat,
			'jumlah_retur'         => $jumlah_retur,
			'harga_satuan'         => $harga_satuan,
			'subtotal'             => $subtotal,
			'dari_stok'            => $dari_stok,						
			'keterangan'           => $keterangan_retur,
			'id_pegawai_pembuatan' => $id_pegawai,
			'tanggal_pembuatan'    => date('Y-m-d H:i:s') 		
		);
		return $this->db->insert('vamr4846_vama.retur_pembelian_detail', $dt);
	}

	function update_detail($id_retur_pembelian_m, $id_barang_pusat,  
						   $dari_stok, $jumlah_retur, $harga_satuan, $subtotal, $keterangan_retur, 
						   $id_pegawai, $jumlah_retur_lama, $dari_stok_lama)
	{
		if($dari_stok_lama=='JUAL'){
			$sql = "UPDATE vamr4846_vama.barang_pusat SET total_stok = total_stok + ".$jumlah_retur_lama." 
					WHERE id_barang_pusat = '".$id_barang_pusat."' ";
		}else if($dari_stok_lama='RUSAK'){
			$sql = "UPDATE vamr4846_vama.barang_pusat SET total_stok_rusak = total_stok_rusak + ".$jumlah_retur_lama." 
					WHERE id_barang_pusat = '".$id_barang_pusat."' ";
		}
		$this->db->query($sql);
		
		$dt = array ( 	
			'harga_satuan'           => $harga_satuan,
			'jumlah_retur'           => $jumlah_retur,
			'subtotal'               => $subtotal,
			'dari_stok'              => $dari_stok,
			'keterangan'             => $keterangan_retur,
			'id_pegawai_pembaharuan' => $id_pegawai
		);

		$where = array (	
			'id_retur_pembelian_m' => $id_retur_pembelian_m,
			'id_barang_pusat'      => $id_barang_pusat
		);

		return $this->db->update('vamr4846_vama.retur_pembelian_detail', $dt, $where);
	}

	function get_detail_faktur($id_retur_pembelian_m)
	{
		$this->db->select('
							rpd.id_retur_pembelian_d, 
							rpd.jumlah_beli, rpd.jumlah_retur, rpd.keterangan,
							bp.sku, bp.kode_barang, bp.nama_barang
						  ');
		$this->db->from('vamr4846_vama.retur_pembelian_detail AS rpd');
		$this->db->join('vamr4846_vama.barang_pusat as bp','bp.id_barang_pusat=rpd.id_barang_pusat');
		$this->db->where('rpd.id_retur_pembelian_m', $id_retur_pembelian_m);
		$this->db->order_by('bp.nama_barang','ASC');
		$query = $this->db->get();
		return $query->result();
	}

	function get_total($id_retur_pembelian_m)
	{
		$this->db->select('
			id_retur_pembelian_m,
			IFNULL(COUNT(id_retur_pembelian_d),0) jumlah_barang,
			IFNULL(ROUND(SUM((harga_satuan*jumlah_retur)),0),0) total
		');
		$this->db->from('vamr4846_vama.retur_pembelian_detail');
		$this->db->where('id_retur_pembelian_m', $id_retur_pembelian_m);
		$query = $this->db->get();
		return $query->row();
	}

	function hapus_retur_pembelian_detail($id_retur_pembelian_d, $id_retur_pembelian_m, $no_retur_pembelian, $no_pembelian, $id_supplier,
										  $id_barang_pusat, $harga_satuan, $jumlah_beli, $jumlah_retur, $subtotal, $dari_stok, $keterangan, 
										  $id_pegawai_pembuatan, $id_pegawai_pembaharuan, $id_pegawai_pembatalan, 
										  $tanggal_pembuatan, $tanggal_pembaharuan, $keterangan_batal)
	{
		
		// Kembalikan stok berdasarkan jumlah retur dan dari stok mana
		if ($dari_stok == 'JUAL'){
			$sql = "UPDATE vamr4846_vama.barang_pusat SET total_stok = total_stok + ".$jumlah_retur." 
					WHERE id_barang_pusat = '".$id_barang_pusat."'";
		}else if ($dari_stok == 'RUSAK'){
			$sql = "UPDATE vamr4846_vama.barang_pusat SET total_stok_rusak = total_stok_rusak + ".$jumlah_retur." 
					WHERE id_barang_pusat = '".$id_barang_pusat."'";
		}
		$this->db->query($sql);

		// Simpan retur_pembelian detail ke retur_pembelian detail batal
		$sql = "
				INSERT INTO vamr4846_vama.retur_pembelian_detail_batal 
				(id_retur_pembelian_d, id_retur_pembelian_m, id_supplier, no_retur_pembelian, no_pembelian,
				 id_barang_pusat, harga_satuan, jumlah_beli, jumlah_retur, subtotal, dari_stok, keterangan, 
				 id_pegawai_pembuatan, id_pegawai_pembaharuan, id_pegawai_pembatalan, 
				 tanggal_pembuatan, tanggal_pembaharuan, keterangan_batal) 
				VALUES
				('".$id_retur_pembelian_d."', '".$id_retur_pembelian_m."', '".$id_supplier."', '".$no_retur_pembelian."', '".$no_pembelian."', 
				 '".$id_barang_pusat."', '".$harga_satuan."', '".$jumlah_beli."', '".$jumlah_retur."', '".$subtotal."', '".$dari_stok."', '".$keterangan."', 
				 '".$id_pegawai_pembuatan."', '".$id_pegawai_pembaharuan."', '".$id_pegawai_pembatalan."', 
				 '".$tanggal_pembuatan."', '".$tanggal_pembaharuan."', '".$keterangan_batal."') 
				";
		$this->db->query($sql);

		// Hapus retur_pembelian detail
		return $this->db
					->where(array('id_retur_pembelian_d' => $id_retur_pembelian_d,))
					->delete('vamr4846_vama.retur_pembelian_detail');
	}
}
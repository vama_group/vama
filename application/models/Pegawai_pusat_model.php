<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pegawai_pusat_model extends MY_Model {

	var $table         = 'vamr4846_vama.pegawai_pusat';
	var $column_order  = array('pp.kode_pegawai','pp.nama_pegawai','pp.email','pp.handphone');
	var $column_search = array('pp.kode_pegawai','pp.nama_pegawai','pp.email','pp.handphone');
	var $order         = array('id_pegawai' => 'desc');

	private function _get_datatables_query()
	{
		$this->db->select('pp.*, up.usergroup_name, pp1.nama_pegawai AS pegawai_save, pp2.nama_pegawai AS pegawai_edit');
		$this->db->from('vamr4846_vama.pegawai_pusat AS pp');
		$this->db->join('vamr4846_vama.usergroup AS up', 'up.id_usergroup=pp.id_usergroup', 'LEFT');
		$this->db->join('vamr4846_vama.pegawai_pusat AS pp1', 'pp1.id_pegawai=pp.id_pegawai_pembuatan', 'LEFT');
		$this->db->join('vamr4846_vama.pegawai_pusat AS pp2', 'pp2.id_pegawai=pp.id_pegawai_pembaharuan', 'LEFT');
		$this->db->where('pp.status_hapus', 'TIDAK');

		$i = 0;
		foreach ($this->column_search as $item){
			if($_POST['search']['value']){
				if($i === 0){
					$this->db->group_start();
					$this->db->like($item, $_POST['search']['value']);
				}else{
					$this->db->or_like($item, $_POST['search']['value']);
				}

				if(count($this->column_search) - 1 == $i)
					$this->db->group_end();
			}
			$i++;
		}
		
		if(isset($_POST['order'])){
			$this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		}else if(isset($this->order)){
			$order = $this->order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}

	function get_datatables()
	{
		$this->_get_datatables_query();
		if($_POST['length'] != -1)
		$this->db->limit($_POST['length'], $_POST['start']);
		$query = $this->db->get();
		return $query->result();
	}

	function count_filtered()
	{
		$this->_get_datatables_query();
		$query = $this->db->get();
		return $query->num_rows();
	}

	public function count_all()
	{
		$this->db->from($this->table);
		return $this->db->count_all_results();
	}

	public function get_baris($id_pegawai)
	{
		return $this->db
			->select('pp.id_pegawai, pp.nama_pegawai')
			->where('pp.id_pegawai', $id_pegawai)
			->limit(1)
			->get('vamr4846_vama.pegawai_pusat AS pp');
	}

	public function detail($id_pegawai) {
		$this->db->select('pp.*, up.usergroup_name');
		$this->db->from('vamr4846_vama.pegawai_pusat AS pp');
		$this->db->join('vamr4846_vama.usergroup AS up','up.id_usergroup=pp.id_usergroup');
		$this->db->where('pp.id_pegawai',$id_pegawai);
		$this->db->order_by('pp.id_pegawai','DESC');
		$query=$this->db->get();
		return $query->row();
	}	

	public function listing() {
		$this->db->select('pp.*, up.usergroup_name');
		$this->db->from('vamr4846_vama.pegawai_pusat AS pp');
		$this->db->join('vamr4846_vama.usergroup AS up', 'up.id_usergroup=pp.id_usergroup');
		$this->db->order_by('pp.id_pegawai','DESC');
		$query=$this->db->get();
		return $query->result();
	}

	public function get_by_id($id_pegawai)
	{
		$this->db->select('
			pp.id_pegawai, pp.kode_pegawai, pp.nama_pegawai, 
			pp.alamat_pegawai, pp.email, pp.handphone, pp.foto, 
			ug.id_usergroup, ug.usergroup_name, pp.status_blokir
		');
		$this->db->from('vamr4846_vama.pegawai_pusat AS pp');
		$this->db->join('vamr4846_vama.usergroup AS ug','ug.id_usergroup=pp.id_usergroup');
		$this->db->where('pp.id_pegawai',$id_pegawai);
		$query = $this->db->get();
		return $query->row();
	}

	public function get_by_email($email)
	{
		$this->db->select('
			pp.id_pegawai, pp.kode_pegawai, pp.nama_pegawai, 
			pp.alamat_pegawai, pp.email, pp.handphone, pp.foto, 
			ug.id_usergroup, ug.usergroup_name, pp.status_blokir
		');
		$this->db->from('vamr4846_vama.pegawai_pusat AS pp');
		$this->db->join('vamr4846_vama.usergroup AS ug','ug.id_usergroup=pp.id_usergroup');
		$this->db->where('pp.email', $email);
		$query = $this->db->get();
		return $query->row();
	}

	public function get_by_nama($nama_pegawai)
	{
		$this->db->select('
			pp.id_pegawai, pp.kode_pegawai, pp.nama_pegawai, 
			pp.alamat_pegawai, pp.email, pp.handphone, pp.foto, 
			ug.id_usergroup, ug.usergroup_name, pp.status_blokir');
		$this->db->from('vamr4846_vama.pegawai_pusat AS pp');
		$this->db->join('vamr4846_vama.usergroup AS ug','ug.id_usergroup=pp.id_usergroup');
		$this->db->where('pp.nama_pegawai', $nama_pegawai);
		$query = $this->db->get();
		return $query->row();
	}

	public function cek_password($id_pegawai, $password)
	{
		$this->db->select('pp.id_pegawai, pp.nama_pegawai, pp.email, pp.password');
		$this->db->from('vamr4846_vama.pegawai_pusat AS pp');
		$this->db->where(array(
							'pp.id_pegawai' => $id_pegawai,
							'pp.password'   => $password
						));
		$query = $this->db->get();
		return $query->row();
	}

	public function save($data)
	{
		$this->db->insert($this->table, $data);
		return $this->db->insert_id();
	}

	public function create_useraccess_pusat($id_pegawai_baru, $id_pegawai_save, $tanggal_pembuatan){
		$sql_query = $this->db->query("
			CALL vamr4846_vama.InsertUserAccessPusat('".$id_pegawai_baru."', '".$id_pegawai_save."', '".$tanggal_pembuatan."')
		");
	}

	public function update($where, $data)
	{
		$this->db->update($this->table, $data, $where);
		return $this->db->affected_rows();
	}

	public function delete_by_id($id_pegawai)
	{
		$this->db->where('id_pegawai', $id_pegawai);
		$this->db->delete($this->table);
	}

	// Passsword
	public function password($id_pegawai, $password) {
		$query = $this->db->get_where('vamr4846_vama.pegawai_pusat AS pp', 
								array(
									'pp.id_pegawai' => $id_pegawai,
									'pp.password'   => sha1($password)
								)
							);
		return $query->row();
	}

	public function update_status_hapus($id_pegawai_save, $id_pegawai)
	{
		$sql = "UPDATE vamr4846_vama.pegawai_pusat 
				SET status_hapus = 'IYA', id_pegawai_pembaharuan = '".$id_pegawai."' 
				WHERE id_pegawai = '".$id_pegawai_save."'";
		return $this->db->query($sql);
	}

	// ----------------------------------------------------------------------------------------------------------------------
	// Awal query foto
	public function ambil_daftar_foto($id_pegawai)
	{
		$sql = "SELECT *
				FROM 
					vamr4846_vama.foto_pegawai_pusat AS pp
				WHERE 
					(pp.id_pegawai = '".$id_pegawai."') 
				";
		return $this->db->query($sql);
	}

	public function update_foto_profile($id_pegawai, $nama_foto)
	{
		$sql = "UPDATE vamr4846_vama.pegawai_pusat AS pp 
				SET pp.foto = '".$nama_foto."', pp.id_pegawai_pembaharuan='".$this->session->userdata('id_pegawai')."'
				WHERE pp.id_pegawai = '".$id_pegawai."'";
		return $this->db->query($sql);
	}

	public function ambil_nama_foto($id_foto)
	{
		$this->db->select('fpp.*');
		$this->db->from('vamr4846_vama.foto_pegawai_pusat AS fpp');
		$this->db->where(array(  
		            			'fpp.id_foto' => $id_foto
		            	));
		$query = $this->db->get();
		return $query->row();
	}
	// Akhir query foto
	// ----------------------------------------------------------------------------------------------------------------------
}

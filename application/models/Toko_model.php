<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Toko_model extends MY_Model {

	var $table         = 'vamr4846_toko_mrc.toko';
	var $column_order  = array('toko.kode_toko','nama_toko','toko.email','toko.pin_bb1','toko.telephone1','toko.handphone1');
	var $column_search = array('toko.kode_toko','nama_toko','toko.email','toko.pin_bb1','toko.telephone1','toko.handphone1'); 
	var $order         = array('id_toko' => 'desc'); 

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	// Listing data
	public function listing() {
		$this->db->select('tk.*, pt.nama_pegawai_toko');
		$this->db->from('vamr4846_toko_mrc.toko AS tk');
		$this->db->join('vamr4846_toko_mrc.pegawai_toko AS pt','pt.kode_pegawai_toko=tk.kepala_toko','LEFT');
		$this->db->group_by('tk.kode_toko');
		$this->db->order_by('id_toko','DESC');
		$query=$this->db->get();
		return $query->result();
	}

	// Dapatkan kode terakhir
	public function akhir() {
		$this->db->select('*');
		$this->db->from('vamr4846_toko_mrc.toko');
		$this->db->order_by('id_toko','DESC');
		$query=$this->db->get();
		return $query->row();
	}		

	private function _get_datatables_query()
	{		
		$this->db->select('tk.*, pt.nama_pegawai_toko');
		$this->db->from('vamr4846_toko_mrc.toko AS tk');
		$this->db->join('vamr4846_toko_mrc.pegawai_toko AS pt','pt.kode_pegawai_toko=tk.kepala_toko','LEFT');
		
		$i = 0;
	
		foreach ($this->column_search as $item){
			if($_POST['search']['value']){
				if($i===0){
					$this->db->group_start();
					$this->db->like($item, $_POST['search']['value']);
				}else{
					$this->db->or_like($item, $_POST['search']['value']);
				}

				if(count($this->column_search) - 1 == $i)
					$this->db->group_end();
			}
			$i++;
		}
		
		if(isset($_POST['order'])){
			$this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		}else if(isset($this->order)){
			$order = $this->order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}

	function get_datatables()
	{
		$this->_get_datatables_query();
		if($_POST['length'] != -1)
		$this->db->limit($_POST['length'], $_POST['start']);
		$query = $this->db->get();
		return $query->result();
	}

	function count_filtered()
	{
		$this->_get_datatables_query();
		$query = $this->db->get();
		return $query->num_rows();
	}

	public function count_all()
	{
		$this->db->from($this->table);
		return $this->db->count_all_results();
	}

	public function get_by_id($id_toko)
	{
		$this->db->from($this->table);
		$this->db->where('id_toko',$id_toko);
		$query = $this->db->get();

		return $query->row();
	}

	public function save($data)
	{
		$this->db->insert($this->table, $data);
		return $this->db->insert_id();
	}

	public function update($where, $data)
	{
		$this->db->update($this->table, $data, $where);
		return $this->db->affected_rows();
	}

	public function delete_by_id($id_toko)
	{
		$this->db->where('id_toko', $id_toko);
		$this->db->delete($this->table);
	}


}

<?php
class Paket_promo_master_model extends CI_Model
{
	// Retur pembelian induk
	var $column_order         = array('pm.id_paket_promo_m', 'pm.id_paket_promo_m', 'pm.kode_paket_promo', 'pm.nama_paket_promo');
	var $column_search        = array('pm.kode_paket_promo', 'pm.nama_paket_promo'); 
	var $order                = array('pm.id_paket_promo_m' => 'desc');

	// Retur pembelian detail
	var $column_order_detail  = array('pd.id_paket_promo_d', 'pd.id_paket_promo_d');
	var $column_search_detail = array('pm.kode_paket_promo', 'pm.nama_paket_promo', 'bp.sku', 'bp.nama_barang'); 
	var $order_detail         = array('pd.id_paket_promo_d' => 'DESC');

	// Retur pembelian batal
	var $column_order_batal   = array('pd.id_paket_promo_d', 'pd.id_paket_promo_d');
	var $column_search_batal  = array('pm.kode_paket_promo', 'pm.nama_paket_promo', 'bp.sku', 'bp.nama_barang', 'pd.keterangan_hapus'); 
	var $order_batal          = array('pd.id_paket_promo_d' => 'DESC');

	private function _get_laporan_query($status_paket, $kode_toko)
	{		
		$this->db->select('
			pm.*,
			kp.nama_kategori_paket,
			SUM((bt.harga_eceran) * pd.qty) AS harga_dasar,
			SUM((bt.harga_eceran - pd.discount_harga) * pd.qty) AS harga_paket,
			COUNT(pd.id_barang_pusat) AS jml_item,
			pt1.nama_pegawai_toko AS pegawai_save, pt2.nama_pegawai_toko AS pegawai_edit
		');
		$this->db->from('vamr4846_toko_mrc.paket_master AS pm');
		$this->db->join('vamr4846_toko_mrc.paket_detail AS pd','pd.id_paket_promo_m=pm.id_paket_promo_m AND pd.status_hapus="TIDAK"','LEFT');
		$this->db->join('vamr4846_toko_mrc.barang_toko_'.$kode_toko.' AS bt','bt.id_barang_pusat=pd.id_barang_pusat','LEFT');
		$this->db->join('vamr4846_toko_mrc.kategori_paket as kp','kp.id_kategori_paket=pm.id_kategori_paket','LEFT');
		$this->db->join('vamr4846_toko_mrc.pegawai_toko as pt1','pt1.id_pegawai_toko=pm.id_pegawai_pembuatan','LEFT');
		$this->db->join('vamr4846_toko_mrc.pegawai_toko as pt2','pt2.id_pegawai_toko=pm.id_pegawai_pembaharuan','LEFT');
		$this->db->where(array(
			'pm.status_hapus' => 'TIDAK'
		));
		$this->db->like('pm.status_paket', $status_paket);
		$this->db->like('pm.kode_toko', $kode_toko);		
		$this->db->group_by('pm.id_paket_promo_m');

		$i = 0;
	
		foreach ($this->column_search as $item){
			if($_POST['search']['value']){
				if($i===0){
					$this->db->group_start();
					$this->db->like($item, $_POST['search']['value']);
				}else{
					$this->db->or_like($item, $_POST['search']['value']);
				}

				if(count($this->column_search) - 1 == $i)
					$this->db->group_end();
			}
			$i++;
		}

		if(isset($_POST['order'])){
			if($this->session->userdata('usergroup_name') == 'Super Admin'){
				$this->db->order_by($this->column_order_a[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
			}else{
				$this->db->order_by($this->column_order_b[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
			}
		}else if(isset($this->order)){
			$order = $this->order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}

	private function _get_laporan_query_detail($status_paket, $kode_toko)
	{		
		$this->db->select('
			pd.*, 
			pm.kode_paket_promo, pm.nama_paket_promo, pm.status_paket,
			pm.tanggal_awal_paket, pm.tanggal_akhir_paket,
			bp.sku, bp.nama_barang, bt.harga_eceran,
			pt1.nama_pegawai_toko AS pegawai_save, pt2.nama_pegawai_toko AS pegawai_edit
		');
		$this->db->from('vamr4846_toko_mrc.paket_detail AS pd');
		$this->db->join('vamr4846_toko_mrc.paket_master AS pm','pd.id_paket_promo_m=pm.id_paket_promo_m','LEFT');
		$this->db->join('vamr4846_toko_mrc.barang_toko_'.$kode_toko.' AS bt','bt.id_barang_pusat=pd.id_barang_pusat','LEFT');
		$this->db->join('vamr4846_vama.barang_pusat as bp','bp.id_barang_pusat=pd.id_barang_pusat','LEFT');
		$this->db->join('vamr4846_toko_mrc.pegawai_toko as pt1','pt1.id_pegawai_toko=pm.id_pegawai_pembuatan','LEFT');
		$this->db->join('vamr4846_toko_mrc.pegawai_toko as pt2','pt2.id_pegawai_toko=pm.id_pegawai_pembaharuan','LEFT');
		$this->db->where(array(
			'pd.status_hapus' => 'TIDAK'
		));
		$this->db->like('pm.status_paket', $status_paket);
		$this->db->like('pm.kode_toko', $kode_toko);	

		$i = 0;	
		foreach ($this->column_search_detail as $item){
			if($_POST['search']['value']){
				if($i===0){
					$this->db->group_start();
					$this->db->like($item, $_POST['search']['value']);
				}else{
					$this->db->or_like($item, $_POST['search']['value']);
				}

				if(count($this->column_search_detail) - 1 == $i)
					$this->db->group_end();
			}
			$i++;
		}
		
		if(isset($_POST['order'])){
			if($this->session->userdata('usergroup_name') == 'Super Admin'){
				$this->db->order_by($this->column_order_detail_a[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
			}else{
				$this->db->order_by($this->column_order_detail_b[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
			}
		}else if(isset($this->order_detail)){
			$order = $this->order_detail;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}

	private function _get_laporan_query_batal($status_paket, $kode_toko)
	{		
		$this->db->select('
			pd.*, 
			pm.kode_paket_promo, pm.nama_paket_promo, pm.status_paket,
			pm.tanggal_awal_paket, pm.tanggal_akhir_paket,
			bp.sku, bp.nama_barang, bt.harga_eceran,
			pt1.nama_pegawai_toko AS pegawai_save, pt2.nama_pegawai_toko AS pegawai_edit, pt3.nama_pegawai_toko AS pegawai_hapus
		');
		$this->db->from('vamr4846_toko_mrc.paket_detail AS pd');
		$this->db->join('vamr4846_toko_mrc.paket_master AS pm','pd.id_paket_promo_m=pm.id_paket_promo_m','LEFT');
		$this->db->join('vamr4846_toko_mrc.barang_toko_'.$kode_toko.' AS bt','bt.id_barang_pusat=pd.id_barang_pusat','LEFT');
		$this->db->join('vamr4846_vama.barang_pusat as bp','bp.id_barang_pusat=pd.id_barang_pusat','LEFT');
		$this->db->join('vamr4846_toko_mrc.pegawai_toko as pt1','pt1.id_pegawai_toko=pm.id_pegawai_pembuatan','LEFT');
		$this->db->join('vamr4846_toko_mrc.pegawai_toko as pt2','pt2.id_pegawai_toko=pm.id_pegawai_pembaharuan','LEFT');
		$this->db->join('vamr4846_toko_mrc.pegawai_toko as pt3','pt3.id_pegawai_toko=pm.id_pegawai_hapus','LEFT');
		$this->db->where(array(
			'pd.status_hapus' => 'IYA'
		));
		$this->db->like('pm.kode_toko', $kode_toko);
		
		$i = 0;	
		foreach ($this->column_search_batal as $item){
			if($_POST['search']['value']){
				if($i===0){
					$this->db->group_start();
					$this->db->like($item, $_POST['search']['value']);
				}else{
					$this->db->or_like($item, $_POST['search']['value']);
				}

				if(count($this->column_search_batal) - 1 == $i)
					$this->db->group_end();
			}
			$i++;
		}
		
		if(isset($_POST['order'])){
			if($this->session->userdata('usergroup_name') == 'Super Admin'){
				$this->db->order_by($this->column_order_batal_a[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
			}else{
				$this->db->order_by($this->column_order_batal_b[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
			}
		}else if(isset($this->order_batal)){
			$order = $this->order_batal;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}

	function get_laporan($perintah, $status_paket, $kode_toko)
	{
		$this->$perintah($status_paket, $kode_toko);
		if($_POST['length'] != -1)
		$this->db->limit($_POST['length'], $_POST['start']);
		$query = $this->db->get();
		return $query->result();
	}

	function count_filtered($perintah, $status_paket, $kode_toko)
	{
		$this->$perintah($status_paket, $kode_toko);
		$query = $this->db->get();
		return $query->num_rows();
	}

	public function count_all($table, $status_hapus)
	{
		$this->db->from($table);
		if($status_hapus == 'TIDAK'){
			$this->db->where('status_hapus', 'TIDAK');
		}else if($status_hapus == 'IYA'){
			$this->db->where('status_hapus', 'IYA');
		}
		return $this->db->count_all_results();
	}

	function kode_paket_promo_baru($tahun_sekarang, $bulan_sekarang, $id_pegawai, $kode_toko, $tipe_paket){
	    $this->db->select('CAST(RIGHT(kode_paket_promo, 4)AS INT) + 1 AS no_baru');
	    $this->db->from('vamr4846_toko_mrc.paket_master');
	    $this->db->where(array(  
				'kode_toko'  => $kode_toko,
				'tipe_paket' => $tipe_paket
	    ));
	    $this->db->order_by('tanggal_pembuatan','DESC');
	    $query = $this->db->get();
	    return $query->row();
	}

	function no_retur_pembelian_detail_baru($id_retur_pembelian_m, $kode_toko) {
		$this->db->select('CAST(RIGHT(no_retur_pembelian, 4)AS INT) + 1 AS no_retur_pembelian_detail_baru');
		$this->db->from('vamr4846_toko_mrc.retur_pembelian_detail_'.$kode_toko.'');
		$this->db->where('id_retur_pembelian_m',$id_retur_pembelian_m);
		$this->db->order_by('tanggal_pembuatan','DESC');
		$query = $this->db->get();
		return $query->row();
	}

	function insert_master(
		$kode_paket_promo, $nama_paket_promo, $tipe_paket, $id_kategori_paket, $id_jenis_paket, $tanggal_awal, $tanggal_akhir,
		$catatan, $tanggal, $id_pegawai, $id_toko, $kode_toko
	){
		$dt = array(
			'id_toko'              => $id_toko,
			'kode_toko'            => $kode_toko,
			'kode_paket_promo'     => $kode_paket_promo,
			'nama_paket_promo'     => $nama_paket_promo,
			'tipe_paket'           => $tipe_paket,
			'jenis_paket'          => $id_jenis_paket,
			'status_paket'         => 'MENUNGGU',
			'id_kategori_paket'    => $id_kategori_paket,
			'tanggal_awal_paket'   => $tanggal_awal,
			'tanggal_akhir_paket'  => $tanggal_akhir,
			'keterangan_lain'      => $catatan,
			'id_pegawai_pembuatan' => (empty($id_pegawai)) ? NULL : $id_pegawai,
			'tanggal_pembuatan'    => date('Y-m-d H:i:s')
		);
		return $this->db->insert('vamr4846_toko_mrc.paket_master', $dt);
	}

	function update_master(
		$id_master, $nama_paket_promo, $tipe_paket, $id_kategori_paket, $id_jenis_paket, $tanggal_awal, $tanggal_akhir, $status_paket,
		$catatan, $id_pegawai, $kode_toko
	){
		$dt = array(
			'nama_paket_promo'       => $nama_paket_promo,
			'tipe_paket'             => $tipe_paket,
			'jenis_paket'            => $id_jenis_paket,
			'tanggal_awal_paket'     => $tanggal_awal,
			'tanggal_akhir_paket'    => $tanggal_akhir,
			'status_paket'           => $status_paket,
			'id_kategori_paket'      => $id_kategori_paket,
			'keterangan_lain'        => $catatan,
			'id_pegawai_pembaharuan' => (empty($id_pegawai)) ? NULL: $id_pegawai,
			'tanggal_pembaharuan'    => date('Y-m-d H:i:s')
		);
		$where = array(
			'id_paket_promo_m' => $id_master
		);
		return $this->db->update('vamr4846_toko_mrc.paket_master', $dt, $where);
	}

	function get_by_kode_paket($kode_paket_promo, $kode_toko)
	{
		$this->db->select('*');
		$this->db->from('vamr4846_toko_mrc.paket_master');
		$this->db->where('kode_paket_promo', $kode_paket_promo);
		$query = $this->db->get();
		return $query->row();
	}

	function get_by_id($id_paket_promo_m){
		$this->db->select('*');
		$this->db->from('vamr4846_toko_mrc.paket_master AS pm');
		$this->db->where('id_paket_promo_m', $id_paket_promo_m);
		$query = $this->db->get();
		return $query->row();
	}

	function get_jumlah_jual($id_retur_pembelian_m, $kode_toko){
		$this->db->select('count(*) as jumlah_jual');
		$this->db->from('vamr4846_toko_mrc.retur_pembelian_detail_'.$kode_toko.'');
		$this->db->where('id_retur_pembelian_m', $id_retur_pembelian_m);
		$query = $this->db->get();
		return $query->row();
	}

	function get_master($id_paket_promo_m){
		$ambil_detail = $this->db->query("
			SELECT kode_toko 
			FROM vamr4846_toko_mrc.paket_master 
			WHERE id_paket_promo_m =".$id_paket_promo_m."
		");
		$data_master = $ambil_detail->row();
		if($data_master){			
			$kode_toko = $data_master->kode_toko; 
		}else{
			return;
		}

		$this->db->select('
			pm.*,
			SUM(bt.harga_eceran*pd.qty) AS total_dasar,
			SUM((bt.harga_eceran-pd.discount_harga)*pd.qty) AS total_bersih,
			kp.id_kategori_paket, kp.nama_kategori_paket,
			COUNT(pd.id_paket_promo_d) AS jumlah_barang
		');
		$this->db->from('vamr4846_toko_mrc.paket_master AS pm');
		$this->db->join('vamr4846_toko_mrc.paket_detail AS pd', 'pm.id_paket_promo_m=pd.id_paket_promo_m', 'LEFT');			
		$this->db->join('vamr4846_toko_mrc.barang_toko_'.$kode_toko.' AS bt', 'pd.id_barang_pusat=bt.id_barang_pusat', 'LEFT');
		$this->db->join('vamr4846_toko_mrc.kategori_paket AS kp', 'pm.id_kategori_paket=kp.id_kategori_paket', 'LEFT');		
		$this->db->where(array(
			'pm.id_paket_promo_m' => $id_paket_promo_m,
			'pd.status_hapus'     => 'TIDAK',
			'pm.status_hapus'     => 'TIDAK'
		));
		// $this->db->group_by('pm.id_paket_promo_m');
		$query = $this->db->get();
		return $query->row();
	}

	function get_jumlah_batal($id_paket_promo_m)
	{
		$this->db->select('id_paket_promo_m, count(id_paket_promo_d) as jumlah_barang');
		$this->db->from('vamr4846_toko_mrc.paket_detail');
		$this->db->where('id_paket_promo_m', $id_paket_promo_m);
		$this->db->where('status_hapus', 'IYA');
		$query = $this->db->get();
		return $query->row();
	}

	function hapus_transaksi($id_paket_promo_m, $id_pegawai_pembatalan, $keterangan_batal){
		$dt = array ( 	
			'status_hapus'     => 'IYA',
			'keterangan_hapus' => $keterangan_batal,
			'id_pegawai_hapus' => $id_pegawai_pembatalan,
			'tanggal_hapus'    => date('Y-m-d H:i:s')
		);

		$where = array (	
			'id_paket_promo_m' => $id_paket_promo_m
		);

		return $this->db->update('vamr4846_toko_mrc.paket_master', $dt, $where);
	}

	function get_grand_total($status_paket, $kode_toko)
	{
		$this->db->select('
			COUNT(pm.id_retur_pembelian_m) AS jml_transaksi, 
			SUM(pd.subtotal) AS grand_total_retur_pembelian
		');
		$this->db->from('vamr4846_toko_mrc.retur_pembelian_master_'.$kode_toko.' AS pm');
		$this->db->join('vamr4846_toko_mrc.retur_pembelian_detail_'.$kode_toko.' AS pd', 'pd.id_retur_pembelian_m=pm.id_retur_pembelian_m', 'LEFT');
		$this->db->where('DATE(tanggal_retur) BETWEEN "'. date('Y-m-d', strtotime($tanggal_awal)). '" and "'. date('Y-m-d', strtotime($tanggal_akhir)).'"');
		$query = $this->db->get();
		return $query->row();
	}

	function get_total_perbulan($status_paket, $kode_toko)
	{
		$this->db->select('
			IFNULL(MIN(DATE_FORMAT(tanggal_retur, "%d-%m-%y")),"00-00-00") AS tanggal_awal,
			IFNULL(MAX(DATE_FORMAT(tanggal_retur, "%d-%m-%y")),"00-00-00") AS tanggal_akhir,
			COUNT(id_retur_pembelian_m) AS jml_transaksi, 
			IFNULL(SUM(grand_total),0) AS grand_total
		');
		$this->db->from('vamr4846_toko_mrc.retur_pembelian_master_'.$kode_toko.'');
		$this->db->where(array(
			'tanggal_retur >' => $tanggal_awal,
			'tanggal_retur <=' => $tanggal_akhir
		));
		$query = $this->db->get();
		return $query->row();
	}
}
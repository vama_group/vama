<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Stok_opname_pusat_model extends MY_Model {
	var $table = 'vamr4846_vama.stok_opname_pusat';

	// ---------------------------------------------------------------------------------------------------------------------------------------
	// Stok opname pusat
	var $column_order  = array('so.id_barang_pusat', 'so.id_barang_pusat', 'bp.sku', 'bp.nama_barang', 
							   'bp.harga_eceran', 'so.jumlah_so', 'bp.harga_eceran', 
							   'so.tanggal_pembuatan', 'so.tanggal_pembaharuan'); 	
	var $column_search = array('so.id_barang_pusat', 'bp.sku', 'bp.nama_barang','so.jumlah_so', 'so.catatan');
	var $order         = array('so.tanggal_pembaharuan' => 'DESC');
	// ---------------------------------------------------------------------------------------------------------------------------------------

	// Riwayat stok opname pusat
	var $column_order_rso 	= array('rso.id_barang_pusat', 'bp.sku', 'bp.nama_barang', 'rso.jumlah_so_sebelumnya', 'rso.jumlah_so',
							  		'rso.nama_pegawai', 'rso.tanggal_pembuatan'); 	
	var $column_search_rso 	= array('rso.id_barang_pusat', 'bp.sku', 'bp.nama_barang','rso.jumlah_so', 'rso.catatan'); 		
	var $order_rso 			= array('rso.tanggal_pembuatan' => 'DESC');
	// ---------------------------------------------------------------------------------------------------------------------------------------

	// Stok opname pusat batal
	var $column_order_sob 	= array('sob.id_barang_pusat', 'bp.sku', 'bp.nama_barang', 'sob.jumlah_so', 'sob.keterangan_batal',
							  		'sob.nama_pegawai', 'sob.tanggal_pembuatan'); 	
	var $column_search_sob 	= array('sob.id_barang_pusat', 'bp.sku', 'bp.nama_barang','sob.jumlah_so','sob.keterangan_batal'); 		
	var $order_sob 			= array('sob.tanggal_pembuatan' => 'DESC');
	// ---------------------------------------------------------------------------------------------------------------------------------------

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	private function _get_datatables_query($id_periode_stok_pusat)
	{
		$this->db->select('
			so.*,
			bp.sku, bp.nama_barang, bp.harga_eceran, bp.foto,
			pp1.nama_pegawai AS pegawai_save, pp2.nama_pegawai AS pegawai_edit
		');
		$this->db->from('vamr4846_vama.stok_opname_pusat AS so');
		$this->db->join('vamr4846_vama.barang_pusat AS bp', 'bp.id_barang_pusat=so.id_barang_pusat', 'LEFT');
		$this->db->join('vamr4846_vama.pegawai_pusat AS pp1', 'pp1.id_pegawai=so.id_pegawai_pembuatan', 'LEFT');
		$this->db->join('vamr4846_vama.pegawai_pusat AS pp2', 'pp2.id_pegawai=so.id_pegawai_pembaharuan', 'LEFT');
		$this->db->where('so.id_periode_stok_pusat', $id_periode_stok_pusat);

		$i = 0;
	
		foreach ($this->column_search as $item){
			if($_POST['search']['value']){
				if($i===0){
					$this->db->group_start();
					$this->db->like($item, $_POST['search']['value']);
				}else{
					$this->db->or_like($item, $_POST['search']['value']);
				}

				if(count($this->column_search) - 1 == $i)
					$this->db->group_end();
			}
			$i++;
		}
		
		if(isset($_POST['order'])){
			$this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		}else if(isset($this->order)){
			$order = $this->order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}

	private function _get_datatables_query_riwayat_so($id_periode_stok_pusat)
	{
		$this->db->select('rso.*, 
						   bp.sku, bp.nama_barang, bp.foto, pp.nama_pegawai');
		$this->db->from('vamr4846_vama.riwayat_stok_opname_pusat AS rso');
		$this->db->join('vamr4846_vama.barang_pusat AS bp', 'bp.id_barang_pusat=rso.id_barang_pusat', 'LEFT');
		$this->db->join('vamr4846_vama.pegawai_pusat AS pp','pp.id_pegawai=rso.id_pegawai', 'LEFT');
		$this->db->where('rso.id_periode_stok_pusat', $id_periode_stok_pusat);

		$i = 0;
	
		foreach ($this->column_search_rso as $item){
			if($_POST['search']['value']){
				if($i===0){
					$this->db->group_start();
					$this->db->like($item, $_POST['search']['value']);
				}else{
					$this->db->or_like($item, $_POST['search']['value']);
				}

				if(count($this->column_search_rso) - 1 == $i)
				$this->db->group_end();
			}
			$i++;
		}
		
		if(isset($_POST['order'])){
			$this->db->order_by($this->column_order_rso[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		}else if(isset($this->order_rso)){
			$order = $this->order_rso;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}

	private function _get_datatables_query_batal_so($id_periode_stok_pusat)
	{
		$this->db->select('
			sob.*, 
			bp.sku, bp.nama_barang, bp.foto,
			pp1.nama_pegawai AS pegawai_save, pp2.nama_pegawai AS pegawai_edit, pp3.nama_pegawai AS pegawai_batal
		');
		$this->db->from('vamr4846_vama.stok_opname_pusat_batal AS sob');
		$this->db->join('vamr4846_vama.barang_pusat AS bp', 'bp.id_barang_pusat=sob.id_barang_pusat', 'LEFT');
		$this->db->join('vamr4846_vama.pegawai_pusat AS pp1', 'pp1.id_pegawai=sob.id_pegawai_pembuatan', 'LEFT');
		$this->db->join('vamr4846_vama.pegawai_pusat AS pp2', 'pp2.id_pegawai=sob.id_pegawai_pembaharuan', 'LEFT');
		$this->db->join('vamr4846_vama.pegawai_pusat AS pp3', 'pp3.id_pegawai=sob.id_pegawai_pembatalan', 'LEFT');
		$this->db->where('sob.id_periode_stok_pusat', $id_periode_stok_pusat);

		$i = 0;
	
		foreach ($this->column_search_sob as $item){
			if($_POST['search']['value']){
				if($i===0){
					$this->db->group_start();
					$this->db->like($item, $_POST['search']['value']);
				}else{
					$this->db->or_like($item, $_POST['search']['value']);
				}

				if(count($this->column_search_sob) - 1 == $i)
				$this->db->group_end();
			}
			$i++;
		}
		
		if(isset($_POST['order'])){
			$this->db->order_by($this->column_order_sob[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		}else if(isset($this->order_sob)){
			$order = $this->order_sob;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}

	function get_datatables($id_periode_stok_pusat)
	{
		$this->_get_datatables_query($id_periode_stok_pusat);
		if($_POST['length'] != -1)
		$this->db->limit($_POST['length'], $_POST['start']);
		$query = $this->db->get();
		return $query->result();
	}

	function get_datatables_riwayat($id_periode_stok_pusat)
	{
		$this->_get_datatables_query_riwayat_so($id_periode_stok_pusat);
		if($_POST['length'] != -1)
		$this->db->limit($_POST['length'], $_POST['start']);
		$query = $this->db->get();
		return $query->result();
	}

	function get_datatables_batal($id_periode_stok_pusat)
	{
		$this->_get_datatables_query_batal_so($id_periode_stok_pusat);
		if($_POST['length'] != -1)
		$this->db->limit($_POST['length'], $_POST['start']);
		$query = $this->db->get();
		return $query->result();
	}

	function count_filtered($query, $id_periode_stok_pusat)
	{
		$this->$query($id_periode_stok_pusat);
		$query = $this->db->get();
		return $query->num_rows();
	}

	public function count_all($table)
	{
		$this->db->from('vamr4846_vama.stok_opname_pusat');
		return $this->db->count_all_results();
	}

	public function listing() {
		$this->db->select('so.*, 
						   bp.sku, bp.nama_barang, bp.foto');
		$this->db->from('vamr4846_vama.stok_opname_pusat AS so');
		$this->db->join('vamr4846_vama.barang_pusat AS bp','bp.id_barang_pusat=so.id_barang_pusat','LEFT');
		$this->db->order_by('so.id_stok_opname_pusat','DESC');
		$query=$this->db->get();
		return $query->result();
	}

	public function daftar_so_per_periode($id_periode_stok_pusat) {
		$this->db->select('so.*, 
						   bp.sku, bp.nama_barang, bp.harga_eceran, bp.foto, 
						   pp1.nama_pegawai AS pegawai_save, pp2.nama_pegawai AS pegawai_edit');
		$this->db->from('vamr4846_vama.stok_opname_pusat AS so');
		$this->db->join('vamr4846_vama.barang_pusat AS bp','bp.id_barang_pusat=so.id_barang_pusat','LEFT');
		$this->db->where('id_periode_stok_pusat', $id_periode_stok_pusat);
		$this->db->join('vamr4846_vama.pegawai_pusat AS pp1', 'pp1.id_pegawai=so.id_pegawai_pembuatan', 'LEFT');
		$this->db->join('vamr4846_vama.pegawai_pusat AS pp2', 'pp2.id_pegawai=so.id_pegawai_pembaharuan', 'LEFT');
		$this->db->order_by('so.id_stok_opname_pusat','DESC');
		$query=$this->db->get();
		return $query->result();
	}

	public function get_by_id($id_stok_opname_pusat)
	{		
		$this->db->select('so.*, 
						   bp.sku, bp.nama_barang, bp.harga_eceran, bp.foto, 
						   bp.total_stok, bp.total_stok_rusak, bp.status');
		$this->db->from('vamr4846_vama.stok_opname_pusat AS so');
		$this->db->join('vamr4846_vama.barang_pusat AS bp','bp.id_barang_pusat=so.id_barang_pusat','LEFT');
		$this->db->where('id_stok_opname_pusat', $id_stok_opname_pusat);
		$query = $this->db->get();
		return $query->row();
	}

	public function get_by_sku($sku, $id_periode_stok_pusat)
	{		
		$this->db->select('bp.id_barang_pusat, bp.sku, bp.nama_barang, 
						   bp.harga_eceran, bp.foto,
						   IFNULL(so.jumlah_so, "0") AS jumlah_so_sebelumnya, 
						   bp.total_stok, bp.total_stok_rusak, bp.status');
		$this->db->from('vamr4846_vama.barang_pusat AS bp');
		$this->db->join('vamr4846_vama.stok_opname_pusat AS so',
						'bp.id_barang_pusat=so.id_barang_pusat AND so.id_periode_stok_pusat='.$id_periode_stok_pusat.'',
						'LEFT');
		$this->db->where(array(
			'bp.sku' => $sku
		));
		$query = $this->db->get();
		return $query->row();
	}

	public function get_by_id_barang($id_barang_pusat, $id_periode_stok_pusat, $masuk_stok)
	{
		$this->db->select(
			'so.*, 
			bp.sku, bp.nama_barang, bp.foto, bp.status,
			bp.total_stok, bp.total_stok_rusak');
		$this->db->from('vamr4846_vama.stok_opname_pusat AS so');
		$this->db->join('vamr4846_vama.barang_pusat AS bp', 'bp.id_barang_pusat=so.id_barang_pusat', 'LEFT');
		$this->db->where(array(
			'so.id_barang_pusat'    => $id_barang_pusat,
			'so.masuk_stok'         => $masuk_stok,
			'id_periode_stok_pusat' => $id_periode_stok_pusat
		));
		$query = $this->db->get();
		return $query->row();
	}	

	public function update_stok($id_barang_pusat, $jumlah_so, $jumlah_so_sebelumnya, $masuk_stok)
	{
		if($masuk_stok === 'JUAL'){
			$sql = "UPDATE vamr4846_vama.barang_pusat SET total_stok = total_stok - ".$jumlah_so_sebelumnya." 
					WHERE id_barang_pusat = '".$id_barang_pusat."'";
		}else{
			$sql = "UPDATE vamr4846_vama.barang_pusat SET total_stok_rusak = total_stok_rusak - ".$jumlah_so_sebelumnya." 
					WHERE id_barang_pusat = '".$id_barang_pusat."'";
		}
		$this->db->query($sql);

		if($masuk_stok === 'JUAL'){
			$sql = "UPDATE vamr4846_vama.barang_pusat SET total_stok = total_stok + ".$jumlah_so.", status='AKTIF' 
					WHERE id_barang_pusat = '".$id_barang_pusat."'";
		}else{
			$sql = "UPDATE vamr4846_vama.barang_pusat SET total_stok_rusak = total_stok_rusak + ".$jumlah_so.", status='AKTIF' 
					WHERE id_barang_pusat = '".$id_barang_pusat."'";
		}
		$this->db->query($sql);
	}

	public function save($data, $table)
	{
		$this->db->insert($table, $data);
		return $this->db->insert_id();
	}

	public function update($where, $data)
	{
		$this->db->update($this->table, $data, $where);
		return $this->db->affected_rows();
	}

	public function delete_by_id($id_stok_opname_pusat, $id_barang_pusat, $jumlah_so, $masuk_stok)
	{
		if($masuk_stok == "JUAL"){
			$sql = "UPDATE vamr4846_vama.barang_pusat SET total_stok = total_stok - ".$jumlah_so." 
					WHERE id_barang_pusat = '".$id_barang_pusat."'";
		}else if($masuk_stok == "RUSAK"){
			$sql = "UPDATE vamr4846_vama.barang_pusat SET total_stok_rusak = total_stok_rusak - ".$jumlah_so." 
					WHERE id_barang_pusat = '".$id_barang_pusat."'";
		}

		$this->db->query($sql);

		$this->db->where('id_stok_opname_pusat', $id_stok_opname_pusat);
		$this->db->delete($this->table);
	}

	function get_total_opname_perbulan($tanggal_awal, $tanggal_akhir)
	{
		$this->db->select('
							IFNULL(MIN(DATE_FORMAT(tanggal_pembaharuan, "%d-%m-%y")),"00-00-00") AS tanggal_awal,
							IFNULL(MAX(DATE_FORMAT(tanggal_pembaharuan, "%d-%m-%y")),"00-00-00") AS tanggal_akhir,
							COUNT(id_stok_opname_pusat) AS total_barang_opname, 
							IFNULL(SUM(jumlah_so),0) AS total_qty_opname
					    ');
		$this->db->from('vamr4846_vama.stok_opname_pusat');
		$this->db->where(array(
								'DATE(tanggal_pembaharuan) >' 	=> $tanggal_awal,
								'DATE(tanggal_pembaharuan) <=' 	=> $tanggal_akhir,
						));
		$query = $this->db->get();
		return $query->row();
	}
}

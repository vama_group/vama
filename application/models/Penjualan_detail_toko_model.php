<?php
class Penjualan_detail_toko_model extends CI_Model
{
	// Penjualan induk
	var $column_order          = array('', '', 'bp.sku','bp.nama_barang', 'pd.harga_satuan', 'pd.discount', 
									   'pd.jumlah_beli', 'pd.subtotal');
	var $column_search         = array('bp.kode_barang','bp.sku','bp.nama_barang'); 
	var $order                 = array('pd.tanggal_pembaharuan' => 'DESC'); 
	
	// Penjualan batal
	var $column_order_batal    = array('', '', 'bp.sku','bp.nama_barang', 'pdb.jumlah_beli', 'pdb.subtotal');
	var $column_search_batal   = array('bp.kode_barang','bp.sku','bp.nama_barang', 'pdb.jumlah_beli'); 
	var $order_batal           = array('pdb.tanggal_pembatalan' => 'DESC');
	
	// Cari barang
	var $column_order_barang1  = array('', 'bp.sku','bp.nama_barang','bt.total_stok','bt.harga_eceran');
	var $column_search_barang1 = array('bp.kode_barang','bp.sku','bp.nama_barang'); 
	var $order_barang1         = array('pd.nama_barang' => 'ASC');
	
	private function _get_datatables_query($id_penjualan_m, $kode_toko)
	{		
		$this->db->select('	pd.id_penjualan_d, pd.harga_satuan, 
							(pd.discount * pd.harga_satuan) AS discount, pd.jumlah_beli, pd.subtotal,
							bp.sku, bp.kode_barang, bp.nama_barang,
							pt1.nama_pegawai_toko AS pegawai_save, IFNULL(pt2.nama_pegawai_toko, "-") AS pegawai_edit, 
							pd.tanggal_pembuatan, pd.tanggal_pembaharuan, 
							IFNULL(rpm.id_retur_penjualan_m, "-") AS id_retur_penjualan_m, 
							IFNULL(rpd.jumlah_retur, "0") AS jumlah_retur,
						 ');
		$this->db->from('vamr4846_toko_mrc.penjualan_detail_'.$kode_toko.' AS pd');
		$this->db->join('vamr4846_toko_mrc.penjualan_master_'.$kode_toko.' as pm', 'pd.id_penjualan_m=pm.id_penjualan_m', 'LEFT');
		$this->db->join('vamr4846_vama.barang_pusat as bp','bp.id_barang_pusat=pd.id_barang_pusat','LEFT');
		$this->db->join('vamr4846_toko_mrc.retur_penjualan_master_'.$kode_toko.' as rpm','rpm.id_penjualan_m=pm.id_penjualan_m', 'LEFT');
		$this->db->join('vamr4846_toko_mrc.retur_penjualan_detail_'.$kode_toko.' as rpd','rpd.id_retur_penjualan_m=rpm.id_retur_penjualan_m AND 
														 rpd.id_barang_pusat=pd.id_barang_pusat', 'LEFT');
		$this->db->join('vamr4846_toko_mrc.pegawai_toko as pt1','pt1.id_pegawai_toko=pd.id_pegawai_pembuatan','LEFT');
		$this->db->join('vamr4846_toko_mrc.pegawai_toko as pt2','pt2.id_pegawai_toko=pd.id_pegawai_pembaharuan','LEFT');
		$this->db->where('pd.id_penjualan_m', $id_penjualan_m);

		$i = 0;
		foreach ($this->column_search as $item){
			if($_POST['search']['value']){
				if($i===0){
					$this->db->group_start();
					$this->db->like($item, $_POST['search']['value']);
				}else{
					$this->db->or_like($item, $_POST['search']['value']);
				}

				if(count($this->column_search) - 1 == $i)
					$this->db->group_end();
			}
			$i++;
		}
		
		if(isset($_POST['order'])){
			$this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		}else if(isset($this->order)){
			$order = $this->order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}

	private function _get_datatables_query_batal($id_penjualan_m, $kode_toko)
	{		
		$this->db->select('	pdb.id_penjualan_b, pdb.id_penjualan_d, pdb.id_barang_pusat, pdb.harga_satuan, 
							(pdb.discount * pdb.harga_satuan) AS discount, pdb.jumlah_beli, pdb.subtotal,
							bp.sku, bp.kode_barang, bp.nama_barang, keterangan_batal, pt.nama_pegawai_toko, pdb.tanggal_pembatalan
						 ');
		$this->db->from('vamr4846_toko_mrc.penjualan_detail_batal_'.$kode_toko.' AS pdb');
		$this->db->join('vamr4846_vama.barang_pusat as bp','bp.id_barang_pusat=pdb.id_barang_pusat','LEFT');
		$this->db->join('vamr4846_toko_mrc.pegawai_toko as pt','pt.id_pegawai_toko=pdb.id_pegawai_pembatalan','LEFT');
		$this->db->where('pdb.id_penjualan_m', $id_penjualan_m);

		$i = 0;
		foreach ($this->column_search_batal as $item){
			if($_POST['search']['value']){
				if($i===0){
					$this->db->group_start();
					$this->db->like($item, $_POST['search']['value']);
				}else{
					$this->db->or_like($item, $_POST['search']['value']);
				}

				if(count($this->column_search_batal) - 1 == $i)
					$this->db->group_end();
			}
			$i++;
		}
		
		if(isset($_POST['order'])){
			$this->db->order_by($this->column_order_batal[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		}else if(isset($this->order_batal)){
			$order = $this->order_batal;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}

	private function _get_datatables_query_barang($id_penjualan_m, $kode_toko)
	{		
		$ambil_penjualan	= $this->db->query("SELECT id_barang_pusat 
												FROM vamr4846_toko_mrc.penjualan_detail_".$kode_toko." 
												WHERE id_penjualan_m =".$id_penjualan_m."");
  		$pd_id_barang 		= array();
		foreach ($ambil_penjualan->result() as $row){
			$pd_id_barang[] = $row->id_barang_pusat; 
		}
		$id_barang 			= implode(",", $pd_id_barang);
		$idp 				= explode(",", $id_barang);
		
		$this->db->select('	
			bp.id_barang_pusat, bt.id_barang_toko, bp.sku, bp.kode_barang, bp.nama_barang, bt.total_stok, bt.harga_eceran
		');
		$this->db->from('vamr4846_toko_mrc.barang_toko_'.$kode_toko.' AS bt');
		$this->db->join('vamr4846_vama.barang_pusat as bp', 'bt.id_barang_pusat=bp.id_barang_pusat');
		$this->db->where_not_in('bp.id_barang_pusat', $idp);
		$this->db->where('bp.total_stok >', 0);

		$i = 0;
	
		foreach ($this->column_search_barang1 as $item){
			if($_POST['search']['value']){
				if($i===0){
					$this->db->group_start();
					$this->db->like($item, $_POST['search']['value']);
				}else{
					$this->db->or_like($item, $_POST['search']['value']);
				}

				if(count($this->column_search_barang1) - 1 == $i)
				$this->db->group_end();
			}
			$i++;
		}
		
		if(isset($_POST['order'])){
			$this->db->order_by($this->column_order_barang1[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		}else if(isset($this->order_barang1)){
			$order = $this->order_barang1;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}

	function get_datatables($perintah, $id_penjualan_m, $kode_toko)
	{
		$this->$perintah($id_penjualan_m, $kode_toko);
		if($_POST['length'] != -1)
		$this->db->limit($_POST['length'], $_POST['start']);
		$query = $this->db->get();
		return $query->result();
	}

	function count_filtered($perintah, $id_penjualan_m, $kode_toko)
	{
		$this->$perintah($id_penjualan_m, $kode_toko);
		$query = $this->db->get();
		return $query->num_rows();
	}

	public function count_all($table, $id_penjualan_m, $kode_toko)
	{
		$this->db->from($table);
		if($table == 'vamr4846_toko_mrc.penjualan_detail_'.$kode_toko.'' or $table == 'vamr4846_toko_mrc.penjualan_detail_batal_'.$kode_toko.''){
			$this->db->where('id_penjualan_m',$id_penjualan_m);
		}
		return $this->db->count_all_results();
	}

	function get_penjualan_detail($id_penjualan_d, $kode_toko)
	{
		$this->db->select('pd.*, pm.no_penjualan, pm.id_customer_toko, 
						   pm.status_penjualan, bp.sku, bp.nama_barang');
		$this->db->from('vamr4846_toko_mrc.penjualan_detail_'.$kode_toko.' as pd');
		$this->db->join('vamr4846_toko_mrc.penjualan_master_'.$kode_toko.' as pm','pm.id_penjualan_m=pd.id_penjualan_m','LEFT');
		$this->db->join('vamr4846_vama.barang_pusat as bp','bp.id_barang_pusat=pd.id_barang_pusat','LEFT');
		$this->db->where('id_penjualan_d', $id_penjualan_d);
		$query = $this->db->get();
		return $query->row();
	}

	function get_penjualan_detail_all($id_penjualan_m, $kode_toko)
	{
		$this->db->select('pd.*, pm.no_penjualan, pm.status_penjualan');
		$this->db->from('vamr4846_toko_mrc.penjualan_detail_'.$kode_toko.' AS pd');
		$this->db->join('vamr4846_toko_mrc.penjualan_master_'.$kode_toko.' as pm','pm.id_penjualan_m=pd.id_penjualan_m','LEFT');
		$this->db->join('vamr4846_vama.barang_pusat as bp','bp.id_barang_pusat=pd.id_barang_pusat','LEFT');
		$this->db->where('pd.id_penjualan_m', $id_penjualan_m);
		$query = $this->db->get();
		return $query->result();
	}

	function get_penjualan_batal($id_penjualan_b, $kode_toko)
	{
		$this->db->select('pdb.*, bp.nama_barang, bp.sku');
		$this->db->from('vamr4846_toko_mrc.penjualan_detail_batal_'.$kode_toko.' AS pdb');
		$this->db->join('vamr4846_vama.barang_pusat AS bp', 'pdb.id_barang_pusat=bp.id_barang_pusat');
		$this->db->where('id_penjualan_b', $id_penjualan_b);
		$query = $this->db->get();
		return $query->row();
	}

	function get_id($id_penjualan_m, $id_barang_pusat, $kode_toko)
	{
		$this->db->select('pd.*');
		$this->db->from('vamr4846_toko_mrc.penjualan_detail_'.$kode_toko.' AS pd');
		$this->db->where(array(  
			'pd.id_penjualan_m'  => $id_penjualan_m,
			'pd.id_barang_pusat' => $id_barang_pusat
		));
		$query = $this->db->get();
		return $query->row();
	}	

	function insert_detail($id_penjualan_m, $id_barang_pusat, $jumlah_beli, $modal, $harga_satuan, $discount, $subtotal, $id_pegawai, $kode_toko)
	{ 
		$dt = array(	
			'id_penjualan_m'       => $id_penjualan_m,
			'id_barang_pusat'      => $id_barang_pusat,
			'jumlah_beli'          => $jumlah_beli,
			'modal'                => $modal,
			'harga_satuan'         => $harga_satuan,
			'discount'             => $discount,
			'subtotal'             => $subtotal,
			'id_pegawai_pembuatan' => $id_pegawai,
			'tanggal_pembuatan'    => date('Y-m-d H:i:s') 		
		);
		return $this->db->insert('vamr4846_toko_mrc.penjualan_detail_'.$kode_toko.'', $dt);
	}

	function update_detail(
		$id_penjualan_m, $id_barang_pusat, $jumlah_beli, 
		$modal, $harga_satuan, $discount, $subtotal, $id_pegawai, 
		$jumlah_beli_lama, $kode_toko)
	{
		$sql = "UPDATE vamr4846_vama.barang_pusat SET total_stok = total_stok + ".$jumlah_beli_lama." 
				WHERE id_barang_pusat = '".$id_barang_pusat."'";
		$this->db->query($sql);
		
		$dt = array(	
			'jumlah_beli'            => $jumlah_beli,
			'modal'                  => $modal,
			'harga_satuan'           => $harga_satuan,
			'discount'               => $discount,
			'subtotal'               => $subtotal,
			'id_pegawai_pembaharuan' => $id_pegawai
		);
		
		$where = array(	
			'id_penjualan_m'  => $id_penjualan_m,
			'id_barang_pusat' => $id_barang_pusat
		);

		return $this->db->update('vamr4846_toko_mrc.penjualan_detail_'.$kode_toko.'', $dt, $where);
	}

	function get_detail_faktur($id_penjualan_m, $kode_toko)
	{
		$this->db->select('	
			pd.id_penjualan_d, pd.harga_satuan, 
			pd.discount, pd.jumlah_beli, pd.subtotal,
			bp.sku, bp.kode_barang, bp.nama_barang
		');
		$this->db->from('vamr4846_toko_mrc.penjualan_detail_'.$kode_toko.' AS pd');
		$this->db->join('vamr4846_vama.barang_pusat as bp','bp.id_barang_pusat=pd.id_barang_pusat');
		$this->db->where('pd.id_penjualan_m',$id_penjualan_m);
		$this->db->order_by('bp.nama_barang','ASC');
		$query = $this->db->get();
		return $query->result();
	}

	function get_total($id_penjualan_m, $kode_toko)
	{
		$this->db->select('	
			id_penjualan_m,
			IFNULL(COUNT(id_penjualan_d),0) jumlah_barang,
			IFNULL(ROUND(SUM(((harga_satuan-(harga_satuan*discount))*jumlah_beli)),0),0) total
		');
		$this->db->from('vamr4846_toko_mrc.penjualan_detail_'.$kode_toko.'');
		$this->db->where('id_penjualan_m', $id_penjualan_m);
		$query = $this->db->get();
		return $query->row();
	}

	function hapus_penjualan_detail($id_penjualan_d, $id_penjualan_m, $no_penjualan, $id_customer_toko, 
									$id_barang_pusat, $modal, $harga_satuan, $discount, $jumlah_beli, $subtotal,
									$id_pegawai_pembuatan, $id_pegawai_pembaharuan, $id_pegawai, $tanggal_pembuatan, $tanggal_pembaharuan, 
									$keterangan_batal, $kode_toko, $status_penjualan)
	{
		// Kembalikan stok berdasarkan jumlah beli
		$sql = "UPDATE vamr4846_toko_mrc.barang_toko_".$kode_toko." SET total_stok = total_stok + ".$jumlah_beli." 
				WHERE id_barang_pusat = '".$id_barang_pusat."' ";
		$this->db->query($sql);

		// Simpan penjualan detail ke penjualan detail batal
		$sql = "INSERT INTO vamr4846_toko_mrc.penjualan_detail_batal_".$kode_toko."(
					id_penjualan_d, id_penjualan_m, no_penjualan, id_barang_pusat, id_customer_toko, 
					modal, harga_satuan, discount, jumlah_beli, subtotal, 
					id_pegawai_pembuatan, id_pegawai_pembaharuan, id_pegawai_pembatalan, tanggal_pembuatan, 
				 	tanggal_pembaharuan, keterangan_batal) 
				VALUES(	'".$id_penjualan_d."', '".$id_penjualan_m."', '".$no_penjualan."', '".$id_barang_pusat."', 
						'".$id_customer_toko."', 
				 		'".$modal."',  '".$harga_satuan."', '".$discount."', '".$jumlah_beli."', '".$subtotal."', 
				 		'".$id_pegawai_pembuatan."', '".$id_pegawai_pembaharuan."', '".$id_pegawai."', '".$tanggal_pembuatan."', 
				 		'".$tanggal_pembaharuan."', '".$keterangan_batal."')";
		$this->db->query($sql);

		// Hapus penjualan detail
		return $this->db
					->where('id_penjualan_d', $id_penjualan_d)
					->delete('vamr4846_toko_mrc.penjualan_detail_'.$kode_toko.'');
	}
}
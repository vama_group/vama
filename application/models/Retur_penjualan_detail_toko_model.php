<?php
class Retur_penjualan_detail_model extends CI_Model
{
	// Retur penjualan induk
	var $column_order        = array('bp.sku','bp.nama_barang');
	var $column_search       = array('bp.sku','bp.nama_barang'); 
	var $order               = array('rpd.tanggal_pembaharuan' => 'desc');
	// -------------------------------------------------------------------------------------------------------------------------------
	
	// Retur penjualan batal
	var $column_order_batal  = array('bp.sku','bp.nama_barang');
	var $column_search_batal = array('bp.sku','bp.nama_barang'); 
	var $order_batal         = array('rpdb.tanggal_pembaharuan' => 'desc'); 
	// -------------------------------------------------------------------------------------------------------------------------------

	private function _get_datatables_query($id, $kode_toko)
	{		
		$this->db->select('
			IFNULL(rpd.id_retur_penjualan_d,"-") AS id_retur_penjualan_d,
			pd.id_penjualan_d, pd.id_barang_pusat, bp.sku, bp.nama_barang, pd.harga_satuan, pd.jumlah_beli, 
			IFNULL(rpd.jumlah_retur,0)AS jumlah_retur, IFNULL(rpd.potongan_harga,0) AS potongan_harga, 
			IFNULL(rpd.subtotal_potongan,0) AS subtotal_potongan, IFNULL(rpd.subtotal_retur,0) AS subtotal_retur, 
			IFNULL(rpd.subtotal_saldo,0) AS subtotal_saldo, IFNULL(rpd.masuk_stok,"-") AS masuk_stok, 
			IFNULL(rpd.keterangan,"-") AS keterangan, rpm.status_retur, 
			pt1.nama_pegawai_toko AS pegawai_save, IFNULL(pt2.nama_pegawai_toko, "-") AS pegawai_edit, 
			rpd.tanggal_pembuatan, rpd.tanggal_pembaharuan
		');
		$this->db->from('vamr4846_toko_mrc.penjualan_detail_'.$kode_toko.' AS pd');
		$this->db->join('vamr4846_toko_mrc.penjualan_master_'.$kode_toko.' as pm','pm.id_penjualan_m=pd.id_penjualan_m','LEFT');
		$this->db->join('vamr4846_toko_mrc.retur_penjualan_master_'.$kode_toko.' as rpm','rpm.id_penjualan_m=pm.id_penjualan_m','LEFT');
		$this->db->join('vamr4846_toko_mrc.retur_penjualan_detail_'.$kode_toko.' as rpd','rpd.id_retur_penjualan_m=rpm.id_retur_penjualan_m AND 
														 rpd.id_barang_pusat=pd.id_barang_pusat','LEFT');
		$this->db->join('vamr4846_vama.barang_pusat as bp','bp.id_barang_pusat=pd.id_barang_pusat','LEFT');
		$this->db->join('vamr4846_toko_mrc.pegawai_toko as pt1','pt1.id_pegawai_toko=rpd.id_pegawai_pembuatan','LEFT');
		$this->db->join('vamr4846_toko_mrc.pegawai_toko as pt2','pt2.id_pegawai_toko=rpd.id_pegawai_pembaharuan','LEFT');
		$this->db->where('pm.id_penjualan_m', $id);

		$i = 0;	
		foreach ($this->column_search as $item){
			if($_POST['search']['value']){
				if($i===0){
					$this->db->group_start();
					$this->db->like($item, $_POST['search']['value']);
				}else{
					$this->db->or_like($item, $_POST['search']['value']);
				}

				if(count($this->column_search) - 1 == $i)
					$this->db->group_end();
			}
			$i++;
		}
		
		if(isset($_POST['order'])){
			$this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		}else if(isset($this->order)){
			$order = $this->order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}

	private function _get_datatables_query_batal($id, $kode_toko)
	{		
		$this->db->select('
							rpdb.id_retur_penjualan_b, rpdb.id_barang_pusat, 
							bp.sku, bp.nama_barang, rpdb.harga_satuan, rpdb.jumlah_beli, 
							rpdb.jumlah_retur, rpdb.potongan_harga, 
							rpdb.subtotal_potongan, rpdb.subtotal_retur, rpdb.subtotal_saldo, 
							rpdb.masuk_stok, rpdb.keterangan, rpdb.keterangan_batal,
							pt1.nama_pegawai_toko AS pegawai_pembatalan, rpdb.tanggal_pembatalan
						 ');
		$this->db->from('vamr4846_toko_mrc.retur_penjualan_detail_batal_'.$kode_toko.' AS rpdb');
		$this->db->join('vamr4846_vama.barang_pusat AS bp','bp.id_barang_pusat=rpdb.id_barang_pusat', 'LEFT');
		$this->db->join('vamr4846_toko_mrc.pegawai_toko as pt1','pt1.id_pegawai_toko=rpdb.id_pegawai_pembatalan', 'LEFT');
		$this->db->where('rpdb.id_retur_penjualan_m', $id);

		$i = 0;	
		foreach ($this->column_search_batal as $item){
			if($_POST['search']['value']){
				if($i===0){
					$this->db->group_start();
					$this->db->like($item, $_POST['search']['value']);
				}else{
					$this->db->or_like($item, $_POST['search']['value']);
				}

				if(count($this->column_search_batal) - 1 == $i)
					$this->db->group_end();
			}
			$i++;
		}
		
		if(isset($_POST['order'])){
			$this->db->order_by($this->column_order_batal[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		}else if(isset($this->order_batal)){
			$order = $this->order_batal;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}

	function get_datatables($perintah, $id, $kode_toko)
	{
		$this->$perintah($id, $kode_toko);
		if($_POST['length'] != -1)
		$this->db->limit($_POST['length'], $_POST['start']);
		$query = $this->db->get();
		return $query->result();
	}

	function count_filtered($perintah, $id, $kode_toko)
	{
		$this->$perintah($id, $kode_toko);
		$query = $this->db->get();
		return $query->num_rows();
	}

	public function count_all($table, $id, $kode_toko)
	{
		$this->db->from($table);
		$this->db->where('id_retur_penjualan_m', $id);
		return $this->db->count_all_results();
	}

	function get_retur_penjualan_detail($id_penjualan_d, $kode_toko)
	{
		$this->db->select('
			IFNULL(rpd.id_retur_penjualan_m,"-") AS id_retur_penjualan_m, IFNULL(rpd.id_retur_penjualan_d,"-") AS id_retur_penjualan_d,
			pd.id_penjualan_d, pd.id_barang_pusat, bp.sku, bp.nama_barang, 
			bt.total_stok, bt.total_stok_rusak, pd.harga_satuan, pd.jumlah_beli, 
			IFNULL(rpd.jumlah_retur,0)AS jumlah_retur, 
			IFNULL(rpd.potongan_harga_value,"0.00") AS potongan_harga_value, IFNULL(rpd.potongan_harga,0) AS potongan_harga, 
			IFNULL(rpd.subtotal_potongan,0) AS subtotal_potongan, IFNULL(rpd.subtotal_retur,0) AS subtotal_retur, 
			IFNULL(rpd.subtotal_saldo,0) AS subtotal_saldo, IFNULL(rpd.masuk_stok,"-") AS masuk_stok, 
			IFNULL(rpd.keterangan,"") AS keterangan, rpm.status_retur
		');
		$this->db->from('vamr4846_toko_mrc.penjualan_detail_'.$kode_toko.' AS pd');
		$this->db->join('vamr4846_toko_mrc.penjualan_master_'.$kode_toko.' as pm', 'pm.id_penjualan_m=pd.id_penjualan_m','LEFT');
		$this->db->join('vamr4846_toko_mrc.retur_penjualan_master_'.$kode_toko.' as rpm', 'rpm.id_penjualan_m=pm.id_penjualan_m','LEFT');
		$this->db->join('vamr4846_toko_mrc.retur_penjualan_detail_'.$kode_toko.' as rpd', 'rpd.id_retur_penjualan_m=rpm.id_retur_penjualan_m AND
			 											  rpd.id_barang_pusat=pd.id_barang_pusat','LEFT');
		$this->db->join('vamr4846_toko_mrc.barang_toko_'.$kode_toko.' AS bt', 'bt.id_barang_pusat=pd.id_barang_pusat');
		$this->db->join('vamr4846_vama.barang_pusat as bp','bp.id_barang_pusat=pd.id_barang_pusat','LEFT');
		$this->db->where('pd.id_penjualan_d', $id_penjualan_d);
		$query = $this->db->get();
		return $query->row();
	}

	function get_retur_penjualan_detail_all($id_retur_penjualan_m, $kode_toko)
	{
		$this->db->select('
			rpd.*, rpm.no_retur_penjualan, rpm.status_retur,
			pm.id_penjualan_m, pm.no_penjualan,
			rpm.id_customer_toko, cp.nama_customer_toko,
			bp.id_barang_pusat, bp.sku, bp.nama_barang, bp.total_stok, bp.total_stok_rusak
		');
		$this->db->from('vamr4846_toko_mrc.retur_penjualan_detail_'.$kode_toko.' as rpd');
		$this->db->join('vamr4846_toko_mrc.retur_penjualan_master_'.$kode_toko.' as rpm', 'rpd.id_retur_penjualan_m=rpm.id_retur_penjualan_m', 'LEFT');
		$this->db->join('vamr4846_toko_mrc.penjualan_master_'.$kode_toko.' as pm', 'pm.id_penjualan_m=rpm.id_penjualan_m', 'LEFT');
		$this->db->join('vamr4846_toko_mrc.customer_toko as cp','cp.id_customer_toko=rpm.id_customer_toko', 'LEFT');
		$this->db->join('vamr4846_toko_mrc.barang_toko_'.$kode_toko.' as bp','bp.id_barang_pusat=rpd.id_barang_pusat', 'LEFT');
		$this->db->where('rpd.id_retur_penjualan_m', $id_retur_penjualan_m);
		$query = $this->db->get();
		return $query->result();
	}

	function get_jml_barang($id_retur_penjualan_m, $kode_toko)
	{
		$this->db->select('COUNT(id_retur_penjualan_d) AS jumlah_barang');
		$this->db->from('vamr4846_toko_mrc.retur_penjualan_detail_'.$kode_toko.'');
		$this->db->where('id_retur_penjualan_m', $id_retur_penjualan_m);
		$query = $this->db->get();
		return $query->row();
	}

	function get_detail($id_retur_penjualan_d, $kode_toko)
	{
		$this->db->select('rpd.*, rpm.id_penjualan_m,
						  rpm.no_retur_penjualan, rpm.no_penjualan, rpm.id_customer_toko,
						  bp.nama_barang, bp.sku, bt.total_stok, bt.total_stok_rusak, rpm.status_retur');
		$this->db->from('vamr4846_toko_mrc.retur_penjualan_detail_'.$kode_toko.' AS rpd');
		$this->db->join('vamr4846_toko_mrc.retur_penjualan_master_'.$kode_toko.' AS rpm', 'rpm.id_retur_penjualan_m=rpd.id_retur_penjualan_m', 'LEFT');
		$this->db->join('vamr4846_toko_mrc.barang_toko_'.$kode_toko.' AS bt', 'bt.id_barang_pusat=rpd.id_barang_pusat', 'LEFT');
		$this->db->join('vamr4846_vama.barang_pusat AS bp', 'bp.id_barang_pusat=rpd.id_barang_pusat', 'LEFT');
		$this->db->where('id_retur_penjualan_d', $id_retur_penjualan_d);
		$query = $this->db->get();
		return $query->row();
	}

	function get_id($id_retur_penjualan_m, $id_barang_pusat, $kode_toko)
	{
		$this->db->select('*');
		$this->db->from('vamr4846_toko_mrc.retur_penjualan_detail_'.$kode_toko.'');
		$this->db->where(array(  
			'id_retur_penjualan_m' => $id_retur_penjualan_m,
			'id_barang_pusat'      => $id_barang_pusat
		));
		$query = $this->db->get();
		return $query->row();
	}	

	function insert_detail(
		$id_retur_penjualan_m, $id_penjualan_d, $id_barang_pusat, 
		$harga_satuan, $potongan_harga_value, $potongan_harga, 
		$jumlah_beli, $jumlah_retur, $subtotal_potongan, $subtotal_saldo, $subtotal_retur, 
		$masuk_stok, $keterangan_retur, $id_pegawai, $kode_toko){
		$dt['id_retur_penjualan_m'] = $id_retur_penjualan_m;
		$dt['id_penjualan_d']       = $id_penjualan_d;
		$dt['id_barang_pusat']      = $id_barang_pusat;
		$dt['harga_satuan']         = $harga_satuan;
		$dt['potongan_harga_value'] = $potongan_harga_value;
		$dt['potongan_harga']       = $potongan_harga;
		$dt['jumlah_beli']          = $jumlah_beli;
		$dt['jumlah_retur']         = $jumlah_retur;
		$dt['subtotal_potongan']    = $subtotal_potongan;
		$dt['subtotal_saldo']       = $subtotal_saldo;
		$dt['subtotal_retur']       = $subtotal_retur;
		$dt['potongan_harga']       = $potongan_harga;
		$dt['masuk_stok']           = $masuk_stok;
		$dt['keterangan']           = $keterangan_retur;
		$dt['id_pegawai_pembuatan'] = $id_pegawai;
		$dt['tanggal_pembuatan']    = date('Y-m-d H:i:s');
		return $this->db->insert('vamr4846_toko_mrc.retur_penjualan_detail_'.$kode_toko.'', $dt);
	}

	function update_detail(
		$id_retur_penjualan_m, $id_detail, $id_barang_pusat,
		$harga_satuan, $potongan_harga_value, $potongan_harga, 
		$jumlah_beli, $jumlah_retur, $subtotal_potongan, $subtotal_saldo, $subtotal_retur, 
		$masuk_stok, $keterangan_retur, $id_pegawai, $jumlah_retur_lama, $masuk_stok_lama, $kode_toko){
		if($masuk_stok == 'JUAL'){
			if($masuk_stok_lama == 'RUSAK'){
				$sql_lama = "UPDATE vamr4846_toko_mrc.barang_toko_".$kode_toko." SET total_stok_rusak = total_stok_rusak - ".$jumlah_retur_lama." 
							 WHERE id_barang_pusat = '".$id_barang_pusat."' ";
				$this->db->query($sql_lama);	
			}elseif($masuk_stok_lama == 'JUAL'){
				$sql = "UPDATE vamr4846_toko_mrc.barang_toko_".$kode_toko." SET total_stok = total_stok - ".$jumlah_retur_lama." 
						WHERE id_barang_pusat = '".$id_barang_pusat."' ";
				$this->db->query($sql);
			}
		}elseif($masuk_stok == 'RUSAK'){
			if($masuk_stok_lama == 'JUAL'){
				$sql_lama = "UPDATE vamr4846_toko_mrc.barang_toko_".$kode_toko." SET total_stok = total_stok - ".$jumlah_retur_lama." 
							 WHERE id_barang_pusat = '".$id_barang_pusat."' ";
				$this->db->query($sql_lama);	
			}elseif($masuk_stok_lama == 'RUSAK'){
				$sql = "UPDATE vamr4846_toko_mrc.barang_toko_".$kode_toko." SET total_stok_rusak = total_stok_rusak - ".$jumlah_retur_lama." 
						WHERE id_barang_pusat = '".$id_barang_pusat."'";
				$this->db->query($sql);
			}
		}

		$dt = array(
			'jumlah_retur'           => $jumlah_retur,
			'harga_satuan'           => $harga_satuan,
			'potongan_harga_value'   => $potongan_harga_value,
			'potongan_harga'         => $potongan_harga,
			'jumlah_beli'            => $jumlah_beli,
			'jumlah_retur'           => $jumlah_retur,
			'subtotal_potongan'      => $subtotal_potongan,
			'subtotal_saldo'         => $subtotal_saldo,
			'subtotal_retur'         => $subtotal_retur,
			'masuk_stok'             => $masuk_stok,
			'keterangan'             => $keterangan_retur,
			'id_pegawai_pembaharuan' => $id_pegawai
		);
		
		$where = array(
			'id_retur_penjualan_m' => $id_retur_penjualan_m,
			'id_barang_pusat'      => $id_barang_pusat
		);
		return $this->db->update('vamr4846_toko_mrc.retur_penjualan_detail_'.$kode_toko.'', $dt, $where);
	}

	function get_detail_faktur($id_retur_penjualan_m, $kode_toko)
	{
		$this->db->select('
			rpd.*,
			bp.sku, bp.kode_barang, bp.nama_barang
		');
		$this->db->from('vamr4846_toko_mrc.retur_penjualan_detail_'.$kode_toko.' AS rpd');
		$this->db->join('vamr4846_vama.barang_pusat as bp','bp.id_barang_pusat=rpd.id_barang_pusat');
		$this->db->where('rpd.id_retur_penjualan_m', $id_retur_penjualan_m);
		$this->db->order_by('bp.nama_barang','ASC');
		$query = $this->db->get();
		return $query->result();
	}

	function get_total($id_retur_penjualan_m, $kode_toko)
	{
		$this->db->select('
			IFNULL(id_retur_penjualan_m,"-") AS id_retur_penjualan_m, COUNT(id_retur_penjualan_d)AS jumlah_barang,
			IFNULL(SUM(subtotal_potongan),0) AS total_potongan, IFNULL(SUM(subtotal_saldo),0) AS total_saldo, 
			IFNULL(SUM(subtotal_retur),0) AS total_retur
		');
		$this->db->from('vamr4846_toko_mrc.retur_penjualan_detail_'.$kode_toko.'');
		$this->db->where('id_retur_penjualan_m', $id_retur_penjualan_m);
		$query = $this->db->get();
		return $query->row();
	}

	function hapus_retur_penjualan_detail(
		$id_retur_penjualan_d, $id_retur_penjualan_m, $id_penjualan_m, $id_penjualan_d,
		$no_retur_penjualan, $no_penjualan, $id_customer_toko,
		$id_barang_pusat, $harga_satuan, $potongan_harga_value,
		$potongan_harga, $jumlah_beli, $jumlah_retur, $subtotal_potongan, $subtotal_saldo, $subtotal_retur, $masuk_stok,
		$keterangan, $id_pegawai_pembuatan, $id_pegawai_pembaharuan, $id_pegawai_pembatalan, 
		$tanggal_pembuatan, $tanggal_pembaharuan, $keterangan_batal, $kode_toko
	){
		// Kembalikan stok berdasarkan jumlah beli
		if($masuk_stok == 'JUAL'){
			$v_masuk_stok = 'total_stok';
		}elseif($masuk_stok == 'RUSAK'){
			$v_masuk_stok = 'total_stok_rusak';
		}
		
		$sql = "UPDATE vamr4846_toko_mrc.barang_toko_".$kode_toko." SET ".$v_masuk_stok." = ".$v_masuk_stok." - ".$jumlah_retur." 
				WHERE id_barang_pusat = '".$id_barang_pusat."'";
		$this->db->query($sql);

		// Simpan penjualan detail ke penjualan detail batal
		$sql = "
				INSERT INTO vamr4846_toko_mrc.retur_penjualan_detail_batal_".$kode_toko."
					(id_retur_penjualan_d, id_retur_penjualan_m, id_penjualan_m, id_penjualan_d,
					no_retur_penjualan, no_penjualan, id_customer_toko,
					id_barang_pusat, harga_satuan, potongan_harga_value, potongan_harga, jumlah_beli, jumlah_retur, 
					subtotal_potongan, subtotal_saldo, subtotal_retur, masuk_stok,
					keterangan, id_pegawai_pembuatan, id_pegawai_pembaharuan, 
					id_pegawai_pembatalan, tanggal_pembuatan, tanggal_pembaharuan, keterangan_batal)
				VALUES
					('".$id_retur_penjualan_d."', '".$id_retur_penjualan_m."',
					'".$id_penjualan_m."', '".$id_penjualan_d."', 
					'".$no_retur_penjualan."', '".$no_penjualan."', '".$id_customer_toko."',
					'".$id_barang_pusat."', '".$harga_satuan."', '".$potongan_harga_value."',
					'".$potongan_harga."', '".$jumlah_beli."', '".$jumlah_retur."', 
					'".$subtotal_potongan."', '".$subtotal_saldo."', '".$subtotal_retur."', '".$masuk_stok."',
					'".$keterangan."', '".$id_pegawai_pembuatan."', '".$id_pegawai_pembaharuan."', '".$id_pegawai_pembatalan."', 
					'".$tanggal_pembuatan."', '".$tanggal_pembaharuan."', '".$keterangan_batal."') 
				";
				$this->db->query($sql);

		// Hapus penjualan detail
		return $this->db
					->where('id_retur_penjualan_d', $id_retur_penjualan_d)
					->delete('vamr4846_toko_mrc.retur_penjualan_detail_'.$kode_toko.'');
	}
}
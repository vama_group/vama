<?php
class Paket_promo_detail_model extends CI_Model
{
	// Kategori paket
	var $column_order_kategori  = array('', 'kp.id_kategori_paket', 'kp.nama_kategori_paket');
	var $column_search_kategori = array('kp.nama_kategori_paket'); 
	var $order_kategori         = array('kp.id_kategori_paket' => 'desc');

	// Paket promo induk
	var $column_order 			= array('', '', 'bp.sku','bp.nama_barang', 'bt.harga_eceran', 'pd.qty', 
										'pt1.nama_pegawai_toko', 'pd.tanggal_pembuatan');
	var $column_search 			= array('bp.kode_barang','bp.sku','bp.nama_barang'); 
	var $order 					= array('pd.tanggal_pembaharuan' => 'desc');

	// Paket promo batal
	var $column_order_batal 	= array('', 'bp.sku','bp.nama_barang', 'bt.harga_eceran', 'pd.qty', 'pd.keterangan_hapus');
	var $column_search_batal 	= array('bp.kode_barang','bp.sku','bp.nama_barang'); 
	var $order_batal 			= array('pd.tanggal_hapus' => 'desc');

	// Cari barang
	var $column_order_barang 	= array('', '', 'bp.sku','bp.nama_barang','bp.total_stok');
	var $column_search_barang 	= array('bp.kode_barang','bp.sku','bp.nama_barang'); 
	var $order_barang 			= array('bp.nama_barang' => 'desc');

	private function _get_datatables_query_kategori()
	{		
		$this->db->select('*');
		$this->db->from('vamr4846_toko_mrc.kategori_paket AS kp');
		$this->db->where('kp.status_hapus','TIDAK');

		$i = 0;	
		foreach ($this->column_search_kategori as $item){
			if($_POST['search']['value']){
				if($i===0){
					$this->db->group_start();
					$this->db->like($item, $_POST['search']['value']);
				}else{
					$this->db->or_like($item, $_POST['search']['value']);
				}

				if(count($this->column_search_kategori) - 1 == $i)
					$this->db->group_end();
			}
			$i++;
		}
		
		if(isset($_POST['order'])){
			$this->db->order_by($this->column_order_kategori[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		}else if(isset($this->order_kategori)){
			$order = $this->order_kategori;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}

	private function _get_datatables_query($id_paket_promo_m, $kode_toko)
	{		
		$this->db->select('
			pd.id_paket_promo_d, pd.id_paket_promo_m,
			bp.sku, bp.kode_barang, bp.nama_barang, bt.harga_eceran, pd.harga_satuan, pd.discount_harga, pd.qty,
			(bt.harga_eceran - pd.discount_harga) AS harga_bersih,
			(bt.harga_eceran - pd.discount_harga) * pd.qty AS subtotal,
			pt1.nama_pegawai_toko AS pegawai_save, IFNULL(pt2.nama_pegawai_toko, "-") AS pegawai_edit, 
			pd.tanggal_pembuatan, pd.tanggal_pembaharuan
		');
		$this->db->from('vamr4846_toko_mrc.paket_detail AS pd');
		$this->db->join('vamr4846_toko_mrc.barang_toko_'.$kode_toko.' as bt','bt.id_barang_pusat=pd.id_barang_pusat','LEFT');
		$this->db->join('vamr4846_vama.barang_pusat as bp','bp.id_barang_pusat=pd.id_barang_pusat','LEFT');
		$this->db->join('vamr4846_toko_mrc.pegawai_toko as pt1','pt1.id_pegawai_toko=pd.id_pegawai_pembuatan','LEFT');
		$this->db->join('vamr4846_toko_mrc.pegawai_toko as pt2','pt2.id_pegawai_toko=pd.id_pegawai_pembaharuan','LEFT');
		$this->db->where(array(
			'pd.id_paket_promo_m' => $id_paket_promo_m,
			'pd.status_hapus'     => 'TIDAK'
		));

		$i = 0;	
		foreach ($this->column_search as $item){
			if($_POST['search']['value']){
				if($i===0){
					$this->db->group_start();
					$this->db->like($item, $_POST['search']['value']);
				}else{
					$this->db->or_like($item, $_POST['search']['value']);
				}

				if(count($this->column_search) - 1 == $i)
					$this->db->group_end();
			}
			$i++;
		}
		
		if(isset($_POST['order'])){
			$this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		}else if(isset($this->order)){
			$order = $this->order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}

	private function _get_datatables_query_batal($id_paket_promo_m, $kode_toko)
	{		
		$this->db->select('
			pd.*, 
			bp.sku, bp.nama_barang, bt.harga_eceran,
			pt1.nama_pegawai_toko AS pegawai_save, pt2.nama_pegawai_toko AS pegawai_edit, pt3.nama_pegawai_toko AS pegawai_hapus
		');
		$this->db->from('vamr4846_toko_mrc.paket_detail AS pd');
		$this->db->join('vamr4846_toko_mrc.barang_toko_'.$kode_toko.' as bt','bt.id_barang_pusat=pd.id_barang_pusat','LEFT');
		$this->db->join('vamr4846_vama.barang_pusat as bp','bp.id_barang_pusat=pd.id_barang_pusat','LEFT');
		$this->db->join('vamr4846_toko_mrc.pegawai_toko as pt1','pt1.id_pegawai_toko=pd.id_pegawai_pembuatan','LEFT');
		$this->db->join('vamr4846_toko_mrc.pegawai_toko as pt2','pt2.id_pegawai_toko=pd.id_pegawai_pembaharuan','LEFT');
		$this->db->join('vamr4846_toko_mrc.pegawai_toko as pt3','pt3.id_pegawai_toko=pd.id_pegawai_hapus','LEFT');
		$this->db->where(array(
			'pd.id_paket_promo_m' => $id_paket_promo_m,
			'pd.status_hapus'     => 'IYA'
		));

		$i = 0;
		foreach ($this->column_search_batal as $item){
			if($_POST['search']['value']){
				if($i===0){
					$this->db->group_start();
					$this->db->like($item, $_POST['search']['value']);
				}else{
					$this->db->or_like($item, $_POST['search']['value']);
				}

				if(count($this->column_search_batal) - 1 == $i)
				$this->db->group_end();
			}
			$i++;
		}
		
		if(isset($_POST['order'])){
			$this->db->order_by($this->column_order_batal[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		}else if(isset($this->order_batal)){
			$order = $this->order_batal;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}

	private function _get_datatables_query_barang($id_paket_promo_m, $kode_toko)
	{		
		$ambil_detail = $this->db->query("
			SELECT id_barang_pusat 
			FROM vamr4846_toko_mrc.paket_detail 
			WHERE id_paket_promo_m =".$id_paket_promo_m."
		");
		$pd_id_barang = array();
		foreach ($ambil_detail->result() as $row){
			$pd_id_barang[] = $row->id_barang_pusat; 
		}
		$id_barang = implode(",", $pd_id_barang);
		$idrp      = explode(",", $id_barang);
		
		$this->db->select('
			bp.id_barang_pusat, bp.sku, bp.kode_barang, bp.nama_barang, 
			bt.total_stok, bt.total_stok_rusak, bt.harga_eceran AS harga_satuan, bt.kode_toko
		');
		$this->db->from('vamr4846_toko_mrc.barang_toko_'.$kode_toko.' as bt');
		$this->db->join('vamr4846_vama.barang_pusat as bp', 'bp.id_barang_pusat=bt.id_barang_pusat', 'LEFT');
		$this->db->where('bt.status', 'AKTIF');
		$this->db->where('bt.status_hapus', 'TIDAK');
		$this->db->where('bt.total_stok >', '0');
		$this->db->where_not_in('bt.id_barang_pusat', $idrp);
		
		$i = 0;	
		foreach ($this->column_search_barang as $item){
			if($_POST['search']['value']){
				if($i===0){
					$this->db->group_start();
					$this->db->like($item, $_POST['search']['value']);
				}else{
					$this->db->or_like($item, $_POST['search']['value']);
				}

				if(count($this->column_search_barang) - 1 == $i)
				$this->db->group_end();
			}
			$i++;
		}
		
		if(isset($_POST['order'])){
			$this->db->order_by($this->column_order_barang[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		}else if(isset($this->order_barang)){
			$order = $this->order_barang;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}

	function get_datatables($perintah, $id_paket_promo_m, $kode_toko)
	{
		$this->$perintah($id_paket_promo_m, $kode_toko);
		if($_POST['length'] != -1)
		$this->db->limit($_POST['length'], $_POST['start']);
		$query = $this->db->get();
		return $query->result();
	}

	function count_filtered($perintah, $id_paket_promo_m, $kode_toko)
	{
		$this->$perintah($id_paket_promo_m, $kode_toko);
		$query = $this->db->get();
		return $query->num_rows();
	}

	public function count_all($table, $id_paket_promo_m, $status_hapus)
	{
		$this->db->from($table);
		if($table == 'vamr4846_toko_mrc.paket_detail'){
			$this->db->where('id_paket_promo_m', $id_paket_promo_m);
			$this->db->where('status_hapus', $status_hapus);
		}else if ($table == 'vamr4846_toko_mrc.kategori_paket'){
			$this->db->where('status_hapus','TIDAK');
		}else{
			$ambil_detail = $this->db->query("
				SELECT id_barang_pusat 
				FROM vamr4846_toko_mrc.paket_detail 
				WHERE id_paket_promo_m =".$id_paket_promo_m."
			");
	  		$pd_id_barang 	= array();
			foreach ($ambil_detail->result() as $row){
				$pd_id_barang[] = $row->id_barang_pusat; 
			}
			$id_barang = implode(",", $pd_id_barang);
			$idrp      = explode(",", $id_barang);
			$this->db->where_not_in('id_barang_pusat', $idrp);
		}
		return $this->db->count_all_results();
	}

	function get_paket_promo_detail($id_paket_promo_d, $kode_toko)
	{
		$this->db->select('
			pd.*, 
			(bt.harga_eceran - pd.discount_harga) AS harga_bersih,
			(bt.harga_eceran - pd.discount_harga) * pd.qty AS subtotal,
			pm.kode_paket_promo, pm.id_kategori_paket, pm.jenis_paket,  pm.status_paket,
			bp.sku, bp.nama_barang, bt.harga_eceran, format(bt.harga_eceran, 0) as harga
		');
		$this->db->from('vamr4846_toko_mrc.paket_detail as pd');
		$this->db->join('vamr4846_toko_mrc.paket_master as pm','pm.id_paket_promo_m=pd.id_paket_promo_m','LEFT');
		$this->db->join('vamr4846_toko_mrc.barang_toko_'.$kode_toko.' as bt','bt.id_barang_pusat=pd.id_barang_pusat','LEFT');
		$this->db->join('vamr4846_vama.barang_pusat as bp','bp.id_barang_pusat=pd.id_barang_pusat','LEFT');
		$this->db->where('pd.id_paket_promo_d', $id_paket_promo_d);
		$query = $this->db->get();
		return $query->row();
	}

	function get_id($id_paket_promo_m, $id_barang_pusat){
		$this->db->select('*');
		$this->db->from('vamr4846_toko_mrc.paket_detail');
		$this->db->where(array(  
			'id_paket_promo_m' => $id_paket_promo_m,
			'id_barang_pusat'  => $id_barang_pusat
		));
		$query = $this->db->get();
		return $query->row();
	}

	function insert_detail(
		$id_master, $id_barang_pusat,
		$jumlah_barang, $harga_satuan,
		$discount_value, $discount_harga,
		$id_pegawai, $kode_toko
	){ 
		$dt = array (
			'id_paket_promo_m'     => $id_master,
			'id_barang_pusat'      => $id_barang_pusat,
			'qty'                  => $jumlah_barang,
			'harga_satuan'         => $harga_satuan,
			'discount_value'       => $discount_value,
			'discount_harga'       => $discount_harga,
			'id_pegawai_pembuatan' => $id_pegawai,
			'tanggal_pembuatan'    => date('Y-m-d H: i: s') 		
		);
		return $this->db->insert('vamr4846_toko_mrc.paket_detail', $dt);
	}

	function update_detail(
		$id_master, $id_barang_pusat,  
		$jumlah_barang, $harga_satuan,
		$discount_value, $discount_harga, 
		$id_pegawai, $kode_toko
	){		
		$dt = array ( 	
			'harga_satuan'           => $harga_satuan,
			'discount_value'         => $discount_value,
			'discount_harga'         => $discount_harga,
			'qty'                    => $jumlah_barang,
			'id_pegawai_pembaharuan' => $id_pegawai,
			'tanggal_pembaharuan'    => date('Y-m-d H: i: s')
		);

		$where = array (	
			'id_paket_promo_m' => $id_master,
			'id_barang_pusat'  => $id_barang_pusat
		);

		return $this->db->update('vamr4846_toko_mrc.paket_detail', $dt, $where);
	}

	function get_detail_faktur($id_paket_promo_m, $kode_toko){
		$this->db->select('
							pd.id_paket_promo_d, 
							pd.jumlah_retur, pd.jumlah_masuk, pd.keterangan,
							bp.sku, bp.kode_barang, bp.nama_barang, pd.kondisi_barang
						  ');
		$this->db->from('vamr4846_toko_mrc.retur_pembelian_detail_'.$kode_toko.' AS pd');
		$this->db->join('vamr4846_vama.barang_pusat as bp','bp.id_barang_pusat=pd.id_barang_pusat');
		$this->db->where('pd.id_paket_promo_m', $id_paket_promo_m);
		$this->db->order_by('bp.nama_barang','ASC');
		$query = $this->db->get();
		return $query->result();
	}

	function get_total($id_paket_promo_m, $kode_toko)
	{
		$this->db->select('
			id_paket_promo_m,
			IFNULL(COUNT(id_paket_promo_d),0) jumlah_barang,
			IFNULL(ROUND(SUM((harga_satuan*jumlah_retur)),0),0) total
		');
		$this->db->from('vamr4846_toko_mrc.retur_pembelian_detail_'.$kode_toko.'');
		$this->db->where('id_paket_promo_m', $id_paket_promo_m);
		$query = $this->db->get();
		return $query->row();
	}

	function hapus_paket_promo_detail(
		$id_paket_promo_d, $id_paket_promo_m,
		$id_pegawai_pembatalan, $keterangan_batal
	){
		$dt = array ( 	
			'status_hapus'     => 'IYA',
			'keterangan_hapus' => $keterangan_batal,
			'id_pegawai_hapus' => $id_pegawai_pembatalan,
			'tanggal_hapus'    => date('Y-m-d H:i:s')
		);

		$where = array (	
			'id_paket_promo_m' => $id_paket_promo_m,
			'id_paket_promo_d' => $id_paket_promo_d
		);

		return $this->db->update('vamr4846_toko_mrc.paket_detail', $dt, $where);
	}
}
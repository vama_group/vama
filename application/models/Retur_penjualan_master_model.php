<?php
class Retur_penjualan_master_model extends CI_Model
{
	// Induk
	var $column_order         = array('rpm.id_retur_penjualan_m', 'rpm.id_retur_penjualan_m', 
									  'rpm.no_retur_penjualan', 'rpm.no_penjualan', 'rpm.status_retur', 'rpm.tanggal_retur',
									  'cp.nama_customer_pusat', 'rpm.total_retur', 'rpm.total_potongan', 'rpm.total_saldo',
									  'pp1.nama_pegawai', 'rpm.tanggal_pembuatan', 'pp2.nama_pegawai', 'rpm.tanggal_pembaharuan');
	var $column_search        = array('rpm.no_retur_penjualan', 'rpm.no_penjualan', 'cp.nama_customer_pusat'); 
	var $order                = array('rpm.id_retur_penjualan_m' => 'desc');
	// --------------------------------------------------------------------------------------------------------------------------------
	
	// Detail
	var $column_order_detail  = array('rpm.id_retur_penjualan_m', 'rpm.id_retur_penjualan_m', 
									  'rpm.no_retur_penjualan', 'rpm.no_penjualan',
									  'cp.nama_customer_pusat', 'rpd.masuk_stok', 'bp.sku', 'bp.nama_barang', 
									  'rpd.harga_satuan', 'rpd.jumlah_beli', 'rpd.jumlah_retur', 'rpd.potongan_harga',
									  'rpd.subtotal_potongan', 'rpd.subtotal_saldo', 'rpd.sabtotal_retur', 
									  'rpd.keterangan', 'rpm.status_retur',
									  'pp1.nama_pegawai', 'rpd.tanggal_pembuatan', 'pp2.nama_pegawai', 'rpd.tanggal_pembaharuan');
	var $column_search_detail = array('rpm.no_retur_penjualan', 'rpm.no_penjualan', 'cp.nama_customer_pusat', 'rpd.keterangan'); 
	var $order_detail         = array('rpm.id_retur_penjualan_m' => 'desc');
	// --------------------------------------------------------------------------------------------------------------------------------
	
	// Batal
	var $column_order_batal   = array('rpdb.id_retur_penjualan_m', 'rpdb.id_retur_penjualan_m', 
									  'rpdb.no_retur_penjualan', 'rpdb.no_penjualan',
									  'cp.nama_customer_pusat', 'rpdb.masuk_stok', 'bp.sku', 'bp.nama_barang', 
									  'rpdb.harga_satuan', 'rpdb.jumlah_beli', 'rpdb.jumlah_retur', 'rpdb.potongan_harga',
									  'rpdb.subtotal_potongan', 'rpdb.subtotal_saldo', 'rpdb.sabtotal_retur',
									  'rpdb.keterangan', 'rpdb.keterangan_batal', 
									  'pp3.nama_pegawai', 'rpdb.tanggal_pembatalan',
									  'pp1.nama_pegawai', 'rpdb.tanggal_pembuatan', 'pp2.nama_pegawai', 'rpdb.tanggal_pembaharuan');
	var $column_search_batal  = array('rpdb.no_retur_penjualan', 'rpdb.no_penjualan', 'cp.nama_customer_pusat', 
									  'rpdb.keterangan', 'rpdb.keterangan_batal'); 
	var $order_batal          = array('rpdb.id_retur_penjualan_b' => 'desc');
	// --------------------------------------------------------------------------------------------------------------------------------
	
	private function _get_laporan_query($tanggal_awal, $tanggal_akhir)
	{		
		$this->db->select('rpm.*, cp.nama_customer_pusat as nama_customer,
						   pp1.nama_pegawai AS pegawai_save, pp2.nama_pegawai AS pegawai_edit, 
						   rpm.tanggal_pembuatan, rpm.tanggal_pembaharuan
						  ');
		$this->db->from('vamr4846_vama.retur_penjualan_master AS rpm');
		$this->db->join('vamr4846_vama.customer_pusat as cp','cp.id_customer_pusat=rpm.id_customer_pusat','LEFT');
		$this->db->join('vamr4846_vama.pegawai_pusat as pp1','pp1.id_pegawai=rpm.id_pegawai_pembuatan','LEFT');
		$this->db->join('vamr4846_vama.pegawai_pusat as pp2','pp2.id_pegawai=rpm.id_pegawai_pembaharuan','LEFT');
		$this->db->where('DATE(rpm.tanggal_retur) BETWEEN "'. date('Y-m-d', strtotime($tanggal_awal)). '" AND 
														  "'. date('Y-m-d', strtotime($tanggal_akhir)).'"');
		
		$i = 0;
		foreach ($this->column_search as $item){
			if($_POST['search']['value']){
				if($i===0){
					$this->db->group_start();
					$this->db->like($item, $_POST['search']['value']);
				}else{
					$this->db->or_like($item, $_POST['search']['value']);
				}

				if(count($this->column_search) - 1 == $i) 
					$this->db->group_end();
			}
			$i++;
		}

		if(isset($_POST['order'])){
			$this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		}else if(isset($this->order)){
			$order = $this->order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}

	private function _get_laporan_query_detail($tanggal_awal, $tanggal_akhir)
	{		
		$this->db->select('rpd.*, 
						   rpm.no_retur_penjualan, rpm.no_penjualan, rpm.tanggal_retur, rpm.status_retur,
						   cp.nama_customer_pusat as nama_customer,
						   bp.sku, bp.nama_barang, 
						   pp1.nama_pegawai AS pegawai_save, pp2.nama_pegawai AS pegawai_edit
						  ');
		$this->db->from('vamr4846_vama.retur_penjualan_master AS rpm');
		$this->db->join('vamr4846_vama.retur_penjualan_detail AS rpd', 'rpd.id_retur_penjualan_m=rpm.id_retur_penjualan_m');
		$this->db->join('vamr4846_vama.barang_pusat AS bp', 'bp.id_barang_pusat=rpd.id_barang_pusat');
		$this->db->join('vamr4846_vama.customer_pusat as cp','cp.id_customer_pusat=rpm.id_customer_pusat','LEFT');
		$this->db->join('vamr4846_vama.pegawai_pusat as pp1','pp1.id_pegawai=rpd.id_pegawai_pembuatan','LEFT');
		$this->db->join('vamr4846_vama.pegawai_pusat as pp2','pp2.id_pegawai=rpd.id_pegawai_pembaharuan','LEFT');
		$this->db->where('DATE(rpm.tanggal_retur) BETWEEN "'. date('Y-m-d', strtotime($tanggal_awal)). '" AND 
														  "'. date('Y-m-d', strtotime($tanggal_akhir)).'"');
		
		$i = 0;
		foreach ($this->column_search_detail as $item){
			if($_POST['search']['value']){
				if($i===0){
					$this->db->group_start();
					$this->db->like($item, $_POST['search']['value']);
				}else{
					$this->db->or_like($item, $_POST['search']['value']);
				}

				if(count($this->column_search_detail) - 1 == $i) 
					$this->db->group_end();
			}
			$i++;
		}

		if(isset($_POST['order'])){
			$this->db->order_by($this->column_order_detail[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		}else if(isset($this->order_detail)){
			$order = $this->order_detail;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}

	private function _get_laporan_query_detail_batal($tanggal_awal, $tanggal_akhir)
	{		
		$this->db->select('
			rpdb.*,
			cp.nama_customer_pusat as nama_customer,
			bp.sku, bp.nama_barang, 
			pp1.nama_pegawai AS pegawai_save, pp2.nama_pegawai AS pegawai_edit, pp3.nama_pegawai AS pegawai_batal
		');
		$this->db->from('vamr4846_vama.retur_penjualan_detail_batal AS rpdb');
		$this->db->join('vamr4846_vama.barang_pusat AS bp', 'bp.id_barang_pusat=rpdb.id_barang_pusat');
		$this->db->join('vamr4846_vama.customer_pusat as cp','cp.id_customer_pusat=rpdb.id_customer_pusat','LEFT');
		$this->db->join('vamr4846_vama.pegawai_pusat as pp1','pp1.id_pegawai=rpdb.id_pegawai_pembuatan','LEFT');
		$this->db->join('vamr4846_vama.pegawai_pusat as pp2','pp2.id_pegawai=rpdb.id_pegawai_pembaharuan','LEFT');
		$this->db->join('vamr4846_vama.pegawai_pusat as pp3','pp3.id_pegawai=rpdb.id_pegawai_pembatalan','LEFT');
		$this->db->where('DATE(rpdb.tanggal_pembatalan) BETWEEN "'. date('Y-m-d', strtotime($tanggal_awal)). '" AND 
														  "'. date('Y-m-d', strtotime($tanggal_akhir)).'"');
		
		$i = 0;
		foreach ($this->column_search_batal as $item){
			if($_POST['search']['value']){
				if($i===0){
					$this->db->group_start();
					$this->db->like($item, $_POST['search']['value']);
				}else{
					$this->db->or_like($item, $_POST['search']['value']);
				}

				if(count($this->column_search_batal) - 1 == $i) 
					$this->db->group_end();
			}
			$i++;
		}

		if(isset($_POST['order'])){
			$this->db->order_by($this->column_order_batal[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		}else if(isset($this->order_batal)){
			$order = $this->order_batal;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}

	function get_laporan($perintah, $tanggal_awal, $tanggal_akhir)
	{
		$this->$perintah($tanggal_awal, $tanggal_akhir);
		if($_POST['length'] != -1)
		$this->db->limit($_POST['length'], $_POST['start']);
		$query = $this->db->get();
		return $query->result();
	}

	function count_filtered($perintah, $tanggal_awal, $tanggal_akhir)
	{
		$this->$perintah($tanggal_awal, $tanggal_akhir);
		$query = $this->db->get();
		return $query->num_rows();
	}

	public function count_all($table, $tanggal_awal, $tanggal_akhir)
	{
		$this->db->from($table);
		if($table == 'vamr4846_vama.retur_penjualan_master'){
			$this->db->where('DATE(tanggal_retur) BETWEEN "'. date('Y-m-d', strtotime($tanggal_awal)). '" AND 
														  "'. date('Y-m-d', strtotime($tanggal_akhir)).'"');
		}else if($table == 'vamr4846_vama.retur_penjualan_detail AS rpd'){
			$this->db->join('vamr4846_vama.retur_penjualan_master AS rpm', 'rpm.id_retur_penjualan_m=rpd.id_retur_penjualan_m', 'LEFT');
			$this->db->where('DATE(rpm.tanggal_retur) BETWEEN "'. date('Y-m-d', strtotime($tanggal_awal)). '" AND 
													   "'. date('Y-m-d', strtotime($tanggal_akhir)).'"');
		}else if($table == 'vamr4846_vama.retur_penjualan_detail_batal'){
			$this->db->where('DATE(tanggal_pembatalan) BETWEEN "'. date('Y-m-d', strtotime($tanggal_awal)). '" AND 
															   "'. date('Y-m-d', strtotime($tanggal_akhir)).'"');
		}
		return $this->db->count_all_results();
	}

	function get_grand_total($tanggal_awal, $tanggal_akhir, $jenis_perintah, $status_retur)
	{
		$this->db->select('	
			COUNT(id_retur_penjualan_m) AS jml_transaksi, SUM(total_potongan) AS grand_total_potongan, 
			SUM(total_retur) AS grand_total_retur
		');
		$this->db->from('vamr4846_vama.retur_penjualan_master');
		$this->db->where('DATE(tanggal_retur) BETWEEN "'. date('Y-m-d', strtotime($tanggal_awal)). '" AND 
													  "'. date('Y-m-d', strtotime($tanggal_akhir)).'" AND 
						  tipe_customer_pusat 		  '.$jenis_perintah.'  "MRC" AND
						  status_retur 				  = "'.$status_retur.'"
						');
		$query = $this->db->get();
		return $query->row();
	}

	function get_total_perbulan($tanggal_awal, $tanggal_akhir, $jenis_perintah)
	{
		$this->db->select('
			IFNULL(MIN(DATE_FORMAT(tanggal_retur, "%d-%m-%y")),"00-00-00") AS tanggal_awal,
			IFNULL(MAX(DATE_FORMAT(tanggal_retur, "%d-%m-%y")),"00-00-00") AS tanggal_akhir,
			COUNT(id_retur_penjualan_m) AS jml_transaksi, 
			IFNULL(SUM(total_potongan),0) AS grand_total_potongan, 
			IFNULL(SUM(total_retur),0) AS grand_total_retur
		');
		$this->db->from('vamr4846_vama.retur_penjualan_master');
		$this->db->where('
							DATE(tanggal_retur) BETWEEN "'. date('Y-m-d', strtotime($tanggal_awal)). '" AND 
												  		"'. date('Y-m-d', strtotime($tanggal_akhir)).'" AND
							tipe_customer_pusat 		'.$jenis_perintah.' "MRC"
						');
		$query = $this->db->get();
		return $query->row();
	}

	// No. pembelian baru
	function no_retur_penjualan_baru($tahun_sekarang, $bulan_sekarang, $id_pegawai) {
	    $this->db->select('COUNT(*)+1 AS no_baru');
	    $this->db->from('vamr4846_vama.retur_penjualan_master');
	    $this->db->where(array(  
    						'YEAR(tanggal_retur)'	=> $tahun_sekarang,
               				'MONTH(tanggal_retur)'  => $bulan_sekarang,
    						'id_pegawai_pembuatan'	=> $id_pegawai
	    				));
	    $this->db->order_by('tanggal_pembuatan','DESC');
	    $query = $this->db->get();
	    return $query->row();
	}

	// No. retur penjualan detail detail baru
	function no_retur_penjualan_detail_baru($id_retur_penjualan_m) {
		$this->db->select('COUNT(*)+1 AS no_retur_penjualan_detail_baru');
		$this->db->from('vamr4846_vama.retur_penjualan_detail');
		$this->db->where('id_retur_penjualan_m',$id_retur_penjualan_m);
		$this->db->order_by('tanggal_pembuatan','DESC');
		$query = $this->db->get();
		return $query->row();
	}

	function cari_daftar_transaksi($keyword)
	{
		$sql = "
			SELECT 
				rpm.id_retur_penjualan_m, rpm.no_retur_penjualan,
				pm.id_penjualan_m, pm.no_penjualan, pm.id_customer_pusat, cp.foto, cp.nama_customer_pusat as nama_customer,
				pm.grand_total, FORMAT(pm.grand_total,0) as grandtotal, pm.tanggal
			FROM 
				vamr4846_vama.penjualan_master AS pm
			LEFT JOIN 
				vamr4846_vama.customer_pusat AS cp ON cp.id_customer_pusat=pm.id_customer_pusat
			LEFT JOIN 
				vamr4846_vama.retur_penjualan_master AS rpm ON rpm.id_penjualan_m=pm.id_penjualan_m
			WHERE 
				pm.id_penjualan_m NOT IN (SELECT id_penjualan_m FROM vamr4846_vama.retur_penjualan_master WHERE status_penjualan='SELESAI') 
				AND pm.status_penjualan='SELESAI'
				AND 
				( 
					pm.no_penjualan LIKE '%".$keyword."%' OR
					cp.nama_customer_pusat LIKE '%".$keyword."%'
				)
				AND pm.tipe_customer_pusat !='MRC'
			LIMIT 10
		";
		return $this->db->query($sql);
	}

	function insert_master($no_retur_penjualan, $id_penjualan_m, $no_penjualan, $id_customer_pusat, $tipe_customer_pusat,
						   $total_potongan, $total_saldo, $total_retur, $tanggal, $catatan, $id_pegawai)
	{
		$dt = array(
			'no_retur_penjualan'   => $no_retur_penjualan,
			'id_penjualan_m'       => $id_penjualan_m,
			'no_penjualan'         => $no_penjualan,
			'id_customer_pusat'    => (empty($id_customer_pusat)) ? NULL : $id_customer_pusat,
			'tipe_customer_pusat'  => $tipe_customer_pusat,
			'total_retur'          => $total_retur,
			'total_potongan'       => $total_potongan,
			'total_saldo'          => $total_saldo,
			'tanggal_retur'        => $tanggal,
			'keterangan_lain'      => $catatan,
			'id_pegawai_pembuatan' => (empty($id_pegawai)) ? NULL : $id_pegawai,
			'tanggal_pembuatan'    => date('Y-m-d H:i:s')
		);
		return $this->db->insert('vamr4846_vama.retur_penjualan_master', $dt);
	}

	function update_master($id_retur_penjualan_m, $id_penjualan_m, $no_penjualan, $id_customer_pusat, $tipe_customer_pusat,
						   $total_potongan, $total_saldo, $total_retur, $tanggal, $catatan, $id_pegawai)
	{
		$dt = array(
			'id_penjualan_m'         => $id_penjualan_m,
			'no_penjualan'           => $no_penjualan,
			'id_customer_pusat'      => (empty($id_customer_pusat)) ? NULL : $id_customer_pusat,
			'tipe_customer_pusat'    => $tipe_customer_pusat,
			'total_retur'            => $total_retur,
			'total_potongan'         => $total_potongan,
			'total_saldo'            => $total_saldo,
			'tanggal_retur'          => $tanggal,
			'keterangan_lain'        => $catatan,
			'id_pegawai_pembaharuan' => (empty($id_pegawai)) ? NULL : $id_pegawai,
			'tanggal_pembuatan'      => date('Y-m-d H:i:s')
		);
		$where = array(
			'id_retur_penjualan_m'   => $id_retur_penjualan_m
		);
		return $this->db->update('vamr4846_vama.retur_penjualan_master', $dt, $where);
	}

	function update_master_tanpa_total($id_retur_penjualan_m, $id_penjualan_m, $no_penjualan, $id_customer_pusat, 
									   $tanggal, $catatan, $id_pegawai, $status_retur)
	{
		$dt = array(
			'id_penjualan_m'         => $id_penjualan_m,
			'status_retur' 			 => $status_retur,
			'no_penjualan'           => $no_penjualan,
			'id_customer_pusat'      => (empty($id_customer_pusat)) ? NULL : $id_customer_pusat,
			'tanggal_retur'          => $tanggal,
			'keterangan_lain'        => $catatan,
			'id_pegawai_pembaharuan' => (empty($id_pegawai)) ? NULL : $id_pegawai,
			'tanggal_pembuatan'      => date('Y-m-d H:i:s')
		);
		
		$where = array(
			'id_retur_penjualan_m'   => $id_retur_penjualan_m
		);
		return $this->db->update('vamr4846_vama.retur_penjualan_master', $dt, $where);
	}

	function get_master($id_retur_penjualan_m)
	{
		$this->db->select('rpm.*, count(id_retur_penjualan_d) as jumlah_retur,
						  IFNULL(cp.nama_customer_pusat,"-") AS nama_customer, 
						  IFNULL(cp.kode_customer_pusat,"-") AS kode_customer_pusat, 
						  IFNULL(cp.nama_customer_pusat,"-") AS nama_customer_pusat, 
						  IFNULL(cp.alamat_customer_pusat,"-") AS alamat_customer_pusat, 
						  IFNULL(cp.handphone1,"-") AS handphone1,
						  pm.grand_total, pp.nama_pegawai');
		$this->db->from('vamr4846_vama.retur_penjualan_master AS rpm');
		$this->db->join('vamr4846_vama.retur_penjualan_detail as rpd', 'rpm.id_retur_penjualan_m=rpd.id_retur_penjualan_m', 'LEFT');		
		$this->db->join('vamr4846_vama.penjualan_master as pm', 'pm.id_penjualan_m=rpm.id_penjualan_m', 'LEFT');		
		$this->db->join('vamr4846_vama.customer_pusat as cp', 'rpm.id_customer_pusat=cp.id_customer_pusat', 'LEFT');
		$this->db->join('vamr4846_vama.pegawai_pusat as pp', 'rpm.id_pegawai_pembaharuan=pp.id_pegawai', 'LEFT');
		$this->db->where('rpm.id_retur_penjualan_m', $id_retur_penjualan_m);
		$query = $this->db->get();
		return $query->row();
	}

	function get_jumlah_batal($id_retur_penjualan_m)
	{
		$this->db->select('rpdb.*, count(id_retur_penjualan_b) AS jumlah_barang');
		$this->db->from('vamr4846_vama.retur_penjualan_detail_batal AS rpdb');
		$this->db->where('id_retur_penjualan_m', $id_retur_penjualan_m);
		$query = $this->db->get();
		return $query->row();
	}

	function get_id($no_retur_penjualan)
	{
		$this->db->select('*');
		$this->db->from('vamr4846_vama.retur_penjualan_master');
		$this->db->where('no_retur_penjualan', $no_retur_penjualan);
		$query = $this->db->get();
		return $query->row();
	}

	function get_by_id_retur($id_retur_penjualan_m)
	{
		$this->db->select('*');
		$this->db->from('vamr4846_vama.retur_penjualan_master');
		$this->db->where('id_retur_penjualan_m', $id_retur_penjualan_m);
		$query = $this->db->get();
		return $query->row();
	}

	function get_by_id_penjualan($id_penjualan_m)
	{
		$this->db->select('rpm.*, 
						   IFNULL(cp.kode_customer_pusat, "-") AS kode_customer_pusat, 
						   IFNULL(cp.nama_customer_pusat, "-") AS nama_customer_pusat, 
						   IFNULL(cp.tipe_customer_pusat, "-") AS tipe_customer_pusat, 
						   IFNULL(cp.handphone1, "-") AS handphone1');
		$this->db->from('vamr4846_vama.retur_penjualan_master As rpm');
		$this->db->join('vamr4846_vama.customer_pusat AS cp', 'cp.id_customer_pusat=rpm.id_customer_pusat', 'LEFT');
		$this->db->where('id_penjualan_m', $id_penjualan_m);
		$query = $this->db->get();
		return $query->row();
	}

	function hapus_transaksi($id_retur_penjualan_m, $id_pegawai_pembatalan, $keterangan_batal)
	{
		// Ambil data retur penjualan detail
		$this->db->select('rpd.*, rpm.no_retur_penjualan, rpm.no_penjualan, rpm.id_penjualan_m, 
			   			   rpm.id_customer_pusat, rpm.tipe_customer_pusat');
		$this->db->from('vamr4846_vama.retur_penjualan_detail As rpd');
		$this->db->join('vamr4846_vama.retur_penjualan_master AS rpm', 'rpm.id_retur_penjualan_m=rpd.id_retur_penjualan_m', 'LEFT');
		$this->db->where('rpd.id_retur_penjualan_m', $id_retur_penjualan_m);
		$loop = $this->db->get();

		foreach($loop->result() as $b){
			if($b->masuk_stok == 'JUAL'){
				$update_stok = 'total_stok';
			}else{
				$update_stok = 'total_stok_rusak';
			}

			// Kembalikan stok berdasarkan jumlah retur dan keterangan masuk stok
			$sql = "UPDATE vamr4846_vama.barang_pusat SET ".$update_stok." = ".$update_stok." - ".$b->jumlah_retur." 
					WHERE id_barang_pusat = '".$b->id_barang_pusat."'";
			$this->db->query($sql);

			// Simpan penjualan detail ke penjualan detail batal
			$sql_insert_batal = "
					INSERT INTO vamr4846_vama.retur_penjualan_detail_batal 
						(id_retur_penjualan_d, id_retur_penjualan_m, id_penjualan_m, id_penjualan_d, no_retur_penjualan,
						 no_penjualan, id_customer_pusat, tipe_customer_pusat, id_barang_pusat, harga_satuan, potongan_harga_value,
						 potongan_harga, jumlah_beli, jumlah_retur, subtotal_potongan, subtotal_saldo, subtotal_retur, masuk_stok, keterangan, 
						 id_pegawai_pembuatan, id_pegawai_pembaharuan, id_pegawai_pembatalan, tanggal_pembuatan, tanggal_pembaharuan, 
						 keterangan_batal) 
					VALUES
						('".$b->id_retur_penjualan_d."', '".$b->id_retur_penjualan_m."', 
						 '".$b->id_penjualan_m."', '".$b->id_penjualan_d."', '".$b->no_retur_penjualan."', '".$b->no_penjualan."',
						 '".$b->id_customer_pusat."', '".$b->tipe_customer_pusat."', '".$b->id_barang_pusat."', '".$b->harga_satuan."', 
						 '".$b->potongan_harga_value."', '".$b->potongan_harga."', '".$b->jumlah_beli."', '".$b->jumlah_retur."', 
						 '".$b->subtotal_potongan."', '".$b->subtotal_saldo."', '".$b->subtotal_retur."', '".$b->masuk_stok."', 
						 '".$b->keterangan."', '".$b->id_pegawai_pembuatan."', '".$b->id_pegawai_pembaharuan."', 
						 '".$id_pegawai_pembatalan."', '".$b->tanggal_pembuatan."', '".$b->tanggal_pembaharuan."', '".$keterangan_batal."') 
					";
			$this->db->query($sql_insert_batal);
		}

		$this->db->where('id_retur_penjualan_m', $id_retur_penjualan_m)->delete('vamr4846_vama.retur_penjualan_detail');
		return $this->db
					->where('id_retur_penjualan_m', $id_retur_penjualan_m)
					->delete('vamr4846_vama.retur_penjualan_master');
	}
}
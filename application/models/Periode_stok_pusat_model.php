<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Periode_stok_pusat_model extends MY_Model {

	var $table = 'vamr4846_vama.periode_stok_pusat';

	var $column_order = array(
		'kode_periode_stok_pusat','tanggal_periode_awal','tanggal_periode_akhir',
		'id_pegawai_pembuatan','id_pegawai_pembaharuan'
	); 
	
	var $column_search = array(
		'kode_periode_stok_pusat','tanggal_periode_awal','tanggal_periode_akhir',
		'id_pegawai_pembuatan','id_pegawai_pembaharuan'
	); 
	
	var $order = array('id_periode_stok_pusat' => 'desc');

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	public function data_stok_sekarang($id_periode_stok_pusat, $tanggal_awal, $tanggal_akhir, $id_pegawai, $tanggal_sekarang)
	{		
		$this->db->select("
			bp.id_barang_pusat,
			IFNULL(
				IFNULL(dso.jumlah_so,0) + 
				IFNULL(dpb.jumlah_beli,0) + 
				IFNULL(ddp.jumlah_perakitan,0) + 
				IFNULL(drpjt.jumlah_retur,0) -
				IFNULL(hd.jumlah_hadiah,0) -
				IFNULL(dup.jumlah_komponen,0) - 
				IFNULL(dpj.jumlah_beli, 0) -
				IFNULL(drpbt.jumlah_retur,0)
			, 0) AS jumlah_so_sebelumnya,
			IFNULL(
				IFNULL(dso.jumlah_so,0) + 
				IFNULL(dpb.jumlah_beli,0) + 
				IFNULL(ddp.jumlah_perakitan,0) + 
				IFNULL(drpjt.jumlah_retur,0) -
				IFNULL(hd.jumlah_hadiah,0) -
				IFNULL(dup.jumlah_komponen,0) - 
				IFNULL(dpj.jumlah_beli, 0) -
				IFNULL(drpbt.jumlah_retur,0)
			, 0) AS jumlah_so,
			'".$id_periode_stok_pusat."' AS id_periode_stok_pusat, 'JUAL' AS masuk_stok, 'Hasil cut off' AS catatan, 
			'".$id_pegawai."' AS id_pegawai_pembuatan, '".date('Y-m-d H:i:s')."' AS tanggal_pembuatan
		");
		$this->db->from('vamr4846_vama.barang_pusat AS bp');
		$this->db->join('vamr4846_vama.supplier AS sp', 'sp.kode_supplier=bp.kode_supplier');
		$this->db->join('(SELECT id_barang_pusat, SUM(jumlah_so) AS jumlah_so FROM vamr4846_vama.stok_opname_pusat  
						  WHERE DATE(tanggal_pembuatan) BETWEEN "'.$tanggal_awal.'" AND "'.$tanggal_akhir.'" 
						  GROUP BY id_barang_pusat) AS dso', 'dso.id_barang_pusat=bp.id_barang_pusat', 'LEFT');
		$this->db->join('(SELECT id_barang_pusat, SUM(jumlah_beli) AS jumlah_beli FROM vamr4846_vama.pembelian_detail 
						  WHERE DATE(tanggal_pembuatan) BETWEEN "'.$tanggal_awal.'" AND "'.$tanggal_akhir.'" 
						  GROUP BY id_barang_pusat) AS dpb', 'dpb.id_barang_pusat=bp.id_barang_pusat', 'LEFT');
		$this->db->join('(SELECT id_barang_pusat, SUM(jumlah_perakitan) AS jumlah_perakitan FROM vamr4846_vama.perakitan_master 
						  WHERE DATE(tanggal_pembuatan) BETWEEN "'.$tanggal_awal.'" AND "'.$tanggal_akhir.'" 
						  GROUP BY id_barang_pusat) AS ddp', 'ddp.id_barang_pusat=bp.id_barang_pusat', 'LEFT');
		$this->db->join('(SELECT id_barang_pusat, SUM(jumlah_retur) AS jumlah_retur FROM vamr4846_vama.retur_penjualan_detail 
						  WHERE masuk_stok="JUAL" AND DATE(tanggal_pembuatan) BETWEEN "'.$tanggal_awal.'" AND "'.$tanggal_akhir.'" 
						  GROUP BY id_barang_pusat) AS drpjt', 'drpjt.id_barang_pusat=bp.id_barang_pusat', 'LEFT');
		$this->db->join('(SELECT id_barang_pusat, SUM(jumlah_hadiah) AS jumlah_hadiah FROM vamr4846_vama.hadiah_detail 
						  WHERE DATE(tanggal_pembuatan) BETWEEN "'.$tanggal_awal.'" AND "'.$tanggal_akhir.'" 
						  GROUP BY id_barang_pusat) AS hd', 'hd.id_barang_pusat=bp.id_barang_pusat', 'LEFT');
		$this->db->join('(SELECT id_barang_pusat, SUM(jumlah_komponen) AS jumlah_komponen FROM vamr4846_vama.perakitan_detail 
						  WHERE DATE(tanggal_pembuatan) BETWEEN "'.$tanggal_awal.'" AND "'.$tanggal_akhir.'" 
						  GROUP BY id_barang_pusat) AS dup', 'dup.id_barang_pusat=bp.id_barang_pusat', 'LEFT');
		$this->db->join('(SELECT id_barang_pusat, SUM(jumlah_beli) AS jumlah_beli FROM vamr4846_vama.penjualan_detail 
						  WHERE DATE(tanggal_pembuatan) BETWEEN "'.$tanggal_awal.'" AND "'.$tanggal_akhir.'" 
						  GROUP BY id_barang_pusat) AS dpj', 'dpj.id_barang_pusat=bp.id_barang_pusat', 'LEFT');
		$this->db->join('(SELECT id_barang_pusat, SUM(jumlah_retur) AS jumlah_retur FROM vamr4846_vama.retur_pembelian_detail 
						  WHERE dari_stok="JUAL" AND DATE(tanggal_pembuatan) BETWEEN "'.$tanggal_awal.'" AND "'.$tanggal_akhir.'" 
						  GROUP BY id_barang_pusat) AS drpbt', 'drpbt.id_barang_pusat=bp.id_barang_pusat', 'LEFT');
		$this->db->where('bp.status_hapus','TIDAK');
		$query=$this->db->get();
		// return $query->result_array();
		
		if($query->num_rows()) {
		    $new_so = $query->result();
		    foreach ($new_so as $row => $so) {
		        // $this->db->insert("vamr4846_vama.stok_opname_pusat", $so);
		        $this->db->insert(
		        	"vamr4846_vama.stok_opname_pusat", 
		        	array(
						"id_barang_pusat"       => $so->id_barang_pusat,
						"jumlah_so_sebelumnya"  => $so->jumlah_so_sebelumnya,
						"jumlah_so"             => $so->jumlah_so,
						"masuk_stok"            => $so->masuk_stok,
						"id_periode_stok_pusat" => $so->id_periode_stok_pusat,
						"catatan"               => $so->catatan,
						"id_pegawai_pembuatan"  => $id_pegawai,
						"tanggal_pembuatan"     => date('Y-m-d H:i:s')
					)
		        );
		        $this->db->update(
		        	"vamr4846_vama.barang_pusat", 
		        	array(
						"total_stok"             => $so->jumlah_so,
						"id_pegawai_pembaharuan" => $id_pegawai
		        	),
		        	array(
						"id_barang_pusat" => $so->id_barang_pusat
		        	)
		        );
		        $this->db->insert(
		        	"vamr4846_vama.riwayat_stok_opname_pusat", 
		        	array(
						"id_barang_pusat"       => $so->id_barang_pusat,
						"jumlah_so_sebelumnya"  => $so->jumlah_so_sebelumnya,
						"jumlah_so"             => $so->jumlah_so,
						"masuk_stok"            => $so->masuk_stok,
						"id_periode_stok_pusat" => $so->id_periode_stok_pusat,
						"catatan"               => $so->catatan,
						"id_pegawai"            => $id_pegawai,
						"tanggal_pembuatan"     => date('Y-m-d H:i:s')
					)
		        );
		    }           
		}		
	}

	public function listing() 
	{
		$this->db->select('*');
		$this->db->from('vamr4846_vama.periode_stok_pusat AS psp');
		$this->db->order_by('psp.id_periode_stok_pusat','DESC');
		$query=$this->db->get();
		return $query->result();
	}

	public function akhir() 
	{
		$this->db->select('*');
		$this->db->from('vamr4846_vama.periode_stok_pusat');
		$this->db->order_by('id_periode_stok_pusat', 'DESC');
		$query=$this->db->get();
		return $query->row();
	}

	private function _get_datatables_query()
	{
		$this->db->select('psp.*, pp.nama_pegawai AS pegawai_save');
		$this->db->from('vamr4846_vama.periode_stok_pusat AS psp');
		$this->db->join('vamr4846_vama.pegawai_pusat AS pp', 'pp.id_pegawai=psp.id_pegawai_pembuatan', 'LEFT');

		$i = 0;
		foreach ($this->column_search as $item){
			if($_POST['search']['value']){
				if($i===0){
					$this->db->group_start();
					$this->db->like($item, $_POST['search']['value']);
				}else{
					$this->db->or_like($item, $_POST['search']['value']);
				}

				if(count($this->column_search) - 1 == $i)
					$this->db->group_end();
			}
			$i++;
		}
		
		if(isset($_POST['order'])){
			$this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		}else if(isset($this->order)){
			$order = $this->order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}

	function get_datatables()
	{
		$this->_get_datatables_query();
		if($_POST['length'] != -1)
		$this->db->limit($_POST['length'], $_POST['start']);
		$query = $this->db->get();
		return $query->result();
	}

	function count_filtered()
	{
		$this->_get_datatables_query();
		$query = $this->db->get();
		return $query->num_rows();
	}

	public function count_all()
	{
		$this->db->from($this->table);
		return $this->db->count_all_results();
	}

	public function get_by_id($id_periode_stok_pusat)
	{
		$this->db->select('*');
		$this->db->from($this->table);
		$this->db->where('id_periode_stok_pusat', $id_periode_stok_pusat);
		$query = $this->db->get();
		return $query->row();
	}

	public function save($data)
	{
		$this->db->insert($this->table, $data);
		return $this->db->insert_id();
	}

	public function update($where, $data)
	{
		$this->db->update($this->table, $data, $where);
		return $this->db->affected_rows();
	}

	public function delete_by_id($id_periode_stok_pusat)
	{
		$this->db->where('id_periode_stok_pusat', $id_periode_stok_pusat);
		$this->db->delete($this->table);
	}

	public function save_stok_opname($table, $data)
	{
		$sql = "
			INSERT INTO ".$table." (
			id_barang_pusat, jumlah_so_sebelumnya, jumlah_so, id_periode_stok_pusat, 
			masuk_stok, catatan, id_pegawai_pembuatan, id_pegawai_pembaharuan, tanggal_pembuatan)
			".$data."
		";
		$this->db->query($sql);
		// return $this->db->insert($table, $data);
		// return $this->db->insert_id();
	}
}

<?php
class Hadiah_master_model extends CI_Model
{
	// Hadiah induk
	var $column_order         = array('hm.no_hadiah', 'hm.nama_hadiah', 'cp.nama_customer_pusat', 'hm.status_hadiah', 'hm.tanggal', 
									  'jumlah_barang', 'total_qty', 'total', 'keterangan_lain', 'pp1.nama_pegawai', 'hm.tanggal_pembuatan',
									  'pp2.nama_pegawai', 'hm.tanggal_pembaharuan');
	var $column_search        = array('hm.no_hadiah', 'hm.nama_hadiah', 'cp.nama_customer_pusat', 'hm.status_hadiah', 'hm.keterangan_lain'); 
	var $order                = array('hm.id_hadiah_m' => 'desc');
	
	// Hadiah detail
	var $column_order_detail  = array('hm.no_hadiah', 'hm.nama_hadiah', 'hm.status_hadiah', 'hm.tanggal', 'bp.sku', 'bp.nama_barang', 
									  'hd.harga_satuan', 'hd.jumlah_hadiah', 'hd.subtotal', 'cp.nama_customer', 'hm.keterangan_lain', 
									  'pp1.nama_pegawai', 'hd.tanggal_pembuatan', 'pp2.nama_pegawai', 'hd.tanggal_pembaharuan');
	var $column_search_detail = array('hm.no_hadiah', 'hm.nama_hadiah', 'bp.sku', 'bp.nama_barang', 
									  'cp.nama_customer_pusat', 'hm.status_hadiah', 'hm.keterangan_lain'); 
	var $order_detail         = array('id_hadiah_d' => 'desc');
	
	// Hadiah batal
	var $column_order_batal   = array('hdb.no_hadiah', 'hdb.nama_hadiah', 'bp.sku', 'bp.nama_barang', 
									  'hdb.harga_satuan', 'hdb.jumlah_hadiah', 'hdb.subtotal', 'cp.nama_customer', 'hdb.keterangan_lain',
									  'hdb.keterangan_batal', 'pp3.nama_pegawai', 'hdb.tanggal_pembatalan', 
									  'pp1.nama_pegawai', 'hd.tanggal_pembuatan', 'pp2.nama_pegawai', 'hdb.tanggal_pembaharuan');
	var $column_search_batal  = array('hdb.no_hadiah', 'hdb.nama_hadiah', 'cp.nama_customer_pusat', 
									  'hdb.keterangan_lain', 'hdb.keterangan_batal'); 
	var $order_batal          = array('id_hadiah_b' => 'desc');

	private function _get_laporan_query($tanggal_awal, $tanggal_akhir)
	{		
		$this->db->select('
							hm.id_hadiah_m, no_hadiah, nama_hadiah, IFNULL(cp.nama_customer_pusat,"-") AS nama_customer, 
							COUNT(hd.id_hadiah_d) AS jumlah_barang, SUM(hd.jumlah_hadiah) AS total_qty,
							hm.total, tanggal, hm.status_hadiah , hm.keterangan_lain,
							IFNULL(pp1.nama_pegawai, "-") AS pegawai_save, IFNULL(pp2.nama_pegawai, "-") AS pegawai_edit,
							hm.tanggal_pembuatan, hm.tanggal_pembaharuan
						 ');
		$this->db->from('vamr4846_vama.hadiah_master AS hm');
		$this->db->join('vamr4846_vama.hadiah_detail AS hd', 'hd.id_hadiah_m=hm.id_hadiah_m', 'LEFT');
		$this->db->join('vamr4846_vama.customer_pusat AS cp', 'cp.id_customer_pusat=hm.id_customer_pusat', 'LEFT');
		$this->db->join('vamr4846_vama.pegawai_pusat AS pp1', 'pp1.id_pegawai=hm.id_pegawai_pembuatan', 'LEFT');
		$this->db->join('vamr4846_vama.pegawai_pusat AS pp2', 'pp2.id_pegawai=hm.id_pegawai_pembaharuan', 'LEFT');
		$this->db->where('DATE(hm.tanggal) BETWEEN "'. date('Y-m-d', strtotime($tanggal_awal)). '" AND 
												   "'. date('Y-m-d', strtotime($tanggal_akhir)).'"');
		$this->db->group_by('hm.id_hadiah_m');

		$i = 0;
		foreach ($this->column_search as $item){
			if($_POST['search']['value']){
				if($i === 0){
					$this->db->group_start();
					$this->db->like($item, $_POST['search']['value']);
				}else{
					$this->db->or_like($item, $_POST['search']['value']);
				}

				if(count($this->column_search) - 1 == $i)
					$this->db->group_end();
			}
			$i++;
		}

		if(isset($_POST['order'])){
			$this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		}else if(isset($this->order)){
			$order = $this->order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}

	private function _get_laporan_query_detail($tanggal_awal, $tanggal_akhir)
	{		
		$this->db->select('
							hm.id_hadiah_m, hd.id_hadiah_d,
							hm.no_hadiah, hm.nama_hadiah, hm.status_hadiah, hm.tanggal, bp.sku, bp.nama_barang, hd.harga_satuan, 
							hd.jumlah_hadiah, hd.subtotal, IFNULL(cp.nama_customer_pusat,"-") AS nama_customer, hm.keterangan_lain, 
							IFNULL(pp1.nama_pegawai, "-") AS pegawai_save, IFNULL(pp2.nama_pegawai, "-") AS pegawai_edit, 
							hd.tanggal_pembuatan, hd.tanggal_pembaharuan
						 ');
		$this->db->from('vamr4846_vama.hadiah_detail AS hd');
		$this->db->join('vamr4846_vama.hadiah_master AS hm', 'hd.id_hadiah_m=hm.id_hadiah_m', 'LEFT');
		$this->db->join('vamr4846_vama.barang_pusat AS bp', 'bp.id_barang_pusat=hd.id_barang_pusat', 'LEFT');
		$this->db->join('vamr4846_vama.customer_pusat AS cp', 'cp.id_customer_pusat=hm.id_customer_pusat', 'LEFT');
		$this->db->join('vamr4846_vama.pegawai_pusat AS pp1', 'pp1.id_pegawai=hd.id_pegawai_pembuatan', 'LEFT');
		$this->db->join('vamr4846_vama.pegawai_pusat AS pp2', 'pp2.id_pegawai=hd.id_pegawai_pembaharuan', 'LEFT');
		$this->db->where('DATE(hm.tanggal) BETWEEN "'. date('Y-m-d', strtotime($tanggal_awal)). '" AND 
												   "'. date('Y-m-d', strtotime($tanggal_akhir)).'"');

		$i = 0;
		foreach ($this->column_search_detail as $item){
			if($_POST['search']['value']){
				if($i === 0){
					$this->db->group_start();
					$this->db->like($item, $_POST['search']['value']);
				}else{
					$this->db->or_like($item, $_POST['search']['value']);
				}

				if(count($this->column_search_detail) - 1 == $i)
					$this->db->group_end();
			}
			$i++;
		}

		if(isset($_POST['order'])){
			$this->db->order_by($this->column_order_detail[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		}else if(isset($this->order_detail)){
			$order = $this->order_detail;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}

	private function _get_laporan_query_batal($tanggal_awal, $tanggal_akhir)
	{		
		$this->db->select('
							hdb.id_hadiah_b, hdb.id_hadiah_d, hdb.id_hadiah_m, 
							hdb.no_hadiah, hdb.nama_hadiah, bp.sku, bp.nama_barang, hdb.harga_satuan,
							hdb.jumlah_hadiah, hdb.subtotal, IFNULL(cp.nama_customer_pusat,"-") AS nama_customer,
							IFNULL(pp1.nama_pegawai, "-") AS pegawai_save, IFNULL(pp2.nama_pegawai, "-") AS pegawai_edit, IFNULL(pp3.nama_pegawai, "-") AS pegawai_batal, 
							hdb.tanggal_pembuatan, hdb.tanggal_pembaharuan, hdb.tanggal_pembatalan, hdb.keterangan_batal,
						 ');
		$this->db->from('vamr4846_vama.hadiah_detail_batal AS hdb');
		$this->db->join('vamr4846_vama.barang_pusat AS bp', 'bp.id_barang_pusat=hdb.id_barang_pusat', 'LEFT');
		$this->db->join('vamr4846_vama.customer_pusat AS cp', 'cp.id_customer_pusat=hdb.id_customer_pusat', 'LEFT');
		$this->db->join('vamr4846_vama.pegawai_pusat AS pp1', 'pp1.id_pegawai=hdb.id_pegawai_pembuatan', 'LEFT');
		$this->db->join('vamr4846_vama.pegawai_pusat AS pp2', 'pp2.id_pegawai=hdb.id_pegawai_pembaharuan', 'LEFT');
		$this->db->join('vamr4846_vama.pegawai_pusat AS pp3', 'pp3.id_pegawai=hdb.id_pegawai_pembatalan', 'LEFT');
		$this->db->where('DATE(hdb.tanggal_pembatalan) BETWEEN "'. date('Y-m-d', strtotime($tanggal_awal)). '" AND 
												   "'. date('Y-m-d', strtotime($tanggal_akhir)).'"');

		$i = 0;
		foreach ($this->column_search_batal as $item){
			if($_POST['search']['value']){
				if($i === 0){
					$this->db->group_start();
					$this->db->like($item, $_POST['search']['value']);
				}else{
					$this->db->or_like($item, $_POST['search']['value']);
				}

				if(count($this->column_search_batal) - 1 == $i)
					$this->db->group_end();
			}
			$i++;
		}

		if(isset($_POST['order'])){
			$this->db->order_by($this->column_order_batal[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		}else if(isset($this->order_batal)){
			$order = $this->order_batal;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}

	function get_laporan($perintah, $tanggal_awal, $tanggal_akhir)
	{
		$this->$perintah($tanggal_awal, $tanggal_akhir);
		if($_POST['length'] != -1)
		$this->db->limit($_POST['length'], $_POST['start']);
		$query = $this->db->get();
		return $query->result();
	}

	function count_filtered($perintah, $tanggal_awal, $tanggal_akhir)
	{
		$this->$perintah($tanggal_awal, $tanggal_akhir);
		$query = $this->db->get();
		return $query->num_rows();
	}

	public function count_all($table, $tanggal_awal, $tanggal_akhir)
	{
		$this->db->from($table);
		if($table == 'vamr4846_vama.hadiah_master'){
			$this->db->where('DATE(tanggal) 
							  BETWEEN "'. date('Y-m-d', strtotime($tanggal_awal)). '" and "'. date('Y-m-d', strtotime($tanggal_akhir)).'"');
		}else if($table == 'vamr4846_vama.hadiah_detail'){
			$this->db->where('DATE(tanggal_pembuatan) 
							  BETWEEN "'. date('Y-m-d', strtotime($tanggal_awal)). '" and "'. date('Y-m-d', strtotime($tanggal_akhir)).'"');
		}else if($table == 'vamr4846_vama.hadiah_detail_batal'){
			$this->db->where('DATE(tanggal_pembatalan) 
							  BETWEEN "'. date('Y-m-d', strtotime($tanggal_awal)). '" and "'. date('Y-m-d', strtotime($tanggal_akhir)).'"');
		}
		return $this->db->count_all_results();
	}

	function no_hadiah_baru($tahun_sekarang, $bulan_sekarang, $id_pegawai) {
	    $this->db->select('COUNT(*)+1 AS no_baru');
	    $this->db->from('vamr4846_vama.hadiah_master');
	    $this->db->where(array(  
			'YEAR(tanggal)'        => $tahun_sekarang,
			'MONTH(tanggal)'       => $bulan_sekarang,
			'id_pegawai_pembuatan' => $id_pegawai
		));
	    $this->db->order_by('tanggal_pembuatan','DESC');
	    $query = $this->db->get();
	    return $query->row();
	}

	function insert_master($no_hadiah, $nama_hadiah, $tanggal, $id_pegawai, $id_customer_pusat, $total, $catatan)
	{
		$dt = array(
			'no_hadiah'            => $no_hadiah,
			'nama_hadiah'          => $nama_hadiah,
			'tanggal'              => $tanggal,
			'total'                => $total,
			'keterangan_lain'      => $catatan,
			'id_customer_pusat'    => (empty($id_customer_pusat)) ? NULL : $id_customer_pusat,
			'id_pegawai_pembuatan' => (empty($id_pegawai)) ? NULL : $id_pegawai,
			'tanggal_pembuatan'    => date('Y-m-d H:i:s')
		);
		return $this->db->insert('vamr4846_vama.hadiah_master', $dt);
	}

	function update_master($id_hadiah_m, $nama_hadiah, $id_pegawai, $id_customer_pusat, $total, $tanggal, $catatan, $status_hadiah)
	{
		$dt = array(
			'nama_hadiah'            => $nama_hadiah,
			'total'                  => $total,
			'tanggal'                => $tanggal,
			'keterangan_lain'        => $catatan,
			'status_hadiah'          => $status_hadiah,
			'id_customer_pusat'      => (empty($id_customer_pusat)) ? NULL : $id_customer_pusat,
			'id_pegawai_pembaharuan' => (empty($id_pegawai)) ? NULL : $id_pegawai
		);
		$where = array('id_hadiah_m' =>  $id_hadiah_m);
		return $this->db->update('vamr4846_vama.hadiah_master', $dt, $where);
	}

	function get_id($no_hadiah)
	{
		$this->db->select('*');
		$this->db->from('vamr4846_vama.hadiah_master');
		$this->db->where('no_hadiah', $no_hadiah);
		$query = $this->db->get();
		return $query->row();
	}

	function get_by_id($id_hadiah_m)
	{
		$this->db->select('*');
		$this->db->from('vamr4846_vama.hadiah_master');
		$this->db->where('id_hadiah_m', $id_hadiah_m);
		$query = $this->db->get();
		return $query->row();
	}

	function get_master($id_hadiah_m)
	{
		$this->db->select('hm.*, 
						  count(hd.id_hadiah_d) AS jumlah_barang,
						  IFNULL(cp.id_customer_pusat,"0") AS id_customer_pusat,
						  IFNULL(cp.kode_customer_pusat,"-") AS kode_customer_pusat, 
						  IFNULL(cp.nama_customer_pusat,"-") AS nama_customer_pusat, 
						  IFNULL(cp.tipe_customer_pusat,"-") AS tipe_customer_pusat, 
						  IFNULL(cp.handphone1,"-") AS handphone1, pp.nama_pegawai');
		$this->db->from('vamr4846_vama.hadiah_master AS hm');
		$this->db->join('vamr4846_vama.hadiah_detail as hd', 'hm.id_hadiah_m=hd.id_hadiah_m', 'LEFT');
		$this->db->join('vamr4846_vama.customer_pusat as cp', 'hm.id_customer_pusat=cp.id_customer_pusat', 'LEFT');
		$this->db->join('vamr4846_vama.pegawai_pusat as pp', 'hm.id_pegawai_pembaharuan=pp.id_pegawai', 'LEFT');
		$this->db->where('hm.id_hadiah_m', $id_hadiah_m);
		$query = $this->db->get();
		return $query->row();
	}

	function get_jumlah_batal($id_hadiah_m)
	{
		$this->db->select('id_hadiah_m, count(id_hadiah_b) AS jumlah_barang');
		$this->db->from('vamr4846_vama.hadiah_detail_batal');
		$this->db->where('id_hadiah_m', $id_hadiah_m);
		$query = $this->db->get();
		return $query->row();
	}

	function hapus_transaksi($id_hadiah_m, $id_customer_pusat, $no_hadiah, $nama_hadiah, $keterangan_batal, $id_pegawai_pembatalan)
	{
		$loop = $this->db
					 ->select('*')
					 ->where('id_hadiah_m', $id_hadiah_m)
					 ->get('vamr4846_vama.hadiah_detail');

		foreach($loop->result() as $b){
			$sql = "UPDATE `vamr4846_vama.barang_pusat` SET `total_stok` = `total_stok` + ".$b->jumlah_hadiah." 
					WHERE `id_barang_pusat` = '".$b->id_barang_pusat."' ";
			$this->db->query($sql);

			$sql = "INSERT INTO `vamr4846_vama.hadiah_detail_batal` 
					(
						id_hadiah_d, id_hadiah_m, id_customer_pusat, no_hadiah, nama_hadiah, id_barang_pusat, modal,
						harga_satuan, jumlah_hadiah, subtotal, id_pegawai_pembuatan, id_pegawai_pembaharuan, id_pegawai_pembatalan,
						tanggal_pembuatan, tanggal_pembaharuan, keterangan_batal
					) 
					VALUES 
					(
						'".$b->id_hadiah_d."', '".$b->id_hadiah_m."', '".$id_customer_pusat."', '".$no_hadiah."', 
						'".$nama_hadiah."', '".$b->id_barang_pusat."', '".$b->modal."', '".$b->harga_satuan."', 
						'".$b->jumlah_hadiah."', '".$b->subtotal."', '".$b->id_pegawai_pembuatan."', 
						'".$b->id_pegawai_pembaharuan."', '".$id_pegawai_pembatalan."', '".$b->tanggal_pembuatan."', 
						'".$b->tanggal_pembaharuan."', '".$keterangan_batal."'						
					)";
			$this->db->query($sql);						
		}

		$this->db->where('id_hadiah_m', $id_hadiah_m)->delete('vamr4846_vama.hadiah_detail');
		return $this->db
					->where('id_hadiah_m', $id_hadiah_m)
					->delete('vamr4846_vama.hadiah_master');
	}

	function hapus_master($id_hadiah_m)
	{
		return $this->db
			->where('id_hadiah_m', $id_hadiah_m)
			->delete('vamr4846_vama.hadiah_master');
	}

	function get_grand_total($tanggal_awal, $tanggal_akhir)
	{
		$this->db->select('
							COUNT(id_hadiah_m) AS jml_transaksi, 
							SUM(total) AS grand_total_hadiah
					      ');
		$this->db->from('vamr4846_vama.hadiah_master');
		$this->db->where('DATE(tanggal) BETWEEN "'. date('Y-m-d', strtotime($tanggal_awal)). '" AND 
												"'. date('Y-m-d', strtotime($tanggal_akhir)).'"');
		$query = $this->db->get();
		return $query->row();
	}

	function get_total_selesai_perbulan($tanggal_awal, $tanggal_akhir)
	{
		$this->db->select('
							IFNULL(MIN(DATE_FORMAT(tanggal, "%d-%m-%y")),"00-00-00") AS tanggal_awal,
							IFNULL(MAX(DATE_FORMAT(tanggal, "%d-%m-%y")),"00-00-00") AS tanggal_akhir,
							IFNULL(COUNT(id_hadiah_m),0) AS jml_transaksi, 
							IFNULL(SUM(total),0) AS grand_total
					      ');
		$this->db->from('vamr4846_vama.hadiah_master');
		$this->db->where(array(
								'tanggal >' 	=> $tanggal_awal,
								'tanggal <=' 	=> $tanggal_akhir,
								'status_hadiah'	=> 'SELESAI'
							   )
						);
		$query = $this->db->get();
		return $query->row();
	}

	function get_total_menunggu_perbulan($tanggal_awal, $tanggal_akhir)
	{
		$this->db->select('
							IFNULL(MIN(DATE_FORMAT(tanggal, "%d-%m-%y")),"00-00-00") AS tanggal_awal,
							IFNULL(MAX(DATE_FORMAT(tanggal, "%d-%m-%y")),"00-00-00") AS tanggal_akhir,
							IFNULL(COUNT(id_hadiah_m),0) AS jml_transaksi, 
							IFNULL(SUM(total),0) AS grand_total
					      ');
		$this->db->from('vamr4846_vama.hadiah_master');
		$this->db->where(array(
								'tanggal >' 	=> $tanggal_awal,
								'tanggal <=' 	=> $tanggal_akhir,
								'status_hadiah' => 'MENUNGGU'
							   )
						);
		$query = $this->db->get();
		return $query->row();
	}
}
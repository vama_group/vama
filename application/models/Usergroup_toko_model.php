<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Usergroup_toko_model extends CI_Model {

	// Load database
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	// Listing data
	public function listing(){
		$this->db->select('*');
		$this->db->from('vamr4846_toko_mrc.usergroup_toko');
		$this->db->order_by('id_usergroup','ASC');
		$query=$this->db->get();
		return $query->result();
	}

	// Detail data
	public function detail($id_usergroup){
		$this->db->select('*');
		$this->db->from('vamr4846_toko_mrc.usergroup_toko');
		$this->db->where('id_usergroup',$id_usergroup);
		$this->db->order_by('id_usergroup','DESC');
		$query=$this->db->get();
		return $query->result();
	}	

	// Insert data
	public function tambah(){
		$this->db->insert('vamr4846_toko_mrc.usergroup_toko',$data);
	}	

	// Update data
	public function edit(){
		$this->db->where('vamr4846_toko_mrc.id_usergroup',$data['id_usergroup']);
		$this->db->update('usergroup_toko',$data);
	}	

	// Delete data
	public function delete(){
		$this->db->where('vamr4846_toko_mrc.id_usergroup',$data['id_usergroup']);
		$this->db->delete('usergroup_toko',$data);
	}	
}

/* End of file Pegaai_model.php */
/* Location: ./application/models/Pegaai_model.php */
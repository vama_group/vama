<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Customer_pusat_model extends MY_Model {
	// Table customer pusat
	var $table 			= 'vamr4846_vama.customer_pusat';
	var $column_order	= array('cp.kode_customer_pusat', 'cp.nama_customer_pusat', 'cp.email', 
								'cp.telephone1', 'cp.handphone1','cp.fax','cp.asal_customer_pusat','cp.tipe_customer_pusat'); 
	var $column_search 	= array('cp.kode_customer_pusat', 'cp.nama_customer_pusat', 'cp.email', 
								'cp.telephone1', 'cp.handphone1','cp.fax','cp.asal_customer_pusat','cp.tipe_customer_pusat'); 
	var $order 			= array('cp.id_customer_pusat' => 'desc');

	// Table pendapatan poin
	var $column_order_pendapatan_poin  = array('ppo.no_penjualan', 'ppo.total_penjualan', 'ppo.jumlah_poin', 
											   'ppo.tanggal_pembaharuan', 'pp1.nama_pegawai'); 
	var $column_search_pendapatan_poin = array('ppo.no_penjualan', 'ppo.total_penjualan', 'ppo.jumlah_poin'); 
	var $order_pendapatan_poin         = array('ppo.id_poin_online' => 'desc');

	// Table penukaran poin
	var $column_order_penukaran_poin  = array('ppo.no_penukaran_poin', 'bp.nama_barang', 'ppo.qty', 
											  'ppo.tanggal_pembaharuan', 'ppo.nama_pegawai'); 
	var $column_search_penukaran_poin = array('ppo.no_penukaran_poin', 'bp.nama_barang', 'ppo.qty'); 
	var $order_penukaran_poin         = array('ppo.id_poin_online' => 'desc');

	private function _get_datatables_query()
	{		
		$this->db->select('
			cp.*, 
			IFNULL(rso.nama_sumber_online, "-") AS nama_sumber_online, 
			pp1.nama_pegawai AS pegawai_save, pp2.nama_pegawai As pegawai_edit'
		);
		$this->db->from('vamr4846_vama.customer_pusat AS cp');
		$this->db->join('vamr4846_vama.referensi_sumber_online AS rso', 'rso.id_referensi=cp.id_referensi', 'LEFT');
		$this->db->join('vamr4846_vama.pegawai_pusat AS pp1', 'pp1.id_pegawai=cp.id_pegawai_pembuatan', 'LEFT');
		$this->db->join('vamr4846_vama.pegawai_pusat AS pp2', 'pp2.id_pegawai=cp.id_pegawai_pembaharuan', 'LEFT');
		$this->db->where('cp.status_hapus', 'TIDAK');

		$i = 0;
		foreach ($this->column_search as $item){
			if($_POST['search']['value']){
				if($i===0){
					$this->db->group_start();
					$this->db->like($item, $_POST['search']['value']);
				}else{
					$this->db->or_like($item, $_POST['search']['value']);
				}

				if(count($this->column_search) - 1 == $i)
					$this->db->group_end();
				}
			$i++;
		}
		
		if(isset($_POST['order'])){
			$this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		}else if(isset($this->order)){
			$order = $this->order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}

	private function _get_datatables_query_pendapatan_poin($id_customer_pusat)
	{		
		$this->db->select('
			ppo.*, 
			pp1.nama_pegawai AS pegawai_save, pp2.nama_pegawai As pegawai_edit'
		);
		$this->db->from('vamr4846_vama.poin_penjualan_online AS ppo');
		$this->db->join('vamr4846_vama.pegawai_pusat AS pp1', 'pp1.id_pegawai=ppo.id_pegawai_pembuatan', 'LEFT');
		$this->db->join('vamr4846_vama.pegawai_pusat AS pp2', 'pp2.id_pegawai=ppo.id_pegawai_pembaharuan', 'LEFT');
		$this->db->where('ppo.id_customer_pusat', $id_customer_pusat);

		$i = 0;
		foreach ($this->column_search_pendapatan_poin as $item){
			if($_POST['search']['value']){
				if($i===0){
					$this->db->group_start();
					$this->db->like($item, $_POST['search']['value']);
				}else{
					$this->db->or_like($item, $_POST['search']['value']);
				}

				if(count($this->column_search_pendapatan_poin) - 1 == $i)
					$this->db->group_end();
				}
			$i++;
		}
		
		if(isset($_POST['order'])){
			$this->db->order_by($this->column_order_pendapatan_poin[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		}else if(isset($this->order_pendapatan_poin)){
			$order = $this->order_pendapatan_poin;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}

	function get_datatables($perintah, $id_customer_pusat)
	{
		$this->$perintah($id_customer_pusat);
		if($_POST['length'] != -1)
		$this->db->limit($_POST['length'], $_POST['start']);
		$query = $this->db->get();
		return $query->result();
	}

	function count_filtered($perintah, $id_customer_pusat)
	{
		$this->$perintah($id_customer_pusat);
		$query = $this->db->get();
		return $query->num_rows();
	}

	public function count_all($table, $id_customer_pusat)
	{
		$this->db->from($table);
		if($table == 'vamr4846_vama.customer_pusat'){
			$this->db->where('status_hapus', 'TIDAK');			
		}else if($table == 'vamr4846_vama.poin_penjualan_online'){
			$this->db->where('id_customer_pusat', $id_customer_pusat);						
		}
		return $this->db->count_all_results();
	}

	function cari_customer($keyword, $tipe_customer_pusat)
	{
		$sql = "
			SELECT 
				id_customer_pusat, kode_customer_pusat, nama_customer_pusat, tipe_customer_pusat, 
				IF(handphone1='','Tidak ada',handphone1) AS handphone1, IF(email='','Tidak ada email',email) AS email, foto
			FROM 
				vamr4846_vama.customer_pusat 
			WHERE 
				( 	id_customer_pusat LIKE '%".$this->db->escape_like_str($keyword)."%' 
					OR nama_customer_pusat LIKE '%".$this->db->escape_like_str($keyword)."%' 
					OR email LIKE '%".$this->db->escape_like_str($keyword)."%' 
				) AND tipe_customer_pusat !='".$tipe_customer_pusat."' 
		";

		return $this->db->query($sql);
	}

	public function cari_customer_pusat($id_customer_pusat) {
		$this->db->select('*');
		$this->db->from('vamr4846_vama.customer_pusat AS cp');
		$this->db->where('id_customer_pusat',$id_customer_pusat);
		$query=$this->db->get();
		return $query->row();
	}

	public function cari_kode_customer_pusat($kode_customer_pusat) {
		$this->db->select('*');
		$this->db->from('vamr4846_vama.customer_pusat AS cp');
		$this->db->where('kode_customer_pusat',$kode_customer_pusat);
		$query=$this->db->get();
		return $query->row();
	}

	public function listing() {
		$this->db->select('*');
		$this->db->from('vamr4846_vama.customer_pusat AS cp');
		$this->db->order_by('id_customer_pusat','DESC');
		$query=$this->db->get();
		return $query->result();
	}

	// Listing data mrc
	public function listing_toko() {
		$this->db->select('cp.*');
		$this->db->from('vamr4846_vama.customer_pusat AS cp');
		$this->db->where('cp.tipe_customer_pusat', 'MRC');		
		$this->db->order_by('cp.id_customer_pusat', 'ASC');
		$query=$this->db->get();
		return $query->result();
	}

	// Dapatkan kode terakhir customer umum
	public function akhir_umum() {
		$this->db->select('count(id_customer_pusat)+1 AS jumlah_customer');
		$this->db->from('vamr4846_vama.customer_pusat AS cp');
		$this->db->not_like('tipe_customer_pusat','MRC');
		$this->db->order_by('id_customer_pusat','DESC');
		$query=$this->db->get();
		return $query->row();
	}	

	// Dapatkan kode terakhir customer "MRC"
	public function akhir_toko() {
		$this->db->select('count(id_customer_pusat)+1 AS jumlah_customer');
		$this->db->from('vamr4846_vama.customer_pusat AS cp');
		$this->db->where('tipe_customer_pusat','MRC');
		$this->db->order_by('id_customer_pusat','DESC');
		$query=$this->db->get();
		return $query->row();
	}

	public function get_by_id($id_customer_pusat)
	{
		$this->db->select('cp.*, IFNULL(rso.id_referensi, "0") AS id_referensi');
		$this->db->from('vamr4846_vama.customer_pusat AS cp');
		$this->db->join('vamr4846_vama.referensi_sumber_online AS rso', 'rso.id_referensi=cp.id_referensi', 'LEFT');
		$this->db->where('cp.id_customer_pusat',$id_customer_pusat);
		$query = $this->db->get();
		return $query->row();
	}

	public function get_by_kode($kode_customer_pusat)
	{
		$this->db->select('cp.*, IFNULL(rso.id_referensi, "0") AS id_referensi');
		$this->db->from('vamr4846_vama.customer_pusat AS cp');
		$this->db->join('vamr4846_vama.referensi_sumber_online AS rso', 'rso.id_referensi=cp.id_referensi', 'LEFT');
		$this->db->where('cp.kode_customer_pusat', $kode_customer_pusat);
		$query = $this->db->get();
		return $query->row();
	}

	public function validasi_customer($kode_customer_lama)
	{
		$this->db->select('cp.*, IFNULL(rso.id_referensi, "0") AS id_referensi');
		$this->db->from('vamr4846_vama.customer_pusat AS cp');
		$this->db->join('vamr4846_vama.referensi_sumber_online AS rso', 'rso.id_referensi=cp.id_referensi', 'LEFT');
		$this->db->where('cp.kode_customer_lama', $kode_customer_lama);
		$query = $this->db->get();
		return $query->row();
	}

	public function get_by_nama($nama_customer_pusat)
	{
		$this->db->from($this->table);
		$this->db->where('nama_customer_pusat',$nama_customer_pusat);
		$query = $this->db->get();
		return $query->row();
	}

	public function get_by_email($email)
	{
		$this->db->from($this->table);
		$this->db->where('email',$email);
		$query = $this->db->get();
		return $query->row();
	}

	function get_customer_by_email($email)
	{
		return $this->db
			->select('id_customer_pusat, nama_customer_pusat, handphone1, tipe_customer_pusat')
			->where('email', $email)
			->limit(1)
			->get('vamr4846_vama.customer_pusat');
	}

	public function get_by_handphone1($handphone1)
	{
		$this->db->from($this->table);
		$this->db->where('handphone1',$handphone1);
		$query = $this->db->get();
		return $query->row();
	}

	public function save($data)
	{
		$this->db->insert($this->table, $data);
		return $this->db->insert_id();
	}

	public function create_table_toko($kode_toko){
		$sql_query = $this->db->query("
			CALL vamr4846_toko_mrc.CreateTableToko('".$kode_toko."')
		");
	}

	public function update($where, $data)
	{
		$this->db->update($this->table, $data, $where);
		return $this->db->affected_rows();
	}

	public function delete_by_id($id_customer_pusat)
	{
		$this->db->where('id_customer_pusat', $id_customer_pusat);
		$this->db->delete($this->table);
	}

	public function update_status_hapus($id_customer_pusat, $id_pegawai)
	{
		$sql = "UPDATE vamr4846_vama.customer_pusat 
				SET status_hapus = 'IYA', id_pegawai_pembaharuan = '".$this->db->escape_like_str($id_pegawai)."' 
				WHERE id_customer_pusat = '".$this->db->escape_like_str($id_customer_pusat)."'";
		return $this->db->query($sql);
	}

	function ambil_customer_pusat_lama()
	{
		$sql = "SELECT * FROM vamr4846_vama.customer_pusat_lama";
		return $this->db->query($sql);
	}
}

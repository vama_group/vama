<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kategori_paket_model extends MY_Model {

	var $table         = 'vamr4846_toko_mrc.kategori_paket';
	var $column_order  = array(
		'kp.id_kategori_paket','kp.id_kategori_paket','kp.kode_kategori_paket','kp.nama_kategori_paket',
		'pp1.nama_pegawai_toko','kp.tanggal_pembuatan','pp2.nama_pegawai_toko','kp.tanggal_pembaharuan');
	var $column_search = array('kp.kode_kategori_paket','kp.nama_kategori_paket');
	var $order         = array('kp.id_kategori_paket' => 'desc');

	private function _get_datatables_query()
	{
		$this->db->select('kp.*, pp1.nama_pegawai AS pegawai_save, pp2.nama_pegawai As pegawai_edit');
		$this->db->from('vamr4846_toko_mrc.kategori_paket AS kp');
		$this->db->join('vamr4846_toko_mrc.pegawai_toko AS pp1', 'pp1.id_pegawai_toko=kp.id_pegawai_pembuatan', 'LEFT');
		$this->db->join('vamr4846_toko_mrc.pegawai_toko AS pp2', 'pp2.id_pegawai_toko=kp.id_pegawai_pembaharuan', 'LEFT');
		$this->db->where('kp.status_hapus', 'TIDAK');

		$i = 0;
		foreach ($this->column_search as $item){
			if($_POST['search']['value']){
				if($i===0){
					$this->db->group_start();
					$this->db->like($item, $_POST['search']['value']);
				}else{
					$this->db->or_like($item, $_POST['search']['value']);
				}

				if(count($this->column_search) - 1 == $i)
					$this->db->group_end();
			}
			$i++;
		}
		
		if(isset($_POST['order'])){
			$this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		}else if(isset($this->order)){
			$order = $this->order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}

	function get_datatables()
	{
		$this->_get_datatables_query();
		if($_POST['length'] != -1)
		$this->db->limit($_POST['length'], $_POST['start']);
		$query = $this->db->get();
		return $query->result();
	}

	function count_filtered()
	{
		$this->_get_datatables_query();
		$query = $this->db->get();
		return $query->num_rows();
	}

	public function count_all()
	{
		$this->db->from($this->table);
		$this->db->where('status_hapus', 'TIDAK');
		return $this->db->count_all_results();
	}

	// Listing data
	public function listing($nama_kategori_paket){
		$this->db->select('*');
		$this->db->from('vamr4846_toko_mrc.kategori_paket');
		$this->db->like('nama_kategori_paket', $nama_kategori_paket);
		$this->db->order_by('nama_kategori_paket','ASC');
		$query=$this->db->get();
		return $query->result();
	}

	// Dapatkan kode terakhir
	public function akhir(){
		$this->db->select('*');
		$this->db->from('vamr4846_toko_mrc.kategori_paket');
		$this->db->order_by('id_kategori_paket','DESC');
		$query=$this->db->get();
		return $query->row();
	}

	public function save($data)
	{
		$this->db->insert($this->table, $data);
		return $this->db->insert_id();
	}

	public function update($where, $data)
	{
		$this->db->update($this->table, $data, $where);
		return $this->db->affected_rows();
	}

	public function delete_by_id($id_kategori_paket)
	{
		$this->db->where('id_kategori_paket', $id_kategori_paket);
		$this->db->delete($this->table);
	}

	public function update_status_hapus($id_kategori_paket, $id_pegawai)
	{
		$sql = "UPDATE vamr4846_toko_mrc.kategori_paket 
				SET status_hapus = 'IYA', id_pegawai_hapus = '".$id_pegawai."' 
				WHERE id_kategori_paket = '".$id_kategori_paket."'";
		return $this->db->query($sql);
	}

	// ------------------------------------------------------------------------
	function get_kategori($nama_field, $nama_kategori_paket)
	{
		$this->db->select('*');
		$this->db->from('vamr4846_toko_mrc.kategori_paket');
		$this->db->where(array(  
			$nama_field => $nama_kategori_paket
		));
		$query = $this->db->get();
		return $query->row();
	}

	function insert_kategori_paket($nama_kategori_paket, $id_pegawai)
	{ 
		$dt = array (
			'nama_kategori_paket'  => $nama_kategori_paket,
			'id_pegawai_pembuatan' => $id_pegawai,
			'tanggal_pembuatan'    => date('Y-m-d H:i:s') 		
		);
		return $this->db->insert('vamr4846_toko_mrc.kategori_paket', $dt);
	}

	function update_kategori_paket($id_kategori_paket, $nama_kategori_paket, $id_pegawai){
		$dt = array ( 	
			'nama_kategori_paket'    => $nama_kategori_paket,
			'id_pegawai_pembaharuan' => $id_pegawai
		);

		$where = array (	
			'id_kategori_paket' => $id_kategori_paket
		);

		return $this->db->update('vamr4846_toko_mrc.kategori_paket', $dt, $where);
	}
}

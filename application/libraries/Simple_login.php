<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Simple_login
{
	// SET SUPER GLOBAL
	var $CI = NULL;
	public function __construct()
	{
        $this->CI =& get_instance();
	}

	public function mac(){
		ob_start();
		system('ipconfig/all');
		$mycom = ob_get_contents();
		ob_clean();

		$findme = "Physical";
		$pmac   = strpos($mycom, $findme);
		$mac    = substr($mycom, ($pmac+36),17);
		return $mac;
	}
	// Login
	public function login($username, $password){
		// Query untuk pencocokan data
	    $query = $this->CI->db->get_where('vamr4846_vama.pegawai_pusat', array(
			'email'    => $username, 
			'password' => sha1($password)
		));
	                    
	    // Jika ada hasilnya
	    if($query->num_rows() == 1){
	      $row = $this->CI->db->query('
					SELECT pg.*, ug.usergroup_name 
					FROM vamr4846_vama.pegawai_pusat as pg
					LEFT JOIN vamr4846_vama.usergroup as ug on ug.id_usergroup=pg.id_usergroup
					WHERE email = "'.$username.'" AND password = "'.sha1($password).'"
			');
			
			$admin          = $row->row();
			$id             = $admin->id_pegawai;
			$level          = $admin->id_usergroup;
			$password       = $admin->password;
			$nama_pegawai   = $admin->nama_pegawai;
			$foto           = $admin->foto;
			$usergroup_name = $admin->usergroup_name;

			$mac = $this->mac();
			$this->CI->session->set_userdata('foto', $foto); 
			$this->CI->session->set_userdata('username', $username); 
			$this->CI->session->set_userdata('usergroup_name', $usergroup_name); 
			$this->CI->session->set_userdata('id_usergroup', $level); 
			$this->CI->session->set_userdata('nama_pegawai', $nama_pegawai);
			$this->CI->session->set_userdata('password', $password);
			$this->CI->session->set_userdata('id_login', uniqid(rand()));
			$this->CI->session->set_userdata('id_pegawai', $id);
			// $this->CI->session->set_userdata('id_pegawai_'.$mac.'', $id);

			// Kalau benar di redirect
			redirect(base_url().'dashboard');
	    }else{
			$this->CI->session->set_flashdata('info_login',
			'Oopss..! Ada sedikit kesalahan. Coba cek email dan password kamu, lalu coba lagi ya!');
			redirect(base_url().'login');
	    }
	    return false;
	}
	  
	// Cek login
	public function cek_login(){
		if($this->CI->session->userdata('username') == '' && 
			$this->CI->session->userdata('id_usergroup')=='') {
		  	$this->CI->session->set_flashdata('info_login', 'Oops...silakan login dulu');
		  	redirect(base_url().'login');
		}else{
			$id_pegawai = $this->CI->session->userdata('id_pegawai');
			$query      = $this->CI->db->query('
				SELECT pg.id_pegawai, pg.nama_pegawai, ug.usergroup_name 
				FROM vamr4846_vama.pegawai_pusat as pg
				LEFT JOIN vamr4846_vama.usergroup as ug on ug.id_usergroup=pg.id_usergroup
				WHERE id_pegawai = "'.$id_pegawai.'"
			');
			
			$data_pegawai = $query->row();
			if($data_pegawai){
				$this->CI->session->set_userdata('usergroup_name', $data_pegawai->usergroup_name);
			}
		}  
	}
	  
	// Logout
	public function logout(){
		$this->CI->session->sess_destroy();
		// $this->CI->session->unset_userdata('foto');
		// $this->CI->session->unset_userdata('username');
		// $this->CI->session->unset_userdata('id_usergroup');
		// $this->CI->session->unset_userdata('usergroup_name');
		// $this->CI->session->unset_userdata('nama_pegawai');
		// $this->CI->session->unset_userdata('password');
		// $this->CI->session->unset_userdata('id_login');
		// $this->CI->session->unset_userdata('id_pegawai');
		// $this->CI->session->unset_userdata('link');
		$this->CI->session->set_flashdata('info_login','Terimakasih, Anda berhasil logout');
		redirect(base_url().'login');
	}	

	// Lock screen
	public function cek_lockscreen(){
		if($this->CI->session->userdata('password') == '') {
			$this->CI->session->set_flashdata('info_login',
			'Oops...silakan login dulu');
			redirect(base_url().'login/lock');
		}
	}
}

/* End of file Simple_login.php */
/* Location: ./application/libraries/Simple_login.php */

DELIMITER $$

USE `vama`$$

DROP PROCEDURE IF EXISTS `InsertUserAccessPusat`$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `InsertUserAccessPusat`(IN XID_Pegawai VARCHAR(15), XID_Pegawai_Save VARCHAR(15), XTanggal_Pembuatan VARCHAR(19))
BEGIN
	#Dashboard
	INSERT INTO useraccess_pusat (id_pegawai, id_menu, act_read, act_create, act_update, act_delete, id_pegawai_pembuatan, tanggal_pembuatan) VALUES (XID_Pegawai, '1', 0, 0, 0, 0, XID_Pegawai_Save, XTanggal_Pembuatan);

	#Master Data
	INSERT INTO useraccess_pusat (id_pegawai, id_menu, act_read, act_create, act_update, act_delete, id_pegawai_pembuatan, tanggal_pembuatan) VALUES (XID_Pegawai, '2', 0, 0, 0, 0, XID_Pegawai_Save, XTanggal_Pembuatan);
	INSERT INTO useraccess_pusat (id_pegawai, id_menu, act_read, act_create, act_update, act_delete, id_pegawai_pembuatan, tanggal_pembuatan) VALUES (XID_Pegawai, '3', 0, 0, 0, 0, XID_Pegawai_Save, XTanggal_Pembuatan);
	INSERT INTO useraccess_pusat (id_pegawai, id_menu, act_read, act_create, act_update, act_delete, id_pegawai_pembuatan, tanggal_pembuatan) VALUES (XID_Pegawai, '4', 0, 0, 0, 0, XID_Pegawai_Save, XTanggal_Pembuatan);
	INSERT INTO useraccess_pusat (id_pegawai, id_menu, act_read, act_create, act_update, act_delete, id_pegawai_pembuatan, tanggal_pembuatan) VALUES (XID_Pegawai, '5', 0, 0, 0, 0, XID_Pegawai_Save, XTanggal_Pembuatan);
	INSERT INTO useraccess_pusat (id_pegawai, id_menu, act_read, act_create, act_update, act_delete, id_pegawai_pembuatan, tanggal_pembuatan) VALUES (XID_Pegawai, '6', 0, 0, 0, 0, XID_Pegawai_Save, XTanggal_Pembuatan);
	INSERT INTO useraccess_pusat (id_pegawai, id_menu, act_read, act_create, act_update, act_delete, id_pegawai_pembuatan, tanggal_pembuatan) VALUES (XID_Pegawai, '7', 0, 0, 0, 0, XID_Pegawai_Save, XTanggal_Pembuatan);
	INSERT INTO useraccess_pusat (id_pegawai, id_menu, act_read, act_create, act_update, act_delete, id_pegawai_pembuatan, tanggal_pembuatan) VALUES (XID_Pegawai, '8', 0, 0, 0, 0, XID_Pegawai_Save, XTanggal_Pembuatan);
	INSERT INTO useraccess_pusat (id_pegawai, id_menu, act_read, act_create, act_update, act_delete, id_pegawai_pembuatan, tanggal_pembuatan) VALUES (XID_Pegawai, '9', 0, 0, 0, 0, XID_Pegawai_Save, XTanggal_Pembuatan);
	INSERT INTO useraccess_pusat (id_pegawai, id_menu, act_read, act_create, act_update, act_delete, id_pegawai_pembuatan, tanggal_pembuatan) VALUES (XID_Pegawai, '10', 0, 0, 0, 0, XID_Pegawai_Save, XTanggal_Pembuatan);
	INSERT INTO useraccess_pusat (id_pegawai, id_menu, act_read, act_create, act_update, act_delete, id_pegawai_pembuatan, tanggal_pembuatan) VALUES (XID_Pegawai, '11', 0, 0, 0, 0, XID_Pegawai_Save, XTanggal_Pembuatan);
	INSERT INTO useraccess_pusat (id_pegawai, id_menu, act_read, act_create, act_update, act_delete, id_pegawai_pembuatan, tanggal_pembuatan) VALUES (XID_Pegawai, '12', 0, 0, 0, 0, XID_Pegawai_Save, XTanggal_Pembuatan);	
	INSERT INTO useraccess_pusat (id_pegawai, id_menu, act_read, act_create, act_update, act_delete, id_pegawai_pembuatan, tanggal_pembuatan) VALUES (XID_Pegawai, '13', 0, 0, 0, 0, XID_Pegawai_Save, XTanggal_Pembuatan);
	INSERT INTO useraccess_pusat (id_pegawai, id_menu, act_read, act_create, act_update, act_delete, id_pegawai_pembuatan, tanggal_pembuatan) VALUES (XID_Pegawai, '14', 0, 0, 0, 0, XID_Pegawai_Save, XTanggal_Pembuatan);
	INSERT INTO useraccess_pusat (id_pegawai, id_menu, act_read, act_create, act_update, act_delete, id_pegawai_pembuatan, tanggal_pembuatan) VALUES (XID_Pegawai, '15', 0, 0, 0, 0, XID_Pegawai_Save, XTanggal_Pembuatan);
	
	#Master Barang
	INSERT INTO useraccess_pusat (id_pegawai, id_menu, act_read, act_create, act_update, act_delete, id_pegawai_pembuatan, tanggal_pembuatan) VALUES (XID_Pegawai, '16', 0, 0, 0, 0, XID_Pegawai_Save, XTanggal_Pembuatan);
	INSERT INTO useraccess_pusat (id_pegawai, id_menu, act_read, act_create, act_update, act_delete, id_pegawai_pembuatan, tanggal_pembuatan) VALUES (XID_Pegawai, '17', 0, 0, 0, 0, XID_Pegawai_Save, XTanggal_Pembuatan);
	INSERT INTO useraccess_pusat (id_pegawai, id_menu, act_read, act_create, act_update, act_delete, id_pegawai_pembuatan, tanggal_pembuatan) VALUES (XID_Pegawai, '18', 0, 0, 0, 0, XID_Pegawai_Save, XTanggal_Pembuatan);
	INSERT INTO useraccess_pusat (id_pegawai, id_menu, act_read, act_create, act_update, act_delete, id_pegawai_pembuatan, tanggal_pembuatan) VALUES (XID_Pegawai, '19', 0, 0, 0, 0, XID_Pegawai_Save, XTanggal_Pembuatan);
	INSERT INTO useraccess_pusat (id_pegawai, id_menu, act_read, act_create, act_update, act_delete, id_pegawai_pembuatan, tanggal_pembuatan) VALUES (XID_Pegawai, '20', 0, 0, 0, 0, XID_Pegawai_Save, XTanggal_Pembuatan);
	
	#Transaksi Data
	INSERT INTO useraccess_pusat (id_pegawai, id_menu, act_read, act_create, act_update, act_delete, id_pegawai_pembuatan, tanggal_pembuatan) VALUES (XID_Pegawai, '21', 0, 0, 0, 0, XID_Pegawai_Save, XTanggal_Pembuatan);
	INSERT INTO useraccess_pusat (id_pegawai, id_menu, act_read, act_create, act_update, act_delete, id_pegawai_pembuatan, tanggal_pembuatan) VALUES (XID_Pegawai, '22', 0, 0, 0, 0, XID_Pegawai_Save, XTanggal_Pembuatan);
	INSERT INTO useraccess_pusat (id_pegawai, id_menu, act_read, act_create, act_update, act_delete, id_pegawai_pembuatan, tanggal_pembuatan) VALUES (XID_Pegawai, '23', 0, 0, 0, 0, XID_Pegawai_Save, XTanggal_Pembuatan);
	INSERT INTO useraccess_pusat (id_pegawai, id_menu, act_read, act_create, act_update, act_delete, id_pegawai_pembuatan, tanggal_pembuatan) VALUES (XID_Pegawai, '24', 0, 0, 0, 0, XID_Pegawai_Save, XTanggal_Pembuatan);
	INSERT INTO useraccess_pusat (id_pegawai, id_menu, act_read, act_create, act_update, act_delete, id_pegawai_pembuatan, tanggal_pembuatan) VALUES (XID_Pegawai, '25', 0, 0, 0, 0, XID_Pegawai_Save, XTanggal_Pembuatan);
	INSERT INTO useraccess_pusat (id_pegawai, id_menu, act_read, act_create, act_update, act_delete, id_pegawai_pembuatan, tanggal_pembuatan) VALUES (XID_Pegawai, '26', 0, 0, 0, 0, XID_Pegawai_Save, XTanggal_Pembuatan);
	INSERT INTO useraccess_pusat (id_pegawai, id_menu, act_read, act_create, act_update, act_delete, id_pegawai_pembuatan, tanggal_pembuatan) VALUES (XID_Pegawai, '27', 0, 0, 0, 0, XID_Pegawai_Save, XTanggal_Pembuatan);
	INSERT INTO useraccess_pusat (id_pegawai, id_menu, act_read, act_create, act_update, act_delete, id_pegawai_pembuatan, tanggal_pembuatan) VALUES (XID_Pegawai, '28', 0, 0, 0, 0, XID_Pegawai_Save, XTanggal_Pembuatan);
	INSERT INTO useraccess_pusat (id_pegawai, id_menu, act_read, act_create, act_update, act_delete, id_pegawai_pembuatan, tanggal_pembuatan) VALUES (XID_Pegawai, '29', 0, 0, 0, 0, XID_Pegawai_Save, XTanggal_Pembuatan);
	INSERT INTO useraccess_pusat (id_pegawai, id_menu, act_read, act_create, act_update, act_delete, id_pegawai_pembuatan, tanggal_pembuatan) VALUES (XID_Pegawai, '30', 0, 0, 0, 0, XID_Pegawai_Save, XTanggal_Pembuatan);
	INSERT INTO useraccess_pusat (id_pegawai, id_menu, act_read, act_create, act_update, act_delete, id_pegawai_pembuatan, tanggal_pembuatan) VALUES (XID_Pegawai, '31', 0, 0, 0, 0, XID_Pegawai_Save, XTanggal_Pembuatan);
	
	#Laporan Data
	INSERT INTO useraccess_pusat (id_pegawai, id_menu, act_read, act_create, act_update, act_delete, id_pegawai_pembuatan, tanggal_pembuatan) VALUES (XID_Pegawai, '32', 0, 0, 0, 0, XID_Pegawai_Save, XTanggal_Pembuatan);
	INSERT INTO useraccess_pusat (id_pegawai, id_menu, act_read, act_create, act_update, act_delete, id_pegawai_pembuatan, tanggal_pembuatan) VALUES (XID_Pegawai, '33', 0, 0, 0, 0, XID_Pegawai_Save, XTanggal_Pembuatan);
	INSERT INTO useraccess_pusat (id_pegawai, id_menu, act_read, act_create, act_update, act_delete, id_pegawai_pembuatan, tanggal_pembuatan) VALUES (XID_Pegawai, '34', 0, 0, 0, 0, XID_Pegawai_Save, XTanggal_Pembuatan);
	INSERT INTO useraccess_pusat (id_pegawai, id_menu, act_read, act_create, act_update, act_delete, id_pegawai_pembuatan, tanggal_pembuatan) VALUES (XID_Pegawai, '35', 0, 0, 0, 0, XID_Pegawai_Save, XTanggal_Pembuatan);
	INSERT INTO useraccess_pusat (id_pegawai, id_menu, act_read, act_create, act_update, act_delete, id_pegawai_pembuatan, tanggal_pembuatan) VALUES (XID_Pegawai, '36', 0, 0, 0, 0, XID_Pegawai_Save, XTanggal_Pembuatan);
	INSERT INTO useraccess_pusat (id_pegawai, id_menu, act_read, act_create, act_update, act_delete, id_pegawai_pembuatan, tanggal_pembuatan) VALUES (XID_Pegawai, '37', 0, 0, 0, 0, XID_Pegawai_Save, XTanggal_Pembuatan);
	INSERT INTO useraccess_pusat (id_pegawai, id_menu, act_read, act_create, act_update, act_delete, id_pegawai_pembuatan, tanggal_pembuatan) VALUES (XID_Pegawai, '38', 0, 0, 0, 0, XID_Pegawai_Save, XTanggal_Pembuatan);
	INSERT INTO useraccess_pusat (id_pegawai, id_menu, act_read, act_create, act_update, act_delete, id_pegawai_pembuatan, tanggal_pembuatan) VALUES (XID_Pegawai, '39', 0, 0, 0, 0, XID_Pegawai_Save, XTanggal_Pembuatan);
	INSERT INTO useraccess_pusat (id_pegawai, id_menu, act_read, act_create, act_update, act_delete, id_pegawai_pembuatan, tanggal_pembuatan) VALUES (XID_Pegawai, '40', 0, 0, 0, 0, XID_Pegawai_Save, XTanggal_Pembuatan);
	INSERT INTO useraccess_pusat (id_pegawai, id_menu, act_read, act_create, act_update, act_delete, id_pegawai_pembuatan, tanggal_pembuatan) VALUES (XID_Pegawai, '41', 0, 0, 0, 0, XID_Pegawai_Save, XTanggal_Pembuatan);
	INSERT INTO useraccess_pusat (id_pegawai, id_menu, act_read, act_create, act_update, act_delete, id_pegawai_pembuatan, tanggal_pembuatan) VALUES (XID_Pegawai, '42', 0, 0, 0, 0, XID_Pegawai_Save, XTanggal_Pembuatan);
	INSERT INTO useraccess_pusat (id_pegawai, id_menu, act_read, act_create, act_update, act_delete, id_pegawai_pembuatan, tanggal_pembuatan) VALUES (XID_Pegawai, '43', 0, 0, 0, 0, XID_Pegawai_Save, XTanggal_Pembuatan);
	INSERT INTO useraccess_pusat (id_pegawai, id_menu, act_read, act_create, act_update, act_delete, id_pegawai_pembuatan, tanggal_pembuatan) VALUES (XID_Pegawai, '44', 0, 0, 0, 0, XID_Pegawai_Save, XTanggal_Pembuatan);
	INSERT INTO useraccess_pusat (id_pegawai, id_menu, act_read, act_create, act_update, act_delete, id_pegawai_pembuatan, tanggal_pembuatan) VALUES (XID_Pegawai, '45', 0, 0, 0, 0, XID_Pegawai_Save, XTanggal_Pembuatan);
	INSERT INTO useraccess_pusat (id_pegawai, id_menu, act_read, act_create, act_update, act_delete, id_pegawai_pembuatan, tanggal_pembuatan) VALUES (XID_Pegawai, '46', 0, 0, 0, 0, XID_Pegawai_Save, XTanggal_Pembuatan);
	END$$

DELIMITER ;
<?php
	if($access_create == '1'){
		$readonly	= '';
		$disabled	= '';
	}else{		
		$readonly	= 'readonly';
		$disabled	= 'disabled';
	}
?>

<div class="content">
	<div class="container">
	<br>		
		<div class="row">
        	<div class="col-sm-12">
	        	<div class="row">
	        		<!-- Awal total harga -->
					<div class='col-sm-4'>
						<div class="panel panel-color panel-danger">
							<!-- Default panel contents -->
							<div class="panel-heading">
								<h3 class="panel-title">Referensi Penukaran Poin</h3>
							</div>

							<table class='table table-condensed table-striped table-hover' id='Total'>
								<tbody>
									<!-- Awal scan barang dan pilih Barang -->
									<tr class="text-dark">
		                    			<td colspan="2">
                                        	<div id="induk_pencarian_barang" class="input-group col-md-12 table-responsive">
			                                    <input id='pencarian_kode' name="pencarian_kode" placeholder="SKU / Nama Barang" class="form-control input-md text-dark" type="text" <?php echo $disabled ?>>
			                                    <span id="id_barang_pusat" name="id_barang_pusat" style="display: none;"></span>
			                                    
			                                    <span class="input-group-btn">
			                                        <button id="tombol_scan" name="tombol_scan" type="button" class="btn waves-effect waves-light btn-md btn-primary" <?php echo $disabled ?>>
			                                        <i id="icon_barang" class="fa fa-search"></i></button>
			                                    </span>
			                                </div>
			                                <div id='hasil_pencarian' name="hasil_pencarian" class="alert"></div>
		                    			</td>
		                    		</tr>
		                    		<!-- Akhir scan barang dan pilih Barang -->

									<tr>
										<td colspan="2">
											<small>Nama Barang</small> <br/>
											<b><span class="text-dark" id="nama_barang" name="nama_barang">Belum ada barang yg dipilih</span></b>
										</td>
									</tr>

									<tr>
										<td><small>Jumlah Poin</td>
										<td class="text-right">
											<input type='number' class='form-control input-sm' id='jumlah_poin' name='jumlah_poin' value="0" disabled>
										</td>
									</tr>

									<tr>
										<td><small>Qty</small></td>
										<td class="text-right">
											<input type='number' class='form-control input-sm' id='qty' name='qty' value="0" disabled>
										</td>
									</tr>												

									<tr>
										<td colspan="2">							
											<textarea name='catatan' id='catatan' class='form-control input-sm' rows='2' placeholder="Catatan Poin (Jika Ada)" style='resize: vertical; width:100%;' disabled></textarea>
										</td>
									</tr>

									<tr>
										<td>
											<button id='Simpan' type='button' class='btn btn-primary' title="Simpan Transaksi">
												<i class='fa fa-paper-plane-o'></i>
											</button>							
										</td>
										<td></td>
									</tr>

									<tr>
										<td colspan="2">
											<p><i class='fa fa-keyboard-o fa-fw'></i> <b><small>Shortcut Keyboard : </small></b></p>
											<div class='row'>
												<div class='col-sm-6'><small>F7 = Simpan SO</small></div>
												<div class='col-sm-6'><small>F8 = Referesh Data SO</small></div>
												<div class='col-sm-6'><small>F9 = Referesh Data Riwayat SO</small></div>
												<!-- <div class='col-sm-6'><small>F10 = </small></div> -->
											</div> 
										</td>
									</tr>	
								</tbody>
							</table>
						</div>
					</div>
					<!-- Akhir total harga --> 
					
	        		<!-- Awal daftar transaksi -->
		            <div class="col-sm-8">
			        	<!-- Awal daftar transaksi dan catatan transaksi --> 
		            	<div class="card-box table-responsive">
							<table id='TableTransaksi' class="table table-condensed table-striped table-hover dt-responsive nowrap" cellspacing="0" width="100%">
								<thead class="input-sm">
									<tr class="text-dark">
										<th>#</th>
										<th>Tombol</th>
										<th>SKU</th>
										<th>Nama Barang</th>
										<th>Jumlah Poin</th>
										<th>Qty</th>
										<th>Catatan</th>
										<th>Pembuatan</th>
										<th>Pembaharuan</th>
									</tr>
								</thead>

								<tbody class="input-sm text-dark"></tbody>
							</table>
						</div>
		            </div>
		            <!-- Akhir daftar transaksi -->	
				</div>
            </div>
        </div>
	</div>
</div>

<div class="modal" id="ModalGue" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class='fa fa-times-circle'></i></button>
				<h4 class="modal-title" id="ModalHeader"></h4>
			</div>
			<div class="modal-body" id="ModalContent"></div>
			<div class="modal-footer" id="ModalFooter"></div>
		</div>
	</div>
</div>

<div class="modal" id="ModalPesan" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class='fa fa-times-circle'></i></button>
				<h4 class="modal-title" id="ModalHeaderPesan"></h4>
			</div>
			<div class="modal-body" id="ModalContentPesan"></div>
			<div class="modal-footer" id="ModalFooterPesan"></div>
		</div>
	</div>
</div>

<script>
	$('#ModalGue').on('hide.bs.modal', function () {
	   setTimeout(function(){ 
	        $('#ModalHeader, #ModalContent, #ModalFooter').html('');
	   }, 500);
	});

	$('#ModalPesan').on('hide.bs.modal', function () {
	   setTimeout(function(){ 
	        $('#ModalHeaderPesan, #ModalContentPesan, #ModalFooterPesan').html('');
	   }, 500);
	});
</script>

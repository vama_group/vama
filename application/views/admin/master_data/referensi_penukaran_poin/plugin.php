<!-- Awal CSS -->
<link href="assets/plugin/zircos/plugins/datatables/jquery.dataTables.min.css" rel="stylesheet" type="text/css"/>
<link href="assets/plugin/zircos/plugins/datatables/responsive.bootstrap.min.css" rel="stylesheet" type="text/css"/>
<link href="assets/plugin/zircos/plugins/datatables/dataTables.bootstrap.min.css" rel="stylesheet" type="text/css"/>
<link href="assets/plugin/zircos/material-design/assets/css/tambahan.css" rel="stylesheet" type="text/css"/>
<!-- Akhir CSS -->

<!-- Awal JS -->
<script src="assets/plugin/zircos/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="assets/plugin/zircos/plugins/datatables/dataTables.bootstrap.js"></script>
<script src="assets/plugin/zircos/material-design/assets/js/detect.js"></script>
<script src="assets/plugin/zircos/material-design/assets/js/jquery.slimscroll.js"></script>
<script src="assets/plugin/zircos/plugins/datatables/dataTables.responsive.min.js"></script>
<script src="assets/plugin/zircos/plugins/datatables/responsive.bootstrap.min.js"></script>
<script src="assets/plugin/zircos/material-design/assets/pages/jquery.datatables.init.js"></script>
<!-- Akhir JS -->

<!-- Awal Sweet-Alert  -->
<link href="assets/plugin/zircos/plugins/bootstrap-sweetalert/sweet-alert.css" rel="stylesheet" type="text/css">
<script src="assets/plugin/zircos/plugins/bootstrap-sweetalert/sweet-alert.min.js"></script>
<!-- Akhir Sweet-Alert  -->

<script>
    $('#navbar').removeClass('navbar-default');
    $('#navbar').addClass('navbar-red');
    $('#JudulHalaman').html('Referensi Penukaran Poin - VAMA');

    var table_referensi = $('#TableTransaksi').DataTable();

    $(document).ready(function(){
        $("#pencarian_kode").focus();
        $("#wrapper").removeClass('forced');
        $("#wrapper").addClass('forced enlarged');
 
        TampilkanDataReferensi();
    });

    function TampilkanDataReferensi(){
        table_referensi.clear();table_referensi.destroy();
        table_referensi=$('#TableTransaksi').DataTable({
            ordering: true,
            bFilter:true,
            processing:true,
            serverSide:true,
            order:[],
            lengthMenu: [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
            pagingType: "full",

            oLanguage:{
                sLoadingRecords:'Silakan Tunggu',
                sProcessing:"<img src='assets/loader/cubeloader2.2.gif' width='50' height='50'> Harap tunggu",
                sZeroRecords:"Belum ada data untuk ditampilkan",
                sSearch : "Pencarian data"
            },
            ajax:{
                url: "<?php echo site_url('referensi_penukaran_poin/ajax_list')?>",
                type: "POST"
            },
            columnDefs: [{ 
                            "targets": [ -1 ],
                            "orderable": false,
                        },],
        });
    }

    var delay = (function (){
        var timer = 0;
        return function (callback, ms) {
            clearTimeout(timer);
            timer = setTimeout(callback, ms);
        };
    })();

    function reload_table_referensi()
    {
        table_referensi.ajax.reload();
    }

    $(document).on('click', '#daftar-autocomplete li', function(){
        var id_barang       = $(this).find('span#id_barang').html();
        var SKU             = $(this).find('span#skunya').html();
        var NamaBarang      = $(this).find('span#barangnya').html();
        var TipeCustomer    = $('span#data_tipe_customer_pusat').html();
        var JenisHarga      = 'ECERAN';
        var Harganya        = $(this).find('span#eceran').html();

        $('div#hasil_pencarian').hide();
        $('#id_barang_pusat').html(id_barang);
        $('#pencarian_kode').val(SKU);
        $('#nama_barang').html(NamaBarang);
        
        $('#jumlah_poin').removeAttr('readonly').val('');
        $('#jumlah_poin').removeAttr('disabled').val();
        $('#qty').removeAttr('readonly').val('');
        $('#qty').removeAttr('disabled').val();
        $('#catatan').removeAttr('disabled').val('');
        $('#Simpan').removeAttr('disabled').val();
        $('#jumlah_poin').focus();
    });

    $(document).on('click', '#tombol_scan', function(e){
        AutoCompleteGue($('#pencarian_kode').val());
    });

    $(document).on('keyup', '#pencarian_kode', function(e){
        if($(this).val() !== '')
        {
            var charCode = e.which || e.keyCode;
            if(charCode == 40){
                if($('div#hasil_pencarian li.autocomplete_active').length > 0){
                    var Selanjutnya = $('div#hasil_pencarian li.autocomplete_active').next();
                    $('div#hasil_pencarian li.autocomplete_active').removeClass('autocomplete_active');

                    Selanjutnya.addClass('autocomplete_active');
                }else{
                    $('div#hasil_pencarian li:first').addClass('autocomplete_active');
                }
            }else if(charCode == 38){
                if($('div#hasil_pencarian li.autocomplete_active').length > 0){
                    var Sebelumnya = $('div#hasil_pencarian li.autocomplete_active').prev();
                    $('div#hasil_pencarian li.autocomplete_active').removeClass('autocomplete_active');
                
                    Sebelumnya.addClass('autocomplete_active');
                }else{
                    $('div#hasil_pencarian li:first').addClass('autocomplete_active');
                }
            }else if(charCode == 13){
                // Awal fungsi enter dan ambil data barang dengan ajax
                var SKU             = $(this).val();
                $('#pencarian_kode').val(SKU);
                $('div#hasil_pencarian').hide();

                $.ajax({
                    url: "<?php echo site_url('barang_pusat/ajax_cari_barang_untuk_transaksi'); ?>",
                    type: "POST",
                    cache: false,
                    data: 'sku=' + SKU,
                    dataType:'json',
                    success: function(json){
                        if(json.status == 1){
                            $('#nama_barang').html(json.data['nama_barang']);
                            $('#harga_satuan').html('Rp. ' + to_rupiah(json.data['harga_eceran']));

                            if($('#nama_barang').val() == ''){
                                $('#qty').removeAttr('readonly').val('');
                                $('#qty').removeAttr('disabled').val('');
                                $('#catatan').removeAttr('disabled').val('');
                                $('#Simpan').removeAttr('disabled').val();
                                $('#qty').focus();
                            }
                        }else if(json.status == 0){
                            $('#qty').val('0');

                            $('.modal-dialog').removeClass('modal-lg');
                            $('.modal-dialog').addClass('modal-sm');
                            $('#ModalHeader').html('Oops !');
                            $('#ModalContent').html(json.pesan);
                            $('#ModalFooter').html("<button id='info_stok' type='button' class='btn btn-primary'>Ok, Saya Mengerti</button>");
                            $('#ModalGue').modal('show');
                        }
                    }
                });
            }else{
                AutoCompleteGue($(this).val());
            }
        }else{
            // Bersihkan data pemilihan barang
            $('#id_barang_pusat').html('');
            $('#nama_barang').html('Belum ada barang yang dipilih');
            $('#harga_satuan').html('Rp. 0');

            $('#qty').prop('readonly', true).val('');
            $('#qty').prop('disabled', true).val('');
            $('#catatan').prop('disabled', true).val('');

            $('div#hasil_pencarian').hide();
        }
    });

    $(document).on('keydown', '#jumlah_poin', function(e){
        var charCode = e.which || e.keyCode;
        setTimeout(function(){
            if(charCode == 9){
                $('#qty').focus();
                return false;   
            }
            
            if(charCode == 13){
                if($('#jumlah_poin').val() == '0' || $('#jumlah_poin').val() == ''){
                    swal({
                        title: "Oops !", 
                        text: "Harap masukan jumlah poin terlebih dahulu.", 
                        type: "error", 
                        confirmButtonClass: 'btn-danger btn-md waves-effect waves-light',
                        confirmButtonText: "Ok"
                    },function (isConfirm){
                        $('#jumlah_poin').focus();
                        return false;
                    });
                    return;
                }else{
                    $('#qty').focus();
                } 
            }
        }, 100);
    });

    $(document).on('keydown', '#qty', function(e){
        var charCode = e.which || e.keyCode;
        setTimeout(function(){
            if(charCode == 9){
                $('#catatan').focus();
                return false;   
            }
            
            if(charCode == 13){
                if($('#qty').val() == '0' || $('#qty').val() == ''){
                    swal({
                        title: "Oops !", 
                        text: "Harap masukan qty terlebih dahulu.", 
                        type: "error", 
                        confirmButtonClass: 'btn-danger btn-md waves-effect waves-light',
                        confirmButtonText: "Ok"
                    },function (isConfirm){
                        setTimeout(function(){ 
                            $('#qty').focus();
                        }, 100);
                    });
                    return;
                }else if($('#qty').val() !== ''){
                    if($('#id_barang_pusat').html() !== ''){
                        $('.modal-dialog').removeClass('modal-lg');
                        $('.modal-dialog').addClass('modal-sm');
                        $('#ModalHeader').html('Konfirmasi');
                        $('#ModalContent').html(
                                                "Apakah anda yakin ingin menyimpan referensi penukaran poin ini ? </br></br>" +
                                                "SKU : </br>" + 
                                                "<b class='text-dark'>" + $('#pencarian_kode').val() + "</b></br></br>" +

                                                "Nama Barang : </br>" + 
                                                "<b class='text-dark'>" + $('#nama_barang').html() + "</b></br></br>" +

                                                "Jumlah Poin : </br>" + 
                                                "<b class='text-dark'>" + $('#jumlah_poin').val() + "</b></br></br>" +

                                                "Qty : </br>" + 
                                                "<b class='text-dark'>" + $('#qty').val() + "</b>"                              
                                                );
                        $('#ModalFooter').html("<button type='button' class='btn btn-primary' id='SimpanData'>Ya, saya yakin</button><button type='button' class='btn btn-default' data-dismiss='modal'>Batal</button>");
                        $('#ModalGue').modal('show');

                        setTimeout(function(){ 
                            $('button#SimpanData').focus();
                        }, 500);
                    
                    }
                }else{
                    swal({
                        title: "Oops !", 
                        text: "Harap masukan qty terlebih dahulu.", 
                        type: "error", 
                        confirmButtonClass: 'btn-danger btn-md waves-effect waves-light',
                        confirmButtonText: "Ok"
                    },function (isConfirm){
                        setTimeout(function(){ 
                            $('#qty').focus();
                        }, 100);
                    });
                    return;
                } 
            }
        }, 100);
    });

    $(document).on('click', 'button#info_stok', function(){
        $('#ModalGue').modal('hide');
        $('#qty').focus();
    });

    $(document).on('keydown', 'body', function(e){
        var charCode = ( e.which ) ? e.which : event.keyCode;

        if(charCode == 118) //F7
        {
            if($('#jumlah_poin').val() == '0' || $('#jumlah_poin').val() == ''){
                swal({
                    title: "Oops !", 
                    text: "Harap masukan jumlah poin terlebih dahulu.", 
                    type: "error", 
                    confirmButtonClass: 'btn-danger btn-md waves-effect waves-light',
                    confirmButtonText: "Ok"
                },function (isConfirm){
                    setTimeout(function(){ 
                        $('#jumlah_poin').focus();
                    }, 500);
                    return false;
                });
                return;
            }

            if($('#qty').val() == '0' || $('#qty').val() == ''){
                swal({
                    title: "Oops !", 
                    text: "Harap masukan qty terlebih dahulu.", 
                    type: "error", 
                    confirmButtonClass: 'btn-danger btn-md waves-effect waves-light',
                    confirmButtonText: "Ok, saya mengerti"
                },function (isConfirm){
                    setTimeout(function(){ 
                        $('#qty').focus();
                    }, 500);
                    return false;
                });
                return;

            }else if($('#qty').val() !== ''){
                $('.modal-dialog').removeClass('modal-lg');
                $('.modal-dialog').addClass('modal-sm');
                $('#ModalHeader').html('Konfirmasi');
                $('#ModalContent').html(
                                        "Apakah anda yakin ingin menyimpan referensi penukaran poin ini ? </br></br>" +
                                        "SKU : </br>" + 
                                        "<b class='text-dark'>" + $('#pencarian_kode').val() + "</b></br></br>" +

                                        "Nama Barang : </br>" + 
                                        "<b class='text-dark'>" + $('#nama_barang').html() + "</b></br></br>" +

                                        "Jumlah Poin : </br>" + 
                                        "<b class='text-dark'>" + $('#jumlah_poin').val() + "</b></br></br>" +                              

                                        "QTY : </br>" + 
                                        "<b class='text-dark'>" + $('#qty').val() + "</b>"                              
                                        );
                $('#ModalFooter').html("<button type='button' class='btn btn-primary' id='SimpanData2'>Ya, saya yakin</button><button type='button' class='btn btn-default' data-dismiss='modal'>Batal</button>");
                $('#ModalGue').modal('show');

                setTimeout(function(){ 
                    $('button#SimpanData2').focus();
                }, 500);

                return false;
            }
        }

        if(charCode == 119) //F8
        {
            
        }

        if(charCode == 120) //F9
        {
            
        }

        if(charCode == 121) //F10
        {
            
        }
    });

    $(document).on('click', 'body', function(){
        $('div#hasil_pencarian').hide();
        $('div#hasil_pencarian_harga').hide();
    });

    $(document).on('click', 'button#Simpan', function(){
        if($('#id_barang_pusat').html() !== ''){
            if($('#jumlah_poin').val() == '0' || $('#jumlah_poin').val() == ''){
                swal({
                    title: "Oops !", 
                    text: "Harap masukan jumlah poin terlebih dahulu.", 
                    type: "error", 
                    confirmButtonClass: 'btn-danger btn-md waves-effect waves-light',
                    confirmButtonText: "Ok"
                },function (isConfirm){
                    setTimeout(function(){ 
                        $('#jumlah_poin').focus();
                    }, 500);
                    return false;
                });
                return;
            }
            
            if($('#qty').val() == '0' || $('#qty').val() == ''){
                swal({
                    title: "Oops !", 
                    text: "Harap masukan qty terlebih dahulu.", 
                    type: "error", 
                    confirmButtonClass: 'btn-danger btn-md waves-effect waves-light',
                    confirmButtonText: "Ok, saya mengerti"
                },function (isConfirm){
                    setTimeout(function(){ 
                        $('#qty').focus();
                    }, 500);
                    return false;
                });
                return;

            }else if($('#qty').val() !== ''){
                $('.modal-dialog').removeClass('modal-lg');
                $('.modal-dialog').addClass('modal-sm');
                $('#ModalHeader').html('Konfirmasi');
                $('#ModalContent').html(
                                        "Apakah anda yakin ingin menyimpan referensi penukaran poin ini ? </br></br>" +
                                        "SKU : </br>" + 
                                        "<b class='text-dark'>" + $('#pencarian_kode').val() + "</b></br></br>" +

                                        "Nama Barang : </br>" + 
                                        "<b class='text-dark'>" + $('#nama_barang').html() + "</b></br></br>" +

                                        "Jumlah Poin : </br>" + 
                                        "<b class='text-dark'>" + $('#jumlah_poin').val() + "</b></br></br>" +                              

                                        "QTY : </br>" + 
                                        "<b class='text-dark'>" + $('#qty').val() + "</b>"                              
                                        );
                $('#ModalFooter').html("<button type='button' class='btn btn-primary' id='SimpanData2'>Ya, saya yakin</button><button type='button' class='btn btn-default' data-dismiss='modal'>Batal</button>");
                $('#ModalGue').modal('show');

                setTimeout(function(){ 
                    $('button#SimpanData2').focus();
                }, 500);

                return false;
            }
        }else{
            swal({
                title: "Oops !", 
                text: "Belum ada barang yang anda pilih, harap pilih barang terlebih dahulu untuk menyimpan referensi penukaran poin.", 
                type: "error", 
                confirmButtonClass: 'btn-danger btn-md waves-effect waves-light',
                confirmButtonText: "Ok, saya mengerti"
            },function (isConfirm){
                setTimeout(function(){ 
                    $('#pencarian_kode').focus();
                }, 500);
                return false;
            });
            return;
        }
    });

    $(document).on('click', 'button#SimpanData', function(){
        $('#ModalGue').modal('hide');
        simpan_referensi();
    });

    $(document).on('click', 'button#SimpanData2', function(){
        $('#ModalGue').modal('hide');
        simpan_referensi();
    });

    function simpan_referensi()
    {
        // var Indexnya = $(this).parent().parent().index();
        var FormData = "id_barang_pusat="+$('#id_barang_pusat').html();
            FormData += "&jumlah_poin="+$('#jumlah_poin').val();
            FormData += "&qty="+$('#qty').val();
            FormData += "&catatan="+encodeURI($('#catatan').val());
            FormData += "&tanggal="+encodeURI($('#tanggal').val());

        $.ajax({
            url: "<?php echo site_url('referensi_penukaran_poin/ajax_simpan'); ?>",
            type: "POST",
            cache: false,
            data: FormData,
            dataType:'json',
            success: function(data){
                if(data.status == 1){   
                    // Bersihkan data pemilihan barang
                    $('#id_barang_pusat').html('');
                    $('#pencarian_kode').val('');
                    $('#nama_barang').html('Belum ada barang yang dipilih');
                    $('#jumlah_poin').val('0');
                    $('#qty').prop('disabled', true).val('0');
                    $('#catatan').prop('disabled', true).val('');
                    $('#Simpan').prop('disabled', true);

                    reload_table_referensi();
                    $('#pencarian_kode').focus();

                }else if(data.status == 0){
                    swal({
                        title: "Oops !", 
                        text: data.pesan, 
                        type: "error", 
                        confirmButtonClass: 'btn-danger btn-md waves-effect waves-light',
                        confirmButtonText: "Ok"
                    },function (isConfirm){
                        setTimeout(function(){ 
                            // $('#pencarian_kode').focus();
                            window.location.href="referensi_penukaran_poin";
                        }, 100);
                        return false;
                    });
                    return;
                }
            }
        });
    }

    function edit_referensi(id_referensi)
    {
        $.ajax({
            url : "<?php echo site_url('referensi_penukaran_poin/ajax_edit'); ?>",
            type: "POST",
            cache: false,
            data: 'id_referensi=' + id_referensi,
            dataType: "JSON",
            success: function(data){
                var Indexnya = $(this).parent().parent().parent().parent().index();
                $('#id_barang_pusat').html(data.id_barang_pusat);
                $('#pencarian_kode').val(data.sku);
                $('#nama_barang').html(data.nama_barang);
                $('#jumlah_poin').val(to_rupiah(data.jumlah_poin));
                $('#qty').val(to_rupiah(data.qty));
                $('#catatan').val(data.keterangan_lain);
                $('#qty').val(data.qty).focus();

                $('#jumlah_poin').removeAttr('disabled').val();
                $('#qty').removeAttr('disabled').val();
                $('#Simpan').removeAttr('disabled').val();
                $('#catatan').removeAttr('disabled').val();
            },
            error: function (jqXHR, textStatus, errorThrown){
                swal({
                    title: "Oops !", 
                    text: "Gagal mengambil data.", 
                    type: "error", 
                    confirmButtonClass: 'btn-danger btn-md waves-effect waves-light',
                    confirmButtonText: "Ok"
                },function (isConfirm){
                    window.location.href="referensi_penukaran_poin";
                    return false;
                });
                return;
                
            }
        });
    }

    function verifikasi_hapus(id_referensi)
    {
        $.ajax({
            url : "<?php echo site_url('referensi_penukaran_poin/ajax_verifikasi_hapus'); ?>",
            type: "POST",
            cache: false,
            data: 'id_referensi=' + id_referensi,
            dataType: "JSON",
            success: function(data){
                $('.modal-dialog').removeClass('modal-lg');
                $('.modal-dialog').addClass('modal-sm');
                $('#ModalHeader').html('Informasi Hapus');
                $('#ModalContent').html(data.pesan);
                $('#ModalFooter').html(data.footer);
                $('#ModalGue').modal('show');
            },
            error: function (jqXHR, textStatus, errorThrown){
                swal({
                    title: "Oops !", 
                    text: "Gagal mengambil data.", 
                    type: "error", 
                    confirmButtonClass: 'btn-danger btn-md waves-effect waves-light',
                    confirmButtonText: "Ok"
                },function (isConfirm){
                    window.location.href="referensi_penukaran_poin";
                    return false;
                });
            }
        });
    }

    function hapus_referensi(id_referensi)
    {
        var FormData = "tanggal=" + encodeURI($('#tanggal').val());
            FormData += "&id_referensi=" + id_referensi; 
            FormData += "&keterangan_batal=" + encodeURI($('#keterangan_batal').val());

        $.ajax({
            url : "<?php echo site_url('referensi_penukaran_poin/ajax_hapus'); ?>",
            type: "POST",
            data: FormData,
            dataType: "JSON",
            success: function(data){
                if(data.status == 1){
                    reload_table_referensi();

                }else if(data.status == 0){
                    $('.modal-dialog').removeClass('modal-lg');
                    $('.modal-dialog').addClass('modal-sm');
                    $('#ModalHeader').html('Informasi Referensi Penukaran Poin');
                    $('#ModalContent').html(data.pesan);
                    $('#ModalFooter').html("<button type='button' class='btn btn-primary' data-dismiss='modal' autofocus>Ok, Saya Mengerti</button>");
                    $('#ModalGue').modal('show');
                    reload_table_referensi();
                }
               
            }
        });
    }

    function AutoCompleteGue(KataKunci)
    {
        $('#induk_pencarian_barang').removeClass('has-warning');
        $('#induk_pencarian_barang').addClass('has-has-succes');

        $('#icon_barang').removeClass('fa-search');
        $('#icon_barang').addClass('fa-spin fa-refresh');

        $('#tombol_scan').removeClass('btn-warning');
        $('#tombol_scan').addClass('btn-primary');

        $('#qty').prop('readonly', true).val();
        $('div#hasil_pencarian').hide();

        delay(function () {
            $.ajax({
                url: "<?php echo site_url('referensi_penukaran_poin/ajax_kode'); ?>",
                type: "POST",
                cache: false,
                data: 'keyword=' + KataKunci,
                dataType:'json',
                success: function(json){
                    if(json.status == 1){
                        $('#icon_barang').removeClass('fa-spin fa-refresh');
                        $('#icon_barang').addClass('fa-search');

                        $('#id_barang_pusat').html('');
                        $('#nama_barang').html('Belum ada barang yang dipilih');
                        $('#harga_satuan').html('Rp. 0');
                        $('#qty').prop('disabled', true).val('');
                        $('#catatan').prop('disabled', true).val('');
                        $('#Simpan').prop('disabled', true).val();

                        $('div#hasil_pencarian').show('fast');
                        $('div#hasil_pencarian').html(json.datanya);

                    }else{
                        $('#icon_barang').removeClass('fa-spin fa-refresh');
                        $('#icon_barang').addClass('fa-search');

                        $('#induk_pencarian_barang').removeClass('has-succes');
                        $('#induk_pencarian_barang').addClass('has-warning');

                        $('#tombol_scan').removeClass('btn-primary');
                        $('#tombol_scan').addClass('btn-warning');

                    }               
                }
            });
        }, 500);
    }

    function to_rupiah(angka){
        var rev     = parseInt(angka, 10).toString().split('').reverse().join('');
        var rev2    = '';
        for(var i = 0; i < rev.length; i++){
            rev2  += rev[i];
            if((i + 1) % 3 === 0 && i !== (rev.length - 1)){
                rev2 += '.';
            }
        }
        return rev2.split('').reverse().join('');

        // Pakai Simbol Rp.
        // return 'Rp. ' + rev2.split('').reverse().join('');
    }

    function to_angka(rp)
    {
        return parseInt(rp.replace(/,.*|\D/g,''),10)
    }

    function check_int(evt) {
        var charCode = ( evt.which ) ? evt.which : event.keyCode;
        return ( charCode >= 48 && charCode <= 57 || charCode == 8 );
    }

</script>
<!-- Akhir Script CRUD -->

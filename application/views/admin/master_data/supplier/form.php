<!-- Bootstrap modal -->
<div class="modal fade" id="modal_form" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h5 class="modal-title">Supplier</h5>
            </div>
            
            <div class="modal-body form">
                <form action="#" id="form" class="form-group">
                    <input type="hidden" value="" name="id_supplier"/> 
                    <div class="form-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-horizontal">
                                    <label class="control-label col-md-3 small">NAMA SUPPLIER</label>
                                    <div class="col-md-9">
                                        <input id="nama_supplier" name="nama_supplier" placeholder="Nama Supplier" class="form-control" type="text" autofocus>
                                        <span class="help-block"></span>
                                    </div>
                                </div>

                                <div class="form-horizontal">
                                    <label class="control-label col-md-3 small">Asal & Tipe Bisinis</label>
                                    <div class="col-md-9">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <select id="asal_supplier" name="asal_supplier" class="form-control">
                                                    <option value="LOCAL">LOCAL</option>
                                                    <option value="IMPOR">IMPOR</option>
                                                </select>
                                                <span class="help-block"></span>
                                            </div>

                                            <div class="col-md-6">
                                                <select id="tipe_bisnis" name="tipe_bisnis" class="form-control">
                                                    <option value="PUTUS">PUTUS</option>
                                                    <option value="KONSINYASI">KONSINYASI</option>
                                                    <option value="TITIP_JUAL">TITIP JUAL</option>
                                                </select>
                                                <span class="help-block"></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-horizontal">
                                    <label class="control-label col-md-3 small">TIPE SUPPLIER</label>
                                    <div class="col-md-9">
                                        <select id="tipe_supplier" name="tipe_supplier" class="form-control">
                                            <option value="DISTRIBUTOR">DISTRIBUTOR</option>
                                            <option value="PRINSIPIL">PRINSIPIL</option>
                                            <option value="TRADER">TRADER</option>
                                        </select>
                                        <span class="help-block"></span>
                                    </div>
                                </div>                                

                                <div class="form-horizontal">
                                    <label class="control-label col-md-3 small">Alamat</label>
                                    <div class="col-md-9">
                                        <textarea id="alamat_supplier" name="alamat_supplier" placeholder="Alamat" class="form-control"></textarea>
                                        <span class="help-block"></span>                            
                                    </div>
                                </div>                                
                            </div>

                            <div class="col-md-6">
                                <div class="form-horizontal">
                                    <label class="control-label col-md-3 small">NO. SIUP</label>
                                    <div class="col-md-9">
                                        <input name="no_siup" placeholder="NO. SIUP" class="form-control" type="text">
                                        <span class="help-block"></span>
                                    </div>
                                </div>

                                <div class="form-horizontal">
                                    <label class="control-label col-md-3 small">Kontak & Jabatan</label>
                                    <div class="col-md-9">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <input name="kontak_pribadi" placeholder="Kontak" class="form-control" type="text">
                                                <span class="help-block"></span>
                                            </div>
                                            <div class="col-md-6">
                                                <input name="jabatan" placeholder="Jabatan" class="form-control" type="text">
                                                <span class="help-block"></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-horizontal">
                                    <label class="control-label col-md-3 small">Email</label>
                                    <div class="col-md-9">
                                        <input name="email" placeholder="email" class="form-control" type="email">
                                        <span class="help-block"></span>
                                    </div>
                                </div>

                                <div class="form-horizontal">
                                    <label class="control-label col-md-3 small">HP 1 & 2</label>
                                    <div class="col-md-9">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <input name="handphone1" placeholder="Handphone 1" class="form-control" type="number">
                                                <span class="help-block"></span>
                                            </div>
                                            <div class="col-md-6">
                                                <input name="handphone2" placeholder="Handphone 2" class="form-control" type="number">
                                                <span class="help-block"></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-horizontal">
                                    <label class="control-label col-md-3 small">Tlpn 1 & 2</label>
                                    <div class="col-md-9">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <input name="telephone1" placeholder="Telephone 1" class="form-control" type="number">
                                                <span class="help-block"></span>
                                            </div>
                                            <div class="col-md-6">
                                                <input name="telephone2" placeholder="Telephone 2" class="form-control" type="number">
                                                <span class="help-block"></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-horizontal">
                                    <label class="control-label col-md-3 small">FAX</label>
                                    <div class="col-md-9">
                                        <input name="fax" placeholder="Fax" class="form-control" type="number">
                                        <span class="help-block"></span>
                                    </div>
                                </div>
                            </div>
                        </div>                        
                    </div>
                </form>
            </div>

            <div class="modal-footer">
                <button type="button" id="btnSave" onclick="save()" class="btn btn-primary">Simpan</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- End Bootstrap modal -->
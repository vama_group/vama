<!-- Start content -->
<div class="content">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="page-title-box">
                    <h4 class="page-title text-primary"><i class='fa fa-building'></i><b>  SUPPLIER</b></h4>
                    <ol class="breadcrumb p-0 m-0">
                        <li>
                            <?php if($access_create == '1'){ ?>
                                <button class="btn btn-rounded btn-xs btn-success" onclick="add_supplier()">
                                    <i class="fa fa-plus"></i> 
                                    TAMBAH
                                </button>
                            <?php  } ?>
                            <button class="btn btn-rounded btn-xs btn-primary" onclick="reload_table()">
                                <i class="fa fa-refresh"></i> 
                                REFRESH
                            </button>
                        </li>                        
                    </ol>

                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
        
        <div class="row">
            <div class="col-sm-12">
                <div class="card-box table-responsive">
                    <table id="table" class="table table-condensed table-striped table-hover dt-responsive nowrap" cellspacing="0" width="100%">
                        <thead class="input-sm">
                            <tr>
                                <th>#</th>
                                <?php if($access_update == 1 or $access_delete == 1){ ?>
                                    <th>Tombol</th>
                                <?php } ?>
                                <th>Kode Supplier</th>
                                <th style="width: 350px">Nama Supplier</th>
                                <th>Tipe Bisnis</th>
                                <th>Tipe Supplier</th>
                                
                                <th>Pegawai Save</th>
                                <th>Tanggal save</th>
                                <th>Pegawai Edit</th>
                                <th>Tanggal Edit</th>
                            </tr>
                        </thead>

                        <tbody class="input-sm">
                        </tbody>
                    </table>                    
                </div>
            </div>
        </div>
    </div> <!-- container -->
</div> 
<!-- content -->

<div class="modal" id="ModalGue" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class='fa fa-times-circle'></i></button>
                <h4 class="modal-title" id="ModalHeader"></h4>
            </div>
            <div class="modal-body" id="ModalContent"></div>
            <div class="modal-footer" id="ModalFooter"></div>
        </div>
    </div>
</div>

<script>
$('#ModalGue').on('hide.bs.modal', function () {
   setTimeout(function(){ 
        $('#ModalHeader, #ModalContent, #ModalFooter').html('');
   }, 500);
});
</script>
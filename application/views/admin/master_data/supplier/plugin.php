<!-- Awal CSS -->
<link href="assets/plugin/zircos/plugins/datatables/jquery.dataTables.min.css" rel="stylesheet" type="text/css"/>
<link href="assets/plugin/zircos/plugins/datatables/responsive.bootstrap.min.css" rel="stylesheet" type="text/css"/>
<link href="assets/plugin/zircos/plugins/datatables/dataTables.bootstrap.min.css" rel="stylesheet" type="text/css"/>
<link href="assets/plugin/zircos/material-design/assets/css/tambahan.css" rel="stylesheet" type="text/css"/>

<!-- Awal JS -->
<script src="assets/plugin/zircos/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="assets/plugin/zircos/plugins/datatables/dataTables.bootstrap.js"></script>
<script src="assets/plugin/zircos/plugins/datatables/dataTables.responsive.min.js"></script>
<script src="assets/plugin/zircos/plugins/datatables/responsive.bootstrap.min.js"></script>
<script src="assets/plugin/zircos/material-design/assets/pages/jquery.datatables.init.js"></script>

<!-- Awal Sweet-Alert  -->
<link href="assets/plugin/zircos/plugins/bootstrap-sweetalert/sweet-alert.css" rel="stylesheet" type="text/css">
<script src="assets/plugin/zircos/plugins/bootstrap-sweetalert/sweet-alert.min.js"></script>
<!-- Akhir Sweet-Alert  -->

<script type="text/javascript">
    var save_method;
    var table;

    $(document).ready(function() {
        $('#JudulHalaman').html('Supplier - VAMA');

        table = $('#table').DataTable({ 
            processing: true,
            serverSide: true,
            order: [],

            ajax: {
                url: "<?php echo site_url('supplier/ajax_list')?>",
                type: "POST",
                data: {'id_periode_stok_pusat' : '1'}
            },

            columnDefs: [
                { 
                    targets: [ -1 ],
                    orderable: false,
                },
            ],

        });

        var _swal = window.swal;
        window.swal = function(){
            var previousWindowKeyDown = window.onkeydown;
            _swal.apply(this, Array.prototype.slice.call(arguments, 0));
            window.onkeydown = previousWindowKeyDown;
        };

        $("input").change(function(){
            $(this).parent().parent().removeClass('has-error');
            $(this).next().empty();
        });

        $("textarea").change(function(){
            $(this).parent().parent().removeClass('has-error');
            $(this).next().empty();
        });

        $("select").change(function(){
            $(this).parent().parent().removeClass('has-error');
            $(this).next().empty();
        });
    });

    function reload_table()
    {
        table.ajax.reload(null, false); 
    }

    function add_supplier()
    {
        save_method = 'add';
        $('#form')[0].reset();
        $('.form-horizontal').removeClass('has-error');
        $('.help-block').empty();
        $('#modal_form').modal('show');
        $('.modal-title').text('Tambah Supplier');
    }

    function edit_supplier(id_supplier)
    {
        save_method = 'update';
        $('#form')[0].reset();
        $('.form-horizontal').removeClass('has-error');
        $('.help-block').empty();

        $.ajax({
            url : "<?php echo site_url('supplier/ajax_edit'); ?>",
            type: "POST",
            cache: false,
            data: 'id_supplier=' + id_supplier,
            dataType: "JSON",
            success: function(data){
                $('[name="id_supplier"]').val(data.id_supplier);
                $('[name="no_siup"]').val(data.no_siup);
                $('[name="nama_supplier"]').val(data.nama_supplier);
                $('[name="asal_supplier"]').val(data.asal_supplier);
                $('[name="alamat_supplier"]').val(data.alamat_supplier);           
                $('[name="tipe_bisnis"]').val(data.tipe_bisnis);           
                $('[name="tipe_supplier"]').val(data.tipe_supplier);           
                $('[name="kontak_pribadi"]').val(data.kontak_pribadi);
                $('[name="jabatan"]').val(data.jabatan);           
                $('[name="email"]').val(data.email);
                $('[name="telephone1"]').val(data.telephone1);
                $('[name="telephone2"]').val(data.telephone2);
                $('[name="fax"]').val(data.fax);
                $('[name="handphone1"]').val(data.handphone1);
                $('[name="handphone2"]').val(data.handphone2);
                $('[name="password"]').val(data.password);
                $('[name="usergroup"]').val(data.id_usergroup);
                $('[name="status_blokir"]').val(data.status_blokir);
                $('#modal_form').modal('show');
                $('.modal-title').text('Edit Supplier : ' 
                                        + data.kode_supplier  
                                        + ' - '
                                        + data.nama_supplier);
            },
            error: function (jqXHR, textStatus, errorThrown){
                swal({
                    title: "Gagal!", 
                    text: "Data supplier gagal untuk ditampilkan.", 
                    type: "error", 
                    confirmButtonClass: 'btn-danger btn-md waves-effect waves-light',
                    confirmButtonText: "Ok"
                }, function(isConfirm){
                    window.location.href = "supplier";
                });
            }
        });
    }

    function save()
    {
        var url;
        var judul;
        var pesan;
        if(save_method == 'add'){
            judul   = "Yakin ingin menambahkan supplier ?";
            pesan   = "Nama : " + $('#nama_supplier').val() + ", " + 
                      "Tipe Supplier : " + $('#tipe_supplier').val();
            url     = "<?php echo site_url('supplier/ajax_add')?>";
        }else{
            judul   = "Yakin ingin memperbaharui supplier ?";
            pesan   = "Nama : " + $('#nama_supplier').val() + ", " + 
                      "Tipe Supplier : " + $('#tipe_supplier').val();
            url     = "<?php echo site_url('supplier/ajax_update')?>";
        }

        swal({
            title: judul,
            text: pesan,
            type: "info",
            showCancelButton: true,
            confirmButtonClass: 'btn-primary btn-md waves-effect waves-light',
            confirmButtonText: "Iya",
            cancelButtonText: "Batal",
            closeOnConfirm: false,
        }, function (isConfirm){
            if(isConfirm){
                $('#btnSave').text('menyimpan...');
                $('#btnSave').attr('disabled',true);

                $.ajax({
                    url : url,
                    type: "POST",
                    data: $('#form').serialize(),
                    dataType: "JSON",
                    success: function(data){
                        if(data.status){
                            $('#modal_form').modal('hide');
                            reload_table();

                            swal({
                                title: "Berhasil!", 
                                text: "Data supplier berhasil disimpan.", 
                                type: "success", 
                                confirmButtonText: "Ok"
                            });
                        }else{
                            for (var i = 0; i < data.inputerror.length; i++){
                                $('[name="'+data.inputerror[i]+'"]').parent().parent().addClass('has-error');
                                $('[name="'+data.inputerror[i]+'"]').next().text(data.error_string[i]);
                            }

                            swal({
                                title: "Gagal!", 
                                text: "Data supplier gagal disimpan.", 
                                type: "error", 
                                confirmButtonClass: 'btn-danger btn-md waves-effect waves-light',
                                confirmButtonText: "Ok"
                            });
                        }

                        $('#btnSave').text('Simpan');
                        $('#btnSave').attr('disabled',false);
                    },
                    error: function (jqXHR, textStatus, errorThrown){
                        swal({
                            title: "Gagal!", 
                            text: "Data supplier gagal disimpan.", 
                            type: "error", 
                            confirmButtonClass: 'btn-danger btn-md waves-effect waves-light',
                            confirmButtonText: "Ok"
                        }, function(isConfirm){
                            window.location.href = "supplier";
                        });
                    }
                });
            }
        });
    }

    function verifikasi_delete(id_supplier)
    {
        $.ajax({
            url : "<?php echo site_url('supplier/ajax_verifikasi_delete') ;?>",
            type: "POST",
            cache: false,
            data: 'id_supplier=' + id_supplier,
            dataType: "JSON",
            success: function(data){
                $('.modal-dialog').removeClass('modal-sm');
                $('.modal-dialog').addClass('modal-md');
                $('#ModalHeader').html('Informasi Hapus Jenis Barang');
                $('#ModalContent').html(data.pesan);
                $('#ModalFooter').html(data.footer);
                $('#ModalGue').modal('show');
            },
            error: function (jqXHR, textStatus, errorThrown){
                swal({
                    title: "Gagal!", 
                    text: "Data supplier gagal untuk ditampilkan", 
                    type: "error", 
                    confirmButtonClass: 'btn-danger btn-md waves-effect waves-light',
                    confirmButtonText: "Ok"
                }, function(isConfirm){
                    window.location.href = "supplier";
                });
            }
        });
    }

    function delete_supplier(id_supplier)
    {
        $.ajax({
            url : "<?php echo site_url('supplier/ajax_delete'); ?>",
            type: "POST",
            cache: false,
            data: 'id_supplier=' + id_supplier,
            dataType: "JSON",
            success: function(data){
                $('#modal_form').modal('hide');
                reload_table();
                swal({
                    title: "Berhasil!", 
                    text: "Data supplier berhasil dihapus.", 
                    type: "success", 
                    confirmButtonText: "Ok"
                });
            },
            error: function (jqXHR, textStatus, errorThrown){
                swal({
                    title: "Gagal!", 
                    text: "Data supplier gagal dihapus.", 
                    type: "error", 
                    confirmButtonClass: 'btn-danger btn-md waves-effect waves-light',
                    confirmButtonText: "Ok"
                }, function(isConfirm){
                    window.location.href = "supplier";
                });
            }
        });
    }
</script>
<!-- Akhir Script CRUD -->
<!-- Awal CSS -->
<link href="assets/plugin/zircos/plugins/datatables/jquery.dataTables.min.css" rel="stylesheet" type="text/css"/>
<link href="assets/plugin/zircos/plugins/datatables/responsive.bootstrap.min.css" rel="stylesheet" type="text/css"/>
<link href="assets/plugin/zircos/plugins/datatables/dataTables.bootstrap.min.css" rel="stylesheet" type="text/css"/>
<link href="assets/plugin/zircos/material-design/assets/css/tambahan.css" rel="stylesheet" type="text/css"/>
<link href="assets/plugin/zircos/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css" rel="stylesheet">

<!-- Awal JS -->
<script src="assets/plugin/zircos/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="assets/plugin/zircos/plugins/datatables/dataTables.bootstrap.js"></script>
<script src="assets/plugin/zircos/plugins/datatables/dataTables.responsive.min.js"></script>
<script src="assets/plugin/zircos/plugins/datatables/responsive.bootstrap.min.js"></script>
<script src="assets/plugin/zircos/material-design/assets/pages/jquery.datatables.init.js"></script>
<script src="assets/plugin/zircos/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>

<!-- Awal Sweet-Alert  -->
<link href="assets/plugin/zircos/plugins/bootstrap-sweetalert/sweet-alert.css" rel="stylesheet" type="text/css">
<script src="assets/plugin/zircos/plugins/bootstrap-sweetalert/sweet-alert.min.js"></script>
<!-- Akhir Sweet-Alert  -->

<script type="text/javascript">
var save_method;
var table;

$(document).ready(function() {
    $('#JudulHalaman').html('VAMA | Periode Stok Toko');

    table = $('#table').DataTable({ 
        "processing": true,
        "serverSide": true,
        "order": [],

        "ajax": {
            "url": "<?php echo site_url('periode_stok_toko/ajax_list')?>",
            "type": "POST"
        },

        "columnDefs": [
        { 
            "targets": [ -1 ],
            "orderable": false,
        },
        ],
    });

     //datepicker
    $('#tanggal_periode_awal').datepicker({
        autoclose: true,
        format: "yyyy-mm-dd",
        todayHighlight: true
    });

    $('#tanggal_periode_akhir').datepicker({
        autoclose: true,
        format: "yyyy-mm-dd",
        todayHighlight: true
    });

    $(document).on('DOMSubtreeModified', '#tanggal_periode_awal', function(){
        var tanggal_awal = $('#tanggal_periode_awal').val();
        if(tanggal_awal !== ''){
            $('#tanggal_periode_akhir').val(tanggal_awal);
        }
    });

    $("input").change(function(){
        $(this).parent().parent().removeClass('has-error');
        $(this).next().empty();
    });
    $("textarea").change(function(){
        $(this).parent().parent().removeClass('has-error');
        $(this).next().empty();
    });
    $("select2").change(function(){
        $(this).parent().parent().removeClass('has-error');
        $(this).next().empty();
    });
});

function add_periode_stok_toko()
{
    save_method = 'add';
    $('#form')[0].reset();
    $('.form-group').removeClass('has-error');
    $('.help-block').empty();
    $('#modal_form').modal('show');
    $('.modal-title').text('Tambah Periode Stok tokot');
}

function edit_periode_stok_toko(id_periode_stok_toko)
{
    save_method = 'update';
    $('#form')[0].reset();
    $('.form-group').removeClass('has-error');
    $('.help-block').empty();

    $.ajax({
        url : "<?php echo site_url('periode_stok_toko/ajax_edit/')?>/" + id_periode_stok_toko,
        type: "GET",
        dataType: "JSON",
        success: function(data){
            $('[name="id_periode_stok_toko"]').val(data.id_periode_stok_toko);
            $('[name="id_toko"]').val(data.id_toko);
            $('[name="kode_periode_stok_toko"]').val(data.kode_periode_stok_toko);
            $('[name="tanggal_periode_awal"]').val(data.tanggal_periode_awal);
            $('[name="tanggal_periode_akhir"]').val(data.tanggal_periode_akhir);
            $('#modal_form').modal('show');
            $('.modal-title').text('Edit Periode Stok toko : ' + data.kode_periode_stok_toko);
        },
        error: function (jqXHR, textStatus, errorThrown){
            alert('Error get data from ajax');
        }
    }
    );
}

function reload_table()
{
    table.ajax.reload(null,false);
}

function save()
{
    $('#btnSave').text('menyimpan...');
    $('#btnSave').attr('disabled',true);
    var url;

    if(save_method == 'add'){
        url = "<?php echo site_url('periode_stok_toko/ajax_add')?>";
    } else {
        url = "<?php echo site_url('periode_stok_toko/ajax_update')?>";
    }

    $.ajax({
        url : url,
        type: "POST",
        data: $('#form').serialize(),
        dataType: "JSON",
        success: function(data){
            if(data.status){
                if(data.status == '1'){
                    $('#modal_form').modal('hide');
                    reload_table();

                    swal({
                        title: "Berhasil!", 
                        text: "Data periode stok toko berhasil disimpan.", 
                        type: "success", 
                        confirmButtonClass: 'btn-success btn-md waves-effect waves-light',
                        confirmButtonText: "Ok"
                    });
                }else if(data.status =='0'){
                    swal({
                        title: "Gagal!", 
                        text: "Data periode stok toko gagal disimpan.", 
                        type: "error", 
                        confirmButtonClass: 'btn-danger btn-md waves-effect waves-light',
                        confirmButtonText: "Ok"
                    });
                }
            }else{
                for (var i = 0; i < data.inputerror.length; i++){
                    $('[name="'+data.inputerror[i]+'"]').parent().parent().addClass('has-error');
                    $('[name="'+data.inputerror[i]+'"]').next().text(data.error_string[i]);
                }
            }
            $('#btnSave').text('Simpan');
            $('#btnSave').attr('disabled',false);
        },
        error: function (jqXHR, textStatus, errorThrown){
            alert('Error adding / update data');
            $('#btnSave').text('Simpan');
            $('#btnSave').attr('disabled',false);

        }
    });
}

</script>
<!-- Akhir Script CRUD -->

<!-- Datepicker -->
<script src="assets/plugin/zircos/plugins/moment/moment.js"></script>
<script src="assets/plugin/zircos/plugins/timepicker/bootstrap-timepicker.js"></script>
<script src="assets/plugin/zircos/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js"></script>
<script src="assets/plugin/zircos/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
<script src="assets/plugin/zircos/plugins/clockpicker/js/bootstrap-clockpicker.min.js"></script>
<script src="assets/plugin/zircos/plugins/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- Datepicker -->
<!-- Start content -->
<div class="content">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="page-title-box">
                    <h4 class="page-title text-primary"><i class='mdi mdi-calendar-clock'></i><b>  Periode Stok Toko</b></h4>
                    <ol class="breadcrumb p-0 m-0">
                        <li>
                            <?php if($access_create == 1){ ?>
                                <button class="btn btn-rounded btn-xs btn-success" onclick="add_periode_stok_toko()">
                                    <i class="fa fa-plus"></i> 
                                    TAMBAH
                                </button>
                            <?php } ?>

                            <button class="btn btn-rounded btn-xs btn-primary" onclick="reload_table()">
                                <i class="fa fa-refresh"></i> 
                                REFRESH
                            </button>
                        </li>                        
                    </ol>

                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
        
        <div class="row">
            <div class="col-sm-12">
                <div class="card-box table-responsive">
                    <table id="table" class="table table-condensed table-striped table-hover dt-responsive nowrap" cellspacing="0" width="100%">
                        <thead class="input-sm">
                            <tr>
                                <th>#</th>
                                <th>NAMA TOKO</th>
                                <th>KODE PERIODE</th>
                                <th>TANGGAL AWAL</th>
                                <th>TANGGAL AKHIR</th>
                                <th>OPERATOR PEMBUATAN</th>
                                <th>TANGGAL PEMBUATAN</th>
                                <th style="width:50px;">TOMBOL</th>
                            </tr>
                        </thead>

                        <tbody class="input-sm">
                        </tbody>
                    </table>                    
                </div>
            </div>
        </div>
    </div> <!-- container -->
</div> 
<!-- content -->
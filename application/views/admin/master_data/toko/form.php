<!-- Bootstrap modal -->
<div class="modal fade" id="modal_form" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h3 class="modal-title">Toko Form</h3>
            </div>
            <div class="modal-body form">
                <form action="#" id="form" class="form-horizontal">
                    <input type="hidden" value="" name="id_toko"/> 
                    <div class="form-body">
                        <div class="row">
                            <div class="col-md-6">
                                <!-- <div class="form-group">
                                    <label class="control-label col-md-3">Kode Toko</label>
                                    <div class="col-md-9">
                                        <input name="kode_toko" placeholder="Kode Toko" class="form-control" type="text">
                                        <span class="help-block"></span>
                                    </div>
                                </div> -->

                                <div class="form-group">
                                    <label class="control-label col-md-3">Nama Toko</label>
                                    <div class="col-md-9">
                                        <input name="nama_toko" placeholder="Nama Toko" class="form-control" type="text">
                                        <span class="help-block"></span>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3">Kepala Toko</label>
                                    <div class="col-md-9">
                                        <select name="kepala_toko" class="form-control">
                                            <?php foreach ($list_pegawai_toko as $pegawai_toko) { ?>
                                            <option value="<?php echo $pegawai_toko->kode_pegawai_toko ?>">
                                            <?php echo $pegawai_toko->nama_pegawai_toko ?>
                                            </option>
                                            <?php } ?>
                                        </select>
                                        <span class="help-block"></span>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3">Alamat</label>
                                    <div class="col-md-9">
                                        <textarea name="alamat_toko" placeholder="Alamat" class="form-control"></textarea>
                                        <span class="help-block"></span>                            
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label col-md-3">Email</label>
                                    <div class="col-md-9">
                                        <input name="email" placeholder="Email" class="form-control" type="email">
                                        <span class="help-block"></span>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3">Pin BB</label>
                                    <div class="col-md-9">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <input name="pin_bb1" placeholder="Pin BB 1" class="form-control" type="text">
                                                <span class="help-block"></span>
                                            </div>

                                            <div class="col-md-6">
                                                <input name="pin_bb2" placeholder="Pin BB 2" class="form-control" type="text">
                                                <span class="help-block"></span>
                                            </div>                                            
                                        </div>
                                    </div>
                                </div>                                

                                <div class="form-group">
                                    <label class="control-label col-md-3">Handphone</label>
                                    <div class="col-md-9">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <input name="handphone1" placeholder="Handphone 1" class="form-control" type="number">
                                                <span class="help-block"></span>
                                            </div>

                                            <div class="col-md-6">
                                                <input name="handphone2" placeholder="Handphone 2" class="form-control" type="number">
                                                <span class="help-block"></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3">Telephone</label>
                                    <div class="col-md-9">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <input name="telephone1" placeholder="Telephone 1" class="form-control" type="number">
                                                <span class="help-block"></span>
                                            </div>

                                            <div class="col-md-6">
                                                <input name="telephone2" placeholder="Telephone 2" class="form-control" type="number">
                                                <span class="help-block"></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3">Fax</label>
                                    <div class="col-md-9">
                                        <input name="fax" placeholder="Fax" class="form-control" type="number">
                                        <span class="help-block"></span>
                                    </div>
                                </div>                    
                            </div>
                        </div>                        
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" id="btnSave" onclick="save()" class="btn btn-primary">Save</button>
                <button type="button" class="btn btn-pink" data-dismiss="modal">Cancel</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- End Bootstrap modal
<!-- Start content -->
<div class="content">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="page-title-box">
                    <h4 class="page-title">Toko</h4>
                    <?php 
                        $uri1   = $this->uri->segment(1);        
                        $uri2   = $this->uri->segment(2);
                        $uri3   = $this->uri->segment(3);
                    ?>                    

                    <ol class="breadcrumb p-0 m-0">
                        <li>
                            <a href="<?php echo base_url('')?>">Dashboard</a>
                        </li>
                        <li>
                            <a href="<?php echo base_url($uri1) ?>"><?php echo ucfirst($uri1) ?></a>
                        </li>
                        <?php if ($uri2 !="") { ?>
                        <li class="active">
                            <?php echo ucfirst($uri2) ?>
                        </li>
                        <?php } ?>
                    </ol>

                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
        <!-- end row -->
        
        <div class="row">
            <div class="col-sm-12">
                <div class="card-box table-responsive">
                    <div class="row">
                        <div class="col-xs-6 col-md-6">
                            <!-- <h4 class="m-t-0 header-title"><b>Daftar Pegawai Pusat</b></h4> -->
                            <button class="btn btn-sm btn-success" onclick="add_toko()"><i class="glyphicon glyphicon-plus"></i> TAMBAH</button>
                            <button class="btn btn-sm btn-primary" onclick="reload_table()"><i class="glyphicon glyphicon-refresh"></i> REFRESH</button>

                        </div>
                        <div class="col-xs-6 col-md-6">
                        </div>
                    </div>
                    
                    <table id="table" class="table table-striped table-hover table-colored table-primary dt-responsive nowrap" cellspacing="0" width="100%">
                        <thead class="input-sm">
                            <tr>
                                <th style="width:60px;">KODE TOKO</th>
                                <th style="width:150px;">NAMA TOKO</th>
                                <th>KEPALA TOKO</th>                                
                                <th>EMAIL</th>
                                <th>PIN BB</th>
                                <th>HANDPHONE</th>
                                <th>TELEPHONE</th>
                                <th style="width:50px;">TOMBOL</th>
                            </tr>
                        </thead>

                        <tbody class="input-sm">
                        </tbody>

                        <tfoot class="input-sm">
                            <tr>
                                <th>KODE TOKO</th>
                                <th>NAMA TOKO</th>
                                <th>KEPALA TOKO</th>                                
                                <th>EMAIL</th>
                                <th>PIN BB</th>
                                <th>HANDPHONE</th>
                                <th>TELEPHONE</th>
                                <th>TOMBOL</th>
                            </tr>
                        </tfoot>
                    </table>                    
                </div>
            </div>
        </div>
    </div> <!-- container -->
</div> 
<!-- content -->
<style type="text/css">
    .dropzone {
        border: 2px dashed #0087F7;
    }
</style>

<!-- Bootstrap modal -->
<div class="modal fade" id="modal_form" role="dialog">
    <div class="modal-dialog modal-full">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h5 class="modal-title">Form Master Barang</h5>
            </div>

            <div class="modal-body form">
                <!-- Awal induk tab -->
                <ul class="nav nav-tabs" id="tab_barang">
                    <li class="active">
                        <a href="#tab_barang_pusat" id="tab_pusat" data-toggle="tab" aria-expanded="true">
                            <span class="visible-xs"><i class="fa fa-check-square"></i></span>
                            <span class="hidden-xs">Data Barang Pusat</span>
                        </a>
                    </li>
                   <!--  <li class="">
                        <a href="#tab_barang_toko" id="tab_toko" data-toggle="tab" aria-expanded="true">
                            <span class="visible-xs"><i class="typcn typcn-home"></i></span>
                            <span class="hidden-xs">Data Barang Toko</span>
                        </a>
                    </li> -->
                    <li class="">
                        <a href="#tab_foto_barang" id="tab_foto" data-toggle="tab" aria-expanded="true">
                            <span class="visible-xs"><i class="mdi mdi-camera"></i></span>
                            <span class="hidden-xs">Foto</span>
                        </a>
                    </li>
                </ul>
                <!-- Akhir induk tab -->

                <!-- Awal isi tab -->
                <div class="tab-content">
                    <!-- Awal data barang pusat -->
                    <div class="tab-pane active" id="tab_barang_pusat" name="tab_barang_pusat">
                        <form action="#" id="form" class="form-group">
                            <input type="hidden" id="id_barang_pusat" name="id_barang_pusat">

                            <div class="form-body">
                                <div class="row">
                                    <!-- Awal baris kiri -->
                                    <div id="baris_kiri" class="col-md-4">
                                        <div class="form-horizontal">
                                            <label class="control-label col-md-3 small">Supplier</label>
                                            <div class="col-md-9 form-horizontal">
                                                <div id="induk_pencarian_supplier">
                                                    <select class="form-control select2" id="pilih_supplier" name="pilih_supplier"></select>
                                                </div>
                                                <span class="help-block"></span>
                                            </div>
                                        </div>

                                        <div class="form-horizontal">
                                            <label class="control-label col-md-3 small">Jenis Barang</label>
                                            <div class="col-md-9 form-horizontal">
                                                <div id="induk_pencarian_jenis_barang">
                                                    <select class="form-control select2" id="pilih_jenis_barang" name="pilih_jenis_barang"></select>
                                                </div>
                                                <span class="help-block"></span>
                                            </div>
                                        </div>

                                        <div class="form-horizontal">
                                            <label class="control-label col-md-3 small">Kategori Barang</label>
                                            <div class="col-md-9 form-horizontal">
                                                <div id="induk_pencarian_kategori_barang">
                                                    <select class="form-control select2" id="pilih_kategori_barang" name="pilih_kategori_barang"></select>
                                                </div>
                                                <span class="help-block"></span>
                                            </div>
                                        </div>

                                        <div class="form-horizontal">
                                            <label class="control-label col-md-3 small">Nama Barang</label>
                                            <div class="col-md-9">
                                                <textarea id="nama_barang" name="nama_barang" placeholder="Nama Barang" class="form-control text-dark"></textarea>
                                                <span class="help-block"></span>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Akhir baris kiri -->

                                    <!-- Awal baris tengah -->
                                    <div id="baris_tengah" class="col-md-6">
                                        <div class="form-horizontal">
                                            <label class="control-label col-md-3 small">Modal</label>
                                            <div class="col-md-9">
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <input id="harga_modal" name="harga_modal" placeholder="Modal" class="form-control text-dark" type="text">
                                                        <input type="hidden" id='harga_modal_hidden' name="harga_modal_hidden">
                                                        <span class="help-block"></span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-horizontal">
                                            <label class="control-label col-md-3 small">Resistensi Modal</label>
                                            <div class="col-md-9">
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <input id="resistensi_harga_modal_persen" name="resistensi_harga_modal_persen" placeholder="% Res Modal" class="form-control text-dark" type="text" value='0.00'>
                                                        <span class="help-block"></span>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <input id="resistensi_harga_modal" name="resistensi_harga_modal" placeholder="Rp. Res Modal" class="form-control text-dark" type="text">
                                                        <input type="hidden" id='resistensi_harga_modal_hidden' name="resistensi_harga_modal_hidden">
                                                        <span class="help-block"></span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-horizontal">
                                            <label class="control-label col-md-3 small">Modal Bersih</label>
                                            <div class="col-md-9">
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <input id="harga_modal_bersih" name="harga_modal_bersih" placeholder="Modal Bersih" class="form-control text-dark" type="text">
                                                        <input type="hidden" id='harga_modal_bersih_hidden' name="harga_modal_bersih_hidden">
                                                        <span class="help-block"></span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-horizontal">
                                            <label class="control-label col-md-3 small">Resistensi Eceran</label>
                                            <div class="col-md-9">
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <input id="resistensi_harga_eceran_persen" name="resistensi_harga_eceran_persen" placeholder="% Res Harga Eceran" class="form-control text-dark" type="text" value='0.00'>
                                                        <span class="help-block"></span>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <input id="resistensi_harga_eceran" name="resistensi_harga_eceran" placeholder="Rp. Resistensi Harga Eceran" class="form-control text-dark" type="text">
                                                        <input type="hidden" id='resistensi_harga_eceran_hidden' name="resistensi_harga_eceran_hidden">
                                                        <span class="help-block"></span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-horizontal">
                                            <label class="control-label col-md-3 small">Harga Eceran</label>
                                            <div class="col-md-9">
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <input id="harga_eceran_pusat" name="harga_eceran_pusat" placeholder="Rp. Harga Eceran" class="form-control text-dark" type="text" value="0">
                                                        <input type="hidden" id='harga_eceran_pusat_hidden' name="harga_eceran_pusat_hidden">
                                                        <span class="help-block"></span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-horizontal">
                                            <label class="control-label col-md-3 small">Hrg Grosir 1 & Grosir 2</label>
                                            <div class="col-md-9">
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <input id="harga_grosir1_pusat" name="harga_grosir1_pusat" placeholder="Harga Grosir 1" class="form-control text-dark" type="text">
                                                        <input type="hidden" id='harga_grosir1_pusat_hidden' name="harga_grosir1_pusat_hidden">
                                                        <span class="help-block"></span>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <input id="harga_grosir2_pusat" name="harga_grosir2_pusat" placeholder="Harga Grosir 2" class="form-control text-dark" type="text">
                                                        <input type="hidden" id='harga_grosir2_pusat_hidden' name="harga_grosir2_pusat_hidden">
                                                        <span class="help-block"></span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-horizontal">
                                            <label class="control-label col-md-3 small">Status Barang 1 & 2</label>
                                            <div class="col-md-9">
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <select name="status" class="form-control text-dark">
                                                            <option value="AKTIF">AKTIF</option>
                                                            <option value="TIDAK AKTIF">TIDAK AKTIF</option>
                                                        </select>
                                                        <span class="help-block"></span>
                                                    </div>

                                                    <div class="col-md-6">
                                                        <select name="status2" class="form-control text-dark">
                                                            <option value="JUAL">JUAL</option>
                                                            <option value="MERCHENDISE">MERCHENDISE</option>
                                                            <option value="UNDIAN">UNDIAN</option>
                                                        </select>
                                                        <span class="help-block"></span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Akhir baris tengah -->

                                    <!-- Awal baris kanan -->
                                    <div id="baris_kanan" class="col-md-2">
                                        <div id="foto_barang" name="foto_barang">
                                        </div>
                                    </div>
                                    <!-- Akhir baris kanan -->
                                </div>
                            </div>
                        </form>
                    </div>
                    <!-- Akhir data barang pusat -->

                    <!-- Awal data barang toko -->
                    <!-- <div class="tab-pane" id="tab_barang_toko" name="tab_barang_toko">
                        <div class="row">
                            <div class="col-md-6">
                                <form action="#" id="formBarangToko" class="form-group">
                                    <div class="col-md-12">
                                        <input id="kode_barang" name="kode_barang" class="form-control input-sm" type="hidden" >
                                        <input id="sku" name="sku" class="form-control input-sm" type="hidden">
                                        <div class="form-horizontal">
                                            <label class="control-label col-md-3 small">Nama Toko : </label>
                                            <div class="col-md-9">
                                                <input id="id_barang_toko" name="id_barang_toko" type="hidden" class="form-control" disabled>
                                                <input id="kode_toko" name="kode_toko" type="hidden" class="form-control" disabled>
                                                <input id="nama_toko" name="nama_toko" placeholder="Nama toko" class="form-control text-dark" type="text" disabled>
                                                <span class="help-block"></span>
                                            </div>
                                        </div>
                                        <div class="form-horizontal">
                                            <label class="control-label col-md-3 small">Harga Eceran : </label>
                                            <div class="col-md-9">
                                                <input id="harga_eceran_toko_hidden" name="harga_eceran_toko_hidden" placeholder="Harga eceran toko" class="form-control text-dark" type="hidden" disabled>
                                                <input id="harga_eceran_toko" name="harga_eceran_toko" placeholder="Harga eceran toko" class="form-control text-dark" type="text" disabled>
                                                <span class="help-block"></span>
                                            </div>
                                        </div>
                                        <div class="form-horizontal">
                                            <label class="control-label col-md-3 small">Status : </label>
                                            <div class="col-md-6">
                                                <select id="status_data_toko" name="status_data_toko" class="form-control text-dark" disabled>
                                                    <option value="">-- PILIH --</option>
                                                    <option value="AKTIF">AKTIF</option>
                                                    <option value="TIDAK AKTIF">TIDAK AKTIF</option>
                                                </select>
                                                <span class="help-block"></span>
                                            </div>
                                            <div class="col-sm-3">
                                                <?php if($access_update_barang_toko == '1'){ ?>
                                                    <button type="button" id="SimpanBarangToko" class="btn btn-primary btn-block" disabled>
                                                        Simpan
                                                    </button>
                                                <?php } ?>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>

                            <div class="col-md-6">
                                <table id='TableBarangToko' class="table table-condensed table-striped table-hover dt-responsive" cellspacing="0" width="100%">
                                    <thead class="input-sm">
                                        <tr class="text-dark">
                                            <th>#</th>
                                            <th>Tombol</th>
                                            <th>Nama Toko</th>
                                            <th>Harga Eceran</th>
                                            <th>Status</th>
                                            <th>Tanggal</th>
                                        </tr>
                                    </thead>

                                    <tbody class="input-sm text-dark"></tbody>
                                </table>
                            </div>
                        </div>
                    </div> -->
                    <!-- Akhir data barang toko -->

                    <!-- Awal data foto -->
                    <div class="tab-pane" id="tab_foto_barang" name="tab_foto_barang">
                        <div class="row">
                            <!-- Awal area upload -->
                            <div class="col-md-4">
                                <div class="dropzone">
                                    <div class="dz-message">
                                        <h3><i class="typcn typcn-upload"></i> Klik atau Drop gambar disini</h3>
                                    </div>
                                </div>
                            </div>
                            <!-- Akhir area upload -->

                            <!-- Awal hasil upload -->
                            <div class="col-md-8">
                                <div id="daftar_foto" class="parent-container">
                                </div>
                            </div>
                            <!-- Akhir hasil upload -->
                        </div>
                    </div>
                    <!-- Akhir data foto -->
                </div>
                <!-- Akhir isi tab -->
            </div>

            <div class="modal-footer">
                <button type="button" id="btnSave" onclick="save()" class="btn btn-primary">Simpan</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- End Bootstrap modal -->

<script>
    function simpan_barang_toko(){
        var FormData = "id_barang_toko="+$('#id_barang_toko').val();
            FormData += "&kode_toko="+$('#kode_toko').val();
            FormData += "&harga_eceran="+$('#harga_eceran_toko_hidden').val();
            FormData += "&status="+$('#status_data_toko').val();

        $.ajax({
            url: "<?php echo site_url('barang_toko/ajax_update_barang_toko'); ?>",
            type: "POST",
            cache: false,
            data: FormData,
            dataType:'json',
            success: function(data){
                if(data.status == 1){
                    reload_table_barang_toko();
                    $('#id_barang_toko').val('');
                    $('#kode_toko').val('');
                    $('#nama_toko').val('');
                    $('#harga_eceran_toko').val('');
                    $('#harga_eceran_toko_hidden').val('');
                    $('#status_data_toko').val('');

                    $('#nama_toko').prop('disabled', true);
                    $('#harga_eceran_toko').prop('disabled', true);
                    $('#status_data_toko').prop('disabled', true);
                    $('#SimpanBarangToko').prop('disabled', true);
                }else if(data.status == 0){
                    for (var i = 0; i < data.inputerror.length; i++){
                        $('[name="'+data.inputerror[i]+'"]').parent().parent().addClass('has-error');
                        $('[name="'+data.inputerror[i]+'"]').next().text(data.error_string[i]);
                    }
                    $('#SimpanBarangToko').text('Simpan');
                    $('#SimpanBarangToko').attr('disabled',false);
                }
            }
        });
    }

    $(document).on('keyup', '#harga_eceran_toko', function(e){
        var charCode = e.which || e.keyCode;
        if(charCode == 13){
            simpan_barang_toko();
        }
        else
        {
            var Ecerean   = $('#harga_eceran_toko').val();
                Ecerean   = to_angka(Ecerean);

                if (Ecerean>0)
                {
                    $('#harga_eceran_toko').val(to_rupiah(Ecerean));
                    $('#harga_eceran_toko_hidden').val(Ecerean);
                }
                else
                {
                    $('#harga_eceran_toko').val('');
                    $('#harga_eceran_toko_hidden').val('0');
                }
        }
    });

    $(document).on('click', '#SimpanBarangToko', function(){
        simpan_barang_toko();
    });

    $(document).on('click', '#tab_pusat', function(){
        $('#btnSave').show();
        $.ajax({
            url : "<?php echo site_url('barang_pusat/ajax_edit'); ?>",
            type: "POST",
            cache: false,
            data: 'id_barang_pusat=' + $('#id_barang_pusat').val(),
            dataType: "JSON",
            success: function(json){
                // $('[name="id_barang_pusat"]').val(json.data.id_barang_pusat);
                $('[name="kode_barang"]').val(json.data.kode_barang);
                $('[name="sku"]').val(json.data.sku);
                $('[name="kode_supplier"]').val(json.data.kode_supplier);
                $('[name="nama_supplier"]').val(json.data.nama_supplier);
                $('[name="kode_jenis"]').val(json.data.kode_jenis);
                $('[name="nama_jenis"]').val(json.data.nama_jenis);
                $('[name="kode_kategori"]').val(json.data.kode_kategori);
                $('[name="nama_kategori"]').val(json.data.nama_kategori);
                $('[name="foto_barang"]').html(json.foto);

                $('[name="nama_barang"]').val(json.data.nama_barang);
                $('[name="harga_modal"]').val(to_rupiah(json.data.modal));
                $('[name="harga_modal_hidden"]').val(json.data.modal);
                $('[name="resistensi_harga_modal_persen"]').val(json.data.resistensi_modal);
                $('[name="harga_modal_bersih"]').val(to_rupiah(json.data.modal_bersih));
                $('[name="harga_modal_bersih_hidden"]').val(json.data.modal_bersih);
                $('[name="resistensi_harga_eceran_persen"]').val(json.data.resistensi_harga_eceran);
                $('[name="harga_eceran_pusat"]').val(to_rupiah(json.data.harga_eceran));
                $('[name="harga_eceran_pusat_hidden"]').val(json.data.harga_eceran);            
                $('[name="harga_grosir1_pusat"]').val(to_rupiah(json.data.harga_grosir1));
                $('[name="harga_grosir1_pusat_hidden"]').val(json.data.harga_grosir1);
                $('[name="harga_grosir2_pusat"]').val(to_rupiah(json.data.harga_grosir2));
                $('[name="harga_grosir2_pusat_hidden"]').val(json.data.harga_grosir2);
                $('[name="status"]').val(json.data.status);
                $('[name="status2"]').val(json.data.status2);
                HitungModalBersih();
                HitungHargaEceran();
            },
            error: function (jqXHR, textStatus, errorThrown){
                alert('Gagal menampilkan data barang pusat');
            }
        });
    });

    $(document).on('click', '#tab_toko', function(){
        reload_table_barang_toko();
        $('#formBarangToko')[0].reset(); // reset form on modals
        $('.form-horizontal').removeClass('has-error'); // clear error class
        $('.help-block').empty(); // clear error string 
        $('#btnSave').hide();
    });

    $(document).on('click', '#tab_foto', function(){
        $sku = $('#sku').val();
        tamplikan_daftar_foto($sku);
        $('#btnSave').hide();
    });

    function edit_barang_toko(id_barang_toko)
    {
        $('#formBarangToko')[0].reset(); // reset form on modals
        $('.form-horizontal').removeClass('has-error'); // clear error class
        $('.help-block').empty(); // clear error string 
        //Ajax Load data from ajax
        $.ajax({
            url : "<?php echo site_url('barang_toko/ajax_edit_barang_toko') ;?>",
            type: "POST",
            cache: false,
            data: 'id_barang_toko=' + id_barang_toko,
            dataType: "JSON",
            success: function(data){
                $('#id_barang_toko').val(data.id_barang_toko);
                $('#kode_toko').val(data.kode_toko);
                $('#nama_toko').val(data.nama_customer_pusat);
                $('#harga_eceran_toko').val(to_rupiah(data.harga_eceran));
                $('#harga_eceran_toko_hidden').val(data.harga_eceran);
                if($('#harga_eceran_toko_hidden').val() == '0'){
                    $('#harga_eceran_toko').val('');
                    $('#harga_eceran_toko_hidden').val('');
                }

                $('#status_data_toko').val(data.status);

                $('#harga_eceran_toko').removeAttr('disabled').val();
                $('#status_data_toko').removeAttr('disabled').val();
                $('#SimpanBarangToko').removeAttr('disabled').val();
                $('#harga_eceran_toko').focus();
            },
            error: function (jqXHR, textStatus, errorThrown){
                alert('Error get data from ajax');
            }
        });
    }

    function reload_table_barang_toko()
    {
        $('#TableBarangToko').DataTable().ajax.reload();
    }

    $(document).ready(function(){
        $('#kode_barang').val("#");
        $('#TableBarangToko').DataTable({
            "processing": true,
            "serverSide": true,
            "order": [],
            "lengthMenu": [[5, 10, 25, 50, -1], [5, 10, 25, 50, "All"]],
            "pagingType": "full",

            "ajax": {
                "url": "<?php echo site_url('barang_toko/ajax_list_barang_toko')?>",
                "type": "POST",
                "data": function ( d ) {
                    d.kode_barang = $('#kode_barang').val();
                }
            },
            "deferLoading": 0,
            "columnDefs": [{
                "targets": [ -1 ],
                "orderable": false,
            },
            ],
        });
    });
</script>

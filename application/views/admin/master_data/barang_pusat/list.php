<!-- Start content -->
<div class="content">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="page-title-box">
                    <h4 class="page-title text-primary"><i class='mdi mdi-cube-outline'></i><b>  Barang Pusat</b></h4>
                    <ol class="breadcrumb p-0 m-0">
                        <li>
                            <input type='hidden' id="id_periode_stok_pusat" name="id_periode_stok_pusat" 
                            value="<?php echo $data_periode_sekarang->id_periode_stok_pusat ?>">

                            <b class="small text-muted">Periode Stok : </b>
                            <select id="pilih_periode_stok_pusat" name="pilih_periode_stok_pusat" class="btn btn-rounded btn-xs" onchange="">
                                <?php foreach ($data_periode AS $dp){ ?>
                                    <option value="<?php echo $dp->id_periode_stok_pusat ?>" class="text-dark">
                                        <?php echo $dp->kode_periode_stok_pusat, ' (',
                                        $dp->tanggal_periode_awal, '>', 
                                        $dp->tanggal_periode_akhir, ')' ?>
                                    </option>
                                <?php } ?>
                            </select>

                            <?php if($access_create == '1'){ ?>
                                <button class="btn btn-rounded btn-xs btn-success" onclick="add_barang_pusat()">
                                    <i class="fa fa-plus"></i> 
                                    TAMBAH
                                </button>
                            <?php } ?>
                            
                            <button id="ReferesData" class="btn btn-rounded btn-xs btn-primary" onclick="refresh_data()">
                                <i class="fa fa-refresh"></i> 
                                REFRESH
                            </button>

                            <?php if($this->session->userdata('id_pegawai') == '1'){ ?>
                                <!-- <button id="ImportBarangPusat" class="btn btn-rounded btn-xs btn-default" onclick="import_barang_pusat()">
                                    <i id="icon_import" class="fa fa-refresh"></i> 
                                    Import Barang Pusat
                                </button> -->

                                <button id="FixStok" class="btn btn-rounded btn-xs btn-success" onclick="fix_stok()">
                                    <i id="icon_fix" class="fa fa-check"></i> 
                                    Fix Stok
                                </button>
                            <?php } ?>                                                                
                        </li>                        
                    </ol>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12" style="height: calc(100% - 112px) !important; overflow: auto;">
                <div class="card-box table-responsive">
                    <!-- Awal induk tab -->
                    <ul class="nav nav-tabs tabs-bordered" id="tab_list_barang">
                        <li class="active">
                            <a href="#tab_induk_barang" id="tab_induk" data-toggle="tab" aria-expanded="true">
                                <span class="visible-xs"><i class="fa fa-check-square"></i></span>
                                <span class="hidden-xs">Induk</span>
                            </a>
                        </li>
                        <li class="">
                            <a href="#tab_transaksi_barang" id="tab_transaksi" data-toggle="tab" aria-expanded="true">
                                <span class="visible-xs"><i class="typcn typcn-home"></i></span>
                                <span class="hidden-xs">Transaksi Stok Jual</span>
                            </a>
                        </li>
                        <li class="">
                            <a href="#tab_transaksi_barang_rusak" id="tab_transaksi_rusak" data-toggle="tab" aria-expanded="true">
                                <span class="visible-xs"><i class="typcn typcn-home"></i></span>
                                <span class="hidden-xs">Transaksi Stok Rusak</span>
                            </a>
                        </li>                        
                        <li class="">
                            <a href="#tab_pembaharuan_barang" id="tab_pembaharuan" data-toggle="tab" aria-expanded="true">
                                <span class="visible-xs"><i class="mdi mdi-camera"></i></span>
                                <span class="hidden-xs">Pembaharuan</span>
                            </a>
                        </li>

                        <?php if($this->session->userdata('id_pegawai') == '1'){ ?>
                            <li class="">
                                <a href="#tab_periksa_stok_barang" id="tab_periksa_stok" data-toggle="tab" aria-expanded="true">
                                    <span class="visible-xs"><i class="fa fa-balance-scale"></i></span>
                                    <span class="hidden-xs">Periksa Stok</span>
                                </a>
                            </li>
                        <?php } ?>
                    </ul>
                    <!-- Akhir induk tab -->
                    

                    <!-- Isi Tab -->
                    <div class="tab-content">
                        <!-- Awal master barang -->
                        <div class="tab-pane active" id="tab_induk_barang">
                            <table id="TableMasterBarang" class="table table-condensed table-striped table-hover nowrap" cellspacing="0" width="100%">
                                <thead class="input-sm">
                                    <tr id="header_induk_barang" name="header_induk_barang" class="text-dark">
                                        <th></th>
                                    </tr>
                                </thead>

                                <tbody class="input-sm">
                                </tbody>
                            </table>
                        </div>
                        <!-- Akhir Master Barang -->

                        <!-- Awal histori stok jual -->
                        <div class="tab-pane" id="tab_transaksi_barang">
                            <table id="TableDaftarTransaksi" class="table table-condensed table-striped table-hover nowrap" cellspacing="0" width="100%">
                                <thead class="input-sm">
                                    <tr class="text-dark">
                                        <th>#</th>
                                        <th>SKU</th>
                                        <th>NAMA BARANG</th>
                                        <th>ECERAN</th>
                                        <th>SO +</th>
                                        <th>PB +</th>
                                        <th>DP +</th>
                                        <th>RPJT +</th>
                                        <th>RBDT +</th>
                                        <th>- PJ</th>
                                        <th>- HD</th>
                                        <th>- UP</th>
                                        <th>- RPBT</th>
                                        <th>STOK JUAL</th>
                                    </tr>
                                </thead>

                                <tbody class="input-sm">
                                </tbody>
                            </table>
                        </div>
                        <!-- Akhir histori stok jual -->

                        <!-- Awal histori stok rusak -->
                        <div class="tab-pane" id="tab_transaksi_barang_rusak">
                            <table id="TableDaftarTransaksiRusak" class="table table-condensed table-striped table-hover nowrap" cellspacing="0" width="100%">
                                <thead class="input-sm">
                                    <tr class="text-dark">
                                        <th>#</th>
                                        <th>SKU</th>
                                        <th>NAMA BARANG</th>
                                        <th>ECERAN</th>
                                        <th>RPR +</th>
                                        <th>RBDTR +</th>
                                        <th>- RPBR</th>
                                        <th>STOK RUSAK</th>
                                    </tr>
                                </thead>

                                <tbody class="input-sm">
                                </tbody>
                            </table>
                        </div>
                        <!-- Akhir histori stok rusak -->

                        <!-- Awal daftar pembaharuan barang -->
                        <div class="tab-pane" id="tab_pembaharuan_barang">
                            <table id="TablePembaharuanBarang" class="table table-condensed table-striped table-hover nowrap" cellspacing="0" width="100%">
                                <thead class="input-sm">
                                    <tr class="text-dark">
                                        <th>#</th>
                                        <th>TOMBOL</th>
                                        <th>SKU</th>
                                        <th>NAMA BARANG</th>
                                        <th>ECERAN</th>
                                        <th>GROSIR 1</th>
                                        <th>GROSIR 2</th>
                                        <th>STOK JUAL</th>
                                        
                                        <th>Pegawai Save</th>
                                        <th>Tanggal save</th>
                                        <th>Pegawai Edit</th>
                                        <th>Tanggal Edit</th>
                                    </tr>
                                </thead>

                                <tbody class="input-sm">
                                </tbody>
                            </table>
                        </div>
                        <!-- Akhir daftar pembaharuan barang -->

                        <?php if($this->session->userdata('id_pegawai') == '1'){ ?>
                            <!-- Awal periksa stok -->
                            <div class="tab-pane" id="tab_periksa_stok_barang">
                                <table id="TablePeriksaStok" class="table table-condensed table-striped table-hover nowrap" cellspacing="0" width="100%">
                                    <thead class="input-sm">
                                        <tr>
                                            <th>#</th>
                                            <th>TOMBOL</th>
                                            <th>SKU</th>
                                            <th>NAMA BARANG</th>
                                            <th>ECERAN</th>
                                            <th>STOK HITUNG</th>
                                            <th>STOK TABEL</th>
                                            <th>KOREKSI</th>
                                            <th>STATUS</th>
                                        </tr>
                                    </thead>

                                    <tbody class="input-sm">
                                    </tbody>
                                </table>
                            </div>
                            <!-- Akhir periksa stok -->
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </div> <!-- container -->
</div> 
<!-- content -->

<div class="modal" id="ModalGue" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class='fa fa-times-circle'></i></button>
                <h4 class="modal-title" id="ModalHeader"></h4>
            </div>
            <div class="modal-body" id="ModalContent"></div>
            <div class="modal-footer" id="ModalFooter"></div>
        </div>
    </div>
</div>

<div class="modal" id="ModalDetailTransaksi" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class='fa fa-times-circle'></i></button>
                <h5 class="modal-title" id="ModalHeaderDetailTransaksi"></h5>
            </div>
            <div class="modal-body" id="ModalContentDetailTransaksi">
                <div class="row">
                    <!-- Awal detail barang -->
                    <div class="col-md-6">
                        <div class="row">
                            <div class="col-md-2">
                                <small>Kode Periode</small><br>
                                <span class="text-dark" id="kode_periode_stok_pusat">-</span>
                            </div>

                            <div class="col-md-3">
                                <small>Tanggal Awal Periode</small><br>
                                <span class="text-dark" id="tanggal_periode_awal">-</span>
                            </div>

                            <div class="col-md-3">
                                <small>Tanggal Akhir Periode</small><br>
                                <span class="text-dark" id="tanggal_periode_akhir">-</span>
                            </div>

                            <div class="col-md-2 list_data">
                                <div id="foto_barang_kecil" name="foto_barang_kecil" class="inbox-item">
                                </div>
                            </div>                            
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-md-2">
                                <span id="id_barang_pusat_detail" style="display: none;"></span>
                                <small>SKU</small><br>
                                <span class="text-dark" id="sku">-</span>
                            </div>

                            <div class="col-md-3">
                                <small>Kode Barang</small><br>
                                <span class="text-dark" id="kode_barang">-</span>
                            </div>

                            <div class="col-md-2">
                                <small>Harga Eceran</small><br>
                                <span class="text-dark" id="harga_eceran">Rp. 0</span>
                            </div>

                            <div class="col-md-2">
                                <small>Harga Grosir 1</small><br>
                                <span class="text-dark" id="harga_grosir1">Rp. 0</span>
                            </div>

                            <div class="col-md-2">
                                <small>Harga Grosir 2</small><br>
                                <span class="text-dark" id="harga_grosir2">Rp. 0</span>
                            </div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-md-3">
                                <span class="text-muted">STOK JUAL : </span>
                                <h2 class="text-primary" id="stok_jual">9.999</h2>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-2">
                                <small>SO</small><br>
                                <span class="btn btn-xs btn-default btn-block text-primary" id="so">9.999</span>
                            </div>

                            <div class="col-md-2">
                                <small>+ PB</small><br>
                                <span class="btn btn-xs btn-default btn-block text-primary" id="pb">9.999</span>
                            </div>

                            <div class="col-md-2">
                                <small>+ DP</small><br>
                                <span class="btn btn-xs btn-default btn-block text-primary" id="dp">9.999</span>
                            </div>

                            <div class="col-md-2">
                                <small>+ RPT</small><br>
                                <span class="btn btn-xs btn-default btn-block text-primary" id="rpt">9.999</span>
                            </div>

                            <div class="col-md-2">
                                <small>+ RBDTT</small><br>
                                <span class="btn btn-xs btn-default btn-block text-primary" id="rbdtt">9.999</span>
                            </div>

                            <div class="col-md-2">
                                <small>- UP</small><br>
                                <span class="btn btn-xs btn-default btn-block text-danger" id="up">9.999</span>
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-md-2">
                                <small>- PJU</small><br>
                                <span class="btn btn-xs btn-default btn-block text-danger" id="pju">9.999</span>
                            </div>

                            <div class="col-md-2">
                                <small>- PJT</small><br>
                                <span class="btn btn-xs btn-default btn-block text-danger" id="pjt">9.999</span>
                            </div>

                            <div class="col-md-2">
                                <small>- RPBT</small><br>
                                <span class="btn btn-xs btn-default btn-block text-danger" id="rpbt">9.999</span>
                            </div>

                            <div class="col-md-2">
                                <small>- PP</small><br>
                                <span class="btn btn-xs btn-default btn-block text-danger" id="pp">9.999</span>
                            </div>

                            <div class="col-md-2">
                                <small>- HD</small><br>
                                <span class="btn btn-xs btn-default btn-block text-danger" id="hd">9.999</span>
                            </div>

                            <div class="col-md-2">
                                <small>- PS</small><br>
                                <span class="btn btn-xs btn-default btn-block text-danger" id="ps1">9.999</span>
                            </div>
                        </div>
                        <hr>

                        <div class="row">
                            <div class="col-md-3">
                                <span class="text-muted">STOK RUSAK : </span>
                                <h2 class="text-danger" id="stok_rusak">9.999</h2>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-2">
                                <small>+ RPR</small><br>
                                <span class="btn btn-xs btn-default btn-block text-primary" id="rpr">9.999</span>
                            </div>

                            <div class="col-md-2">
                                <small>+ RBDTR</small><br>
                                <span class="btn btn-xs btn-default btn-block text-primary" id="rbdttr">9.999</span>
                            </div>

                            <div class="col-md-2">
                                <small>+ PS</small><br>
                                <span class="btn btn-xs btn-default btn-block text-primary" id="ps2">9.999</span>
                            </div>

                            <div class="col-md-2">
                                <small>- RPBR</small><br>
                                <span class="btn btn-xs btn-default btn-block text-danger" id="rpbr">9.999</span>
                            </div>
                        </div>                    
                    </div>
                    <!-- Akhir detail barang -->
                    
                    <!-- Awal detail transaksi -->
                    <div class="col-md-6">
                        <h3 id="judul_transaksi" class="text-dark">Transaksi</h3>
                        <table id='TableDetailTransaksi' class="table table-condensed table-striped table-hover dt-responsive nowrap" cellspacing="0" width="100%">
                            <thead class="input-sm">
                                <tr id="header_detail_transaksi" class="text-dark">
                                    <th></th>
                                </tr>
                            </thead>

                            <tbody class="input-sm text-dark"></tbody>
                        </table>
                    </div>
                    <!-- Akhir detail transaksi -->
                </div>
            </div>
            <div class="modal-footer" id="ModalFooterDetailTransaksi"></div>
        </div>
    </div>
</div>

<div class="modal" id="ModalUbahHarga" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class='fa fa-times-circle'></i></button>
                <h5 class="modal-title" id="ModalHeaderUbahHarga"></h5>
            </div>
            <div class="modal-body" id="ModalContentUbahHarga">
                <div class="row">
                    <!-- Awal detail barang -->
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-2">
                                <span id="id_barang_pusat_ubah" style="display: none;"></span>
                                <small>SKU</small><br>
                                <span class="text-dark" id="sku_ubah">-</span>
                            </div>

                            <div class="col-md-3">
                                <small>Kode Barang</small><br>
                                <span class="text-dark" id="kode_barang_ubah">-</span>
                            </div>

                            <div class="col-md-2">
                                <small>Harga Eceran</small><br>
                                <span class="text-dark" id="harga_eceran_ubah">Rp. 0</span>
                            </div>

                            <div class="col-md-2">
                                <small>Harga Grosir 1</small><br>
                                <span class="text-dark" id="harga_grosir1_ubah">Rp. 0</span>
                            </div>

                            <div class="col-md-2">
                                <small>Harga Grosir 2</small><br>
                                <span class="text-dark" id="harga_grosir2_ubah">Rp. 0</span>
                            </div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-md-4">
                                <small>Nama Toko</small><br>
                                <select id="kode_toko" name="kode_toko" class="btn btn-rounded btn-xs btn-default" onchange="TampilkanHargaEceranToko()">
                                    <option value="" class="text-dark ">PILIH TOKO</option>
                                    <?php foreach ($data_toko AS $dt){ ?>
                                        <option value="<?php echo $dt->kode_customer_pusat ?>" class="text-dark">
                                            <?php echo $dt->nama_customer_pusat ?>
                                        </option>
                                    <?php } ?>
                                </select>
                            </div>

                            <div class="col-md-4">
                                <small>Harga Eceran Toko</small><br>
                                <input id="harga_eceran_toko" name="harga_eceran_toko" placeholder="Harga Eceran Toko" class="form-control text-dark" type="text">
                                <input type="hidden" id='harga_eceran_toko_hidden' name="harga_eceran_toko_hidden">
                                <span class="help-block"></span>
                            </div>
                            <div class="col-md-4">
                                <button type="button" id="btnSaveEceran" onclick="save_eceran_toko()" class="btn btn-primary">Simpan</button>
                            </div>
                        </div>
                    </div>
                    <!-- Akhir detail barang -->
                </div>
            </div>
            <div class="modal-footer" id="ModalFooterDetailTransaksi"></div>
        </div>
    </div>
</div>

<script>
    $('#ModalGue').on('hide.bs.modal', function () {
       setTimeout(function(){ 
            $('#ModalHeader, #ModalContent, #ModalFooter').html('');
       }, 500);
    });

    $('#ModalDetailTransaksi').on('hide.bs.modal', function () {
       setTimeout(function(){ 
            $('#ModalHeaderDetailTransaksi, #ModalFooterDetailTransaksi').html('');
       }, 500);
    }); 

    $('#ModalUbahHarga').on('hide.bs.modal', function () {
       setTimeout(function(){ 
            $('#ModalHeaderUbahHarga, #ModalFooterUbahHarga').html('');
       }, 500);
    }); 
</script>
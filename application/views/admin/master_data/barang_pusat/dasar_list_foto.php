<hr>
<h5 class="text-muted text-center">Daftar foto</h5>
<hr>

<div class="row port m-b-20">
    <div class="portfolioContainer">
        <?php for($i=1;$i<=9;$i++): ?>
            <div class="col-md-3 col-md-2">
                <div class="thumb">
                    <a href="assets/upload/image/barang/thumbs/HEADLAMP_TRIANGLE_CHROME.jpg" class="image-popup" title="Screenshot-<?php echo $i ?>">
                        <img src="assets/upload/image/barang/thumbs/HEADLAMP_TRIANGLE_CHROME.jpg" class="thumb-img" alt="work-thumbnail">
                    </a>
                    <div class="gal-detail">
                        <h4>Foto Barang - <?php echo $i ?></h4>
                        <p>
                            <a href="#" class="btn btn-danger" role="button">Hapus</a>
                        </p>
                    </div>
                </div>
            </div>
        <?php endfor; ?>
    </div>
</div>
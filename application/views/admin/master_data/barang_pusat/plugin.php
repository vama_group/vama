<link href="assets/plugin/zircos/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
<link href="assets/plugin/zircos/plugins/bootstrap-select/css/bootstrap-select.min.css" rel="stylesheet" />
<script src="assets/select2/select2.min.js" type="text/javascript"></script>

<!-- Awal CSS -->
<link href="assets/plugin/zircos/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
<link href="assets/plugin/zircos/material-design/assets/css/tambahan.css" rel="stylesheet" type="text/css"/>
<link href="assets/plugin/zircos/plugins/datatables/jquery.dataTables.min.css" rel="stylesheet" type="text/css"/>
<link href="assets/plugin/zircos/plugins/datatables/buttons.bootstrap.min.css" rel="stylesheet" type="text/css"/>
<link href="assets/plugin/zircos/plugins/datatables/dataTables.colVis.css" rel="stylesheet" type="text/css"/>
<link href="assets/plugin/zircos/plugins/datatables/scroller.bootstrap.min.css" rel="stylesheet" type="text/css"/>
<link href="assets/plugin/zircos/plugins/datatables/fixedColumns.dataTables.min.css" rel="stylesheet" type="text/css"/>
<link href="assets/plugin/zircos/plugins/datatables/responsive.bootstrap.min.css" rel="stylesheet" type="text/css"/>
<link href="assets/plugin/zircos/plugins/datatables/dataTables.bootstrap.min.css" rel="stylesheet" type="text/css"/>
<link href="assets/plugin/zircos/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css" rel="stylesheet">
<link href="assets/plugin/zircos/plugins/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
<!-- Akhir CSS -->

<!-- Awal JS -->
<script src="assets/plugin/zircos/plugins/select2/js/select2.min.js" type="text/javascript"></script>
<script src="assets/plugin/zircos/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="assets/plugin/zircos/plugins/datatables/dataTables.bootstrap.js"></script>

<script src="assets/plugin/zircos/material-design/assets/js/detect.js"></script>
<script src="assets/plugin/zircos/material-design/assets/js/jquery.slimscroll.js"></script>
<script src="assets/plugin/zircos/plugins/datatables/dataTables.colVis.js"></script>
<script src="assets/plugin/zircos/plugins/datatables/dataTables.responsive.min.js"></script>
<script src="assets/plugin/zircos/plugins/datatables/responsive.bootstrap.min.js"></script>
<script src="assets/plugin/zircos/plugins/datatables/dataTables.fixedColumns.min.js"></script>
<script src="assets/plugin/zircos/plugins/datatables/dataTables.scroller.min.js"></script>
<!-- Akhir JS -->

<!-- Awal datatable -->
<script src="assets/plugin/zircos/plugins/datatables/dataTables.buttons.min.js"></script>
<script src="assets/plugin/zircos/plugins/datatables/buttons.bootstrap.min.js"></script>
<script src="assets/plugin/zircos/plugins/datatables/jszip.min.js"></script>
<script src="assets/plugin/zircos/plugins/datatables/pdfmake.min.js"></script>
<script src="assets/plugin/zircos/plugins/datatables/vfs_fonts.js"></script>
<script src="assets/plugin/zircos/plugins/datatables/buttons.html5.min.js"></script>
<script src="assets/plugin/zircos/plugins/datatables/buttons.print.min.js"></script>
<!-- Akhir datatable -->

<!-- Awal date rangepicker -->
<script src="assets/plugin/zircos/plugins/moment/moment.js"></script>
<script src="assets/plugin/zircos/plugins/timepicker/bootstrap-timepicker.js"></script>
<script src="assets/plugin/zircos/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js"></script>
<script src="assets/plugin/zircos/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
<script src="assets/plugin/zircos/plugins/clockpicker/js/bootstrap-clockpicker.min.js"></script>
<script src="assets/plugin/zircos/plugins/bootstrap-daterangepicker/daterangepicker.js"></script>
<script src="assets/plugin/zircos/material-design/assets/pages/jquery.form-pickers.init.js"></script>
<!-- Akhir date rangepicker -->

<!-- Awal upload foto-->
<link rel="stylesheet" href="assets/plugin/zircos/plugins/magnific-popup/css/magnific-popup.css"/>
<script type="text/javascript" src="assets/plugin/zircos/plugins/magnific-popup/js/jquery.magnific-popup.min.js"></script>

<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/dropzone/dropzone.min.css') ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/dropzone/basic.min.css') ?>">
<script type="text/javascript" src="<?php echo base_url('assets/dropzone/dropzone.min.js') ?>"></script>
<!-- Akhir upload foto-->

<!-- Awal Sweet-Alert  -->
<link href="assets/plugin/zircos/plugins/bootstrap-sweetalert/sweet-alert.css" rel="stylesheet" type="text/css">
<script src="assets/plugin/zircos/plugins/bootstrap-sweetalert/sweet-alert.min.js"></script>
<!-- Akhir Sweet-Alert  -->

<!-- Awal Script CRUD -->
<script type="text/javascript">
    var save_method;
    var status_tampil                = '0';
    var table_master_barang          = $('#TableMasterBarang').DataTable();
    var table_daftar_transaksi       = $('#TableDaftarTransaksi').DataTable();
    var table_pembaharuan_barang     = $('#TablePembaharuanBarang').DataTable();
    var table_daftar_transaksi_rusak = $('#TableDaftarTransaksiRusak').DataTable();
    var table_detail_transaksi       = $('#TableDetailTransaksi').DataTable();
    var table_daftar_periksa_stok    = $('#TablePeriksaStok').DataTable();
    var jenis_transaksi              = "so";
    var id_tab                       = 1;
    var id_barang_pusat;

    <?php if($this->session->userdata('usergroup_name') == 'Super Admin'){ ?>
        var pesan_tampil = 'Yakin ingin menampilkan detail ?';
    <?php } ?>

    // Awal fungsi pilih supplier
    $("#pilih_supplier").select2({
        placeholder: 'Pilih supplier',
        ajax: {
            url:"<?php echo site_url('supplier/list_supplier')?>",
            dataType: 'json',
            type: "GET",
            delay: 250,
            data: function (params) {
                return {
                    ns: params.term
                };
            },
            processResults: function (data) {
                if(data !== ''){
                    var res = data.map(function (item) {
                          return {id: item.kode_supplier, text: item.nama_supplier};
                        });
                }else{
                    res = '';
                }
                return {
                    results: res
                };
            }
        }
    });

    $("#pilih_supplier").change(function(){        
        $('#pilih_supplier').select2('close');
        $('#pilih_jenis_barang').select2('open').select2('options');
    });
    // Akhir fungsi pilih supplier

    // Awal fungsi pilih jenis barang
    $("#pilih_jenis_barang").select2({
        placeholder: 'Pilih jenis barang',
        ajax: {
            url:"<?php echo site_url('jenis_barang/list_jenis_barang')?>",
            dataType: 'json',
            type: "GET",
            delay: 250,
            data: function (params) {
                return {
                    nj: params.term
                };
            },
            processResults: function (data) {
                if(data !== ''){
                    var res = data.map(function (item) {
                          return {id: item.kode_jenis, text: item.nama_jenis};
                        });
                }else{
                    res = '';
                }
                return {
                    results: res
                };
            }
        }
    });

    $("#pilih_jenis_barang").change(function(){        
        $('#pilih_jenis_barang').select2('close');
        $('#pilih_kategori_barang').html('');
        $('#pilih_kategori_barang').select2('open').select2('options');
    });
    // Akhir fungsi pilih jenis barang

    // Awal fungsi pilih kategori barang
    $("#pilih_kategori_barang").select2({
        placeholder: 'Pilih kategori barang',
        ajax: {
            url:"<?php echo site_url('kategori_barang/list_kategori_barang')?>",
            dataType: 'json',
            type: "GET",
            delay: 250,
            data: function (params) {
                return {
                    nk: params.term,
                    kode_jenis: $('#pilih_jenis_barang').val()
                };
            },
            processResults: function (data) {
                if(data !== ''){
                    var res = data.map(function (item) {
                          return {id: item.kode_kategori, text: item.nama_kategori};
                        });
                }else{
                    res = '';
                }
                return {
                    results: res
                };
            }
        }
    });

    $("#pilih_kategori_barang").change(function(){        
        $('#nama_barang').focus();
    });
    // Akhir fungsi pilih kategori barang

    function TampilkanMasterBarang(){
        $('#header_induk_barang').html(
            '<th>#</th>' +
            '<th>TOMBOL</th>' +
            '<th>SKU</th>' +
            '<th>Nama Barang</th>' +
            '<th>STK JL</th>' +
            '<th>STK RK</th>' +
            '<th>ECERAN</th>' +
            '<th>GROSIR 1</th>' +
            '<th>GROSIR 2</th>' +
            '<th>STATUS</th>' +
            '<th>PG Save</th>' +
            '<th>TGL Save</th>' +
            '<th>PG Edit</th>' +
            '<th>TGL Edit</th>'
        );

        <?php if($this->session->userdata('usergroup_name') == 'Super Admin'){ ?>
            if(status_tampil == '1'){
                $('#header_induk_barang').html(
                    '<th>#</th>' +
                    '<th>TOMBOL</th>' +
                    '<th>SUPPLIER</th>' +
                    '<th>SKU</th>' +
                    '<th>Nama Barang</th>' +
                    '<th>STOK JUAL</th>' +
                    '<th>STOK RUSAK</th>' +
                    '<th>MODAL</th>' +
                    '<th>RES MODAL</th>' +
                    '<th>MODAL BERSIH</th>' +
                    '<th>ECERAN</th>' +
                    '<th>GROSIR 1</th>' +
                    '<th>GROSIR 2</th>' +
                    '<th>STATUS</th>' +
                    '<th>Pegawai Save</th>' +
                    '<th>Tanggal Save</th>' +
                    '<th>Pegawai Edit</th>' +
                    '<th>Tanggal Edit</th>'
                );
            }
        <?php } ?>

        table_master_barang.clear();table_master_barang.destroy();
        table_master_barang=$('#TableMasterBarang').DataTable({
            <?php if($this->session->userdata('usergroup_name') == 'Super Admin'){ ?>
                dom : "Blftpi",
            <?php } ?>
            buttons: [
                {
                    extend: "copy",
                    text: "<i class='fa fa-files-o text-primary'></i>",
                    titleAttr: "Salin data ke clipboard"
                }, 
                {
                    extend: "excel",
                    text: "<i class='fa fa-file-excel-o text-success'></i>",
                    titleAttr: "Excel"
                }, 
                {
                    extend: "pdf",
                    text: "<i class='fa fa-file-pdf-o text-danger'></i>",
                    titleAttr: "PDF"
                }, 
                {
                    extend: "print",
                    text: "<i class='fa fa-print'></i>",
                    titleAttr: "Print"
                }
            ],
            "dom": 'Clftpir',
            colVis: {
                buttonText: "<i class='fa fa-eye'></i> <b>Tampilkan / sembunyikan kolom</b>"
            },
            stateSave: true,
            ordering: true,
            bFilter:true,
            processing:true,
            serverSide:true,
            order:[],
            lengthMenu: [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
            oLanguage:{
                sLoadingRecords:'Silakan Tunggu',
                sProcessing:"<img src='assets/loader/cubeloader2.2.gif' width='50' height='50'> Harap tunggu",
                sZeroRecords:"Data Tidak Ditemukan",
                sSearch : "Pencarian :  _INPUT_",
                sLengthMenu: "<i class='btn btn-primary btn-sm fa fa-folder-open-o'></i><span class='btn btn-default btn-xs'>Menampilkan _MENU_ barang pusat</span>",
                sInfo: "<span class='btn btn-sm'>Menampilkan (_START_ sampai _END_) dari _TOTAL_ barang pusat</span>",
                sInfoEmpty: "Tidak ada barang pusat untuk ditampilkan",
                sInfoFiltered: " - (disaring dari _MAX_ total barang pusat)"
            },
            ajax:{
                url: "<?php echo site_url('barang_pusat/ajax_list_master_barang')?>",
                type: "POST",
                data: {'id_periode_stok_pusat' : $('#pilih_periode_stok_pusat').val(),
                       'status_tampil' : status_tampil}
            },
            columnDefs: [{ 
                            "targets": [ -1 ],
                            "orderable": false,
                        },],

            scrollX: true,
            deferRender: true,
            scrollY: 650,
            scrollCollapse: true,
            scroller: true
        });
        $('#TableMasterBarang').css("width", "100% !important");
    }

    function TampilkanDaftarTransaksi(){
        table_daftar_transaksi.clear();table_daftar_transaksi.destroy();
        table_daftar_transaksi=$('#TableDaftarTransaksi').DataTable(
        {
            <?php if($this->session->userdata('usergroup_name') == 'Super Admin'){ ?>
                dom : "Blftpi",
            <?php } ?>
            buttons: [
                        {
                            extend: "copy",
                            text: "<i class='fa fa-files-o text-primary'></i>",
                            titleAttr: "Salin data ke clipboard"
                        }, 
                        {
                            extend: "excel",
                            text: "<i class='fa fa-file-excel-o text-success'></i>",
                            titleAttr: "Excel"
                        }, 
                        {
                            extend: "pdf",
                            text: "<i class='fa fa-file-pdf-o text-danger'></i>",
                            titleAttr: "PDF"
                        }, 
                        {
                            extend: "print",
                            text: "<i class='fa fa-print'></i>",
                            titleAttr: "Print"
                        }
                     ],
            stateSave: true,
            ordering: true,
            bFilter:true,
            processing:true,
            serverSide:true,
            order:[],
            lengthMenu: [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
            // buttons: ['pageLength'],
            oLanguage:{
                sLoadingRecords:'Silakan Tunggu',
                sProcessing:"<img src='assets/loader/cubeloader2.2.gif' width='50' height='50'> Harap tunggu",
                sZeroRecords:"Data Tidak Ditemukan",
                sSearch : "Pencarian :  _INPUT_",
                sLengthMenu: "<i class='btn btn-primary btn-sm fa fa-folder-open-o'></i><span class='btn btn-default btn-xs'>Menampilkan _MENU_ barang pusat</span>",
                sInfo: "<span class='btn btn-sm'>Menampilkan (_START_ sampai _END_) dari _TOTAL_ barang pusat</span>",
                sInfoEmpty: "Tidak ada barang pusat untuk ditampilkan",
                sInfoFiltered: " - (disaring dari _MAX_ total barang pusat)"
            },
            ajax:{
                url: "<?php echo site_url('barang_pusat/ajax_list_daftar_transaksi')?>",
                type: "POST",
                data: {'id_periode_stok_pusat' : $('#pilih_periode_stok_pusat').val()}
            },
            columnDefs: [{ 
                            "targets": [ -1 ],
                            "orderable": false,
                        },],

            scrollX: true,
            deferRender: true,
            scrollY: 650,
            scrollCollapse: true,
            scroller: true
        });
    }

    function TampilkanDaftarTransaksiRusak(){
        table_daftar_transaksi_rusak.clear();table_daftar_transaksi_rusak.destroy();
        table_daftar_transaksi_rusak=$('#TableDaftarTransaksiRusak').DataTable({
            <?php if($this->session->userdata('usergroup_name') == 'Super Admin'){ ?>
                dom : "Blftpi",
            <?php } ?>
            buttons: [
                        {
                            extend: "copy",
                            text: "<i class='fa fa-files-o text-primary'></i>",
                            titleAttr: "Salin data ke clipboard"
                        }, 
                        {
                            extend: "excel",
                            text: "<i class='fa fa-file-excel-o text-success'></i>",
                            titleAttr: "Excel"
                        }, 
                        {
                            extend: "pdf",
                            text: "<i class='fa fa-file-pdf-o text-danger'></i>",
                            titleAttr: "PDF"
                        }, 
                        {
                            extend: "print",
                            text: "<i class='fa fa-print'></i>",
                            titleAttr: "Print"
                        }
                     ],
            stateSave: true,
            ordering: true,
            bFilter:true,
            processing:true,
            serverSide:true,
            order:[],
            lengthMenu: [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
            // buttons: ['pageLength'],
            oLanguage:{
                sLoadingRecords:'Silakan Tunggu',
                sProcessing:"<img src='assets/loader/cubeloader2.2.gif' width='50' height='50'> Harap tunggu",
                sZeroRecords:"Data Tidak Ditemukan",
                sSearch : "Pencarian :  _INPUT_",
                sLengthMenu: "<i class='btn btn-primary btn-sm fa fa-folder-open-o'></i><span class='btn btn-default btn-xs'>Menampilkan _MENU_ barang pusat</span>",
                sInfo: "<span class='btn btn-sm'>Menampilkan (_START_ sampai _END_) dari _TOTAL_ barang pusat</span>",
                sInfoEmpty: "Tidak ada barang pusat untuk ditampilkan",
                sInfoFiltered: " - (disaring dari _MAX_ total barang pusat)"
            },
            ajax:{
                url: "<?php echo site_url('barang_pusat/ajax_list_daftar_transaksi_rusak')?>",
                type: "POST",
                data: {'id_periode_stok_pusat' : $('#pilih_periode_stok_pusat').val()}
            },
            columnDefs: [{ 
                            "targets": [ -1 ],
                            "orderable": false,
                        },],
            scrollX: true,
            deferRender: true,
            scrollY: 650,
            scrollCollapse: true,
            scroller: true
        });
    }

    function TampilkanPembaharuanBarang(){
        table_pembaharuan_barang.clear();table_pembaharuan_barang.destroy();
        table_pembaharuan_barang=$('#TablePembaharuanBarang').DataTable({
            <?php if($this->session->userdata('usergroup_name') == 'Super Admin'){ ?>
                dom : "Blftpi",
            <?php } ?>
            buttons: [
                        {
                            extend: "copy",
                            text: "<i class='fa fa-files-o text-primary'></i>",
                            titleAttr: "Salin data ke clipboard"
                        }, 
                        {
                            extend: "excel",
                            text: "<i class='fa fa-file-excel-o text-success'></i>",
                            titleAttr: "Excel"
                        }, 
                        {
                            extend: "pdf",
                            text: "<i class='fa fa-file-pdf-o text-danger'></i>",
                            titleAttr: "PDF"
                        }, 
                        {
                            extend: "print",
                            text: "<i class='fa fa-print'></i>",
                            titleAttr: "Print"
                        }
                     ],
            stateSave: true,
            ordering: true,
            bFilter:true,
            processing:true,
            serverSide:true,
            order:[],
            lengthMenu: [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
            // buttons: ['pageLength'],
            oLanguage:{
                sLoadingRecords:'Silakan Tunggu',
                sProcessing:"<img src='assets/loader/cubeloader2.2.gif' width='50' height='50'> Harap tunggu",
                sZeroRecords:"Data Tidak Ditemukan",
                sSearch : "Pencarian :  _INPUT_",
                sLengthMenu: "<i class='btn btn-primary btn-sm fa fa-folder-open-o'></i><span class='btn btn-default btn-xs'>Menampilkan _MENU_ barang pusat</span>",
                sInfo: "<span class='btn btn-sm'>Menampilkan (_START_ sampai _END_) dari _TOTAL_ barang pusat</span>",
                sInfoEmpty: "Tidak ada barang pusat untuk ditampilkan",
                sInfoFiltered: " - (disaring dari _MAX_ total barang pusat)"
            },
            ajax:{
                url: "<?php echo site_url('barang_pusat/ajax_list_pembaharuan_barang')?>",
                type: "POST",
                data: {'id_periode_stok_pusat' : $('#pilih_periode_stok_pusat').val()}
            },
            columnDefs: [{ 
                            "targets": [ -1 ],
                            "orderable": false,
                        },],

            scrollX: true,
            deferRender: true,
            scrollY: 650,
            scrollCollapse: true,
            scroller: true
        });
    }

    function TampilkanDetailTransaksi(){
        // Stok Jual
        // alert(jenis_transaksi);
        // return;
        
        if(jenis_transaksi == 'so'){
            $('#header_detail_transaksi').html(
                '<th>#</th>' +
                '<th>SO Sebelumnya</th>' +
                '<th>SO Sekarang</th>' +
                '<th>Catatan</th>' +
                '<th>Pegawai Save</th>' +
                '<th>Tanggal Save</th>' +
                '<th>Pegawai Edit</th>' +
                '<th>Tanggal Edit</th>'
            );
        }else if(jenis_transaksi == 'pb'){
            $('#header_detail_transaksi').html(
                '<th>#</th>' +
                '<th>No. Pembelian</th>' +
                '<th>No. Faktur</th>' +
                '<th>Jumlah Beli</th>' +
                '<th>Catatan</th>' +
                '<th>Pegawai Save</th>' +
                '<th>Tanggal Save</th>' +
                '<th>Pegawai Edit</th>' +
                '<th>Tanggal Edit</th>'
            );
        }else if(jenis_transaksi == 'dp'){
            $('#header_detail_transaksi').html(
                '<th>#</th>' +
                '<th>No. Perakitan</th>' +
                '<th>Jumlah Perakitan</th>' +
                '<th>Catatan</th>' +
                '<th>Pegawai Save</th>' +
                '<th>Tanggal Save</th>' +
                '<th>Pegawai Edit</th>' +
                '<th>Tanggal Edit</th>'
            );
        }else if(jenis_transaksi == 'rpt'){
            $('#header_detail_transaksi').html(
                '<th>#</th>' +
                '<th>No. Retur Penjualan</th>' +
                '<th>No. Penjualan</th>' +
                '<th>Jumlah Jual</th>' +
                '<th>Jumlah Retur</th>' +
                '<th>Catatan</th>' +
                '<th>Pegawai Save</th>' +
                '<th>Tanggal Save</th>' +
                '<th>Pegawai Edit</th>' +
                '<th>Tanggal Edit</th>'
            );
        }else if(jenis_transaksi == 'rbdtt'){
            $('#header_detail_transaksi').html(
                '<th>#</th>' +
                '<th>No. Retur</th>' +
                '<th>Nama Toko</th>' +
                '<th>Jumlah Masuk</th>' +
                '<th>Catatan</th>' +
                '<th>Pegawai Save</th>' +
                '<th>Tanggal Save</th>' +
                '<th>Pegawai Edit</th>' +
                '<th>Tanggal Edit</th>'
            );
        }else if(jenis_transaksi == 'up'){
            $('#header_detail_transaksi').html(
                '<th>#</th>' +
                '<th>No. Perakitan</th>' +
                '<th>Jumlah Komponen</th>' +
                '<th>Catatan</th>' +
                '<th>Pegawai Save</th>' +
                '<th>Tanggal Save</th>' +
                '<th>Pegawai Edit</th>' +
                '<th>Tanggal Edit</th>'
            );
        }else if(jenis_transaksi == 'pju'){
            $('#header_detail_transaksi').html(
                '<th>#</th>' +
                '<th>No. Penjualan</th>' +
                '<th>Nama Customer</th>' +
                '<th>Jumlah Jual</th>' +
                '<th>Tipe Penjualan</th>' +
                '<th>Catatan</th>' +
                '<th>Pegawai Save</th>' +
                '<th>Tanggal Save</th>' +
                '<th>Pegawai Edit</th>' +
                '<th>Tanggal Edit</th>'
            );
        }else if(jenis_transaksi == 'pjt'){
            $('#header_detail_transaksi').html(
                '<th>#</th>' +
                '<th>No. Penjualan</th>' +
                '<th>Nama Toko</th>' +
                '<th>Jumlah Jual</th>' +
                '<th>Catatan</th>' +
                '<th>Pegawai Save</th>' +
                '<th>Tanggal Save</th>' +
                '<th>Pegawai Edit</th>' +
                '<th>Tanggal Edit</th>' +
                '<th>Pegawai Masuk</th>' +
                '<th>Tanggal Masuk</th>'
            );
        }else if(jenis_transaksi == 'rpbt'){
            $('#header_detail_transaksi').html(
                '<th>#</th>' +
                '<th>No. Retur Pembelian</th>' +
                '<th>No. Pembelian</th>' +
                '<th>Jumlah Retur</th>' +
                '<th>Catatan</th>' +
                '<th>Pegawai Save</th>' +
                '<th>Tanggal Save</th>' +
                '<th>Pegawai Edit</th>' +
                '<th>Tanggal Edit</th>'
            );
        }else if(jenis_transaksi == 'hd'){
            $('#header_detail_transaksi').html(
                '<th>#</th>' +
                '<th>No. Hadiah</th>' +
                '<th>Nama Hadiah</th>' +
                '<th>Nama Customer</th>' +
                '<th>Jumlah Hadiah</th>' +
                '<th>Catatan</th>' +
                '<th>Pegawai Save</th>' +
                '<th>Tanggal Save</th>' +
                '<th>Pegawai Edit</th>' +
                '<th>Tanggal Edit</th>'
            );
        }

        // Stok Rusak
        if(jenis_transaksi == 'rpr'){
            $('#header_detail_transaksi').html(
                '<th>#</th>' +
                '<th>No. Retur Penjualan</th>' +
                '<th>No. Penjualan</th>' +
                '<th>Jumlah Jual</th>' +
                '<th>Jumlah Retur</th>' +
                '<th>Nama Customer</th>' +
                '<th>Catatan</th>' +
                '<th>Pegawai Save</th>' +
                '<th>Tanggal Save</th>' +
                '<th>Pegawai Edit</th>' +
                '<th>Tanggal Edit</th>'
            );
        }else if(jenis_transaksi == 'rbdttr'){
            $('#header_detail_transaksi').html(
                '<th>#</th>' +
                '<th>No. Retur</th>' +
                '<th>Nama Toko</th>' +
                '<th>Jumlah Masuk</th>' +
                '<th>Catatan</th>' +
                '<th>Pegawai Save</th>' +
                '<th>Tanggal Save</th>' +
                '<th>Pegawai Edit</th>' +
                '<th>Tanggal Edit</th>'
            );
        }else if(jenis_transaksi == 'rpbr'){
            $('#header_detail_transaksi').html(
                '<th>#</th>' +
                '<th>No. Retur Pembelian</th>' +
                '<th>No. Pembelian</th>' +
                '<th>Jumlah Retur</th>' +
                '<th>Catatan</th>' +
                '<th>Pegawai Save</th>' +
                '<th>Tanggal Save</th>' +
                '<th>Pegawai Edit</th>' +
                '<th>Tanggal Edit</th>'
            );
        }

        table_detail_transaksi.clear();table_detail_transaksi.destroy();
        table_detail_transaksi=$('#TableDetailTransaksi').DataTable({
            <?php if($this->session->userdata('usergroup_name') == 'Super Admin'){ ?>
                dom : "Blftpi",
            <?php } ?>
            buttons: [
                        {
                            extend: "copy",
                            text: "<i class='fa fa-files-o text-primary'></i>",
                            titleAttr: "Salin data ke clipboard"
                        }, 
                        {
                            extend: "excel",
                            text: "<i class='fa fa-file-excel-o text-success'></i>",
                            titleAttr: "Excel"
                        }, 
                        {
                            extend: "pdf",
                            text: "<i class='fa fa-file-pdf-o text-danger'></i>",
                            titleAttr: "PDF"
                        }, 
                        {
                            extend: "print",
                            text: "<i class='fa fa-print'></i>",
                            titleAttr: "Print"
                        }
                     ],
            stateSave: true,
            ordering: true,
            bFilter:true,
            processing:true,
            serverSide:true,
            order:[],
            lengthMenu: [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
            // buttons: ['pageLength'],
            oLanguage:{
                sLoadingRecords:'Silakan Tunggu',
                sProcessing:"<img src='assets/loader/cubeloader2.2.gif' width='50' height='50'> Harap tunggu",
                sZeroRecords:"Data Tidak Ditemukan",
                sSearch : "Pencarian :  _INPUT_",
                sLengthMenu: "<i class='btn btn-primary btn-sm fa fa-folder-open-o'></i><span class='btn btn-default btn-xs'>Menampilkan _MENU_ barang pusat</span>",
                sInfo: "<span class='btn btn-sm'>Menampilkan (_START_ sampai _END_) dari _TOTAL_ barang pusat</span>",
                sInfoEmpty: "Tidak ada barang pusat untuk ditampilkan",
                sInfoFiltered: " - (disaring dari _MAX_ total barang pusat)"
            },
            ajax:{
                url: "<?php echo site_url('barang_pusat/ajax_list_detail_transaksi')?>",
                type: "POST",
                data: {
                    'id_barang_pusat' : $('#id_barang_pusat_detail').html(),
                    'jenis_transaksi' : jenis_transaksi,
                    'id_periode_stok_pusat' : $('#pilih_periode_stok_pusat').val()
                }
            },
            columnDefs: [{ 
                            "targets": [ -1 ],
                            "orderable": false,
                        },],
        });
    }

    function TampilkanDaftarPeriksaStok(){
        // Awal table periksa stok Barang
        table_daftar_periksa_stok.clear();table_daftar_periksa_stok.destroy();
        table_daftar_periksa_stok=$('#TablePeriksaStok').DataTable(
        {
            <?php if($this->session->userdata('usergroup_name') == 'Super Admin'){ ?>
                dom : "Blftpi",
            <?php } ?>
            buttons: [
                        {
                            extend: "copy",
                            text: "<i class='fa fa-files-o text-primary'></i>",
                            titleAttr: "Salin data ke clipboard"
                        }, 
                        {
                            extend: "excel",
                            text: "<i class='fa fa-file-excel-o text-success'></i>",
                            titleAttr: "Excel"
                        }, 
                        {
                            extend: "pdf",
                            text: "<i class='fa fa-file-pdf-o text-danger'></i>",
                            titleAttr: "PDF"
                        }, 
                        {
                            extend: "print",
                            text: "<i class='fa fa-print'></i>",
                            titleAttr: "Print"
                        }
                     ],
            stateSave: true,
            ordering: true,
            bFilter:true,
            processing:true,
            serverSide:true,
            order:[],
            lengthMenu: [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
            oLanguage:{
                sLoadingRecords:'Silakan Tunggu',
                sProcessing:"<img src='assets/loader/cubeloader2.2.gif' width='50' height='50'> Harap tunggu",
                sZeroRecords:"Data Tidak Ditemukan",
                sSearch : "Pencarian :  _INPUT_",
                sLengthMenu: "<i class='btn btn-primary btn-sm fa fa-folder-open-o'></i><span class='btn btn-default btn-xs'>Menampilkan _MENU_ barang pusat</span>",
                sInfo: "<span class='btn btn-sm'>Menampilkan (_START_ sampai _END_) dari _TOTAL_ barang pusat</span>",
                sInfoEmpty: "Tidak ada barang pusat yang selisih untuk ditampilkan",
                sInfoFiltered: " - (disaring dari _MAX_ total barang pusat)"
            },
            ajax:{
                url: "<?php echo site_url('barang_pusat/ajax_list_periksa_stok')?>",
                type: "POST",
                data: {'id_periode_stok_pusat' : $('#pilih_periode_stok_pusat').val()}
            },
            columnDefs: [
                            { 
                                "targets": [ -1 ],
                                "orderable": false,
                            },
                        ],
            scrollX: true,
            deferRender: true,
            scrollY: 650,
            scrollCollapse: true,
            scroller: true
        });

        $('#TablePeriksaStok').css("width", "100%");
        // Akhir table periksa stok barang
    }

    $(document).ready(function() {
        $('#JudulHalaman').html('Barang Pusat - VAMA');
        $("#wrapper").removeClass('forced');
        $("#wrapper").addClass('forced enlarged');

        status_tampil = '0';
        TampilkanMasterBarang();

        // Awal fungsi lihat foto
        $('.parent-container').magnificPopup({
            delegate: 'a[name="foto"]',
            type: 'image',
            closeOnContentClick: true,
            mainClass: 'mfp-fade',
            gallery: {
                enabled: true,
                navigateByImgClick: true,
                preload: [0,1]
            }
        });
        // Akhir fungsi lihat foto     

        var _swal = window.swal;
        window.swal = function(){
            var previousWindowKeyDown = window.onkeydown;
            _swal.apply(this, Array.prototype.slice.call(arguments, 0));
            window.onkeydown = previousWindowKeyDown;
        };
        
        // Awal deklarasi select row
        $('#TableMasterBarang tbody').on( 'click', 'tr', function(){
            $(this).toggleClass('selected');
        });

        $('#TableDaftarTransaksi tbody').on( 'click', 'tr', function(){
            $(this).toggleClass('selected');
        });

        $('#TableDaftarTransaksiRusak tbody').on( 'click', 'tr', function(){
            $(this).toggleClass('selected');
        });

        $('#TablePembaharuanBarang tbody').on( 'click', 'tr', function(){
            $(this).toggleClass('selected');
        });

        $('#TablePeriksaStok tbody').on( 'click', 'tr', function(){
            $(this).toggleClass('selected');
        });
        // Akhir deklarasi select row

        $("input").change(function(){
            $(this).parent().parent().parent().removeClass('has-error');
            $(this).parent().parent().parent().removeClass('has-warning');
            $(this).next().empty();
        });

        $("input-group").change(function(){
            $(this).parent().parent().removeClass('has-error');
            $(this).next().empty();
        });

        $("textarea").change(function(){
            $(this).parent().parent().parent().removeClass('has-error');
            $(this).next().empty();
        });

        $(document).on('keyup', '#harga_modal', function(){
            var hrg_mod     = $('#harga_modal').val();
            hrg_mod         = to_angka(hrg_mod);

            if(hrg_mod > 0){
                $('#harga_modal').val(to_rupiah(hrg_mod));
                $('#harga_modal_hidden').val(hrg_mod);
            }else{
                $('#harga_modal').val('');
                $('#harga_modal_hidden').val('0');
            }
             
            HitungModalBersih();
            // HitungHargaEceran();
        });

        $(document).on('keyup', '#resistensi_harga_modal_persen', function(){
            HitungModalBersih();
            // HitungHargaEceran();
            // HitungResistensiHargaEceran();
        });

        $(document).on('keyup', '#resistensi_harga_modal', function(){
            var res_hrg_mod     = $('#resistensi_harga_modal').val();
            res_hrg_mod         = to_angka(res_hrg_mod);

            if(res_hrg_mod > 0){
                $('#resistensi_harga_modal').val(to_rupiah(res_hrg_mod));
                $('#resistensi_harga_modal_hidden').val(res_hrg_mod);
            }else{
                $('#resistensi_harga_modal').val('');
                $('#resistensi_harga_modal_hidden').val('0');
            }
             
            HitungPersenDanHargaModalBersih();
            // HitungHargaEceran();
            // HitungResistensiHargaEceran();
        });

        $(document).on('keyup', '#harga_modal_bersih', function(){
            var hrg_mod_ber     = $('#harga_modal_bersih').val();
            hrg_mod_ber         = to_angka(hrg_mod_ber);

            if(hrg_mod_ber > 0){
                $('#harga_modal_bersih').val(to_rupiah(hrg_mod_ber));
                $('#harga_modal_bersih_hidden').val(hrg_mod_ber);
            }else{
                $('#harga_modal_bersih').val('');
                $('#harga_modal_bersih_hidden').val('0');
            }
             
            HitungResistensiHargaModalBersih();
            // HitungHargaEceran();
        });

        $(document).on('keyup', '#resistensi_harga_eceran_persen', function(){
            HitungHargaEceran();
        });

        $(document).on('keyup', '#resistensi_harga_eceran', function(e){
            var charCode = e.which || e.keyCode;
            if(charCode == 13){
                $('#harga_eceran').focus();
            }else{
                var RE   = $('#resistensi_harga_eceran').val();
                    RE   = to_angka(RE);
                    
                    if(RE > 0){
                        $('#resistensi_harga_eceran').val(to_rupiah(RE));
                        $('#resistensi_harga_eceran_hidden').val(RE);
                    }else{
                        $('#resistensi_harga_eceran').val('');
                        $('#resistensi_harga_eceran_hidden').val('0');
                    }

                HitungPersenDanHargaEceran();
            }
        });

        $(document).on('keyup', '#harga_eceran_pusat', function(e){
            var charCode = e.which || e.keyCode;
            if(charCode == 13){
                $('#harga_grosir1_pusat').focus();
            }else{
                var HE   = $('#harga_eceran_pusat').val();
                    HE   = to_angka(HE);
                    
                    if(HE > 0){
                        $('#harga_eceran_pusat').val(to_rupiah(HE));
                        $('#harga_eceran_pusat_hidden').val(HE);
                    }else{
                        $('#harga_eceran_pusat').val('');
                        $('#harga_eceran_pusat_hidden').val('0');
                    }

                HitungResistensiHargaEceran();
            }
        });

        $(document).on('keyup', '#harga_grosir1_pusat', function(e){
            var charCode = e.which || e.keyCode;
            if(charCode == 13){
                $('#harga_grosir2_pusat').focus();
            }else{
                var hrg_gros1   = $('#harga_grosir1_pusat').val();
                    hrg_gros1   = to_angka(hrg_gros1);
                
                if(hrg_gros1 > 0){
                    $('#harga_grosir1_pusat').val(to_rupiah(hrg_gros1));
                    $('#harga_grosir1_pusat_hidden').val(hrg_gros1);
                }else{
                    $('#harga_grosir1_pusat').val('');
                    $('#harga_grosir1_pusat_hidden').val('0');
                }
            }
        });

        $(document).on('keyup', '#harga_grosir2_pusat', function(e){
            var charCode = e.which || e.keyCode;
            if(charCode == 13){
                $('#btnSave').focus();
            }else{
                var hrg_gros2   = $('#harga_grosir2_pusat').val();
                    hrg_gros2   = to_angka(hrg_gros2);
                
                if (hrg_gros2>0){
                    $('#harga_grosir2_pusat').val(to_rupiah(hrg_gros2));
                    $('#harga_grosir2_pusat_hidden').val(hrg_gros2);
                }else{
                    $('#harga_grosir2_pusat').val('');
                    $('#harga_grosir2_pusat_hidden').val('0');
                }
            }
        });  
    });
    
    $(document).on('keydown', 'body', function(e){
        var charCode = ( e.which ) ? e.which : event.keyCode;
        if(charCode == 118) //F7
        {
            refresh_data();
        }

        <?php if($this->session->userdata('usergroup_name') == 'Super Admin' || $this->session->userdata('usergroup_name') == 'Admin'){ ?>
            if(charCode == 119) //F8
            {
                swal({
                    title: pesan_tampil,
                    type: "info",
                    showCancelButton: true,
                    confirmButtonClass: 'btn-primary btn-md waves-effect waves-light',
                    confirmButtonText: "Iya, saya yakin",
                    cancelButtonText: "Batal",
                },function (isConfirm){
                    if(isConfirm){
                        if(status_tampil == '0'){
                            status_tampil = '1';
                            pesan_tampil  = 'Yakin ingin menyembunyikan detail ?';
                            TampilkanMasterBarang();
                        }else{
                            status_tampil = '0';
                            pesan_tampil  = 'Yakin ingin menampilkan detail ?';
                            TampilkanMasterBarang();
                        }
                    }
                });
                return false;
            }
        <?php } ?>
    });

    $(document).on('change', '#pilih_periode_stok_pusat', function(){
        if(id_tab == 1){
            TampilkanMasterBarang();
        }else if(id_tab == 2){
            TampilkanDaftarTransaksi();
        }else if(id_tab == 3){
            TampilkanDaftarTransaksiRusak();
        }else if(id_tab == 4){
            TampilkanPembaharuanBarang();
        }
    });

    $(document).on('click', '#tab_induk', function(){
        id_tab = 1;
        TampilkanMasterBarang();
    });

    $(document).on('click', '#tab_transaksi', function(){
        id_tab = 2;
        TampilkanDaftarTransaksi();
    });

    $(document).on('click', '#tab_transaksi_rusak', function(){
        id_tab = 3;
        TampilkanDaftarTransaksiRusak();
    });

    $(document).on('click', '#tab_pembaharuan', function(){
        id_tab = 4;
        TampilkanPembaharuanBarang();
    });

    $(document).on('click', '#tab_periksa_stok', function(){
        id_tab = 5;
        TampilkanDaftarPeriksaStok();
    });

    function refresh_data()
    {
        if(id_tab == 1){
            // table_master_barang.ajax.reload(null, false); 
            TampilkanMasterBarang();
        }else if(id_tab == 2){
            // table_daftar_transaksi.ajax.reload(null, false);
            TampilkanDaftarTransaksi();
        }else if(id_tab == 3){
            // table_daftar_transaksi_rusak.ajax.reload(null, false);
            TampilkanDaftarTransaksiRusak();
        }else if(id_tab == 4){
            // table_pembaharuan_barang.ajax.reload(null, false);
            TampilkanPembaharuanBarang();
        }else if(id_tab == 5){
            // table_pembaharuan_barang.ajax.reload(null, false);
            TampilkanDaftarPeriksaStok();
        }
    }

    function fix_stok()
    {
        $('#icon_fix').removeClass('fa-spin fa-refresh');
        $('#icon_fix').addClass('fa-spin fa-refresh');

        $.ajax({
            url : "<?php echo site_url('barang_pusat/ajax_fix_stok'); ?>",
            type: "POST",
            cache: false,
            data: 'id_periode_stok_pusat=' + $('#pilih_periode_stok_pusat').val(),
            dataType: "JSON",
            success: function(data){
                if(data.status == 1){
                    swal({
                        title: "Berhasil!", 
                        text: "Data stok barang pusat berhasil di perbaiki.", 
                        type: "success", 
                        confirmButtonText: "Ok"
                    });
                    refresh_data();
                    $('#icon_fix').removeClass('fa-spin fa-refresh');
                }else if(data.status == 0){
                   swal({
                        title: "Gagal!", 
                        text: "Tidak ada stok barang pusat untuk di perbaiki.", 
                        type: "error", 
                        confirmButtonClass: 'btn-danger btn-md waves-effect waves-light',
                        confirmButtonText: "Ok"
                    }, function(isConfirm){
                        $('#icon_fix').removeClass('fa-spin fa-refresh');
                    });
                }
            },
            error: function (jqXHR, textStatus, errorThrown){
                swal({
                    title: "Gagal!", 
                    text: "Data stok barang pusat gagal di perbaiki.", 
                    type: "error", 
                    confirmButtonClass: 'btn-danger btn-md waves-effect waves-light',
                    confirmButtonText: "Ok"
                }, function(isConfirm){
                    $('#icon_fix').removeClass('fa-spin fa-refresh');
                });
            }
        });
    }

    function import_barang_pusat()
    {
        $('#icon_import').removeClass('fa-refresh');
        $('#icon_import').addClass('fa-spin fa-refresh');

        $.ajax({
            url : "<?php echo site_url('barang_pusat/ajax_import_barang_pusat'); ?>",
            type: "POST",
            cache: false,
            dataType: "JSON",
            success: function(data){
                if(data.status == 1){
                    reload_table_master_barang();
                    swal({
                        title: "Berhasil!", 
                        text: data.pesan, 
                        type: "success", 
                        confirmButtonText: "Ok"
                    });
                }else if(data.status == 0){
                   swal({
                        title: "Gagal!", 
                        text: data.pesan, 
                        type: "error", 
                        confirmButtonClass: 'btn-danger btn-md waves-effect waves-light',
                        confirmButtonText: "Ok"
                    }, function(isConfirm){
                        window.location.href = "barang_pusat";
                    });
                }
            },
            error: function (jqXHR, textStatus, errorThrown){
                swal({
                    title: "Gagal!", 
                    text: "Data barang pusat gagal di import.", 
                    type: "error", 
                    confirmButtonClass: 'btn-danger btn-md waves-effect waves-light',
                    confirmButtonText: "Ok"
                }, function(isConfirm){
                    window.location.href = "barang_pusat";
                });
            }
        });
        $('#icon_import').removeClass('fa-spin fa-refresh');
        $('#icon_import').addClass('fa-refresh');
    }

    function reload_table_master_barang()
    {
        table_master_barang.ajax.reload(null,false); 
    }

    function reload_table_daftar_transaksi()
    {
        table_daftar_transaksi.ajax.reload(null,false);
    }

    function reload_table_daftar_transaksi_rusak()
    {
        table_daftar_transaksi_rusak.ajax.reload(null,false); 
    }

    function reload_table_pembaharuan_barang()
    {
        table_pembaharuan_barang.ajax.reload(null,false);
    }

    function reload_table_periksa_stok()
    {
        table_daftar_periksa_stok.ajax.reload(null,false);
    }

    $(document).on('click', '#so', function(){
        default_tombol();
        $('#so').removeClass('btn-default');
        $('#so').addClass('btn-primary');

        id_barang_pusat = $('#id_barang_pusat_detail').html();
        jenis_transaksi = 'so';
        $('#judul_transaksi').html("Transaksi Stok Opname");
        TampilkanDetailTransaksi();
    });

    $(document).on('click', '#pb', function(){
        default_tombol();
        $('#pb').removeClass('btn-default');
        $('#pb').addClass('btn-primary');

        id_barang_pusat = $('#id_barang_pusat_detail').html();
        jenis_transaksi = 'pb';
        $('#judul_transaksi').html("Transaksi Pembelian");
        TampilkanDetailTransaksi();
    });

    $(document).on('click', '#dp', function(){
        default_tombol();
        $('#dp').removeClass('btn-default');
        $('#dp').addClass('btn-primary');

        id_barang_pusat = $('#id_barang_pusat_detail').html();
        jenis_transaksi = 'dp';
        $('#judul_transaksi').html("Transaksi Dari Perakitan");
        TampilkanDetailTransaksi();
    });

    $(document).on('click', '#rpt', function(){
        default_tombol();
        $('#rpt').removeClass('btn-default');
        $('#rpt').addClass('btn-primary');

        id_barang_pusat = $('#id_barang_pusat_detail').html();
        jenis_transaksi = 'rpt';
        $('#judul_transaksi').html("Transaksi Retur Penjualan Tidak Rusak / Masuk Stok Jual");
        TampilkanDetailTransaksi();
    });
    
    $(document).on('click', '#rbdtt', function(){
        default_tombol();
        $('#rbdtt').removeClass('btn-default');
        $('#rbdtt').addClass('btn-primary');

        id_barang_pusat = $('#id_barang_pusat_detail').html();
        jenis_transaksi = 'rbdtt';
        $('#judul_transaksi').html("Transaksi Retur Dari Toko Tidak Rusak / Masuk Stok Jual");
        TampilkanDetailTransaksi();
    });

    $(document).on('click', '#up', function(){
        default_tombol();
        $('#up').removeClass('btn-default');
        $('#up').addClass('btn-danger');

        id_barang_pusat = $('#id_barang_pusat_detail').html();
        jenis_transaksi = 'up';
        $('#judul_transaksi').html("Transaksi Untuk Perakitan / Komponen Perakitan");
        TampilkanDetailTransaksi();
    });

    $(document).on('click', '#pju', function(){
        default_tombol();
        $('#pju').removeClass('btn-default');
        $('#pju').addClass('btn-danger');

        id_barang_pusat = $('#id_barang_pusat_detail').html();
        jenis_transaksi = 'pju';
        $('#judul_transaksi').html("Transaksi Penjualan Umum / Grosir");
        TampilkanDetailTransaksi();
    });

    $(document).on('click', '#pjt', function(){
        default_tombol();
        $('#pjt').removeClass('btn-default');
        $('#pjt').addClass('btn-danger');

        id_barang_pusat = $('#id_barang_pusat_detail').html();
        jenis_transaksi = 'pjt';
        $('#judul_transaksi').html("Transaksi Penjualan Ke Toko");
        TampilkanDetailTransaksi();
    });

    $(document).on('click', '#rpbt', function(){
        default_tombol();
        $('#rpbt').removeClass('btn-default');
        $('#rpbt').addClass('btn-danger');

        id_barang_pusat = $('#id_barang_pusat_detail').html();
        jenis_transaksi = 'rpbt';
        $('#judul_transaksi').html("Transaksi Retur Pembelian Tidak Rusak / Dari Stok Jual");
        TampilkanDetailTransaksi();
    });

    $(document).on('click', '#hd', function(){
        default_tombol();
        $('#hd').removeClass('btn-default');
        $('#hd').addClass('btn-danger');

        id_barang_pusat = $('#id_barang_pusat_detail').html();
        jenis_transaksi = 'hd';
        $('#judul_transaksi').html("Transaksi Hadiah");
        TampilkanDetailTransaksi();
    });

    $(document).on('click', '#rpr', function(){
        default_tombol();
        $('#rpr').removeClass('btn-default');
        $('#rpr').addClass('btn-primary');

        id_barang_pusat = $('#id_barang_pusat_detail').html();
        jenis_transaksi = 'rpr';
        $('#judul_transaksi').html("Transaksi Retur Penjualan Rusak / Masuk Stok Rusak");
        TampilkanDetailTransaksi();
    });

    $(document).on('click', '#rbdttr', function(){
        default_tombol();
        $('#rbdttr').removeClass('btn-default');
        $('#rbdttr').addClass('btn-primary');

        id_barang_pusat = $('#id_barang_pusat_detail').html();
        jenis_transaksi = 'rbdttr';
        $('#judul_transaksi').html("Transaksi Retur Dari Toko Rusak / Masuk Stok Rusak");
        TampilkanDetailTransaksi();
    });

    $(document).on('click', '#rpbr', function(){
        default_tombol();
        $('#rpbr').removeClass('btn-default');
        $('#rpbr').addClass('btn-danger');

        id_barang_pusat = $('#id_barang_pusat_detail').html();
        jenis_transaksi = 'rpbr';
        $('#judul_transaksi').html("Transaksi Retur Pembelian Rusak / Kurangi Stok Rusak");
        TampilkanDetailTransaksi();
    });

    function default_tombol(){
        // Awal stok jual
        // -------------------------------------
        $('#so').removeClass('btn-primary');
        $('#pb').removeClass('btn-primary');
        $('#dp').removeClass('btn-primary');
        $('#rpt').removeClass('btn-primary');
        $('#rbdtt').removeClass('btn-primary');
        $('#up').removeClass('btn-danger');
        $('#pju').removeClass('btn-danger');
        $('#pjt').removeClass('btn-danger');
        $('#rpbt').removeClass('btn-danger');
        $('#pp').removeClass('btn-danger');
        $('#hd').removeClass('btn-danger');
        $('#ps').removeClass('btn-danger');

        $('#so').addClass('btn-default');
        $('#pb').addClass('btn-default');
        $('#dp').addClass('btn-default');
        $('#rpt').addClass('btn-default');
        $('#rbdtt').addClass('btn-default');
        $('#up').addClass('btn-default');
        $('#pju').addClass('btn-default');
        $('#pjt').addClass('btn-default');
        $('#rpbt').addClass('btn-default');
        $('#pp').addClass('btn-default');
        $('#hd').addClass('btn-default');
        $('#ps').addClass('btn-default');
        // Akhir stok jual
        // -------------------------------------

        // Awal stok jual
        // -------------------------------------
        $('#rpr').removeClass('btn-primary');
        $('#rbdtr').removeClass('btn-primary');
        $('#ps').removeClass('btn-primary');
        $('#rpbr').removeClass('btn-danger');

        $('#rpr').addClass('btn-default');
        $('#rbdtr').removeClass('btn-default');
        $('#ps').removeClass('btn-default');
        $('#rpbr').removeClass('btn-default');
        // Akhir stok jual
        // -------------------------------------        
    }

    function to_rupiah(angka)
    {
        var rev  = parseInt(angka, 10).toString().split('').reverse().join('');
        var rev2 = '';
        for(var i = 0; i < rev.length; i++){
            rev2  += rev[i];
            if((i + 1) % 3 === 0 && i !== (rev.length - 1)){
                rev2 += '.';
            }
        }
        return 'Rp. ' + rev2.split('').reverse().join('');
    }

    function to_angka(rp)
    {
        return parseInt(rp.replace(/,.*|\D/g,''),10)
    }

    function HitungModalBersih()
    {
        var Mod            = $('#harga_modal_hidden').val();
        var Persen_Res_Mod = $('#resistensi_harga_modal_persen').val();
        var Res_Mod        = parseInt(Mod) * Persen_Res_Mod;
        var Mod_Ber        = parseInt(Mod) + parseInt(Res_Mod);

        if(Res_Mod>0){
            $('#resistensi_harga_modal').val(to_rupiah(Res_Mod));
            $('#resistensi_harga_modal_hidden').val(Res_Mod);
        }else{
            $('#resistensi_harga_modal').val(to_rupiah('0'));
            $('#resistensi_harga_modal_hidden').val('0');
        }

        if(Mod_Ber>0){
            $('#harga_modal_bersih_hidden').val(Mod_Ber);
            $('#harga_modal_bersih').val(to_rupiah(Mod_Ber));
        }else{
            $('#harga_modal_bersih_hidden').val('');
            $('#harga_modal_bersih').val('0');        
        }
    }

    function HitungPersenDanHargaModalBersih(){
        var Mod              = $('#harga_modal_hidden').val();
        var Res_Hrg_Modal    = $('#resistensi_harga_modal_hidden').val();
        var Mod_Ber          = parseInt(Mod) + parseInt(Res_Hrg_Modal);
        var Persen_Res_Modal = parseInt(Res_Hrg_Modal) / parseInt(Mod);

        if(Mod_Ber>0){
            $('#harga_modal_bersih').val(to_rupiah(Mod_Ber));        
            $('#harga_modal_bersih_hidden').val(Mod_Ber);        
        }else{
            $('#harga_modal_bersih_hidden').val(to_rupiah(Mod_Ber));        
            $('#harga_modal_bersih_hidden').val(Mod_Ber);        
        }

        if(Persen_Res_Modal>0){
            $('#resistensi_harga_modal_persen').val(Persen_Res_Modal);
        }else{
            $('#resistensi_harga_modal_persen').val('0.00');
        }
    }

    function HitungResistensiHargaModalBersih()
    {
        var Mod        = $('#harga_modal_hidden').val();
        var Mod_Ber    = $('#harga_modal_bersih_hidden').val();
        var Res_Mod    = parseInt(Mod_Ber) - parseInt(Mod);
        var Persen_Mod = parseInt(Res_Mod) / parseInt(Mod);

        if(Res_Mod > 0){
            $('#resistensi_harga_modal').val(to_rupiah(Res_Mod));        
            $('#resistensi_harga_modal_hidden').val(Res_Mod);        
        }else{
            $('#resistensi_harga_modal').val('Rp. 0');        
            $('#resistensi_harga_modal_hidden').val('0');        
        }

        if(Persen_Mod>0){
            $('#resistensi_harga_modal_persen').val(Persen_Mod);
        }else{
            $('#resistensi_harga_modal_persen').val('0.00');
        }
    }

    function HitungHargaEceran()
    {
        var Mod_Ber = $('#harga_modal_bersih_hidden').val();
        if(Mod_Ber <= 0){
            Mod_Ber = 0;
        }

        var Persen_Res_Ecer = $('#resistensi_harga_eceran_persen').val();
        var Res_Hrg_Ecer    = parseInt(Mod_Ber) * Persen_Res_Ecer;
        var Hrg_Ecer        = parseInt(Mod_Ber) + parseInt(Res_Hrg_Ecer);

        if(Res_Hrg_Ecer > 0){
            
        }else{
            $('#resistensi_harga_eceran').val('Rp. 0');        
            $('#resistensi_harga_eceran_hidden').val('0');
        }

        if (Hrg_Ecer > 0){
            $('#resistensi_harga_eceran').val(to_rupiah(Res_Hrg_Ecer));        
            $('#resistensi_harga_eceran_hidden').val(parseInt(Res_Hrg_Ecer));
            $('#harga_eceran_pusat').val(to_rupiah(Hrg_Ecer));        
            $('#harga_eceran_pusat_hidden').val(Hrg_Ecer);        
        }else{
            $('#harga_eceran_pusat').val(to_rupiah(Mod_Ber));                
            $('#harga_eceran_pusat_hidden').val(Mod_Ber);        
        }
    }

    function HitungPersenDanHargaEceran(){
        var Mod_Ber         = $('#harga_modal_bersih_hidden').val();
        var Res_Hrg_Ecer    = $('#resistensi_harga_eceran_hidden').val();
        var Hrg_Ecer        = parseInt(Mod_Ber) + parseInt(Res_Hrg_Ecer);
        var Persen_Res_Ecer = parseInt(Res_Hrg_Ecer) / parseInt(Mod_Ber);

        if(Res_Hrg_Ecer > 0){
                   
        }else{
            $('#harga_eceran_pusat').val(to_rupiah(Mod_Ber));        
            $('#harga_eceran_pusat_hidden').val(Mod_Ber);        
        }

        if(Persen_Res_Ecer > 0){
            $('#harga_eceran_pusat').val(to_rupiah(Hrg_Ecer));        
            $('#harga_eceran_pusat_hidden').val(Hrg_Ecer); 
            $('#resistensi_harga_eceran_persen').val(Persen_Res_Ecer);
        }else{
            $('#resistensi_harga_eceran_persen').val('0.00');
        }
    }

    function HitungResistensiHargaEceran()
    {
        var Mod_Ber         = $('#harga_modal_bersih_hidden').val();
        var Hrg_Ecer        = $('#harga_eceran_pusat_hidden').val();
        var Res_Hrg_Ecer    = parseInt(Hrg_Ecer) - parseInt(Mod_Ber);
        var Persen_Res_Ecer = parseInt(Res_Hrg_Ecer) / parseInt(Mod_Ber);

        if (Res_Hrg_Ecer > 0){
                 
        }else{
            $('#resistensi_harga_eceran').val('Rp. 0');        
            $('#resistensi_harga_eceran_hidden').val('0');        
        }

        if(Persen_Res_Ecer > 0){
            $('#resistensi_harga_eceran').val(to_rupiah(Res_Hrg_Ecer));        
            $('#resistensi_harga_eceran_hidden').val(Res_Hrg_Ecer);   
            $('#resistensi_harga_eceran_persen').val(Persen_Res_Ecer);
        }else{
            $('#resistensi_harga_eceran_persen').val('0.00');
        }
    }

    function add_barang_pusat()
    {
        default_pencarian();
        $('#pilih_supplier').attr('disabled',false).html('');
        $('#pilih_jenis_barang').attr('disabled',false).html('');
        $('#pilih_kategori_barang').attr('disabled',false).html('');        
                
        $('#baris_kiri').removeClass('col-md-4');
        $('#baris_kiri').addClass('col-md-6');
        $('#baris_kanan').removeClass('col-md-2');

        save_method = 'add';
        $('#foto_barang').html('');
        $('#form')[0].reset();
        $('.form-horizontal').removeClass('has-error');
        $('.help-block').empty();

        $('#tab_foto_barang').removeClass('active');
        $('#tab_barang_pusat').addClass('active');
        $('#tab_barang').hide();
        $('#kode_barang').val('#');
        
        $('.modal-dialog').removeClass('modal-sm');
        $('.modal-dialog').removeClass('modal-md');
        $('.modal-dialog').removeClass('modal-lg');
        $('.modal-dialog').addClass('modal-full');
        $('#modal_form').modal('show');
        $('.modal-title').text('Tambah Barang Pusat');
    }

    function default_pencarian(){
        $('#induk_pencarian_jenis_barang').removeClass('has-warning');
        $('#induk_pencarian_jenis_barang').addClass('has-succes');

        $('#icon_pencarian_jenis_barang').removeClass('fa-spin fa-refresh');
        $('#icon_pencarian_jenis_barang').addClass('fa-search');

        $('#tombol_scan_jenis_barang').removeClass('btn-warning');
        $('#tombol_scan_jenis_barang').addClass('btn-primary');
        $('#tombol_scan_jenis_barang').prop('disabled', true);
        // --------------------------------------------------------------

        $('#induk_pencarian_kategori_barang').removeClass('has-warning');
        $('#induk_pencarian_kategori_barang').addClass('has-succes');

        $('#icon_pencarian_kategori_barang').removeClass('fa-spin fa-refresh');
        $('#icon_pencarian_kategori_barang').addClass('fa-search');

        $('#tombol_scan_kategori_barang').removeClass('btn-warning');
        $('#tombol_scan_kategori_barang').addClass('btn-primary');
        $('#tombol_scan_kategori_barang').prop('disabled', true);
    }

    function edit_barang_pusat(id_barang_pusat)
    {
        default_pencarian();
        $('#hasil_pencarian_jenis_barang').hide();
        $('#hasil_pencarian_kategori_barang').hide();

        $('#baris_kiri').removeClass('col-md-6');
        $('#baris_kiri').addClass('col-md-4');
        $('#baris_kanan').addClass('col-md-2');

        save_method = 'update';
        $('#form')[0].reset();
        // $('#formBarangToko')[0].reset();
        $('.form-horizontal').removeClass('has-error');
        $('.help-block').empty();  

        $('#tab_barang li').removeClass('active'); 
        $('#tab_barang li').removeClass('hidden');
        // $('#tab_barang_toko').removeClass('active'); 
        $('#tab_foto_barang').removeClass('active'); 

        $('#tab_barang li:first').addClass('active'); 
        $('#tab_barang_pusat').addClass('active');
        $('#tab_barang').show();

        $.ajax({
            url : "<?php echo site_url('barang_pusat/ajax_edit'); ?>",
            type: "POST",
            cache: false,
            data: 'id_barang_pusat=' +  id_barang_pusat,
            dataType: "json",                
            success: function(json){
                $('[name="id_barang_pusat"]').val(json.data.id_barang_pusat);
                $('[name="kode_barang"]').val(json.data.kode_barang);
                $('[name="sku"]').val(json.data.sku);

                $('#pilih_supplier').html('').attr('disabled',true);
                $('#pilih_supplier').append(
                    "<option value='" + json.data.kode_supplier + "'>"
                    +json.data.nama_supplier
                    +"</option>"
                );

                $('#pilih_jenis_barang').html('').attr('disabled',true);
                $('#pilih_jenis_barang').append(
                    "<option value='" + json.data.kode_jenis + "'>"
                    +json.data.nama_jenis
                    +"</option>"
                );

                $('#pilih_kategori_barang').html('').attr('disabled',true);
                $('#pilih_kategori_barang').append(
                    "<option value='" + json.data.kode_kategori + "'>"
                    +json.data.nama_kategori
                    +"</option>"
                );

                $('[name="foto_barang"]').html(json.foto);
                $('[name="nama_barang"]').val(json.data.nama_barang);
                $('[name="harga_modal"]').val(to_rupiah(json.data.modal));
                $('[name="harga_modal_hidden"]').val(json.data.modal);
                $('[name="resistensi_harga_modal_persen"]').val(json.data.resistensi_modal);
                $('[name="harga_modal_bersih"]').val(to_rupiah(json.data.modal_bersih));
                $('[name="harga_modal_bersih_hidden"]').val(json.data.modal_bersih);
                $('[name="resistensi_harga_eceran_persen"]').val(json.data.resistensi_harga_eceran);
                $('[name="harga_eceran_pusat"]').val(to_rupiah(json.data.harga_eceran));
                $('[name="harga_eceran_pusat_hidden"]').val(json.data.harga_eceran);            
                $('[name="harga_grosir1_pusat"]').val(to_rupiah(json.data.harga_grosir1));
                $('[name="harga_grosir1_pusat_hidden"]').val(json.data.harga_grosir1);
                $('[name="harga_grosir2_pusat"]').val(to_rupiah(json.data.harga_grosir2));
                $('[name="harga_grosir2_pusat_hidden"]').val(json.data.harga_grosir2);
                $('[name="status"]').val(json.data.status);
                $('[name="status2"]').val(json.data.status2);
                
                $('#modal_form').modal('show');
                $('.modal-dialog').removeClass('modal-sm');
                $('.modal-dialog').removeClass('modal-md');
                $('.modal-dialog').removeClass('modal-lg');
                $('.modal-dialog').addClass('modal-full');
                $('.modal-title').text('Edit Barang : ' + json.data.sku + ' - '+ json.data.nama_barang);

                $('#tombol_scan_supplier').attr('disabled',true);
                $('#tombol_scan_jenis_barang').attr('disabled',true);
                $('#tombol_scan_kategori_barang').attr('disabled',true);

                // HitungModalBersih();
                // HitungHargaEceran();
            },
            error: function (jqXHR, textStatus, errorThrown){
                swal({
                    title: "Gagal mengambil data barang pusat !",
                    text: "Harap hubungi administrator pusat!",
                    type: "warning",
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Ok, saya mengerti",
                },function (isConfirm) {
                    if(isConfirm){
                        window.location.href="barang_pusat";
                    }
                });
            }
        });
    }

    function TampilkanHargaEceranToko()
    {
        $.ajax({
            url : "<?php echo site_url('barang_pusat/ajax_harga_eceran_toko'); ?>",
            type: "POST",
            cache: false,
            data: 'id_barang_pusat=' + $('#id_barang_pusat_ubah').html() +
                  '&kode_toko=' + $('#kode_toko').val(),
            dataType: "json",                
            success: function(json){
                if(json.status == '1'){
                    $('#harga_eceran_toko').val(to_rupiah(json.eceran_toko));
                    $('#harga_eceran_toko_hidden').val(json.eceran_toko);
                }else if(json.status == '0'){
                    swal({
                        title: "Gagal!",
                        text: json.pesan,
                        type: "warning",
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Ok, saya mengerti",
                    },function (isConfirm) {
                        if(isConfirm){
                            $('#kode_toko').val('').focus();
                            $('#harga_eceran_toko').val('0');
                        }
                    });
                }
            },
            error: function (jqXHR, textStatus, errorThrown){
                swal({
                    title: "Gagal mengambil data barang pusat !",
                    text: "Harap hubungi administrator pusat!",
                    type: "warning",
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Ok, saya mengerti",
                },function (isConfirm) {
                    if(isConfirm){
                        $('#kode_toko').val('').focus();
                        $('#harga_eceran_toko').val('0');
                    }
                });
            }
        });
    }

    function save_eceran_toko()
    {
        $.ajax({
            url : "<?php echo site_url('barang_pusat/ajax_update_harga_eceran_toko'); ?>",
            type: "POST",
            cache: false,
            data: 'id_barang_pusat=' + $('#id_barang_pusat_ubah').html() +
                  '&kode_toko=' + $('#kode_toko').val() + 
                  '&harga_eceran=' + $('#harga_eceran_toko_hidden').val(),
            dataType: "json",                
            success: function(json){
                if(json.status == '1'){
                    swal({
                        title: "Berhasil !",
                        text: "Harga eceran toko berhasil di simpan.",
                        type: "success",
                        confirmButtonText: "Ok, saya mengerti",
                    }, function (isConfirm) {
                        $('#kode_toko').val('').focus();
                        $('#harga_eceran_toko').val('0');
                    });
                }else if(json.status == '0'){
                    swal({
                        title: "Gagal !",
                        text: json.pesan,
                        type: "warning",
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Ok, saya mengerti",
                    }, function (isConfirm) {
                        $('#kode_toko').focus();
                    });
                }
            },
            error: function (jqXHR, textStatus, errorThrown){
                swal({
                    title: "Gagal menyimpan eceran toko!",
                    text: "Harap hubungi administrator pusat!",
                    type: "warning",
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Ok, saya mengerti",
                },function (isConfirm) {
                    if(isConfirm){
                        window.location.href="barang_pusat";
                    }
                });
            }
        });
    }

    function detail_transaksi(id_barang_pusat){
        $.ajax({
            url : "<?php echo site_url('barang_pusat/detail_transaksi'); ?>",
            type: "POST",
            cache: false,
            data: 'id_barang_pusat=' +  id_barang_pusat +
                  '&id_periode_stok_pusat=' + $('#pilih_periode_stok_pusat').val(),
            dataType: "json",                
            success: function(json){
                $('#id_barang_pusat_detail').html(json.data_stok_jual['id_barang_pusat']);
                $('#sku').html(json.data_stok_jual['sku']);
                $('#kode_barang').html(json.data_stok_jual['kode_barang']);
                $('#harga_eceran').html(to_rupiah(json.data_stok_jual['harga_eceran']));
                $('#harga_grosir1').html(to_rupiah(json.data_stok_jual['harga_grosir1']));
                $('#harga_grosir2').html(to_rupiah(json.data_stok_jual['harga_grosir2']));
                $('#kode_periode_stok_pusat').html(json.kode_periode_stok_pusat);
                $('#tanggal_periode_awal').html(json.tanggal_periode_awal);
                $('#tanggal_periode_akhir').html(json.tanggal_periode_akhir);
                $('#foto_barang_kecil').html(json.foto);

                // Jumlah transaksi
                $('#so').html(json.data_stok_jual['jumlah_so']);
                $('#pb').html(json.data_stok_jual['jumlah_beli']);
                $('#dp').html(json.data_stok_jual['jumlah_perakitan']);
                $('#rpt').html(json.data_stok_jual['jumlah_retur_penjualan']);
                $('#rbdtt').html(json.data_stok_jual['jumlah_retur_toko']);
                $('#up').html(json.data_stok_jual['jumlah_komponen']);
                $('#pju').html(json.data_stok_jual['jumlah_jual_umum']);
                $('#pjt').html(json.data_stok_jual['jumlah_jual_toko']);
                $('#rpbt').html(json.data_stok_jual['jumlah_retur_pembelian']);
                $('#pp').html('0');
                $('#hd').html(json.data_stok_jual['jumlah_hadiah']);
                $('#ps1').html('0');
                $('#stok_jual').html(json.data_stok_jual['stok_jual']);
 
                $('#rpr').html(json.data_stok_rusak['rpr']);
                $('#rbdttr').html(json.data_stok_rusak['rbdtr']);
                $('#ps2').html(json.data_stok_rusak['ps']);
                $('#rpbr').html(json.data_stok_rusak['rpbr']);
                $('#stok_rusak').html(json.data_stok_rusak['stok_rusak']);

                $('.modal-dialog').removeClass('modal-md');
                $('.modal-dialog').addClass('modal-full modal-xl');
                $('#ModalHeaderDetailTransaksi').html('<b class="text-primary">Detail transaksi :</b> <b class="text-dark">' +
                                                 json.data_stok_jual['nama_barang'] +
                                                 '</b>');
                $('#ModalDetailTransaksi').modal('show');
            },
            error: function (jqXHR, textStatus, errorThrown){
                swal({
                    title: "Gagal mengambil data barang pusat !",
                    text: "Harap hubungi administrator pusat!",
                    type: "warning",
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Ok, saya mengerti",
                },function (isConfirm) {
                    if(isConfirm){
                        window.location.href="barang_pusat";
                    }
                });
            }
        });
    }

    function save()
    { 
        var url;
        var judul;
        var pesan;
        if(save_method == 'add'){
            judul   = "Yakin ingin menambahkan barang pusat ?";
            pesan   = "Nama Barang : " + $('#nama_barang').val() + ", " + 
                      "Supplier : " + $('#pilih_supplier').text();
            url     = "<?php echo site_url('barang_pusat/ajax_add')?>";
        }else{
            judul   = "Yakin ingin memperbaharui barang pusat ?";
            pesan   = "Nama Barang : " + $('#nama_barang').val() + ", " + 
                      "Supplier : " + $('#pilih_supplier').text() + ", " +
                      "Jenis: " + $('#pilih_jenis_barang').val() + '-' + $('#pilih_jenis_barang').text() + ', ' +
                      "Kategori: " + $('#pilih_kategori_barang').val() + '-' + $('#pilih_kategori_barang').text();
            url     = "<?php echo site_url('barang_pusat/ajax_update')?>";
        }

        swal({
            title: judul,
            text: pesan,
            type: "info",
            showCancelButton: true,
            confirmButtonClass: 'btn-primary btn-md waves-effect waves-light',
            confirmButtonText: "Iya",
            cancelButtonText: "Batal",
            closeOnConfirm: false
        }, function(isConfirm){
            if(isConfirm){
                $('#btnSave').text('menyimpan...');
                $('#btnSave').attr('disabled', true);
            
                $.ajax({
                    url : url,
                    type: "POST",
                    data: $('#form').serialize(),
                    dataType: "JSON",
                    success: function(data){
                        if(data.status){
                            $('#modal_form').modal('hide');
                            reload_table_master_barang();
                            swal({
                                showCancelButton: true,
                                confirmButtonClass: 'btn-primary btn-md waves-effect waves-light',
                                title: "Berhasil!", 
                                text: "Data barang pusat berhasil disimpan, apakah anda ingin merubah harga eceran toko ?", 
                                type: "success", 
                                confirmButtonText: "Iya, saya ingin",
                                cancelButtonText: "Batal"
                            }, function (isConfirm){
                                if(isConfirm){
                                    $.ajax({
                                        url : "<?php echo site_url('barang_pusat/ajax_edit'); ?>",
                                        type: "POST",
                                        cache: false,
                                        data: 'id_barang_pusat=' + data.id_barang_pusat,
                                        dataType: "json",                
                                        success: function(json){
                                            $('#id_barang_pusat_ubah').html(json.data.id_barang_pusat);
                                            $('#kode_barang_ubah').html(json.data.kode_barang);
                                            $('#sku_ubah').html(json.data.sku);
                                            $('#harga_eceran_ubah').html(to_rupiah(json.data.harga_eceran));
                                            $('#harga_grosir1_ubah').html(to_rupiah(json.data.harga_grosir1));
                                            $('#harga_grosir2_ubah').html(to_rupiah(json.data.harga_grosir2));

                                            // $('#modal_form').modal('show');
                                            $('#ModalUbahHarga').modal('show');
                                            $('.modal-dialog').removeClass('modal-sm');
                                            $('.modal-dialog').removeClass('modal-md');
                                            $('.modal-dialog').removeClass('modal-lg');
                                            $('.modal-dialog').removeClass('modal-full');
                                            $('.modal-dialog').addClass('modal-lg');
                                            $('.modal-title').text('Edit Eceran Toko : ' + json.data.sku + ' - '+ json.data.nama_barang);
                                        },
                                        error: function (jqXHR, textStatus, errorThrown){
                                            swal({
                                                title: "Gagal mengambil data barang pusat !",
                                                text: "Harap hubungi administrator pusat!",
                                                type: "warning",
                                                confirmButtonColor: "#DD6B55",
                                                confirmButtonText: "Ok, saya mengerti",
                                            },function (isConfirm) {
                                                if(isConfirm){
                                                    window.location.href="barang_pusat";
                                                }
                                            });
                                        }
                                    });
                                }
                            });                       
                        }else{
                            for(var i = 0; i < data.inputerror.length; i++){
                                if(data.inputerror[i] == 'nama_barang'){
                                    $('[name="'+data.inputerror[i]+'"]').parent().parent().addClass('has-error');
                                    $('[name="'+data.inputerror[i]+'"]').next().text(data.error_string[i]);
                                }else{
                                    $('[name="'+data.inputerror[i]+'"]').parent().parent().parent().addClass('has-error');
                                    $('[name="'+data.inputerror[i]+'"]').next().next().text(data.error_string[i]);
                                }
                            }
                            swal({
                                title: "Gagal!", 
                                text: "Data barang pusat gagal disimpan.", 
                                type: "error", 
                                confirmButtonClass: 'btn-danger btn-md waves-effect waves-light',
                                confirmButtonText: "Ok"
                            });
                        }
                        $('#btnSave').text('Simpan');
                        $('#btnSave').attr('disabled',false);
                    },
                    error: function (jqXHR, textStatus, errorThrown){
                        swal({
                            title: "Gagal menyimpan atau memperbaharui data barang pusat !",
                            text: "Harap hubungi administrator pusat!",
                            type: "warning",
                            confirmButtonColor: "#DD6B55",
                            confirmButtonText: "Ok, saya mengerti",
                        }, function (isConfirm) {
                            if(isConfirm){
                                window.location.href="barang_pusat";
                            }
                        });
                    }
                });
            }
        });
    }

    function verifikasi_delete(id_barang_pusat)
    {
        $.ajax({
            url : "<?php echo site_url('barang_pusat/ajax_verifikasi_delete') ;?>",
            type: "POST",
            cache: false,
            data: 'id_barang_pusat=' + id_barang_pusat,
            dataType: "JSON",
            success: function(data){
                $('.modal-dialog').removeClass('modal-md');
                $('.modal-dialog').addClass('modal-sm');
                $('#ModalHeader').html('Informasi Hapus Barang');
                $('#ModalContent').html(data.pesan);
                $('#ModalFooter').html(data.footer);
                $('#ModalGue').modal('show');
            },
            error: function (jqXHR, textStatus, errorThrown){
                swal({
                    title: "Gagal !",
                    text: "Data barang pusat gagal untuk ditampilkan",
                    type: "warning",
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Ok, saya mengerti",
                },function (isConfirm) {
                    if (isConfirm){
                        window.location.href="barang_pusat";
                    }
                });
            }
        });
    }

    function delete_barang_pusat(id_barang_pusat)
    {
        $.ajax({
            url : "<?php echo site_url('barang_pusat/ajax_delete'); ?>",
            type: "POST",
            cache: false,
            data: 'id_barang_pusat=' + id_barang_pusat,
            dataType: "JSON",
            success: function(data){
                if(data.status == 1){
                    $('#modal_form').modal('hide');
                    reload_table_master_barang();
                    swal({
                        title: "Berhasil!", 
                        text: "Data barang pusat berhasil dihapus.", 
                        type: "success", 
                        confirmButtonText: "Ok"
                    });
                }else if(data.status == 0){
                    $('#ModalGue').modal('hide');
                    $('.modal-dialog').removeClass('modal-lg');
                    $('.modal-dialog').addClass('modal-sm');
                    $('#ModalHeader').html('Oops !');
                    $('#ModalContent').html(data.pesan);
                    $('#ModalFooter').html("");
                    $('#ModalFooter').html("<button type='button' class='btn btn-primary' data-dismiss='modal'>Ok, Saya Mengerti</button>");
                    $('#ModalGue').modal('show');
                }
            },
            error: function (jqXHR, textStatus, errorThrown){
                swal({
                    title: "Gagal!", 
                    text: "Data barang pusat gagal dihapus.", 
                    type: "error", 
                    confirmButtonClass: 'btn-danger btn-md waves-effect waves-light',
                    confirmButtonText: "Ok"
                }, function(isConfirm){
                    window.location.href = "barang_pusat";
                });
            }
        });
    }

    // Untuk menunda saat menjalankan fungsi ajax di keyup atau change
    var delay = (function () {
        var timer = 0;
        return function (callback, ms) {
            clearTimeout(timer);
            timer = setTimeout(callback, ms);
        };
    })();

    // Awal upload dan hapus foto
    Dropzone.autoDiscover = false;
    var foto_upload= new Dropzone(".dropzone",{
        url : "<?php echo base_url('barang_pusat/proses_upload') ?>",
        maxFilesize: 2,
        maxFiles: 12,
        method:"post",
        acceptedFiles:"image/*,.jpg,.png,.jpeg",
        paramName:"userfile",
        dictInvalidFileType:"Type file ini tidak dizinkan",
        addRemoveLinks:true,
        maxThumbnailFilesize: 20,
    });

    //Event ketika memulai mengupload
    foto_upload.on("sending",function(a,b,c){
        a.token = Math.random();
        a.sku   = $('#sku').val();        
        c.append("token_foto",a.token);
        c.append("sku",a.sku); 
    });

    foto_upload.on("success",function(){
        $(".dz-preview").remove();
        $(".dropzone").removeClass('dz-started');

        $sku = $('#sku').val();
        tamplikan_daftar_foto($sku);
    });

    //Event ketika foto dihapus
    foto_upload.on("removedfile",function(a){
        var token = a.token;
        $.ajax({
            type:"post",
            data:{token:token,},
            url:"<?php echo base_url('barang_pusat/remove_foto') ?>",
            cache:false,
            dataType: 'json',
            success: function(){
                console.log("Foto terhapus");
            },
            error: function(){
                console.log("Error");
            }
        });
    });
    // Akhir upload dan hapus foto

    // Awal ajax daftar foto
    function tamplikan_daftar_foto($sku)
    {
        $('div#daftar_foto').hide();    
        var Lebar   = Lebar + 25;
        $.ajax({
            url: "<?php echo site_url('barang_pusat/ajax_daftar_foto'); ?>",
            type: "POST",
            cache: false,
            data: 'sku=' + $sku,
            dataType:'json',
            success: function(json){
                if(json.status == 1){
                    $('div#daftar_foto').show('fast');
                    $('div#daftar_foto').html(json.datanya);
                }
            }
        });
    }
    // Akhir ajax daftar foto

    // Awal hapus perfoto
    function delete_foto(id_foto)
    {
        $.ajax({
            url : "<?php echo site_url('barang_pusat/ajax_delete_foto'); ?>",
            type: "POST",
            cache: false,
            data: 'id_foto=' + id_foto,
            dataType: "JSON",
            success: function(data){
                if(data.status == 1){
                    $sku = $('#sku').val();
                    tamplikan_daftar_foto($sku);
                }else if(data.status == 0){
                    swal({
                        title: "Gagal !",
                        text: "Gagal menghapus foto barang pusat.",
                        type: "warning",
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Ok, saya mengerti",
                    });
                }
            }
        });
    }
    // Akhir hapus perfoto

    // Awal jadikan foto profile barang
    function jadikan_profile_foto(id_foto)
    {
        $.ajax({
            url : "<?php echo site_url('barang_pusat/ajax_jadikan_foto_profile'); ?>",
            type: "POST",
            cache: false,
            data: 'id_foto=' + id_foto,
            dataType: "JSON",
            success: function(data){
                if(data.status == 1){
                    $sku = $('#sku').val();
                    tamplikan_daftar_foto($sku);
                    reload_table_master_barang();
                }else if(data.status == 0){
                    alert('Error update foto profile from ajax');
                    swal({
                        title: "Gagal !",
                        text: "Gagal update foto profile barang pusat.",
                        type: "warning",
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Ok, saya mengerti",
                    });
                }
            }
        });
    }
    // Akhir jadikan foto profile barang 
</script>
<!-- Akhir Script CRUD -->

<!-- Awal datatable -->
<link href="assets/plugin/zircos/plugins/datatables/buttons.bootstrap.min.css" rel="stylesheet" type="text/css"/>
<link href="assets/plugin/zircos/plugins/datatables/dataTables.colVis.css" rel="stylesheet" type="text/css"/>
<script src="assets/plugin/zircos/plugins/datatables/dataTables.buttons.min.js"></script>
<script src="assets/plugin/zircos/plugins/datatables/buttons.bootstrap.min.js"></script>
<script src="assets/plugin/zircos/plugins/datatables/jszip.min.js"></script>
<script src="assets/plugin/zircos/plugins/datatables/pdfmake.min.js"></script>
<script src="assets/plugin/zircos/plugins/datatables/vfs_fonts.js"></script>
<script src="assets/plugin/zircos/plugins/datatables/buttons.html5.min.js"></script>
<script src="assets/plugin/zircos/plugins/datatables/buttons.print.min.js"></script>
<!-- Akhir datatable -->
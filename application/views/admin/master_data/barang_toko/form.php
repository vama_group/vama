<!-- Bootstrap modal -->
<div class="modal fade" id="modal_form" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h5 class="modal-title">Barang Toko Form</h5>
            </div>
            <div class="modal-body form">
                <form action="#" id="form" class="form-group">
                    <input id="id_barang_toko" name="id_barang_toko" type="hidden">
                    <input id="kode_toko_edit" name="kode_toko_edit" type="hidden">
                    <div class="form-body">
                        <div class="row"> 
                            <div class="col-md-9">
                                <div class="form-horizontal">
                                    <label class="control-label col-md-3 small">Nama Toko</label>
                                    <div class="col-md-9">
                                        <input id="nama_toko" name="nama_toko" placeholder="Nama Toko" class="form-control" type="text" required readonly>
                                        <span class="help-block"></span>
                                    </div>
                                </div>

                                <div class="form-horizontal">
                                    <label class="control-label col-md-3 small">Kode Barang & SKU</label>
                                    <div class="col-md-9">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <input id="kode_barang" name="kode_barang" placeholder="Kode Barang" class="form-control" type="text" required readonly>
                                                <span class="help-block"></span>
                                            </div>

                                            <div class="col-md-6">
                                                <input id="sku" name="sku" placeholder="SKU" class="form-control" type="text" required readonly>
                                                <span class="help-block"></span>
                                            </div>                                        
                                        </div>
                                    </div>
                                </div>                      

                                <div class="form-horizontal">
                                    <label class="control-label col-md-3 small">Nama Barang</label>
                                    <div class="col-md-9">
                                        <input id="nama_barang" name="nama_barang" placeholder="Nama Barang" class="form-control" type="text" required readonly>
                                        <span class="help-block"></span>
                                    </div>
                                </div>                                                                                                                                                                                           
                                <div class="form-horizontal">
                                    <label class="control-label col-md-3 small">Harga Eceran & Status Barang</label>
                                    <div class="col-md-9">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <input id="harga_eceran_hidden" name="harga_eceran_hidden" placeholder="Harga Eceran" class="form-control" type="hidden">
                                                <input id="harga_eceran" name="harga_eceran" placeholder="Harga Eceran" class="form-control" type="text">
                                                <span class="help-block"></span>
                                            </div>

                                            <div class="col-md-6">
                                                <select name="status" class="form-control">
                                                    <option value="AKTIF">AKTIF</option>
                                                    <option value="TIDAK AKTIF">TIDAK AKTIF</option>
                                                </select>
                                            </div>                                        
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div id="foto_barang" name="foto_barang">
                                </div>
                            </div>
                        </div>
                    </div>                                                                                         
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" id="btnSave" onclick="save()" class="btn btn-primary">Simpan</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- End Bootstrap modal -->


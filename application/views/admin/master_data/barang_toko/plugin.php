<!-- Awal CSS -->
<link href="assets/plugin/zircos/plugins/datatables/jquery.dataTables.min.css" rel="stylesheet" type="text/css"/>
<link href="assets/plugin/zircos/plugins/datatables/responsive.bootstrap.min.css" rel="stylesheet" type="text/css"/>
<link href="assets/plugin/zircos/plugins/datatables/dataTables.bootstrap.min.css" rel="stylesheet" type="text/css"/>
<link href="assets/plugin/zircos/material-design/assets/css/tambahan.css" rel="stylesheet" type="text/css"/>

<!-- Awal JS -->
<script src="assets/plugin/zircos/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="assets/plugin/zircos/plugins/datatables/dataTables.bootstrap.js"></script>
<script src="assets/plugin/zircos/material-design/assets/js/detect.js"></script>
<script src="assets/plugin/zircos/material-design/assets/js/jquery.slimscroll.js"></script>
<script src="assets/plugin/zircos/plugins/datatables/dataTables.responsive.min.js"></script>
<script src="assets/plugin/zircos/plugins/datatables/responsive.bootstrap.min.js"></script>
<script src="assets/plugin/zircos/material-design/assets/pages/jquery.datatables.init.js"></script>
<!-- Akhir JS -->

<!-- Awal upload foto-->
<link rel="stylesheet" href="assets/plugin/zircos/plugins/magnific-popup/css/magnific-popup.css"/>
<script type="text/javascript" src="assets/plugin/zircos/plugins/magnific-popup/js/jquery.magnific-popup.min.js"></script>

<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/dropzone/dropzone.min.css') ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/dropzone/basic.min.css') ?>">
<script type="text/javascript" src="<?php echo base_url('assets/dropzone/dropzone.min.js') ?>"></script>
<!-- Akhir upload foto-->

<!-- Awal Sweet-Alert  -->
<link href="assets/plugin/zircos/plugins/bootstrap-sweetalert/sweet-alert.css" rel="stylesheet" type="text/css">
<script src="assets/plugin/zircos/plugins/bootstrap-sweetalert/sweet-alert.min.js"></script>
<!-- Akhir Sweet-Alert  -->

<!-- Awal Script CRUD -->
<script type="text/javascript">
    var save_method;
    var table_master_barang          = $('#TableMasterBarang').DataTable();
    var table_daftar_transaksi       = $('#TableDaftarTransaksi').DataTable();
    var table_pembaharuan_barang     = $('#TablePembaharuanBarang').DataTable();
    var table_daftar_transaksi_rusak = $('#TableDaftarTransaksiRusak').DataTable();
    var table_daftar_periksa_stok    = $('#TablePeriksaStok').DataTable();
    var table_daftar_periksa_harga   = $('#TablePeriksaHarga').DataTable();

    var delay = (function () {
        var timer = 0;
        return function (callback, ms){
            clearTimeout(timer);
            timer = setTimeout(callback, ms);
        };
    })();
    
    function removeAllOptions(selectbox){   
        var i;
        for(i=selectbox.options.length-1;i>=0;i--)
        {   selectbox.remove(i); }
    }

    function addOption(selectbox, value, text ){   
        var optn = document.createElement("OPTION");
        optn.text = text;
        optn.value = value;
        selectbox.options.add(optn);
    }

    function TampilkanData(){
        if($('#id_tab').html() == '1'){
            if($('#kode_toko').val() == ''){
                table_master_barang.clear();table_master_barang.destroy();
                table_master_barang=$('#TableMasterBarang').DataTable({});
                swal({
                    title: "Oops!", 
                    text: "Harap pilih toko terlebih dahulu.", 
                    type: "info", 
                    confirmButtonClass: 'btn-primary btn-md waves-effect waves-light',
                    confirmButtonText: "Ok"
                }, function(){
                    $('#kode_toko').focus();
                });
            }else{
                TampilkanMasterBarang();
            }
        }else if($('#id_tab').html() == '2'){
            if($('#kode_toko').val() == ''){
                table_daftar_transaksi.clear();table_daftar_transaksi.destroy();
                table_daftar_transaksi=$('#TableDaftarTransaksi').DataTable({});
                swal({
                    title: "Oops!", 
                    text: "Harap pilih toko terlebih dahulu.", 
                    type: "info", 
                    confirmButtonClass: 'btn-primary btn-md waves-effect waves-light',
                    confirmButtonText: "Ok"
                }, function(){
                    $('#kode_toko').focus();
                });
            }else{
                TampilkanDaftarTransaksi();
            }
        }else if($('#id_tab').html() == '3'){
            if($('#kode_toko').val() == ''){
                table_daftar_transaksi_rusak.clear();table_daftar_transaksi_rusak.destroy();
                table_daftar_transaksi_rusak = $('#TableDaftarTransaksiRusak').DataTable({});
                swal({
                    title: "Oops!", 
                    text: "Harap pilih toko terlebih dahulu.", 
                    type: "info", 
                    confirmButtonClass: 'btn-primary btn-md waves-effect waves-light',
                    confirmButtonText: "Ok"
                }, function(){
                    $('#kode_toko').focus();
                });
            }else{
                TampilkanDaftarTransaksiRusak();
            }
        }else if($('#id_tab').html() == '4'){
            if($('#kode_toko').val() == ''){
                table_daftar_transaksi_rusak.clear();table_daftar_transaksi_rusak.destroy();
                table_daftar_transaksi_rusak = $('#TableDaftarTransaksiRusak').DataTable({});
                swal({
                    title: "Oops!", 
                    text: "Harap pilih toko terlebih dahulu.", 
                    type: "info", 
                    confirmButtonClass: 'btn-primary btn-md waves-effect waves-light',
                    confirmButtonText: "Ok"
                }, function(){
                    $('#kode_toko').focus();
                });
            }else{
                TampilkanPembaharuanBarang();
            }
        }else if($('#id_tab').html() == '5'){
            if($('#kode_toko').val() == ''){
                table_daftar_periksa_stok.clear();table_daftar_periksa_stok.destroy();
                table_daftar_periksa_stok = $('#TablePeriksaStok').DataTable({});
                swal({
                    title: "Oops!", 
                    text: "Harap pilih toko terlebih dahulu.", 
                    type: "info", 
                    confirmButtonClass: 'btn-primary btn-md waves-effect waves-light',
                    confirmButtonText: "Ok"
                }, function(){
                    $('#kode_toko').focus();
                });
            }else{
                TampilkanDaftarPeriksaStok();
            }
        }else if($('#id_tab').html() == '6'){
            if($('#kode_toko').val() == ''){
                table_daftar_periksa_harga.clear();table_daftar_periksa_harga.destroy();
                table_daftar_periksa_harga = $('#TablePeriksaHarga').DataTable({});
                swal({
                    title: "Oops!", 
                    text: "Harap pilih toko terlebih dahulu.", 
                    type: "info", 
                    confirmButtonClass: 'btn-primary btn-md waves-effect waves-light',
                    confirmButtonText: "Ok"
                }, function(){
                    $('#kode_toko').focus();
                });
            }else{
                TampilkanDaftarPeriksaHarga();
            }
        }
    }

    function RefreshData(){
        if($('#id_tab').html() == '1'){
            reload_table_master_barang();
        }else if($('#id_tab').html() == '2'){
            if($('#kode_toko').val() == ''){
                swal({
                    title: "Oops!", 
                    text: "Harap pilih toko terlebih dahulu.", 
                    type: "info", 
                    confirmButtonClass: 'btn-primary btn-md waves-effect waves-light',
                    confirmButtonText: "Ok"
                }, function(){
                    $('#kode_toko').focus();
                });
            }else{
                reload_table_daftar_transaksi();
            }
        }else if($('#id_tab').html() == '3'){
            if($('#kode_toko').val() == ''){
                $('#kode_toko').focus();
                swal({
                    title: "Oops!", 
                    text: "Harap pilih toko terlebih dahulu.", 
                    type: "info", 
                    confirmButtonClass: 'btn-primary btn-md waves-effect waves-light',
                    confirmButtonText: "Ok"
                }, function(){
                    $('#kode_toko').focus();
                });
            }else{
                reload_table_daftar_transaksi_rusak();
            }
        }else if($('#id_tab').html() == '4'){
            reload_table_pembaharuan_barang();
        }else if($('#id_tab').html() == '5'){
            if($('#kode_toko').val() == ''){
                $('#kode_toko').focus();
                swal({
                    title: "Oops!", 
                    text: "Harap pilih toko terlebih dahulu.", 
                    type: "info", 
                    confirmButtonClass: 'btn-primary btn-md waves-effect waves-light',
                    confirmButtonText: "Ok"
                }, function(){
                    $('#kode_toko').focus();
                });
            }else{
                reload_table_daftar_periksa_stok();
            }
        }else if($('#id_tab').html() == '6'){
            if($('#kode_toko').val() == ''){
                $('#kode_toko').focus();
                swal({
                    title: "Oops!", 
                    text: "Harap pilih toko terlebih dahulu.", 
                    type: "info", 
                    confirmButtonClass: 'btn-primary btn-md waves-effect waves-light',
                    confirmButtonText: "Ok"
                }, function(){
                    $('#kode_toko').focus();
                });
            }else{
                reload_table_periksa_harga();
            }
        }
    }

    function import_barang_toko()
    {
        $('#icon_import').removeClass('fa-refresh');
        $('#icon_import').addClass('fa-spin fa-refresh');

        $.ajax({
            url : "<?php echo site_url('barang_toko/ajax_import_barang_toko'); ?>",
            type: "POST",
            cache: false,
            dataType: "JSON",
            success: function(data){
                if(data.status == 1){
                    swal({
                        title: "Berhasil!", 
                        text: "Data barang toko berhasil di import.", 
                        type: "success", 
                        confirmButtonText: "Ok"
                    });
                    window.location.href = "barang_toko";
                }else if(data.status == 0){
                   swal({
                        title: "Gagal!", 
                        text: "Tidak ada data barang toko yg di import 1.", 
                        type: "error", 
                        confirmButtonClass: 'btn-danger btn-md waves-effect waves-light',
                        confirmButtonText: "Ok"
                    }, function(isConfirm){
                        // window.location.href = "barang_toko";
                    });
                }
            },
            error: function (jqXHR, textStatus, errorThrown){
                swal({
                    title: "Gagal!", 
                    text: "Data barang toko gagal di import 2.", 
                    type: "error", 
                    confirmButtonClass: 'btn-danger btn-md waves-effect waves-light',
                    confirmButtonText: "Ok"
                }, function(isConfirm){
                    // window.location.href = "barang_toko";
                });
            }
        });
        $('#icon_import').removeClass('fa-spin fa-refresh');
        $('#icon_import').addClass('fa-refresh');
    }

    function import_stok_toko()
    {
        $('#icon_import').removeClass('fa-refresh');
        $('#icon_import').addClass('fa-spin fa-refresh');

        $.ajax({
            url : "<?php echo site_url('barang_toko/ajax_import_stok_toko'); ?>",
            type: "POST",
            cache: false,
            dataType: "JSON",
            success: function(data){
                if(data.status == 1){
                    swal({
                        title: "Berhasil!", 
                        text: "Data barang toko berhasil di import.", 
                        type: "success", 
                        confirmButtonText: "Ok"
                    });
                    window.location.href = "barang_toko";
                }else if(data.status == 0){
                   swal({
                        title: "Gagal!", 
                        text: "Tidak ada data barang toko yg di import 1.", 
                        type: "error", 
                        confirmButtonClass: 'btn-danger btn-md waves-effect waves-light',
                        confirmButtonText: "Ok"
                    }, function(isConfirm){
                        // window.location.href = "barang_toko";
                    });
                }
            },
            error: function (jqXHR, textStatus, errorThrown){
                swal({
                    title: "Gagal!", 
                    text: "Data barang toko gagal di import 2.", 
                    type: "error", 
                    confirmButtonClass: 'btn-danger btn-md waves-effect waves-light',
                    confirmButtonText: "Ok"
                }, function(isConfirm){
                    // window.location.href = "barang_toko";
                });
            }
        });
        $('#icon_import').removeClass('fa-spin fa-refresh');
        $('#icon_import').addClass('fa-refresh');
    }

    function fix_stok()
    {
        $('#icon_fix').removeClass('fa-spin fa-refresh');
        $('#icon_fix').addClass('fa-spin fa-refresh');

        $.ajax({
            url : "<?php echo site_url('barang_toko/ajax_fix_stok_toko'); ?>",
            type: "POST",
            cache: false,
            data: 'kode_toko=' + $('#kode_toko').val() +
                  '&id_periode_stok=' + $('#pilih_periode_stok_toko').val(),
            dataType: "JSON",
            success: function(data){
                if(data.status == 1){
                    swal({
                        title: "Berhasil!", 
                        text: "Data stok barang toko berhasil di perbaiki.", 
                        type: "success", 
                        confirmButtonText: "Ok"
                    });
                    TampilkanData();
                    $('#icon_fix').removeClass('fa-spin fa-refresh');
                }else if(data.status == 0){
                   swal({
                        title: "Gagal!", 
                        text: "Tidak ada stok barang toko untuk di perbaiki.", 
                        type: "error", 
                        confirmButtonClass: 'btn-danger btn-md waves-effect waves-light',
                        confirmButtonText: "Ok"
                    }, function(isConfirm){
                        // window.location.href = "barang_toko";
                        $('#icon_fix').removeClass('fa-spin fa-refresh');
                    });
                }
            },
            error: function (jqXHR, textStatus, errorThrown){
                swal({
                    title: "Gagal!", 
                    text: "Data stok barang toko gagal di perbaiki.", 
                    type: "error", 
                    confirmButtonClass: 'btn-danger btn-md waves-effect waves-light',
                    confirmButtonText: "Ok"
                }, function(isConfirm){
                    // window.location.href = "barang_toko";
                    $('#icon_fix').removeClass('fa-spin fa-refresh');
                });
            }
        });
    }

    function fix_harga()
    {
        swal({
            title: 'Yakin ingin memperbaharui harga eceran toko berdasarkan harga pusat ?',
            type: "info",
            showCancelButton: true,
            confirmButtonClass: 'btn-primary btn-md waves-effect waves-light',
            confirmButtonText: "Iya, saya yakin",
            cancelButtonText: "Batal",
            closeOnConfirm: false
        }, function(isConfirm){
            if(isConfirm){
                $('#icon_harga').removeClass('fa-spin fa-refresh');
                $('#icon_harga').addClass('fa-spin fa-refresh');

                $.ajax({
                    url : "<?php echo site_url('barang_toko/ajax_fix_harga_eceran_toko'); ?>",
                    type: "POST",
                    cache: false,
                    data: 'kode_toko=' + $('#kode_toko').val() +
                          '&id_periode_stok=' + $('#pilih_periode_stok_toko').val(),
                    dataType: "JSON",
                    success: function(data){
                        if(data.status == 1){
                            setTimeout(function(){
                                swal({
                                    title: "Berhasil!", 
                                    text: "Data harga eceran barang toko berhasil di perbaiki.", 
                                    type: "success", 
                                    confirmButtonText: "Ok"
                                });
                                TampilkanData();
                                $('#icon_harga').removeClass('fa-spin fa-refresh');
                            }, 100);
                        }else if(data.status == 0){
                            setTimeout(function(){
                                swal({
                                    title: "Gagal!", 
                                    text: "Tidak ada harga eceran barang toko untuk di perbaiki.", 
                                    type: "error", 
                                    confirmButtonClass: 'btn-danger btn-md waves-effect waves-light',
                                    confirmButtonText: "Ok"
                                }, function(isConfirm){
                                    // window.location.href = "barang_toko";
                                    $('#icon_harga').removeClass('fa-spin fa-refresh');
                                });
                           }, 100);
                        }
                    },
                    error: function (jqXHR, textStatus, errorThrown){
                        setTimeout(function(){
                            swal({
                                title: "Gagal!", 
                                text: "Data harga eceran barang toko gagal di perbaiki.", 
                                type: "error", 
                                confirmButtonClass: 'btn-danger btn-md waves-effect waves-light',
                                confirmButtonText: "Ok"
                            }, function(isConfirm){
                                // window.location.href = "barang_toko";
                                $('#icon_harga').removeClass('fa-spin fa-refresh');
                            });
                        }, 100);
                    }
                });
            }
        });
    }

    function TampilkanPeriodeStok(){
        if($('#kode_toko').val() == ''){
            swal({
                title: "Harap pilih toko terlebih dahulu !",
                type: "warning",
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Ok, saya mengerti",
            });
            return;
        }

        $.ajax({
            url : "<?php echo site_url('barang_toko/ambil_periode_stok'); ?>",
            type: "POST",
            cache: false,
            data: 'kode_toko=' + $('#kode_toko').val(),
            dataType: "JSON",
            success: function(json){
                if(json.status == '1'){
                    $('#pilih_periode_stok_toko').removeAttr('disabled').val();
                    removeAllOptions(document.form_select.pilih_periode_stok_toko);
                    var i;
                    for (i = 0; i < json.jumlah; i++) {
                        addOption(
                            document.form_select.pilih_periode_stok_toko, 
                            json.data[i].id_periode_stok_toko, 
                            json.data[i].kode_periode_stok_toko
                        );
                    }
                    TampilkanData();
                }else{
                    $('#pilih_periode_stok_toko').prop('readonly', true).val();
                    swal({
                        title: "Gagal mengambil data periode stok toko, harap pilih toko terlebih dahulu !",
                        text: "Harap hubungi administrator pusat!",
                        type: "warning",
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Ok, saya mengerti",
                    }, function(){
                        window.location.href="barang_toko";
                    });
                }
            },
            error: function (jqXHR, textStatus, errorThrown){
                $('#pilih_periode_stok_toko').prop('readonly', true).val();
                swal({
                    title: "Gagal mengambil data periode stok toko, harap pilih toko terlebih dahulu !",
                    text: "Harap hubungi administrator pusat!",
                    type: "warning",
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Ok, saya mengerti",
                }, function(){
                    window.location.href="barang_toko";
                });
            }
        });
    }

    function TampilkanMasterBarang(){
        // Awal table master barang
        table_master_barang.clear();table_master_barang.destroy();
        table_master_barang=$('#TableMasterBarang').DataTable({
            <?php if($this->session->userdata('usergroup_name') == 'Super Admin'){ ?>
                dom : "Blftpi",
            <?php } ?>
            buttons: [
                        {
                            extend: "copy",
                            text: "<i class='fa fa-files-o text-primary'></i>",
                            titleAttr: "Salin data ke clipboard"
                        }, 
                        {
                            extend: "excel",
                            text: "<i class='fa fa-file-excel-o text-success'></i>",
                            titleAttr: "Excel"
                        }, 
                        {
                            extend: "pdf",
                            text: "<i class='fa fa-file-pdf-o text-danger'></i>",
                            titleAttr: "PDF"
                        }, 
                        {
                            extend: "print",
                            text: "<i class='fa fa-print'></i>",
                            titleAttr: "Print"
                        }
                     ],
            stateSave: true,
            ordering: true,
            bFilter:true,
            processing:true,
            serverSide:true,
            order:[],
            lengthMenu: [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],

            oLanguage:{
                sLoadingRecords:'Silakan Tunggu',
                sProcessing:"<img src='assets/loader/cubeloader2.2.gif' width='50' height='50'> Harap tunggu",
                sZeroRecords:"Data Tidak Ditemukan",
                sSearch : "Pencarian :  _INPUT_",
                sLengthMenu: "<i class='btn btn-primary btn-sm fa fa-folder-open-o'></i><span class='btn btn-default btn-xs'>Menampilkan _MENU_ barang toko</span>",
                sInfo: "<span class='btn btn-sm'>Menampilkan (_START_ sampai _END_) dari _TOTAL_ barang toko</span>",
                sInfoEmpty: "Tidak ada barang toko untuk ditampilkan",
                sInfoFiltered: " - (disaring dari _MAX_ total barang toko)"
            },
            ajax:{
                url: "<?php echo site_url('barang_toko/ajax_list')?>",
                type: "POST",
                data: {'kode_toko' : $('#kode_toko').val(),
                       'id_periode_stok' : $('#pilih_periode_stok_toko').val()}
            },
            columnDefs: [
                            { 
                                "targets": [ -1 ],
                                "orderable": false,
                            },
                        ],
        });
        // Akhir table master barang
    }

    function TampilkanDaftarTransaksi(){
        // Awal table daftar transaksi
        table_daftar_transaksi.clear();table_daftar_transaksi.destroy();
        table_daftar_transaksi=$('#TableDaftarTransaksi').DataTable({
            <?php if($this->session->userdata('usergroup_name') == 'Super Admin'){ ?>
                dom : "Blftpi",
            <?php } ?>
            buttons: [
                        {
                            extend: "copy",
                            text: "<i class='fa fa-files-o text-primary'></i>",
                            titleAttr: "Salin data ke clipboard"
                        }, 
                        {
                            extend: "excel",
                            text: "<i class='fa fa-file-excel-o text-success'></i>",
                            titleAttr: "Excel"
                        }, 
                        {
                            extend: "pdf",
                            text: "<i class='fa fa-file-pdf-o text-danger'></i>",
                            titleAttr: "PDF"
                        }, 
                        {
                            extend: "print",
                            text: "<i class='fa fa-print'></i>",
                            titleAttr: "Print"
                        }
                     ],
            stateSave: true,
            ordering: true,
            bFilter:true,
            processing:true,
            serverSide:true,
            order:[],
            lengthMenu: [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
            oLanguage:{
                sLoadingRecords:'Silakan Tunggu',
                sProcessing:"<img src='assets/loader/cubeloader2.2.gif' width='50' height='50'> Harap tunggu",
                sZeroRecords:"Data Tidak Ditemukan",
                sSearch : "Pencarian :  _INPUT_",
                sLengthMenu: "<i class='btn btn-primary btn-sm fa fa-folder-open-o'></i><span class='btn btn-default btn-xs'>Menampilkan _MENU_ barang toko</span>",
                sInfo: "<span class='btn btn-sm'>Menampilkan (_START_ sampai _END_) dari _TOTAL_ barang toko</span>",
                sInfoEmpty: "Tidak ada barang toko untuk ditampilkan",
                sInfoFiltered: " - (disaring dari _MAX_ total barang toko)"
            },
            ajax:{
                url: "<?php echo site_url('barang_toko/ajax_list_daftar_transaksi')?>",
                type: "POST",
                data: {'kode_toko' : $('#kode_toko').val(),
                       'id_periode_stok' : $('#pilih_periode_stok_toko').val()}
            },
            columnDefs: [
                            { 
                                "targets": [ -1 ],
                                "orderable": false,
                            },
                        ],
        });
        // Akhir table daftar transaksi
    }

    function TampilkanDaftarTransaksiRusak(){
        // Awal table daftar transaksi
        table_daftar_transaksi_rusak.clear();table_daftar_transaksi_rusak.destroy();
        table_daftar_transaksi_rusak=$('#TableDaftarTransaksiRusak').DataTable(
        {
            <?php if($this->session->userdata('usergroup_name') == 'Super Admin'){ ?>
                dom : "Blftpi",
            <?php } ?>
            buttons: [
                        {
                            extend: "copy",
                            text: "<i class='fa fa-files-o text-primary'></i>",
                            titleAttr: "Salin data ke clipboard"
                        }, 
                        {
                            extend: "excel",
                            text: "<i class='fa fa-file-excel-o text-success'></i>",
                            titleAttr: "Excel"
                        }, 
                        {
                            extend: "pdf",
                            text: "<i class='fa fa-file-pdf-o text-danger'></i>",
                            titleAttr: "PDF"
                        }, 
                        {
                            extend: "print",
                            text: "<i class='fa fa-print'></i>",
                            titleAttr: "Print"
                        }
                     ],
            stateSave: true,
            ordering: true,
            bFilter:true,
            processing:true,
            serverSide:true,
            order:[],
            lengthMenu: [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
            // buttons: ['pageLength'],
            oLanguage:{
                sLoadingRecords:'Silakan Tunggu',
                sProcessing:"<img src='assets/loader/cubeloader2.2.gif' width='50' height='50'> Harap tunggu",
                sZeroRecords:"Data Tidak Ditemukan",
                sSearch : "Pencarian :  _INPUT_",
                sLengthMenu: "<i class='btn btn-primary btn-sm fa fa-folder-open-o'></i><span class='btn btn-default btn-xs'>Menampilkan _MENU_ barang toko</span>",
                sInfo: "<span class='btn btn-sm'>Menampilkan (_START_ sampai _END_) dari _TOTAL_ barang toko</span>",
                sInfoEmpty: "Tidak ada barang toko untuk ditampilkan",
                sInfoFiltered: " - (disaring dari _MAX_ total barang toko)"
            },
            ajax:{
                url: "<?php echo site_url('barang_toko/ajax_list_daftar_transaksi_rusak')?>",
                type: "POST",
                data: {'kode_toko' : $('#kode_toko').val(),
                       'id_periode_stok' : $('#pilih_periode_stok_toko').val()}
            },
            columnDefs: [
                            { 
                                "targets": [ -1 ],
                                "orderable": false,
                            },
                        ],
        });
        // Akhir table daftar transaksi
    }

    function TampilkanPembaharuanBarang(){
        // Awal table pembaharuan Barang
        table_pembaharuan_barang.clear();table_pembaharuan_barang.destroy();
        table_pembaharuan_barang=$('#TablePembaharuanBarang').DataTable(
        {
            <?php if($this->session->userdata('usergroup_name') == 'Super Admin'){ ?>
                dom : "Blftpi",
            <?php } ?>
            buttons: [
                        {
                            extend: "copy",
                            text: "<i class='fa fa-files-o text-primary'></i>",
                            titleAttr: "Salin data ke clipboard"
                        }, 
                        {
                            extend: "excel",
                            text: "<i class='fa fa-file-excel-o text-success'></i>",
                            titleAttr: "Excel"
                        }, 
                        {
                            extend: "pdf",
                            text: "<i class='fa fa-file-pdf-o text-danger'></i>",
                            titleAttr: "PDF"
                        }, 
                        {
                            extend: "print",
                            text: "<i class='fa fa-print'></i>",
                            titleAttr: "Print"
                        }
                     ],
            stateSave: true,
            ordering: true,
            bFilter:true,
            processing:true,
            serverSide:true,
            order:[],
            lengthMenu: [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
            oLanguage:{
                sLoadingRecords:'Silakan Tunggu',
                sProcessing:"<img src='assets/loader/cubeloader2.2.gif' width='50' height='50'> Harap tunggu",
                sZeroRecords:"Data Tidak Ditemukan",
                sSearch : "Pencarian :  _INPUT_",
                sLengthMenu: "<i class='btn btn-primary btn-sm fa fa-folder-open-o'></i><span class='btn btn-default btn-xs'>Menampilkan _MENU_ barang toko</span>",
                sInfo: "<span class='btn btn-sm'>Menampilkan (_START_ sampai _END_) dari _TOTAL_ barang toko</span>",
                sInfoEmpty: "Tidak ada barang toko untuk ditampilkan",
                sInfoFiltered: " - (disaring dari _MAX_ total barang toko)"
            },
            ajax:{
                url: "<?php echo site_url('barang_toko/ajax_list_pembaharuan_barang')?>",
                type: "POST",
                data: {'kode_toko' : $('#kode_toko').val(),
                       'id_periode_stok' : $('#pilih_periode_stok_toko').val()}
            },
            columnDefs: [
                            { 
                                "targets": [ -1 ],
                                "orderable": false,
                            },
                        ],
        });
        // Akhir table pembaharuan barang
    }

    function TampilkanDaftarPeriksaStok(){
        // Awal table periksa stok Barang
        table_daftar_periksa_stok.clear();table_daftar_periksa_stok.destroy();
        table_daftar_periksa_stok=$('#TablePeriksaStok').DataTable(
        {
            <?php if($this->session->userdata('usergroup_name') == 'Super Admin'){ ?>
                dom : "Blftpi",
            <?php } ?>
            buttons: [
                        {
                            extend: "copy",
                            text: "<i class='fa fa-files-o text-primary'></i>",
                            titleAttr: "Salin data ke clipboard"
                        }, 
                        {
                            extend: "excel",
                            text: "<i class='fa fa-file-excel-o text-success'></i>",
                            titleAttr: "Excel"
                        }, 
                        {
                            extend: "pdf",
                            text: "<i class='fa fa-file-pdf-o text-danger'></i>",
                            titleAttr: "PDF"
                        }, 
                        {
                            extend: "print",
                            text: "<i class='fa fa-print'></i>",
                            titleAttr: "Print"
                        }
                     ],
            stateSave: true,
            ordering: true,
            bFilter:true,
            processing:true,
            serverSide:true,
            order:[],
            lengthMenu: [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
            oLanguage:{
                sLoadingRecords:'Silakan Tunggu',
                sProcessing:"<img src='assets/loader/cubeloader2.2.gif' width='50' height='50'> Harap tunggu",
                sZeroRecords:"Data Tidak Ditemukan",
                sSearch : "Pencarian :  _INPUT_",
                sLengthMenu: "<i class='btn btn-primary btn-sm fa fa-folder-open-o'></i><span class='btn btn-default btn-xs'>Menampilkan _MENU_ barang toko</span>",
                sInfo: "<span class='btn btn-sm'>Menampilkan (_START_ sampai _END_) dari _TOTAL_ barang toko</span>",
                sInfoEmpty: "Tidak ada barang toko untuk ditampilkan",
                sInfoFiltered: " - (disaring dari _MAX_ total barang toko)"
            },
            ajax:{
                url: "<?php echo site_url('barang_toko/ajax_list_periksa_stok')?>",
                type: "POST",
                data: {'kode_toko' : $('#kode_toko').val(),
                       'id_periode_stok' : $('#pilih_periode_stok_toko').val()}
            },
            columnDefs: [
                            { 
                                "targets": [ -1 ],
                                "orderable": false,
                            },
                        ],
        });
        // Akhir table periksa stok barang
    }

    function TampilkanDaftarPeriksaHarga(){
        // Awal table periksa harga Barang
        table_daftar_periksa_harga.clear();table_daftar_periksa_harga.destroy();
        table_daftar_periksa_harga=$('#TablePeriksaHarga').DataTable(
        {
            <?php if($this->session->userdata('usergroup_name') == 'Super Admin'){ ?>
                dom : "Blftpi",
            <?php } ?>
            buttons: [
                        {
                            extend: "copy",
                            text: "<i class='fa fa-files-o text-primary'></i>",
                            titleAttr: "Salin data ke clipboard"
                        }, 
                        {
                            extend: "excel",
                            text: "<i class='fa fa-file-excel-o text-success'></i>",
                            titleAttr: "Excel"
                        }, 
                        {
                            extend: "pdf",
                            text: "<i class='fa fa-file-pdf-o text-danger'></i>",
                            titleAttr: "PDF"
                        }, 
                        {
                            extend: "print",
                            text: "<i class='fa fa-print'></i>",
                            titleAttr: "Print"
                        }
                     ],
            stateSave: true,
            ordering: true,
            bFilter:true,
            processing:true,
            serverSide:true,
            order:[],
            lengthMenu: [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
            oLanguage:{
                sLoadingRecords:'Silakan Tunggu',
                sProcessing:"<img src='assets/loader/cubeloader2.2.gif' width='50' height='50'> Harap tunggu",
                sZeroRecords:"Data Tidak Ditemukan",
                sSearch : "Pencarian :  _INPUT_",
                sLengthMenu: "<i class='btn btn-primary btn-sm fa fa-folder-open-o'></i><span class='btn btn-default btn-xs'>Menampilkan _MENU_ barang toko</span>",
                sInfo: "<span class='btn btn-sm'>Menampilkan (_START_ sampai _END_) dari _TOTAL_ barang toko</span>",
                sInfoEmpty: "Tidak ada barang toko untuk ditampilkan",
                sInfoFiltered: " - (disaring dari _MAX_ total barang toko)"
            },
            ajax:{
                url: "<?php echo site_url('barang_toko/ajax_list_periksa_harga')?>",
                type: "POST",
                data: {'kode_toko' : $('#kode_toko').val(),
                       'id_periode_stok' : $('#pilih_periode_stok_toko').val()}
            },
            columnDefs: [
                            { 
                                "targets": [ -1 ],
                                "orderable": false,
                            },
                        ],
        });
        // Akhir table periksa harga barang
    }

    function reload_table_master_barang()
    {
        table_master_barang.ajax.reload(null,false); 
    }

    function reload_table_daftar_transaksi()
    {
        table_daftar_transaksi.ajax.reload(null,false);
    }

    function reload_table_daftar_transaksi_rusak()
    {
        table_daftar_transaksi_rusak.ajax.reload(null,false);
    }

    function reload_table_pembaharuan_barang()
    {
        table_pembaharuan_barang.ajax.reload(null,false);
    }

    function reload_table_periksa_stok()
    {
        table_daftar_periksa_stok.ajax.reload(null,false);
    }

    function reload_table_periksa_harga()
    {
        table_daftar_periksa_harga.ajax.reload(null,false);
    }

    $(document).ready(function() {
        $('#navbar').removeClass('navbar-default');
        $('#navbar').addClass('navbar-orange');
        $("#wrapper").removeClass('forced');
        $("#wrapper").addClass('forced enlarged');
        $('#JudulHalaman').html('Barang Toko - VAMA');

        var _swal = window.swal;
        window.swal = function(){
            var previousWindowKeyDown = window.onkeydown;
            _swal.apply(this, Array.prototype.slice.call(arguments, 0));
            window.onkeydown = previousWindowKeyDown;
        };

        $("input").change(function(){
            $(this).parent().parent().removeClass('has-error');
            $(this).next().empty();
        });

        $("textarea").change(function(){
            $(this).parent().parent().removeClass('has-error');
            $(this).next().empty();
        });

        $("select").change(function(){
            if($('select#kode_toko')){
            }else{
                $(this).parent().parent().removeClass('has-error');
                $(this).next().empty();
            }
        });
    });

    $(document).on('click', '#tab_induk', function(){
        TampilkanMasterBarang();
        $('#id_tab').html('1');
    });

    $(document).on('click', '#tab_transaksi', function(){
        $('#id_tab').html('2');
        TampilkanData();
    });

    $(document).on('click', '#tab_transaksi_rusak', function(){
        $('#id_tab').html('3');
        TampilkanData();
    }); 

    $(document).on('click', '#tab_pembaharuan', function(){
        $('#id_tab').html('4');
        TampilkanData();
    });

    $(document).on('click', '#tab_periksa_stok', function(){
        $('#id_tab').html('5');
        TampilkanData();
    });

    $(document).on('click', '#tab_periksa_harga', function(){
        $('#id_tab').html('6');
        TampilkanData();
    });

    function to_rupiah(angka)
    {
        var rev     = parseInt(angka, 10).toString().split('').reverse().join('');
        var rev2    = '';
        for(var i = 0; i < rev.length; i++){
            rev2  += rev[i];
            if((i + 1) % 3 === 0 && i !== (rev.length - 1)){
                rev2 += '.';
            }
        }
        return 'Rp. ' + rev2.split('').reverse().join('');
    }

    function to_angka(rp)
    {
        return parseInt(rp.replace(/,.*|\D/g,''),10)
    }

    $(document).on('keyup', '#harga_eceran', function(){
        var hrg_eceran       = $('#harga_eceran').val();
        hrg_eceran           = to_angka(hrg_eceran);
        
        if (hrg_eceran>0) 
        {
            $('#harga_eceran').val(to_rupiah(hrg_eceran));
            $('#harga_eceran_hidden').val(hrg_eceran);
        } 
        else
        {
            $('#harga_eceran').val('');
            $('#harga_eceran_hidden').val('0');
        }
    }); 

    function add_barang_toko()
    {
        save_method = 'add';
        $('#form')[0].reset();
        $('.form-horizontal').removeClass('has-error');
        $('.help-block').empty();
        $('#modal_form').modal('show');
        $('.modal-title').text('Tambah Barang Toko');
    }

    function edit_barang_toko(id_barang_toko, kode_toko)
    {
        save_method = 'update';
        $('#form')[0].reset();
        $('.form-horizontal').removeClass('has-error');
        $('.help-block').empty();

        $.ajax({
            url : "<?php echo site_url('barang_toko/ajax_edit'); ?>",
            type: "POST",
            cache: false,
            data: 'id_barang_toko=' + id_barang_toko +
                  '&kode_toko=' + kode_toko,
            dataType: "JSON",
            success: function(json){
                $('[name="id_barang_toko"]').val(json.data.id_barang_toko);
                $('[name="kode_toko_edit"]').val(kode_toko);  
                $('[name="nama_toko"]').val(json.data.nama_customer_pusat);            
                $('[name="kode_barang"]').val(json.data.kode_barang);
                $('[name="sku"]').val(json.data.sku);
                $('[name="nama_barang"]').val(json.data.nama_barang);
                $('[name="harga_eceran"]').val(to_rupiah(json.data.harga_eceran));
                $('[name="harga_eceran_hidden"]').val(json.data.harga_eceran);
                $('[name="status"]').val(json.data.status);
                $('#foto_barang').html(json.foto);
                $('modal-dialog').removeClass('modal-sm');
                $('modal-dialog').addClass('modal-lg');
                $('#modal_form').modal('show');
                $('.modal-title').text('Edit Barang Toko');
            },
            error: function (jqXHR, textStatus, errorThrown){
                swal({
                    title: "Gagal mengambil data barang toko !",
                    text: "Harap hubungi administrator pusat!",
                    type: "warning",
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Ok, saya mengerti",
                }, function(){
                    window.location.href="barang_toko";
                });
            }
        });
    }

    function save()
    {
        var url;
        var judul;
        var pesan;
        if(save_method == 'add'){
            judul   = "Yakin ingin menambahkan barang toko ?";
            pesan   = "Nama Barang : " + $('#nama_barang').val() + ", " + 
                      "Nama Toko : " + $('#nama_toko').val();
            url     = "<?php echo site_url('barang_toko/ajax_add')?>";
        }else{
            judul   = "Yakin ingin memperbaharui barang toko ?";
            pesan   = "Nama Barang : " + $('#nama_barang').val() + ", " + 
                      "Nama Toko : " + $('#nama_toko').val();
            url     = "<?php echo site_url('barang_toko/ajax_update')?>";
        }

        swal({
            title: judul,
            text: pesan,
            type: "info",
            showCancelButton: true,
            confirmButtonClass: 'btn-primary btn-md waves-effect waves-light',
            confirmButtonText: "Iya",
            cancelButtonText: "Batal",
            closeOnConfirm: false
        }, function(isConfirm){
            if(isConfirm){
                $('#btnSave').text('menyimpan...');
                $('#btnSave').attr('disabled', true);

                $.ajax({
                    url : url,
                    type: "POST",
                    data: $('#form').serialize(),
                    dataType: "JSON",
                    success: function(data){
                        if(data.status){
                            if(data.status == 1){
                                $('#modal_form').modal('hide');
                                RefreshData();
                                setTimeout(function(){
                                    swal({
                                        title: "Berhasil!", 
                                        text: "Data barang toko berhasil disimpan.", 
                                        type: "success", 
                                        confirmButtonText: "Ok"
                                    });
                                }, 100);
                            }else if(data.status == 0){
                                $('#modal_form').modal('hide');
                                RefreshData();
                                setTimeout(function(){
                                    swal({
                                        title: "Gagal!", 
                                        text: data.pesan, 
                                        type: "danger", 
                                        confirmButtonText: "Ok"
                                    });
                                }, 100);
                            }   
                        }else{
                            for (var i = 0; i < data.inputerror.length; i++){
                                $('[name="'+data.inputerror[i]+'"]').parent().parent().parent().parent().addClass('has-error'); 
                                $('[name="'+data.inputerror[i]+'"]').next().text(data.error_string[i]); 
                            }
                            setTimeout(function(){
                                swal({
                                    title: "Gagal!", 
                                    text: "Data barang toko gagal disimpan.", 
                                    type: "error", 
                                    confirmButtonClass: 'btn-danger btn-md waves-effect waves-light',
                                    confirmButtonText: "Ok"
                                });
                            }, 100);
                        }
                        $('#btnSave').text('Simpan');
                        $('#btnSave').attr('disabled',false);
                    },
                    error: function (jqXHR, textStatus, errorThrown){
                        swal({
                            title: "Gagal menyimpan atau memperbaharui data barang toko !",
                            text: "Harap hubungi administrator pusat!",
                            type: "warning",
                            confirmButtonColor: "#DD6B55",
                            confirmButtonText: "Ok, saya mengerti",
                        }, function (isConfirm) {
                            if(isConfirm){
                                window.location.href="barang_toko";
                            }
                        });
                    }
                });
            }
        });
    }

    function verifikasi_delete(id_barang_toko)
    {
        $.ajax({
            url : "<?php echo site_url('barang_toko/ajax_verifikasi_delete'); ?>",
            type: "POST",
            cache: false,
            data: 'id_barang_toko=' + id_barang_toko,
            dataType: "JSON",
            success: function(data){
                $('.modal-dialog').removeClass('modal-sm');
                $('.modal-dialog').addClass('modal-md');
                $('#ModalHeader').html('Informasi Hapus Barang Toko');
                $('#ModalContent').html(data.pesan);
                $('#ModalFooter').html(data.footer);
                $('#ModalGue').modal('show');
            },error: function (jqXHR, textStatus, errorThrown){
                swal({
                    title: "Gagal !",
                    text: "Data barang toko gagal untuk ditampilkan",
                    type: "warning",
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Ok, saya mengerti",
                },function (isConfirm) {
                    if (isConfirm){
                        window.location.href="barang_toko";
                    }
                });
            }
        });
    }

    function delete_barang_toko(id_barang_toko)
    {
        $.ajax({
            url : "<?php echo site_url('barang_toko/ajax_delete'); ?>",
            type: "POST",
            cache: false,
            data: 'id_barang_toko=' + id_barang_toko,
            dataType: "JSON",
            success: function(data){
                $('#modal_form').modal('hide');
                reload_table();
                swal({
                    title: "Berhasil!", 
                    text: "Data barang toko berhasil dihapus.", 
                    type: "success", 
                    confirmButtonText: "Ok"
                });
            },
            error: function (jqXHR, textStatus, errorThrown){
                swal({
                    title: "Gagal!", 
                    text: "Data barang toko gagal dihapus.", 
                    type: "error", 
                    confirmButtonClass: 'btn-danger btn-md waves-effect waves-light',
                    confirmButtonText: "Ok"
                }, function(isConfirm){
                    window.location.href = "barang_toko";
                });
            }
        });
    }

    function cetak_excel(){
        // window.open("<?php //echo site_url('barang_toko/ajax_cetak_ke_excel?'); ?>",'_blank');
        swal({
            title: "Yakin ingin export data ke excel ?",
            type: "info",
            showCancelButton: true,
            confirmButtonClass: 'btn-primary btn-md waves-effect waves-light',
            confirmButtonText: "Iya",
            cancelButtonText: "Batal",
            closeOnConfirm: false
        }, function(isConfirm){
            if(isConfirm){
                $.ajax({
                    url : "<?php echo site_url('barang_toko/ajax_cetak_ke_excel'); ?>",
                    type: "POST",
                    cache: false,
                    data: 'kode_toko=' + $('#kode_toko').val() +
                          '&id_periode_stok=' + $('#pilih_periode_stok_toko').val(),
                    dataType: "JSON",
                    success: function(data){
                        if(data.status == 1){
                            swal({
                                title: "Berhasil!", 
                                text: data.pesan, 
                                type: "success", 
                                confirmButtonText: "Ok"
                            });
                        }else if(data.status == 0){
                            swal({
                                title: "Oops!", 
                                text: data.pesan, 
                                type: "error",
                                confirmButtonClass: 'btn-danger btn-md waves-effect waves-light',
                                confirmButtonText: "Ok"
                            });
                        }
                    },
                    error: function (jqXHR, textStatus, errorThrown){
                        swal({
                            title: "Gagal!", 
                            text: "Data barang pusat gagal di export ke excel.", 
                            type: "error", 
                            confirmButtonClass: 'btn-danger btn-md waves-effect waves-light',
                            confirmButtonText: "Ok"
                        }, function(isConfirm){
                            window.location.href = "barang_toko";
                        });
                    }
                });
            }
        });
    } 
</script>
<!-- Akhir Script CRUD -->

<!-- Awal datatable -->
<link href="assets/plugin/zircos/plugins/datatables/buttons.bootstrap.min.css" rel="stylesheet" type="text/css"/>
<link href="assets/plugin/zircos/plugins/datatables/dataTables.colVis.css" rel="stylesheet" type="text/css"/>
<script src="assets/plugin/zircos/plugins/datatables/dataTables.buttons.min.js"></script>
<script src="assets/plugin/zircos/plugins/datatables/buttons.bootstrap.min.js"></script>
<script src="assets/plugin/zircos/plugins/datatables/jszip.min.js"></script>
<script src="assets/plugin/zircos/plugins/datatables/pdfmake.min.js"></script>
<script src="assets/plugin/zircos/plugins/datatables/vfs_fonts.js"></script>
<script src="assets/plugin/zircos/plugins/datatables/buttons.html5.min.js"></script>
<script src="assets/plugin/zircos/plugins/datatables/buttons.print.min.js"></script>
<!-- Akhir datatable -->
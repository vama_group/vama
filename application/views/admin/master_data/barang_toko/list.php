<!-- Start content -->
<div class="content">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="page-title-box">
                    <h4 class="page-title text-warning"><i class='mdi mdi-cube-outline'></i><b>  Barang Toko</b></h4>
                    <ol class="breadcrumb p-0 m-0">
                        <li>
                            <form name="form_select">
                                <span id="id_tab" style="display: none;">1</span>
                                <b class="small text-warning">Toko : </b>
                                <select id="kode_toko" name="kode_toko" class="btn btn-rounded btn-xs btn-default" onchange="TampilkanPeriodeStok()">
                                    <option value="" class="text-dark ">PILIH TOKO</option>
                                    <?php foreach ($data_toko AS $dt){ ?>
                                        <option value="<?php echo $dt->kode_customer_pusat ?>" class="text-dark">
                                            <?php echo $dt->nama_customer_pusat ?>
                                        </option>
                                    <?php } ?>
                                </select>

                                <select id="pilih_periode_stok_toko" name="pilih_periode_stok_toko" class="btn btn-rounded btn-xs btn-default" onchange="TampilkanData()" disabled>
                                    <option value="">Pilih Periode Stok</option>
                                </select>
                            
                                <span class="btn btn-rounded btn-xs btn-primary" onclick="TampilkanData()">
                                    <i class="glyphicon glyphicon-refresh"></i> 
                                    REFRESH
                                </span>
                            </form>
                            
                            <?php if($this->session->userdata('id_pegawai') == '1' or 
                                     $this->session->userdata('id_pegawai') == '2'){ ?>
                                <!-- <button id="ImportBarangToko" class="btn btn-rounded btn-xs btn-default" onclick="import_barang_toko()">
                                    <i id="icon_import" class="fa fa-refresh"></i> 
                                    Import Barang Toko
                                </button>
                                <button id="ImportStokToko" class="btn btn-rounded btn-xs btn-default" onclick="import_stok_toko()">
                                    <i id="icon_import" class="fa fa-refresh"></i> 
                                    Import Stok Toko
                                </button> -->
                                <!-- <button id="CetakExcel" class="btn btn-rounded btn-xs btn-success" onclick="cetak_excel()">
                                    <i class="fa fa-file-excel-o"></i> 
                                    Cetak Ke Excel
                                </button> -->

                                <button id="FixStok" class="btn btn-rounded btn-xs btn-success" onclick="fix_stok()">
                                    <i id="icon_fix" class="fa fa-check"></i> 
                                    Fix Stok
                                </button>

                                <button id="FixHarga" class="btn btn-rounded btn-xs btn-success" onclick="fix_harga()">
                                    <i id="icon_harga" class="fa fa-check"></i> 
                                    Fix Harga
                                </button>
                            <?php } ?>
                        </li>                        
                    </ol>

                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
        
        <div class="row">
            <div class="col-sm-12">
                <div class="card-box table-responsive"> 
                    <!-- Awal induk tab -->
                    <ul class="nav nav-tabs tabs-bordered" id="tab_list_barang">
                        <li class="active">
                            <a href="#tab_induk_barang" id="tab_induk" data-toggle="tab" aria-expanded="true">
                                <span class="visible-xs"><i class="fa fa-check-square"></i></span>
                                <span class="hidden-xs">Induk</span>
                            </a>
                        </li>
                        <li class="">
                            <a href="#tab_transaksi_barang" id="tab_transaksi" data-toggle="tab" aria-expanded="true">
                                <span class="visible-xs"><i class="typcn typcn-home"></i></span>
                                <span class="hidden-xs">Transaksi Stok Jual</span>
                            </a>
                        </li>
                        <li class="">
                            <a href="#tab_transaksi_barang_rusak" id="tab_transaksi_rusak" data-toggle="tab" aria-expanded="true">
                                <span class="visible-xs"><i class="typcn typcn-home"></i></span>
                                <span class="hidden-xs">Transaksi Stok Rusak</span>
                            </a>
                        </li>                        
                        <li class="">
                            <a href="#tab_pembaharuan_barang" id="tab_pembaharuan" data-toggle="tab" aria-expanded="true">
                                <span class="visible-xs"><i class="mdi mdi-camera"></i></span>
                                <span class="hidden-xs">Pembaharuan</span>
                            </a>
                        </li>

                        <?php if($this->session->userdata('id_pegawai') == '1' or 
                                 $this->session->userdata('id_pegawai') == '2'){ ?>
                            <li class="">
                                <a href="#tab_periksa_stok_barang" id="tab_periksa_stok" data-toggle="tab" aria-expanded="true">
                                    <span class="visible-xs"><i class="fa fa-balance-scale"></i></span>
                                    <span class="hidden-xs">Periksa Stok</span>
                                </a>
                            </li>

                            <li class="">
                                <a href="#tab_periksa_harga_barang" id="tab_periksa_harga" data-toggle="tab" aria-expanded="true">
                                    <span class="visible-xs"><i class="fa fa-balance-scale"></i></span>
                                    <span class="hidden-xs">Periksa Harga</span>
                                </a>
                            </li>
                        <?php } ?>
                    </ul>
                    <!-- Akhir induk tab -->

                    <!-- Isi Tab -->
                    <div class="tab-content">
                        <!-- Awal master barang toko -->
                        <div class="tab-pane active" id="tab_induk_barang">              
                            <table id="TableMasterBarang" class="table table-condensed table-striped table-hover dt-responsive nowrap" cellspacing="0" width="100%">
                                <thead class="input-sm">
                                    <tr>
                                        <th>#</th>
                                        <th>TOMBOL</th>
                                        <th>TOKO</th>
                                        <th>SKU</th>
                                        <th>NAMA BARANG</th>
                                        <th>STOK JUAL</th>
                                        <th>STOK RUSAK</th>
                                        <th>HARGA</th>
                                        <th>STATUS 1</th>
                                        <th>STATUS 2</th>                                
                                        
                                        <th>Pegawai Save</th>
                                        <th>Tanggal save</th>
                                        <th>Pegawai Edit</th>
                                        <th>Tanggal Edit</th>
                                    </tr>
                                </thead>

                                <tbody class="input-sm">
                                </tbody>
                            </table>
                        </div>
                        <!-- Awal master barang toko -->

                        <!-- Awal histori stok jual -->
                        <div class="tab-pane" id="tab_transaksi_barang">
                            <table id="TableDaftarTransaksi" class="table table-condensed table-striped table-hover dt-responsive nowrap" cellspacing="0" width="100%">
                                <thead class="input-sm">
                                    <tr>
                                        <th>#</th>
                                        <th>TOKO</th>
                                        <th>SKU</th>
                                        <th style="width: 560px">NAMA BARANG</th>
                                        <th>ECERAN</th>
                                        <th>SO +</th>
                                        <th>BM +</th>
                                        <th>- PJ</th>
                                        <th>- RPT</th>
                                        <th>- RBKPT</th>
                                        <th>- PP</th>
                                        <th>STOK JUAL</th>
                                    </tr>
                                </thead>

                                <tbody class="input-sm">
                                </tbody>
                            </table>
                        </div>
                        <!-- Akhir histori stok jual -->

                        <!-- Awal histori stok rusak -->
                        <div class="tab-pane" id="tab_transaksi_barang_rusak">
                            <table id="TableDaftarTransaksiRusak" class="table table-condensed table-striped table-hover dt-responsive nowrap" cellspacing="0" width="100%">
                                <thead class="input-sm">
                                    <tr>
                                        <th>#</th>
                                        <th>TOKO</th>
                                        <th>SKU</th>
                                        <th style="width: 560px">NAMA BARANG</th>
                                        <th>ECERAN</th>
                                        <th>RPR +</th>
                                        <th>- RBKPR</th>
                                        <th>STOK RUSAK</th>
                                    </tr>
                                </thead>

                                <tbody class="input-sm">
                                </tbody>
                            </table>
                        </div>
                        <!-- Akhir histori stok rusak -->

                        <!-- Awal daftar pembaharuan barang -->
                        <div class="tab-pane" id="tab_pembaharuan_barang">
                            <table id="TablePembaharuanBarang" class="table table-condensed table-striped table-hover dt-responsive nowrap" cellspacing="0" width="100%">
                                <thead class="input-sm">
                                    <tr>
                                        <th>#</th>
                                        <th>TOMBOL</th>
                                        <th>TOKO</th>
                                        <th>SKU</th>
                                        <th style="width: 560px">NAMA BARANG</th>
                                        <th>STOK JUAL</th>
                                        <th>ECERAN</th>

                                        <th>Pegawai Save</th>
                                        <th>Tanggal save</th>
                                        <th>Pegawai Edit</th>
                                        <th>Tanggal Edit</th>
                                    </tr>
                                </thead>

                                <tbody class="input-sm">
                                </tbody>
                            </table>
                        </div>
                        <!-- Akhir daftar pembaharuan barang -->

                        <?php if($this->session->userdata('id_pegawai') == '1' or 
                                 $this->session->userdata('id_pegawai') == '2'){ ?>
                            <!-- Awal periksa stok -->
                            <div class="tab-pane" id="tab_periksa_stok_barang">
                                <table id="TablePeriksaStok" class="table table-condensed table-striped table-hover dt-responsive nowrap" cellspacing="0" width="100%">
                                    <thead class="input-sm">
                                        <tr>
                                            <th>#</th>
                                            <th>TOMBOL</th>
                                            <th>TOKO</th>
                                            <th>SKU</th>
                                            <th style="width: 560px">NAMA BARANG</th>
                                            <th>ECERAN</th>
                                            <th>STOK HITUNG</th>
                                            <th>STOK TABEL</th>
                                            <th>KOREKSI</th>
                                            <th>STATUS</th>
                                        </tr>
                                    </thead>

                                    <tbody class="input-sm">
                                    </tbody>
                                </table>
                            </div>
                            <!-- Akhir periksa stok -->

                            <!-- Awal periksa harga -->
                            <div class="tab-pane" id="tab_periksa_harga_barang">
                                <table id="TablePeriksaHarga" class="table table-condensed table-striped table-hover dt-responsive nowrap" cellspacing="0" width="100%">
                                    <thead class="input-sm">
                                        <tr>
                                            <th>#</th>
                                            <th>TOMBOL</th>
                                            <th>TOKO</th>
                                            <th>SKU</th>
                                            <th style="width: 560px">NAMA BARANG</th>
                                            <th>ECERAN PUSAT</th>
                                            <th>ECERAN TOKO</th>
                                            <th>KOREKSI HARGA</th>
                                            <th>STOK</th>
                                            <th>STATUS</th>
                                        </tr>
                                    </thead>

                                    <tbody class="input-sm">
                                    </tbody>
                                </table>
                            </div>
                            <!-- Akhir periksa harga -->
                        <?php } ?>
                    </div>                    
                </div>
            </div>
        </div>
    </div> <!-- container -->
</div> 
<!-- content -->

<div class="modal" id="ModalGue" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class='fa fa-times-circle'></i></button>
                <h4 class="modal-title" id="ModalHeader"></h4>
            </div>
            <div class="modal-body" id="ModalContent"></div>
            <div class="modal-footer" id="ModalFooter"></div>
        </div>
    </div>
</div>

<script>
$('#ModalGue').on('hide.bs.modal', function () {
   setTimeout(function(){ 
        $('#ModalHeader, #ModalContent, #ModalFooter').html('');
   }, 500);
});
</script>

<link href="assets/select2/select2.min.css" rel="stylesheet" type="text/css" />
<script src="assets/select2/select2.min.js" type="text/javascript"></script><link href="assets/plugin/zircos/plugins/datatables/jquery.dataTables.min.css" rel="stylesheet" type="text/css"/>

<!-- Awal CSS -->
<link href="assets/plugin/zircos/plugins/datatables/responsive.bootstrap.min.css" rel="stylesheet" type="text/css"/>
<link href="assets/plugin/zircos/plugins/datatables/dataTables.bootstrap.min.css" rel="stylesheet" type="text/css"/>
<link href="assets/plugin/zircos/material-design/assets/css/tambahan.css" rel="stylesheet" type="text/css"/>
<link href="assets/plugin/zircos/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css" rel="stylesheet">
<link href="assets/plugin/zircos/plugins/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
<!-- Akhir CSS -->

<!-- Awal JS -->
<script src="assets/plugin/zircos/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="assets/plugin/zircos/plugins/datatables/dataTables.bootstrap.js"></script>

<script src="assets/plugin/zircos/material-design/assets/js/detect.js"></script>
<script src="assets/plugin/zircos/material-design/assets/js/jquery.slimscroll.js"></script>
<script src="assets/plugin/zircos/plugins/datatables/dataTables.responsive.min.js"></script>
<script src="assets/plugin/zircos/plugins/datatables/responsive.bootstrap.min.js"></script>
<script src="assets/plugin/zircos/material-design/assets/pages/jquery.datatables.init.js"></script>
<!-- Akhir JS -->

<!-- Awal Sweet-Alert  -->
<link href="assets/plugin/zircos/plugins/bootstrap-sweetalert/sweet-alert.css" rel="stylesheet" type="text/css">
<script src="assets/plugin/zircos/plugins/bootstrap-sweetalert/sweet-alert.min.js"></script>
<!-- Akhir Sweet-Alert  -->

<script>
    var url               = "paket_harga_spesial";
    var table_induk       = $('#TableTransaksi').DataTable();
    var table_batal       = $('#TableTransaksiBatal').DataTable();
    var table_cari_barang = $('#TableCariBarang').DataTable();
    $('#JudulHalaman').html('Paket & Harga Spesial');

    $(document).ready(function(){
        $('#navbar').removeClass('navbar-default');
        $('#navbar').addClass('navbar-pink');
        $("#wrapper").removeClass('forced');
        $("#wrapper").addClass('forced enlarged');

        var id_paket_promo_m  = $('#id_paket_promo_m').val(); 
        ambil_data(id_paket_promo_m);

        if($('#id_paket_promo_m').val() == '0'){
            $('#nama_paket_promo').focus();
        }else{
            $('#pencarian_kode_barang').focus();
        }

        //datepicker
        $('#tanggal_awal').datepicker({
            autoclose: true,
            format: "yyyy-mm-dd",
            todayHighlight: true
        });

        $('#tanggal_akhir').datepicker({
            autoclose: true,
            format: "yyyy-mm-dd",
            todayHighlight: true
        });
    });

    var delay = (function () {
        var timer = 0;
        return function (callback, ms) {
            clearTimeout(timer);
            timer = setTimeout(callback, ms);
        };
    })();

    $("#id_kategori_paket").select2({
        ajax: {
            url:"<?php echo site_url('paket_harga_spesial/list_kategori_paket')?>",
            dataType: 'json',
            type: "GET",
            delay: 250,
            data: function (params) {
                return {
                    nk: params.term
                };
            },
            processResults: function (data) {
                if(data !== ''){
                    var res = data.map(function (item) {
                          return {id: item.id_kategori_paket, text: item.nama_kategori_paket};
                        });
                }else{
                    res = '';
                }
                return {
                    results: res
                };
            }
        }
    });

    $("#id_kategori_paket").change(function(){        
        $('#id_jenis_paket').focus();
    });

    $(document).on('click', '#tombol_tambah_kategori', function(e){
        e.preventDefault();
        $.ajax({
            url: "<?php echo site_url('paket_harga_spesial/form_kategori_paket'); ?>",
            type: "POST",
            cache: false,
            data: 'id_penjualan_m=' + $('#id_penjualan_m').val(),
            dataType:'json',
            success: function(json){
                if(json.status == 1){
                    $('.modal-dialog').removeClass('modal-sm');
                    $('.modal-dialog').removeClass('modal-lg');
                    $('.modal-dialog').addClass('modal-lg');
                    $('#ModalHeader').html('Tambah Kategori Paket');
                    $('#ModalContent').load('<?php echo site_url('paket_harga_spesial/inputan_kategori_paket'); ?>');
                    $('#ModalGue').modal('show');
                }else{
                    swal({
                        title: "Oops !",
                        text: json.pesan,
                        type: "error",
                        confirmButtonClass: 'btn-danger btn-md waves-effect waves-light',
                        confirmButtonText: "Ok"
                    },function(isConfirm){
                        window.location.href = url;
                    });
                }
            }, error: function (jqXHR, textStatus, errorThrown){
                swal({
                    title: "Oops !",
                    text: "Gagal membuka form tambah kategori paket.",
                    type: "error",
                    confirmButtonClass: 'btn-danger btn-md waves-effect waves-light',
                    confirmButtonText: "Ok"
                },function(isConfirm){
                    window.location.href = url;
                });
            }
        });
    });

    function ambil_data(id_paket_promo_m)
    {
        usergroup = "<?php echo $this->session->userdata('usergroup_name') ?>";
        if (id_paket_promo_m !=='0'){
            $.ajax({
                url : "<?php echo site_url('paket_harga_spesial/ambil_data'); ?>",
                type: "POST",
                cache: false,
                data: 'id_paket_promo_m=' + id_paket_promo_m,
                dataType:'JSON',
                success: function(json){
                    url = "paket_harga_spesial/transaksi/?&id=" + id_paket_promo_m;
                    $('#kode_toko').val(json.data_induk['kode_toko']);
                    $('#kode_paket_promo').html(json.data_induk['kode_paket_promo']);
                    $('#nama_paket_promo').val(json.data_induk['nama_paket_promo']);

                    if(json.data_induk.tipe_paket == 'PAKET REGULER'){
                        $('#tipe_paket').html('');
                        $('#tipe_paket').append(
                            "<option value='" + json.data_induk.tipe_paket + "'>"
                            +json.data_induk.tipe_paket
                            +"</option>"
                        );
                        $('#kode_toko').prop('disabled', true);
                        $('#tipe_paket').prop('disabled', true);
                    }else{
                        $('#tipe_paket').val(json.data_induk['tipe_paket']);
                    }

                    $('#id_kategori_paket').html('');
                    $('#id_kategori_paket').append(
                        "<option value='" + json.data_induk.id_kategori_paket + "'>"
                        +json.data_induk.nama_kategori_paket
                        +"</option>"
                    );

                    $('#id_jenis_paket').val(json.data_induk['jenis_paket']);
                    $('#tanggal_awal').val(json.data_induk['tanggal_awal_paket']);
                    $('#tanggal_akhir').val(json.data_induk['tanggal_akhir_paket']);
                    $('#catatan').val(json.data_induk['keterangan_lain']);
                    $('#total_dasar').html(to_rupiah(json.data_induk['total_dasar']));
                    $('#total_bersih').html(to_rupiah(json.data_induk['total_bersih']));

                    $('#judul_tab_induk').html("Data Paket Barang : <span class='badge up bg-primary'>" + 
                                                json.data_induk['jumlah_barang'] +
                                               " Barang </span>");
                    $('#judul_tab_batal').html("Data Paket Barang : <span class='badge up bg-danger'>" + 
                                                json.data_batal['jumlah_barang'] +
                                               " Barang </span>");

                    tampilkan_data();

                },error : function(data){ 
                    url = 'paket_harga_spesial';
                    swal({
                        title: "Oops !", 
                        text: "Data paket promo gagal ditampilkan.", 
                        type: "error", 
                        confirmButtonClass: 'btn-danger btn-md waves-effect waves-light',
                        confirmButtonText: "Ok"
                    },function (isConfirm){
                        window.location.href = url;
                    });
                }
            });
        } 
    }

    function tampilkan_data(){
        table_induk.clear();table_induk.destroy();
        table_induk=$('#TableTransaksi').DataTable(
        {
            stateSave: true,
            ordering: true,
            bFilter:true,
            processing:true,
            serverSide:true,
            order:[],
            lengthMenu: [[5, 10, 25, 50, -1], [5, 10, 25, 50, "All"]],
            pagingType: "full",

            oLanguage:{
                sLoadingRecords:'Silakan Tunggu',
                sProcessing:"<img src='assets/loader/cubeloader2.2.gif' width='50' height='50'> Harap tunggu",
                sZeroRecords:"Belum ada data untuk ditampilkan",
                sSearch : "Pencarian data"
            },
            ajax:{
                url: "<?php echo site_url('paket_harga_spesial/ajax_list')?>",
                type: "POST",
                data: {'id_paket_promo_m' : $('#id_paket_promo_m').val()}
            },
            columnDefs: [{ 
                "targets": [ -1 ],
                "orderable": false,
            },],
        });
    }

    function tampilkan_data_batal(){
        table_batal.clear();table_batal.destroy();
        table_batal=$('#TableTransaksiBatal').DataTable(
        {
            stateSave: true,
            ordering: true,
            bFilter:true,
            processing:true,
            serverSide:true,
            order:[],
            lengthMenu: [[5, 10, 25, 50, -1], [5, 10, 25, 50, "All"]],
            pagingType: "full",

            oLanguage:{
                sLoadingRecords:'Silakan Tunggu',
                sProcessing:"<img src='assets/loader/cubeloader2.2.gif' width='50' height='50'> Harap tunggu",
                sZeroRecords:"Belum ada data untuk ditampilkan",
                sSearch : "Pencarian data"
            },
            ajax:{
                url: "<?php echo site_url('paket_harga_spesial/ajax_list_batal')?>",
                type: "POST",
                data: {'id_paket_promo_m' : $('#id_paket_promo_m').val()}
            },
            columnDefs: [{ 
                "targets": [ -1 ],
                "orderable": false,
            },],
        });
    }

    function tampilkan_data_barang(){
        table_cari_barang.clear();table_cari_barang.destroy();
        table_cari_barang=$('#TableCariBarang').DataTable({
            ordering: true,
            bFilter:true,
            processing:true,
            serverSide:true,
            order:[],
            lengthMenu: [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
            pagingType: "full",

            oLanguage:{
                sLoadingRecords:'Silakan Tunggu',
                sProcessing:"<img src='assets/loader/cubeloader2.2.gif' width='50' height='50'> Harap tunggu",
                sZeroRecords:"Belum ada data untuk ditampilkan",
                sSearch : "Pencarian data"
            },
            ajax:{
                url: "<?php echo site_url('paket_harga_spesial/ajax_list_barang')?>",
                type: "POST",
                data: {
                    'id_paket_promo_m' : $('#id_paket_promo_m').val(),
                    'kode_toko' : $("#kode_toko").val()
                }
            },
            columnDefs: [{ 
                "targets": [ -1 ],
                "orderable": false,
            },],
        });
    }

    $(document).on('click', '#tab_induk', function(e){
        tampilkan_data();
    });

    $(document).on('click', '#tab_batal', function(e){
        tampilkan_data_batal();
    });

    function reload_table_induk()
    {
        table_induk.ajax.reload();
    }

    function reload_table_batal()
    {
        table_batal.ajax.reload();
    }

    $(document).on('keydown', '#nama_paket_promo', function(e){
        var charCode = e.which || e.keyCode;
        if(charCode == 13 || charCode == 9){
            $('#id_kategori_paket').select2('open').select2('options'); 
        }
    });

    $(document).on('click', 'button#SimpanDetail', function(){
        simpan_detail();
    });

    $(document).on('click', '#daftar-autocomplete li', function(){
        $(this).parent().parent().parent().find('input').val($(this).find('span#skunya').html());

        var id_barang_pusat = $(this).find('span#id_barang').html();
        var nama_barang     = $(this).find('span#barangnya').html();
        var harga           = $(this).find('span#harga_eceran').html();

        $('div#hasil_pencarian').hide();
        $('#id_barang_pusat').html(id_barang_pusat);
        $('#nama_barang').html(nama_barang);
        $('#harga_satuan').val(to_rupiah(harga));
        $('#jumlah_barang').removeAttr('disabled').val('').focus();
        $('#discount_value').removeAttr('disabled');
        $('#discount_harga').removeAttr('disabled');
        $('#SimpanDetail').removeAttr('disabled').val();
        HitungSubtotalBarang();
    });

    $(document).on('keyup', '#pencarian_kode_barang', function(e){
        if($(this).val() !== ''){
            var charCode = e.which || e.keyCode;
            if(charCode == 40){
                if($('div#hasil_pencarian li.autocomplete_active').length > 0){
                    var Selanjutnya = $('div#hasil_pencarian li.autocomplete_active').next();
                    $('div#hasil_pencarian li.autocomplete_active').removeClass('autocomplete_active');

                    Selanjutnya.addClass('autocomplete_active');
                }else{
                    $('div#hasil_pencarian li:first').addClass('autocomplete_active');
                }
            }else if(charCode == 38){
                if($('div#hasil_pencarian li.autocomplete_active').length > 0){
                    var Sebelumnya = $('div#hasil_pencarian li.autocomplete_active').prev();
                    $('div#hasil_pencarian li.autocomplete_active').removeClass('autocomplete_active');
                
                    Sebelumnya.addClass('autocomplete_active');
                }else{
                    $('div#hasil_pencarian li:first').addClass('autocomplete_active');
                }
            }else if(charCode == 13){
                $('div#hasil_pencarian_barang').hide();
                $.ajax({
                    url: "<?php echo site_url('paket_harga_spesial/ajax_cari_barang'); ?>",
                    type: "POST",
                    cache: false,
                    data: 'sku=' + $(this).val() +
                          '&kode_toko=' + $('#kode_toko').val(),
                    dataType:'json',
                    success: function(json){
                        if(json.status == 1){
                            $('#id_barang_pusat').html(json.data['id_barang_pusat']);
                            $('#nama_barang').html(json.data['nama_barang']);
                            $('#harga_satuan').val(to_rupiah(json.data['harga_satuan']));
                            $('#discount_value').removeAttr('disabled');
                            $('#discount_harga').removeAttr('disabled');

                            $('#jumlah_barang').removeAttr('disabled').val('').focus();
                            $('#SimpanDetail').removeAttr('disabled').val();
                            HitungSubtotalBarang();

                        }else if(json.status == 0){
                            $('#id_barang_pusat').html('');
                            $('#nama_barang').html('Belum ada barang yg dipilih');
                            $('#harga_satuan').val('Rp. 0');
                            $('#jumlah_barang').prop('disabled', true).val('');
                            $('#SimpanDetail').prop('disabled', true).val('');

                            $('.modal-dialog').removeClass('modal-lg');
                            $('.modal-dialog').addClass('modal-sm');
                            $('#ModalHeader').html('Oops !');
                            $('#ModalContent').html(json.pesan);
                            $('#ModalFooter').html("<button id='info_stok' type='button' class='btn btn-primary'>Ok, Saya Mengerti</button>");
                            $('#ModalGue').modal('show');
                            setTimeout(function(){ 
                                $('#info_stok').focus();
                            }, 100);
                        }
                    }
                });
            }else{
                $('#id_barang_pusat').html('');
                $('#nama_barang').html('Belum ada barang yg dipilih');
                $('#jumlah_barang').prop('disabled', true).val('');            
                $('#harga_satuan').val('Rp. 0');
                $('#discount_value').val('0.00');
                $('#discount_harga').val('Rp. 0');
                $('#harga_bersih').val('Rp. 0');
                $('#subtotal').html('Rp. 0');


                $('#SimpanDetail').prop('disabled', true).val('');
                $('div#hasil_pencarian').hide();
                AutoCompletedBarang($(this).val());
            }
        }else{
            // Bersihkan data pemilihan barang
            $('#id_barang_pusat').html('');
            $('#nama_barang').html('Belum ada barang yg dipilih');
            $('#jumlah_barang').prop('disabled', true).val('');            
            $('#harga_satuan').val('Rp. 0');
            $('#discount_value').val('0.00');
            $('#discount_harga').val('Rp. 0');
            $('#harga_bersih').val('Rp. 0');
            $('#subtotal').html('Rp. 0');


            $('#SimpanDetail').prop('disabled', true).val('');
            $('div#hasil_pencarian').hide();
        }
    });

    $(document).on('click', '#tombol_scan_barang', function(e){
        tampilkan_data_barang();
        $('.modal-dialog').removeClass('modal-sm');
        $('.modal-dialog').removeClass('modal-md');
        $('.modal-dialog').addClass('modal-lg');
        $('#ModalHeaderCariBarang').html('Cari barang paket promo : <b class="text-primary">' + 
                                         $('#nama_paket_promo').val()) +
                                         '</b>';        
        $('#ModalCariBarang').modal('show');
    });

    function proses_barang($sku, $kode_toko){
        $.ajax({
            url: "<?php echo site_url('paket_harga_spesial/ajax_cari_barang'); ?>",
            type: "POST",
            cache: false,
            data: 'sku=' + $sku +
                  '&kode_toko=' + $kode_toko,
            dataType:'json',
            success: function(json){
                if(json.status == 1){
                    $('#id_barang_pusat').html(json.data['id_barang_pusat']);
                    $('#pencarian_kode_barang').val(json.data['sku']);
                    $('#nama_barang').html(json.data['nama_barang']);
                    $('#harga_satuan').val(to_rupiah(json.data['harga_satuan']));
                    $('#discount_value').removeAttr('disabled');
                    $('#discount_harga').removeAttr('disabled');
                    $('#jumlah_barang').removeAttr('disabled').val();
                    $('#SimpanDetail').removeAttr('disabled').val();

                    $('#ModalCariBarang').modal('hide');
                    HitungSubtotalBarang();
                    setTimeout(function(){ 
                        $('#jumlah_barang').focus();
                    }, 100);
                }else if(json.status == 0){
                    $('#id_barang_pusat').html('');
                    $('#nama_barang').html('Belum ada barang yg dipilih');
                    $('#harga_satuan').html('Rp. 0');
                    $('#jumlah_barang').prop('disabled', true).val('');
                    $('#SimpanDetail').prop('disabled', true).val('');

                    $('.modal-dialog').removeClass('modal-lg');
                    $('.modal-dialog').addClass('modal-sm');
                    $('#ModalHeaderPesan').html('Oops !');
                    $('#ModalContentPesan').html(json.pesan);
                    $('#ModalFooterPesan').html("<button id='info_stok' type='button' class='btn btn-primary'>Ok, Saya Mengerti</button>");
                    $('#ModalPesan').modal('show');
                    $('#ModalCariBarang').modal('hide');

                    setTimeout(function(){ 
                        $('#info_stok').focus();
                    }, 100);
                }
            }
        });
    }

    $(document).on('keyup', '#jumlah_barang', function(){
        HitungSubtotalBarang2();
    });

    $(document).on('keydown', '#jumlah_barang', function(e){
        var charCode = e.which || e.keyCode;
        if(charCode == 9){
            $('#SimpanDetail').focus();
            return false;   
        }
        if(charCode == 13){
            if($('#jumlah_barang').val() !== ''){
                $('#discount_harga').focus();
            } 
        }
    });

    $(document).on('keydown', '#discount_value', function(e){
        var charCode = e.which || e.keyCode;
        if(charCode == 9){
            $('#jumlah_barang').focus();
            return false;
        }else if(charCode == 13){
            setTimeout(function(){
                HitungSubtotalBarang();
                simpan_detail();
            }, 100);
        }
    });

    $(document).on('keyup', '#discount_value', function(){
        HitungSubtotalBarang();
    });

    $(document).on('keydown', '#discount_harga', function(e){
        var charCode = e.which || e.keyCode;
        if(charCode == 9){
            $('#jumlah_barang').focus();
            return false;
        }else if(charCode == 13){
            setTimeout(function(){
                HitungSubtotalBarang2();
                simpan_detail();
            }, 100);
        }
    });

    $(document).on('keyup', '#discount_harga', function(){
        var hrg_disc = $('#discount_harga').val();
            hrg_disc = to_angka(hrg_disc);

        if(hrg_disc > 0){
            $('#discount_harga').val(to_rupiah(hrg_disc));
        }else{
            $('#discount_harga').val('');
        }

        HitungSubtotalBarang2();
    });

    function HitungSubtotalBarang()
    {
        var Harga          = to_angka($('#harga_satuan').val());
        var JumlahBeli     = $('#jumlah_barang').val();
        var Disc           = $('#discount_value').val();

        var Harga_Discount = Disc * parseInt(Harga);
        var Harga_Bersih   = parseInt(Harga) - parseInt(Harga_Discount);

        if(Harga_Discount > 0){
            $('#discount_harga').val(to_rupiah(Harga_Discount));
            $('#harga_bersih').val(to_rupiah(Harga_Bersih));
        }else{
            $('#discount_harga').val('0');
            $('#harga_bersih').val(to_rupiah(Harga));
        }

        var SubTotal = parseInt(Harga-Harga_Discount) * parseInt(JumlahBeli);

        if(SubTotal >= 0){
            SubTotalRp = to_rupiah(SubTotal);        
        }else{
            SubTotalRp = to_rupiah(Harga);
        }
        $('#subtotal').html(SubTotalRp);
    }

    function HitungSubtotalBarang2()
    {
        var Harga               = to_angka($('#harga_satuan').val());
        var JumlahBeli          = $('#jumlah_barang').val();
        var Disc                = to_angka($('#discount_harga').val());
        var Persen_Discount     = Disc / parseInt(Harga);
        var Harga_Bersih        = parseInt(Harga) - parseInt(Disc);

        if (Persen_Discount > 0){
            $('#discount_value').val(Persen_Discount);
            $('#harga_bersih').val(to_rupiah(Harga_Bersih));
        }else{
            $('#discount_value').val('0.00');
            $('#harga_bersih').val(to_rupiah(Harga));
        }

        var SubTotal   = parseInt(Harga-Disc) * parseInt(JumlahBeli);
        
        if(SubTotal >= 0){
            SubTotalRp = to_rupiah(SubTotal);        
        }else{
            SubTotalRp = to_rupiah(Harga);
        }
        $('#subtotal').html(SubTotalRp);
    }

    $(document).on('click', 'button#info_stok', function(){
        $('#ModalPesan').modal('hide');
        $('#pencarian_kode_barang').focus();
    });

    $(document).on('keydown', 'body', function(e){
        var charCode = ( e.which ) ? e.which : event.keyCode;

        if(charCode == 117) //F6
        {
            tampilkan_data_barang();
            $('.modal-dialog').removeClass('modal-sm');
            $('.modal-dialog').removeClass('modal-md');
            $('.modal-dialog').addClass('modal-lg');
            $('#ModalHeaderCariBarang').html('Cari barang paket promo : <b class="text-primary">' + 
                                             $('#nama_paket_promo').val()) +
                                             '</b>';        
            $('#ModalCariBarang').modal('show');
            return false;
        }

        if(charCode == 118) //F7
        {
            $('#catatan').focus();
            return false;
        }

        if(charCode == 119) //F8
        {
            konfirmasi_tahan_transaksi();
            return false;
        }

        if(charCode == 120) //F9
        {
            konfirmasi_simpan_transaksi();
            return false;
        }
    });

    $(document).on('click', 'body', function(){
        $('div#hasil_pencarian_barang').hide();
    });

    function konfirmasi_tahan_transaksi(){
        if($('#nama_paket_promo').val() == ''){
            swal({
                title: "Oops !", 
                text: "Harap masuk nama paket terlebih dahulu", 
                type: "error", 
                confirmButtonClass: 'btn-danger btn-md waves-effect waves-light',
                confirmButtonText: "Ok"
            }, function (isConfirm){
                delay(function(){
                    $('#nama_paket_promo').focus();
                }, 100);
            });
            return;
        }

        if($('#id_kategori_paket').val() == '' || $('#id_kategori_paket').val() == null){
            swal({
                title: "Oops !", 
                text: "Harap pilih kategori paket terlebih dahulu", 
                type: "error", 
                confirmButtonClass: 'btn-danger btn-md waves-effect waves-light',
                confirmButtonText: "Ok"
            }, function (isConfirm){
                delay(function(){
                    $('#id_kategori_paket').select2('open').select2('options');
                }, 100);
            }); 
            return;
        }

        if($('#id_jenis_paket').val() == '0'){
            swal({
                title: "Oops !", 
                text: "Harap pilih jenis paket terlebih dahulu", 
                type: "error", 
                confirmButtonClass: 'btn-danger btn-md waves-effect waves-light',
                confirmButtonText: "Ok"
            }, function (isConfirm){
                delay(function(){
                    $('#id_jenis_paket').focus();
                }, 100);
            });
            return;
        }

        swal({
            title: "Apakah anda yakin ingin menahan transaksi paket promo ini ?",
            type: "info",
            showCancelButton: true,
            confirmButtonClass: 'btn-primary btn-md waves-effect waves-light',
            confirmButtonText: "Iya",
            cancelButtonText: "Batal",
        }, function (isConfirm){
            if(isConfirm){
                TahanTransaksi();
            } 
        });
    }

    $(document).on('click', 'button#Tahan', function(){
        konfirmasi_tahan_transaksi();
    });

    function konfirmasi_simpan_transaksi(){
        if($('#nama_paket_promo').val() == ''){
            swal({
                title: "Oops !", 
                text: "Harap masuk nama paket terlebih dahulu", 
                type: "error", 
                confirmButtonClass: 'btn-danger btn-md waves-effect waves-light',
                confirmButtonText: "Ok"
            }, function (isConfirm){
                delay(function(){
                    $('#nama_paket_promo').focus();
                }, 100);
            });
            return;
        }

        if($('#id_kategori_paket').val() == '' || $('#id_kategori_paket').val() == null){
            swal({
                title: "Oops !", 
                text: "Harap pilih kategori paket terlebih dahulu", 
                type: "error", 
                confirmButtonClass: 'btn-danger btn-md waves-effect waves-light',
                confirmButtonText: "Ok"
            }, function (isConfirm){
                delay(function(){
                    $('#id_kategori_paket').select2('open').select2('options');
                }, 100);
            }); 
            return;
        }

        if($('#id_jenis_paket').val() == '0'){
            swal({
                title: "Oops !", 
                text: "Harap pilih jenis paket terlebih dahulu", 
                type: "error", 
                confirmButtonClass: 'btn-danger btn-md waves-effect waves-light',
                confirmButtonText: "Ok"
            }, function (isConfirm){
                delay(function(){
                    $('#id_jenis_paket').focus();
                }, 100);
            });
            return;
        }

        swal({
            title: "Apakah anda yakin ingin menahan transaksi paket promo ini ?",
            type: "info",
            showCancelButton: true,
            confirmButtonClass: 'btn-primary btn-md waves-effect waves-light',
            confirmButtonText: "Iya",
            cancelButtonText: "Batal",
        }, function (isConfirm){
            if(isConfirm){
                SimpanTransaksi();
            } 
        });
    }

    $(document).on('click', 'button#Simpan', function(){
        konfirmasi_simpan_transaksi();
    });

    $(document).on('click', 'button#Laporan', function(){
        window.open("laporan_paket_promo",'_blank');
    });

    $(document).on('click', 'button#SimpanTransaksi', function(){
        SimpanTransaksi();
    });

    function simpan_detail()
    {
        if($('#pencarian_kode_barang').val() == '-'){
            $('.modal-dialog').removeClass('modal-lg');
            $('.modal-dialog').addClass('modal-sm');
            $('#ModalHeaderPesan').html('Oops !');
            $('#ModalContentPesan').html('Harap pilih barang.');
            $('#ModalFooterPesan').html("<button type='button' class='btn btn-primary' data-dismiss='modal' autofocus>Ok</button>");
            $('#ModalPesan').modal('show');
            return;
        }else if($('#jumlah_barang').val() <= 0){
            $('.modal-dialog').removeClass('modal-lg');
            $('.modal-dialog').addClass('modal-sm');
            $('#ModalHeaderPesan').html('Oops !');
            $('#ModalContentPesan').html('Harap masukan jumlah barang');
            $('#ModalFooterPesan').html("<button type='button' class='btn btn-primary' data-dismiss='modal' autofocus>Ok</button>");
            $('#ModalPesan').modal('show');
            $('#jumlah_barang').focus();
            return;
        }else if($('#tanggal_awal').val() == '' || $('#tanggal_akhir').val() == ''){
            $('.modal-dialog').removeClass('modal-lg');
            $('.modal-dialog').addClass('modal-sm');
            $('#ModalHeaderPesan').html('Oops !');
            $('#ModalContentPesan').html('Harap pilih tanggal awal dan akhir paket');
            $('#ModalFooterPesan').html("<button type='button' class='btn btn-primary' data-dismiss='modal' autofocus>Ok</button>");
            $('#ModalPesan').modal('show');
            $('#jumlah_barang').focus();
            return;
        }else{
            HitungSubtotalBarang2();
            var FormData = "kode_paket_promo="+$('#kode_paket_promo').html(); 
                FormData += "&kode_toko="+ $('#kode_toko').val();
                FormData += "&id_paket_promo_m="+ $('#id_paket_promo_m').val();
                FormData += "&nama_paket_promo="+$('#nama_paket_promo').val();
                FormData += "&tipe_paket="+$('#tipe_paket').val();
                FormData += "&id_kategori_paket="+$('#id_kategori_paket').val();
                FormData += "&id_jenis_paket="+$('#id_jenis_paket').val();
                FormData += "&tanggal_awal="+$('#tanggal_awal').val();
                FormData += "&tanggal_akhir="+$('#tanggal_akhir').val();
                FormData += "&catatan="+encodeURI($('#catatan').val());

                FormData += "&id_barang_pusat="+$('#id_barang_pusat').html();
                FormData += "&sku="+encodeURI($('#pencarian_kode_barang').val());
                FormData += "&jumlah_barang="+$('#jumlah_barang').val();
                FormData += "&harga_satuan="+$('#harga_satuan').val();
                FormData += "&discount_value="+$('#discount_value').val();
                FormData += "&discount_harga="+to_angka($('#discount_harga').val());
                FormData += "&url="+ url;

            $.ajax({
                url: "<?php echo site_url('paket_harga_spesial/simpan_detail'); ?>",
                type: "POST",
                cache: false,
                data: FormData,
                dataType:'json',
                success: function(data){
                    if(data.status == 1){
                        if(url == 'paket_harga_spesial'){
                            swal({
                                title: "Berhasil!", text: data.pesan, type: "success", 
                                confirmButtonText: "Ok!",
                            }, function(isConfirm){
                                window.location.href = data.url;
                            });
                            return;
                        }else{
                            tampilkan_data();
                            swal({
                                title: "Berhasil!", text: data.pesan, type: "success", 
                                confirmButtonText: "Ok!",
                            }, function(isConfirm){
                                setTimeout(function(){ 
                                    $('#pencarian_kode_barang').focus();
                                }, 100);
                            });
                        }

                        $('#judul_tab_induk').html("Data Paket Promo : <span class='badge up bg-primary'>" + 
                                                    data.jumlah_barang_paket +
                                                   " Barang </span>");
                        $('#judul_tab_batal').html("Data Paket Promo Batal : <span class='badge up bg-danger'>" + 
                                                    data.jumlah_barang_paket_batal +
                                                   " Barang </span>");

                        // Bersihkan data pemilihan barang
                        $('#pencarian_kode_barang').val('');
                        $('#id_barang_pusat').html('');
                        $('#nama_barang').html('Belum ada barang yg dipilih');
                        $('#jumlah_barang').val('').prop('disabled', true);
                        $('#harga_satuan').html('0');
                        $('#discount_value').prop('disabled', true).val('0.00');
                        $('#discount_harga').prop('disabled', true).val('Rp. 0');
                        $('#harga_bersih').prop('disabled', true).val('Rp. 0');
                        $('#subtotal').html('Rp. 0');
                        $('#SimpanDetail').prop('disabled', true);
                        ambil_data(data.id_rpm);

                    }else if(data.status == 2){
                        swal({
                            title: "Oops !", 
                            text: data.pesan, 
                            type: "error", 
                            confirmButtonClass: 'btn-danger btn-md waves-effect waves-light',
                            confirmButtonText: "Ok"
                        },function(isConfirm){
                            window.location.href = data.url;
                        });
                    }else if(data.status == 3){
                        $('.modal-dialog').removeClass('modal-lg');
                        $('.modal-dialog').addClass('modal-sm');
                        $('#ModalHeaderPesan').html('Oops !');
                        $('#ModalContentPesan').html(data.pesan);
                        $('#ModalFooterPesan').html("<button type='button' class='btn btn-primary' data-dismiss='modal' autofocus>Ok, Saya Mengerti</button>");
                        $('#ModalPesan').modal('show');
                    }
                },error: function (jqXHR, textStatus, errorThrown){
                    swal({
                        title: "Oops !", 
                        text: "Data paket promo gagal disimpan.", 
                        type: "error", 
                        confirmButtonClass: 'btn-danger btn-md waves-effect waves-light',
                        confirmButtonText: "Ok"
                    },function(isConfirm){
                        window.location.href = url;
                    });
                }
            });
        }
    }

    function edit_detail(id_paket_promo_d, id_paket_promo_m)
    {
        var FormData = "&id_paket_promo_d="+ id_paket_promo_d;
            FormData += "&id_paket_promo_m=" + id_paket_promo_m;
            FormData += "&url=" + url;

        $.ajax({
            url : "<?php echo site_url('paket_harga_spesial/ajax_edit'); ?>",
            type: "POST",
            cache: false,
            data: FormData,
            dataType: "JSON",
            success: function(json){
                $('#id_barang_pusat').html(json.data['id_barang_pusat']);
                $('#pencarian_kode_barang').val(json.data['sku']);
                $('#nama_barang').html(json.data['nama_barang']);
                $('#harga_satuan').val(to_rupiah(json.data['harga_eceran']));
                $('#jumlah_barang').removeAttr('disabled').val(json.data['qty']);
                $('#SimpanDetail').removeAttr('disabled').val();
                $('#discount_value').removeAttr('disabled').val(json.data['discount_value']);
                $('#harga_bersih').val(to_rupiah(json.data['harga_bersih']));
                $('#subtotal').removeAttr('disabled').html(to_rupiah(json.data['subtotal']));
                $('#discount_harga').removeAttr('disabled').val(to_rupiah(json.data['discount_harga'])).focus();
                
            }, error: function (jqXHR, textStatus, errorThrown){
                swal({
                    title: "Gagal!", 
                    text: "Gagal mengambil data dari ajax.", 
                    type: "error", 
                    confirmButtonClass: 'btn-danger btn-md waves-effect waves-light',
                    confirmButtonText: "Ok"
                },function (isConfirm){
                    window.location.href=url;
                });
            }
        });
    }

    function verifikasi_hapus_detail(id_paket_promo_d, id_paket_promo_m)
    {
        $.ajax({
            url : "<?php echo site_url('paket_harga_spesial/ajax_verifikasi_hapus_detail'); ?>",
            type: "POST",
            cache: false,
            data: "id_paket_promo_d=" + id_paket_promo_d +
                  "&id_paket_promo_m=" + id_paket_promo_m +
                  "&url=" + url,
            dataType: "JSON",
            success: function(data){
                if(data.status == 1){
                    $('.modal-dialog').removeClass('modal-lg');
                    $('.modal-dialog').addClass('modal-sm');
                    $('#ModalHeaderPesan').html('Informasi Hapus Barang');
                    $('#ModalContentPesan').html(data.pesan);
                    $('#ModalFooterPesan').html(data.footer);
                    $('#ModalPesan').modal('show');
                }else if(data.status == 0){
                    swal({
                        title: "Gagal!", 
                        text: data.pesan, 
                        type: "error", 
                        confirmButtonClass: 'btn-danger btn-md waves-effect waves-light',
                        confirmButtonText: "Ok"
                    },function (isConfirm){
                        window.location.href=url;
                    });
                }
            },error: function (jqXHR, textStatus, errorThrown){
                swal({
                    title: "Gagal!", 
                    text: "Gagal mengambil data dari ajax.", 
                    type: "error", 
                    confirmButtonClass: 'btn-danger btn-md waves-effect waves-light',
                    confirmButtonText: "Ok"
                },function (isConfirm){
                    window.location.href=url;
                });
            }
        });
    }

    function hapus_detail(id_paket_promo_d, id_paket_promo_m)
    {
        var FormData = "&url=" + url;
            FormData += "&id_paket_promo_m=" + id_paket_promo_m;
            FormData += "&id_paket_promo_d=" + id_paket_promo_d;
            FormData += "&nama_paket_promo=" + $('#nama_paket_promo').val();
            FormData += "&tipe_paket=" + $('#tipe_paket').val();
            FormData += "&id_kategori_paket=" + $('#id_kategori_paket').val();
            FormData += "&id_jenis_paket=" + $('#id_jenis_paket').val();
            FormData += "&tanggal_awal="+$('#tanggal_awal').val();
            FormData += "&tanggal_akhir="+$('#tanggal_akhir').val();
            FormData += "&catatan=" + encodeURI($('#catatan').val());
            FormData += "&keterangan_batal=" + encodeURI($('#keterangan_batal').val());

        $.ajax({
            url : "<?php echo site_url('paket_harga_spesial/ajax_hapus_detail'); ?>",
            type: "POST",
            cache: false,
            data: FormData,
            dataType: "JSON",
            success: function(data){
                if(data.status == 1){
                    $('#judul_tab_induk').html("Data Paket Promo : <span class='badge up bg-primary'>" + 
                                                data.jumlah_barang_paket +
                                               " Barang </span>");
                    $('#judul_tab_batal').html("Data Paket Promo Batal : <span class='badge up bg-danger'>" + 
                                                data.jumlah_barang_paket_batal +
                                               " Barang </span>");
                    reload_table_induk();
                    ambil_data(data.id_pm);
                    swal({
                        title: "Berhasil!", 
                        text: "Data barang paket promo berhasil dihapus.", 
                        type: "success", 
                        confirmButtonText: "Ok"   
                    });
                }else if(data.status == 0){
                    swal({
                        title: data.judul, 
                        text: data.pesan, 
                        type: data.tipe_pesan,
                        confirmButtonClass: data.gaya_tombol, 
                        confirmButtonText: "Ok"
                    },function (isConfirm){
                        window.location.href=data.url;
                    });
                }
            },error: function (jqXHR, textStatus, errorThrown){
                swal({
                    title: "Gagal!", 
                    text: "Gagal menghapus data dari ajax.", 
                    type: "error", 
                    confirmButtonClass: 'btn-danger btn-md waves-effect waves-light',
                    confirmButtonText: "Ok"
                },function (isConfirm){
                    window.location.href=url;
                });
            }
        });
    }

    function SimpanTransaksi()
    {
        var FormData = "kode_paket_promo="+$('#kode_paket_promo').html();
            FormData += "&tipe_paket="+$('#tipe_paket').val(); 
            FormData += "&nama_paket_promo="+$('#nama_paket_promo').val();
            FormData += "&id_paket_promo_m="+$('#id_paket_promo_m').val();
            FormData += "&id_kategori_paket="+$('#id_kategori_paket').val();
            FormData += "&id_jenis_paket="+$('#id_jenis_paket').val();
            FormData += "&tanggal_awal="+$('#tanggal_awal').val();
            FormData += "&tanggal_akhir="+$('#tanggal_akhir').val();
            FormData += "&catatan="+encodeURI($('#catatan').val());

        $.ajax({
            url: "<?php echo site_url('paket_harga_spesial/simpan_transaksi'); ?>",
            type: "POST",
            cache: false,
            data: FormData,
            dataType:'json',
            success: function(data){
                if(data.status == 1){
                    setTimeout(function(){
                        swal({
                            title: "Berhasil!", 
                            text: data.pesan, 
                            type: "success", 
                            confirmButtonText: "Ok",
                        },function (isConfirm){
                            window.location.href = "paket_harga_spesial";
                        });
                    }, 100);
                }else if(data.status == 0){
                    setTimeout(function(){
                        swal({
                            title: "Oops !", 
                            text: data.pesan, 
                            type: "error", 
                            confirmButtonClass: 'btn-danger btn-md waves-effect waves-light',
                            confirmButtonText: "Ok"
                        },function (isConfirm){
                            window.location.href = data.url;
                        });
                    }, 100);
                }   
            },error: function (jqXHR, textStatus, errorThrown){
                swal({
                    title: "Oops !", 
                    text: "Gagal menyimpan transaksi paket promo ini.", 
                    type: "error", 
                    confirmButtonClass: 'btn-danger btn-md waves-effect waves-light',
                    confirmButtonText: "Ok"
                },function (isConfirm){
                    window.location.href = url;
                });
            }
        });
    }

    function TahanTransaksi()
    {
        var FormData = "kode_paket_promo="+$('#kode_paket_promo').html(); 
            FormData += "&nama_paket_promo="+$('#nama_paket_promo').val();
            FormData += "&tipe_paket="+$('#tipe_paket').val();
            FormData += "&id_paket_promo_m="+$('#id_paket_promo_m').val();
            FormData += "&id_kategori_paket="+$('#id_kategori_paket').val();
            FormData += "&id_jenis_paket="+$('#id_jenis_paket').val();
            FormData += "&tanggal_awal="+$('#tanggal_awal').val();
            FormData += "&tanggal_akhir="+$('#tanggal_akhir').val();
            FormData += "&catatan="+encodeURI($('#catatan').val());

        $.ajax({
            url: "<?php echo site_url('paket_harga_spesial/tahan_transaksi'); ?>",
            type: "POST",
            cache: false,
            data: FormData,
            dataType:'json',
            success: function(data){
                if(data.status == 1){
                    setTimeout(function(){
                        swal({
                            title: "Berhasil!", 
                            text: data.pesan, 
                            type: "success", 
                            confirmButtonText: "Ok"
                        },function (isConfirm){
                            window.location.href="paket_harga_spesial";
                        });
                    }, 100);
                }else if(data.status == 0){
                    setTimeout(function(){
                        swal({
                            title: "Oops !", 
                            text: data.pesan, 
                            type: "error", 
                            confirmButtonClass: 'btn-danger btn-md waves-effect waves-light',
                            confirmButtonText: "Ok"
                        },function (isConfirm){
                            window.location.href = data.url;
                        });
                    }, 100);
                }   
            },error: function (jqXHR, textStatus, errorThrown){
                swal({
                    title: "Oops !", 
                    text: "Gagal menahan transaksi paket promo ini.", 
                    type: "error", 
                    confirmButtonClass: 'btn-danger btn-md waves-effect waves-light',
                    confirmButtonText: "Ok"
                },function (isConfirm){
                    // window.location.href = url;
                });
            }
        });
    }

    function AutoCompletedBarang(KataKunci)
    {    
        $('#id_barang_pusat').html('');
        $('#nama_barang').html('Belum ada barang yg dipilih');
        $('#harga_satuan').html('Rp. 0');
        $('#jumlah_barang').prop('disabled', true).val('');
        $('#SimpanDetail').prop('disabled', true).val('');
            
        $('#induk_pencarian_barang').removeClass('has-warning');
        $('#induk_pencarian_barang').addClass('has-has-succes');

        $('#tombol_scan_barang').removeClass('btn-warning');
        $('#tombol_scan_barang').addClass('btn-primary');

        $('#icon_pencarian_barang').removeClass('fa-search');
        $('#icon_pencarian_barang').addClass('fa-spin fa-refresh');

        $('div#hasil_pencarian').hide();

        delay(function(){
            var id_master = $('#id_paket_promo_m').val();

            $.ajax({
                url: "<?php echo site_url('paket_harga_spesial/ajax_kode'); ?>",
                type: "POST",
                cache: false,
                data: 'keyword=' + KataKunci +
                      '&id_master=' + id_master +
                      '&kode_toko=' + $('#kode_toko').val(),
                dataType:'json',
                success: function(json){
                    if(json.status == 1){
                        $('#icon_pencarian_barang').removeClass('fa-spin fa-refresh');
                        $('#icon_pencarian_barang').addClass('fa-search');

                        $('div#hasil_pencarian_barang').show('fast');
                        $('div#hasil_pencarian_barang').html(json.datanya);

                    }else{
                        $('#icon_pencarian_barang').removeClass('fa-spin fa-refresh');
                        $('#icon_pencarian_barang').addClass('fa-search');

                        $('#induk_pencarian_barang').removeClass('has-primary');
                        $('#induk_pencarian_barang').addClass('has-warning');

                        $('#tombol_scan_barang').removeClass('btn-primary');
                        $('#tombol_scan_barang').addClass('btn-warning');
                    }            
                }
            });
        }, 500);
    }

    function to_rupiah(angka){
        var rev     = parseInt(angka, 10).toString().split('').reverse().join('');
        var rev2    = '';
        for(var i = 0; i < rev.length; i++){
            rev2  += rev[i];
            if((i + 1) % 3 === 0 && i !== (rev.length - 1)){
                rev2 += '.';
            }
        }
        return rev2.split('').reverse().join('');

        // Pakai Simbol Rp.
        // return 'Rp. ' + rev2.split('').reverse().join('');
    }

    function to_angka(rp){
        return parseInt(rp.replace(/,.*|\D/g,''),10);
    }

    function check_int(evt){
        var charCode = ( evt.which ) ? evt.which : event.keyCode;
        return ( charCode >= 48 && charCode <= 57 || charCode == 8 );
    }
</script>
<!-- Akhir Script CRUD -->

<!-- Datepicker -->
<script src="assets/plugin/zircos/plugins/moment/moment.js"></script>
<script src="assets/plugin/zircos/plugins/timepicker/bootstrap-timepicker.js"></script>
<script src="assets/plugin/zircos/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js"></script>
<script src="assets/plugin/zircos/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
<script src="assets/plugin/zircos/plugins/clockpicker/js/bootstrap-clockpicker.min.js"></script>
<script src="assets/plugin/zircos/plugins/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- Datepicker -->
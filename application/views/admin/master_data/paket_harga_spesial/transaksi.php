<?php
	if($access_create == '1'){
		$readonly	= '';
		$disabled	= '';
	}else{		
		$readonly	= 'readonly';
		$disabled	= 'disabled';
	}
?>

<div class="content">
	<div class="container">
		<br>		
		<div class="row">
        	<div class="col-sm-12">
	        	<div class="row">
	        		<!-- Awal total harga -->
					<div class="col-sm-4">
						<div class="panel panel-color panel-pink">
							<!-- Default panel contents -->
							<div class="panel-heading">
								<h3 class="panel-title">KODE PAKET
									<span id="kode_paket_promo" name="kode_paket_promo" class="pull-right"> - </span>
								</h3>
							</div>
							
							<span style="display: none;">
								<input type="hidden" id="id_paket_promo_m" name="id_paket_promo_m" value="<?php echo $id_paket_promo_m ?>">
								<input type="hidden" id="tanggal" name="tanggal" class="form-control input-sm" value="<?php echo date('d-m-Y'); ?>" <?php echo $disabled; ?>>
							</span>

							<table id='Total' class='table table-condensed table-striped table-hover'>
								<tbody>
									<tr>
										<td colspan="2">
											<div class="has-primary has-feedback">
		                                        <span class="fa fa-hashtag form-control-feedback"></span>
		                                        <input id="nama_paket_promo" name="nama_paket_promo" type="text" class="form-control input-md text-dark" placeholder="Masukan nama paket promo" autocomplete="off">
		                                    </div>
										</td>
									</tr>
									
									<tr>
										<td colspan="2">
											<div class="row">
												<div class="col-md-4">
													<small>Nama Toko</small>
												</div>
												<div class="col-md-8">
													<select id="kode_toko" name="kode_toko" class="itemName form-control">
														<option value="" class="text-dark ">PILIH TOKO</option>
														<?php foreach ($data_toko AS $dt){ ?>
															<option value="<?php echo $dt->kode_customer_pusat ?>" class="text-dark">
																<?php echo $dt->nama_customer_pusat ?>
															</option>
														<?php } ?>
													</select>														 													
												</div>
											</div>
										</td>
									</tr>

									<tr>
										<td colspan="2">
											<div class="row">
												<div class="col-md-6">
													<small>Tipe</small>
													<select id="tipe_paket" name="tipe_paket" class="itemName form-control">
														<option value="">PILIH PAKET</option>
														<option value="PAKET KHUSUS">PAKET KHUSUS</option>
														<option value="HARGA PROMO">HARGA PROMO</option>														
													</select>																										
												</div>
												<div class="col-md-6">
													<small>Jenis Paket</small>
													<select id="id_jenis_paket" name="id_jenis_paket" class="form-control">
		                                                <option value="0">Pilih jenis paket</option>
		                                                <option value="BESAR">BESAR</option>
		                                                <option value="KECIL">KECIL</option>
		                                            </select>
												</div>
											</div>
										</td>
									</tr>

									<tr>
										<td colspan="2">
											<div class="row">
												<div class="col-md-4">
													<small>Kategori Paket</small>
												</div>
												<div class="col-md-8">
													<div class="input-group has-primary col-md-12 table-responsive">
														<select id="id_kategori_paket" name="id_kategori_paket" class="itemName form-control">
			                                            </select>													
	 													
	 													<span class="input-group-btn">
				                                            <button id='tombol_tambah_kategori' name="tombol_tambah_kategori" type='button' class='btn btn-primary' title="Laporan Transaksi">
																<i class='fa fa-plus'></i>
															</button>
														</span>
													</div>
												</div>
											</div>
										</td>
									</tr>

									<tr>
										<td colspan="2">
											<div class="row">
												<div class="col-md-6">
													<small>Tanggal awal paket</small>
													<input id="tanggal_awal" type="text" class="form-control" data-date-format="yyyy-mm-dd" name="tanggal_awal" autocomplete="off" readonly>
													<span class="help-block"></span>
												</div>

												<div class="col-md-6">
													<small>Tanggal akhir paket</small>
													<input id="tanggal_akhir" type="text" class="form-control" data-date-format="yyyy-mm-dd" name="tanggal_akhir" autocomplete="off" readonly>
													<span class="help-block"></span>
												</div>                            
											</div>
										</td>
									</tr>
									
									<tr>
										<td><small>Total Dasar</small></td>
										<td class="text-left">
											Rp. <span id='total_dasar' class="pull-right text-dark">0</span>
										</td>
									</tr>

									<tr>
										<td><small>Total Paket</small></td>
										<td class="text-left">
											Rp. <span id='total_bersih' class="pull-right text-success">0</span>
										</td>
									</tr>

									<tr>
										<td colspan="2">
											<textarea id='catatan' name='catatan' class='form-control input-sm' rows='2' placeholder="Catatan Transaksi (Jika Ada)" style='resize: vertical; width:100%;'></textarea>
										</td>
									</tr>
									<tr>
										<td></td>
										<td>
											<?php if($access_create == 1 or $access_update == 1){ ?>
												<button type='button' class='btn btn-primary' id='Simpan'>
													<i class='fa fa-paper-plane-o'></i>
												</button>
											<?php } ?>

											<?php if($access_create == 1 or $access_update == 1){ ?>
												<button type='button' class='btn btn-danger' id='Tahan'>
													<i class='fa fa-hand-paper-o'></i>
												</button>
											<?php } ?>

											<button id='Laporan' type='button' class='btn btn-default' title="Laporan Transaksi">
												<i class='fa fa-file-text-o'></i>
											</button>
										</td>								
									</tr>	
								</tbody>
							</table>
						</div>
					</div>
					<!-- Akhir total harga --> 
					
	        		<!-- Awal pilih barang dan daftar transaksi -->
		            <div class="col-sm-8">
		            	<div class="row">
		            		<!-- Awal scan barang dan pilih Barang -->
		            		<div class="col-sm-3">
			                    <table id='TableScan' class="table table-condensed dt-responsive nowrap card-box">
			                    	<thead class="input-sm">
			                    		<tr class="text-dark">
			                    			<th style="width:170px;">SCAN </th>
			                    		</tr>
			                    	</thead>

			                    	<tbody>
			                    		<tr class="text-dark">
			                    			<td>
			                    				 <div id="induk_pencarian_barang" class="input-group col-md-12">
	                                                <input id='pencarian_kode_barang' name="kode_barang" placeholder="SKU / Nama Barang" class="form-control input-sm text-dark" type="text" <?php echo $disabled ?> autocomplete="off">
	                                                <span id="id_barang_pusat" style="display: none;">-</span>
	                                                <span class="input-group-btn">
	                                                    <button id="tombol_scan_barang" name="tombol_scan_barang" type="button" class="btn waves-effect waves-light btn-sm btn-primary" <?php echo $disabled ?>>
	                                                    <i id="icon_pencarian_barang" class="fa fa-search"></i></button>
	                                                </span>
	                                            </div>
	                                            <div id='hasil_pencarian_barang' name="hasil_pencarian_barang"></div>
			                    			</td>
			                    		</tr>
			                    	</tbody>
			                    </table>
						    </div>
						    <!-- Akhir scan barang dan pilih Barang -->
							
							<!-- Awal nama barang dan qty -->
						    <div class='col-sm-9'>
						    	<table id='TableBarang' class='table table-condensed dt-responsive nowrap card-box'>
						    		<thead class='input-sm'>
			                    		<tr class='text-dark'>
			                    			<th style="width:300px;">Nama Barang</th>
			                    			<th style="width:100px">QTY</th>
			                    			<th style="width:20px"></th>
			                    		</tr>
			                    	</thead>
			                    	<tbody class="input-sm">
			                    		<tr class="text-dark">
			                    			<td>
			                    				<span class="form-control input-sm text-dark" id="nama_barang" name="nama_barang">Belum ada barang yg dipilih</span>
			                    			</td>
			                    			
			                    			<td>
			                    				<input type='number' class='form-control input-sm' id='jumlah_barang' name='jumlah_barang' onkeypress='return check_int(event)' disabled>
			                    			</td>

			                    			<td>
			                    				<button type='button' class='btn btn-primary btn-sm' id='SimpanDetail' disabled>
													<i class='fa fa-paper-plane-o'></i>
												</button>
			                    			</td>
			                    		</tr>
			                    	</tbody>
						    	</table>
						    </div>
							<!-- akhir nama barang dan qty -->

							<!-- Awal rincian harga, discount dan subtotal -->
							<div class="col-sm-12">
								<table id='TableHarga' class="table table-condensed dt-responsive nowrap card-box">
									<thead class="input-sm">
										<tr class="text-dark">
											<th style="width:60px;">Harga</th>
											<th style="width:60px;">Disc %</th>
											<th style="width:60px;">Harga Bersih</th>
											<th style="width:100px;">Subtotal </th>
										</tr>
									</thead>
									<tbody class="input-sm">
										<tr class="text-dark">			                    			
											<!-- Harga -->
											<td class="col-md-3">
												<input type='text' id="harga_satuan" name="harga_satuan" class="form-control input-sm text-dark" value='Rp. 0' readonly disabled>
											</td>
																						
											<!-- Discount -->
											<td class="col-md-3">
												<div class="row">
													<div class="col-md-4">
														<input id='discount_value' name='discount_value' type='text' class='form-control input-sm' value="0.00" disabled>		
													</div>

													<div class="col-md-8">
														<input id='discount_harga' name='discount_harga' type='text' value="Rp. 0" class="form-control input-sm" disabled>
													</div>	
												</div>
											</td>

											<!-- Harga bersih-->
											<td class="col-md-3">
												<input id='harga_bersih' name='harga_bersih' type='text' class='form-control input-sm' value='Rp. 0' disabled>
											</td>

											<!-- Subtotal -->
											<td class="col-md-3">
												<span id="subtotal" name="subtotal" class="form-control input-sm">Rp. 0</span>
											</td>
										</tr>
									</tbody>
								</table>
							</div>
							<!-- Akhir rincian harga, discount dan subtotal -->
						</div>

			        	<!-- Awal daftar transaksi --> 
		            	<div class="card-box table-responsive">
		            		<!-- Awal induk tab -->
		                    <ul class="nav nav-tabs tabs-bordered" id="tab_list_retur_pembelian">
		                        <li class="active">
		                            <a href="#tab_induk_paket_promo" id="tab_induk" data-toggle="tab" aria-expanded="true">
		                                <span class="visible-xs"><i class="fa fa-check-square"></i></span>
		                                <span class="hidden-xs" id="judul_tab_induk">Data Paket Barang</span>
		                            </a>
		                        </li>
		                        <li class="">
		                            <a href="#tab_retur_pembelian_batal" id="tab_batal" data-toggle="tab" aria-expanded="true">
		                                <span class="visible-xs"><i class="typcn typcn-home"></i></span>
		                                <span class="hidden-xs" id="judul_tab_batal">Data Paket Barang Batal</span>
		                            </a>
		                        </li>
		                    </ul>
		                    <!-- Akhir induk tab -->

		                    <!-- Awal isi tab -->
		                    <div class="tab-content">
								<!-- Awal daftar retur pembelian barang -->
		                        <div class="tab-pane active" id="tab_induk_paket_promo">
									<table id='TableTransaksi' class="table table-condensed table-striped table-hover dt-responsive nowrap" cellspacing="0" width="100%">
										<thead class="input-sm">
											<tr class="text-dark">
												<th>#</th>
												<th>Tombol</th>
												<th>SKU</th>
												<th>Nama Barang</th>
												<th>Harga</th>
												<th>DISC</th>
												<th>Qty</th>
												<th>SUBTOTAL</th>
												<th>Pegawai Save</th>
												<th>Tanggal Save</th>
												<th>Pegawai Edit</th>
												<th>Tanggal Edit</th>
											</tr>
										</thead>

										<tbody class="input-sm text-dark"></tbody>
									</table>
								</div>
								<!-- Akhir daftar retur pembelian barang -->

								<!-- Awal daftar retur pembelian barang batal -->
		                        <div class="tab-pane" id="tab_retur_pembelian_batal">
									<table id='TableTransaksiBatal' class="table table-condensed table-striped table-hover dt-responsive nowrap" cellspacing="0" width="100%">
										<thead class="input-sm">
											<tr class="text-dark">
												<th>#</th>
												<th>SKU</th>
												<th>Nama Barang</th>
												<th>Harga</th>
												<th>Qty</th>
												<th>Keterangan Hapus</th>
												<th>Pegawai Hapus</th>
												<th>Tanggal Hapus</th>
												<th>Pegawai Save</th>
												<th>Tanggal Save</th>
												<th>Pegawai Edit</th>
												<th>Tanggal Edit</th>
											</tr>
										</thead>

										<tbody class="input-sm text-dark"></tbody>
									</table>
								</div>
								<!-- Akhir daftar retur pembelian barang batal -->

								<!-- Keterangan shortcut -->
								<div class="row">
									<div class="col-sm-12">
										<p><i class='fa fa-keyboard-o fa-fw'></i> <b><small>Shortcut Keyboard : </small></b></p>
										<small>
											F6 = Cari Barang 
											, F7 = Fokus ke field catatan
											, F8 = Tahan Transaksi
											, F9 = Simpan Transaksi
										</small>
									</div>
								</div>
							</div>
						</div>
				        <!-- Akhir daftar transaksi -->
		            </div>
		            <!-- Akhir pilih barang dan daftar transaksi -->	
				</div>
            </div>
        </div>
	</div>
</div>

<div class="modal" id="ModalGue" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class='fa fa-times-circle'></i></button>
				<h4 class="modal-title" id="ModalHeader"></h4>
			</div>
			<div class="modal-body" id="ModalContent"></div>
			<!-- <div class="modal-footer" id="ModalFooter"></div> -->
		</div>
	</div>
</div>

<div class="modal" id="ModalPesan" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class='fa fa-times-circle'></i></button>
				<h4 class="modal-title" id="ModalHeaderPesan"></h4>
			</div>
			<div class="modal-body" id="ModalContentPesan"></div>
			<div class="modal-footer" id="ModalFooterPesan"></div>
		</div>
	</div>
</div>

<div class="modal" id="ModalCariBarang" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class='fa fa-times-circle'></i></button>
				<h5 class="modal-title" id="ModalHeaderCariBarang"></h5>
			</div>
			<div class="modal-body" id="ModalContentCariBarang">
				<table id='TableCariBarang' class="table table-condensed table-striped table-hover dt-responsive nowrap" cellspacing="0" width="100%">
					<thead class="input-sm">
						<tr class="text-dark">
							<th>#</th>
							<th>SKU</th>
							<th style="width:800px;">Nama Barang</th>
							<th>Harga Eceran</th>
							<th>Stok Jual</th>
							<th>Stok Rusak</th>
							<th>Tombol</th>
						</tr>
					</thead>

					<tbody class="input-sm text-dark"></tbody>
				</table>
			</div>
			<div class="modal-footer" id="ModalFooterCariBarang"></div>
		</div>
	</div>
</div>

<script>
	$('#ModalGue').on('hide.bs.modal', function () {
	   setTimeout(function(){ 
	        $('#ModalHeader, #ModalContent, #ModalFooter').html('');
	   }, 500);
	});

	$('#ModalPesan').on('hide.bs.modal', function () {
	   setTimeout(function(){ 
	        $('#ModalHeaderPesan, #ModalContentPesan, #ModalFooterPesan').html('');
	   }, 500);
	});

	$('#ModalCariBarang').on('hide.bs.modal', function () {
	   setTimeout(function(){ 
	        $('#ModalHeaderCariBarang, #ModalFooterCariBarang').html('');
	   }, 500);
	});	
</script>

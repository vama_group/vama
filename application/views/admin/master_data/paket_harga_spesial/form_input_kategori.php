<?php echo form_open('paket_promo/inputan_kategori_paket', array('id' => 'FormTambahKategori')); ?>

<div class="row">
    <div class="col-sm-9">
        <div class="form-horizontal">
            <label class="control-label col-sm-3 small">Nama Kategori Paket</label>
            <div class="col-sm-7">
                <input id="id_kategori" name="id_kategori" placeholder="ID Kategori Paket" class="form-control" type="hidden" disabled>
                <input id="nama_kategori_paket" name="nama_kategori_paket" placeholder="Nama Ketegori Paket" class="form-control" type="text">
                <span class="help-block"></span>
            </div>
            <div class="col-sm-2">
                <button type="button" id="SimpanKategori" name="SimpanKategori" class="btn btn-primary btn-block">Simpan</button>
            </div>
        </div>
    </div>
    
    <div class="col-sm-12">
        <br/>
    </div>

    <div class="col-sm-12 ">
        <table id='TableKategori' class="table table-condensed table-striped table-hover dt-responsive nowrap" cellspacing="0" width="100%">
            <thead class="input-sm">
                <tr class="text-dark">
                    <th style="width: 10px;">#</th>
                    <th style="width: 10px;">Tombol</th>
                    <th>Nama Kategori</th>
                </tr>
            </thead>

            <tbody class="input-sm text-dark"></tbody>
        </table>
    </div>                             
</div>

<script>
    function TambahKategori()
    {
        $.ajax({
            url: $('#FormTambahDebit').attr('action'),
            type: "POST",
            cache: false,
            data: $('#FormTambahDebit').serialize(),
            dataType:'json',
            success: function(json){
                if(json.status == 1){ 
                    $('#FormTambahDebit').each(function(){
                        this.reset();
                    });

                    $('.modal-dialog').removeClass('modal-lg');
                    $('.modal-dialog').addClass('modal-sm');
                    $('#ModalHeader').html('Berhasil');
                    $('#ModalContent').html(json.pesan);
                    $('#ModalFooter').html("<button type='button' class='btn btn-primary' data-dismiss='modal' autofocus>Okay</button>");
                    $('#ModalGue').modal('show');

                }else{
                    $('#ResponseInput').html(json.pesan);
                }
            }
        });
    }

    function simpan_kategori()
    {
        $('#SimpanKategori').text('menyimpan...');
        $('#SimpanKategori').attr('disabled',true);

        var FormData = "id_kategori_paket="+$('#id_kategori').val();
            FormData += "&nama_kategori_paket="+$('#nama_kategori_paket').val(); 

        $.ajax({
            url: "<?php echo site_url('paket_promo/simpan_kategori'); ?>",
            type: "POST",
            cache: false,
            data: FormData,
            dataType:'json',
            success: function(data){
                if(data.status == 1){   
                    reload_table();
                    $('#id_kategori').val('');
                    $('#nama_kategori_paket').val('').focus();
                    $('#SimpanKategori').text('Simpan');
                    $('#SimpanKategori').attr('disabled',false);
                }else{
                    swal({
                        title: "Oops !",
                        text: data.pesan,
                        type: "error",
                        confirmButtonClass: 'btn-danger btn-md waves-effect waves-light',
                        confirmButtonText: "Ok"
                    },function (isConfirm){
                        $('#nama_kategori_paket').val('').focus();
                    });
                }
            },
            error: function (jqXHR, textStatus, errorThrown){
                alert('Error adding / Perbaharui data');
                $('#SimpanKategori').text('Simpan');
                $('#SimpanKategori').attr('disabled',false);

            }
        });
    }

    $(document).on('keyup', '#nama_kategori_paket', function(e){
        var charCode = e.which || e.keyCode;
        if(charCode == 13){
            simpan_kategori();
        }
    });

    $(document).on('click', '#SimpanKategori', function(){
        simpan_kategori();
    });

    function edit_kategori(id_kategori_paket)
    {
        $('.form-horizontal').removeClass('has-error');
        $('.help-block').empty();

        $.ajax({
            url : "<?php echo site_url('paket_promo/ajax_edit_kategori/')?>/" + id_kategori_paket,
            type: "GET",
            dataType: "JSON",
            success: function(data){
                $('#SimpanKategori').text('Perbaharui');
                $('#id_kategori').val(data.id_kategori_paket);
                $('#nama_kategori_paket').val(data.nama_kategori_paket);
            },
            error: function (jqXHR, textStatus, errorThrown){
                alert('Error get data from ajax');
            }
        });
    }

    function hapus_kategori(id_kategori_paket)
    {
        var FormData = "id_kategori_paket="+$('#id_kategori_paket').val();

        $.ajax({
            url : "<?php echo site_url('paket_promo/ajax_hapus_kategori')?>",
            type: "POST",
            cache: false,
            data: FormData,
            dataType: "JSON",
            success: function(data){
                if(data.status == 1){
                    reload_table();
                    $('#nama_kategori_paket').focus();
                }else if(data.status == 0){
                    reload_table();
                }
            }
        });
    }

    function reload_table()
    {
        table.ajax.reload();
    }

    $(document).ready(function(){
        $('.form-horizontal').removeClass('has-error');
        $('.help-block').empty();

        $("input").change(function(){
            $(this).parent().parent().removeClass('has-error');
            $(this).next().empty();
        });
    });

    var Tombol = "<button type='button' class='btn btn-default' data-dismiss='modal'>Tutup</button>";
    $('#ModalFooter').html(Tombol);

    $("#FormTambahDebit").find('input[type=text],textarea,select').filter(':visible:first').focus();

    $('#SimpanTambahDebit').click(function(e){
        e.preventDefault();
        TambahDebit();
    });

    $('#FormTambahDebit').submit(function(e){
        e.preventDefault();
        TambahDebit();
    });

    var no_penjualan = $('#no_penjualan').html(); 
    table            = $('#TableKategori').DataTable({ 
        processing: true,
        serverSide: true,
        order: [],
        lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]],
        pagingType: "full",

        ajax: {
            url: "<?php echo site_url('paket_promo/ajax_list_kategori')?>",
            type: "POST"
        },

        columnDefs: [
            { 
                targets: [ -1 ],
                orderable: false,
            },
        ],
    });
</script>
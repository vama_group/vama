<!-- Start content -->
<div class="content">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="page-title-box">
                    <!-- <div class="clas row">
                        <div class="clas col-md-7">
                        </div>
                        <div class="clas col-md-3"></div>
                    </div> -->
                    <h4 class="page-title text-pink">
                        <i class='mdi mdi-cube-outline'></i><b>  Daftar Paket Promo : </b>
                        <select id="kode_toko" name="kode_toko" class="btn btn-rounded btn-xs btn-default">
                            <!-- <option value="" class="text-dark ">SEMUA TOKO</option> -->
                            <?php foreach ($data_toko AS $dt){ ?>
                                <option value="<?php echo $dt->kode_customer_pusat ?>" class="text-dark">
                                    <?php echo $dt->nama_customer_pusat ?>
                                </option>
                            <?php } ?>
                        </select>
                    </h4>
                    
                    <ol class="breadcrumb p-0 m-0">
                        <li>
                            <b class="small text-muted">Status paket : </b>
                            <select name="status_paket" id="status_paket">
                                <option value="">SEMUA</option>
                                <option value="DI SIAPKAN">DI SIAPKAN</option>
                                <option value="MENUNGGU">MENUNGGU</option>
                                <option value="DI TERIMA">DI TERIMA</option>
                            </select>
                            <?php if($access_create == '1'){ ?>
                                <button id="tambah_transaksi" class="btn btn-rounded btn-xs btn-success">
                                    <i class="fa fa-plus"></i> TAMBAH
                                </button>
                            <?php } ?>
                            
                            <button id="ReferesData" class="btn btn-rounded btn-xs btn-primary" onclick="refresh_data()">
                                <i class="fa fa-refresh"></i> 
                                REFRESH
                            </button>                                                                            
                        </li>                        
                    </ol>

                    <div class="clearfix"></div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12">
                <div class="card-box table-responsive">
                    <!-- Awal induk tab -->
                    <ul class="nav nav-tabs tabs-bordered">
                        <li class="active">
                            <a id="tab_induk" href="#tab_paket_promo_induk" data-toggle="tab" aria-expanded="false">
                                <span class="visible-xs"><i class="fa fa-check-square"></i></span>
                                <span class="hidden-xs">Paket Promo Induk</span>
                            </a>
                        </li>
                        <li class="">
                            <a id="tab_detail" href="#tab_paket_promo_detail" data-toggle="tab" aria-expanded="true">
                                <span class="visible-xs"><i class="fa fa-recycle"></i></span>
                                <span class="hidden-xs">Paket Promo Detail</span>
                            </a>
                        </li>
                        <li class="">
                            <a id="tab_batal" href="#tab_paket_promo_batal" data-toggle="tab" aria-expanded="true">
                                <span class="visible-xs"><i class="fa fa-recycle"></i></span>
                                <span class="hidden-xs">Paket Promo Batal</span>
                            </a>
                        </li>
                    </ul>
                    <!-- Akhir induk tab -->
                    
                    <div class="tab-content">
                    	<!-- Awal data paket promo induk -->
                		<div class="tab-pane active" id="tab_paket_promo_induk">
							<!-- Daftar transaksi -->
							<table id='table_induk' class="table table-condensed table-striped table-hover dt-responsive nowrap" cellspacing="0" width="100%">
								<thead class="input-sm">
									<tr class="text-dark">
										<th>#</th>
										<th>Tombol</th>
                                        <th>Status</th>
										<th>Kode Paket</th>
										<th>Nama Paket</th>
                                        <th>Periode</th>
										<th>Harga Dasar</th>
										<th>Harga Paket</th>
										<th>Jml Item</th>
                                        <th>Tipe Paket</th>
										<th>Kategori Paket</th>
                                        <th>Jenis Paket</th>
										<th>Pegawai Save</th>
										<th>Tanggal Save</th>
										<th>Pegawai Edit</th>
										<th>Tanggal Edit</th>
									</tr>
								</thead>

								<tbody class="input-sm text-dark"></tbody>
							</table>
						</div>
						<!-- Akhir data paket promo induk -->

						<!-- Awal data paket promo detail -->
						<div class="tab-pane" id="tab_paket_promo_detail">
                            <table id="table_detail" class="table table-condensed table-striped table-hover dt-responsive nowrap" cellspacing="0" width="100%">
                                <thead class="input-sm">
                                    <tr>
                                    	<th>#</th>
                                        <th>Tombol</th>
                                        <th>Status</th>
                                        <th>Nama Paket</th>
                                        <th>Periode</th>
                                        <th>SKU</th>
                                        <th style='width: 1350px;'>Nama Barang</th>
                                        <th>Harga</th>
                                        <th>QTY</th>
                                        <th>Pegawai Save</th>
                                        <th>Tanggal Save</th>
                                        <th>Pegawai Edit</th>
                                        <th>Tanggal Edit</th>
                                        <!-- <th style="width:125px;">Action</th> -->
                                    </tr>
                                </thead>

                                <tbody class="input-sm">
                                </tbody>
                            </table>
                        </div>
                        <!-- Akhir data paket promo detail -->

                        <!-- Awal data paket promo batal -->
						<div class="tab-pane" id="tab_paket_promo_batal">
                            <table id="table_batal" class="table table-condensed table-striped table-hover dt-responsive nowrap" cellspacing="0" width="100%">
                                <thead class="input-sm">
                                    <tr>
                                    	<th>#</th>
                                        <th>Nama Paket</th>
                                        <th>Periode</th>
                                        <th>SKU</th>
                                        <th>Nama Barang</th>
                                        <th>Harga</th>
                                        <th>QTY</th>
                                        <th>KETERANGAN</th>
                                        <th>Pegawai Batal</th>
                                        <th>Tanggal Batal</th>
                                        <th>Pegawai Save</th>
                                        <th>Tanggal Save</th>
                                        <th>Pegawai Edit</th>
                                        <th>Tanggal Edit</th>
                                    </tr>
                                </thead>

                                <tbody class="input-sm">
                                </tbody>
                            </table>
                        </div>
                        <!-- Akhir data paket promo batal -->
					</div>
                </div>
            </div>
        </div>
    </div> <!-- container -->
</div> 
<!-- content -->

<div class="modal" id="ModalGue" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class='fa fa-times-circle'></i></button>
                <h4 class="modal-title" id="ModalHeader"></h4>
            </div>
            <div class="modal-body" id="ModalContent"></div>
            <div class="modal-footer" id="ModalFooter"></div>
        </div>
    </div>
</div>

<script>
    $('#ModalGue').on('hide.bs.modal', function () {
       setTimeout(function(){ 
            $('#ModalHeader, #ModalContent, #ModalFooter').html('');
       }, 500);
    });
</script>

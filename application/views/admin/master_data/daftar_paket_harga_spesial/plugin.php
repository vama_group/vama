<!-- Awal CSS -->
<link href="assets/plugin/zircos/plugins/datatables/jquery.dataTables.min.css" rel="stylesheet" type="text/css"/>
<link href="assets/plugin/zircos/plugins/datatables/responsive.bootstrap.min.css" rel="stylesheet" type="text/css"/>
<link href="assets/plugin/zircos/plugins/datatables/dataTables.bootstrap.min.css" rel="stylesheet" type="text/css"/>
<link href="assets/plugin/zircos/material-design/assets/css/tambahan.css" rel="stylesheet" type="text/css"/>
<!-- Akhir CSS -->

<!-- Awal JS -->
<script src="assets/plugin/zircos/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="assets/plugin/zircos/plugins/datatables/dataTables.bootstrap.js"></script>
<script src="assets/plugin/zircos/material-design/assets/js/detect.js"></script>
<script src="assets/plugin/zircos/material-design/assets/js/jquery.slimscroll.js"></script>
<script src="assets/plugin/zircos/plugins/datatables/dataTables.responsive.min.js"></script>
<script src="assets/plugin/zircos/plugins/datatables/responsive.bootstrap.min.js"></script>
<script src="assets/plugin/zircos/material-design/assets/pages/jquery.datatables.init.js"></script>
<!-- Akhir JS -->

<!-- Awal Sweet-Alert  -->
<link href="assets/plugin/zircos/plugins/bootstrap-sweetalert/sweet-alert.css" rel="stylesheet" type="text/css">
<script src="assets/plugin/zircos/plugins/bootstrap-sweetalert/sweet-alert.min.js"></script>
<!-- Akhir Sweet-Alert  -->

<script>
    $('#navbar').removeClass('navbar-default');
    $('#navbar').addClass('navbar-pink');
    $('#JudulHalaman').html('Daftar Paket Promo');

    var table_induk  = $('#table_induk').DataTable();
    var table_detail = $('#table_detail').DataTable();
    var table_batal  = $('#table_batal').DataTable();
    var id_tab       = 1;

    $(document).ready(function(){
        $("#pencarian_kode").focus();
        $("#wrapper").removeClass('forced');
        $("#wrapper").addClass('forced enlarged');
        TampilkanDataPaket();
    });

    $(document).on('click', 'button#tambah_transaksi', function(){
        window.open("paket_harga_spesial");
    });

    $(document).on('change', '#status_paket', function(){
        if(id_tab == 1){
            TampilkanDataPaket();
        }else if(id_tab == 2){
            TampilkanDataPaketDetail();
        }else if(id_tab == 3){
            TampilkanDataPaketBatal();
        }
    });

    $(document).on('change', '#kode_toko', function(){
        if(id_tab == 1){
            TampilkanDataPaket();
        }else if(id_tab == 2){
            TampilkanDataPaketDetail();
        }else if(id_tab == 3){
            TampilkanDataPaketBatal();
        }
    });

    function TampilkanDataPaket(){
        table_induk.clear();table_induk.destroy();
        table_induk=$('#table_induk').DataTable(
        {
            ordering: true,
            bFilter:true,
            processing:true,
            serverSide:true,
            order:[],
            lengthMenu: [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
            pagingType: "full",

            oLanguage:{
                sLoadingRecords:'Silakan Tunggu',
                sProcessing:"<img src='assets/loader/cubeloader2.2.gif' width='50' height='50'> Harap tunggu",
                sZeroRecords:"Belum ada data untuk ditampilkan",
                sSearch : "Pencarian data"
            },
            ajax:{
                url: "<?php echo site_url('daftar_paket_harga_spesial/ajax_list')?>",
                type: "POST",
                data: {
                    'status_paket' : $('#status_paket').val(),
                    'kode_toko' : $('#kode_toko').val()
                }
            },
            columnDefs: [{ 
                "targets": [ -1 ],
                "orderable": false,
            },],
        });
    }

    function TampilkanDataPaketDetail(){
        table_detail.clear();table_detail.destroy();
        table_detail=$('#table_detail').DataTable(
        {
            ordering: true,
            bFilter:true,
            processing:true,
            serverSide:true,
            order:[],
            lengthMenu: [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
            pagingType: "full",

            oLanguage:{
                sLoadingRecords:'Silakan Tunggu',
                sProcessing:"<img src='assets/loader/cubeloader2.2.gif' width='50' height='50'> Harap tunggu",
                sZeroRecords:"Belum ada data untuk ditampilkan",
                sSearch : "Pencarian data"
            },
            ajax:{
                url: "<?php echo site_url('daftar_paket_harga_spesial/ajax_list_detail')?>",
                type: "POST",
                data: {
                    'status_paket' : $('#status_paket').val(),
                    'kode_toko' : $('#kode_toko').val()
                }
            },
            columnDefs: [{ 
                "targets": [ -1 ],
                "orderable": false,
            },],
        });
    }
    
    function TampilkanDataPaketBatal(){
        table_batal.clear();table_batal.destroy();
        table_batal=$('#table_batal').DataTable(
        {
            ordering: true,
            bFilter:true,
            processing:true,
            serverSide:true,
            order:[],
            lengthMenu: [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
            pagingType: "full",

            oLanguage:{
                sLoadingRecords:'Silakan Tunggu',
                sProcessing:"<img src='assets/loader/cubeloader2.2.gif' width='50' height='50'> Harap tunggu",
                sZeroRecords:"Belum ada data untuk ditampilkan",
                sSearch : "Pencarian data"
            },
            ajax:{
                url: "<?php echo site_url('daftar_paket_harga_spesial/ajax_list_batal')?>",
                type: "POST",
                data: {
                    'status_paket' : $('#status_paket').val(),
                    'kode_toko' : $('#kode_toko').val()
                }
            },
            columnDefs: [{ 
                "targets": [ -1 ],
                "orderable": false,
            },],
        });
    }

    var delay = (function (){
        var timer = 0;
        return function (callback, ms) {
            clearTimeout(timer);
            timer = setTimeout(callback, ms);
        };
    })();

    $(document).on('click', '#tab_induk', function(){
        id_tab = 1;
        TampilkanDataPaket();
    });

    $(document).on('click', '#tab_detail', function(){
        id_tab = 2;
        TampilkanDataPaketDetail();
    });

    $(document).on('click', '#tab_batal', function(){
        id_tab = 3;
        TampilkanDataPaketBatal();
    });

    function refresh_data(){
        if(id_tab == 1){
            TampilkanDataPaket();
        }else if(id_tab == 2){
            TampilkanDataPaketDetail();
        }else if(id_tab == 3){
            TampilkanDataPaketBatal();
        }
    };

    function edit_paket_promo(id_paket_promo_m)
    {
        if((id_paket_promo_m) !== ''){
            var FrmData = "&id="+id_paket_promo_m;
            window.open("paket_harga_spesial/transaksi/?" + FrmData,'_blank');
        }else{
            $('.modal-dialog').removeClass('modal-lg');
            $('.modal-dialog').addClass('modal-sm');
            $('#ModalHeader').html('Oops !');
            $('#ModalContent').html('Paket promo tidak dikenal');
            $('#ModalFooter').html("<button type='button' class='btn btn-primary' data-dismiss='modal' autofocus>Ok</button>");
            $('#ModalGue').modal('show');
        }
    }
</script>
<!-- Akhir Script CRUD -->

<!-- Bootstrap modal -->
<div class="modal fade" id="modal_form" role="dialog">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h5 class="modal-title">Kategori Barang</h5>
            </div>
            <div class="modal-body form">
                <form action="#" id="form" class="form-group">
                    <div class="row">
                        <input type="hidden" value="" name="id_kategori_barang"/> 
                        <div class="form-horizontal">
                            <label class="control-label col-md-3 small">Nama Jenis Barang</label>
                            <div class="col-md-9 form-horizontal">
                                <div id="induk_pencarian" class="input-group">
                                    <input id='nama_jenis' name="nama_jenis" placeholder="Masukan nama jenis barang" class="form-control input-md text-dark" type="text">
                                    <input id='kode_jenis' name="kode_jenis" class="form-control text-dark" type="text" style="display: none">
                                    <span class="input-group-btn">
                                        <button id="tombol_scan" name="tombol_scan" type="button" class="btn waves-effect waves-light btn-md btn-primary">
                                        <i id="icon_pencarian" class="fa fa-search"></i></button>
                                    </span>
                                </div>
                                <div id='hasil_pencarian_jenis_barang' name="hasil_pencarian_jenis_barang"></div>
                                <span class="help-block"></span>
                            </div>
                        </div>

                        <div class="form-horizontal">
                            <label class="control-label col-md-3 small">Nama Kategori Barang</label>
                            <div class="col-md-9">
                                <input id="nama_kategori_barang" name="nama_kategori_barang" placeholder="Masukan nama kategori barang" class="form-control" type="text">
                                <span class="help-block"></span>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" id="btnSave" onclick="save()" class="btn btn-primary">Simpan</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- End Bootstrap modal -->


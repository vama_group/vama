<!-- Awal CSS -->
<link href="assets/plugin/zircos/plugins/datatables/jquery.dataTables.min.css" rel="stylesheet" type="text/css"/>
<link href="assets/plugin/zircos/plugins/datatables/responsive.bootstrap.min.css" rel="stylesheet" type="text/css"/>
<link href="assets/plugin/zircos/plugins/datatables/dataTables.bootstrap.min.css" rel="stylesheet" type="text/css"/>
<link href="assets/plugin/zircos/material-design/assets/css/tambahan.css" rel="stylesheet" type="text/css"/>

<!-- Awal JS -->
<script src="assets/plugin/zircos/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="assets/plugin/zircos/plugins/datatables/dataTables.bootstrap.js"></script>
<script src="assets/plugin/zircos/plugins/datatables/dataTables.responsive.min.js"></script>
<script src="assets/plugin/zircos/plugins/datatables/responsive.bootstrap.min.js"></script>
<script src="assets/plugin/zircos/material-design/assets/pages/jquery.datatables.init.js"></script>

<!-- Awal Sweet-Alert  -->
<link href="assets/plugin/zircos/plugins/bootstrap-sweetalert/sweet-alert.css" rel="stylesheet" type="text/css">
<script src="assets/plugin/zircos/plugins/bootstrap-sweetalert/sweet-alert.min.js"></script>
<!-- Akhir Sweet-Alert  -->

<script type="text/javascript">
    var save_method;
    var table;

    $(document).ready(function(){
        $('#JudulHalaman').html('Kategori Barang - VAMA');
        
        table = $('#table').DataTable({ 
            processing: true,
            serverSide: true,
            order: [],

            ajax: {
                url: "<?php echo site_url('kategori_barang/ajax_list')?>",
                type: "POST",
                data: {'id_periode_stok_pusat' : '1'}
            },

            columnDefs: [
                { 
                    targets: [ -1 ],
                    orderable: false,
                },
            ],
        });

        var _swal = window.swal;
        window.swal = function(){
            var previousWindowKeyDown = window.onkeydown;
            _swal.apply(this, Array.prototype.slice.call(arguments, 0));
            window.onkeydown = previousWindowKeyDown;
        };
        
        $("input").change(function(){
            $(this).parent().parent().removeClass('has-error');
            $(this).next().empty();
        });

        $("input-group").change(function(){
            $(this).parent().parent().removeClass('has-error');
            $(this).next().empty();
        });

        $("textarea").change(function(){
            $(this).parent().parent().removeClass('has-error');
            $(this).next().empty();
        });
    });

    function reload_table()
    {
        table.ajax.reload(null,false);
    }

    $(document).on('keyup', '#nama_jenis', function(e){
        if($(this).val() !== ''){
            var charCode = e.which || e.keyCode;
            if(charCode == 40){
                if($('div#hasil_pencarian_jenis_barang li.autocomplete_active').length > 0){
                    var Selanjutnya = $('div#hasil_pencarian_jenis_barang li.autocomplete_active').next();
                    $('div#hasil_pencarian_jenis_barang li.autocomplete_active').removeClass('autocomplete_active');
                    Selanjutnya.addClass('autocomplete_active');
                }else{
                    $('div#hasil_pencarian_jenis_barang li:first').addClass('autocomplete_active');
                }
            }else if(charCode == 38){
                if($('div#hasil_pencarian_jenis_barang li.autocomplete_active').length > 0){
                    var Sebelumnya = $('div#hasil_pencarian_jenis_barang li.autocomplete_active').prev();
                    $('div#hasil_pencarian_jenis_barang li.autocomplete_active').removeClass('autocomplete_active');
                    Sebelumnya.addClass('autocomplete_active');
                }else{
                    $('div#hasil_pencarian_jenis_barang li:first').addClass('autocomplete_active');
                }
            }else if(charCode == 13){
                // Ambil dari daftar hasl pencarian
                var kd_jenis   = $('div#hasil_pencarian_jenis_barang li.autocomplete_active span#data_kode_jenis').html();
                var nm_jenis   = $('div#hasil_pencarian_jenis_barang li.autocomplete_active span#data_nama_jenis').html();
                
                $('#hasil_pencarian_jenis_barang').hide();
        
                $('#kode_jenis').val(kd_jenis);            
                $('#nama_jenis').val(nm_jenis);
                $('#nama_kategori').focus();
            }else{
                AutoCompletedJenisBarang($(this).width(), $(this).val(), $(this).parent().parent().index());
            }
        }else{
            $('#kode_jenis').val('');            
            $('div#hasil_pencarian_jenis_barang').hide();
        }
    });

    $(document).on('click', '#daftar_autocompleted_jenis_barang li', function(){    
        var kd_jenis   = $(this).find('#data_kode_jenis').html();
        var nm_jenis   = $(this).find('#data_nama_jenis').html();
        
        $('#hasil_pencarian_jenis_barang').hide();
        $('#kode_jenis').val(kd_jenis);            
        $('#nama_jenis').val(nm_jenis);
        $('#nama_kategori').focus();
    });

    $(document).on('click', '#tombol_scan', function(e){
        AutoCompletedJenisBarang($('#nama_jenis').width(), $('#nama_jenis').val(), $('#nama_jenis').parent().parent().index());
    });

    // Untuk menunda saat menjalankan fungsi ajax di keyup atau change
    var delay = (function () {
        var timer = 0;
        return function (callback, ms) {
            clearTimeout(timer);
            timer = setTimeout(callback, ms);
        };
    })();

    function AutoCompletedJenisBarang(Lebar, KataKunci, Indexnya)
    {
        $('#induk_pencarian').removeClass('has-warning');
        $('#induk_pencarian').addClass('has-has-succes');

        $('#icon_pencarian').removeClass('fa-search');
        $('#icon_pencarian').addClass('fa-spin fa-refresh');

        $('#tombol_scan').removeClass('btn-warning');
        $('#tombol_scan').addClass('btn-primary');
        $('#tombol_scan').prop('disabled', true);

        $('div#hasil_pencarian_jenis_barang').hide();
        
        delay(function(){
            var Lebar   = Lebar + 25;
            $.ajax({
                url: "<?php echo site_url('jenis_barang/ajax_pencarian_jenis_barang'); ?>",
                type: "POST",
                cache: false,
                data: 'keyword=' + KataKunci,
                dataType:'json',
                success: function(json){
                    if(json.status == 1){
                        $('div#hasil_pencarian_jenis_barang').show('fast');
                        $('div#hasil_pencarian_jenis_barang').html(json.datanya);
                        $('#kode_jenis').html('-');

                        $('#icon_pencarian').removeClass('fa-spin fa-refresh');
                        $('#icon_pencarian').addClass('fa-search');
                        $('#tombol_scan').removeAttr('disabled');
                    }else{
                        $('#icon_pencarian').removeClass('fa-spin fa-refresh');
                        $('#icon_pencarian').addClass('fa-search');

                        $('#induk_pencarian').removeClass('has-succes');
                        $('#induk_pencarian').addClass('has-warning');

                        $('#tombol_scan').removeClass('btn-primary');
                        $('#tombol_scan').addClass('btn-warning');
                        $('#tombol_scan').removeAttr('disabled')
                    }
                }
            });
        }, 500);
    }

    function add_kategori_barang()
    {
        save_method = 'add';
        $('[name="nama_jenis"]').attr('disabled',false);
        $('[name="tombol_scan"]').attr('disabled',false);
        $('#daftar_autocompleted_jenis_barang').hide();
        $('#form')[0].reset();
        $('.form-horizontal').removeClass('has-error');
        $('.help-block').empty();
        $('#modal_form').modal('show');
        $('.modal-title').text('Tambah Kategori Barang');
    }

    function edit_kategori_barang(id_kategori_barang)
    {
        save_method = 'update';
        $('#daftar_autocompleted_jenis_barang').hide();
        $('#form')[0].reset();
        $('.form-horizontal').removeClass('has-error');
        $('.help-block').empty();

        $.ajax({
            url : "<?php echo site_url('kategori_barang/ajax_edit'); ?>",
            type: "POST",
            data: 'id_kategori_barang=' + id_kategori_barang,
            dataType: "JSON",
            success: function(data){
                $('[name="kode_jenis"]').val(data.kode_jenis);            
                $('[name="nama_jenis"]').val(data.nama_jenis);            
                $('[name="id_kategori_barang"]').val(data.id_kategori_barang);
                $('[name="nama_kategori_barang"]').val(data.nama_kategori);
                $('#modal_form').modal('show');
                $('.modal-title').text('Edit Kategori Barang');
                $('[name="nama_jenis"]').attr('disabled',true);
                $('[name="tombol_scan"]').attr('disabled',true);
            },
            error: function (jqXHR, textStatus, errorThrown){
                swal({
                    title: "Gagal!", 
                    text: "Data kategori barang gagal untuk ditampilkan.", 
                    type: "error", 
                    confirmButtonClass: 'btn-danger btn-md waves-effect waves-light',
                    confirmButtonText: "Ok"
                }, function(isConfirm){
                    window.location.href = "kategori_barang";
                });
            }
        });
    }

    function save()
    {
        var url;
        var judul;
        var pesan;
        if(save_method == 'add'){
            judul   = "Yakin ingin menambahkan kategori barang ?";
            pesan   = "Jenis Barang : " + $('#nama_jenis').val() + ", " + 
                      "Kategori Barang : " + $('#nama_kategori_barang').val();
            url     = "<?php echo site_url('kategori_barang/ajax_add')?>";
        }else{
            judul   = "Yakin ingin memperbaharui supplier ?";
            pesan   = "Jenis Barang : " + $('#nama_jenis').val() + ", " + 
                      "Kategori Barang : " + $('#nama_kategori_barang').val();
            url     = "<?php echo site_url('kategori_barang/ajax_update')?>";
        }

        swal({
            title: judul,
            text: pesan,
            type: "info",
            showCancelButton: true,
            confirmButtonClass: 'btn-primary btn-md waves-effect waves-light',
            confirmButtonText: "Iya",
            cancelButtonText: "Batal",
            closeOnConfirm: false,
        }, function (isConfirm){
            if(isConfirm){
                $('#btnSave').text('menyimpan...');
                $('#btnSave').attr('disabled',true);

                $.ajax({
                    url : url,
                    type: "POST",
                    data: $('#form').serialize(),
                    dataType: "JSON",
                    success: function(data){
                        if(data.status){
                            $('#modal_form').modal('hide');
                            reload_table();

                            swal({
                                title: "Berhasil!", 
                                text: "Data kategori barang berhasil disimpan.", 
                                type: "success", 
                                confirmButtonText: "Ok"
                            });
                        }else{
                            for (var i = 0; i < data.inputerror.length; i++){
                                $('[name="'+data.inputerror[i]+'"]').parent().parent().addClass('has-error'); 
                                $('[name="'+data.inputerror[i]+'"]').next().text(data.error_string[i]); 
                            }

                            swal({
                                title: "Gagal!", 
                                text: "Data kategori barang gagal disimpan.", 
                                type: "error", 
                                confirmButtonClass: 'btn-danger btn-md waves-effect waves-light',
                                confirmButtonText: "Ok"
                            });
                        }

                        $('#btnSave').text('Simpan');
                        $('#btnSave').attr('disabled',false);
                    },
                    error: function (jqXHR, textStatus, errorThrown){
                        swal({
                            title: "Gagal!", 
                            text: "Data kategori barang gagal disimpan.", 
                            type: "error", 
                            confirmButtonClass: 'btn-danger btn-md waves-effect waves-light',
                            confirmButtonText: "Ok"
                        }, function(isConfirm){
                            window.location.href = "kategori_barang";
                        });
                    }
                });
            }
        });
    }

    function verifikasi_delete(id_kategori_barang)
    {
        $.ajax({
            url : "<?php echo site_url('kategori_barang/ajax_verifikasi_delete') ;?>",
            type: "POST",
            cache: false,
            data: 'id_kategori_barang=' + id_kategori_barang,
            dataType: "JSON",
            success: function(data){
                $('.modal-dialog').removeClass('modal-sm');
                $('.modal-dialog').addClass('modal-md');
                $('#ModalHeader').html('Informasi Hapus Kategori Barang');
                $('#ModalContent').html(data.pesan);
                $('#ModalFooter').html(data.footer);
                $('#ModalGue').modal('show');

            },error: function (jqXHR, textStatus, errorThrown){
                swal({
                title: "Gagal!", 
                text: "Data kategori barang gagal untuk ditampilkan", 
                type: "error", 
                confirmButtonClass: 'btn-danger btn-md waves-effect waves-light',
                confirmButtonText: "Ok"
                }, function(isConfirm){
                    window.location.href = "kategori_barang";
                });
            }
        });
    }

    function delete_kategori_barang(id_kategori_barang)
    {
        $.ajax({
            url : "<?php echo site_url('kategori_barang/ajax_delete'); ?>",
            type: "POST",
            cache: false,
            data: 'id_kategori_barang=' + id_kategori_barang,
            dataType: "JSON",
            success: function(data){
                reload_table();
                swal({
                    title: "Berhasil!", 
                    text: "Data kategori barang berhasil dihapus.", 
                    type: "success", 
                    confirmButtonText: "Ok"
                });
            },
            error: function (jqXHR, textStatus, errorThrown){
                swal({
                    title: "Gagal!", 
                    text: "Data kategori barang gagal dihapus.", 
                    type: "error", 
                    confirmButtonClass: 'btn-danger btn-md waves-effect waves-light',
                    confirmButtonText: "Ok"
                }, function(isConfirm){
                    window.location.href = "kategori_barang";
                });
            }
        });
    }
</script>
<!-- Akhir Script CRUD -->
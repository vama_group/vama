<!-- Bootstrap modal -->
<div class="modal fade" id="modal_form" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h5 class="modal-title">Pegawai Toko</h5>
            </div>
            
            <div class="modal-body form">
                <!-- Awal induk tab -->
                <ul class="nav nav-tabs" id="tab_pegawai">
                    <li class="active">
                        <a id="tab_detail_pegawai" href="#tab_data_detail_pegawai" data-toggle="tab" aria-expanded="true">
                            <span class="visible-xs"><i class="fa fa-check-square"></i></span>
                            <span class="hidden-xs">Data Pegawai</span>
                        </a>
                    </li>
                    <li class="">
                        <a id="tab_otoritas" href="#tab_data_otoritas" data-toggle="tab" aria-expanded="true">
                            <span class="visible-xs"><i class="mdi mdi-access-point-network"></i></span>
                            <span class="hidden-xs">Data Otoritas</span>
                        </a>
                    </li>
                    <li class="">
                        <a href="#tab_foto_pegawai" id="tab_foto" data-toggle="tab" aria-expanded="true">
                            <span class="visible-xs"><i class="mdi mdi-camera"></i></span>
                            <span class="hidden-xs">Foto</span>
                        </a>
                    </li>
                    <li class="">
                        <a id="tab_rubah_password" href="#tab_data_rubah_password" data-toggle="tab" aria-expanded="true">
                            <span class="visible-xs"><i class="mdi mdi-account-key"></i></span>
                            <span class="hidden-xs">Rubah Password</span>
                        </a>
                    </li>
                </ul>
                <!-- Akhir induk tab -->

                <!-- Awal isi tab -->
                <div class="tab-content">
                    <!-- Awal data pegawai -->
                    <div class="tab-pane active" id="tab_data_detail_pegawai" name="tab_data_detail_pegawai">
                        <form action="#" id="form" class="form-group">
                            <input type="hidden" value="" id="id_pegawai_toko" name="id_pegawai_toko"> 
                            <div class="form-body">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-horizontal">
                                            <label class="control-label col-md-3 small">Toko</label>
                                            <div class="col-md-9">
                                                <select id="kode_toko" name="kode_toko" class="form-control">
                                                    <?php foreach ($list_toko as $toko) { ?>
                                                    <option value="<?php echo $toko->kode_customer_pusat ?>">
                                                    <?php echo $toko->nama_customer_pusat ?>
                                                    </option>
                                                    <?php } ?>
                                                </select>
                                                <span class="help-block"></span>
                                            </div>
                                        </div>

                                        <div class="form-horizontal">
                                            <label class="control-label col-md-3 small">Nama</label>
                                            <div class="col-md-9">
                                                <input id="nama_pegawai_toko" name="nama_pegawai_toko" placeholder="Nama" class="form-control" type="text">
                                                <span class="help-block"></span>
                                            </div>
                                        </div>

                                        <div class="form-horizontal">
                                            <label class="control-label col-md-3 small">Email</label>
                                            <div class="col-md-9">
                                                <input id="email" name="email" placeholder="Email" class="form-control" type="email">
                                                <span class="help-block"></span>
                                            </div>
                                        </div>

                                        <div id="kotak_password" class="form-horizontal">
                                            <label class="control-label col-md-3 small">Password</label>
                                            <div class="col-md-9">
                                                <input id="password" name="password" placeholder="Password" class="form-control" type="password">
                                                <span class="help-block"></span>
                                            </div>
                                        </div>                                

                                        <div class="form-horizontal">
                                            <label class="control-label col-md-3 small">Handphone</label>
                                            <div class="col-md-9">
                                                <input id="handphone" name="handphone" placeholder="Handphone" class="form-control" type="number">
                                                <span class="help-block"></span>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-horizontal">
                                            <label class="control-label col-md-3 small">Alamat</label>
                                            <div class="col-md-9">
                                                <textarea id="alamat_pegawai_toko" name="alamat_pegawai_toko" placeholder="Alamat" class="form-control"></textarea>
                                                <span class="help-block"></span>                            
                                            </div>
                                        </div>

                                        <div class="form-horizontal">
                                            <label class="control-label col-md-3 small">Usergroup</label>
                                            <div class="col-md-9">
                                                <select id="usergroup" name="usergroup" class="form-control">
                                                    <?php foreach ($list_usergroup as $usergroup) { ?>
                                                    <option value="<?php echo $usergroup->id_usergroup ?>">
                                                    <?php echo $usergroup->usergroup_name ?>
                                                    </option>
                                                    <?php } ?>
                                                </select>
                                                <span class="help-block"></span>
                                            </div>
                                        </div>

                                        <div class="form-horizontal">
                                            <label class="control-label col-md-3 small">Status Blokir</label>
                                            <div class="col-md-9">
                                                <select id="status_blokir" name="status_blokir" class="form-control">
                                                    <option value="TIDAK">TIDAK</option>
                                                    <option value="IYA">IYA</option>
                                                </select>
                                                <span class="help-block"></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>                        
                            </div>
                        </form>
                    </div>

                    <!-- Awal data otoritas -->
                    <div class="tab-pane active" id="tab_data_otoritas" name="tab_data_otoritas">
                        <form action="#" id="formOtoritas" class="form-horizontal">
                            <div class="form-body">
                                <input type="hidden" value="" id="id_pegawai_otoritas" name="id_pegawai_otoritas"/>
                                <div class="row">
                                    <div class="col-md-12">   
                                        <table id="TableOtoritas"  class="table table-condensed table-striped table-hover nowrap" cellspacing="0" width="100%">
                                            <thead>
                                                <tr class="text-dark small">
                                                    <th>#</th>
                                                    <th style="width: 550px">Nama Menu</th>
                                                    <th>Read</th>
                                                    <th>Create</th>
                                                    <th>Update</th>
                                                    <th>Delete</th>
                                                </tr>
                                            </thead>
                                            <tbody class="text-dark small"></tbody>
                                        </table>
                                    </div>
                                </div>                        
                            </div>
                        </form>
                    </div>
                    <!-- Akhir data otoritas -->

                    <!-- Awal data foto -->
                    <div class="tab-pane" id="tab_foto_pegawai" name="tab_foto_pegawai">
                        <div class="row">
                            <!-- Awal area upload -->
                            <div class="col-md-4">
                                <div class="dropzone">
                                    <div class="dz-message">
                                        <h3><i class="typcn typcn-upload"></i> Klik atau Drop gambar disini</h3>
                                    </div>
                                </div>
                            </div>
                            <!-- Akhir area upload -->

                            <!-- Awal hasil upload -->
                            <div class="col-md-8">
                                <div id="daftar_foto" class="parent-container">
                                </div>
                            </div>
                            <!-- Akhir hasil upload -->
                        </div>
                    </div>
                    <!-- Akhir data foto -->

                    <!-- Awal data rubah password -->
                    <div class="tab-pane active" id="tab_data_rubah_password" name="tab_data_rubah_password">
                        <form action="#" id="formRubahPassword" class="form-horizontal">
                            <div class="form-body">
                                <input type="hidden" value="" name="id_pegawai_password"/>
                                <div class="row">
                                    <div class="col-md-12">
                                       <div class="form-horizontal">
                                            <label class="control-label col-md-3 small">Password Baru</label>
                                            <div class="col-md-9">
                                                <input id="password_baru" name="password_baru" placeholder="masukan password baru" class="form-control" type="password">
                                                <span class="help-block"></span>
                                            </div>
                                        </div>

                                        <div class="form-horizontal">
                                            <label class="control-label col-md-3 small">Konfirmasi Password Baru</label>
                                            <div class="col-md-9">
                                                <input id="konfirmasi_password_baru" name="konfirmasi_password_baru" placeholder="masukan lagi password baru" class="form-control" type="password">
                                                <span class="help-block"></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>                        
                            </div>
                        </form>
                    </div>
                    <!-- Akhir data rubah password -->
                </div>
            </div>

            <div class="modal-footer">
                <button type="button" id="btnSave" onclick="save()" class="btn btn-primary">Simpan</button>
                <button type="button" id="btnSavePass" onclick="savePass()" class="btn btn-primary" style="display: none">Simpan Password</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- End Bootstrap modal -->

<script>
    function update_useraccess($id_menu, $id_pegawai, $field, $status)
    {
        if($field == 1){$nama_fungsi='read'}
        else if($field == 2){$nama_fungsi='create'}
        else if($field == 3){$nama_fungsi='update'}
        else if($field == 4){$nama_fungsi='delete'}

        if($('#' + $nama_fungsi + '_' + $id_menu).val() == 1){
            $status_sekarang = '0';
        }else if($('#' + $nama_fungsi + '_' + $id_menu).val() == 0){
            $status_sekarang = '1';
        }

        // ajax update usseraccess
        var FormData = "id_menu=" + $id_menu; 
            FormData += "&id_pegawai=" + $id_pegawai;
            FormData += "&field=" + $field;
            FormData += "&status=" + $status_sekarang;

        $.ajax({
            url : "<?php echo site_url('pegawai_toko/ajax_update_usseraccess')?>",
            type: "POST",
            data: FormData,
            dataType: "JSON",
            success: function(data){
                $('#' + $nama_fungsi + '_' + $id_menu).val($status_sekarang);
            },
            error: function (jqXHR, textStatus, errorThrown){
                alert('Error updateing data usseraccess');
            }
        });
    }   
</script>
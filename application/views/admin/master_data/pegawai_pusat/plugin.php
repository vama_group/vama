<!-- Awal CSS -->
<link href="assets/plugin/zircos/plugins/datatables/jquery.dataTables.min.css" rel="stylesheet" type="text/css"/>
<link href="assets/plugin/zircos/plugins/datatables/responsive.bootstrap.min.css" rel="stylesheet" type="text/css"/>
<link href="assets/plugin/zircos/plugins/datatables/fixedHeader.bootstrap.min.css" rel="stylesheet" type="text/css"/>
<link href="assets/plugin/zircos/plugins/datatables/dataTables.bootstrap.min.css" rel="stylesheet" type="text/css"/>
<link href="assets/plugin/zircos/material-design/assets/css/tambahan.css" rel="stylesheet" type="text/css"/>

<!-- Awal JS -->
<script src="assets/plugin/zircos/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="assets/plugin/zircos/plugins/datatables/dataTables.bootstrap.js"></script>
<script src="assets/plugin/zircos/material-design/assets/js/detect.js"></script>
<script src="assets/plugin/zircos/material-design/assets/js/jquery.slimscroll.js"></script>
<script src="assets/plugin/zircos/plugins/datatables/dataTables.responsive.min.js"></script>
<script src="assets/plugin/zircos/plugins/datatables/responsive.bootstrap.min.js"></script>
<script src="assets/plugin/zircos/plugins/datatables/dataTables.fixedHeader.min.js"></script>
<script src="assets/plugin/zircos/material-design/assets/pages/jquery.datatables.init.js"></script>
<!-- Akhir JS -->

<!-- Awal upload foto-->
<link rel="stylesheet" href="assets/plugin/zircos/plugins/magnific-popup/css/magnific-popup.css"/>
<script type="text/javascript" src="assets/plugin/zircos/plugins/magnific-popup/js/jquery.magnific-popup.min.js"></script>

<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/dropzone/dropzone.min.css') ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/dropzone/basic.min.css') ?>">
<script type="text/javascript" src="<?php echo base_url('assets/dropzone/dropzone.min.js') ?>"></script>
<!-- Akhir upload foto-->

<!-- Awal Sweet-Alert  -->
<link href="assets/plugin/zircos/plugins/bootstrap-sweetalert/sweet-alert.css" rel="stylesheet" type="text/css">
<script src="assets/plugin/zircos/plugins/bootstrap-sweetalert/sweet-alert.min.js"></script>
<!-- Akhir Sweet-Alert  -->

<script type="text/javascript">
    var save_method;
    var table          = $('#table').DataTable();
    var table_otoritas = $('#TableOtoritas').DataTable();

    $(document).ready(function() {
        $('#JudulHalaman').html('Pegawai Pusat - VAMA');
        TampilkanDaftarPegawai();
            
        var _swal = window.swal;
        window.swal = function(){
            var previousWindowKeyDown = window.onkeydown;
            _swal.apply(this, Array.prototype.slice.call(arguments, 0));
            window.onkeydown = previousWindowKeyDown;
        };

        $("input").change(function(){
            $(this).parent().parent().removeClass('has-error');
            $(this).next().empty();
        });
        $("textarea").change(function(){
            $(this).parent().parent().removeClass('has-error');
            $(this).next().empty();
        });
        $("select").change(function(){
            $(this).parent().parent().removeClass('has-error');
            $(this).next().empty();
        });
    });

    function TampilkanDaftarPegawai(){
        table.clear();table.destroy();
        table = $('#table').DataTable({ 
            processing: true,
            serverSide: true,
            order: [],

            ajax: {
                "url": "<?php echo site_url('pegawai_pusat/ajax_list')?>",
                "type": "POST",
                "data" : {'id_periode_stok_pusat' : '1'}
            },

            columnDefs: [
                { 
                    targets: [ -1 ],
                    orderable: false,
                },
            ],
        });
    }

    function TampilkanOtoritas(){
        table_otoritas.clear();table_otoritas.destroy();
        table_otoritas = $('#TableOtoritas').DataTable({
            ordering: true,
            bFilter:true,
            processing:true,
            serverSide:true,
            order:[],
            lengthMenu: [[50, 100, -1], [50, 100, "All"]],
            oLanguage:{
                sLoadingRecords:'Silakan Tunggu',
                sProcessing:"<img src='assets/loader/cubeloader2.2.gif' width='50' height='50'> Harap tunggu",
                sZeroRecords:"Data Tidak Ditemukan",
                sSearch : "Pencarian :  _INPUT_",
                sLengthMenu: "<i class='btn btn-primary btn-sm fa fa-folder-open-o'></i><span class='btn btn-default btn-xs'>Menampilkan _MENU_ otoritas</span>",
                sInfo: "<span class='btn btn-sm'>Menampilkan (_START_ sampai _END_) dari _TOTAL_ otoritas</span>",
                sInfoEmpty: "Tidak ada otoritas untuk ditampilkan",
                sInfoFiltered: " - (disaring dari _MAX_ total otoritas)"
            },
            ajax:{
                url: "<?php echo site_url('pegawai_pusat/ajax_list_otoritas')?>",
                type: "POST",
                data: {'id_pegawai_otoritas' : $('#id_pegawai_otoritas').val()}
            },
            columnDefs: [
                            { 
                                "targets": [ -1 ],
                                "orderable": false,
                            },
                        ],

            // scrollX: true,
            deferRender: true,
            scrollY: 350,
            scrollCollapse: true,
            scroller: true
        });
    }

    $(document).on('click', '#tab_detail_pegawai', function(){
        $('.modal-dialog').removeClass('modal-md');
        $('.modal-dialog').addClass('modal-lg');
        $('#btnSavePass').hide();
        $('#btnSave').show();
        id_pegawai = $('#id_pegawai').val();
 
        $.ajax({
            url : "<?php echo site_url('pegawai_pusat/ajax_edit'); ?>",
            type: "POST",
            cache: false,
            data: 'id_pegawai_pusat=' + id_pegawai,
            dataType: "JSON",
            success: function(json){
                $('#kotak_password').hide();
                $('[name="id_pegawai"]').val(json.data.id_pegawai);
                $('[name="id_pegawai_otoritas"]').val(json.data.id_pegawai);
                $('[name="id_pegawai_password"]').val(json.data.id_pegawai);
                $('[name="kode_pegawai"]').val(json.data.kode_pegawai);
                $('[name="nama_pegawai"]').val(json.data.nama_pegawai);
                $('[name="alamat_pegawai"]').val(json.data.alamat_pegawai);
                $('[name="email"]').val(json.data.email);
                $('[name="handphone"]').val(json.data.handphone);
                $('[name="password"]').val(json.data.password);
                $('[name="usergroup"]').val(json.data.id_usergroup);
                $('[name="status_blokir"]').val(json.data.status_blokir);
                $('[name="foto_pegawai"]').html(json.foto);
            }, error: function (jqXHR, textStatus, errorThrown){
                alert('Error get data from ajax');
            }
        });
    });

    $(document).on('click', '#tab_otoritas', function(){
        TampilkanOtoritas();
        $('.modal-dialog').removeClass('modal-lg');
        $('.modal-dialog').removeClass('modal-md');
        $('.modal-dialog').addClass('modal-lg');
        $('#btnSave').hide();
        $('#btnSavePass').hide();
    });

    $(document).on('click', '#tab_foto', function(){
        $('.modal-dialog').removeClass('modal-lg');
        $('.modal-dialog').removeClass('modal-md');
        $('.modal-dialog').addClass('modal-lg');

        $id_pegawai = $('#id_pegawai').val();
        tamplikan_daftar_foto($id_pegawai);
        $('#btnSave').hide();
    });

    $(document).on('click', '#tab_rubah_password', function(){
        $('.modal-dialog').removeClass('modal-sm');
        $('.modal-dialog').removeClass('modal-lg');
        $('.modal-dialog').addClass('modal-md');
        $('#btnSave').hide();
        $('#btnSavePass').show();
    });

    // Awal fungsi lihat foto
    $('.parent-container').magnificPopup({
        delegate: 'a[name="foto"]',
        type: 'image',
        closeOnContentClick: true,
        mainClass: 'mfp-fade',
        gallery: {
            enabled: true,
            navigateByImgClick: true,
            preload: [0,1]
        }
    });
        // Akhir fungsi lihat foto   

    function add_pegawai()
    {
        save_method = 'add';
        $('#btnSave').show();
        $('#btnSavePass').hide();
        $('#kotak_password').show();
        $('#form')[0].reset();
        $('#formOtoritas')[0].reset();
        $('#formRubahPassword')[0].reset();
        $('.form-horizontal').removeClass('has-error');
        $('.help-block').empty();

        $('.modal-dialog').removeClass('modal-lg');
        $('.modal-dialog').addClass('modal-md');
        $('#modal_form').modal('show');
        $('.modal-title').text('Tambah Pegawai Pusat');

        $('#sisi_kanan').html('');
        $('#sisi_kiri').removeClass('col-md-8');
        $('#sisi_kiri').addClass('col-md-12');

        $('#tab_pegawai').hide('');
        $('#tab_otoritas').hide('');
        $('#tab_rubah_password').hide('');
        $('#tab_data_otoritas').removeClass('active');
        $('#tab_data_rubah_password').removeClass('active');
        $('#tab_data_detail_pegawai').addClass('active');    
    }

    function edit_pegawai(id_pegawai)
    {
        save_method = 'update';
        $('#form')[0].reset();
        $('#formOtoritas')[0].reset();
        $('#formRubahPassword')[0].reset();
        $('.form-horizontal').removeClass('has-error');
        $('.form-group').removeClass('has-error');
        $('.help-block').empty();

        $('#tab_pegawai li').removeClass('active');
        $('#tab_data_otoritas').removeClass('active'); 
        $('#tab_foto_pegawai').removeClass('active'); 
        $('#tab_data_rubah_password').removeClass('active'); 

        $('#tab_pegawai li:first').addClass('active'); 
        $('#tab_data_detail_pegawai').addClass('active');
        $('#tab_pegawai').show();
        $('#tab_otoritas').show();
        $('#tab_rubah_password').show();
        $('#btnSave').show();
        $('#btnSavePass').hide();

        $.ajax({
            url : "<?php echo site_url('pegawai_pusat/ajax_edit'); ?>",
            type: "POST",
            cache: false,
            data: 'id_pegawai_pusat=' + id_pegawai,
            dataType: "JSON",
            success: function(json){
                $('.modal-dialog').removeClass('modal-md');
                $('.modal-dialog').addClass('modal-md');
                $('.modal-title').text('Edit Pegawai Pusat : ' + json.data.email);
                $('#sisi_kanan').html('<div class="col-md-12">' +
                                         '<div id="foto_pegawai" name="foto_pegawai">' +
                                         '</div>' +
                                      '</div>'
                                    );
                $('#sisi_kiri').removeClass('col-md-12');
                $('#sisi_kiri').addClass('col-md-8');
                $('#sisi_kanan').removeClass('col-md-4');
                $('#sisi_kanan').addClass('col-md-4');
                $('.modal-dialog').removeClass('modal-md');
                $('.modal-dialog').addClass('modal-lg');
                $('#modal_form').modal('show');

                $('#kotak_password').hide();
                $('[name="id_pegawai"]').val(json.data.id_pegawai);
                $('[name="id_pegawai_otoritas"]').val(json.data.id_pegawai);
                $('[name="id_pegawai_password"]').val(json.data.id_pegawai);
                $('[name="kode_pegawai"]').val(json.data.kode_pegawai);
                $('[name="nama_pegawai"]').val(json.data.nama_pegawai);
                $('[name="alamat_pegawai"]').val(json.data.alamat_pegawai);
                $('[name="email"]').val(json.data.email);
                $('[name="handphone"]').val(json.data.handphone);
                $('[name="usergroup"]').val(json.data.id_usergroup);
                $('[name="status_blokir"]').val(json.data.status_blokir);
                $('[name="foto_pegawai"]').html(json.foto);
            },
            error: function (jqXHR, textStatus, errorThrown){
                swal({
                    title: "Gagal!", 
                    text: "Data pegawai pusat gagal di tampilkan.", 
                    type: "error", 
                    confirmButtonClass: 'btn-danger btn-md waves-effect waves-light',
                    confirmButtonText: "Ok"
                },function (isConfirm){
                    window.location.href = "pegawai_pusat";
                });
            }
        });
    }

    function reload_table()
    {
        TampilkanDaftarPegawai();
    }

    function save()
    {
        var url;
        var judul;
        var pesan;
        if(save_method == 'add'){
            judul   = "Yakin ingin menambahkan pegawai pusat ?";
            pesan   = "Nama : " + $('#nama_pegawai').val() + ", " + 
                      "Email : " + $('#email').val();
            url     = "<?php echo site_url('pegawai_pusat/ajax_add')?>";
        }else{
            judul   = "Yakin ingin memperbaharui pegawai pusat ?";
            pesan   = "Nama : " + $('#nama_pegawai').val() + ", " + 
                      "Email : " + $('#email').val();
            url     = "<?php echo site_url('pegawai_pusat/ajax_update')?>";
        }

        swal({
            title: judul,
            text: pesan,
            type: "info",
            showCancelButton: true,
            confirmButtonClass: 'btn-primary btn-md waves-effect waves-light',
            confirmButtonText: "Iya",
            cancelButtonText: "Batal",
            closeOnConfirm: false,
        }, function (isConfirm){
            if(isConfirm){
                $('#btnSave').text('menyimpan...');
                $('#btnSave').attr('disabled',true);
                $.ajax({
                    url : url,
                    type: "POST",
                    data: $('#form').serialize(),
                    dataType: "JSON",
                    success: function(data){
                        if(data.status){
                            $('#modal_form').modal('hide');
                            reload_table();
                            
                            swal({
                                title: "Berhasil!", 
                                text: "Data pegawai berhasil disimpan.", 
                                type: "success", 
                                confirmButtonText: "Ok"
                            },function (isConfirm){
                                window.location.href="pegawai_pusat";
                            });
                        }else{
                            for (var i = 0; i < data.inputerror.length; i++){
                                $('[name="'+data.inputerror[i]+'"]').parent().parent().addClass('has-error'); 
                                $('[name="'+data.inputerror[i]+'"]').next().text(data.error_string[i]);
                            }

                            swal({
                                title: "Gagal!", 
                                text: "Data pegawai gagal disimpan.", 
                                type: "error", 
                                confirmButtonClass: 'btn-danger btn-md waves-effect waves-light',
                                confirmButtonText: "Ok"
                            });
                        }

                        $('#btnSave').text('Simpan');
                        $('#btnSave').attr('disabled',false);
                    },
                    error: function (jqXHR, textStatus, errorThrown){
                        swal({
                            title: "Gagal!", 
                            text: "Data pegawai gagal disimpan.", 
                            type: "error", 
                            confirmButtonClass: 'btn-danger btn-md waves-effect waves-light',
                            confirmButtonText: "Ok"
                        },function (isConfirm){
                            window.location.href = "pegawai_pusat";
                        });
                    }
                });
            }
        });
    }

    function savePass()
    {
        $('#btnSavePass').text('menyimpan password...');
        $('#btnSavePass').attr('disabled',true);
        $.ajax({
            url : "<?php echo site_url('pegawai_pusat/ajax_update_password')?>",
            type: "POST",
            data: $('#formRubahPassword').serialize(),
            dataType: "JSON",
            success: function(data){
                if(data.status){
                    $('#modal_form').modal('hide');
                    reload_table();
                }else{
                    for (var i = 0; i < data.inputerror.length; i++){
                        $('[name="'+data.inputerror[i]+'"]').parent().parent().addClass('has-error'); 
                        $('[name="'+data.inputerror[i]+'"]').next().text(data.error_string[i]);
                    }
                }

                $('#btnSavePass').text('Simpan Password');
                $('#btnSavePass').attr('disabled',false);
            },
            error: function (jqXHR, textStatus, errorThrown){
                alert('Error adding / update data');
                $('#btnSavePass').text('Simpan Password');
                $('#btnSavePass').attr('disabled',false);
            }
        });
    }

    function verifikasi_delete(id_pegawai)
    {
        $.ajax({
            url : "<?php echo site_url('pegawai_pusat/ajax_verifikasi_delete') ;?>",
            type: "POST",
            cache: false,
            data: 'id_pegawai=' + id_pegawai,
            dataType: "JSON",
            success: function(data){
                $('.modal-dialog').removeClass('modal-lg');
                $('.modal-dialog').removeClass('modal-sm');
                $('.modal-dialog').addClass('modal-md');
                $('#ModalHeader').html('Informasi Hapus Pegawai Pusat');
                $('#ModalContent').html(data.pesan);
                $('#ModalFooter').html(data.footer);
                $('#ModalGue').modal('show');

            }, error: function (jqXHR, textStatus, errorThrown){
               swal({
                    title: "Gagal!", 
                    text: "Data pegawai gagal ditampilkan.", 
                    type: "error", 
                    confirmButtonClass: 'btn-danger btn-md waves-effect waves-light',
                    confirmButtonText: "Ok"
                },function (isConfirm){
                    window.location.href = "pegawai_pusat";
                });
            }
        });
    }

    function delete_pegawai_pusat(id_pegawai)
    {
        $.ajax({
            url : "<?php echo site_url('pegawai_pusat/ajax_delete'); ?>",
            type: "POST",
            cache: false,
            data: 'id_pegawai=' + id_pegawai,
            dataType: "JSON",
            success: function(data){
                reload_table();
                swal({
                    title: "Berhasil!", 
                    text: "Data pegawai pusat berhasil dihapus.", 
                    type: "success", 
                    confirmButtonText: "Ok"
                });
            },
            error: function (jqXHR, textStatus, errorThrown){
                swal({
                    title: "Gagal!", 
                    text: "Data pegawai gagal ditampilkan.", 
                    type: "error", 
                    confirmButtonClass: 'btn-danger btn-md waves-effect waves-light',
                    confirmButtonText: "Ok"
                },function (isConfirm){
                    window.location.href = "pegawai_pusat";
                });
            }
        });
    }

    Dropzone.autoDiscover = false;
    var foto_upload= new Dropzone(".dropzone",{
        url : "<?php echo base_url('pegawai_pusat/proses_upload') ?>",
        maxFilesize: 2,
        maxFiles: 12,
        method:"post",
        acceptedFiles:"image/*,.jpg,.png,.jpeg",
        paramName:"userfile",
        dictInvalidFileType:"Type file ini tidak dizinkan",
        addRemoveLinks:true,
        maxThumbnailFilesize: 20,
    });

    // Event ketika memulai mengupload
    foto_upload.on("sending",function(a,b,c){
        a.token         = Math.random();
        a.id_pegawai    = $('#id_pegawai').val();
        // a.nama_pegawai   = $('#nama_pegawai').val();
        
        c.append("token_foto",a.token); //Menmpersiapkan token untuk masing masing foto
        c.append("id_pegawai",a.id_pegawai); 
        // c.append("nama_barang",a.nama_barang); 
        // console.log(a);
    });

    foto_upload.on("success",function(){
        $(".dz-preview").remove();
        $(".dropzone").removeClass('dz-started');

        $id_pegawai = $('#id_pegawai').val();
        tamplikan_daftar_foto($id_pegawai);
    });

    // Event ketika foto dihapus
    foto_upload.on("removedfile",function(a){
        var token = a.token;
        $.ajax({
            type:"post",
            data:{token:token,},
            url:"<?php echo base_url('pegawai_pusat/remove_foto') ?>",
            cache:false,
            dataType: 'json',
            success: function(){
                console.log("Foto terhapus");
            },
            error: function(){
                console.log("Error");
            }
        });
    });
    // Akhir upload dan hapus foto

    // Awal ajax daftar foto
    function tamplikan_daftar_foto($id_pegawai)
    {
        $('div#daftar_foto').hide();    
        var Lebar   = Lebar + 25;
        $.ajax({
            url: "<?php echo site_url('pegawai_pusat/ajax_daftar_foto'); ?>",
            type: "POST",
            cache: false,
            data: 'id_pegawai=' + $id_pegawai,
            dataType:'json',
            success: function(json){
                if(json.status == 1){
                    $('div#daftar_foto').show('fast');
                    $('div#daftar_foto').html(json.datanya);
                }
            }
        });
    }
    // Akhir ajax daftar foto

    // Awal hapus perfoto
    function delete_foto(id_foto)
    {
        $.ajax({
            url : "<?php echo site_url('pegawai_pusat/ajax_delete_foto'); ?>",
            type: "POST",
            cache: false,
            data: 'id_foto=' + id_foto,
            dataType: "JSON",
            success: function(data){
                if(data.status == 1){
                    $id_pegawai = $('#id_pegawai').val();
                    tamplikan_daftar_foto($id_pegawai);
                }else if(data.status == 0){
                    alert('Error delete foto from ajax');
                }
            }
        });
    }
    // Akhir hapus perfoto

    // Awal jadikan foto profile pegawai
    function jadikan_profile_foto(id_foto)
    {
        $.ajax({
            url : "<?php echo site_url('pegawai_pusat/ajax_jadikan_foto_profile'); ?>",
            type: "POST",
            cache: false,
            data: 'id_foto=' + id_foto,
            dataType: "JSON",
            success: function(data){
                if(data.status == 1){
                    $id_pegawai = $('#id_pegawai').val();
                    tamplikan_daftar_foto($id_pegawai);
                    reload_table();
                }else if(data.status == 0){
                    alert('Error update foto profile from ajax');
                }
            }
        });
    }
    // Akhir jadikan foto profile pegawai
</script>
<!-- Akhir Script CRUD -->

<!-- Datepicker -->
<link href="assets/plugin/zircos/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css" rel="stylesheet">
<link href="assets/plugin/zircos/plugins/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">

<!-- Bootstrap modal -->
<div class="modal fade" id="modal_form" role="dialog">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h5 class="modal-title">Periode Stok Pusat Form</h5>
            </div>

            <div class="modal-body form">
                <form action="#" id="form" class="form-group">
                    <input type="hidden" value="" name="id_periode_stok_pusat"/> 
                    <div class="form-body">
                      <div class="row">
                        <div class="form-horizontal">
                              <label class="control-label col-md-3">Kode Periode</label>
                              <div class="col-md-9">
                                  <input name="kode_periode_stok_pusat" placeholder="Kode Periode" class="form-control" type="text" value="<?php echo $kode_periode_stok_pusat ?>" readonly>
                                  <span class="help-block"></span>
                              </div>
                        </div>

                        <div class="form-horizontal">
                          <label class="control-label col-md-3">Tanggal Awal & Akhir</label>
                          <div class="col-md-9">
                            <div class="row">
                              <div class="col-md-5">
                                <input id="tanggal_periode_awal" type="text" class="form-control" data-date-format="yyyy-mm-dd" name="tanggal_periode_awal"/>
                                <span class="help-block"></span>
                              </div>

                              <div class="col-md-2">
                                  <span class="input-group-addon bg-custom text-white b-0"><</span>
                              </div>

                              <div class="col-md-5">
                                <input id="tanggal_periode_akhir" type="text" class="form-control" data-date-format="yyyy-mm-dd" name="tanggal_periode_akhir"/>
                                <span class="help-block"></span>
                              </div>                            
                            </div>
                          </div> 
                        </div>
                      </div>
                    </div>
                </form>
            </div>
            
            <div class="modal-footer">
                <button type="button" id="btnSave" onclick="save()" class="btn btn-primary">Simpan</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- End Bootstrap modal -->

<!-- validasi -->
<script>
$(function() {
  $( "#awal" ).datepicker({
    defaultDate: "+1w",
    changeMonth: true,
    dateFormat: "yy-mm-dd",
    changeYear: true,
    numberOfMonths: 2,
    onSelect: function( selectedDate ) {
      $( "#awal" ).datepicker( "option", "minDate", selectedDate );
    }
  });
  $( "#akhir" ).datepicker({
    defaultDate: "+1w",
    dateFormat: "yy-mm-dd",
    changeYear: true,
    changeMonth: true,
    numberOfMonths: 2,
    onSelect: function( selectedDate ) {
      $( "#akhir" ).datepicker( "option", "maxDate", selectedDate );
    }
  });
});
</script>
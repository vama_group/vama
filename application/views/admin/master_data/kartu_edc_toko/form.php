<!-- Bootstrap modal -->
<div class="modal fade" id="modal_form" role="dialog">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h5 class="modal-title">Kartu EDC Pusat Form</h5>
            </div>
            <div class="modal-body form">
                <form action="#" id="form" class="form-group">
                    <input type="hidden" value="" id="id_kartu_edc_toko" name="id_kartu_edc_toko"/> 
                    <div class="form-body">
                        <div class="row">
                            <div class="form-horizontal">
                                <label class="control-label col-md-3">Toko</label>
                                <div class="col-md-9">
                                    <select id="kode_toko" name="kode_toko" class="form-control">
                                        <?php foreach ($list_toko as $toko) { ?>
                                            <option value="<?php echo $toko->kode_customer_pusat ?>">
                                                <?php echo $toko->nama_customer_pusat ?>
                                            </option>
                                        <?php } ?>
                                    </select>
                                    <span class="help-block"></span>
                                </div>
                            </div> 
                                                   
                            <div class="form-horizontal">
                                <label class="control-label col-md-3">No. Kartu EDC</label>
                                <div class="col-md-9">
                                    <input id="no_kartu_edc" name="no_kartu_edc" placeholder="No. Kartu EDC" class="form-control" type="text">
                                    <span class="help-block"></span>
                                </div>
                            </div>

                            <div class="form-horizontal">
                                <label class="control-label col-md-3">Nama Bank</label>
                                <div class="col-md-9">
                                    <input id="nama_bank" name="nama_bank" placeholder="Nama Bank" class="form-control" type="text">
                                    <span class="help-block"></span>
                                </div>
                            </div>

                            <div class="form-horizontal">
                                <label class="control-label col-md-3">Atas Nama</label>
                                <div class="col-md-9">
                                    <input id="atas_nama" name="atas_nama" placeholder="Atas Nama" class="form-control" type="text">
                                    <span class="help-block"></span>
                                </div>
                            </div>
                        </div>                        
                    </div>
                </form>
            </div>

            <div class="modal-footer">
                <button type="button" id="btnSave" onclick="save()" class="btn btn-primary">Simpan</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- End Bootstrap modal -->


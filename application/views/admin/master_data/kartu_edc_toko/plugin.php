<!-- Awal CSS -->
<link href="assets/plugin/zircos/plugins/datatables/jquery.dataTables.min.css" rel="stylesheet" type="text/css"/>
<link href="assets/plugin/zircos/plugins/datatables/responsive.bootstrap.min.css" rel="stylesheet" type="text/css"/>
<link href="assets/plugin/zircos/plugins/datatables/dataTables.bootstrap.min.css" rel="stylesheet" type="text/css"/>
<link href="assets/plugin/zircos/material-design/assets/css/tambahan.css" rel="stylesheet" type="text/css"/>

<!-- Awal JS -->
<script src="assets/plugin/zircos/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="assets/plugin/zircos/plugins/datatables/dataTables.bootstrap.js"></script>
<script src="assets/plugin/zircos/plugins/datatables/dataTables.responsive.min.js"></script>
<script src="assets/plugin/zircos/plugins/datatables/responsive.bootstrap.min.js"></script>
<script src="assets/plugin/zircos/material-design/assets/pages/jquery.datatables.init.js"></script>

<!-- Awal Sweet-Alert  -->
<link href="assets/plugin/zircos/plugins/bootstrap-sweetalert/sweet-alert.css" rel="stylesheet" type="text/css">
<script src="assets/plugin/zircos/plugins/bootstrap-sweetalert/sweet-alert.min.js"></script>
<!-- Akhir Sweet-Alert  -->

<script type="text/javascript">
    var save_method;
    var table;

    $(document).ready(function(){
        $('#JudulHalaman').html('Kartu EDC Toko - VAMA');

        table = $('#table').DataTable({ 
            processing: true,
            serverSide: true,
            order: [],

            ajax: {
                url: "<?php echo site_url('kartu_edc_toko/ajax_list')?>",
                type: "POST",
                data: {'id_periode_stok_pusat' : '1'}
            },

            columnDefs: [
                { 
                    targets: [ -1 ],
                    orderable: false,
                },
            ],

        });

        var _swal = window.swal;
        window.swal = function(){
            var previousWindowKeyDown = window.onkeydown;
            _swal.apply(this, Array.prototype.slice.call(arguments, 0));
            window.onkeydown = previousWindowKeyDown;
        };

        $("input").change(function(){
            $(this).parent().parent().removeClass('has-error');
            $(this).next().empty();
        });
        
        $("textarea").change(function(){
            $(this).parent().parent().removeClass('has-error');
            $(this).next().empty();
        });

        $("select").change(function(){
            $(this).parent().parent().removeClass('has-error');
            $(this).next().empty();
        });
    });

    function reload_table()
    {
        table.ajax.reload(null,false); 
    }

    function add_kartu_edc_toko()
    {
        save_method = 'add';
        $('#form')[0].reset();
        $('.form-horizontal').removeClass('has-error');
        $('.help-block').empty();
        $('#modal_form').modal('show');
        $('.modal-title').text('Tambah Kartu EDC Toko');
    }

    function edit_kartu_edc_toko(id_kartu_edc_toko)
    {
        save_method = 'update';
        $('#form')[0].reset();
        $('.form-horizontal').removeClass('has-error');
        $('.help-block').empty();

        $.ajax({
            url : "<?php echo site_url('kartu_edc_toko/ajax_edit'); ?>",
            type: "POST",
            cache: false,
            data: 'id_kartu_edc_toko=' + id_kartu_edc_toko,
            dataType: "JSON",

            success: function(data){
                $('[name="id_kartu_edc_toko"]').val(data.id_kartu_edc_toko);
                $('[name="kode_toko"]').val(data.kode_toko);
                $('[name="no_kartu_edc"]').val(data.no_kartu_edc);
                $('[name="nama_bank"]').val(data.nama_bank);
                $('[name="atas_nama"]').val(data.atas_nama);
                $('#modal_form').modal('show'); 
                $('.modal-title').text('Edit Kartu EDC Toko');

            },
            error: function (jqXHR, textStatus, errorThrown){
                swal({
                    title: "Gagal!", 
                    text: "Data kartu edc toko gagal untuk ditampilkan.", 
                    type: "error", 
                    confirmButtonClass: 'btn-danger btn-md waves-effect waves-light',
                    confirmButtonText: "Ok"
                }, function(isConfirm){
                    window.location.href = "kartu_edc_toko";
                });
            }
        });
    }

    function save()
    {
        var url;
        var judul;
        var pesan;
        if(save_method == 'add'){
            judul   = "Yakin ingin menambahkan kartu edc toko ?";
            pesan   = "No. Kartu EDC : " + $('#no_kartu_edc').val() + ", " + 
                      "Nama Bank : " + $('#nama_bank').val();
            url     = "<?php echo site_url('kartu_edc_toko/ajax_add')?>";
        }else{
            judul   = "Yakin ingin memperbaharui kartu edc toko ?";
            pesan   = "No. Kartu EDC : " + $('#no_kartu_edc').val() + ", " + 
                      "Nama Bank : " + $('#nama_bank').val();
            url     = "<?php echo site_url('kartu_edc_toko/ajax_update')?>";
        }

        swal({
            title: judul,
            text: pesan,
            type: "info",
            showCancelButton: true,
            confirmButtonClass: 'btn-primary btn-md waves-effect waves-light',
            confirmButtonText: "Iya",
            cancelButtonText: "Batal",
            closeOnConfirm: false,
        }, function (isConfirm){
            if(isConfirm){
                $('#btnSave').text('menyimpan...');
                $('#btnSave').attr('disabled',true);
            
                $.ajax({
                    url : url,
                    type: "POST",
                    data: $('#form').serialize(),
                    dataType: "JSON",
                    success: function(data){
                        if(data.status){
                            $('#modal_form').modal('hide');
                            reload_table();

                            swal({
                                title: "Berhasil!", 
                                text: "Data kartu edc toko berhasil disimpan.", 
                                type: "success", 
                                confirmButtonText: "Ok"
                            });
                        }else{
                            for (var i = 0; i < data.inputerror.length; i++){
                                $('[name="'+data.inputerror[i]+'"]').parent().parent().addClass('has-error');
                                $('[name="'+data.inputerror[i]+'"]').next().text(data.error_string[i]);
                            }

                            swal({
                                title: "Gagal!", 
                                text: "Data kartu edc toko gagal disimpan.", 
                                type: "error", 
                                confirmButtonClass: 'btn-danger btn-md waves-effect waves-light',
                                confirmButtonText: "Ok"
                            });
                        }
                        $('#btnSave').text('Simpan');
                        $('#btnSave').attr('disabled',false); 

                    },
                    error: function (jqXHR, textStatus, errorThrown){
                        swal({
                            title: "Gagal!", 
                            text: "Data kartu edc toko gagal disimpan.", 
                            type: "error", 
                            confirmButtonClass: 'btn-danger btn-md waves-effect waves-light',
                            confirmButtonText: "Ok"
                        }, function(isConfirm){
                            window.location.href = "kartu_edc_toko";
                        });
                    }
                });
            }
        });
    }

    function verifikasi_delete(id_kartu_edc_toko)
    {
        $.ajax({
            url : "<?php echo site_url('kartu_edc_toko/ajax_verifikasi_delete') ;?>",
            type: "POST",
            cache: false,
            data: 'id_kartu_edc_toko=' + id_kartu_edc_toko,
            dataType: "JSON",
            success: function(data){
                $('.modal-dialog').removeClass('modal-sm');
                $('.modal-dialog').addClass('modal-md');
                $('#ModalHeader').html('Informasi Hapus Kartu EDC Toko');
                $('#ModalContent').html(data.pesan);
                $('#ModalFooter').html(data.footer);
                $('#ModalGue').modal('show');

            },error: function (jqXHR, textStatus, errorThrown){
                swal({
                    title: "Gagal!", 
                    text: "Data kartu edc toko gagal ditampilkan.", 
                    type: "error", 
                    confirmButtonClass: 'btn-danger btn-md waves-effect waves-light',
                    confirmButtonText: "Ok"
                }, function(isConfirm){
                    window.location.href = "kartu_edc_toko";
                });
            }
        });
    }

    function delete_kartu_edc_toko(id_kartu_edc_toko)
    {
        $.ajax({
            url : "<?php echo site_url('kartu_edc_toko/ajax_delete'); ?>",
            type: "POST",
            cache: false,
            data: 'id_kartu_edc_toko=' + id_kartu_edc_toko,
            dataType: "JSON",

            success: function(data){
                $('#modal_form').modal('hide');
                reload_table();

                swal({
                    title: "Berhasil!", 
                    text: "Data kartu edc toko berhasil dihapus.", 
                    type: "success", 
                    confirmButtonText: "Ok"
                });
            },
            error: function (jqXHR, textStatus, errorThrown){
                swal({
                    title: "Gagal!", 
                    text: "Data kartu edc toko gagal dihapus.", 
                    type: "error", 
                    confirmButtonClass: 'btn-danger btn-md waves-effect waves-light',
                    confirmButtonText: "Ok"
                }, function(isConfirm){
                    window.location.href = "kartu_edc_toko";
                });;
            }
        });
    }
</script>
<!-- Akhir Script CRUD -->
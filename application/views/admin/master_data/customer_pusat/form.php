<!-- Bootstrap modal -->
<div class="modal fade" id="modal_form" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h5 class="modal-title">Customer Pusat Form</h5>
            </div>
            <div class="modal-body form">
                <form action="#" id="form" class="form-group">
                    <input type="hidden" value="" id="id_customer_pusat" name="id_customer_pusat"/> 
                    <div class="form-body">
                        <div class="row">
                            <!-- Awal induk tab -->
                            <ul class="nav nav-tabs" id="tab_induk">
                                <li class="active">
                                    <a id="tab_customer" href="#tab_data_customer" data-toggle="tab" aria-expanded="true">
                                        <span class="visible-xs"><i class="mdi mdi-account-location"></i></span>
                                        <span class="hidden-xs">Data Customer</span>
                                    </a>
                                </li>
                                <li class="">
                                    <a id="tab_pendapatan_poin" href="#tab_data_pendapatan_poin" data-toggle="tab" aria-expanded="true">
                                        <span class="visible-xs"><i class="fa fa-gift"></i></span>
                                        <span class="hidden-xs">History Pendapatan Poin</span>
                                    </a>
                                </li>
                                <li class="">
                                    <a id="tab_penukaran_poin" href="#tab_data_penukaran_poin" data-toggle="tab" aria-expanded="true">
                                        <span class="visible-xs"><i class="fa fa-cut"></i></span>
                                        <span class="hidden-xs">History Penukaran Poin</span>
                                    </a>
                                </li>
                            </ul>
                            <!-- Akhir induk tab -->
                            <!-- Awal isi tab -->
                            <div class="tab-content">
                                <!-- Awal data customer -->
                                <div class="tab-pane active" id="tab_data_customer" name="tab_data_customer">
                                    <!-- <div class="row"> -->
                                    <div class="col-md-6">
                                        <div class="form-horizontal">
                                            <label class="control-label col-md-3 small">NAMA</label>
                                            <div class="col-md-9">
                                                <input id="nama_customer_pusat" name="nama_customer_pusat" placeholder="Nama Customer" class="form-control" type="text" autofocus>
                                                <span class="help-block"></span>
                                            </div>
                                        </div>                               

                                        <div class="form-horizontal">
                                            <label class="control-label col-md-3 small">Email</label>
                                            <div class="col-md-9">
                                                <input id="email" name="email" placeholder="email" class="form-control" type="email">
                                                <span class="help-block"></span>
                                            </div>
                                        </div>

                                        <div class="form-horizontal">
                                            <label class="control-label col-md-3 small">HP 1 - 2</label>
                                            <div class="col-md-9">
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <input name="handphone1" placeholder="Handphone 1" class="form-control" type="number">
                                                        <span class="help-block"></span>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <input name="handphone2" placeholder="Handphone 2" class="form-control" type="number">
                                                        <span class="help-block"></span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        
                                        <div class="form-horizontal">
                                            <label class="control-label col-md-3 small">Alamat</label>
                                            <div class="col-md-9">
                                                <textarea id="alamat_customer_pusat" name="alamat_customer_pusat" placeholder="Alamat" class="form-control"></textarea>
                                                <span class="help-block"></span>                            
                                            </div>
                                        </div>                                
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-horizontal">
                                            <label class="control-label col-md-3 small">NO. SIUP</label>
                                            <div class="col-md-9">
                                                <input id="no_siup" name="no_siup" placeholder="NO. SIUP" class="form-control" type="text">
                                                <span class="help-block"></span>
                                            </div>
                                        </div>                                

                                        <div class="form-horizontal">
                                            <label class="control-label col-md-3 small">Asal Customer</label>
                                            <div class="col-md-9">
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <select id="asal_customer_pusat" name="asal_customer_pusat" class="form-control">
                                                            <option value="OFFLINE">OFFLINE</option>
                                                            <option value="ONLINE">ONLINE</option>
                                                        </select>
                                                        <span class="help-block"></span>
                                                    </div>

                                                    <div class="col-md-6">
                                                        <select id="sumber_online" name="sumber_online" class="form-control" disabled>
                                                            <option value="0">Pilih</option>
                                                            <?php foreach ($list_sumber as $sumber) { ?>
                                                                <option value="<?php echo $sumber->id_referensi ?>">
                                                                    <?php echo $sumber->nama_sumber_online ?>
                                                                </option>
                                                            <?php } ?>
                                                        </select>
                                                        <span class="help-block"></span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-horizontal">
                                            <label class="control-label col-md-3 small">TIPE CUTSOMER</label>
                                            <div class="col-md-9">
                                                <select id="tipe_customer_pusat" name="tipe_customer_pusat" class="form-control">
                                                    <option value="GROSIR">GROSIR</option>
                                                    <option value="BENGKEL">BENGKEL</option>
                                                    <option value="ECERAN">ECERAN</option>
                                                    <option value="mrc">MRC</option>
                                                </select>
                                                <span class="help-block"></span>
                                            </div>
                                        </div>

                                        <div class="form-horizontal">
                                            <label class="control-label col-md-3 small">Tlpn 1 & 2</label>
                                            <div class="col-md-9">
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <input id="telephone1" name="telephone1" placeholder="Telephone 1" class="form-control" type="number">
                                                        <span class="help-block"></span>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <input id="telephone2" name="telephone2" placeholder="Telephone 2" class="form-control" type="number">
                                                        <span class="help-block"></span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <!-- <div class="form-horizontal">
                                            <label class="control-label col-md-3 small">Pin BB</label>
                                            <div class="col-md-9">
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <input id="pin_bb1"" name="pin_bb1" placeholder="Pin BB 1" class="form-control" type="text">
                                                        <span class="help-block"></span>
                                                    </div>

                                                    <div class="col-md-6">
                                                        <input id="pin_bb2" name="pin_bb2" placeholder="Pin BB 2" class="form-control" type="text">
                                                        <span class="help-block"></span>
                                                    </div>                                            
                                                </div>
                                            </div>
                                        </div>  -->


                                        <div class="form-horizontal">
                                            <label class="control-label col-md-3 small">Kontak & Jabatan</label>
                                            <div class="col-md-9">
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <input id="kontak_pribadi" name="kontak_pribadi" placeholder="Kontak" class="form-control" type="text">
                                                        <span class="help-block"></span>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <input id="jabatan" name="jabatan" placeholder="Jabatan" class="form-control" type="text">
                                                        <span class="help-block"></span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-horizontal">
                                            <label class="control-label col-md-3 small">FAX</label>
                                            <div class="col-md-9">
                                                <input id="fax" name="fax" placeholder="Fax" class="form-control" type="number">
                                                <span class="help-block"></span>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- </div> -->
                                </div>

                                <div class="tab-pane" id="tab_data_pendapatan_poin" name="tab_data_pendapatan_poin">
                                    <div class="row">
                                        <div class="col-md-12">   
                                            <table id="TablePendapatanPoin"  class="table table-condensed table-striped table-hover nowrap" cellspacing="0" width="100%">
                                                <thead>
                                                    <tr class="text-dark small">
                                                        <th>#</th>
                                                        <th style="width: 550px">No. Penjualan</th>
                                                        <th>Total Belanja</th>
                                                        <th>Jumlah Poin</th>
                                                        <th>Tanggal Save</th>
                                                        <th>Pegawai Save</th>
                                                        <th>Tanggal Edit</th>
                                                        <th>Pegawai Edit</th>
                                                    </tr>
                                                </thead>
                                                <tbody class="text-dark small"></tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>

                                <div class="tab-pane" id="tab_data_penukaran_poin" name="tab_data_penukaran_poin">
                                </div>
                            </div>
                        </div>                        
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" id="btnSave" name="btnSave" onclick="save()" class="btn btn-primary">Simpan</button>
                <button type="button" id="btnBatal" name="btnBatal" class="btn btn-danger" data-dismiss="modal">Batal</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- End Bootstrap modal
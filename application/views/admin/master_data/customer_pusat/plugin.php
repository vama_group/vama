<!-- Awal CSS -->
<link href="assets/plugin/zircos/plugins/datatables/jquery.dataTables.min.css" rel="stylesheet" type="text/css"/>
<link href="assets/plugin/zircos/plugins/datatables/responsive.bootstrap.min.css" rel="stylesheet" type="text/css"/>
<link href="assets/plugin/zircos/plugins/datatables/dataTables.bootstrap.min.css" rel="stylesheet" type="text/css"/>
<link href="assets/plugin/zircos/material-design/assets/css/tambahan.css" rel="stylesheet" type="text/css"/>

<!-- Awal JS -->
<script src="assets/plugin/zircos/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="assets/plugin/zircos/plugins/datatables/dataTables.bootstrap.js"></script>
<script src="assets/plugin/zircos/material-design/assets/js/detect.js"></script>
<script src="assets/plugin/zircos/material-design/assets/js/jquery.slimscroll.js"></script>
<script src="assets/plugin/zircos/plugins/datatables/dataTables.responsive.min.js"></script>
<script src="assets/plugin/zircos/plugins/datatables/responsive.bootstrap.min.js"></script>
<script src="assets/plugin/zircos/material-design/assets/pages/jquery.datatables.init.js"></script>
<!-- Akhir JS -->

<!-- Awal Sweet-Alert  -->
<link href="assets/plugin/zircos/plugins/bootstrap-sweetalert/sweet-alert.css" rel="stylesheet" type="text/css">
<script src="assets/plugin/zircos/plugins/bootstrap-sweetalert/sweet-alert.min.js"></script>
<!-- Akhir Sweet-Alert  -->


<!-- Awal Script CRUD -->
<script type="text/javascript">
    var save_method;
    var table;
    var table_pendapatan_poin = $('#TablePendapatanPoin').DataTable();

    function TampilkanPendapatanPoin(id_customer_pusat){
        table_pendapatan_poin.clear();table_pendapatan_poin.destroy();
        table_pendapatan_poin = $('#TablePendapatanPoin').DataTable({
            ordering: true,
            bFilter:true,
            processing:true,
            serverSide:true,
            order:[],
            lengthMenu: [[50, 100, -1], [50, 100, "All"]],
            oLanguage:{
                sLoadingRecords:'Silakan Tunggu',
                sProcessing:"<img src='assets/loader/cubeloader2.2.gif' width='50' height='50'> Harap tunggu",
                sZeroRecords:"Data Tidak Ditemukan",
                sSearch : "Pencarian :  _INPUT_",
                sLengthMenu: "<i class='btn btn-primary btn-sm fa fa-folder-open-o'></i><span class='btn btn-default btn-xs'>Menampilkan _MENU_ pendapatan poin</span>",
                sInfo: "<span class='btn btn-sm'>Menampilkan (_START_ sampai _END_) dari _TOTAL_ pendapatan poin</span>",
                sInfoEmpty: "Tidak ada otoritas untuk ditampilkan",
                sInfoFiltered: " - (disaring dari _MAX_ total otoritas)"
            },
            ajax:{
                url: "<?php echo site_url('customer_pusat/ajax_list_pendapatan_poin')?>",
                type: "POST",
                data: {'id_customer_pusat' : $('#id_customer_pusat').val()}
                // data: {'id_customer_pusat' : id_customer_pusat}
            },
            columnDefs: [{ 
                            "targets": [ -1 ],
                            "orderable": false,
                        },],

            deferRender: true,
            scrollY: 350,
            scrollCollapse: true,
            scroller: true
        });
    }

    $(document).on('click', '#tab_customer', function(){
        $('#btnSave').show();
        $('#btnBatal').show();
    });

    $(document).on('click', '#tab_pendapatan_poin', function(){
        // alert($('#id_customer_pusat').val());
        TampilkanPendapatanPoin();
        // TampilkanPendapatanPoin($('#id_customer_pusat').val());
        $('#btnSave').hide();
        $('#btnBatal').hide();
    });

    $(document).ready(function() {    
        $('#JudulHalaman').html('Customer Pusat - VAMA');

        table=$('#table').DataTable({
            ordering: true,
            bFilter:true,
            processing:true,
            serverSide:true,
            order:[],
            lengthMenu: [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
            oLanguage:{
                sLoadingRecords:'Silakan Tunggu',
                sProcessing:"<img src='assets/loader/cubeloader2.2.gif' width='50' height='50'> Harap tunggu",
                sZeroRecords:"Data Tidak Ditemukan",
                sSearch : "Pencarian :  _INPUT_",
                sLengthMenu: "Menampilkan _MENU_ customer pusat",
                sInfo: "<span class='btn btn-sm'>Menampilkan (_START_ sampai _END_) dari _TOTAL_ customer pusat</span>",
                sInfoEmpty: "Tidak ada customer pusat untuk ditampilkan",
                sInfoFiltered: " - (disaring dari _MAX_ total customer pusat)"
            },

            ajax: {
                url: "<?php echo site_url('customer_pusat/ajax_list')?>",
                type: "POST",
                data: {'id_periode_stok_pusat' : '1'}
            },

            columnDefs: [
                            { 
                                "targets": [ -1 ],
                                "orderable": false,
                            },
                        ],
        });

        var _swal = window.swal;
        window.swal = function(){
            var previousWindowKeyDown = window.onkeydown;
            _swal.apply(this, Array.prototype.slice.call(arguments, 0));
            window.onkeydown = previousWindowKeyDown;
        };    

        $("#asal_customer_pusat").change(function(){
            if($(this).val() == 'OFFLINE'){
                $('#sumber_online').prop('disabled', true).val('0');
            }else if($(this).val() == 'ONLINE'){
                $('#sumber_online').removeAttr('disabled').val('0');
            }
        });

        $("input").change(function(){
            $(this).parent().parent().removeClass('has-error');
            $(this).next().empty();
        });

        $("textarea").change(function(){
            $(this).parent().parent().removeClass('has-error');
            $(this).next().empty();
        });

        $("select").change(function(){
            $(this).parent().parent().removeClass('has-error');
            $(this).next().empty();
        });
    });

    function reload_table()
    {
        table.ajax.reload(null,false);
    }

    function import_customer_pusat()
    {
        $('#icon_import').removeClass('fa fa-refresh');
        $('#icon_import').addClass('fa fa-spin fa-refresh');

        $.ajax({
            url : "<?php echo site_url('customer_pusat/ajax_import_customer_pusat'); ?>",
            type: "POST",
            cache: false,
            dataType: "JSON",
            success: function(data){
                if(data.status == 1){
                    reload_table();
                    swal({
                        title: "Berhasil!", 
                        text: "Data customer pusat berhasil di import.", 
                        type: "success", 
                        confirmButtonText: "Ok"
                    });
                }else if(data.status == 0){
                   swal({
                        title: "Gagal!", 
                        text: "Tidak ada data customer pusat yg di import.", 
                        type: "error", 
                        confirmButtonClass: 'btn-danger btn-md waves-effect waves-light',
                        confirmButtonText: "Ok"
                    }, function(isConfirm){
                        window.location.href = "customer_pusat";
                    });
                }
            },
            error: function (jqXHR, textStatus, errorThrown){
                swal({
                    title: "Gagal!", 
                    text: "Data customer pusat gagal di import.", 
                    type: "error", 
                    confirmButtonClass: 'btn-danger btn-md waves-effect waves-light',
                    confirmButtonText: "Ok"
                }, function(isConfirm){
                    window.location.href = "customer_pusat";
                });
            }
        });
        $('#icon_import').removeClass('fa fa-spin fa-refresh');
        $('#icon_import').addClass('fa fa-refresh');
    }

    function add_customer_pusat()
    {
        save_method = 'add';
        $('#form')[0].reset();
        $('.form-horizontal').removeClass('has-error');
        $('.help-block').empty();
        $('#modal_form').modal('show');
        $('.modal-title').text('Tambah Customer Pusat');
        $('[name="sumber_online"]').prop('disabled', true);

        $('#tab_induk').hide('');
        $('#tab_customer').hide('');
        $('#tab_pendapatan_poin').hide('');
        $('#tab_penukaran_poin').hide('');
        $('#tab_data_pendapatan_poin').removeClass('active');
        $('#tab_data_penukaran_poin').removeClass('active');
        $('#tab_data_customer').addClass('active');   
    }

    function edit_customer_pusat(id_customer_pusat)
    {
        save_method = 'update';
        $('#form')[0].reset();
        $('.form-horizontal').removeClass('has-error');
        $('.help-block').empty();

        $.ajax({
            url : "<?php echo site_url('customer_pusat/ajax_edit'); ?>",
            type: "POST",
            cache: false,
            data: 'id_customer_pusat=' + id_customer_pusat,
            dataType: "JSON",
            success: function(data){
                $('[name="id_customer_pusat"]').val(data.id_customer_pusat);
                $('[name="no_siup"]').val(data.no_siup);
                $('[name="nama_customer_pusat"]').val(data.nama_customer_pusat);
                $('[name="asal_customer_pusat"]').val(data.asal_customer_pusat);
                $('[name="alamat_customer_pusat"]').val(data.alamat_customer_pusat);           
                $('[name="sumber_online"]').val(data.id_referensi);           
                $('[name="tipe_customer_pusat"]').val(data.tipe_customer_pusat);           
                $('[name="kontak_pribadi"]').val(data.kontak_pribadi);
                $('[name="jabatan"]').val(data.jabatan);           
                $('[name="email"]').val(data.email);
                $('[name="pin_bb1"]').val(data.pin_bb1);
                $('[name="pin_bb2"]').val(data.pin_bb2);
                $('[name="telephone1"]').val(data.telephone1);
                $('[name="telephone2"]').val(data.telephone2);
                $('[name="fax"]').val(data.fax);
                $('[name="handphone1"]').val(data.handphone1);
                $('[name="handphone2"]').val(data.handphone2);
                $('[name="password"]').val(data.password);
                $('[name="usergroup"]').val(data.id_usergroup);
                $('[name="status_blokir"]').val(data.status_blokir);
                $('#modal_form').modal('show');
                $('.modal-title').text('Edit Customer Pusat');
                if(data.asal_customer_pusat == 'ONLINE'){
                    $('[name="sumber_online"]').removeAttr('disabled', false);
                }

                $('#tab_induk').show('');
                $('#tab_customer').show('');
                $('#tab_pendapatan_poin').show('');
                $('#tab_penukaran_poin').show('');
                $('#tab_data_pendapatan_poin').removeClass('active');
                $('#tab_data_penukaran_poin').removeClass('active');
                $('#tab_data_customer').addClass('active');
                $('#tab_induk li').removeClass('active');
                $('#tab_induk li:first').addClass('active');  
            },
            error: function (jqXHR, textStatus, errorThrown){
                swal({
                    title: "Gagal!", 
                    text: "Data customer pusat gagal ditampilkan.", 
                    type: "error", 
                    confirmButtonClass: 'btn-danger btn-md waves-effect waves-light',
                    confirmButtonText: "Ok"
                }, function(isConfirm){
                    window.location.href = "customer_pusat";
                });
            }
        });
    }

    function save()
    {
        var url;
        var judul;
        var pesan;
        if(save_method == 'add'){
            judul   = "Yakin ingin menambahkan customer pusat ?";
            pesan   = "Nama : " + $('#nama_customer_pusat').val() + ", " + 
                      "Tipe Customer : " + $('#tipe_customer_pusat').val();
            url     = "<?php echo site_url('customer_pusat/ajax_add')?>";
        }else{
            judul   = "Yakin ingin memperbaharui customer pusat ?";
            pesan   = "Nama : " + $('#nama_customer_pusat').val() + ", " + 
                      "Tipe Customer : " + $('#tipe_customer_pusat').val();
            url     = "<?php echo site_url('customer_pusat/ajax_update')?>";
        }

        swal({
            title: judul,
            text: pesan,
            type: "info",
            showCancelButton: true,
            confirmButtonClass: 'btn-primary btn-md waves-effect waves-light',
            confirmButtonText: "Iya",
            cancelButtonText: "Batal",
            closeOnConfirm: false,
        }, function (isConfirm){
            if(isConfirm){
                $('#btnSave').text('menyimpan...');
                $('#btnSave').attr('disabled',true);

                $.ajax({
                    url : url,
                    type: "POST",
                    data: $('#form').serialize(),
                    dataType: "JSON",
                    success: function(data){
                        if(data.status){
                            $('#modal_form').modal('hide');
                            reload_table();

                            swal({
                                title: "Berhasil!", 
                                text: "Data customer pusat berhasil disimpan.", 
                                type: "success", 
                                confirmButtonText: "Ok"
                            });
                        }else{
                            for (var i = 0; i < data.inputerror.length; i++){
                                if(data.inputerror[i] == 'handphone1' || data.inputerror[i] == 'handphone2'){
                                    $('[name="'+data.inputerror[i]+'"]').parent().parent().parent().parent().addClass('has-error');
                                    $('[name="'+data.inputerror[i]+'"]').next().text(data.error_string[i]);
                                }else{
                                    $('[name="'+data.inputerror[i]+'"]').parent().parent().addClass('has-error');
                                    $('[name="'+data.inputerror[i]+'"]').next().text(data.error_string[i]);
                                }
                                // $('[name="'+data.inputerror[i]+'"]').parent().parent().addClass('has-error');
                                // $('[name="'+data.inputerror[i]+'"]').next().text(data.error_string[i]);
                            }

                            swal({
                                title: "Gagal!", 
                                text: "Data customer pusat gagal disimpan.", 
                                type: "error", 
                                confirmButtonClass: 'btn-danger btn-md waves-effect waves-light',
                                confirmButtonText: "Ok"
                            });
                        }
                        $('#btnSave').text('Simpan');
                        $('#btnSave').attr('disabled',false);
                    },
                    error: function (jqXHR, textStatus, errorThrown){
                        swal({
                            title: "Gagal!", 
                            text: "Data customer pusat gagal disimpan.", 
                            type: "error", 
                            confirmButtonClass: 'btn-danger btn-md waves-effect waves-light',
                            confirmButtonText: "Ok"
                        }, function(isConfirm){
                            window.location.href = "customer_pusat";
                        });
                    }
                });
            }
        });
    }

    function verifikasi_delete(id_customer_pusat)
    {
        $.ajax({
            url : "<?php echo site_url('customer_pusat/ajax_verifikasi_delete') ;?>",
            type: "POST",
            cache: false,
            data: 'id_customer_pusat=' + id_customer_pusat,
            dataType: "JSON",
            success: function(data){
                $('.modal-dialog').removeClass('modal-sm');
                $('.modal-dialog').addClass('modal-md');
                $('#ModalHeader').html('Informasi Hapus Jenis Barang');
                $('#ModalContent').html(data.pesan);
                $('#ModalFooter').html(data.footer);
                $('#ModalGue').modal('show');

            },error: function (jqXHR, textStatus, errorThrown){
                swal({
                    title: "Gagal!", 
                    text: "Data customer pusat gagal ditampilkan.", 
                    type: "error", 
                    confirmButtonClass: 'btn-danger btn-md waves-effect waves-light',
                    confirmButtonText: "Ok"
                }, function(isConfirm){
                    window.location.href = "customer_pusat";
                });
            }
        });
    }

    function delete_customer_pusat(id_customer_pusat)
    {
        $.ajax({
            url : "<?php echo site_url('customer_pusat/ajax_delete'); ?>",
            type: "POST",
            cache: false,
            data: 'id_customer_pusat=' + id_customer_pusat,
            dataType: "JSON",
            success: function(data){
                $('#modal_form').modal('hide');
                reload_table();

                swal({
                    title: "Berhasil!", 
                    text: "Data customer pusat berhasil dihapus.", 
                    type: "success", 
                    confirmButtonText: "Ok"
                });
            },
            error: function (jqXHR, textStatus, errorThrown){
                swal({
                    title: "Gagal!", 
                    text: "Data customer pusat gagal dihapus.", 
                    type: "error", 
                    confirmButtonClass: 'btn-danger btn-md waves-effect waves-light',
                    confirmButtonText: "Ok"
                }, function(isConfirm){
                    window.location.href = "customer_pusat";
                });
            }
        });
    }
</script>
<!-- Akhir Script CRUD -->

<!-- Awal datatable -->
<link href="assets/plugin/zircos/plugins/datatables/buttons.bootstrap.min.css" rel="stylesheet" type="text/css"/>
<link href="assets/plugin/zircos/plugins/datatables/dataTables.colVis.css" rel="stylesheet" type="text/css"/>
<script src="assets/plugin/zircos/plugins/datatables/dataTables.buttons.min.js"></script>
<script src="assets/plugin/zircos/plugins/datatables/buttons.bootstrap.min.js"></script>
<script src="assets/plugin/zircos/plugins/datatables/jszip.min.js"></script>
<script src="assets/plugin/zircos/plugins/datatables/pdfmake.min.js"></script>
<script src="assets/plugin/zircos/plugins/datatables/vfs_fonts.js"></script>
<script src="assets/plugin/zircos/plugins/datatables/buttons.html5.min.js"></script>
<script src="assets/plugin/zircos/plugins/datatables/buttons.print.min.js"></script>
<!-- Akhir datatable -->
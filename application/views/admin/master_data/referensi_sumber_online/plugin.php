<!-- Awal CSS -->
<link href="assets/plugin/zircos/plugins/datatables/jquery.dataTables.min.css" rel="stylesheet" type="text/css"/>
<link href="assets/plugin/zircos/plugins/datatables/responsive.bootstrap.min.css" rel="stylesheet" type="text/css"/>
<link href="assets/plugin/zircos/plugins/datatables/dataTables.bootstrap.min.css" rel="stylesheet" type="text/css"/>
<link href="assets/plugin/zircos/material-design/assets/css/tambahan.css" rel="stylesheet" type="text/css"/>

<!-- Awal JS -->
<script src="assets/plugin/zircos/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="assets/plugin/zircos/plugins/datatables/dataTables.bootstrap.js"></script>
<script src="assets/plugin/zircos/plugins/datatables/dataTables.responsive.min.js"></script>
<script src="assets/plugin/zircos/plugins/datatables/responsive.bootstrap.min.js"></script>
<script src="assets/plugin/zircos/material-design/assets/pages/jquery.datatables.init.js"></script>

<!-- Awal Sweet-Alert  -->
<link href="assets/plugin/zircos/plugins/bootstrap-sweetalert/sweet-alert.css" rel="stylesheet" type="text/css">
<script src="assets/plugin/zircos/plugins/bootstrap-sweetalert/sweet-alert.min.js"></script>
<!-- Akhir Sweet-Alert  -->

<script type="text/javascript">
    var save_method;
    var table;

    $(document).ready(function() {
        $('#JudulHalaman').html('Referensi Sumber Online - VAMA');

        table = $('#table').DataTable({ 
            processing: true,
            serverSide: true,
            order: [],

            "ajax": {
                url: "<?php echo site_url('referensi_sumber_online/ajax_list')?>",
                type: "POST"
            },

            columnDefs: [{ 
                targets: [ -1 ],
                orderable: false,
            },],
        });

        var _swal = window.swal;
        window.swal = function(){
            var previousWindowKeyDown = window.onkeydown;
            _swal.apply(this, Array.prototype.slice.call(arguments, 0));
            window.onkeydown = previousWindowKeyDown;
        };
        
        $("input").change(function(){
            $(this).parent().parent().removeClass('has-error');
            $(this).next().empty();
        });
        $("textarea").change(function(){
            $(this).parent().parent().removeClass('has-error');
            $(this).next().empty();
        });
        $("select").change(function(){
            $(this).parent().parent().removeClass('has-error');
            $(this).next().empty();
        });
    });

    function reload_table()
    {
        table.ajax.reload(null,false);
    }

    function add_referensi()
    {
        save_method = 'add';
        $('#form')[0].reset();
        $('.form-horizontal').removeClass('has-error');
        $('.help-block').empty();
        $('#modal_form').modal('show');
        $('.modal-title').text('Tambah Referensi Sumber Online');
    }

    function edit_referensi(id_referensi)
    {
        save_method = 'update';
        $('#form')[0].reset();
        $('.form-horizontal').removeClass('has-error');
        $('.help-block').empty();

        $.ajax({
            url : "<?php echo site_url('referensi_sumber_online/ajax_edit'); ?>",
            type: "POST",
            cache: false,
            data: 'id_referensi=' + id_referensi,
            dataType: "JSON",
            success: function(data){
                $('[name="id_referensi"]').val(data.id_referensi);
                $('[name="nama_sumber_online"]').val(data.nama_sumber_online);
                $('#modal_form').modal('show');
                $('.modal-title').text('Edit Referensi Sumber Online');
            },
            error: function (jqXHR, textStatus, errorThrown){
                swal({
                    title: "Gagal!", 
                    text: "Data referensi sumber online gagal untuk ditampilkan.", 
                    type: "error", 
                    confirmButtonClass: 'btn-danger btn-md waves-effect waves-light',
                    confirmButtonText: "Ok"
                }, function(isConfirm){
                    window.location.href = "referensi_sumber_online";
                });
            }
        });
    }

    function save()
    {
        var url;
        var judul;
        var pesan;
        if(save_method == 'add'){
            judul   = "Yakin ingin menambahkan referensi sumber online ?";
            pesan   = "Nama Sumber Online : " + $('#nama_sumber_online').val();
            url     = "<?php echo site_url('referensi_sumber_online/ajax_add')?>";
        }else{
            judul   = "Yakin ingin memperbaharui referensi sumber online ?";
            pesan   = "Nama Jenis Barang : " + $('#nama_sumber_online').val();
            url     = "<?php echo site_url('referensi_sumber_online/ajax_update')?>";
        }

        swal({
            title: judul,
            text: pesan,
            type: "info",
            showCancelButton: true,
            confirmButtonClass: 'btn-primary btn-md waves-effect waves-light',
            confirmButtonText: "Iya",
            cancelButtonText: "Batal",
            closeOnConfirm: false,
        }, function (isConfirm){
            if(isConfirm){
                $('#btnSave').text('menyimpan...');
                $('#btnSave').attr('disabled',true);

                $.ajax({
                    url : url,
                    type: "POST",
                    data: $('#form').serialize(),
                    dataType: "JSON",
                    success: function(data){
                        if(data.status){
                            $('#modal_form').modal('hide');
                            reload_table();

                            swal({
                                title: "Berhasil!", 
                                text: "Data referensi sumber online berhasil disimpan.", 
                                type: "success", 
                                confirmButtonText: "Ok"
                            });
                        }else{
                            for (var i = 0; i < data.inputerror.length; i++){
                                $('[name="'+data.inputerror[i]+'"]').parent().parent().addClass('has-error'); 
                                $('[name="'+data.inputerror[i]+'"]').next().text(data.error_string[i]); 
                            }

                            swal({
                                title: "Gagal!", 
                                text: "Data referensi sumber online gagal disimpan.", 
                                type: "error", 
                                confirmButtonClass: 'btn-danger btn-md waves-effect waves-light',
                                confirmButtonText: "Ok"
                            });
                        }

                        $('#btnSave').text('Simpan');
                        $('#btnSave').attr('disabled',false);
                    },
                    error: function (jqXHR, textStatus, errorThrown){
                        swal({
                            title: "Gagal!", 
                            text: "Data referensi sumber online gagal disimpan.", 
                            type: "error", 
                            confirmButtonClass: 'btn-danger btn-md waves-effect waves-light',
                            confirmButtonText: "Ok"
                        }, function(isConfirm){
                            window.location.href = "referensi_sumber_online";
                        });
                    }
                });
            }
        });
    }

    function verifikasi_delete(id_referensi)
    {
        $.ajax({
            url : "<?php echo site_url('referensi_sumber_online/ajax_verifikasi_delete') ;?>",
            type: "POST",
            cache: false,
            data: 'id_referensi=' + id_referensi,
            dataType: "JSON",
            success: function(data){
                $('.modal-dialog').removeClass('modal-sm');
                $('.modal-dialog').addClass('modal-md');
                $('#ModalHeader').html('Informasi Hapus Jenis Barang');
                $('#ModalContent').html(data.pesan);
                $('#ModalFooter').html(data.footer);
                $('#ModalGue').modal('show');

            },error: function (jqXHR, textStatus, errorThrown){
                swal({
                    title: "Gagal!", 
                    text: "Data referensi sumber online gagal untuk ditampilkan", 
                    type: "error", 
                    confirmButtonClass: 'btn-danger btn-md waves-effect waves-light',
                    confirmButtonText: "Ok"
                }, function(isConfirm){
                    window.location.href = "referensi_sumber_online";
                });
            }
        });
    }

    function delete_referensi(id_referensi)
    {
        $.ajax({
            url : "<?php echo site_url('referensi_sumber_online/ajax_delete'); ?>",
            type: "POST",
            cache: false,
            data: 'id_referensi=' + id_referensi,
            dataType: "JSON",
            success: function(data){
                reload_table();
                swal({
                    title: "Berhasil!", 
                    text: "Data referensi sumber online berhasil dihapus.", 
                    type: "success", 
                    confirmButtonText: "Ok"
                });
            },
            error: function (jqXHR, textStatus, errorThrown){
                swal({
                    title: "Gagal!", 
                    text: "Data referensi sumber onlie gagal dihapus.", 
                    type: "error", 
                    confirmButtonClass: 'btn-danger btn-md waves-effect waves-light',
                    confirmButtonText: "Ok"
                }, function(isConfirm){
                    window.location.href = "referensi_sumber_online";
                });
            }
        });
    }

</script>
<!-- Akhir Script CRUD -->

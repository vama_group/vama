<!-- Bootstrap modal -->
<div class="modal fade" id="modal_form" role="dialog">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h5 class="modal-title">Jenis Barang</h5>
            </div>
            <div class="modal-body form">
                <form action="#" id="form" class="form-group">
                    <input type="hidden" value="" name="id_jenis_barang"/> 
                    <div class="form-body">
                        <div class="row">
                            <div class="form-horizontal">
                                <label class="control-label col-md-3 small">Nama Jenis Barang</label>
                                <div class="col-md-9">
                                    <input id="nama_jenis_barang" name="nama_jenis_barang" placeholder="Nama jenis barang" class="form-control text-dark" type="text" autofocus>
                                    <span class="help-block"></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>

            <div class="modal-footer">
                <button type="button" id="btnSave" onclick="save()" class="btn btn-primary">Simpan</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- End Bootstrap modal -->
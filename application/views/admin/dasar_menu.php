<li class="menu-title">NAVIGASI</li>

<li>
    <a href="dashboard" class="waves-effect"><i class="mdi mdi-view-dashboard"></i><span class="badge badge-success pull-right">2</span> <span> Dashboard </span> </a>
</li>

<!-- Awal menu master data -->
<li class="has_sub">
    <a href="javascript:void(0);" class="waves-effect"><i class="mdi mdi-view-list"></i><span> Master Data </span> <span class="menu-arrow"></span></a>
    <ul class="list-unstyled">
        <!-- Menu pegawai -->
        <li class="has_sub">
            <a href="javascript:void(0);" class="waves-effect"><i class="mdi mdi-account-card-details"></i><span> Pegawai </span> <span class="menu-arrow"></span></a>
            <ul class="list-unstyled">
                <li><a href="<?php echo base_url ('pegawai_pusat') ?>"><i class=""></i><span>Pegawai Pusat</span></a></li>
                <li><a href="<?php echo base_url ('pegawai_toko') ?>"><i class=""></i><span>Pegawai Toko</span></a></li>
            </ul>
        </li>
        
        <!-- Menu periode stok -->
        <li class="has_sub">
            <a href="javascript:void(0);" class="waves-effect"><i class="mdi mdi-calendar-clock"></i><span> Periode Stok </span> <span class="menu-arrow"></span></a>
            <ul class="list-unstyled">
                <li><a href="<?php echo base_url ('periode_stok_pusat') ?>"> Periode Stok Pusat</a></li>
                <li><a href="<?php echo base_url ('periode_stok_toko') ?>"> Periode Stok Toko</a></li>
            </ul>
        </li>

        <!-- Data Supplier -->
        <li class="has_sub">
            <a href="<?php echo base_url ('supplier') ?>" class="waves-effect"><i class="fa fa-building"></i><span>Supplier </span></a>
        </li>

        <!-- Menu toko -->
        <!-- <li class="has_sub">
            <a href="<?php //echo base_url ('toko') ?>" class="waves-effect"><i class="typcn typcn-home"></i><span> Toko </span></a>
        </li> -->

        <li class="has_sub">
            <a href="javascript:void(0);" class="waves-effect"><i class="mdi mdi-account-location"></i><span> Customer </span> <span class="menu-arrow"></span></a>
            <ul class="list-unstyled">
                <li><a href="<?php echo base_url ('customer_pusat') ?>"> Customer Pusat</a></li>
                <li><a href="<?php echo base_url ('customer_toko') ?>"> Customer Toko</a></li>
            </ul>
        </li>

        <!-- Menu kartu edc pusat dan toko -->
        <li class="has_sub">
            <a href="javascript:void(0);" class="waves-effect"><i class="mdi mdi-credit-card"></i><span> Kartu EDC </span> <span class="menu-arrow"></span></a>
            <ul class="list-unstyled">
                <li><a href="<?php echo base_url ('kartu_edc_pusat') ?>"> Kartu EDC Pusat</a></li>
                <li><a href="<?php echo base_url ('kartu_edc_toko') ?>"> Kartu EDC Toko</a></li>
            </ul>
        </li>
    </ul>
</li>
<!-- Akhir Menu master data -->

<!-- Menu barang -->
<li class="has_sub">
    <a href="javascript:void(0);" class="waves-effect"><i class="mdi mdi-cube-outline"></i><span> Master Barang </span> <span class="menu-arrow"></span></a>
    <ul class="list-unstyled">
        <li><a href="<?php echo base_url ('jenis_barang') ?>"> Jenis Barang</a></li>
        <li><a href="<?php echo base_url ('kategori_barang') ?>"> Kategori Barang</a></li>
        <li><a href="<?php echo base_url ('barang_pusat') ?>"> Barang Pusat</a></li>
        <li><a href="<?php echo base_url ('barang_toko') ?>"> Barang Toko</a></li>
    </ul>
</li>

<!-- <li class="menu-title">Transaksi</li> -->

<!-- Awal Menu transaksi -->
<li class="has_sub">
    <a href="javascript:void(0);" class="waves-effect"><i class="mdi mdi-cash-multiple"></i><span> Transaksi </span> <span class="menu-arrow"></span></a>
    <ul class="list-unstyled">
        <!-- Menu stok opname -->
        <li class="has_sub">
            <a href="<?php echo base_url ('stok_opname_pusat') ?>" class="waves-effect"><i class="mdi mdi-counter"></i><span>Stok Opname </span></a>
            <!-- <ul class="list-unstyled">
                <li><a href="<?php // echo base_url ('stok_opname_pusat') ?>">Stok Opname Pusat</a></li>
                <li><a href="<?php // echo base_url ('stok_opname_toko') ?>">Stok Opname Toko</a></li>
            </ul> -->
        </li>

        <!-- Menu pembelian -->
        <li class="has_sub">
            <a href="<?php echo base_url ('pembelian') ?>" class="waves-effect"><i class="mdi mdi-database-plus"></i><span>Pembelian </span></a>
        </li>

        <!-- Menu retur pembelian -->
        <li class="has_sub">
            <a href="<?php echo base_url ('retur_pembelian') ?>" class="waves-effect"><i class="mdi mdi-database-minus"></i><span>Retur Pembelian </span></a>
        </li>

        <!-- Menu Penjualan -->
        <li class="has_sub">
            <a href="<?php echo base_url ('penjualan') ?>" class="waves-effect"><i class="fa fa-cart-plus"></i><span>Penjualan </span></a>
        </li>

        <!-- Menu Retur Penjualan -->
        <li class="has_sub">
            <a href="<?php echo base_url ('retur_penjualan') ?>" class="waves-effect"><i class="fa fa-cart-arrow-down"></i><span>Retur Penjualan </span></a>
        </li>

        <!-- Perakitan -->
        <li class="has_sub">
            <a href="<?php echo base_url ('perakitan') ?>" class="waves-effect"><i class="fa fa-cubes"></i><span>Perakitan Barang </span></a>
        </li>

        <!-- Paket dan Harga spesial -->
        <li class="has_sub">
            <a href="<?php echo base_url ('harga_spesial') ?>" class="waves-effect"><i class="mdi mdi-sale"></i><span>Paket & Hrg Spesial </span></a>
        </li>

        <!-- Menu Hadiah -->
        <li class="has_sub">
            <a href="<?php echo base_url ('hadiah') ?>" class="waves-effect"><i class="mdi mdi-gift"></i><span>Hadiah </span></a>
        </li>
    </ul>
</li>
<!-- Akhir menu transaksi -->

<!-- Awal Laporan menu laporan -->
<li class="has_sub">
    <a href="javascript:void(0);" class="waves-effect"><i class="mdi mdi-file-chart"></i><span> Laporan </span> <span class="menu-arrow"></span></a>
    <ul class="list-unstyled">
        <!-- Laporan stok opname -->
        <li class="has_sub">
            <a href="javascript:void(0);" class="waves-effect"><i class="mdi mdi-counter"></i><span> Stok Opname </span> <span class="menu-arrow"></span></a>
            <ul class="list-unstyled">
                <li><a href="<?php echo base_url ('stok_opname_pusat') ?>">Stok Opname Pusat</a></li>
                <li><a href="<?php echo base_url ('stok_opname_toko') ?>">Stok Opname Toko</a></li>
            </ul>
        </li>

        <!-- Laporan pembelian -->
        <li class="has_sub">
            <a href="<?php echo base_url ('laporan_pembelian') ?>" class="waves-effect"><i class="mdi mdi-database-plus"></i><span>Pembelian </span></a>
        </li>

        <!-- Laporan retur pembelian -->
        <li class="has_sub">
            <a href="<?php echo base_url ('laporan_retur_pembelian') ?>" class="waves-effect"><i class="mdi mdi-database-minus"></i><span>Retur Pembelian </span></a>
        </li>

        <!-- Laporan Penjualan -->
        <li class="has_sub">
            <a href="<?php echo base_url ('laporan_penjualan') ?>" class="waves-effect"><i class="fa fa-cart-plus"></i><span>Penjualan </span></a>
        </li>

        <!-- Laporan Retur Penjualan -->
        <li class="has_sub">
            <a href="<?php echo base_url ('laporan_retur_penjualan') ?>" class="waves-effect"><i class="fa fa-cart-arrow-down"></i><span>Retur Penjualan </span></a>
        </li>

        <!-- Laporan perakitan -->
        <li class="has_sub">
            <a href="<?php echo base_url ('laporan_perakitan') ?>" class="waves-effect"><i class="fa fa-cubes"></i><span>Perakitan Barang </span></a>
        </li>

        <!-- Laporan paket dan harga spesial -->
        <li class="has_sub">
            <a href="<?php echo base_url ('laporan_harga_spesial') ?>" class="waves-effect"><i class="mdi mdi-sale"></i><span>Paket & Hrg Spesial </span></a>
        </li>

        <!-- Laporan hadiah -->
        <li class="has_sub">
            <a href="<?php echo base_url ('laporan_hadiah') ?>" class="waves-effect"><i class="mdi mdi-gift"></i><span>Hadiah </span></a>
        </li>
    </ul>
</li>
<!-- Akhir menu laporan -->
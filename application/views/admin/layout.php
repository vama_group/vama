<?php
    // Proteksi halaman
    $this->simple_login->cek_login();
    $this->simple_login->cek_lockscreen();
?>

<!DOCTYPE html>
<html>
    <head>
        <base href="<?php echo base_url(); ?>">
        <meta name="theme-color" content="#337ab7" /> <!-- URL Theme Color untuk Chrome, Firefox OS, Opera dan Vivaldi -->
        <meta name="msapplication-navbutton-color" content="#337ab7" /> <!-- URL Theme Color untuk Windows Phone -->
        
        <meta name="apple-mobile-web-app-capable" content="yes" /><!-- URL Theme Color untuk iOS Safari -->
        <meta name="apple-mobile-web-app-status-bar-style" content="#337ab7" /><!-- URL Theme Color untuk iOS Safari -->

        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="A fully featured admin theme which can be used to build CRM, CMS, etc.">
        <meta name="author" content="Coderthemes">

        <!-- App favicon -->
        <link rel="shortcut icon" href="assets/upload/logo/VMG-1.png">

        <!-- App title -->
        <title id="JudulHalaman">VAMA GROUP</title>

        <!-- App css -->
        <link href="assets/plugin/zircos/material-design/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/plugin/zircos/material-design/assets/css/core.css" rel="stylesheet" type="text/css" />
       <!--  <link href="assets/plugin/zircos/material-design/assets/css/components.css" rel="stylesheet" type="text/css" /> -->
        <link href="assets/plugin/zircos/material-design/assets/css/icons.css" rel="stylesheet" type="text/css" />
        <link href="assets/plugin/zircos/material-design/assets/css/pages.css" rel="stylesheet" type="text/css" />
        <link href="assets/plugin/zircos/material-design/assets/css/menu.css" rel="stylesheet" type="text/css" />
        <link href="assets/plugin/zircos/material-design/assets/css/responsive.css" rel="stylesheet" type="text/css" />
		<link href="assets/plugin/zircos/plugins/switchery/switchery.min.css" rel="stylesheet">

        <script src="assets/plugin/zircos/material-design/assets/js/modernizr.min.js"></script>
        <script>
            var resizefunc = [];
        </script>

        <!-- jQuery  -->
        <script src="assets/plugin/zircos/material-design/assets/js/jquery.min.js"></script>
        <script src="assets/plugin/zircos/material-design/assets/js/bootstrap.min.js"></script>
        <script src="assets/plugin/zircos/material-design/assets/js/detect.js"></script>
        <script src="assets/plugin/zircos/material-design/assets/js/fastclick.js"></script>
        <script src="assets/plugin/zircos/material-design/assets/js/jquery.blockUI.js"></script>
        <script src="assets/plugin/zircos/material-design/assets/js/waves.js"></script>
        <script src="assets/plugin/zircos/material-design/assets/js/jquery.slimscroll.js"></script>
        <script src="assets/plugin/zircos/material-design/assets/js/jquery.scrollTo.min.js"></script>
        <script src="assets/plugin/zircos/plugins/switchery/switchery.min.js"></script>         
    </head>


    <!-- <body class="fixed-left"> -->
    <body class="fixed-left">
        <!-- <div id='LoadingDulu'></div> -->
        
        <!-- Loader -->
        <div id="preloader">
            <div id="status">
                <div class="spinner">
                  <div class="spinner-wrapper">
                    <div class="rotator">
                      <div class="inner-spin"></div>
                      <div class="inner-spin"></div>
                    </div>
                  </div>
                </div>
            </div>
        </div>

        <!-- Begin page -->
        <div id="wrapper">

            <!-- Top Bar Start -->
            <div class="topbar">
                <!-- LOGO -->
                <div class="topbar-left">
                    <a href="dashboard" class="logo"><span>VA<span>MA</span></span><i class="mdi mdi-cube"></i></a>
                    <!-- Image logo -->
                    <!--<a href="index.html" class="logo">-->
                        <!--<span>-->
                            <!--<img src="assets/plugin/zircos/material-design/assets/images/logo.png" alt="" height="30">-->
                        <!--</span>-->
                        <!--<i>-->
                            <!--<img src="assets/plugin/zircos/material-design/assets/images/logo_sm.png" alt="" height="28">-->
                        <!--</i>-->
                    <!--</a>-->
                </div>

                <!-- Button mobile view to collapse sidebar menu -->
                <div id="navbar" class="navbar navbar-default" role="navigation">
                    <div class="container">

                        <!-- Navbar-left -->
                        <ul class="nav navbar-nav navbar-left">
                            <li>
                                <button id="TombolMenu" class="button-menu-mobile open-left waves-effect waves-light">
                                    <i class="mdi mdi-menu"></i>
                                </button>
                            </li>

                            <li class="hidden-xs">
                                <form id="KotakPencarian" role="search" class="app-search">
                                    <input type="text" placeholder="Search..." class="form-control">
                                    <a href=""><i class="fa fa-search"></i></a>
                                </form>
                            </li>
                        </ul>

                        <!-- Right(Notification) -->
                        <ul class="nav navbar-nav navbar-right">
                            <!-- <li>
                                <a href="#" class="right-menu-item dropdown-toggle" data-toggle="dropdown">
                                    <i class="mdi mdi-bell"></i>
                                    <span class="badge up bg-primary">4</span>
                                </a>

                                <ul class="dropdown-menu dropdown-menu-right arrow-dropdown-menu arrow-menu-right dropdown-lg user-list notify-list">
                                    <li>
                                        <h5>Notifications</h5>
                                    </li>
                                    <li>
                                        <a href="#" class="user-list-item">
                                            <div class="icon bg-info">
                                                <i class="mdi mdi-account"></i>
                                            </div>
                                            <div class="user-desc">
                                                <span class="name">New Signup</span>
                                                <span class="time">5 hours ago</span>
                                            </div>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#" class="user-list-item">
                                            <div class="icon bg-danger">
                                                <i class="mdi mdi-comment"></i>
                                            </div>
                                            <div class="user-desc">
                                                <span class="name">New Message received</span>
                                                <span class="time">1 day ago</span>
                                            </div>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#" class="user-list-item">
                                            <div class="icon bg-warning">
                                                <i class="mdi mdi-settings"></i>
                                            </div>
                                            <div class="user-desc">
                                                <span class="name">Settings</span>
                                                <span class="time">1 day ago</span>
                                            </div>
                                        </a>
                                    </li>
                                    <li class="all-msgs text-center">
                                        <p class="m-0"><a href="#">See all Notification</a></p>
                                    </li>
                                </ul>
                            </li> -->

                            <!-- <li>
                                <a href="#" class="right-menu-item dropdown-toggle" data-toggle="dropdown">
                                    <i class="mdi mdi-email"></i>
                                    <span class="badge up bg-danger">8</span>
                                </a>

                                <ul class="dropdown-menu dropdown-menu-right arrow-dropdown-menu arrow-menu-right dropdown-lg user-list notify-list">
                                    <li>
                                        <h5>Messages</h5>
                                    </li>
                                    <li>
                                        <a href="#" class="user-list-item">
                                            <div class="avatar">
                                                <img src="assets/plugin/zircos/material-design/assets/images/users/avatar-2.jpg" alt="">
                                            </div>
                                            <div class="user-desc">
                                                <span class="name">Patricia Beach</span>
                                                <span class="desc">There are new settings available</span>
                                                <span class="time">2 hours ago</span>
                                            </div>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#" class="user-list-item">
                                            <div class="avatar">
                                                <img src="assets/plugin/zircos/material-design/assets/images/users/avatar-3.jpg" alt="">
                                            </div>
                                            <div class="user-desc">
                                                <span class="name">Connie Lucas</span>
                                                <span class="desc">There are new settings available</span>
                                                <span class="time">2 hours ago</span>
                                            </div>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#" class="user-list-item">
                                            <div class="avatar">
                                                <img src="assets/plugin/zircos/material-design/assets/images/users/avatar-4.jpg" alt="">
                                            </div>
                                            <div class="user-desc">
                                                <span class="name">Margaret Becker</span>
                                                <span class="desc">There are new settings available</span>
                                                <span class="time">2 hours ago</span>
                                            </div>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#" class="user-list-item">
                                            <div class="avatar">
                                                <img src="assets/plugin/zircos/material-design/assets/images/users/avatar-4.jpg" alt="">
                                            </div>
                                            <div class="user-desc">
                                                <span class="name"><b>STOP LAMP</b></span>
                                                <span class="name">SKU : 12345</span>
                                                <span class="name">ECERAN : Rp. 900.000</span>
                                            </div>
                                        </a>
                                    </li>
                                </ul>
                            </li> -->

                            <li>
                                <a href="javascript:void(0);" class="right-bar-toggle right-menu-item">
                                    <i class="mdi mdi-settings"></i>
                                </a>
                            </li>

                            <li class="dropdown user-box">
                                <a href="" class="dropdown-toggle waves-effect waves-light user-link" data-toggle="dropdown" aria-expanded="true">

                                    <?php if($data_pegawai->foto !="") { ?>
                                    <img src="<?php echo base_url('assets/upload/image/pegawai_pusat/thumbs/'.$data_pegawai->foto)?>" alt="user-img" class="img-circle user-img">
                                    
                                    <?php } else{ ?>
                                    <img src="<?php echo base_url() ?>assets/upload/image/pegawai_pusat/thumbs/avatar.jpg" alt="user-img" class="img-circle user-img">
                                    <?php } ?>

                                </a>

                                <ul class="dropdown-menu dropdown-menu-right arrow-dropdown-menu arrow-menu-right user-list notify-list">
                                    <li>
                                        <h5>Hi, <?php echo $data_pegawai->nama_pegawai ?></h5>
                                    </li>
                                    <li><a href="<?php echo base_url('profile') ?>"><i class="ti-user m-r-5"></i> Profile</a></li>
                                    <li><a href="<?php echo base_url('profile/password') ?>"><i class="ti-lock m-r-5"></i> Change Password</a></li>
                                    <li><a href="<?php echo base_url('login/lock?redirect='.current_url()) ?>"><i class="ti-settings m-r-5"></i> Lock Screen</a></li>
                                    <li><a href="<?php echo base_url('login/logout') ?>"><i class="ti-power-off m-r-5"></i> Logout</a></li>
                                </ul>                                
                                <!-- <a href="" class="dropdown-toggle waves-effect waves-light user-link" data-toggle="dropdown" aria-expanded="true">
                                    <img src="assets/plugin/zircos/material-design/assets/images/users/avatar-1.jpg" alt="user-img" class="img-circle user-img">
                                </a>

                                <ul class="dropdown-menu dropdown-menu-right arrow-dropdown-menu arrow-menu-right user-list notify-list">
                                    <li>
                                        <h5>Hi, John</h5>
                                    </li>
                                    <li><a href="javascript:void(0)"><i class="ti-user m-r-5"></i> Profile</a></li>
                                    <li><a href="javascript:void(0)"><i class="ti-settings m-r-5"></i> Settings</a></li>
                                    <li><a href="javascript:void(0)"><i class="ti-lock m-r-5"></i> Lock screen</a></li>
                                    <li><a href="javascript:void(0)"><i class="ti-power-off m-r-5"></i> Logout</a></li>
                                </ul> -->
                            </li>

                        </ul> <!-- end navbar-right -->

                    </div><!-- end container -->
                </div><!-- end navbar -->
            </div>
            <!-- Top Bar End -->


            <!-- ========== Left Sidebar Start ========== -->
            <div class="left side-menu">
                <div class="sidebar-inner slimscrollleft">

                    <!--- Sidemenu -->
                    <div id="sidebar-menu">
                        <!-- Awal Informasi User Login -->
                        <div class="user-details">
                            <div class="overlay"></div>
                            <div class="text-center">
                                <?php if($data_pegawai->foto !="") { ?>
                                        <img src="<?php echo base_url('assets/upload/image/pegawai_pusat/thumbs/'.$data_pegawai->foto)?>" alt="user-img" class="thumb-md img-circle">
                                    <?php }else{ ?>
                                    <img src="<?php echo base_url() ?>assets/upload/image/pegawai_pusat/thumbs/avatar.jpg" alt="user-img" class="thumb-md img-circle">
                                <?php } ?>      
                            </div>
                            <div class="user-info">
                                <div>
                                    <a href="#setting-dropdown" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                        <?php echo $data_pegawai->nama_pegawai ?> - 
                                        <span id="usergroup_name"><?php echo $data_pegawai->usergroup_name ?></span>
                                        <span class="mdi mdi-menu-down"></span>
                                    </a>
                                </div>
                            </div>
                        </div>

                        <div class="dropdown" id="setting-dropdown">
                            <ul class="dropdown-menu">
                                <li><a href="<?php echo base_url('profile') ?>"><i class="mdi mdi-face-profile m-r-5"></i> Profile</a></li>
                                <li><a href="<?php echo base_url('profile/password') ?>"><i class="mdi mdi-account-settings-variant m-r-5"></i> Change Password</a></li>
                                <li><a href="<?php echo base_url('login/lock?redirect='.current_url()) ?>"><i class="mdi mdi-lock m-r-5"></i> Lock screen</a></li>
                                <li><a href="<?php echo base_url('login/logout') ?>"><i class="mdi mdi-logout m-r-5"></i> Logout</a></li>
                            </ul>
                        </div>
                        <!-- Akhir Informasi User Login -->

                        <!-- Awal daftar menu -->
                        <ul id="daftar_menu">
                            <li class="menu-title">NAVIGASI</li>
                            <?php if($this->session->userdata('usergroup_name') == 'Super Admin'){ ?>
                            <li class="has_sub"><a href="javascript:void(0);" class="waves-effect">
                                <i class="fa fa-tachometer"></i>
                                <span> Dashboard</span><span class="menu-arrow"></span></a>
                                <ul class="list-unstyled">
                                    <li class="has_sub">
                                        <a href="dashboard" class="waves-effect"><i class="fa fa-tachometer"></i><span> Umum</span></a>
                                    </li>
                                    <li class="has_sub">
                                        <a href="dashboard2" class="waves-effect"><i class="fa fa-calculator"></i><span> Detail</span></a>
                                    </li>
                                    <li class="has_sub">
                                        <a href="dashboard_grafik" class="waves-effect"><i class="fa fa-line-chart"></i><span> Grafik</span></a>
                                    </li>                                 
                                </ul>
                            </li>
                            <?php }else{ ?>
                                <li class="has_sub">
                                    <a href="dashboard" class="waves-effect"><i class="fa fa-tachometer"></i><span> Dashboard</span></a>
                                </li>
                            <?php } ?>
                            <!-- Induk menu -->
                            <?php foreach ($data_induk_menu->result() AS $induk_menu){ ?>
                                <li class="has_sub"><a href="javascript:void(0);" class="waves-effect">
                                    <i class="<?php echo $induk_menu->icon ?>"></i>
                                    <span> <?php echo $induk_menu->nama_menu ?></span><span class="menu-arrow"></span></a>
                                    <ul class="list-unstyled">
                                        <!-- Sub menu -->
                                        <?php foreach($this->useraccess_pusat->get_sub_menu($induk_menu->id_pegawai, $induk_menu->id_menu)->result() AS $sub_menu){ ?>
                                            <?php if($sub_menu->url == 'sub_menu'){ ?>
                                                <li class="has_sub"><a href="javascript:void(0);" class="waves-effect"><i class="<?php echo $sub_menu->icon ?>"></i>
                                                    <span><?php echo $sub_menu->nama_menu ?></span><span class="menu-arrow"></span></a>
                                                    <ul class="list-unstyled">
                                                         <?php foreach($this->useraccess_pusat->get_anak_menu($sub_menu->id_pegawai, $sub_menu->id_menu)->result() AS $anak_menu){ ?>
                                                            <li><a href="<?php echo base_url($anak_menu->url) ?>"><i class="<?php echo $anak_menu->icon ?>"></i>
                                                                <small><?php echo $anak_menu->nama_menu ?></small></a>
                                                            </li>
                                                         <?php } ?>
                                                    </ul>
                                                </li>
                                            <?php }else{ ?>
                                                <li class="has_sub">
                                                    <a href="<?php echo base_url($sub_menu->url) ?>" class="waves-effect"><i class="<?php echo $sub_menu->icon ?>"></i>
                                                    <span><?php echo $sub_menu->nama_menu ?></span></a>
                                                </li>
                                            <?php } ?>
                                        <?php } ?>
                                    </ul>
                                </li>
                            <?php } ?>
                        </ul>
                        <!-- Akhir daftar menu -->
                    </div>

                    <!-- Sidebar -->
                    <!-- <div class="clearfix"></div> -->

                    <div class="help-box">
                        <h6 class="text-muted m-t-0">For Help ?</h6>
                        <p class="">
                            <span class="text-dark"><b>Email:</b></span>
                            support@mrc.com
                        </p>
                        <!-- <p class="m-b-0"><span class="text-dark"><b>HP: 0856-951-20071</b></span> <br/> </p> -->
                    </div>

                </div>
                <!-- Sidebar -left -->

            </div>
            <!-- Left Sidebar End -->

            <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="content-page">
                <pre id="session" style="display: none; background-color: #000; color: #7FFF00; padding: 5px; margin-top: 60px; margin-buttom: 0px;">
                    <?php print_r($this->session->userdata()); ?>
                </pre>    
                
                <?php echo $isi_halaman; ?>
                <footer class="footer text-right">
                    2018 © Janan Rasyid.
                </footer>
            </div>

            <!-- ============================================================== -->
            <!-- End Right content here -->
            <!-- ============================================================== -->


            <!-- Right Sidebar -->
            <div class="side-bar right-bar">
                <a href="javascript:void(0);" class="right-bar-toggle">
                    <i class="mdi mdi-close-circle-outline"></i>
                </a>
                <h4 class="">Settings</h4>
                <div class="setting-list nicescroll">
                    <div class="row m-t-20">
                        <div class="col-xs-8">
                            <h5 class="m-0">Notifications</h5>
                            <p class="text-muted m-b-0"><small>Do you need them?</small></p>
                        </div>
                        <div class="col-xs-4 text-right">
                            <input type="checkbox" checked data-plugin="switchery" data-color="#7fc1fc" data-size="small"/>
                        </div>
                    </div>

                    <div class="row m-t-20">
                        <div class="col-xs-8">
                            <h5 class="m-0">API Access</h5>
                            <p class="m-b-0 text-muted"><small>Enable/Disable access</small></p>
                        </div>
                        <div class="col-xs-4 text-right">
                            <input type="checkbox" checked data-plugin="switchery" data-color="#7fc1fc" data-size="small"/>
                        </div>
                    </div>

                    <div class="row m-t-20">
                        <div class="col-xs-8">
                            <h5 class="m-0">Auto Updates</h5>
                            <p class="m-b-0 text-muted"><small>Keep up to date</small></p>
                        </div>
                        <div class="col-xs-4 text-right">
                            <input type="checkbox" checked data-plugin="switchery" data-color="#7fc1fc" data-size="small"/>
                        </div>
                    </div>

                    <div class="row m-t-20">
                        <div class="col-xs-8">
                            <h5 class="m-0">Online Status</h5>
                            <p class="m-b-0 text-muted"><small>Show your status to all</small></p>
                        </div>
                        <div class="col-xs-4 text-right">
                            <input type="checkbox" checked data-plugin="switchery" data-color="#7fc1fc" data-size="small"/>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /Right-bar -->

        </div>
        <!-- END wrapper -->

        <link href="assets/plugin/zircos/material-design/assets/css/components.css" rel="stylesheet" type="text/css" />
        
        <!-- App js -->
        <script src="assets/plugin/zircos/material-design/assets/js/jquery.core.js"></script>
        <script src="assets/plugin/zircos/material-design/assets/js/jquery.app.js"></script>
    </body>
</html>

<script type="text/javascript">
    document.onkeyup=function(e){
        var e = e || window.event; // for IE to cover IEs window object
        if(e.shiftKey && e.which == 83) {
            $("#session").show();
            return false;
        }
        if(e.shiftKey && e.which == 81) {
            $("#session").hide();
            return false;
        }
    }  
</script>
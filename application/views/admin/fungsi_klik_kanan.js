<?php if($this->session->userdata('usergroup_name') == 'Super Admin'){ ?>
        var status_modal    = '0';
        var pesan_modal     = 'Apakah anda yakin ingin menampilkan modal ?';

        function clickIE4(){
            if(event.button==2){
                swal({
                    title: pesan_modal,
                    type: "info",
                    showCancelButton: true,
                    confirmButtonClass: 'btn-primary btn-md waves-effect waves-light',
                    confirmButtonText: "Iya, saya yakin",
                    cancelButtonText: "Batal",
                },function (isConfirm){
                    if(isConfirm){
                        TampilkanModal();
                    } 
                });
                return false;
            }
        }
        
        function clickNS4(e){
            if (document.layers||document.getElementById&&!document.all){
                if (e.which==2||e.which==3){
                    swal({
                        title: pesan_modal,
                        type: "info",
                        showCancelButton: true,
                        confirmButtonClass: 'btn-primary btn-md waves-effect waves-light',
                        confirmButtonText: "Iya, saya yakin",
                        cancelButtonText: "Batal",
                    },function (isConfirm){
                        if(isConfirm){
                            TampilkanModal();
                        } 
                    });
                    return false;
                }
            }
        }

        if(document.layers){
            document.captureEvents(Event.MOUSEDOWN);
            document.onmousedown=clickNS4;
        }else if (document.all&&!document.getElementById){
            document.onmousedown=clickIE4;
        }

        document.oncontextmenu = new Function(
            "swal({" + 
                "title: pesan_modal," + 
                "type: 'info'," +
                "showCancelButton: true," +
                "confirmButtonClass: 'btn-primary btn-md waves-effect waves-light'," +
                "confirmButtonText: 'Iya, saya yakin'," +
                "cancelButtonText: 'Batal'," +
            "},function (isConfirm){" +
                "if(isConfirm){TampilkanModal();}" + 
            "});" +
            "return false;"
        )

        function TampilkanModal(){
            if(status_modal == '0'){
                $('#KotakModal').removeAttr('hidden');
                status_modal    = '1';
                pesan_modal     = "Sembunyikan modal ?";
            }else if(status_modal == '1'){
                $('#KotakModal').prop('hidden', true);
                status_modal    = '0';
                pesan_modal     = "Apakah anda yakin ingin menampilkan modal ?";
            }
        }
    <?php } ?>
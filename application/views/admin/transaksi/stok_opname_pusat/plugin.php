<!-- Awal CSS -->
<link href="assets/plugin/zircos/plugins/datatables/jquery.dataTables.min.css" rel="stylesheet" type="text/css"/>
<link href="assets/plugin/zircos/plugins/datatables/responsive.bootstrap.min.css" rel="stylesheet" type="text/css"/>
<link href="assets/plugin/zircos/plugins/datatables/dataTables.bootstrap.min.css" rel="stylesheet" type="text/css"/>
<link href="assets/plugin/zircos/material-design/assets/css/tambahan.css" rel="stylesheet" type="text/css"/>
<!-- Akhir CSS -->

<!-- Awal JS -->
<script src="assets/plugin/zircos/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="assets/plugin/zircos/plugins/datatables/dataTables.bootstrap.js"></script>
<script src="assets/plugin/zircos/material-design/assets/js/detect.js"></script>
<script src="assets/plugin/zircos/material-design/assets/js/jquery.slimscroll.js"></script>
<script src="assets/plugin/zircos/plugins/datatables/dataTables.responsive.min.js"></script>
<script src="assets/plugin/zircos/plugins/datatables/responsive.bootstrap.min.js"></script>
<script src="assets/plugin/zircos/material-design/assets/pages/jquery.datatables.init.js"></script>
<!-- Akhir JS -->

<!-- Awal Sweet-Alert  -->
<link href="assets/plugin/zircos/plugins/bootstrap-sweetalert/sweet-alert.css" rel="stylesheet" type="text/css">
<script src="assets/plugin/zircos/plugins/bootstrap-sweetalert/sweet-alert.min.js"></script>
<!-- Akhir Sweet-Alert  -->

<script>
    $('#navbar').removeClass('navbar-default');
    $('#navbar').addClass('navbar-biru-muda');
    $('#JudulHalaman').html('Stok Opname Pusat - VAMA');

    var table_so         = $('#TableTransaksi').DataTable();
    var table_riwayat_so = $('#table_riwayat_stok_opname_pusat').DataTable();
    var table_batal_so   = $('#table_batal_stok_opname_pusat').DataTable();

    $(document).ready(function(){
        $("#pencarian_kode").focus();
        $("#wrapper").removeClass('forced');
        $("#wrapper").addClass('forced enlarged');
        TampilkanDataStokOpname();
    });

    function TampilkanDataStokOpname(){
        var id_periode_stok_pusat  = $('#id_periode_stok_pusat').val();

        table_so.clear();table_so.destroy();
        table_so=$('#TableTransaksi').DataTable(
        {
            ordering: true,
            bFilter:true,
            processing:true,
            serverSide:true,
            order:[],
            lengthMenu: [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
            pagingType: "full",

            oLanguage:{
                sLoadingRecords:'Silakan Tunggu',
                sProcessing:"<img src='assets/loader/cubeloader2.2.gif' width='50' height='50'> Harap tunggu",
                sZeroRecords:"Belum ada data untuk ditampilkan",
                sSearch : "Pencarian data"
            },
            ajax:{
                url: "<?php echo site_url('stok_opname_pusat/ajax_list')?>",
                type: "POST",
                data: {'id_periode_stok_pusat' : id_periode_stok_pusat}
            },
            columnDefs: [
                            { 
                                "targets": [ -1 ],
                                "orderable": false,
                            },
                        ],
        });
    }

    function TampilkanDataRiwayatStokOpname(){
        var id_periode_stok_pusat  = $('#id_periode_stok_pusat').val();

        table_riwayat_so.clear();table_riwayat_so.destroy();
        table_riwayat_so=$('#table_riwayat_stok_opname_pusat').DataTable(
        {
            ordering: true,
            bFilter:true,
            processing:true,
            serverSide:true,
            order:[],
            lengthMenu: [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
            pagingType: "full",

            oLanguage:{
                sLoadingRecords:'Silakan Tunggu',
                sProcessing:"<img src='assets/loader/cubeloader2.2.gif' width='50' height='50'> Harap tunggu",
                sZeroRecords:"Belum ada data untuk ditampilkan",
                sSearch : "Pencarian data"
            },
            ajax:{
                url: "<?php echo site_url('stok_opname_pusat/ajax_list_riwayat')?>",
                type: "POST",
                data: {'id_periode_stok_pusat' : id_periode_stok_pusat}
            },
            columnDefs: [
                            { 
                                "targets": [ -1 ],
                                "orderable": false,
                            },
                        ],
        });
    }
    
    function TampilkanDataStokOpnameBatal(){
        var id_periode_stok_pusat  = $('#id_periode_stok_pusat').val();

        table_batal_so.clear();table_batal_so.destroy();
        table_batal_so=$('#table_batal_stok_opname_pusat').DataTable(
        {
            ordering: true,
            bFilter:true,
            processing:true,
            serverSide:true,
            order:[],
            lengthMenu: [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
            pagingType: "full",

            oLanguage:{
                sLoadingRecords:'Silakan Tunggu',
                sProcessing:"<img src='assets/loader/cubeloader2.2.gif' width='50' height='50'> Harap tunggu",
                sZeroRecords:"Belum ada data untuk ditampilkan",
                sSearch : "Pencarian data"
            },
            ajax:{
                url: "<?php echo site_url('stok_opname_pusat/ajax_list_batal')?>",
                type: "POST",
                data: {'id_periode_stok_pusat' : id_periode_stok_pusat}
            },
            columnDefs: [
                            { 
                                "targets": [ -1 ],
                                "orderable": false,
                            },
                        ],
        });
    }

    var delay = (function (){
        var timer = 0;
        return function (callback, ms) {
            clearTimeout(timer);
            timer = setTimeout(callback, ms);
        };
    })();

    $(document).on('click', '#tab_so', function(){
        TampilkanDataStokOpname();
    });

    $(document).on('click', '#tab_riwayat_so', function(){
        // reload_table_riwayat_so();
        TampilkanDataRiwayatStokOpname();
    });

    $(document).on('click', '#tab_batal_so', function(){
        // reload_table_batal_so();
        TampilkanDataStokOpnameBatal();
    });

    function reload_table_so()
    {
        table_so.ajax.reload();
    }

    function reload_table_riwayat_so()
    {
        table_riwayat_so.ajax.reload();
    }

    function reload_table_batal_so()
    {
        table_batal_so.ajax.reload();
    }

    $(document).on('click', '#daftar-autocomplete li', function(){
        var id_barang    = $(this).find('span#id_barang').html();
        var SKU          = $(this).find('span#skunya').html();
        var NamaBarang   = $(this).find('span#barangnya').html();
        var TipeCustomer = $('span#data_tipe_customer_pusat').html();
        var JenisHarga   = 'ECERAN';
        var Harganya     = $(this).find('span#eceran').html();
        var Jumlah_SO    = $(this).find('span#jumlah_so_sebelumnya').html();
        var Status       = $(this).find('span#status').html();

        $('div#hasil_pencarian').hide();
        $('#id_barang_pusat').html(id_barang);
        $('#pencarian_kode').val(SKU);
        $('#nama_barang').html(NamaBarang);
        $('#so_sebelumnya').html(Jumlah_SO).removeClass('text-dark').addClass('text-danger');
        $('#status_barang').html(Status).removeClass('text-dark').addClass('text-danger');
        $('#harga_satuan').html('Rp. ' + to_rupiah(Harganya));

        if(Jumlah_SO > 0){
            $('#so_sebelumnya').removeClass('text-dark').addClass('text-danger');
            $('#status_barang').removeClass('text-dark').addClass('text-danger');
        }else{
            $('#so_sebelumnya').removeClass('text-danger').addClass('text-dark');
            $('#status_barang').removeClass('text-danger').addClass('text-dark');
        }

        if(Status == 'TIDAK AKTIF'){
            $('#status_barang').removeClass('text-dark').addClass('text-danger');
        }
                            
        if(id_barang > 0){
            $('#jumlah_so').removeAttr('readonly');
            $('#jumlah_so').removeAttr('disabled');
            $('#catatan').removeAttr('disabled').val('');
            $('#masuk_stok').removeAttr('disabled').val('JUAL');
            $('#SimpanSO').removeAttr('disabled').val();
            $('#jumlah_so').focus();
        }
    });

    $(document).on('click', '#tombol_scan', function(e){
        AutoCompleteGue($('#pencarian_kode').val());
    });

    $(document).on('keyup', '#pencarian_kode', function(e){
        if($(this).val() !== '')
        {
            var charCode = e.which || e.keyCode;
            if(charCode == 40){
                if($('div#hasil_pencarian li.autocomplete_active').length > 0){
                    var Selanjutnya = $('div#hasil_pencarian li.autocomplete_active').next();
                    $('div#hasil_pencarian li.autocomplete_active').removeClass('autocomplete_active');

                    Selanjutnya.addClass('autocomplete_active');
                }else{
                    $('div#hasil_pencarian li:first').addClass('autocomplete_active');
                }
            }else if(charCode == 38){
                if($('div#hasil_pencarian li.autocomplete_active').length > 0){
                    var Sebelumnya = $('div#hasil_pencarian li.autocomplete_active').prev();
                    $('div#hasil_pencarian li.autocomplete_active').removeClass('autocomplete_active');
                
                    Sebelumnya.addClass('autocomplete_active');
                }else{
                    $('div#hasil_pencarian li:first').addClass('autocomplete_active');
                }
            }else if(charCode == 13){
                // Awal fungsi enter dan ambil data barang dengan ajax
                var SKU = $(this).val();
                $('#pencarian_kode').val(SKU);
                $('div#hasil_pencarian').hide();

                $.ajax({
                    url: "<?php echo site_url('stok_opname_pusat/ajax_cari_barang'); ?>",
                    type: "POST",
                    cache: false,
                    data: 'sku=' + SKU,
                    dataType:'json',
                    success: function(json){
                        if(json.status == 1){
                            $('#so_sebelumnya').removeClass('text-danger').addClass('text-dark');
                            $('#status_barang').removeClass('text-danger').addClass('text-dark');

                            $('#id_barang_pusat').html(json.data['id_barang_pusat']);
                            $('#pencarian_kode').val(json.data['sku']);

                            $('#nama_barang').html(json.data['nama_barang']);
                            $('#harga_satuan').html('Rp. ' + to_rupiah(json.data['harga_eceran']));
                            
                            if(json.data['total_stok'] > 0){
                                $('#so_sebelumnya').removeClass('text-dark').addClass('text-danger');
                                $('#status_barang').removeClass('text-dark').addClass('text-danger');
                            }

                            if(json.data['status'] == 'TIDAK AKTIF'){
                                $('#status_barang').removeClass('text-dark').addClass('text-danger');
                            }

                            $('#so_sebelumnya').html(json.data['jumlah_so_sebelumnya']);
                            $('#status_barang').html(json.data['status']);

                            $('div#hasil_pencarian').hide();
                            $('#jumlah_so').removeAttr('readonly').val();
                            $('#jumlah_so').removeAttr('disabled').val('');
                            $('#catatan').removeAttr('disabled').val('');
                            $('#masuk_stok').removeAttr('disabled').val('JUAL');
                            $('#SimpanSO').removeAttr('disabled').val();
                            $('#jumlah_so').focus();
                        }else if(json.status == 0){
                            $('#so_sebelumnya').removeClass('text-danger').addClass('text-dark').html('0');
                            $('#status_barang').removeClass('text-danger').addClass('text-dark').html('-');
                            $('#jumlah_so').prop('disabled', true).val('0');
                            $('#masuk_stok').prop('disabled', true).val('JUAL');

                            swal({
                                title: "Oops !",
                                text: json.pesan,
                                type: "error",
                                confirmButtonClass: 'btn-danger btn-md waves-effect waves-light',
                                confirmButtonText: "Ok"
                            },function (isConfirm){
                                $('#pencarian_kode').focus();
                            });
                        }
                    }
                });
            }else{
                // AutoCompleteGue($(this).val());
            }
        }else{
            // Bersihkan data pemilihan barang
            $('#id_barang_pusat').html('');
            $('#nama_barang').html('Belum ada barang yang dipilih');
            $('#harga_satuan').html('Rp. 0');

            $('#jumlah_so').prop('readonly', true).val('');
            $('#jumlah_so').prop('disabled', true).val('');
            $('#catatan').prop('disabled', true).val('');
            $('#so_sebelumnya').removeClass('text-dark').html('0');
            $('#status_barang').removeClass('text-dark').html('-');

            $('div#hasil_pencarian').hide();
        }
    });

    $(document).on('keydown', '#jumlah_so', function(e){
        var charCode = e.which || e.keyCode;
        if(charCode == 9){
            $('#catatan').focus();
            return false;   
        }
        
        if(charCode == 13){
            konfirmasi_simpan();
        }
    });

    function konfirmasi_simpan(){
        if($('#jumlah_so').val() == '0' || $('#jumlah_so').val() == ''){
            $('.modal-dialog').removeClass('modal-lg');
            $('.modal-dialog').addClass('modal-sm');
            $('#ModalHeader').html('Oops !');
            $('#ModalContent').html('<b class="text-danger">Harap masukan jumlah stok opname pusat !</b>');
            $('#ModalFooter').html("<button type='button' class='btn btn-primary' data-dismiss='modal' id='setuju' autofocus>Ok, saya mengerti</button>");
            $('#ModalGue').modal('show');

            setTimeout(function(){ 
                $('button#setuju').focus();
            }, 500);
        }else if($('#jumlah_so').val() !== ''){
            if($('#id_barang_pusat').html() !== ''){
                $('.modal-dialog').removeClass('modal-lg');
                $('.modal-dialog').addClass('modal-sm');
                $('#ModalHeader').html('Konfirmasi');
                $('#ModalContent').html(
                    "<b class='text-danger'>Apakah anda yakin ingin menyimpan stok opname pusat ini ? </b></br></br>" +
                    "SKU : </br>" + 
                    "<b class='text-dark'>" + $('#pencarian_kode').val() + "</b></br></br>" +

                    "Nama Barang : </br>" + 
                    "<b class='text-dark'>" + $('#nama_barang').html() + "</b></br></br>" +

                    "Qty SO : </br>" + 
                    "<b class='text-dark'>" + $('#jumlah_so').val() + "</b></br></br>" +

                    "Masuk Stok : </br>" + 
                    "<b class='text-dark'>" + $('#masuk_stok').val() + "</b>"
                );
                $('#ModalFooter').html("<button type='button' class='btn btn-primary' id='SimpanDataSO'>Ya, saya yakin</button><button type='button' class='btn btn-default' data-dismiss='modal'>Batal</button>");
                $('#ModalGue').modal('show');

                setTimeout(function(){ 
                    $('button#SimpanDataSO').focus();
                }, 500);
            
            }
        }else{
            $('.modal-dialog').removeClass('modal-lg');
            $('.modal-dialog').addClass('modal-sm');
            $('#ModalHeader').html('Oops !');
            $('#ModalContent').html('Harap masukan jumlah stok opname_pusat');
            $('#ModalFooter').html("<button type='button' class='btn btn-primary' data-dismiss='modal' autofocus>Ok</button>");
            $('#ModalGue').modal('show');
        } 
    }

    $(document).on('click', 'button#info_stok', function(){
        $('#ModalGue').modal('hide');
        $('#jumlah_so').focus();
    });

    $(document).on('keydown', 'body', function(e){
        var charCode = ( e.which ) ? e.which : event.keyCode;

        if(charCode == 118) //F7
        {
            konfirmasi_simpan();
        }

        if(charCode == 119) //F8
        {
            
        }

        if(charCode == 120) //F9
        {
            
        }

        if(charCode == 121) //F10
        {
            
        }
    });

    $(document).on('click', 'body', function(){
        $('div#hasil_pencarian').hide();
        $('div#hasil_pencarian_harga').hide();
    });

    $(document).on('click', 'button#SimpanSO', function(){
        konfirmasi_simpan();
    });

    $(document).on('click', 'button#Laporan', function(){
        window.location.href="laporan_stok_opname_pusat";
    });

    $(document).on('click', 'button#IyaCetak', function(){
        CetakStruk();
        window.location.href="stok_opname_pusat";
    });

    $(document).on('click', 'button#TidakCetak', function(){
        window.location.href="stok_opname_pusat";
    });

    $(document).on('click', 'button#SimpanDataSO', function(){
        $('#ModalGue').modal('hide');
        simpan_stok_opname_pusat();
    });

    $(document).on('click', 'button#CetakStruk', function(){
        CetakStruk();
        return false;
    });

    function simpan_stok_opname_pusat(){
        // var Indexnya = $(this).parent().parent().index();
        var FormData = "id_periode_stok_pusat="+$('#id_periode_stok_pusat').val(); 
            FormData += "&id_barang_pusat="+$('#id_barang_pusat').html();
            FormData += "&jumlah_so="+$('#jumlah_so').val();
            FormData += "&masuk_stok="+$('#masuk_stok').val();
            FormData += "&catatan="+encodeURI($('#catatan').val());
            FormData += "&tanggal="+encodeURI($('#tanggal').val());

        $.ajax({
            url: "<?php echo site_url('stok_opname_pusat/ajax_simpan'); ?>",
            type: "POST",
            cache: false,
            data: FormData,
            dataType:'json',
            success: function(data){
                if(data.status == 1){   
                    // Bersihkan data pemilihan barang
                    $('#id_barang_pusat').html('');
                    $('#pencarian_kode').val('');
                    $('#nama_barang').html('Belum ada barang yang dipilih');
                    $('#harga_satuan').html('Rp. 0');
                    $('#so_sebelumnya').html('0').removeClass('text-danger');
                    $('#so_sebelumnya').html('0').addClass('text-dark');
                    $('#status_barang').removeClass('text-danger');
                    $('#status_barang').html('-').addClass('text-dark');
                    $('#jumlah_so').val('');
                    $('#jumlah_so').prop('disabled', true);
                    $('#catatan').val('');
                    $('#masuk_stok').prop('disabled', true).val('-');
                    $('#catatan').prop('disabled', true);
                    $('#SimpanSO').prop('disabled', true);

                    reload_table_so();
                    $('#pencarian_kode').focus();

                }else if(data.status == 0){
                    // Tampilkan pesan kesalahan
                    $('.modal-dialog').removeClass('modal-lg');
                    $('.modal-dialog').addClass('modal-sm');
                    $('#ModalHeaderPesan').html('Oops !');
                    $('#ModalContentPesan').html(data.pesan);
                    $('#ModalFooterPesan').html("<button type='button' class='btn btn-primary' data-dismiss='modal' autofocus>Ok, Saya Mengerti</button>");
                    $('#ModalPesan').modal('show');

                }
            }
        });
    }

    function edit_stok_opname_pusat(id_stok_opname_pusat)
    {
        $.ajax({
            url : "<?php echo site_url('stok_opname_pusat/ajax_edit'); ?>",
            type: "POST",
            cache: false,
            data: 'id_stok_opname_pusat=' + id_stok_opname_pusat,
            dataType: "JSON",
            success: function(data){
                var Indexnya = $(this).parent().parent().parent().parent().index();
                $('#id_barang_pusat').html(data.id_barang_pusat);
                $('#pencarian_kode').val(data.sku);
                $('#nama_barang').html(data.nama_barang);
                $('#harga_satuan').html(to_rupiah(data.harga_eceran));
                $('#catatan').val(data.catatan);
                $('#so_sebelumnya').html(data.jumlah_so).removeClass('text-dark').addClass('text-danger');
                $('#status_barang').html(data.status).removeClass('text-dark').addClass('text-danger');
                $('#masuk_stok').prop('disabled', true).val(data.masuk_stok);
                $('#jumlah_so').val('').focus();
                $('#jumlah_so').removeAttr('disabled').val();
                $('#SimpanSO').removeAttr('disabled').val();
                $('#catatan').removeAttr('disabled').val();
            },
            error: function (jqXHR, textStatus, errorThrown){
                alert('Error get data from ajax');
                window.location.href="stok_opname_pusat";
            }
        });
    }

    function verifikasi_hapus(id_stok_opname_pusat)
    {
        $.ajax({
            url : "<?php echo site_url('stok_opname_pusat/ajax_verifikasi_hapus'); ?>",
            type: "POST",
            cache: false,
            data: 'id_stok_opname_pusat=' + id_stok_opname_pusat,
            dataType: "JSON",
            success: function(data){
                $('.modal-dialog').removeClass('modal-lg');
                $('.modal-dialog').addClass('modal-sm');
                $('#ModalHeader').html('Informasi Stok Opname');
                $('#ModalContent').html(data.pesan);
                $('#ModalFooter').html(data.footer);
                $('#ModalGue').modal('show');
            },
            error: function (jqXHR, textStatus, errorThrown){
                alert('Error get data from ajax');
                window.location.href="stok_opname_pusat";
            }
        });
    }

    function hapus_so(id_stok_opname_pusat)
    {
        var FormData = "tanggal=" + encodeURI($('#tanggal').val());
            FormData += "&id_stok_opname_pusat=" + id_stok_opname_pusat; 
            FormData += "&keterangan_batal=" + encodeURI($('#keterangan_batal').val());

        $.ajax({
            url : "<?php echo site_url('stok_opname_pusat/ajax_hapus'); ?>",
            type: "POST",
            data: FormData,
            dataType: "JSON",
            success: function(data){
                if(data.status == 1){
                    reload_table_so();

                }else if(data.status == 0){
                    $('.modal-dialog').removeClass('modal-lg');
                    $('.modal-dialog').addClass('modal-sm');
                    $('#ModalHeader').html('Informasi Stok Opname');
                    $('#ModalContent').html(data.pesan);
                    $('#ModalFooter').html("<button type='button' class='btn btn-primary' data-dismiss='modal' autofocus>Ok, Saya Mengerti</button>");
                    $('#ModalGue').modal('show');
                    reload_table_so();
                }
               
            }
        });
    }

    function AutoCompleteGue(KataKunci)
    {
        $('#induk_pencarian_barang').removeClass('has-warning');
        $('#induk_pencarian_barang').addClass('has-has-succes');

        $('#icon_barang').removeClass('fa-search');
        $('#icon_barang').addClass('fa-spin fa-refresh');

        $('#tombol_scan').removeClass('btn-warning');
        $('#tombol_scan').addClass('btn-primary');

        $('#jumlah_so').prop('readonly', true).val();
        $('#so_sebelumnya').html('0').removeClass('text-danger');
        $('#so_sebelumnya').html('0').addClass('text-dark');
        $('#status_barang').removeClass('text-danger');
        $('#status_barang').html('-').addClass('text-dark');
        $('div#hasil_pencarian').hide();

        delay(function () {
            $.ajax({
                url: "<?php echo site_url('stok_opname_pusat/ajax_kode'); ?>",
                type: "POST",
                cache: false,
                data: 'keyword=' + KataKunci,
                dataType:'json',
                success: function(json){
                    if(json.status == 1){
                        $('#icon_barang').removeClass('fa-spin fa-refresh');
                        $('#icon_barang').addClass('fa-search');

                        $('#id_barang_pusat').html('');
                        $('#nama_barang').html('Belum ada barang yang dipilih');
                        $('#harga_satuan').html('Rp. 0');
                        $('#jumlah_so').prop('disabled', true).val('');
                        $('#masuk_stok').prop('disabled', true).val('JUAL');
                        $('#catatan').prop('disabled', true).val('');
                        $('#SimpanSO').prop('disabled', true).val();

                        $('div#hasil_pencarian').show('fast');
                        $('div#hasil_pencarian').html(json.datanya);

                    }else{
                        $('#icon_barang').removeClass('fa-spin fa-refresh');
                        $('#icon_barang').addClass('fa-search');

                        $('#induk_pencarian_barang').removeClass('has-succes');
                        $('#induk_pencarian_barang').addClass('has-warning');

                        $('#tombol_scan').removeClass('btn-primary');
                        $('#tombol_scan').addClass('btn-warning');

                    }               
                }
            });
        }, 500);
    }

    function to_rupiah(angka){
        var rev     = parseInt(angka, 10).toString().split('').reverse().join('');
        var rev2    = '';
        for(var i = 0; i < rev.length; i++){
            rev2  += rev[i];
            if((i + 1) % 3 === 0 && i !== (rev.length - 1)){
                rev2 += '.';
            }
        }
        return rev2.split('').reverse().join('');

        // Pakai Simbol Rp.
        // return 'Rp. ' + rev2.split('').reverse().join('');
    }

    function to_angka(rp)
    {
        return parseInt(rp.replace(/,.*|\D/g,''),10)
    }

    function check_int(evt) {
        var charCode = ( evt.which ) ? evt.which : event.keyCode;
        return ( charCode >= 48 && charCode <= 57 || charCode == 8 );
    }

</script>
<!-- Akhir Script CRUD -->

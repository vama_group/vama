<?php
	if($access_create == '1'){
		$readonly	= '';
		$disabled	= '';
	}else{		
		$readonly	= 'readonly';
		$disabled	= 'disabled';
	}
?>

<div class="content">
	<div class="container">
	<br>		
		<div class="row">
        	<div class="col-sm-12">
	        	<div class="row">
	        		<!-- Awal total harga -->
					<div class='col-sm-4'>
						<div class="panel panel-color panel-info">
							<!-- Default panel contents -->
							<div class="panel-heading">
								<h3 class="panel-title">Stok Opname Pusat
									<span class="pull-right" name='kode_periode' id='kode_periode'>
										PERIODE : <b><?php echo $kode_periode_stok_pusat ?></b>
									</span>
								</h3>
							</div>

							<span style="display: none;">
							<input type='hidden' id="id_periode_stok_pusat" name="id_periode_stok_pusat" value="<?php echo $id_periode_stok_pusat ?>">
							<input type='hidden' name='tanggal' class='form-control input-sm' id='tanggal' value="<?php echo date('d-m-Y'); ?>" <?php echo $disabled; ?>>
							</span>

							<table class='table table-condensed table-striped table-hover' id='Total'>
								<tbody>
									<tr>
										<td>
											<small>Tangggal Periode Awal</small><br/>
											<b>
												<span class="text-dark" id="tanggal_awal" name="tanggal_awal">
													<?php echo $tanggal_periode_awal ?>	
												</span>
											</b>
										</td>
										<td>
											<small>Tangggal Periode Akhir</small><br/>
											<b>
												<span class="text-dark" id="tanggal_akhir" name="tanggal_akhir">
													<?php echo $tanggal_periode_akhir ?>	
												</span>
											</b>
										</td>
									</tr>

									<!-- Awal scan barang dan pilih Barang -->
									<tr class="text-dark">
		                    			<td colspan="2">
                                        	<div id="induk_pencarian_barang" class="input-group col-md-12 table-responsive">
			                                    <input id='pencarian_kode' name="pencarian_kode" placeholder="SKU / Nama Barang" class="form-control input-md text-dark" type="text" <?php echo $disabled ?> autocomplete="off">
			                                    <span id="id_barang_pusat" name="id_barang_pusat" style="display: none;"></span>
			                                    
			                                    <span class="input-group-btn">
			                                        <button id="tombol_scan" name="tombol_scan" type="button" class="btn waves-effect waves-light btn-md btn-primary" <?php echo $disabled ?>>
			                                        <i id="icon_barang" class="fa fa-search"></i></button>
			                                    </span>
			                                </div>
			                                <div id='hasil_pencarian' name="hasil_pencarian" class="alert"></div>
		                    			</td>
		                    		</tr>
		                    		<!-- Akhir scan barang dan pilih Barang -->

									<tr>
										<td colspan="2">
											<small>Nama Barang</small> <br/>
											<b><span class="text-dark" id="nama_barang" name="nama_barang">Belum ada barang yg dipilih</span></b>
										</td>
									</tr>

									<tr>
										<td><small>Harga Eceran</td>
										<td>
											<b><span id="harga_satuan" name="harga_satuan" class="input-md text-dark" type="text">Rp. 0</span></b>
										</td>
									</tr>

									<tr>
										<td>
											<small>SO Sebelumnya</small><br>
											<b><span id="so_sebelumnya" name="so_sebelumnya" class="input-md text-dark" type="text">0</span></b>
										</td>
										<td>
											<small>Status</small><br>
											<b><span id="status_barang" name="status_barang" class="input-md text-dark" type="text">-</span></b>
										</td>
									</tr>

									<tr>
										<td><small>Qty SO</small></td>
										<td class="text-right">
											<input type='number' class='form-control input-sm' id='jumlah_so' name='jumlah_so' value="0" disabled>
										</td>
									</tr>

									<tr>
										<td><small>Masuk Stok</small></td>
		                    			<td>
		                    				<select id='masuk_stok' name='masuk_stok' class='form-control input-sm' disabled>
		                    					<option value="JUAL">JUAL</option>
		                    					<option value="RUSAK">RUSAK</option>
		                    				</select>
		                    			</td>
	                    			</tr>

									<tr>
										<td colspan="2">							
											<textarea name='catatan' id='catatan' class='form-control input-sm' rows='2' placeholder="Catatan SO (Jika Ada)" style='resize: vertical; width:100%;' disabled></textarea>
										</td>
									</tr>

									<tr>
										<td>
											<button id='SimpanSO' type='button' class='btn btn-info' title="Simpan Transaksi" disabled>
												<i class='fa fa-paper-plane-o'></i>
											</button>

											<?php if($access_laporan == 1){ ?>
												<button id='Laporan' type='button' class='btn btn-default' title="Laporan Transaksi">
													<i class='fa fa-file-text-o'></i>
												</button>
											<?php } ?>									
										</td>
										<td></td>
									</tr>

									<tr>
										<td colspan="2">
											<p><i class='fa fa-keyboard-o fa-fw'></i> <b><small>Shortcut Keyboard : </small></b></p>
											<div class='row'>
												<div class='col-sm-6'><small>F7 = Simpan SO</small></div>
												<div class='col-sm-6'><small>F8 = Referesh Data SO</small></div>
												<div class='col-sm-6'><small>F9 = Referesh Data Riwayat SO</small></div>
												<!-- <div class='col-sm-6'><small>F10 = </small></div> -->
											</div> 
										</td>
									</tr>	
								</tbody>
							</table>
						</div>
					</div>
					<!-- Akhir total harga --> 
					
	        		<!-- Awal pilih barang dan daftar transaksi -->
		            <div class="col-sm-8">
			        	<!-- Awal daftar transaksi dan catatan transaksi --> 
		            	<div class="card-box table-responsive">
		            		<!-- Induk Tab -->
		                    <ul class="nav nav-tabs tabs-bordered">
		                        <li class="active">
		                            <a id="tab_so" href="#tab_stok_opname_pusat" data-toggle="tab" aria-expanded="false">
		                                <span class="visible-xs"><i class="fa fa-check-square"></i></span>
		                                <span class="hidden-xs">Stok Opname Pusat</span>
		                            </a>
		                        </li>
		                        <li class="">
		                            <a id="tab_riwayat_so" href="#tab_riwayat_stok_opname_pusat" data-toggle="tab" aria-expanded="true">
		                                <span class="visible-xs"><i class="fa fa-recycle"></i></span>
		                                <span class="hidden-xs">Riwayat Stok Opname Pusat</span>
		                            </a>
		                        </li>
		                        <li class="">
		                            <a id="tab_batal_so" href="#tab_batal_stok_opname_pusat" data-toggle="tab" aria-expanded="true">
		                                <span class="visible-xs"><i class="fa fa-recycle"></i></span>
		                                <span class="hidden-xs">Stok Opname Pusat Batal</span>
		                            </a>
		                        </li>
		                    </ul>

		                    <div class="tab-content">
		                    	<!-- Awal data stok opname -->
                        		<div class="tab-pane active" id="tab_stok_opname_pusat">
									<!-- Daftar transaksi -->
									<table id='TableTransaksi' class="table table-condensed table-striped table-hover dt-responsive nowrap" cellspacing="0" width="100%">
										<thead class="input-sm">
											<tr class="text-dark">
												<th>#</th>
												<th>Tombol</th>
												<!-- <th>Foto</th> -->
												<th>SKU</th>
												<th>Nama Barang</th>
												<th>Harga</th>
												<th>QTY SB</th>
												<th>QTY SO</th>
												<th>STOK</th>
												<th>Catatan</th>
												<th>Pegawai Save</th>
												<th>Tanggal Save</th>
												<th>Pegawai Edit</th>
												<th>Tanggal Edit</th>
											</tr>
										</thead>

										<tbody class="input-sm text-dark"></tbody>
									</table>
								</div>
								<!-- Akhir data stok opname -->

								<!-- Awal data riwayat stok opname -->
								<div class="tab-pane" id="tab_riwayat_stok_opname_pusat">
		                            <table id="table_riwayat_stok_opname_pusat" class="table table-condensed table-striped table-hover dt-responsive nowrap" cellspacing="0" width="100%">
		                                <thead class="input-sm">
		                                    <tr>
		                                    	<th>#</th>
		                                        <th>SKU</th>
		                                        <th style='width: 1350px;'>NAMA BARANG</th>
		                                        <th>QTY SB</th>
		                                        <th>QTY SO</th>
		                                        <th>STOK</th>
		                                        <th>CATATAN</th>
		                                        <th>PEGAWAI</th>
		                                        <th>TANGGAL</th>
		                                        <!-- <th style="width:125px;">Action</th> -->
		                                    </tr>
		                                </thead>

		                                <tbody class="input-sm">
		                                </tbody>
		                            </table>
		                        </div>
		                        <!-- Akhir data riwayat stok opname -->

		                        <!-- Awal data batal stok opname -->
								<div class="tab-pane" id="tab_batal_stok_opname_pusat">
		                            <table id="table_batal_stok_opname_pusat" class="table table-condensed table-striped table-hover dt-responsive nowrap" cellspacing="0" width="100%">
		                                <thead class="input-sm">
		                                    <tr>
		                                    	<th>#</th>
		                                        <th>SKU</th>
		                                        <th>NAMA BARANG</th>
		                                        <th>QTY SO</th>
		                                        <th>STOK</th>
		                                        <th>KETERANGAN</th>
		                                        <th>Pegawai Batal</th>
		                                        <th>Tanggal Batal</th>
		                                        <th>Pegawai Save</th>
		                                        <th>Tanggal Save</th>
		                                        <th>Pegawai Edit</th>
		                                        <th>Tanggal Edit</th>
		                                    </tr>
		                                </thead>

		                                <tbody class="input-sm">
		                                </tbody>
		                            </table>
		                        </div>
		                        <!-- Akhir data batal stok opname -->
							</div>
						</div>
				        <!-- Akhir daftar transaksi -->
		            </div>
		            <!-- Akhir pilih barang dan daftar transaksi -->	
				</div>
            </div>
        </div>
	</div>
</div>

<div class="modal" id="ModalGue" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class='fa fa-times-circle'></i></button>
				<h4 class="modal-title" id="ModalHeader"></h4>
			</div>
			<div class="modal-body" id="ModalContent"></div>
			<div class="modal-footer" id="ModalFooter"></div>
		</div>
	</div>
</div>

<div class="modal" id="ModalPesan" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class='fa fa-times-circle'></i></button>
				<h4 class="modal-title" id="ModalHeaderPesan"></h4>
			</div>
			<div class="modal-body" id="ModalContentPesan"></div>
			<div class="modal-footer" id="ModalFooterPesan"></div>
		</div>
	</div>
</div>

<script>
	$('#ModalGue').on('hide.bs.modal', function () {
	   setTimeout(function(){ 
	        $('#ModalHeader, #ModalContent, #ModalFooter').html('');
	   }, 500);
	});

	$('#ModalPesan').on('hide.bs.modal', function () {
	   setTimeout(function(){ 
	        $('#ModalHeaderPesan, #ModalContentPesan, #ModalFooterPesan').html('');
	   }, 500);
	});
</script>

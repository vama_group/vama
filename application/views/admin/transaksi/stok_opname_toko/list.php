<!-- Start content -->
<div class="content">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="page-title-box">
                    <h4 class="page-title">Stok Opname Toko</h4>
                    <?php 
                        $uri1   = $this->uri->segment(1);        
                        $uri2   = $this->uri->segment(2);
                        $uri3   = $this->uri->segment(3);
                    ?>                    

                    <ol class="breadcrumb p-0 m-0">
                        <li>
                            <a href="<?php echo base_url('')?>">Dashboard</a>
                        </li>
                        <li>
                            <a href="<?php echo base_url($uri1) ?>"><?php echo ucfirst($uri1) ?></a>
                        </li>
                        <?php if ($uri2 !="") { ?>
                        <li class="active">
                            <?php echo ucfirst($uri2) ?>
                        </li>
                        <?php } ?>
                    </ol>

                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
        <!-- end row -->
        
        <div class="row">
            <div class="col-sm-12">
                <div class="card-box table-responsive">

                    <!-- Induk Tab -->
                    <ul class="nav nav-tabs tabs-bordered">
                        <li class="active">
                            <a href="#tab_stok_opname_toko" data-toggle="tab" aria-expanded="false">
                                <span class="visible-xs"><i class="fa fa-check-square"></i></span>
                                <span class="hidden-xs">Stok Opname Toko</span>
                            </a>
                        </li>
                        <li class="">
                            <a href="#tab_riwayat_stok_opname_toko" data-toggle="tab" aria-expanded="true">
                                <span class="visible-xs"><i class="fa fa-recycle"></i></span>
                                <span class="hidden-xs">Riwayat Stok Opname Toko</span>
                            </a>
                        </li>
                    </ul>

                    <!-- Isi Tab -->
                    <div class="tab-content">
                        <div class="tab-pane active" id="tab_stok_opname_toko">
                            <div class="row">
                                <div class="col-xs-6 col-md-6">
                                    <button class="btn btn-sm btn-success" onclick="add_stok_opname_pusat()"><i class="glyphicon glyphicon-plus"></i> TAMBAH</button>
                                    <button class="btn btn-sm btn-primary" onclick="reload_table()"><i class="glyphicon glyphicon-refresh"></i> REFRESH</button>

                                </div>
                                <div class="col-xs-6 col-md-6">
                                </div>
                            </div>
                            
                            <table id="table_stok_opname_toko" class="table table-striped table-hover table-colored table-primary dt-responsive nowrap" cellspacing="0" width="100%">
                                <thead class="input-sm">
                                    <tr>
                                        <th>FOTO</th>
                                        <th>SKU</th>
                                        <th>NAMA BARANG</th>
                                        <th>QTY</th>
                                        <th>TANGGAL PEMBUATAN</th>
                                        <th>TANGGAL PEMBAHARUAN</th>
                                        <th style="width:125px;">Action</th>
                                    </tr>
                                </thead>

                                <tbody class="input-sm">
                                </tbody>

                                <tfoot class="input-sm">
                                    <tr>
                                        <th>FOTO</th>
                                        <th>SKU</th>
                                        <th>NAMA BARANG</th>
                                        <th>QTY</th>
                                        <th>TANGGAL PEMBUATAN</th>
                                        <th>TANGGAL PEMBAHARUAN</th>
                                        <th>Action</th>
                                    </tr>
                                </tfoot>
                            </table>                    
                        </div>

                        <div class="tab-pane" id="tab_riwayat_stok_opname_toko">
                            <div class="row">
                                <div class="col-xs-6 col-md-6">
                                    <button class="btn btn-sm btn-primary" onclick="reload_table_riwayat()"><i class="glyphicon glyphicon-refresh"></i> REFRESH</button>
                                </div>
                                <div class="col-xs-6 col-md-6">
                                </div>
                            </div>
                            
                            <table id="table_riwayat_stok_opname_toko" class="table table-striped table-hover table-colored table-primary dt-responsive nowrap" cellspacing="0" width="100%">
                                <thead class="input-sm">
                                    <tr>
                                        <th>FOTO</th>
                                        <th>SKU</th>
                                        <th>NAMA BARANG</th>
                                        <th>QTY SEBELUMNYA</th>
                                        <th>QTY</th>
                                        <th>TANGGAL PEMBUATAN</th>
                                        <th>TANGGAL PEMBAHARUAN</th>
                                        <!-- <th style="width:125px;">Action</th> -->
                                    </tr>
                                </thead>

                                <tbody class="input-sm">
                                </tbody>

                                <tfoot class="input-sm">
                                    <tr>
                                        <th>FOTO</th>
                                        <th>SKU</th>
                                        <th>NAMA BARANG</th>
                                        <th>QTY SEBELUMNYA</th>                                        
                                        <th>QTY</th>
                                        <th>TANGGAL PEMBUATAN</th>
                                        <th>TANGGAL PEMBAHARUAN</th>
                                        <!-- <th>Action</th> -->
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div> <!-- container -->
</div> 
<!-- content
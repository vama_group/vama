<!-- Bootstrap modal -->
<div class="modal fade" id="modal_form" role="dialog">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h3 class="modal-title">Stok Opname Toko Form</h3>
            </div>
            <div class="modal-body form">
                <form action="#" id="form" class="form-horizontal">
                    <input type="hidden" value="" name="id_stok_opname_toko"/>
                    <input type="hidden" value="" name="qty_sebelumnya"/>

                    <div class="form-group">
                        <label class="control-label col-md-3">Barang</label>
                        <div class="col-md-9">
                            <select name="kode_barang" class="form-control select2">
                                <?php foreach ($list_barang_pusat as $barang_pusat) { ?>
                                <option value="<?php echo $barang_pusat->kode_barang ?>">
                                <?php echo $barang_pusat->sku ?> - <?php echo $barang_pusat->nama_barang ?>
                                </option>
                                <?php } ?>
                            </select>
                            <span class="help-block"></span>
                        </div>
                    </div>

                    <div class="form-body">
                        <div class="form-group">
                            <label class="control-label col-md-3">Qty</label>
                            <div class="col-md-9">
                                <input name="qty" placeholder="Qty" class="form-control" type="number">
                                <span class="help-block"></span>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" id="btnSave" onclick="save()" class="btn btn-primary">Save</button>
                <button type="button" class="btn btn-pink" data-dismiss="modal">Cancel</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- End Bootstrap modal -->


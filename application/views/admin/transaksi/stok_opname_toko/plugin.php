<script src="assets/plugin/zircos/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="assets/plugin/zircos/plugins/datatables/dataTables.bootstrap.js"></script>

<script src="assets/plugin/zircos/plugins/datatables/dataTables.responsive.min.js"></script>
<script src="assets/plugin/zircos/plugins/datatables/responsive.bootstrap.min.js"></script>
<script src="assets/plugin/zircos/material-design/assets/pages/jquery.datatables.init.js"></script>

<!-- Awal Script CRUD -->
<script type="text/javascript">
$('#JudulHalaman').html('Stok Opname Toko - VAMA');
var save_method; //for save method string
var table;

$(document).ready(function() {
    //datatables stok opname
    table_stok_opname_toko = $('#table_stok_opname_toko').DataTable({ 
        processing: true,
        serverSide: true,
        order: [],

        ajax: {
            url: "<?php echo site_url('stok_opname_toko/ajax_list')?>",
            type: "POST"
        },

        columnDefs: [
            { 
                targets: [ -1 ],
                orderable: false,
            },
        ],
    });

    //datatables riwayat stok opname
    table_riwayat_stok_opname_toko = $('#table_riwayat_stok_opname_toko').DataTable({ 
        processing: true,
        serverSide: true,
        order: [],

        ajax: {
            url: "<?php echo site_url('stok_opname_toko/ajax_list_riwayat')?>",
            type: "POST"
        },

        "columnDefs": [
            { 
                "targets": [ -1 ],
                "orderable": false,
            },
        ],
    });    

    $("input").change(function(){
        $(this).parent().parent().removeClass('has-error');
        $(this).next().empty();
    });
    $("textarea").change(function(){
        $(this).parent().parent().removeClass('has-error');
        $(this).next().empty();
    });
});

function add_stok_opname_pusat()
{
    save_method = 'add';
    $('#form')[0].reset();
    $('.form-group').removeClass('has-error');
    $('.help-block').empty();
    $('#modal_form').modal('show');
    $('.modal-title').text('Tambah Stok Opname Toko');
}

function edit_stok_opname_pusat(id_stok_opname_toko)
{
    save_method = 'update';
    $('#form')[0].reset();
    $('.form-group').removeClass('has-error');
    $('.help-block').empty();

    $.ajax({
        url : "<?php echo site_url('stok_opname_toko/ajax_edit/')?>/" + id_stok_opname_toko,
        type: "GET",
        dataType: "JSON",
        success: function(data){
            $('[name="id_stok_opname_toko"]').val(data.id_stok_opname_toko);
            $('[name="kode_barang"]').val(data.kode_barang);            
            $('[name="qty_sebelumnya"]').val(data.qty);
            $('[name="qty"]').val(data.qty);
            $('#modal_form').modal('show');
            $('.modal-title').text('Edit Stok Opname Toko');
        },
        error: function (jqXHR, textStatus, errorThrown){
            alert('Error get data from ajax');
        }
    });
}

function reload_table()
{
    table_stok_opname_toko.ajax.reload(null,false);
}

function reload_table_riwayat()
{
    table_riwayat_stok_opname_toko.ajax.reload(null,false);
}

function save()
{
    $('#btnSave').text('saving...');
    $('#btnSave').attr('disabled',true); 
    var url;

    if(save_method == 'add'){
        url = "<?php echo site_url('stok_opname_toko/ajax_add')?>";
    } else {
        url = "<?php echo site_url('stok_opname_toko/ajax_update')?>";
    }

    $.ajax({
        url : url,
        type: "POST",
        data: $('#form').serialize(),
        dataType: "JSON",
        success: function(data){

            if(data.status){
                $('#modal_form').modal('hide');
                reload_table();
                reload_table_riwayat();
            }else{
                for (var i = 0; i < data.inputerror.length; i++){
                    $('[name="'+data.inputerror[i]+'"]').parent().parent().addClass('has-error'); 
                    $('[name="'+data.inputerror[i]+'"]').next().text(data.error_string[i]); 
                }
            }
            $('#btnSave').text('save');
            $('#btnSave').attr('disabled',false);
        },
        error: function (jqXHR, textStatus, errorThrown){
            alert('Error adding / update data');
            $('#btnSave').text('save');
            $('#btnSave').attr('disabled',false); 
        }
    });
}

function delete_stok_opname_pusat(id_stok_opname_toko)
{
    if(confirm('Are you sure delete this data?')){
        $.ajax({
            url : "<?php echo site_url('stok_opname_toko/ajax_delete')?>/"+id_stok_opname_toko,
            type: "POST",
            dataType: "JSON",
            success: function(data){
                $('#modal_form').modal('hide');
                reload_table();
            },
            error: function (jqXHR, textStatus, errorThrown){
                alert('Error deleting data');
            }
        });
    }
}

</script>
<!-- Akhir Script CRUD -->

<!-- Select2 -->
<script src="assets/plugin/zircos/plugins/multiselect/js/jquery.multi-select.js" type="text/javascript"></script>
<script src="assets/plugin/zircos/plugins/jquery-quicksearch/jquery.quicksearch.js" type="text/javascript"></script>
<script src="assets/plugin/zircos/plugins/select2/js/select2.min.js" type="text/javascript"></script>
<script src="assets/plugin/zircos/material-design/assets/pages/jquery.form-advanced.init.js" type="text/javascript"></script>
<!-- Select2 -->
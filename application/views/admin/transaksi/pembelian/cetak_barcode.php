<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Cetak Barcode</title>
	<link rel="stylesheet" href="">

	<style type="text/css">
		body {
		  padding: 0;
		  margin: 0;
		}
		.kotak {
		  margin: 0;
		  padding: 0.5cm;
		}
		table, td, tr {
		  text-align: center;
		  vertical-align: middle;
		}
	</style>
</head>
	
<body>
	<div class="kotak">
	<table>
	    <tr>
	      <td width="33.333333333%">
	        <barcode code="<?php echo $data->sku ?>" type="C128A" height="0.5" text="2" />
	      </td>
	      <td width="33.333333333%">
	        <barcode code="<?php echo $data->sku ?>" type="C128A" height="0.5" text="2" />
	      </td>
	      <td width="33.333333333%">
	        <barcode code="<?php echo $data->sku ?>" type="C128A" height="0.5" text="2" />
	      </td>
	    </tr>
	</table>

	</div>

	<?php  echo '<pagebreak />';  ?>
	<div class="kotak">
	<table>
	    <tr>
	      <td width="33.333333333%">
	        <barcode code="<?php echo $data->sku ?>" type="C128A" height="0.5" text="2" />
	      </td>
	      <td width="33.333333333%">
	        <barcode code="<?php echo $data->sku ?>" type="C128A" height="0.5" text="2" />
	      </td>
	      <td width="33.333333333%">
	        <barcode code="<?php echo $data->sku ?>" type="C128A" height="0.5" text="2" />
	      </td>
	    </tr>
	</table>
	</div>
</body>
</html>
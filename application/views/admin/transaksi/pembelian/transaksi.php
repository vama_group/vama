<?php
	if($access_create == '1'){
		$readonly	= '';
		$disabled	= '';
	}else{		
		$readonly	= 'readonly';
		$disabled	= 'disabled';
	}
?>

<div class="content">
	<div class="container">
		<br>		
		<div class="row">
        	<div class="col-sm-12">
	        	<div class="row">
	        		<!-- Awal total harga -->
					<div class='col-sm-4'>
						<div class="panel panel-color panel-primary">
							<!-- Default panel contents -->
							<div class="panel-heading">
								<h3 class="panel-title">No. Pembelian
									<span class="pull-right" name='no_pembelian' id='no_pembelian'>
										<b>-</b>
									</span>
								</h3>
							</div>

							<span style="display: none;">
								<input type='hidden' id="id_pembelian_m" name="id_pembelian_m" value="<?php echo $id_pembelian_m ?>">
								<input type='hidden' name='tanggal' class='form-control input-sm' id='tanggal' value="<?php echo date('d-m-Y'); ?>" <?php echo $disabled; ?>>
							</span>

							<table class='table table-condensed table-striped table-hover' id='Total'>
								<tbody>
									<tr>
										<td colspan="2">
											<div class="has-success has-feedback">
		                                        <span class="fa fa-hashtag form-control-feedback"></span>
		                                        <input id="no_faktur_supplier" name="no_faktur_supplier" type="text" class="form-control input-md text-dark" placeholder="Masukan no. faktur supplier" autocomplete="off">
		                                    </div>
										</td>
									</tr>
									<tr>
										<td colspan="2">
											<div id="induk_pencarian_supplier" class="input-group col-md-12 table-responsive">
												<select id="pencarian_supplier" name="pencarian_supplier" class="form-control select2"></select>
                                                <!-- <input id='pencarian_supplier' name="pencarian_supplier" placeholder="Masukan nama supplier" class="form-control text-dark" type="text">
                                                <span id="id_supplier_hidden" name="id_supplier_hidden" style="display:none;"></span> -->

                                                <!-- <span class="input-group-btn">
                                                    <button id="tombol_cari_supplier" name="tombol_cari_supplier" type="button" class="btn waves-effect waves-light btn-md btn-primary">
                                                    <i id="icon_pencarian_supplier" class="fa fa-search"></i></button>
                                                </span> -->
                                            </div>
                                            <div id='hasil_pencarian_supplier' name="hasil_pencarian_supplier"></div>
										</td>
									</tr>
									<tr>
										<td>
											<small>Kode Supplier</small> <br/>
											<b><span id="data_kode_supplier" name="data_kode_supplier" class="input-md text-dark" type="text" readonly>-</span></b>
										</td>
										<td>
											<small>Tipe Supplier</small> <br/>
											<b><span id="data_tipe_supplier" name="data_tipe_supplier" class="input-md text-dark" type="text">-</span></b>
										</td>
									</tr>
									<tr>
										<td><small>Jml Barang</small></td>
										<td class="text-right">
											<span id='jumlah_barang' name='jumlah_barang' class="text-dark">0</span>
										</td>
									</tr>

									<?php if($this->session->userdata('usergroup_name') == "Super Admin"){ ?>		
										<tr>
											<td><small>Total</small></td>
											<td class="text-left">Rp. 
												<span id='TotalBayar' name="TotalBayar" class="text-dark pull-right">0</span>
												<input type="hidden" id='TotalBayarHidden' name='TotalBayarHidden'>
											</td>
										</tr>
										<tr>
											<td><small>PPN 10%</small></td>
											<td class="text-left">Rp.
												<input type='hidden' name='PersentasePpn' id='PersentasePpn' class='form-control input-md' value='10' onkeypress='return check_int(event)'>
												<span id='TotalPpn' class="text-dark pull-right">0</span>
												<input type="hidden" id='TotalPpnHidden' name='TotalPpnHidden'>
											</td>
										</tr>
										<tr>
											<td><small>Biaya Lain</small></td>
											<td class="text-left">
												<div class="row">
													<div class="col-md-2">Rp.</div>
													<div class="col-md-10">
														<input type='text' name='BiayaLain' id='BiayaLain' class='form-control input-md text-right text-dark'  onkeypress='return check_int(event)' value='0'>
														<input type="hidden" id='BiayaLainHidden' name='BiayaLainHidden' value='0'>
													</div>
											</td>
										</tr>
										<tr>
											<td><small>Grandotal</small></td>
											<td class="text-left">
												<h4 class="text-primary">Rp. <span id='GrandTotal' class="pull-right">0</span></h4	>
												<input type="hidden" id='GrandTotalHidden' name='GrandTotalHidden' value="0">
											</td>
										</tr>
									<?php } ?>
									<tr>
										<td colspan="2">
											<textarea id='catatan' name='catatan' class='form-control input-sm' rows='2' placeholder="Catatan Transaksi (Jika Ada)" style='resize: vertical; width:100%;'></textarea>
										</td>
									</tr>
									<tr>
										<td></td>
										<td>
											<?php if($access_create == 1 or $access_update == 1){ ?>
												<button type='button' class='btn btn-primary' id='Simpan'>
													<i class='fa fa-paper-plane-o'></i>
												</button>
											<?php } ?>

											<?php if($access_create == 1 or $access_update == 1){ ?>
												<button type='button' class='btn btn-danger' id='Tahan'>
													<i class='fa fa-hand-paper-o'></i>
												</button>
											<?php } ?>

											<?php if($access_laporan == 1){ ?>
												<button id='Laporan' type='button' class='btn btn-default' title="Laporan Transaksi">
													<i class='fa fa-file-text-o'></i>
												</button>
											<?php } ?>
										</td>								
									</tr>	
								</tbody>
							</table>
						</div>
					</div>
					<!-- Akhir total harga --> 
					
	        		<!-- Awal pilih barang dan daftar transaksi -->
		            <div class="col-sm-8">
		            	<div class="row">
		            		<!-- Awal scan barang dan pilih Barang -->
		            		<div class="col-sm-4">
			                    <table id='TableScan' class="table table-condensed dt-responsive nowrap card-box">
			                    	<thead class="input-sm">
			                    		<tr class="text-dark">
			                    			<th style="width:170px;">SCAN </th>
			                    		</tr>
			                    	</thead>

			                    	<tbody>
			                    		<tr class="text-dark">
			                    			<!-- Pencarian Barang : SKU / Nama Barang -->
			                    			<td>
												<div id="induk_pencarian_barang" class="input-group">
			                                        <input id="pencarian_kode_barang" name="pencarian_kode_barang" type="text" class="form-control input-sm" placeholder="SKU / NAMA BARANG" <?php echo $disabled ?> autocomplete="off">
			                                        <span id="id_barang_pusat" name="id_barang_pusat" style="display: none;">-</span>

													<span class="input-group-btn">
														<button id="tombol_scan_barang" type="button" class="btn waves-effect waves-light btn-sm btn-primary" <?php echo $disabled ?>>
														<i id="icon_pencarian_barang" class="fa fa-search"></i></button>
														
														<button id="tombol_lihat_barang" type="button" class="btn waves-effect waves-light btn-sm btn-primary" <?php echo $disabled ?>>
														<i id="icon_lihat_barang" class="fa fa-folder-open"></i></button>
													</span>				                                        
			                                    </div>
			                                    <div id='hasil_pencarian_barang' class="alert"></div>
			                    			</td>
			                    		</tr>
			                    	</tbody>
			                    </table>
						    </div>
						    <!-- Akhir scan barang dan pilih Barang -->

						    <!-- Awal detail barang -->
						    <div class='col-sm-8'>
						    	<table id='TableBarang' class='table table-condensed dt-responsive nowrap card-box'>
						    		<thead class='input-sm'>
			                    		<tr class='text-dark'>
			                    			<th style="width:300px;">Nama Barang </th>
			                    			<th style="width:60px;">Qty</th>
			                    			<th style="width:20px;"></th>
			                    		</tr>
			                    	</thead>
			                    	<tbody class="input-sm">
			                    		<tr class="text-dark">
			                    			<!-- Nama Barang -->
			                    			<td>
			                    				<span class="text-dark" id="nama_barang" name="nama_barang">Belum ada barang yg dipilih</span>
			                    			</td>
			                    			
			                    			<!-- Jumlah Beli -->
			                    			<td>
			                    				<input type='text' class='form-control input-sm' id='jumlah_beli' name='jumlah_beli[]' onkeypress='return check_int(event)' disabled>
			                    			</td>

			                    			<td>
			                    				<button id='SimpanDetail' name="SimpanDetail" class='btn btn-primary btn-sm' type='button' disabled>
													<i class='fa fa-paper-plane-o'></i>
												</button>
			                    			</td>
			                    		</tr>
			                    	</tbody>
						    	</table>
						    </div>
						    <!-- Akhir detail barang -->

						    <!-- Awal subtotal barang -->
						    <div class="col-sm-12">
								<!-- resistensi, harga bersih dan subtotal -->
								<table id='TableHargaBarang' class="table table-condensed dt-responsive nowrap card-box">
						    		<thead class="input-sm">
			                    		<tr class="text-dark">
			                    			<th style="width:20px;">Harga</th>
			                    			<th style="width:20px;">Resistensi </th>
			                    			<th style="width:20px;">Harga Bersih </th>
			                    			<th style="width:220px;">Subtotal</th>
			                    		</tr>
			                    	</thead>
			                    	<tbody class="input-sm">
			                    		<tr class="text-dark">
			                    			<!-- Harga -->
			                    			<td class="col-md-3">
			                    				<input type='hidden' name='harga_beli' id='harga_beli'>
			                    				<input type='text' id="harga_beli_tampil" name="harga_beli_tampil" class="form-control input-sm text-dark" value='Rp. 0' disabled>
			                    			</td>

			                    			<td colspan="1" class="col-md-3">
			                    				<div class="row">
				                    				<div class="col-md-4">
					                    				<!-- Resistensi harga persen -->
					                    				<input type='text' name='resistensi_harga_persen' id='resistensi_harga_persen' class='form-control input-sm' value='0.00' disabled>
					                    			</div>
			                    				
				                    				<div class="col-md-8">
						                    			<!-- Resistensi harga rupiah-->
					                    				<input type='hidden' id='resistensi_harga_hidden' name='resistensi_harga_hidden'>
					                    				<input type='text' id='resistensi_harga' name='resistensi_harga' class='form-control input-sm' value='Rp. 0' disabled>
					                    			</div>
					                    		</div>
					                    	</td>

					                    	<td>
				                    			<!-- Harga bersih-->
			                    				<input type='hidden' id='harga_bersih_hidden' name='harga_bersih_hidden'>
			                    				<input type='text' name='harga_bersih' id='harga_bersih' class='form-control input-sm' value='Rp. 0' disabled>
					                    	</td>

			                    			<!-- Subtotal -->
			                    			<td class="col-md-3">
			                    				<input type='hidden' id='sub_total' name='sub_total'>
			                    				<span class='form-control input-sm' id='sub_total_tampil' name='sub_total_tampil'>Rp. 0</span>
			                    			</td>		                    			
			                    		</tr>
			                    	</tbody>
						    	</table>
							</div>
						    <!-- Akhir subtotal barang -->
						</div>

			        	<!-- Awal daftar transaksi --> 
		            	<div class="card-box table-responsive">
		            		<!-- Awal induk tab -->
		                    <ul class="nav nav-tabs tabs-bordered" id="tab_list_pembelian">
		                        <li class="active">
		                            <a href="#tab_induk_pembelian" id="tab_induk" data-toggle="tab" aria-expanded="true">
		                                <span class="visible-xs"><i class="fa fa-check-square"></i></span>
		                                <span class="hidden-xs" id="judul_tab_induk">Data Pembelian Barang</span>
		                            </a>
		                        </li>
		                        <li class="">
		                            <a href="#tab_pembelian_batal" id="tab_batal" data-toggle="tab" aria-expanded="true">
		                                <span class="visible-xs"><i class="typcn typcn-home"></i></span>
		                                <span class="hidden-xs" id="judul_tab_batal">Data Pembelian Barang Batal</span>
		                            </a>
		                        </li>
		                    </ul>
		                    <!-- Akhir induk tab -->

		                    <!-- Awal isi tab -->
		                    <div class="tab-content">
								<!-- Awal daftar pembelian barang -->
		                        <div class="tab-pane active" id="tab_induk_pembelian">
									<table id='TableTransaksi' class="table table-condensed table-striped table-hover dt-responsive nowrap" cellspacing="0" width="100%">
										<thead class="input-sm">
											<tr class="text-dark">
												<th>#</th>
												<th>Tombol</th>
												<th>SKU</th>
												<th style="width:800px;">Nama Barang</th>
												<th>Qty</th>
												<?php if($this->session->userdata('usergroup_name') == 'Super Admin'){ ?>
													<th>Harga</th>
													<th>Resistensi</th>
													<th>Hrg Bersih</th>
													<th>Subtotal</th>
												<?php } ?>
												<th>Pegawai Save</th>
												<th>Tanggal Save</th>
												<th>Pegawai Edit</th>
												<th>Tanggal Edit</th>
											</tr>
										</thead>

										<tbody class="input-sm text-dark"></tbody>
									</table>
								</div>
								<!-- Akhir daftar pembelian barang -->

								<!-- Awal daftar pembelian barang batal -->
		                        <div class="tab-pane" id="tab_pembelian_batal">
									<table id='TableTransaksiBatal' class="table table-condensed table-striped table-hover dt-responsive nowrap" cellspacing="0" width="100%">
										<thead class="input-sm">
											<tr class="text-dark">
												<th>#</th>
												<th>SKU</th>
												<th style="width:800px;">Nama Barang</th>
												<th>Qty</th>
												<?php if($this->session->userdata('usergroup_name') == 'Super Admin'){ ?>
													<th>Harga</th>
													<th>Resistensi</th>
													<th>Hrg Bersih</th>
													<th>Subtotal</th>
												<?php } ?>
												<th>Keterangan</th>
												<th>Pegawai</th>
												<th>Tanggal</th>
											</tr>
										</thead>

										<tbody class="input-sm text-dark"></tbody>
									</table>
								</div>
								<!-- Akhir daftar pembelian barang batal -->

								<!-- Keterangan shortcut -->
								<div class="row">
									<div class="col-sm-12">
										<p><i class='fa fa-keyboard-o fa-fw'></i> <b><small>Shortcut Keyboard : </small></b></p>
										<small>
											F6 = Cari Barang 
											, F7 = Fokus ke field catatan
											, F8 = Tahan Transaksi
											, F9 = Simpan Transaksi
										</small>
									</div>
								</div>
							</div>
							<!-- Akhir isi tab -->
		            	</div>
		            	<!-- Akhir daftar transaksi -->	
					</div>
					<!-- Awal pilih barang dan daftar transaksi -->
            	</div>
        	</div>
		</div>
	</div>
</div>

<div class="modal" id="ModalGue" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class='fa fa-times-circle'></i></button>
				<h4 class="modal-title" id="ModalHeader"></h4>
			</div>
			<div class="modal-body" id="ModalContent"></div>
			<div class="modal-footer" id="ModalFooter"></div>
		</div>
	</div>
</div>

<div class="modal" id="ModalPesan" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class='fa fa-times-circle'></i></button>
				<h4 class="modal-title" id="ModalHeaderPesan"></h4>
			</div>
			<div class="modal-body" id="ModalContentPesan"></div>
			<div class="modal-footer" id="ModalFooterPesan"></div>
		</div>
	</div>
</div>

<div class="modal" id="ModalCariBarang" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class='fa fa-times-circle'></i></button>
				<h5 class="modal-title" id="ModalHeaderCariBarang"></h5>
			</div>
			<div class="modal-body" id="ModalContentCariBarang">
				<table id='TableCariBarang' class="table table-condensed table-striped table-hover dt-responsive nowrap" cellspacing="0" width="100%">
					<thead class="input-sm">
						<tr class="text-dark">
							<th>#</th>
							<th>SKU</th>
							<th style="width:800px;">Nama Barang</th>
							<th>Stok</th>
							<?php if($this->session->userdata('usergroup_name') == 'Super Admin'){ ?>
								<th>Modal</th>
								<th>Modal Bersih</th>
							<?php } ?>
							<th>Tombol</th>
						</tr>
					</thead>

					<tbody class="input-sm text-dark"></tbody>
				</table>
			</div>
			<div class="modal-footer" id="ModalFooterCariBarang"></div>
		</div>
	</div>
</div>

<script>
	$('#ModalGue').on('hide.bs.modal', function () {
	   setTimeout(function(){ 
	        $('#ModalHeader, #ModalContent, #ModalFooter').html('');
	   }, 500);
	});

	$('#ModalPesan').on('hide.bs.modal', function () {
	   setTimeout(function(){ 
	        $('#ModalHeaderPesan, #ModalContentPesan, #ModalFooterPesan').html('');
	   }, 500);
	});

	$('#ModalCariBarang').on('hide.bs.modal', function () {
	   setTimeout(function(){ 
	        $('#ModalHeaderCariBarang, #ModalFooterCariBarang').html('');
	   }, 500);
	});	
</script>
<!-- Awal CSS -->
<link href="assets/plugin/zircos/plugins/datatables/jquery.dataTables.min.css" rel="stylesheet" type="text/css"/>
<link href="assets/plugin/zircos/plugins/datatables/responsive.bootstrap.min.css" rel="stylesheet" type="text/css"/>
<link href="assets/plugin/zircos/plugins/datatables/dataTables.bootstrap.min.css" rel="stylesheet" type="text/css"/>
<link href="assets/plugin/zircos/material-design/assets/css/tambahan.css" rel="stylesheet" type="text/css"/>
<!-- Akhir CSS -->

<!-- Awal JS -->
<script src="assets/plugin/zircos/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="assets/plugin/zircos/plugins/datatables/dataTables.bootstrap.js"></script>
<script src="assets/plugin/zircos/material-design/assets/js/detect.js"></script>
<script src="assets/plugin/zircos/material-design/assets/js/jquery.slimscroll.js"></script>
<script src="assets/plugin/zircos/plugins/datatables/dataTables.responsive.min.js"></script>
<script src="assets/plugin/zircos/plugins/datatables/responsive.bootstrap.min.js"></script>
<script src="assets/plugin/zircos/material-design/assets/pages/jquery.datatables.init.js"></script>
<!-- Akhir JS -->

<!-- Awal Sweet-Alert  -->
<link href="assets/plugin/zircos/plugins/bootstrap-sweetalert/sweet-alert.css" rel="stylesheet" type="text/css">
<script src="assets/plugin/zircos/plugins/bootstrap-sweetalert/sweet-alert.min.js"></script>
<!-- Akhir Sweet-Alert  -->

<script>
    var url               = "retur_barang_dari_toko";
    var table_induk       = $('#TableTransaksi').DataTable();
    var table_batal       = $('#TableTransaksiBatal').DataTable();
    var table_cari_barang = $('#TableCariBarang').DataTable();    

    $(document).ready(function(){
        $('#navbar').removeClass('navbar-default');
        $('#navbar').addClass('navbar-orange');
        $("#wrapper").removeClass('forced');
        $("#wrapper").addClass('forced enlarged');
        $('#JudulHalaman').html('Retur Barang Dari Toko - VAMA');
        $("#pencarian_transaksi").focus();

        var id_retur_barang_dari_toko_m = '<?php echo $this->input->get('id') ?>';
        var kode_toko                   = '<?php echo $this->input->get('kd') ?>';
        $('#kode_toko').val(kode_toko);

        if(id_retur_barang_dari_toko_m !== '' || kode_toko !== ''){
            ambil_data(id_retur_barang_dari_toko_m, kode_toko);
        }
    });

    function ambil_data(id_retur_barang_dari_toko_m, kode_toko)
    {
        if(id_retur_barang_dari_toko_m !=='0'){
            $.ajax({
                url : "<?php echo site_url('retur_barang_dari_toko/ambil_data')?>",
                type: "POST",
                cache: false,
                data: 'id_retur_barang_dari_toko_m=' + id_retur_barang_dari_toko_m +
                      '&kode_toko=' + kode_toko,
                dataType:'JSON',
                success: function(json){
                    // alert(json.url);
                    url = json.url;
                    $('#TanggalRetur').html(json.data_induk['tanggal_retur']);
                    $('#JumlahBarangRetur').html(to_rupiah(json.jumlah_retur));
                    $('#JumlahBarangMasuk').html(to_rupiah(json.data_induk['jml_barang']));
                    $('#catatan').html(json.data_induk['keterangan_lain']);

                    $('#judul_tab_induk').html("Data Retur Barang : <span class='badge up bg-primary'>" +
                                                json.data_induk['jml_barang'] +
                                               " Barang </span>");
                    $('#judul_tab_batal').html("Data Retur Barang Batal : <span class='badge up bg-danger'>" +
                                                json.data_batal['jml_barang'] +
                                               " Barang </span>");
                    tampilkan_data_induk();
                },
                error : function(data){ 
                    url = 'retur_barang_dari_toko';
                    swal({
                        title: "Oops !",
                        text: "Data retur barang dari toko gagal ditampilkan.",
                        type: "error",
                        confirmButtonClass: 'btn-danger btn-md waves-effect waves-light',
                        confirmButtonText: "Ok"
                    },function (isConfirm){
                        // window.location.href = url;
                    });
                }
            });
        } 
    }

    function tampilkan_data_induk(){
        table_induk.clear();table_induk.destroy();
        table_induk=$('#TableTransaksi').DataTable({
            stateSave: true,
            ordering: true,
            bFilter:true,
            processing:true,
            serverSide:true,
            order:[],
            lengthMenu: [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
            pagingType: "full",

            oLanguage:{
                sLoadingRecords:'Silakan Tunggu',
                sProcessing:"<img src='assets/loader/cubeloader2.2.gif' width='50' height='50'> Harap tunggu",
                sZeroRecords:"Belum ada data untuk ditampilkan",
                sSearch : "Pencarian data"
            },
            ajax:{
                url: "<?php echo site_url('retur_barang_dari_toko/ajax_list_retur')?>",
                type: "POST",
                data: {
                    'kode_toko' : $('#kode_toko').val(),
                    'id_retur_pembelian_m' : $('#id_retur_pembelian_m').val(),
                }
            },
            columnDefs: [{
                "targets": [ -1 ],
                "orderable": false,
            },],
        });
    }

    function tampilkan_data_batal(){
        table_batal.clear();table_batal.destroy();
        table_batal=$('#TableTransaksiBatal').DataTable({
            stateSave: true,
            ordering: true,
            bFilter:true,
            processing:true,
            serverSide:true,
            order:[],
            lengthMenu: [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
            pagingType: "full",

            oLanguage:{
                sLoadingRecords:'Silakan Tunggu',
                sProcessing:"<img src='assets/loader/cubeloader2.2.gif' width='50' height='50'> Harap tunggu",
                sZeroRecords:"Belum ada data untuk ditampilkan",
                sSearch : "Pencarian data"
            },
            ajax:{
                url: "<?php echo site_url('retur_barang_dari_toko/ajax_list_retur_batal')?>",
                type: "POST",
                data: {
                    'kode_toko' : $('#kode_toko').val(),
                    'id_retur_pembelian_m' : $('#id_retur_pembelian_m').val(),
                }
            },
            columnDefs: [{
                "targets": [ -1 ],
                "orderable": false,
            },],
        });
    }

    var delay = (function () {
        var timer = 0;
        return function (callback, ms) {
            clearTimeout(timer);
            timer = setTimeout(callback, ms);
        };
    })();

    $(document).on('click', '#tab_induk', function(e){
        tampilkan_data_induk();
    });

    $(document).on('click', '#tab_batal', function(e){
        tampilkan_data_batal();
    });

    function reload_table_induk()
    {
        table_induk.ajax.reload();
    }

    function reload_table_batal()
    {
        table_batal.ajax.reload();
    }

    $(document).on('click', '#daftar_autocompleted_transaksi li', function(){    
        var id_retur_pembelian_m = $(this).find('span#data_id_retur_pembelian_m').html();
        var no_retur_pembelian   = $(this).find('span#data_no_retur').html();
        var jml_barang           = $(this).find('span#data_jml_barang_retur').html();
        var tanggal_retur        = $(this).find('span#data_tanggal_retur').html();
        var keterangan_lain      = $(this).find('span#data_keterangan_lain').html();
        
        $('#hasil_pencarian_transaksi').hide();
        $('#id_retur_pembelian_m').val(id_retur_pembelian_m);   
        $('#pencarian_transaksi').val(no_retur_pembelian);
        $('#TanggalRetur').html(tanggal_retur);
        $('#JumlahBarangRetur').html(jml_barang);
        $('#catatan').html(keterangan_lain);
        tampilkan_data_induk();
    });

    $(document).on('keyup', '#pencarian_transaksi', function(e){
        if($(this).val() !== ''){
            var charCode = e.which || e.keyCode;
            if(charCode == 40){
                if($('div#hasil_pencarian_transaksi li.autocomplete-active').length > 0){
                    var Selanjutnya = $('div#hasil_pencarian_transaksi li.autocomplete-active').next();
                    $('div#hasil_pencarian_transaksi li.autocomplete-active').removeClass('autocomplete-active');
                    Selanjutnya.addClass('autocomplete-active');
                }else{
                    $('div#hasil_pencarian_transaksi li:first').addClass('autocomplete-active');
                }
            }else if(charCode == 38){
                if($('div#hasil_pencarian_transaksi li.autocomplete-active').length > 0){
                    var Sebelumnya = $('div#hasil_pencarian_transaksi li.autocomplete-active').prev();
                    $('div#hasil_pencarian_transaksi li.autocomplete-active').removeClass('autocomplete-active');
                    Sebelumnya.addClass('autocomplete-active');
                }else{
                    $('div#hasil_pencarian_transaksi li:first').addClass('autocomplete-active');
                }
            }else if(charCode == 13){
                $('div#hasil_pencarian_barang').hide();
                $.ajax({
                    url: "<?php echo site_url('retur_barang_dari_toko/ajax_cari_no_penjualan'); ?>",
                    type: "POST",
                    cache: false,
                    data: 'no_penjualan=' + $('#pencarian_transaksi').val(),
                    dataType: 'json',
                    success: function(json){
                        if(json.status == 1){
                            $('#hasil_pencarian_transaksi').hide();
                            $('#id_retur_pembelian_m').html(json.data['id_retur_pembelian_m']);
                            $('#pencarian_transaksi').val(json.data['no_penjualan']);
                            $('#id_customer_pusat').html(json.data['id_customer_pusat']);
                            $('#nama_customer').html(json.data['nama_customer_pusat']);
                            $('#tanggal_penjualan').html(json.data['tanggal_penjualan']);
                            $('#TotalPenjualan').html(to_rupiah(json.data['grand_total']));
                            $('#TotalPenjualanHidden').html(json.data['grand_total']);
                            $('#jumlah_barang').html(json.data['jumlah_jual']);
                            $('#catatan').focus();
                            tampilkan_data_induk();
                        }else if(json.status == 0){
                            swal({
                                title: "Oops !",
                                text: json.pesan,
                                type: "error",
                                confirmButtonClass: 'btn-danger btn-md waves-effect waves-light',
                                confirmButtonText: "Ok"
                            },function(isConfirm){
                                window.location.href = url;
                            });
                        }
                    }, 
                    error: function (jqXHR, textStatus, errorThrown){
                        swal({
                            title: "Oops !",
                            text: "No. penjualan gagal dicari.",
                            type: "error",
                            confirmButtonClass: 'btn-danger btn-md waves-effect waves-light',
                            confirmButtonText: "Ok"
                        },function(isConfirm){
                            window.location.href = url;
                        });
                    }
                });
            }else{
                // AutoCompletedDaftarTransaksiPenjualan($(this).val());
            }
        }else{
            // $('#id_retur_pembelian_m').html('');            
            // $('#id_customer_pusat').html('-');
            // $('#nama_customer').html('-');
            // $('#tanggal_penjualan').html('-');
            // $('#TotalPenjualan').html('0');
            // $('#TotalPenjualanHidden').html('');
            // $('#jumlah_barang').html('0');

            // Default detail barang
            // $('#sku').html('0');
            // $('#nama_barang').html('0');
            // $('#jumlah_barang').html('0');
            // $('#harga_satuan_hidden').val('0');
            // $('#harga_satuan').html('0');
            // $('#persen_potongan').prop('disabled', true).val('0.00');
            // $('#potongan_harga').prop('disabled', true).val('0');
            // $('#jumlah_beli').html('0');
            // $('#jumlah_retur').prop('disabled', true).val('0');
            // $('#sub_potongan').html('0');
            // $('#sub_potongan_hidden').val('0');
            // $('#sub_saldo').html('0');
            // $('#sub_saldo_hidden').val('0');
            // $('#sub_retur').html('0');
            // $('#sub_retur_hidden').val('0');
            // $('#masuk_stok').prop('disabled', true).val('-');
            // $('#keterangan_retur').prop('disabled', true).val('');
            // $('#SimpanDetail').prop('disabled', true);
            // $('#pencarian_transaksi').focus();
        }
    });

    $(document).on('keydown', 'body', function(e){
        var charCode = ( e.which ) ? e.which : event.keyCode;

        if(charCode == 118) //F7
        {
            $('#catatan').focus();
            return false;
        }

        if(charCode == 119) //F8
        {
            konfirmasi_tahan_transaksi();
            return false;
        }

        if(charCode == 120) //F9
        {
            konfirmasi_simpan_transaksi();
            return false;
        }
    });

    $(document).on('click', 'body', function(){
        $('div#hasil_pencarian_transaksi').hide();
    });

    $(document).on('click', 'button#tombol_cari_transaksi', function(){
        AutoCompletedDaftarTransaksiPenjualan($('#pencarian_transaksi').val());
    });

    $(document).on('click', 'button#Tahan', function(){
        konfirmasi_tahan_transaksi();
    });

    $(document).on('click', 'button#Simpan', function(){
        konfirmasi_simpan_transaksi();
    });

    $(document).on('click', 'button#Laporan', function(){
        window.open("laporan_retur_barang_dari_toko",'_blank');
    });

    $(document).on('keyup', '#persen_potongan', function(e){
        var charCode = e.which || e.keyCode;
        if(charCode == 13){
            if($('#jumlah_retur').val() == 0){
                $('#jumlah_retur').val('');
            }
            $('#jumlah_retur').focus();
        }else{
            HitungSubtotal();
        }
    });

    $(document).on('keyup', '#potongan_harga', function(e){
        var charCode = e.which || e.keyCode;
        if(charCode == 13){
            if($('#jumlah_retur').val() == 0){
                $('#jumlah_retur').val('');
            }
            $('#jumlah_retur').focus();
        }else{
            var PH = $('#potongan_harga').val();
                PH = to_angka(PH);
            
            if(PH > 0){
                $('#potongan_harga').val(to_rupiah(PH));
                $('#potongan_harga_hidden').val(PH);
            }else{
                $('#potongan_harga').val('');
                $('#potongan_harga_hidden').val('0');
            }
            HitungSubtotal2();
        }
    });

    $(document).on('keyup', '#jumlah_retur', function(e){
        var charCode = e.which || e.keyCode;
        if(charCode == 13){
            $('#masuk_stok').focus();
        }else{
            HitungSubtotal();
        }
    });

    $(document).on('keyup', '#keterangan_retur', function(e){
        var charCode = e.which || e.keyCode;
        if(charCode == 13){
            kofirmasi_simpan_barang();
        }
    });

    function kofirmasi_simpan_barang(){
        var pesan = '';
        if($('#sku').html() > 0){
            if($('#jumlah_retur').val() > 0){
                if($('#jumlah_retur').val() > parseInt($('#jumlah_beli').html())){
                    pesan = 'Jumlah retur melebihi jumlah jual, harap kurangi jumlah retur';
                }else{
                    if($('#masuk_stok').val() !== '-'){
                        if($('#keterangan_retur').val() == ''){
                            pesan = 'Harap isikan keterangan retur terlebih dahulu';
                        }
                    }else{
                        pesan = 'Harap pilih masuk stok terlebih dahulu';
                    }                
                }
            }else{
                pesan = 'Harap isikan jumlah retur terlebih dahulu';            }
        }else{
            pesan = 'Belum ada barang yang anda simpan ditransaksi ini, harap simpan barang terlebih dahulu';
        }

        if(pesan !== ''){ 
            swal({
                title: "Oops !",
                text: pesan,
                type: "error",
                confirmButtonClass: 'btn-danger btn-md waves-effect waves-light',
                confirmButtonText: "Ok"
            });
        }else{
            $('.modal-dialog').removeClass('modal-lg');
            $('.modal-dialog').addClass('modal-sm');
            $('#ModalHeader').html('Konfirmasi');
            $('#ModalContent').html(
                                        "<b class='text-dark'>Apakah anda yakin ingin menyimpan retur barang ini ? </b></br></br>" +
                                        "SKU : </br>" +

                                        "<b class='text-dark'>" + $('#pencarian_kode_barang').val() + "</b></br></br>" +

                                        "Nama Barang : </br>" +
                                        "<b class='text-dark'>" + $('#nama_barang').html() + "</b></br></br>" +

                                        "Jumlah Jual : </br>" +
                                        "<h2 class='text-dark'>" + $('#jumlah_beli').html() + "</h2>" +

                                        "Jumlah Retur : </br>" +
                                        "<h2 class='text-danger'>" + $('#jumlah_retur').val() + "</h2>"
                                    );
            $('#ModalFooter').html("<button type='button' class='btn btn-primary' id='SimpanDataBarang' onClick='simpan_barang();'>Ya, saya yakin</button>" +
                                   "<button type='button' class='btn btn-default' data-dismiss='modal'>Batal</button>");
            $('#ModalGue').modal('show');

            setTimeout(function(){
                $('button#SimpanDataBarang').focus();
            }, 500);
        }
    }

    $(document).on('click', 'button#SimpanDetail', function(){
        kofirmasi_simpan_barang();
    });

    function simpan_barang()
    {
        $('#ModalGue').modal('hide');
        var FormData = "no_retur_barang_dari_toko="+$('#no_retur_barang_dari_toko').html(); 
            FormData += "&id_retur_barang_dari_toko_m="+$('#id_retur_barang_dari_toko_m').html();
            FormData += "&id_retur_pembelian_m="+$('#id_retur_pembelian_m').html();
            FormData += "&id_penjualan_d="+$('#id_penjualan_d').val();
            FormData += "&no_penjualan="+$('#pencarian_transaksi').val();
            FormData += "&id_customer_pusat="+$('#id_customer_pusat').html();
            FormData += "&id_barang_pusat="+$('#id_barang_pusat').val();
            FormData += "&sku="+$('#sku').val();
            FormData += "&harga_satuan="+$('#harga_satuan_hidden').val();
            FormData += "&potongan_harga_value="+$('#persen_potongan').val();
            FormData += "&potongan_harga="+$('#potongan_harga_hidden').val();
            FormData += "&jumlah_beli="+$('#jumlah_beli').html();
            FormData += "&jumlah_retur="+$('#jumlah_retur').val();
            FormData += "&subtotal_potongan="+$('#sub_potongan_hidden').val();
            FormData += "&subtotal_saldo="+$('#sub_saldo_hidden').val();
            FormData += "&subtotal_retur="+$('#sub_retur_hidden').val();
            FormData += "&masuk_stok="+$('#masuk_stok').val();
            FormData += "&keterangan_retur="+encodeURI($('#keterangan_retur').val());
            FormData += "&catatan="+encodeURI($('#catatan').val());

        $.ajax({
            url: "<?php echo site_url('retur_barang_dari_toko/simpan_detail'); ?>",
            type: "POST",
            cache: false,
            data: FormData,
            dataType:'json',
            success: function(data){
                if(data.status == 1){
                    if(url == 'retur_barang_dari_toko'){
                        setTimeout(function(){
                            swal({
                                title: "Berhasil!", text: data.pesan, type: "success",
                                confirmButtonText: "Ok",
                            },function(isConfirm){
                                window.location.href = data.url;
                            });
                        }, 100);
                        return;
                    }else{
                        tampilkan_data_induk();
                    }

                    $('#judul_tab_induk').html("Data Retur Barang : <span class='badge up bg-primary'>" +
                                                data.jumlah_masuk +
                                               " Barang </span>");
                    $('#judul_tab_batal').html("Data Retur Barang Batal : <span class='badge up bg-danger'>" +
                                                data.jumlah_batal +
                                               " Barang </span>");

                    // Tampilkan id penjualan master dan total bayar dll
                    $('#id_retur_barang_dari_toko_m').val(data.id_rpm);
                    $('#JumlahBarangRetur').html(to_rupiah(data.jumlah_retur));
                    $('#JumlahBarangMasuk').html(to_rupiah(data.jumlah_masuk));
                    $('#TotalRetur').html(to_rupiah(data.total_retur));
                    $('#TotalReturHidden').val(data.total_retur);
                    $('#TotalPotongan').html(to_rupiah(data.total_potongan));
                    $('#TotalPotonganHidden').val(data.total_potongan);
                    $('#TotalSaldo').html(to_rupiah(data.total_saldo));
                    $('#TotalSaldoHidden').val(data.total_saldo);                

                    // Bersihkan data pemilihan barang
                    $('#id_penjualan_d').val('');
                    $('#id_barang_pusat').val('');
                    $('#sku').html('');
                    $('#nama_barang').html('Belum ada barang yg dipilih');
                    $('#jumlah_beli').html('0');
                    $('#harga_satuan_hidden').val('0');
                    $('#harga_satuan').html('Rp. 0');
                    $('#persen_potongan').val('0.00');
                    $('#potongan_harga').val('Rp. 0');
                    $('#potongan_harga_hidden').val('0');
                    $('#jumlah_retur').val('0');
                    $('#sub_potongan').html('Rp. 0');
                    $('#sub_potongan_hidden').val('0');
                    $('#sub_saldo').html('Rp. 0');
                    $('#sub_saldo_hidden').val('0');
                    $('#sub_retur').html('Rp. 0');
                    $('#sub_retur_hidden').val('0');
                    $('#masuk_stok').val('-');
                    $('#keterangan_retur').val('');

                    $('#persen_potongan').prop('disabled', true);
                    $('#potongan_harga').prop('disabled', true);
                    $('#jumlah_retur').prop('disabled', true);
                    $('#masuk_stok').prop('disabled', true);
                    $('#keterangan_retur').prop('disabled', true);
                    $('#SimpanDetail').prop('disabled', true);
                    swal({
                        title: "Berhasil!", text: data.pesan, type: "success",
                        confirmButtonText: "Ok",
                    },function(isConfirm){
                        setTimeout(function(){
                            $('#catatan').focus();
                        }, 100);
                    });

                }else if(data.status == 2){
                    $('.modal-dialog').removeClass('modal-lg');
                    $('.modal-dialog').addClass('modal-sm');
                    $('#ModalHeader').html('Oops !');
                    $('#ModalContent').html(data.pesan);
                    $('#ModalFooter').html("<button type='button' class='btn btn-danger' data-dismiss='modal' autofocus>Ok, Saya Mengerti</button>");
                    $('#ModalGue').modal('show');
                }else if(data.status == 0){
                    swal({
                        title: "Oops !",
                        text: data.pesan,
                        type: "error",
                        confirmButtonClass: 'btn-danger btn-md waves-effect waves-light',
                        confirmButtonText: "Ok"
                    },function (isConfirm){
                        window.location.href = url;
                    });
                }
            },
            error: function (jqXHR, textStatus, errorThrown){
                swal({
                    title: "Oops !",
                    text: "Gagal menyimpan data retur barang detail.",
                    type: "error",
                    confirmButtonClass: 'btn-danger btn-md waves-effect waves-light',
                    confirmButtonText: "Ok"
                },function (isConfirm){
                    window.location.href = url;
                });
            }
        });
    }

    function simpan_retur_detail(id_retur_pembelian_m, id_retur_pembelian_d, kode_toko)
    {
        swal({
            title: "Apakah barang retur sudah di periksa ? jika sudah, apakah anda yakin untuk menyetuji retur barang ini ?",
            type: "info",
            showCancelButton: true,
            confirmButtonClass: 'btn-primary btn-md waves-effect waves-light',
            confirmButtonText: "Iya, saya yakin",
            cancelButtonText: "Batal",
        },function (isConfirm){
            if(isConfirm){
                setTimeout(function(){ 
                    $.ajax({
                        url : "<?php echo site_url('retur_barang_dari_toko/simpan_detail')?>",
                        type: "POST",
                        cache: false,
                        data: 'id_retur_pembelian_m=' + id_retur_pembelian_m +
                              '&id_retur_pembelian_d=' + id_retur_pembelian_d +
                              '&kode_toko=' + kode_toko,
                        dataType: "JSON",
                        success: function(json){
                            if(json.status == 1){
                                swal({
                                    title: "Berhasil !",
                                    text: json.pesan,
                                    type: "success",
                                    confirmButtonClass: 'btn-success btn-md waves-effect waves-light',
                                    confirmButtonText: "Ok"
                                },function (isConfirm){
                                    if(url == json.url){
                                        tampilkan_data_induk();
                                    }else{
                                        window.location.href = json.url;
                                    }
                                    $('#JumlahBarangRetur').html(to_rupiah(json.jumlah_retur));
                                    $('#JumlahBarangMasuk').html(to_rupiah(json.jumlah_masuk));
                                });
                            }else if(json.status == 0){
                                swal({
                                    title: "Oops !",
                                    text: json.pesan,
                                    type: "error",
                                    confirmButtonClass: 'btn-danger btn-md waves-effect waves-light',
                                    confirmButtonText: "Ok"
                                },function (isConfirm){
                                    window.location.href = url;
                                });
                            }
                        },
                        error: function (jqXHR, textStatus, errorThrown){
                            swal({
                                title: "Oops !",
                                text: "Gagal menyimpan data retur barang dari toko.",
                                type: "error",
                                confirmButtonClass: 'btn-danger btn-md waves-effect waves-light',
                                confirmButtonText: "Ok"
                            },function (isConfirm){
                                window.location.href = url;
                            });
                        }
                    });
                }, 100);
            }
        });
    }

    function verifikasi_hapus_retur_detail(id_retur_pembelian_m, id_retur_pembelian_d, kode_toko)
    {
        $.ajax({
            url : "<?php echo site_url('retur_barang_dari_toko/ajax_verifikasi_hapus_detail')?>",
            type: "POST",
            cache: false,
            data: 'id_retur_pembelian_m=' + id_retur_pembelian_m + 
                  '&id_retur_pembelian_d=' + id_retur_pembelian_d +
                  '&kode_toko=' + kode_toko,
            dataType: "JSON",
            success: function(json){
                if(json.status == 1){
                    $('.modal-dialog').removeClass('modal-lg');
                    $('.modal-dialog').addClass('modal-sm');
                    $('#ModalHeader').html('Informasi Hapus Barang');
                    $('#ModalContent').html(json.pesan);
                    $('#ModalFooter').html(json.footer);
                    $('#ModalGue').modal('show');
                }else{
                    swal({
                        title: "Oops !",
                        text: json.pesan,
                        type: "error",
                        confirmButtonClass: 'btn-danger btn-md waves-effect waves-light',
                        confirmButtonText: "Ok"
                    },function (isConfirm){
                        window.location.href = url;
                    });
                }
            },
            error: function (jqXHR, textStatus, errorThrown){
                swal({
                    title: "Oops !",
                    text: "Gagal menampilkan data retur barang dari toko detail.",
                    type: "error",
                    confirmButtonClass: 'btn-danger btn-md waves-effect waves-light',
                    confirmButtonText: "Ok"
                },function (isConfirm){
                    window.location.href = url;
                });
            }
        });
    }

    function hapus_retur_barang_dari_toko_detail(id_retur_pembelian_m, id_retur_pembelian_d)
    {
        $.ajax({
            url : "<?php echo site_url('retur_barang_dari_toko/ajax_hapus_detail')?>",
            type: "POST",
            cache: false,
            data: "id_retur_pembelian_m=" + id_retur_pembelian_m +
                  "&id_retur_pembelian_d=" + id_retur_pembelian_d +
                  "&kode_toko=" + $('#kode_toko').val(),
            dataType: "JSON",
            success: function(data){
                if(data.status == 1){
                    if(url == 'retur_barang_dari_toko'){
                        setTimeout(function(){
                            swal({
                                title: "Berhasil!", text: data.pesan, type: "success",
                                confirmButtonText: "Ok",
                            },function(isConfirm){
                                window.location.href = data.url;
                            });
                        }, 100);
                        return;
                    }else{
                        tampilkan_data_induk();
                    }

                    // Tampilkan id penjualan master dan total bayar dll
                    $('#id_retur_barang_dari_toko_m').val(data.id_rpm);
                    $('#JumlahBarangRetur').html(to_rupiah(data.jumlah_retur));
                    $('#JumlahBarangMasuk').html(to_rupiah(data.jumlah_masuk));
                    $('#JumlahBarangBatak').html(to_rupiah(data.jumlah_batal));

                    $('#judul_tab_induk').html("Data Retur Barang : <span class='badge up bg-primary'>" +
                                                data.jumlah_masuk +
                                               " Barang </span>");
                    $('#judul_tab_batal').html("Data Retur Barang Batal : <span class='badge up bg-danger'>" +
                                                data.jumlah_batal +
                                               " Barang </span>");
                }else if(data.status == 0){
                    swal({
                        title: data.judul,
                        text: data.pesan,
                        type: data.tipe_pesan,
                        confirmButtonClass: data.gaya_tombol,
                        confirmButtonText: "Ok"
                    },function (isConfirm){
                        window.location.href = url;
                    });
                }
            },
            error: function (jqXHR, textStatus, errorThrown){
                swal({
                    title: "Gagal!",
                    text: "Barang retur barang gagal dihapus.",
                    type: "error",
                    confirmButtonClass: 'btn-danger btn-md waves-effect waves-light',
                    confirmButtonText: "Ok"
                },function (isConfirm){
                    window.location.href = url;
                });
            }
        });
    }

    function tampilkan_total_penjualan()
    {
        $.ajax({
            url: "<?php echo site_url('penjualan/tampilkan_total_penjualan'); ?>",
            type: "POST",
            cache: false,
            data: 'no_penjualan=' + $('#no_penjualan').html(),
            dataType:'json',
            success: function(data){
                if(data.status == 1){   
                    $('#id_retur_pembelian_m').val(data.id_pm);                    
                    $('#pencarian_kode').val('');
                    $('#nama_barang').html('Belum ada barang yg dipilih');
                    $('#harga_satuan').val('');
                    $('#harga_satuan_tampil').html('Rp. 0');
                    $('#jenis_harga').val('');
                    $('#jumlah_beli').val('0');
                    $('#discount').val('0.00');
                    $('#discount_hidden').val('');
                    $('#sub_total').val('');
                    $('#sub_total_tampil').html('Rp. 0');
                    $('#jumlah_beli').prop('disabled', true);
                    $('#discount').prop('disabled', true)
                }else if(data.status == 0){
                    $('.modal-dialog').removeClass('modal-lg');
                    $('.modal-dialog').addClass('modal-sm');
                    $('#ModalHeader').html('Oops !');
                    $('#ModalContent').html(data.pesan);
                    $('#ModalFooter').html("<button type='button' class='btn btn-primary' data-dismiss='modal' autofocus>Ok, Saya Mengerti</button>");
                    $('#ModalGue').modal('show');
                }
            }
        });
    }

    function konfirmasi_tahan_transaksi(){
        if($('#id_retur_pembelian_m').val() == '' | $('#id_retur_pembelian_m').val() == '0'){
            swal({
                title: "Oops !",
                text: "Belum ada transaksi retur barang dari toko yang anda pilih untuk di retur.",
                type: "error",
                confirmButtonClass: 'btn-danger btn-md waves-effect waves-light',
                confirmButtonText: "Ok"
            }, function (isConfirm){
                setTimeout(function(){
                    $('#pencarian_transaksi').focus();
                }, 100);
            });    
        }else{
            swal({
                title: "Apakah anda yakin ingin menahan transaksi retur barang ini ?",
                type: "info",
                showCancelButton: true,
                confirmButtonClass: 'btn-primary btn-md waves-effect waves-light',
                confirmButtonText: "Iya, saya yakin",
                cancelButtonText: "Batal",
            },function (isConfirm){
                if(isConfirm){
                    TahanTransaksi();
                }
            });
        }
    }

    function TahanTransaksi()
    {
        var FormData = "&id_retur_pembelian_m=" + $('#id_retur_pembelian_m').val();
            FormData += "&no_retur_pembelian=" + $('#pencarian_transaksi').val();
            FormData += "&kode_toko=" + $('#kode_toko').val();

        $.ajax({
            url: "<?php echo site_url('retur_barang_dari_toko/tahan_transaksi'); ?>",
            type: "POST",
            cache: false,
            data: FormData,
            dataType:'json',
            success: function(data){
                if(data.status == 1){
                    setTimeout(function(){
                        swal({
                            title: "Berhasil!",
                            text: data.pesan,
                            type: "success",
                            confirmButtonText: "Ok"
                        }, function (isConfirm){
                            if(isConfirm){
                                window.location.href = "retur_barang_dari_toko";
                            }
                        });
                    }, 100);
                }else if(data.status == 0){
                    setTimeout(function(){
                        swal({
                            title: "Oops !",
                            text: data.pesan,
                            type: "error",
                            confirmButtonClass: 'btn-danger btn-md waves-effect waves-light',
                            confirmButtonText: "Ok"
                        }, function (isConfirm){
                            if(isConfirm){
                                window.location.href = data.url;
                            }
                        });
                    }, 100);
                }
            },
            error: function (jqXHR, textStatus, errorThrown){
                setTimeout(function(){
                    swal({
                        title: "Oops !",
                        text: "Gagal menahan transaksi retur barang ini.",
                        type: "error",
                        confirmButtonClass: 'btn-danger btn-md waves-effect waves-light',
                        confirmButtonText: "Ok"
                    }, function (isConfirm){
                        if(isConfirm){
                            window.location.href = url;
                        }
                    });
                }, 100);
            }
        });
    }

    function konfirmasi_simpan_transaksi(){
        if($('#id_retur_pembelian_m').val() == '' | $('#id_retur_pembelian_m').val() == '0'){
            swal({
                title: "Oops !",
                text: "Belum ada transaksi retur barang dari toko yang anda pilih untuk di retur.",
                type: "error",
                confirmButtonClass: 'btn-danger btn-md waves-effect waves-light',
                confirmButtonText: "Ok"
            }, function (isConfirm){
                setTimeout(function(){
                    $('#pencarian_transaksi').focus();
                }, 100);
            });    
        }else{
            swal({
                title: "Apakah anda yakin ingin menyimpan dan menyelesaikan transaksi retur barang ini ?",
                type: "info",
                showCancelButton: true,
                confirmButtonClass: 'btn-primary btn-md waves-effect waves-light',
                confirmButtonText: "Iya, saya yakin",
                cancelButtonText: "Batal",
            },function (isConfirm){
                if(isConfirm){
                    SimpanTransaksi();
                }
            });
        }
    }

    function SimpanTransaksi()
    {
        var FormData = "&id_retur_pembelian_m=" + $('#id_retur_pembelian_m').val();
            FormData += "&no_retur_pembelian=" + $('#pencarian_transaksi').val();
            FormData += "&kode_toko=" + $('#kode_toko').val();

        $.ajax({
            url: "<?php echo site_url('retur_barang_dari_toko/simpan_transaksi'); ?>",
            type: "POST",
            cache: false,
            data: FormData,
            dataType:'json',
            success: function(data){
                if(data.status == 1){
                    setTimeout(function(){
                        swal({
                            title: "Berhasil!",
                            text: data.pesan,
                            type: "success",
                            confirmButtonClass: 'btn-success btn-md waves-effect waves-light',
                            confirmButtonText: "Ok",
                        }, function (isConfirm){
                            window.location.href = "retur_barang_dari_toko";
                        });
                    }, 100);
                }else if(data.status == 0){
                    setTimeout(function(){
                        swal({
                            title: "Oops !",
                            text: data.pesan,
                            type: "error",
                            confirmButtonClass: 'btn-danger btn-md waves-effect waves-light',
                            confirmButtonText: "Ok"
                        },function (isConfirm){
                            if(isConfirm){
                                window.location.href = data.url;
                            }
                        });
                    }, 100);
                }
            },
            error: function (jqXHR, textStatus, errorThrown){
                setTimeout(function(){
                    swal({
                        title: "Oops !",
                        text: "Gagal menyimpan transaksi retur barang ini.",
                        type: "error",
                        confirmButtonClass: 'btn-danger btn-md waves-effect waves-light',
                        confirmButtonText: "Ok"
                    }, function (isConfirm){
                        if(isConfirm){
                            window.location.href = url;
                        }
                    });
                }, 100);
            }
        });
    }

    function AutoCompletedDaftarTransaksiPenjualan(KataKunci)
    {
        $('#tombol_cari_transaksi').removeClass('btn-warning');
        $('#tombol_cari_transaksi').addClass('btn-success');

        $('#induk_pencarian_transaksi').removeClass('has-warning');
        $('#induk_pencarian_transaksi').addClass('has-success');

        $('#icon_pencarian_transaksi').removeClass('fa fa-search');
        $('#icon_pencarian_transaksi').removeClass('fa fa-band');
        $('#icon_pencarian_transaksi').addClass('fa fa-spin fa-refresh');
        $('div#hasil_pencarian_transaksi').hide();

        delay(function(){
            $.ajax({
                url: "<?php echo site_url('retur_barang_dari_toko/ajax_pencarian_transaksi'); ?>",
                type: "POST",
                cache: false,
                data: 'keyword=' + KataKunci +
                      '&kode_toko=' + $('#kode_toko').val(),
                dataType:'json',
                success: function(json){
                    if(json.status == 1){
                        $('div#hasil_pencarian_transaksi').show('fast');
                        $('div#hasil_pencarian_transaksi').html(json.datanya);

                        $('#icon_pencarian_transaksi').removeClass('fa fa-spin fa-refresh');
                        <?php if($access_create == 1 || $access_update == 1){ ?>
                            $('#icon_pencarian_transaksi').addClass('fa fa-search');
                        <?php }else{ ?>
                            $('#icon_pencarian_transaksi').addClass('fa fa-band');
                        <?php } ?>
                    }else if(json.status == 2){
                        swal({
                            title: "Oops !",
                            text: "Maaf anda tidak diizinkan untuk menambahkan data retur barang dari toko!",
                            type: "error",
                            confirmButtonClass: 'btn-danger btn-md waves-effect waves-light',
                            confirmButtonText: "Ok"
                        },function (isConfirm){
                            window.location.href = url;
                        });
                    }else if(json.status == 0){
                        $('#tombol_cari_transaksi').removeClass('btn-success');
                        $('#tombol_cari_transaksi').addClass('btn-warning');
                        $('#icon_pencarian_transaksi').removeClass('fa fa-spin fa-refresh');
                        <?php if($access_create == 1 || $access_update == 1){ ?>
                            $('#icon_pencarian_transaksi').addClass('fa fa-search');
                        <?php }else{ ?>
                            $('#icon_pencarian_transaksi').addClass('fa fa-band');
                        <?php } ?>
                        $('#induk_pencarian_transaksi').removeClass('has-success');
                        $('#induk_pencarian_transaksi').addClass('has-warning');                        
                    }
                }
            });
        }, 1000);
    }

    function HitungSubtotal()
    {
        var Harga              = $('#harga_satuan_hidden').val();
        var JumlahRetur        = $('#jumlah_retur').val();
        var PersentasePotongan = $('#persen_potongan').val();
        var PotonganHarga      = PersentasePotongan * parseInt(Harga);

        if(PotonganHarga > 0){
            $('#potongan_harga').val(to_rupiah(PotonganHarga));
            $('#potongan_harga_hidden').val(PotonganHarga);
        }else{
            $('#potongan_harga').val('0');
            $('#potongan_harga_hidden').val('0');
        }

        var SubPotongan   = parseInt(PotonganHarga) * parseInt(JumlahRetur);
        var SubSaldo      = parseInt(Harga-PotonganHarga) * parseInt(JumlahRetur);
        var SubRetur      = parseInt(Harga) * parseInt(JumlahRetur);
        
        var SubPotonganRp = 'Rp. 0';
        var SubSaldoRp    = 'Rp. 0';
        var SubReturRp    = 'Rp. 0';

        if(SubPotongan >= '0')
        {SubPotonganRp  = to_rupiah(SubPotongan);}

        if(SubSaldo >= '0')
        {SubSaldoRp  = to_rupiah(SubSaldo);}

        if(SubRetur >= '0')
        {SubReturRp  = to_rupiah(SubRetur);}
        
        $('#sub_potongan_hidden').val(SubPotongan);
        $('#sub_potongan').html(SubPotonganRp);

        $('#sub_saldo_hidden').val(SubSaldo);
        $('#sub_saldo').html(SubSaldoRp);

        $('#sub_retur_hidden').val(SubRetur);
        $('#sub_retur').html(SubReturRp);    
    }

    function HitungSubtotal2()
    {
        var Harga              = $('#harga_satuan_hidden').val();
        var JumlahRetur        = $('#jumlah_retur').val();
        var PotonganHarga      = $('#potongan_harga_hidden').val();
        var PersentasePotongan = (PotonganHarga/Harga);
        
        if (PersentasePotongan > 0){
            $('#persen_potongan').val(PersentasePotongan);
        }else{
            $('#persen_potongan').val('0.00');
        }
        
        var SubPotongan        = parseInt(PotonganHarga) * parseInt(JumlahRetur);
        var SubSaldo           = parseInt(Harga-PotonganHarga) * parseInt(JumlahRetur);
        var SubRetur           = parseInt(Harga) * parseInt(JumlahRetur);
        
        var SubPotonganRp      = 'Rp. 0';
        var SubSaldoRp         = 'Rp. 0';
        var SubReturRp         = 'Rp. 0';
        
        if(SubPotongan         >= '0')
        {SubPotonganRp         = to_rupiah(SubPotongan);}
        
        if(SubSaldo            >= '0')
        {SubSaldoRp            = to_rupiah(SubSaldo);}
        
        if(SubRetur            >= '0')
        {SubReturRp            = to_rupiah(SubRetur);}
        
        $('#sub_potongan_hidden').val(SubPotongan);
        $('#sub_potongan').html(SubPotonganRp);

        $('#sub_saldo_hidden').val(SubSaldo);
        $('#sub_saldo').html(SubSaldoRp);

        $('#sub_retur_hidden').val(SubRetur);
        $('#sub_retur').html(SubReturRp);    
    }

    function to_rupiah(angka){
        var rev        = parseInt(angka, 10).toString().split('').reverse().join('');
        var rev2       = '';
        for(var i      = 0; i < rev.length; i++){
            rev2       += rev[i];
            if((i + 1) % 3 === 0 && i !== (rev.length - 1)){
                rev2   += '.';
            }
        }
        return rev2.split('').reverse().join('');

        // Pakai Simbol Rp.
        // return 'Rp. ' + rev2.split('').reverse().join('');
    }

    function to_angka(rp)
    {
        return parseInt(rp.replace(/,.*|\D/g,''),10)
    }

    function check_int(evt) {
        var charCode = ( evt.which ) ? evt.which : event.keyCode;
        return ( charCode >= 48 && charCode <= 57 || charCode == 8 );
    }
</script>
<!-- Akhir Script CRUD -->
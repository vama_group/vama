<?php
	if($access_create == '1' or $access_update == '1'){
		$readonly = '';
		$disabled = '';
	}else{		
		$readonly = 'readonly';
		$disabled = 'disabled';
	}
?>

<div class="content">
	<div class="container">
	<br>		
		<div class="row">
        	<div class="col-sm-12">
	        	<div class="row">
					<div class='col-sm-4'>
						<div class="panel panel-color panel-warning">
							<!-- Default panel contents -->
							<div class="panel-heading">
								<h3 class="panel-title">
									Retur Barang Dari Toko
								</h3>
								<select id="kode_toko" name="kode_toko" class="btn btn-rounded btn-xs btn-default">
                                    <option value="" class="text-dark ">PILIH TOKO</option>
                                    <?php foreach ($data_toko AS $dt){ ?>
                                        <option value="<?php echo $dt->kode_customer_pusat ?>" class="text-dark">
                                            <?php echo $dt->nama_customer_pusat ?>
                                        </option>
                                    <?php } ?>
                                </select>
							</div>

							<span>
								<input type='hidden' id="id_retur_pembelian_m" name="id_retur_pembelian_m" value="<?php echo $id_retur_pembelian_m ?>">
							</span>

							<table class='table table-condensed table-striped table-hover' id='Total'>
								<tbody>
									<tr>
										<td colspan="2">
											<div id="induk_pencarian_transaksi" class="input-group has-warning col-md-12 table-responsive">
                                                <input id='pencarian_transaksi' name="pencarian_transaksi" placeholder="MASUKAN NO. RETUR BARANG" class="form-control input-md text-dark" type="text" <?php echo $disabled ?> <?php echo $readonly ?> value="<?php echo $no_retur_pembelian ?>" autocomplete="off">
                                                
                                                <span class="input-group-btn">
                                                	<?php if($access_create == 1 or $access_update == 1){ ?>
	                                                	<button id="tombol_cari_transaksi" name="tombol_cari_transaksi" type="button" class="btn waves-effect waves-light btn-md btn-warning">
	                                                		<i id="icon_pencarian_transaksi" class="fa fa-search"></i>
	                                                	</button>
                                                	<?php }else{ ?>
	                                                	<button id="tombol_cari_transaksi" name="tombol_cari_transaksi" type="button" class="btn waves-effect waves-light btn-md btn-warning">
		                                            		<i id="icon_pencarian_transaksi" class="fa fa-ban"></i>
		                                            	</button>
	                                                <?php } ?>
                                                </span>
                                            </div>
                                            <div id='hasil_pencarian_transaksi' name="hasil_pencarian_transaksi"></div>
										</td>
									</tr>
									<tr>
										<td><small>Tanggal Retur</small></td>
										<td class="text-left"> 
											<span id='TanggalRetur' name="TanggalRetur" class="text-dark pull-right">0</span>
										</td>
									</tr>
									<tr>
										<td><small>Jumlah Barang (Retur)</small></td>
										<td class="text-left">Rp. 
											<span id='JumlahBarangRetur' name="JumlahBarangRetur" class="text-dark pull-right">0</span>
										</td>
									</tr>
									<tr>
										<td><small>Jumlah Barang (Masuk)</small></td>
										<td class="text-left">Rp. 
											<span id='JumlahBarangMasuk' name="JumlahBarangRetur" class="text-dark pull-right">0</span>
										</td>
									</tr>
									<tr>
										<td colspan="2">
											<!-- <textarea name='catatan' id='catatan' class='form-control input-sm' rows='2' placeholder="Catatan Transaksi (Jika Ada)" style='resize: vertical; width:100%;'></textarea> -->
											<small class="text-muted">Catatan : </small><br>
											<small id="catatan" class="text-danger"></small>
										</td>
									</tr>
									<tr>
										<td></td>
										<td>
											<?php if($access_create == 1 or $access_update == 1){ ?>
												<button type='button' class='btn btn-primary' id='Simpan'>
													<i class='fa fa-save'></i>
												</button>
											<?php } ?>

											<?php if($access_create == 1 or $access_update == 1){ ?>
												<button type='button' class='btn btn-danger' id='Tahan'>
													<i class='fa fa-hand-paper-o'></i>
												</button>
											<?php } ?>

											<?php if($access_laporan == 1){ ?>
												<button id='Laporan' type='button' class='btn btn-default' title="Laporan Transaksi">
													<i class='fa fa-file-text-o'></i>
												</button>
											<?php } ?>
										</td>										
									</tr>	
								</tbody>
							</table>
						</div>
					</div>
					
		            <div class="col-sm-8">
			        	<!-- Awal daftar transaksi dan catatan transaksi --> 
		            	<div class="card-box table-responsive">
		            		<!-- Awal induk tab -->
		                    <ul class="nav nav-tabs tabs-bordered" id="tab_list_penjualan">
		                        <li class="active">
		                            <a href="#tab_induk_retur" id="tab_induk" data-toggle="tab" aria-expanded="true">
		                                <span class="visible-xs"><i class="fa fa-check-square"></i></span>
		                                <span class="hidden-xs" id="judul_tab_induk">Data Retur Barang</span>
		                            </a>
		                        </li>
		                        <li class="">
		                            <a href="#tab_retur_batal" id="tab_batal" data-toggle="tab" aria-expanded="true">
		                                <span class="visible-xs"><i class="typcn typcn-home"></i></span>
		                                <span class="hidden-xs" id="judul_tab_batal">Data Retur Barang Batal</span>
		                            </a>
		                        </li>
		                    </ul>
		                    <!-- Akhir induk tab -->

		                    <!-- Awal isi tab -->
		                    <div class="tab-content">
								<!-- Awal daftar retur penjualan barang -->
		                        <div class="tab-pane active" id="tab_induk_retur">
									<table id='TableTransaksi' class="table table-condensed table-striped table-hover dt-responsive nowrap" cellspacing="0" width="100%">
										<thead class="input-sm">
											<tr class="text-dark">
												<th>#</th>
												<th>Tombol</th>
												<th>SKU</th>
												<th>Nama Barang</th>
												<th>Harga</th>
												<th>MS STK</th>
												<th>KDS BRG</th>
												<th>Jml RTR</th>
												<th>Jml MSK</th>
												<th>Keterangan Retur</th>
												<th>Pegawai Save</th>
												<th>Tanggal Save</th>
												<th>Pegawai Edit</th>
												<th>Tanggal Edit</th>
												<th>Pegawai Masuk</th>
												<th>Tanggal Masuk</th>
											</tr>
										</thead>
										<tbody class="input-sm text-dark"></tbody>
									</table>
								</div>
								<!-- Awal daftar retur penjualan barang -->

								<!-- Awal daftar retur penjualan barang batal -->
		                        <div class="tab-pane" id="tab_retur_batal">
									<table id='TableTransaksiBatal' class="table table-condensed table-striped table-hover dt-responsive nowrap" cellspacing="0" width="100%">
										<thead class="input-sm">
											<tr class="text-dark">
												<th>#</th>
												<th>SKU</th>
												<th>Nama Barang</th>
												<th>Harga</th>
												<th>Keterangan Batal</th>
												<th>MS STK</th>
												<th>KDS BRG</th>
												<th>Jml RTR</th>
												<th>Jml MSK</th>
												<th>keterangan Retur</th>
												<th>Pegawai</th>
												<th>Tanggal</th>
											</tr>
										</thead>
										<tbody class="input-sm text-dark"></tbody>
									</table>
								</div>
								<!-- Akhir daftar retur penjualan barang batal -->
							</div>

							<!-- Catatan transaksi dan keterangan shortcut -->
							<div class="row">
								<div class="col-sm-12">
									<p><i class='fa fa-keyboard-o fa-fw'></i> <b><small>Shortcut Keyboard : </small></b></p>
									<small>
										F7 = Fokus ke field catatan
										, F8 = Tahan Transaksi
										, F9 = Simpan Transaksi
									</small>
								</div>
							</div>
						</div>
				        <!-- Akhir daftar transaksi -->
		            </div>
				</div>
            </div>
        </div>
	</div>
</div>

<div class="modal" id="ModalGue" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class='fa fa-times-circle'></i></button>
				<h4 class="modal-title" id="ModalHeader"></h4>
			</div>
			<div class="modal-body" id="ModalContent"></div>
			<div class="modal-footer" id="ModalFooter"></div>
		</div>
	</div>
</div>

<script>
$('#ModalGue').on('hide.bs.modal', function () {
   setTimeout(function(){ 
        $('#ModalHeader, #ModalContent, #ModalFooter').html('');
   }, 500);
});
</script>

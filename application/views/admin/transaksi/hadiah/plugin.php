<!-- Awal CSS -->
<link href="assets/plugin/zircos/plugins/datatables/jquery.dataTables.min.css" rel="stylesheet" type="text/css"/>
<link href="assets/plugin/zircos/plugins/datatables/responsive.bootstrap.min.css" rel="stylesheet" type="text/css"/>
<link href="assets/plugin/zircos/plugins/datatables/dataTables.bootstrap.min.css" rel="stylesheet" type="text/css"/>
<link href="assets/plugin/zircos/material-design/assets/css/tambahan.css" rel="stylesheet" type="text/css"/>
<!-- Akhir CSS -->

<!-- Awal JS -->
<script src="assets/plugin/zircos/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="assets/plugin/zircos/plugins/datatables/dataTables.bootstrap.js"></script>
<script src="assets/plugin/zircos/material-design/assets/js/detect.js"></script>
<script src="assets/plugin/zircos/material-design/assets/js/jquery.slimscroll.js"></script>
<script src="assets/plugin/zircos/plugins/datatables/dataTables.responsive.min.js"></script>
<script src="assets/plugin/zircos/plugins/datatables/responsive.bootstrap.min.js"></script>
<script src="assets/plugin/zircos/material-design/assets/pages/jquery.datatables.init.js"></script>
<!-- Akhir JS -->

<!-- Awal Sweet-Alert  -->
<link href="assets/plugin/zircos/plugins/bootstrap-sweetalert/sweet-alert.css" rel="stylesheet" type="text/css">
<script src="assets/plugin/zircos/plugins/bootstrap-sweetalert/sweet-alert.min.js"></script>
<!-- Akhir Sweet-Alert  -->

<script>
    var url               = "hadiah";
    var table_induk       = $('#TableTransaksi').DataTable();
    var table_batal       = $('#TableTransaksiBatal').DataTable();
    var table_cari_barang = $('#TableCariBarang').DataTable();

    $(document).ready(function(){
        $('#navbar').removeClass('navbar-default');
        $('#navbar').addClass('navbar-pink');
        $('#JudulHalaman').html('Hadiah - VAMA');

        $("#nama_hadiah").focus();
        $("#wrapper").removeClass('forced');
        $("#wrapper").addClass('forced enlarged');

        var id_hadiah_m  = $('#id_hadiah_m').val(); 
        ambil_data(id_hadiah_m);
    });

    var delay = (function(){
        var timer = 0;
        return function (callback, ms) {
            clearTimeout(timer);
            timer = setTimeout(callback, ms);
        };
    })();

    function ambil_data(id_hadiah_m)
    {
        if(id_hadiah_m !=='0'){
            $.ajax({
                url : "<?php echo site_url('hadiah/ambil_data')?>",
                type: "POST",
                cache: false,
                data : 'id_hadiah_m=' + id_hadiah_m,
                dataType:'JSON',
                success: function(data){
                    $('#judul_tab_induk').html("Data Hadiah : <span class='badge up bg-primary'>" + 
                                                data.jumlah_barang + 
                                                " Barang </span>");
                    $('#judul_tab_batal').html("Data Hadiah Batal : <span class='badge up bg-danger'>" + 
                                                data.jumlah_barang_batal +
                                                " Barang </span>");

                    url = "hadiah/transaksi/?&id=" + id_hadiah_m;
                    $('#no_hadiah').html(data.no_hadiah);
                    $('#nama_hadiah').val(data.nama_hadiah);
                    $('#pencarian_customer').val(data.nama_customer_pusat);
                    $('#id_customer_hidden').html(data.id_customer_pusat);
                    $('#data_handphone_customer_pusat').html(data.handphone1);
                    $('#data_tipe_customer_pusat').html(data.tipe_customer_pusat);
                    $('#jumlah_barang').html(data.jumlah_barang);
                    $('#TotalBayar').html(to_rupiah(data.total));
                    $('#TotalBayarHidden').val(data.total);
                    $('#catatan').val(data.keterangan_lain);
                    tampilkan_data();
                    setTimeout(function(){ 
                        $('#pencarian_kode').focus();
                    }, 10);  
                },
                error : function(data){ 
                    url = 'hadiah';
                    swal({
                        title: "Oops !", 
                        text: "Data hadiah gagal ditampilkan.", 
                        type: "error", 
                        confirmButtonClass: 'btn-danger btn-md waves-effect waves-light',
                        confirmButtonText: "Ok"
                    },function (isConfirm){
                        window.location.href = url;
                    });
                }
            });
        } 
    }

    function tampilkan_data(){
        table_induk.clear();table_induk.destroy();
        table_induk=$('#TableTransaksi').DataTable({
            ordering: true,
            bFilter:true,
            processing:true,
            serverSide:true,
            order:[],
            lengthMenu: [[5, 10, 25, 50, -1], [5, 10, 25, 50, "All"]],
            pagingType: "full",

            oLanguage:{
                sLoadingRecords:'Silakan Tunggu',
                sProcessing:"<img src='assets/loader/cubeloader2.2.gif' width='50' height='50'> Harap tunggu",
                sZeroRecords:"Belum ada data untuk ditampilkan",
                sSearch : "Pencarian data"
            },
            ajax:{
                url: "<?php echo site_url('hadiah/ajax_list')?>",
                type: "POST",
                data: {'id_hadiah_m' : $('#id_hadiah_m').val()}
            },
            columnDefs: [{ 
                            "targets": [ -1 ],
                            "orderable": false,
                        },],
            colVis: {
                "buttonText": "Change columns"
            }
        });
    }

    function tampilkan_data_batal(){
        table_batal.clear();table_batal.destroy();
        table_batal=$('#TableTransaksiBatal').DataTable({
            ordering: true,
            bFilter:true,
            processing:true,
            serverSide:true,
            order:[],
            lengthMenu: [[5, 10, 25, 50, -1], [5, 10, 25, 50, "All"]],
            pagingType: "full",

            oLanguage:{
                sLoadingRecords:'Silakan Tunggu',
                sProcessing:"<img src='assets/loader/cubeloader2.2.gif' width='50' height='50'> Harap tunggu",
                sZeroRecords:"Belum ada data untuk ditampilkan",
                sSearch : "Pencarian data"
            },
            ajax:{
                url: "<?php echo site_url('hadiah/ajax_list_batal')?>",
                type: "POST",
                data: {'id_hadiah_m' : $('#id_hadiah_m').val()}
            },
            columnDefs: [{ 
                            "targets": [ -1 ],
                            "orderable": false,
                        },],
            colVis: {
                "buttonText": "Change columns"
            }
        });
    }

    function tampilkan_data_barang(){
        $('#header_cari_barang').html(
            '<th>#</th>' +
            '<th>SKU</th>' +
            '<th style="width:800px;">Nama Barang</th>' +
            '<th>Harga Eceran</th>' +
            '<th>Qty Pusat</th>' +
            '<th>Tombol</th>'
        );

        table_cari_barang.clear();table_cari_barang.destroy();
        table_cari_barang=$('#TableCariBarang').DataTable(
        {
            ordering: true,
            bFilter:true,
            processing:true,
            serverSide:true,
            order:[],
            lengthMenu: [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
            pagingType: "full",

            oLanguage:{
                sLoadingRecords:'Silakan Tunggu',
                sProcessing:"<img src='assets/loader/cubeloader2.2.gif' width='50' height='50'> Harap tunggu",
                sZeroRecords:"Belum ada data barang untuk ditampilkan",
                sSearch : "Pencarian data"
            },
            ajax:{
                url: "<?php echo site_url('hadiah/ajax_list_barang')?>",
                type: "POST",
                data: {'id_hadiah_m' : $('#id_hadiah_m').val()}
            },
            columnDefs: [
                            { 
                                "targets": [ -1 ],
                                "orderable": false,
                            },
                        ],
        });
    }

    $(document).on('click', '#tab_induk', function(e){
        tampilkan_data();
    });

    $(document).on('click', '#tab_batal', function(e){
        tampilkan_data_batal();
    });

    function reload_table_induk()
    {
        table_induk.ajax.reload();
    }

    function reload_table_batal()
    {
        table_batal.ajax.reload();
    }

    function konfirmasi_simpan_detail(){
        var pesan_kesalahan  = ""; 
        var status_kesalahan = ""; 

        if($('#nama_hadiah').val() == ''){
            pesan_kesalahan += "Harap masukan nama hadiah.";
            status_kesalahan = '1';
        }

        if($('#jumlah_hadiah').val() == '0' || $('#jumlah_hadiah').val() == ''){
            pesan_kesalahan += "\n Harap masukan jumlah hadiah.";
            status_kesalahan = '1';
        }

        if(status_kesalahan == 1){
            swal({
                title: "Oops!",
                text: pesan_kesalahan,
                type: "error",
                confirmButtonClass: 'btn-danger btn-md waves-effect waves-light',
                confirmButtonText: "Iya",
            }, function (isConfirm){
                if(isConfirm){
                    if($('#nama_hadiah').val() == ''){
                        setTimeout(function(){ 
                            $('#nama_hadiah').focus();
                        }, 10);
                        return;
                    }else if($('#jumlah_hadiah').val() == ''){
                        setTimeout(function(){ 
                            $('#jumlah_hadiah').focus();
                        }, 10);
                        return;
                    }
                } 
            });
            return;
        }

        var pesan_simpan = "Apakah anda yakin ingin menyimpan hadiah barang"; 
            pesan_simpan += "\n SKU : "+ $('#pencarian_kode').val();
            pesan_simpan += "\n Nama Barang : "+ $('#nama_barang').html();
            pesan_simpan += "\n Jumlah Hadiah : "+$('#jumlah_hadiah').val();

        $.ajax({
            url: "<?php echo site_url('hadiah/cek_stok'); ?>",
            type: "POST",
            cache: false,
            data: 'id_barang_pusat=' + $('#id_barang_pusat').html() +
                  '&jumlah_hadiah=' + $('#jumlah_hadiah').val() +
                  '&id_hadiah_m=' + $('#id_hadiah_m').val(),
            dataType:'json',
            success: function(data){
                if(data.status == 1){
                    swal({
                        title: "Konfirmasi simpan hadiah ?",
                        text: pesan_simpan,
                        type: "info",
                        showCancelButton: true,
                        confirmButtonClass: 'btn-primary btn-md waves-effect waves-light',
                        confirmButtonText: "Iya",
                        cancelButtonText: "Batal",
                    }, function (isConfirm){
                        if(isConfirm){
                            setTimeout(function(){ 
                                simpan_detail();
                            }, 10);
                            return;
                        } 
                    });
                }else if(data.status == 0){
                    swal({
                        title: 'Oops!', 
                        text: data.pesan, 
                        type: "error", 
                        confirmButtonClass: 'btn-danger btn-md waves-effect waves-light',
                        confirmButtonText: "Ok"
                    }, function (isConfirm){
                        if(data.arahkan == 'jumlah_hadiah'){
                            setTimeout(function(){ 
                                $('#jumlah_hadiah').focus();
                            }, 10);
                        }else if(data.arahkan == 'halaman_utama'){
                            window.location.href = url;
                        }
                    });
                }
            }
        });
    }

    $(document).on('click', 'button#SimpanDetail', function(){
        konfirmasi_simpan_detail();
    });

    $(document).on('click', '#TambahCustomer', function(e){
        e.preventDefault();
        $('.modal-dialog').removeClass('modal-sm');
        $('.modal-dialog').removeClass('modal-lg');
        $('.modal-dialog').addClass('modal-lg');
        $('#ModalHeader').html('Tambah Customer Pusat');
        $('#ModalContent').load($(this).attr('href'));
        $('#ModalGue').modal('show');
    });

    $(document).on('click', '#daftar-autocomplete li', function(){
        // $(this).parent().parent().parent().find('input').val($(this).find('span#skunya').html());

        var id_barang_pusat = $(this).find('span#id_barang_pusat').html();
        var SKU             = $(this).find('span#skunya').html();
        var NamaBarang      = $(this).find('span#barangnya').html();
        var TipeCustomer    = $('span#data_tipe_customer_pusat').html();
        var JenisHarga      = 'ECERAN';
        var Harganya        = $(this).find('span#eceran').html();

        $('#harga_satuan_tampil').prop('readonly', true).val('');
        $('#harga_satuan_tampil').removeAttr('disabled').val();

        $('div#hasil_pencarian').hide();
        $('#id_barang_pusat').html(id_barang_pusat);
        $('#pencarian_kode').val(SKU);
        $('#nama_barang').html(NamaBarang);
        $('#harga_satuan').val(Harganya);
        $('#harga_satuan_tampil').val(to_rupiah(Harganya));
        $('#sub_total').val(Harganya);
        $('#sub_total_tampil').html(to_rupiah(Harganya));
        $('#SimpanDetail').removeAttr('disabled');

        if(Harganya > 0){
            $('#jumlah_hadiah').removeAttr('readonly').val();
            $('#jumlah_hadiah').removeAttr('disabled').val();
            $('#discount').removeAttr('readonly').val('0.00');
            $('#discount').removeAttr('disabled').val('0.00');
            $('#jumlah_hadiah').focus();
        }
    });

    $(document).on('click', '#daftar_autocompleted_customer li', function(){    
        var id_customer   = $(this).find('span#id_customer').html();
        var nama_customer = $(this).find('span#nama_customer').html();
        var tipe_customer = $(this).find('span#tipe_customer').html();
        var hp_customer   = $(this).find('span#hp_customer').html();
        var kode_customer = $(this).find('span#kode_customer').html();
        
        $('#hasil_pencarian_customer').hide();
        $('#id_customer_hidden').html(id_customer);
        $('#kode_customer_pusat').val(kode_customer);
        $('#pencarian_customer').val(nama_customer);
        $('#data_tipe_customer_pusat').html(tipe_customer);
        $('#data_handphone_customer_pusat').html(hp_customer);
        $('#pencarian_kode').focus();
    });

    $(document).on('click', '#tombol_scan', function(e){
        tampilkan_data_barang();
        $('.modal-dialog').removeClass('modal-sm');
        $('.modal-dialog').removeClass('modal-md');
        $('.modal-dialog').addClass('modal-lg');
        $('#ModalHeaderCariBarang').html('CARI BARANG UNTUK : <b class="text-primary">' +
                                         $('#nama_hadiah').val() +
                                         '</b>');
        $('#ModalCariBarang').modal('show');
        return false;
    });

    function proses_barang($sku){
        $.ajax({
            url: "<?php echo site_url('hadiah/ajax_cari_barang_hadiah'); ?>",
            type: "POST",
            cache: false,
            data: 'sku=' + $sku,
            dataType:'json',
            success: function(json){
                if(json.status == 2){
                    swal({
                        title: 'Oops!', 
                        text: json.pesan, 
                        type: 'error', 
                        confirmButtonClass: 'btn-danger btn-md waves-effect waves-light',
                        confirmButtonText: "Ok"
                    }, function(isConfirm){
                        window.location.href = url;
                    });
                    
                }else if(json.status == 1){
                    $('div#hasil_pencarian').hide();
                    $('#id_barang_pusat').html(json.data['id_barang_pusat']);
                    $('#pencarian_kode').val(json.data['sku']);
                    $('#nama_barang').html(json.data['nama_barang']);
                    $('#harga_satuan').val(json.data['harga_satuan']);
                    $('#harga_satuan_tampil').val(to_rupiah(json.data['harga_satuan']));
                    $('#sub_total').val(json.data['harga_satuan']);
                    $('#sub_total_tampil').html(to_rupiah(json.data['harga_satuan']));

                    if($('#harga_satuan').val() >= 0){
                        $('#jumlah_hadiah').removeAttr('readonly').val();
                        $('#jumlah_hadiah').removeAttr('disabled').val();
                        $('#SimpanDetail').removeAttr('disabled').val();
                        $('#ModalCariBarang').modal('hide');
                        setTimeout(function(){
                            $('#jumlah_hadiah').val('').focus();
                        }, 100);
                    }else{
                       swal({
                            title: 'Oops!', 
                            text: 'Harga eceran belum di tentukan, harap hubungin administrator.', 
                            type: 'error', 
                            confirmButtonClass: 'btn-danger btn-md waves-effect waves-light',
                            confirmButtonText: "Ok"
                        }, function(isConfirm){
                            window.location.href = url;
                        }); 
                    }

                }else if(json.status == 0){
                    $('#hasil_pencarian').hide();
                    $('#id_barang_pusat').html('');            
                    $('#pencarian_kode').val('');
                    $('#nama_barang_perakitan').html('Belum ada barang yg dipilih');
                    $('#jumlah_hadiah').val('');
                    $('#harga_satuan').val('0');
                    $('#harga_satuan_tampil').val('Rp. 0');
                    $('#sub_total').val($('#harga_satuan').val());
                    $('#sub_total_tampil').html($('#harga_satuan_tampil').val());
                    $('#SimpanDetail').prop('disabled', true);

                    swal({
                        title: 'Oops!', 
                        text: json.pesan, 
                        type: 'error', 
                        confirmButtonClass: 'btn-danger btn-md waves-effect waves-light',
                        confirmButtonText: "Ok"
                    }, function(isConfirm){
                        setTimeout(function(){ 
                            $('#pencarian_kode').focus();
                        }, 10);
                    });
                }
            }
        });
    }

    $(document).on('keyup', '#pencarian_kode', function(e){
        if($(this).val() !== ''){
            var charCode = e.which || e.keyCode;
            if(charCode == 40){
                if($('div#hasil_pencarian li.autocomplete_active').length > 0){
                    var Selanjutnya = $('div#hasil_pencarian li.autocomplete_active').next();
                    $('div#hasil_pencarian li.autocomplete_active').removeClass('autocomplete_active');

                    Selanjutnya.addClass('autocomplete_active');
                }else{
                    $('div#hasil_pencarian li:first').addClass('autocomplete_active');
                }
            }else if(charCode == 38){
                if($('div#hasil_pencarian li.autocomplete_active').length > 0){
                    var Sebelumnya = $('div#hasil_pencarian li.autocomplete_active').prev();
                    $('div#hasil_pencarian li.autocomplete_active').removeClass('autocomplete_active');
                
                    Sebelumnya.addClass('autocomplete_active');
                }else{
                    $('div#hasil_pencarian li:first').addClass('autocomplete_active');
                }
            }else if(charCode == 13){
                var SKU = $(this).val();

                $('#pencarian_kode').val(SKU);
                $('div#hasil_pencarian').hide();
                $.ajax({
                    url: "<?php echo site_url('hadiah/ajax_cari_barang_hadiah'); ?>",
                    type: "POST",
                    cache: false,
                    data: 'sku=' + SKU,
                    dataType:'json',
                    success: function(json){
                        if(json.status == 2){
                            swal({
                                title: 'Oops!', 
                                text: json.pesan, 
                                type: 'error', 
                                confirmButtonClass: 'btn-danger btn-md waves-effect waves-light',
                                confirmButtonText: "Ok"
                            }, function(isConfirm){
                                window.location.href = url;
                            });
                            
                        }else if(json.status == 1){
                            $('div#hasil_pencarian').hide();
                            $('#id_barang_pusat').html(json.data['id_barang_pusat']);
                            $('#nama_barang').html(json.data['nama_barang']);
                            $('#harga_satuan').val(json.data['harga_satuan']);
                            $('#harga_satuan_tampil').val(to_rupiah(json.data['harga_satuan']));
                            $('#sub_total').val(json.data['harga_satuan']);
                            $('#sub_total_tampil').html(to_rupiah(json.data['harga_satuan']));

                            if($('#harga_satuan').val() >= 0){
                                $('#jumlah_hadiah').removeAttr('readonly').val();
                                $('#jumlah_hadiah').removeAttr('disabled').val();
                                $('#SimpanDetail').removeAttr('disabled').val();
                                $('#jumlah_hadiah').focus();
                            }

                        }else if(json.status == 0){
                            $('#hasil_pencarian').hide();
                            $('#id_barang_pusat').html('');            
                            $('#pencarian_kode').val('');
                            $('#nama_barang_perakitan').html('Belum ada barang yg dipilih');
                            $('#jumlah_hadiah').val('');
                            $('#harga_satuan').val('0');
                            $('#harga_satuan_tampil').val('Rp. 0');
                            $('#sub_total').val($('#harga_satuan').val());
                            $('#sub_total_tampil').html($('#harga_satuan_tampil').val());
                            $('#SimpanDetail').prop('disabled', true);

                            swal({
                                title: 'Oops!', 
                                text: json.pesan, 
                                type: 'error', 
                                confirmButtonClass: 'btn-danger btn-md waves-effect waves-light',
                                confirmButtonText: "Ok"
                            }, function(isConfirm){
                                setTimeout(function(){ 
                                    $('#pencarian_kode').focus();
                                }, 10);
                            });
                        }
                    }
                });
            }else{
                AutoCompleteGue($(this).val());
            }
        }else{
            // Bersihkan data pemilihan barang
            $('#id_barang_pusat').html('');
            $('#nama_barang').html('Belum ada barang yg dipilih');
            $('#harga_satuan').val('0');
            $('#harga_satuan_tampil').val('Rp. 0');
            $('#jumlah_hadiah').prop('disabled', true).val('');
            $('#sub_total').val('0');
            $('#sub_total_tampil').html('Rp. 0');
            $('#SimpanDetail').prop('disabled', true);
            $('div#hasil_pencarian').hide();
        }
    });

    $(document).on('keyup', '#pencarian_customer', function(e){
        if($(this).val() !== ''){
            var charCode = e.which || e.keyCode;
            if(charCode == 40){
                if($('div#hasil_pencarian_customer li.autocomplete_active-customer').length > 0){
                    var Selanjutnya = $('div#hasil_pencarian_customer li.autocomplete_active-customer').next();
                    $('div#hasil_pencarian_customer li.autocomplete_active-customer').removeClass('autocomplete_active-customer');
                    Selanjutnya.addClass('autocomplete_active-customer');
                }else{
                    $('div#hasil_pencarian_customer li:first').addClass('autocomplete_active-customer');
                }
            }else if(charCode == 38){
                if($('div#hasil_pencarian_customer li.autocomplete_active-customer').length > 0){
                    var Sebelumnya = $('div#hasil_pencarian_customer li.autocomplete_active-customer').prev();
                    $('div#hasil_pencarian_customer li.autocomplete_active-customer').removeClass('autocomplete_active-customer');
                    Sebelumnya.addClass('autocomplete_active-customer');
                }else{
                    $('div#hasil_pencarian_customer li:first').addClass('autocomplete_active-customer');
                }
            }else if(charCode == 13){
                var id_customer     = $('div#hasil_pencarian_customer li.autocomplete_active-customer span#id_customer').html();
                var nama_customer   = $('div#hasil_pencarian_customer li.autocomplete_active-customer span#nama_customer').html();
                var tipe_customer   = $('div#hasil_pencarian_customer li.autocomplete_active-customer span#tipe_customer').html();
                var hp_customer     = $('div#hasil_pencarian_customer li.autocomplete_active-customer span#hp_customer').html();
                var kode_customer   = $('div#hasil_pencarian_customer li.autocomplete_active-customer span#kode_customer').html();
        
                $('#hasil_pencarian_customer').hide();
                $('#id_customer_hidden').html(id_customer);
                $('#kode_customer_pusat').val(kode_customer);            
                $('#pencarian_customer').val(nama_customer);
                $('#data_tipe_customer_pusat').html(tipe_customer);
                $('#data_handphone_customer_pusat').html(hp_customer);
                $('#pencarian_kode').focus();
            }else{
                AutoCompletedCustomer($(this).width(), $(this).val(), $(this).parent().parent().index());
            }
        }else{
            // Bersihkan data customer yang di pilih
            $('#id_customer_hidden').html('');
            $('#kode_customer_pusat').val('');            
            $('#data_tipe_customer_pusat').html('-');
            $('#data_handphone_customer_pusat').html('-');
            $('div#hasil_pencarian_customer').hide();

            // Bersihkan data pemilihan barang
            $('#pencarian_kode').val('');
            $('#nama_barang').html('Belum ada barang yg dipilih');
            $('#harga_satuan').val('0');
            $('#harga_satuan_tampil').val('Rp. 0');
            $('#jumlah_hadiah').val('');
            $('#discount').val('0.00');
            $('#discount_hidden').val('0');
            $('#sub_total').val('0');
            $('#sub_total_tampil').html('Rp. 0');

            $('#jumlah_hadiah').prop('readonly', true).val();
            $('#jumlah_hadiah').prop('disabled', true).val();
            $('#discount').prop('readonly', true).val();
            $('#discount').prop('disabled', true).val();
            $('#harga_satuan_tampil').prop('readonly', true).val();
            $('#harga_satuan_tampil').prop('disabled', true).val();
        }
    });

    $(document).on('keyup', '#jumlah_hadiah', function(){
        HitungSubtotalBarang();
    });

    $(document).on('keydown', '#jumlah_hadiah', function(e){
        var charCode = e.which || e.keyCode;
        if(charCode == 9){
            $('#SimpanDetail').focus();
            return false;   
        }
        
        if(charCode == 13){
            konfirmasi_simpan_detail();
        }
    });

    $(document).on('click', 'button#info_stok', function(){
        $('#ModalGue').modal('hide');
        $('#jumlah_hadiah').focus();
    });

    $(document).on('keydown', 'body', function(e){
        var charCode = ( e.which ) ? e.which : event.keyCode;

        if(charCode == 117) //F6
        {
            tampilkan_data_barang();
            $('.modal-dialog').removeClass('modal-sm');
            $('.modal-dialog').removeClass('modal-md');
            $('.modal-dialog').addClass('modal-lg');
            $('#ModalHeaderCariBarang').html('CARI BARANG UNTUK HADIAH : <b class="text-primary">' + 
                                             $('#nama_hadiah').val()) +
                                             '</b>';        
            $('#ModalCariBarang').modal('show');
            return false;
        }

        if(charCode == 118) //F7
        {
            $('#catatan').focus();
            return false;
        }

        if(charCode == 119) //F8
        {
            konfirmasi_tahan_transaksi();
            return false;
        }

        if(charCode == 120) //F9
        {
            konfirmasi_simpan_transaksi();
            return false;
        }
    });

    $(document).on('click', 'body', function(){
        $('div#hasil_pencarian').hide();
        $('div#hasil_pencarian_customer').hide();
        $('div#hasil_pencarian_harga').hide();
    });

    function konfirmasi_tahan_transaksi(){
        if($('#nama_hadiah').val() == ''){
            swal({
                title: "Oops !", 
                text: "Harap masukan nama hadiah terlebih dahulu", 
                type: "error", 
                confirmButtonClass: 'btn-danger btn-md waves-effect waves-light',
                confirmButtonText: "Ok"
            },function (isConfirm){
                if(isConfirm){
                    setTimeout(function(){ 
                        $('#nama_hadiah').focus();
                    }, 100);
                } 
            });
            return;
        }

        if($('#jumlah_barang').html() > 0){
            swal({
                title: "Apakah anda yakin ingin menahan hadiah barang ini ?",
                type: "info",
                showCancelButton: true,
                confirmButtonClass: 'btn-primary btn-md waves-effect waves-light',
                confirmButtonText: "Iya",
                cancelButtonText: "Batal",
            },function (isConfirm){
                if(isConfirm){
                    setTimeout(function(){ 
                        TahanTransaksi();
                    }, 100);
                } 
            });
        }else{
            swal({
                title: "Oops !", 
                text: "Belum ada barang yang anda simpan ditransaksi ini, harap simpan barang terlebih dahulu", 
                type: "error", 
                confirmButtonClass: 'btn-danger btn-md waves-effect waves-light',
                confirmButtonText: "Ok"
            },function (isConfirm){
                if(isConfirm){
                    setTimeout(function(){ 
                        $('#pencarian_kode').focus();
                    }, 100);
                } 
            });
        }
    }

    $(document).on('click', 'button#Tahan', function(){
        konfirmasi_tahan_transaksi();
    });

    function konfirmasi_simpan_transaksi(){
        if($('#nama_hadiah').val() == ''){
            swal({
                title: "Oops !", 
                text: "Harap masukan nama hadiah terlebih dahulu", 
                type: "error", 
                confirmButtonClass: 'btn-danger btn-md waves-effect waves-light',
                confirmButtonText: "Ok"
            },function (isConfirm){
                if(isConfirm){
                    setTimeout(function(){ 
                        $('#nama_hadiah').focus();
                    }, 100);
                } 
            });
            return;
        }

        if($('#jumlah_barang').html() > 0){
            swal({
                title: "Apakah anda yakin ingin memyimpan hadiah barang ini ?",
                type: "info",
                showCancelButton: true,
                confirmButtonClass: 'btn-primary btn-md waves-effect waves-light',
                confirmButtonText: "Iya",
                cancelButtonText: "Batal",
            },function (isConfirm){
                if(isConfirm){
                    setTimeout(function(){ 
                        SimpanTransaksi();
                    }, 100);
                } 
            });
        }else{
            swal({
                title: "Oops !", 
                text: "Belum ada barang yang anda simpan ditransaksi ini, harap simpan barang terlebih dahulu", 
                type: "error", 
                confirmButtonClass: 'btn-danger btn-md waves-effect waves-light',
                confirmButtonText: "Ok"
            },function (isConfirm){
                if(isConfirm){
                    setTimeout(function(){ 
                        $('#pencarian_kode').focus();
                    }, 100);
                } 
            });
        }
    }

    $(document).on('click', 'button#Simpan', function(){
        konfirmasi_simpan_transaksi();
    });

    $(document).on('click', 'button#Laporan', function(){
        window.open("laporan_hadiah",'_blank');
    });

    $(document).on('click', 'button#IyaCetak', function(){
        CetakStruk();
        window.location.href="hadiah";
    });

    $(document).on('click', 'button#TidakCetak', function(){
        window.location.href="hadiah";
    });

    $(document).on('click', 'button#CetakStruk', function(){
        CetakStruk();
        return false;
    });

    function simpan_detail()
    {
        var FormData = "id_hadiah_m="+$('#id_hadiah_m').val(); 
            FormData += "&id_barang_pusat="+$('#id_barang_pusat').html();
            FormData += "&nama_hadiah="+$('#nama_hadiah').val();
            FormData += "&id_customer_pusat="+$('#id_customer_hidden').html();
            FormData += "&catatan="+encodeURI($('#catatan').val());
            FormData += "&harga="+$('#harga_satuan').val();
            FormData += "&jumlah_hadiah="+$('#jumlah_hadiah').val();
            FormData += "&subtotal="+$('#sub_total').val();
            FormData += "&sku="+encodeURI($('#pencarian_kode').val());

        $.ajax({
            url: "<?php echo site_url('hadiah/simpan_hadiah_detail'); ?>",
            type: "POST",
            cache: false,
            data: FormData,
            dataType:'json',
            success: function(data){
                if(data.status == 1){   
                    swal({
                        title: "Berhasil!", 
                        text: "Data barang hadiah berhasil di simpan.", 
                        type: "success", 
                        confirmButtonText: "Ok",
                    }, function(isConfirm){
                        if(isConfirm){
                            if(url == 'hadiah'){
                                window.location.href = data.url;
                            }else{
                                tampilkan_data();
                                setTimeout(function(){ 
                                    $('#pencarian_kode').focus();
                                }, 10);  
                            }    
                        }
                    });

                    $('#judul_tab_induk').html("Data Hadiah : <span class='badge up bg-primary'>" + 
                                                data.jumlah_barang + 
                                                " Barang </span>");
                    $('#judul_tab_batal').html("Data Hadiah Batal : <span class='badge up bg-danger'>" + 
                                                data.jumlah_barang_batal +
                                                " Barang </span>");

                    // Tampilkan id hadiah master dan total bayar dll
                    $('#id_hadiah_m').val(data.id_hm);
                    $('#TotalBayar').html(to_rupiah(data.total));
                    $('#TotalBayarHidden').val(data.total);
                    $('#jumlah_barang').html(data.jumlah_barang);

                    // Bersihkan data pemilihan barang
                    $('#id_barang_pusat').html('');
                    $('#pencarian_kode').val('');
                    $('#nama_barang').html('Belum ada barang yg dipilih');
                    $('#harga_satuan').val('');
                    $('#harga_satuan_tampil').val('Rp. 0');
                    $('#jumlah_hadiah').prop('disabled', true).val('');
                    $('#sub_total').val('');
                    $('#sub_total_tampil').html('Rp. 0');
                    $('#SimpanDetail').prop('disabled', true);

                }else if(data.status == 0){
                    swal({
                        title: 'Oops!', 
                        text: data.pesan, 
                        type: 'error', 
                        confirmButtonClass: 'btn-danger btn-md waves-effect waves-light',
                        confirmButtonText: "Ok"
                    }, function(isConfirm){
                        window.location.href = url;
                    });
                }
            }
        });
    }

    function edit_hadiah_detail(id_hadiah_d)
    {
        $.ajax({
            url : "<?php echo site_url('hadiah/ajax_edit')?>",
            type: "POST",
            cache: false, 
            data: 'id_hadiah_d=' + id_hadiah_d,
            dataType: "JSON",
            success: function(json){
                if(json.status == 1){
                    $('#id_barang_pusat').html(json.data['id_barang_pusat']);
                    $('#pencarian_kode').val(json.data['sku']);
                    $('#nama_barang').html(json.data['nama_barang']);
                    $('#harga_satuan').val(json.data['harga_satuan']);
                    $('#harga_satuan_tampil').val(to_rupiah(json.data['harga_satuan']));
                    $('#jumlah_hadiah').removeAttr('disabled').val();
                    $('#jumlah_hadiah').val(json.data['jumlah_hadiah']);
                    $('#sub_total').val(json.data['subtotal']);
                    $('#sub_total_tampil').html(to_rupiah(json.data['subtotal']));
                    $('#SimpanDetail').removeAttr('disabled', true);
                    $('#jumlah_hadiah').focus();
                }else{
                   swal({
                        title: "Oops!", 
                        text: json.pesan, 
                        type: "error", 
                        confirmButtonClass: 'btn-danger btn-md waves-effect waves-light',
                        confirmButtonText: "Ok"
                    },function (isConfirm){
                        window.location.href=url;
                    }); 
                }
            },
            error: function (jqXHR, textStatus, errorThrown){
                swal({
                    title: "Gagal!", 
                    text: "Data barang hadiah gagal di tampilkan.", 
                    type: "error", 
                    confirmButtonClass: 'btn-danger btn-md waves-effect waves-light',
                    confirmButtonText: "Ok"
                },function (isConfirm){
                    window.location.href=url;
                });
            }
        });
    }

    function verifikasi_hapus_hadiah_detail(id_hadiah_d)
    {
         $.ajax({
            url : "<?php echo site_url('hadiah/ajax_verifikasi_hapus_detail'); ?>",
            type: "POST",
            cache: false,
            data: "id_hadiah_d=" + id_hadiah_d,
            dataType: "JSON",
            success: function(data){
                if(data.status == 1){
                    $('.modal-dialog').removeClass('modal-lg');
                    $('.modal-dialog').addClass('modal-sm');
                    $('#ModalHeader').html('Informasi Hapus Barang');
                    $('#ModalContent').html(data.pesan);
                    $('#ModalFooter').html(data.footer);
                    $('#ModalGue').modal('show');
                }else{
                    swal({
                        title: "Oops!", 
                        text: data.pesan, 
                        type: "error", 
                        confirmButtonClass: 'btn-danger btn-md waves-effect waves-light',
                        confirmButtonText: "Ok"
                    },function (isConfirm){
                        window.location.href=url;
                    });
                }
            }, 
            error: function (jqXHR, textStatus, errorThrown){
                swal({
                    title: "Gagal!", 
                    text: "Gagal mengambil data barang hadiah.", 
                    type: "error", 
                    confirmButtonClass: 'btn-danger btn-md waves-effect waves-light',
                    confirmButtonText: "Ok"
                },function (isConfirm){
                    window.location.href=url;
                });
            }
        });
    }

    function hapus_hadiah_detail(id_hadiah_d)
    {
        var FormData = "id_hadiah_d="+ id_hadiah_d;
            FormData += "&nama_hadiah="+encodeURI($('#nama_hadiah').val());
            FormData += "&id_customer_pusat="+$('#id_customer_hidden').html();
            FormData += "&catatan="+encodeURI($('#catatan').val());
            FormData += "&keterangan_batal="+encodeURI($('#keterangan_batal').val());

        $.ajax({
            url : "<?php echo site_url('hadiah/ajax_hapus_detail')?>/"+id_hadiah_d,
            type: "POST",
            data: FormData,
            dataType: "JSON",
            success: function(data){
                if(data.status == 1){
                    $('#judul_tab_induk').html("Data Hadiah : <span class='badge up bg-primary'>" +
                                                data.jumlah_barang +
                                               " Barang </span>");
                    $('#judul_tab_batal').html("Data Hadiah Batal : <span class='badge up bg-danger'>" +
                                                data.jumlah_barang_batal +
                                               " Barang </span>");

                    $('#TotalBayar').html(to_rupiah(data.total));
                    $('#TotalBayarHidden').val(data.total);
                    $('#jumlah_barang').html(data.jumlah_barang);                
                    $('#UangKembali').val('0');

                    swal({
                        title: 'Berhasil!',
                        text: data.pesan,
                        type: 'success',
                        confirmButtonText: 'Ok'
                    },function (isConfirm){
                        if(data.jumlah_barang == 0){
                            window.location.href = 'hadiah';
                        }else{
                            reload_table_induk();
                        }
                    });
                }else if(data.status == 0){
                    swal({
                        title: 'Oops!',
                        text: data.pesan,
                        type: 'error',
                        confirmButtonClass: 'btn-danger btn-md waves-effect waves-light',
                        confirmButtonText: 'Ok'
                    },function (isConfirm){
                        window.location.href = url;
                    });
                }
            },
            error: function (jqXHR, textStatus, errorThrown){
                swal({
                    title: 'Gagal!',
                    text: 'Hadiah barang gagal dihapus.',
                    type: 'error',
                    confirmButtonClass: 'btn-danger btn-md waves-effect waves-light',
                    confirmButtonText: 'Ok'
                },function (isConfirm){
                    window.location.href = url;
                });
            }
        });
    }

    function SimpanTransaksi()
    {
       var FormData = "id_hadiah_m="+$('#id_hadiah_m').val(); 
            FormData += "&nama_hadiah="+$('#nama_hadiah').val();
            FormData += "&id_customer_pusat="+$('#id_customer_hidden').html();
            FormData += "&catatan="+encodeURI($('#catatan').val());
            FormData += "&total="+$('#TotalBayarHidden').val();

        $.ajax({
            url: "<?php echo site_url('hadiah/simpan_transaksi'); ?>",
            type: "POST",
            cache: false,
            data: FormData,
            dataType:'json',
            success: function(data){
                if(data.status == 1){
                    swal({
                        title: "Berhasil!", 
                        text: data.pesan, 
                        type: "success", 
                        confirmButtonText: "Iya, cetak faktur!",
                        cancelButtonText: "Tidak, nanti saja!",
                        showCancelButton: true
                    }, function (isConfirm){
                        if(isConfirm) {
                            var FormData = "&id_hadiah_m="+$('#id_hadiah_m').val();
                            window.open("<?php echo site_url('faktur/faktur_hadiah/?'); ?>" + FormData,'_blank');
                            window.location.href = "hadiah";
                        }else{
                            window.location.href = "hadiah";
                        }
                    });
                }else if(data.status == 0){
                    swal({
                        title: "Oops !", 
                        text: data.pesan, 
                        type: "error", 
                        confirmButtonClass: 'btn-danger btn-md waves-effect waves-light',
                        confirmButtonText: "Ok"
                    },function (isConfirm){
                        window.location.href = url;
                    });
                } 
            },
            error: function (jqXHR, textStatus, errorThrown){
                swal({
                    title: "Oops !", 
                    text: "Gagal menyimpan transaksi hadiah ini.", 
                    type: "error", 
                    confirmButtonClass: 'btn-danger btn-md waves-effect waves-light',
                    confirmButtonText: "Ok"
                },function (isConfirm){
                    window.location.href = url;
                });
            }
        });
    }

    function TahanTransaksi()
    {
        var FormData = "id_hadiah_m="+$('#id_hadiah_m').val(); 
            FormData += "&nama_hadiah="+$('#nama_hadiah').val();
            FormData += "&id_customer_pusat="+$('#id_customer_hidden').html();
            FormData += "&catatan="+encodeURI($('#catatan').val());
            FormData += "&total="+$('#TotalBayarHidden').val();

        $.ajax({
            url: "<?php echo site_url('hadiah/tahan_transaksi'); ?>",
            type: "POST",
            cache: false,
            data: FormData,
            dataType:'json',
            success: function(data){
                if(data.status == 1){
                    swal({
                        title: "Berhasil!", 
                        text: data.pesan, 
                        type: "success", 
                        confirmButtonText: "Ok"
                    },function (isConfirm){
                        window.location.href="hadiah";
                    });
                }else if(data.status == 0){
                    swal({
                        title: "Oops !", 
                        text: data.pesan, 
                        type: "error", 
                        confirmButtonClass: 'btn-danger btn-md waves-effect waves-light',
                        confirmButtonText: "Ok"
                    },function (isConfirm){
                        window.location.href = url;
                    });
                }   
            },
            error: function (jqXHR, textStatus, errorThrown){
                swal({
                    title: "Oops !", 
                    text: "Gagal menahan transaksi hadiah ini.", 
                    type: "error", 
                    confirmButtonClass: 'btn-danger btn-md waves-effect waves-light',
                    confirmButtonText: "Ok"
                },function (isConfirm){
                    window.location.href = url;
                });
            }
        });
    }

    function AutoCompleteGue(KataKunci)
    {
        $('#id_barang_pusat').html('');
        $('#nama_barang').html('Belum ada barang yg dipilih');
        $('#harga_satuan').val('0');
        $('#harga_satuan_tampil').val('Rp. 0');
        $('#jumlah_hadiah').prop('disabled', true).val('');
        $('#sub_total').val('0');
        $('#sub_total_tampil').html('Rp. 0');
        $('#SimpanDetail').prop('disabled', true);

        $('#induk_pencarian_barang').removeClass('has-warning');
        $('#induk_pencarian_barang').addClass('has-has-succes');
        $('#icon_barang').removeClass('fa-search');
        $('#icon_barang').addClass('fa-spin fa-refresh');
        $('#tombol_scan').removeClass('btn-warning');
        $('#tombol_scan').addClass('btn-primary');
        $('div#hasil_pencarian').hide();

        delay(function(){
            var id_hm = $('#id_hadiah_m').val();
            $.ajax({
                url: "<?php echo site_url('hadiah/ajax_kode'); ?>",
                type: "POST",
                cache: false,
                data: 'keyword=' + KataKunci +
                      '&id_hm=' + id_hm,
                dataType:'json',
                success: function(json){
                    if(json.status == 1){
                        $('#icon_barang').removeClass('fa-spin fa-refresh');
                        $('#icon_barang').addClass('fa-search');
                        $('div#hasil_pencarian').show('fast');
                        $('div#hasil_pencarian').html(json.datanya);
                    }else if(json.status == 2){
                            $('#icon_barang').removeClass('fa-spin fa-refresh');
                            $('#icon_barang').addClass('fa-search');
                            $('#induk_pencarian_barang').removeClass('has-succes');
                            $('#induk_pencarian_barang').addClass('has-warning');

                            swal({
                                title: json.judul_pesan,
                                text: json.isi_pesan,
                                type: "error",
                                confirmButtonClass: 'btn-danger btn-md waves-effect waves-light',
                                confirmButtonText: "Ok",
                            });
                    }else{
                        $('#icon_barang').removeClass('fa-spin fa-refresh');
                        $('#icon_barang').addClass('fa-search');
                        $('#induk_pencarian_barang').removeClass('has-primary');
                        $('#induk_pencarian_barang').addClass('has-warning');
                    }               
                }
            });
        }, 500);
    }

    function AutoCompletedCustomer(Lebar, KataKunci, Indexnya)
    {
        // Awal bersihkan data pemilihan barang
        $('#pencarian_kode').val('');
        $('#nama_barang').html('Belum ada barang yg dipilih');
        $('#harga_satuan').val('0');
        $('#harga_satuan_tampil').val('Rp. 0');
        $('#jumlah_hadiah').val('0');
        $('#discount').val('0.00');
        $('#discount_hidden').val('0');
        $('#sub_total').val('0');
        $('#sub_total_tampil').html('Rp. 0');

        $('#jumlah_hadiah').prop('readonly', true).val();
        $('#jumlah_hadiah').prop('disabled', true).val();
        $('#discount').prop('readonly', true).val();
        $('#discount').prop('disabled', true).val();
        $('#harga_satuan_tampil').prop('readonly', true).val();
        $('#harga_satuan_tampil').prop('disabled', true).val();
        // Akhir bersihkan data pemilihan barang

        $('#induk_pencarian').removeClass('has-warning');
        $('#induk_pencarian').addClass('has-has-succes');

        $('#icon_customer').removeClass('fa-list');
        $('#icon_customer').addClass('fa-spin fa-refresh');

        $('div#hasil_pencarian_customer').hide();
        delay(function () {
            var Lebar   = Lebar + 25;
            $.ajax({
                url: "<?php echo site_url('hadiah/ajax_pencarian_customer'); ?>",
                type: "POST",
                cache: false,
                data: 'keyword=' + KataKunci,
                dataType:'json',
                success: function(json){
                    if(json.status == 1){
                        $('#icon_customer').removeClass('fa-spin fa-refresh');
                        $('#icon_customer').addClass('fa-list');
                        $('div#hasil_pencarian_customer').show('fast');
                        $('div#hasil_pencarian_customer').html(json.datanya);

                        // Bersihkan data customer yang dipilih
                        $('#id_customer_hidden').html('');
                        $('#kode_customer_pusat').val('');
                        $('#data_tipe_customer_pusat').html('-');
                        $('#data_handphone_customer_pusat').html('-');
                    }else{
                        $('#icon_customer').removeClass('fa-spin fa-refresh');
                        $('#icon_customer').addClass('fa-list');

                        $('#induk_pencarian').removeClass('has-succes');
                        $('#induk_pencarian').addClass('has-warning');
                    }
                }
            });
        }, 500);
    }

    function HitungSubtotalBarang()
    {
        var Harga           =   $('#harga_satuan').val();
        var JumlahHadiah    =   $('#jumlah_hadiah').val();

        var SubTotal        = parseInt(Harga) * parseInt(JumlahHadiah);
        var SabTotalRp      = 'Rp. 0';
                              if(SubTotal >= '0')
                              {
                                SabTotalRp  = to_rupiah(SubTotal);
                              }
        
        $('#sub_total').val(SubTotal);
        $('#sub_total_tampil').html(SabTotalRp);
    }

    function to_rupiah(angka){
        var rev     = parseInt(angka, 10).toString().split('').reverse().join('');
        var rev2    = '';
        for(var i = 0; i < rev.length; i++){
            rev2  += rev[i];
            if((i + 1) % 3 === 0 && i !== (rev.length - 1)){
                rev2 += '.';
            }
        }
        return rev2.split('').reverse().join('');

        // Pakai Simbol Rp.
        // return 'Rp. ' + rev2.split('').reverse().join('');
    }

    function to_angka(rp)
    {
        return parseInt(rp.replace(/,.*|\D/g,''),10)
    }

    function check_int(evt) {
        var charCode = ( evt.which ) ? evt.which : event.keyCode;
        return ( charCode >= 48 && charCode <= 57 || charCode == 8 );
    }
</script>
<!-- Akhir Script CRUD -->
<?php
	if($access_create == '1'){
		$readonly = '';
		$disabled = '';
	}else{		
		$readonly = 'readonly';
		$disabled = 'disabled';
	}
?>

<div class="content">
	<div class="container">
	<br>		
		<div class="row">
        	<div class="col-sm-12">
	        	<div class="row">
	        		<!-- Awal total harga -->
					<div class='col-sm-4'>
						<div class="panel panel-color panel-pink">
							<!-- Default panel contents -->
							<div class="panel-heading">
								<h3 class="panel-title">No. Hadiah
									<span class="pull-right" name='no_hadiah' id='no_hadiah'>
										<b>-</b>
									</span>
								</h3>
							</div>

							<span style="display: none;">
							<input type='hidden' id="id_hadiah_m" name="id_hadiah_m" value="<?php echo $id_hadiah_m ?>">
							<input type='hidden' name='tanggal' class='form-control input-sm' id='tanggal' value="<?php echo date('d-m-Y'); ?>" <?php echo $disabled; ?>>
							</span>

							<table class='table table-condensed table-striped table-hover' id='Total'>
								<tbody>
									<tr>
										<td colspan="2">
											<input type="text" class="form-control input-md text-dark" id="nama_hadiah" name="nama_hadiah" placeholder="Masukan nama hadiah" autocomplete="off">
										</td>
									</tr>
									<tr>
										<td colspan="2">
											<div class="row">
												<div class="col-md-10 pull-left">
			                                        <div id='induk_pencarian' class="has-feedback has-primary">
				                                        <span id="icon_customer" class="form-control-feedback fa fa-list"></span>
				                                        <input type="text" class="form-control input-md text-dark" id="pencarian_customer" name="id_customer_pusat[]" placeholder="Masukan nama customer" autocomplete="off">
				                                        <input id="kode_customer_pusat" type="hidden" class="form-control input-sm" value="-">
				                                    </div>
				                                    
				                                    <span id="id_customer_hidden" name="id_customer_hidden" style="display:none;"></span>
				                                    <div id='hasil_pencarian_customer' class="alert"></div>
			                                    </div>
			                                    <div class="col-md-2 pull-right">
			                                        <a href="<?php echo site_url('hadiah/form_customer'); ?>" class='btn btn-pink' id='TambahCustomer'>
			                                        	<i class="mdi mdi-account-plus"></i>
			                                        </a>
			                                    </div>
			                                </div>		                               	
										</td>
									</tr>
									<tr>
										<td>
											<small>Handphone</small> <br/>
											<b><span id="data_handphone_customer_pusat" name="data_handphone_customer_pusat" class="input-md text-dark" type="text" readonly>-</span></b>
										</td>
										<td>
											<small>Tipe Customer</small> <br/>
											<b><span id="data_tipe_customer_pusat" name="data_tipe_customer_pusat" class="input-md text-dark" type="text">-</span></b>
										</td>
									</tr>
									<tr>
										<td><small>Jml Barang</small></td>
										<td class="text-right">
											<span id='jumlah_barang' name='jumlah_barang' class="text-dark">0</span>
										</td>
									</tr>												
									<tr>
										<td><small>Total</small></td>
										<td class="text-left">Rp. 
											<span id='TotalBayar' name="TotalBayar" class="text-dark pull-right">0</span>
											<input type="hidden" id='TotalBayarHidden' name='TotalBayarHidden'>
										</td>
									</tr>

									<tr>
										<td colspan="2">							
											<textarea name='catatan' id='catatan' class='form-control input-sm' rows='2' placeholder="Catatan Transaksi (Jika Ada)" style='resize: vertical; width:100%;'></textarea>
										</td>
									</tr>

									<tr>
										<td>
											<?php if($access_create == 1 or $access_update == 1){ ?>
												<button type='button' class='btn btn-primary' id='Simpan'>
													<i class='fa fa-paper-plane-o'></i>
												</button>
											<?php } ?>

											<?php if($access_create == 1 or $access_update == 1){ ?>
												<button type='button' class='btn btn-danger' id='Tahan'>
													<i class='fa fa-hand-paper-o'></i>
												</button>
											<?php } ?>

											<?php if($access_laporan == 1){ ?>
												<button id='Laporan' type='button' class='btn btn-default' title="Laporan Transaksi">
													<i class='fa fa-file-text-o'></i>
												</button>
											<?php } ?>											
										</td>
										<td></td>
									</tr>

									<tr>
										<td colspan="2">
											<p><i class='fa fa-keyboard-o fa-fw'></i> <b><small>Shortcut Keyboard : </small></b></p>
											<div class='row'>
												<div class='col-sm-6'><small>F6 = Cari Barang</small></div>
												<div class='col-sm-6'><small>F8 = Tahan Transaksi</small></div>
												<div class='col-sm-6'><small>F7 = Fokus ke field catatan</small></div>
												<div class='col-sm-6'><small>F9 = Simpan Transaksi</small></div>
											</div> 
										</td>
									</tr>	
								</tbody>
							</table>
						</div>
					</div>
					<!-- Akhir total harga --> 
					
	        		<!-- Awal pilih barang dan daftar transaksi -->
		            <div class="col-sm-8">
		            	<div class="row">
		            		<!-- Awal scan barang dan pilih Barang -->
		            		<div class="col-sm-4">
			                    <table id='TableScan' class="table table-condensed dt-responsive nowrap card-box">
			                    	<thead class="input-sm">
			                    		<tr class="text-dark">
			                    			<th style="width:170px;">SCAN </th>
			                    		</tr>
			                    	</thead>

			                    	<tbody class="input-sm">
			                    		<tr class="text-dark">
			                    			<!-- Pencarian Barang : SKU / Nama Barang -->
			                    			<td>
			                    				<div id="induk_pencarian_barang" class="input-group">
			                                        <input type="text" class="form-control input-sm" id="pencarian_kode" name="pencarian_kode" placeholder="SKU / Nama Barang" <?php echo $disabled ?> autocomplete="off">
													<span class="input-group-btn">
														<button type="button" class="btn waves-effect waves-light btn-sm btn-primary" id="tombol_scan" <?php echo $disabled ?>>
														<i id="icon_barang" class="fa fa-search"></i></button>
													</span>				                                        
			                                    </div>
			                                    <span id="id_barang_pusat" style="display: none;"></span>
			                                    <div id='hasil_pencarian' class="alert"></div>
			                    			</td>
			                    		</tr>
			                    	</tbody>
			                    </table>
						    </div>
						    <!-- Akhir scan barang dan pilih Barang -->

						    <!-- Awal detail barang -->
						    <div class="col-sm-8">
						    	<table id='TableBarang' class="table table-condensed dt-responsive nowrap card-box">
						    		<thead class="input-sm">
			                    		<tr class="text-dark">
			                    			<th style="width:200;">Nama Barang </th>
			                    			<th style="width:80px;">Harga</th>
			                    			<th style="width:60px;">Qty </th>
			                    			<th style="width:100;">Subtotal </th>
			                    			<th style="width:20;"></th>
			                    		</tr>
			                    	</thead>
			                    	<tbody class="input-sm">
			                    		<tr class="text-dark">
			                    			<!-- Nama Barang -->
			                    			<td>
			                    				<span class="text-dark" id="nama_barang" name="nama_barang">Belum ada barang yg dipilih</span>
			                    			</td>
			                    			
			                    			<!-- Harga -->
			                    			<td>
			                    				<input type='hidden' name='harga_satuan' id='harga_satuan'>
			                    				<input type='text' id="harga_satuan_tampil" name="harga_satuan_tampil" class="form-control input-sm text-dark" value='Rp. 0' readonly disabled>
			                    			</td>
			                    			
			                    			<!-- Jumlah Beli -->
			                    			<td>
			                    				<input type='number' class='form-control input-sm' id='jumlah_hadiah' name='jumlah_hadiah' disabled>
			                    			</td>
			                    			
			                    			<!-- Subtotal -->
			                    			<td>
			                    				<input type='hidden' id="sub_total" name='sub_total'>
			                    				<span class="form-control input-sm" id="sub_total_tampil" name="sub_total_tampil">Rp. 0</span>
			                    			</td>

			                    			<td>
			                    				<button id='SimpanDetail' name="SimpanDetail" class='btn btn-pink btn-sm' type='button' disabled>
													<i class='fa fa-paper-plane-o'></i>
												</button>
			                    			</td>
			                    		</tr>
			                    	</tbody>
						    	</table>
						    </div>
						    <!-- Akhir detail barang -->
						</div>


			        	<!-- Awal daftar transaksi dan catatan transaksi --> 
		            	<div class="card-box table-responsive">
		            		<!-- Awal induk tab -->
		                    <ul class="nav nav-tabs tabs-bordered" id="tab_list_pembelian">
		                        <li class="active">
		                            <a href="#tab_induk_hadiah" id="tab_induk" data-toggle="tab" aria-expanded="true">
		                                <span class="visible-xs"><i class="fa fa-check-square"></i></span>
		                                <span class="hidden-xs" id="judul_tab_induk">Data Hadiah</span>
		                            </a>
		                        </li>
		                        <li class="">
		                            <a href="#tab_hadiah_batal" id="tab_batal" data-toggle="tab" aria-expanded="true">
		                                <span class="visible-xs"><i class="typcn typcn-home"></i></span>
		                                <span class="hidden-xs" id="judul_tab_batal">Data Hadiah Batal</span>
		                            </a>
		                        </li>
		                    </ul>
		                    <!-- Akhir induk tab -->

							<!-- Awal isi tab -->
							<div class="tab-content">
								<!-- Awal daftar hadiah -->
		                        <div class="tab-pane active" id="tab_induk_hadiah">
									<table id='TableTransaksi' class="table table-condensed table-striped table-hover dt-responsive nowrap" cellspacing="0" width="100%">
										<thead class="input-sm">
											<tr class="text-dark">
												<th>#</th>
												<th>Tombol</th>
												<th>SKU</th>
												<th>Nama Barang</th>
												<th>Harga</th>
												<th>Qty</th>
												<th>Subtotal</th>

												<th>Pegawai Save</th>
												<th>Tanggal Save</th>
												<th>Pegawai Edit</th>
												<th>Tanggal Edit</th>
											</tr>
										</thead>

										<tbody class="input-sm text-dark"></tbody>
									</table>
								</div>
								<!-- Akhir daftar hadiah -->

								<!-- Awal daftar hadiah batal -->
		                        <div class="tab-pane" id="tab_hadiah_batal">
									<table id='TableTransaksiBatal' class="table table-condensed table-striped table-hover dt-responsive nowrap" cellspacing="0" width="100%">
										<thead class="input-sm">
											<tr class="text-dark">
												<th>#</th>
												<th>SKU</th>
												<th>Nama Barang</th>
												<th>Harga</th>
												<th>Qty</th>
												<th>Subtotal</th>
												<th>Keterangan Batal</th>
												
												<th>Pegawai Batal</th>
												<th>Tanggal Batal</th>
												<th>Pegawai Save</th>
												<th>Tanggal Save</th>
												<th>Pegawai Edit</th>
												<th>Tanggal Edit</th>
											</tr>
										</thead>

										<tbody class="input-sm text-dark"></tbody>
									</table>
								</div>
								<!-- Akhir daftar hadiah batal -->
							</div>
						</div>
				        <!-- Akhir daftar transaksi -->
		            </div>
		            <!-- Akhir pilih barang dan daftar transaksi -->	
				</div>
            </div>
        </div>
	</div>
</div>

<div class="modal" id="ModalGue" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class='fa fa-times-circle'></i></button>
				<h4 class="modal-title" id="ModalHeader"></h4>
			</div>
			<div class="modal-body" id="ModalContent"></div>
			<div class="modal-footer" id="ModalFooter"></div>
		</div>
	</div>
</div>

<div class="modal" id="ModalPesan" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class='fa fa-times-circle'></i></button>
				<h4 class="modal-title" id="ModalHeaderPesan"></h4>
			</div>
			<div class="modal-body" id="ModalContentPesan"></div>
			<div class="modal-footer" id="ModalFooterPesan"></div>
		</div>
	</div>
</div>

<div class="modal" id="ModalCariBarang" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class='fa fa-times-circle'></i></button>
				<h5 class="modal-title" id="ModalHeaderCariBarang"></h5>
			</div>
			<div class="modal-body" id="ModalContentCariBarang">
				<table id='TableCariBarang' class="table table-condensed table-striped table-hover dt-responsive nowrap" cellspacing="0" width="100%">
					<thead class="input-sm">
						<tr id="header_cari_barang" class="text-dark">
							<th></th>
						</tr>
					</thead>

					<tbody class="input-sm text-dark"></tbody>
				</table>
			</div>
			<div class="modal-footer" id="ModalFooterCariBarang"></div>
		</div>
	</div>
</div>

<script>
	$('#ModalGue').on('hide.bs.modal', function () {
	   setTimeout(function(){ 
	        $('#ModalHeader, #ModalContent, #ModalFooter').html('');
	   }, 500);
	});

	$('#ModalPesan').on('hide.bs.modal', function () {
	   setTimeout(function(){ 
	        $('#ModalHeaderPesan, #ModalContentPesan, #ModalFooterPesan').html('');
	   }, 500);
	});

	$('#ModalCariBarang').on('hide.bs.modal', function () {
	   setTimeout(function(){ 
	        $('#ModalHeaderCariBarang, #ModalFooterCariBarang').html('');
	   }, 500);
	});	
</script>

<?php echo form_open('hadiah/form_customer', array('id' => 'FormTambahCustomer')); ?>
<div class="row">
    <div class="col-md-6">
        <div class="form-horizontal">
            <label class="control-label col-md-3 small">NAMA CUSTOMER</label>
            <div class="col-md-9">
                <input id="nama_customer_pusat" name="nama_customer_pusat" placeholder="Nama Customer" class="form-control" type="text">
                <span class="help-block"></span>
            </div>
        </div>

        <div class="form-horizontal">
            <label class="control-label col-md-3 small">NO. SIUP</label>
            <div class="col-md-9">
                <input name="no_siup" placeholder="NO. SIUP" class="form-control" type="text">
                <span class="help-block"></span>
            </div>
        </div>

        <div class="form-horizontal">
            <label class="control-label col-md-3 small">Asal & Tipe Bisinis</label>
            <div class="col-md-9">
                <div class="row">
                    <div class="col-md-6">
                        <select name="asal_customer_pusat" class="form-control">
                            <!-- <option value="">Pilih Status</option> -->
                            <option value="LOCAL">LOCAL</option>
                            <option value="IMPOR">IMPOR</option>
                        </select>
                        <span class="help-block"></span>
                    </div>

                    <div class="col-md-6">
                        <select name="tipe_bisnis" class="form-control">
                            <!-- <option value="">Pilih Status</option> -->
                            <option value="PUTUS">PUTUS</option>
                            <option value="KONSINYASI">KONSINYASI</option>
                            <option value="TITIP_JUAL">TITIP JUAL</option>
                        </select>
                        <span class="help-block"></span>
                    </div>
                </div>
            </div>
        </div>

        <div class="form-horizontal">
            <label class="control-label col-md-3 small">TIPE CUTSOMER</label>
            <div class="col-md-9">
                <select name="tipe_customer_pusat" class="form-control">
                    <!-- <option value="">Pilih Status</option> -->
                    <option value="GROSIR">GROSIR</option>
                    <option value="BENGKEL">BENGKEL</option>
                    <option value="ECERAN">ECERAN</option>
                    <option value="MRC">MRC</option>
                </select>
                <span class="help-block"></span>
            </div>
        </div>                                

        <div class="form-horizontal">
            <label class="control-label col-md-3 small">Alamat</label>
            <div class="col-md-9">
                <textarea name="alamat_customer_pusat" placeholder="Alamat" class="form-control"></textarea>
                <span class="help-block"></span>                            
            </div>
        </div>                                
    </div>

    <div class="col-md-6">
        <div class="form-horizontal">
            <label class="control-label col-md-3 small">Email</label>
            <div class="col-md-9">
                <input name="email" placeholder="email" class="form-control" type="email">
                <span class="help-block"></span>
            </div>
        </div>

        <div class="form-horizontal">
            <label class="control-label col-md-3 small">HP 1 - 2</label>
            <div class="col-md-9">
                <div class="row">
                    <div class="col-md-6">
                        <input name="handphone1" placeholder="Handphone 1" class="form-control" type="number">
                        <span class="help-block"></span>
                    </div>
                    <div class="col-md-6">
                        <input name="handphone2" placeholder="Handphone 2" class="form-control" type="number">
                        <span class="help-block"></span>
                    </div>
                </div>
            </div>
        </div>

        <div class="form-horizontal">
            <label class="control-label col-md-3 small">Tlpn 1 & 2</label>
            <div class="col-md-9">
                <div class="row">
                    <div class="col-md-6">
                        <input name="telephone1" placeholder="Telephone 1" class="form-control" type="number">
                        <span class="help-block"></span>
                    </div>
                    <div class="col-md-6">
                        <input name="telephone2" placeholder="Telephone 2" class="form-control" type="number">
                        <span class="help-block"></span>
                    </div>
                </div>
            </div>
        </div>

        <div class="form-horizontal">
            <label class="control-label col-md-3 small">Kontak & Jabatan</label>
            <div class="col-md-9">
                <div class="row">
                    <div class="col-md-6">
                        <input name="kontak_pribadi" placeholder="Kontak" class="form-control" type="text">
                        <span class="help-block"></span>
                    </div>
                    <div class="col-md-6">
                        <input name="jabatan" placeholder="Jabatan" class="form-control" type="text">
                        <span class="help-block"></span>
                    </div>
                </div>
            </div>
        </div>

        <div class="form-horizontal">
            <label class="control-label col-md-3 small">FAX</label>
            <div class="col-md-9">
                <input name="fax" placeholder="Fax" class="form-control" type="number">
                <span class="help-block"></span>
            </div>
        </div>
    </div>
</div>                        

<script>
    function TambahCustomer()
    {
        $('#SimpanTambahCustomer').text('menyimpan...'); //change button text
        $('#SimpanTambahCustomer').attr('disabled',true); //set button disable 

        $.ajax({
            url: $('#FormTambahCustomer').attr('action'),
            type: "POST",
            cache: false,
            data: $('#FormTambahCustomer').serialize(),
            dataType:'json',
            success: function(json){
                if(json.status)
                { 
                    $('#FormTambahCustomer').each(function(){
                        this.reset();
                    });

                    $('.modal-dialog').removeClass('modal-lg');
                    $('.modal-dialog').addClass('modal-sm');
                    $('#ModalHeader').html('Berhasil');
                    $('#ModalContent').html(json.pesan);
                    $('#ModalFooter').html("<button type='button' class='btn btn-primary' data-dismiss='modal' autofocus>Okay</button>");
                    $('#ModalGue').modal('show');

                    $('#id_customer_hidden').html(json.id_customer_pusat);
                    $('#pencarian_customer').val(json.nama_customer_pusat);
                    $('#data_handphone_customer_pusat').html(json.handphone1);
                    $('#data_tipe_customer_pusat').html(json.tipe_customer_pusat);
                }
                else 
                {
                    for (var i = 0; i < json.inputerror.length; i++) 
                    {
                        $('[name="'+json.inputerror[i]+'"]').parent().parent().addClass('has-error'); //select parent twice to select div form-group class and add has-error class
                        $('[name="'+json.inputerror[i]+'"]').next().text(json.error_string[i]); //select span help-block class set text error string

                        $('#SimpanTambahCustomer').text('Simpan'); //change button text
                        $('#SimpanTambahCustomer').attr('disabled',false); //set button disable 
                    }
                }
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error adding / update data');
                $('#SimpanTambahCustomer').text('Simpan'); //change button text
                $('#SimpanTambahCustomer').attr('disabled',false); //set button enable 

            }
        });
    }

    $(document).ready(function(){
        $('#FormTambahCustomer')[0].reset(); // reset form on modals
        $('.form-horizontal').removeClass('has-error'); // clear error class
        $('.help-block').empty(); // clear error string    

        //set input/textarea/select event when change value, remove class error and remove text help block 
        $("input").change(function(){
            $(this).parent().parent().removeClass('has-error');
            $(this).next().empty();
        });
        $("textarea").change(function(){
            $(this).parent().parent().removeClass('has-error');
            $(this).next().empty();
        });
        $("select").change(function(){
            $(this).parent().parent().removeClass('has-error');
            $(this).next().empty();
        });
    });

    var Tombol = "<button type='button' class='btn btn-primary' id='SimpanTambahCustomer'>Simpan</button>";
    Tombol += "<button type='button' class='btn btn-default' data-dismiss='modal'>Tutup</button>";
    $('#ModalFooter').html(Tombol);

    $("#FormTambahCustomer").find('input[type=text],textarea,select').filter(':visible:first').focus();

    $('#SimpanTambahCustomer').click(function(e){
        e.preventDefault();
        TambahCustomer();
    });

    $('#FormTambahCustomer').submit(function(e){
        e.preventDefault();
        TambahCustomer();
    });
</script>
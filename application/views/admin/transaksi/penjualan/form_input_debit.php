<?php echo form_open('penjualan/form_input_debit', array('id' => 'FormTambahDebit')); ?>

<div class="row">
    <div class="col-sm-9">
        <div class="form-horizontal">
            <label class="control-label col-sm-3 small">EDC</label>
            <div class="col-sm-9">
                <select id="kartu_edc_pusat" name="kartu_edc_pusat" class="form-control">
                    <option value="">-- Pilih EDC --</option>
                    <?php foreach ($list_edc as $edc) { ?>
                        <option value="<?php echo $edc->id_kartu_edc_pusat ?>"
                        <?php 
                            if(isset($_POST['id_kartu_edc_pusat']) && $_POST['id_kartu_edc_pusat'] == $edc->id_kartu_edc_pusat) 
                                {echo "selected"; } 
                        ?>>
                            <?php echo $edc->nama_bank ?> - A/N : <?php echo $edc->atas_nama ?>
                        </option>

                    <?php } ?>
                </select>
                <span class="help-block"></span>
            </div>
        </div>

        <div class="form-horizontal">
            <label class="control-label col-sm-3 small">No. Kartu Customer</label>
            <div class="col-sm-9">
                <input id="no_kartu_customer" name="no_kartu_customer" placeholder="No. Kartu Customer" class="form-control" type="text">
                <span class="help-block"></span>
            </div>
        </div>

        <div class="form-horizontal">
            <label class="control-label col-sm-3 small">Jumlah Debit</label>
            <div class="col-sm-7">
                <input id="pembayaran_debit_hidden" name="pembayaran_debit_hidden" placeholder="Jumlah pembayaran debit" class="form-control" type="hidden">
                <input id="pembayaran_debit" name="pembayaran_debit" placeholder="Jumlah pembayaran debit" class="form-control" type="text">
                <span class="help-block"></span>
            </div>
            <div class="col-sm-2">
                <button type="button" id="SimpanDebit" name="SimpanDebit" class="btn btn-primary btn-block">Simpan</button>
            </div>
        </div>
    </div>
    
    <div class="col-sm-12">
        <br/>
    </div>

    <div class="col-sm-12 ">
        <table id='TableDebit' class="table table-condensed table-striped table-hover dt-responsive nowrap" cellspacing="0" width="100%">
            <thead class="input-sm">
                <tr class="text-dark">
                    <th>#</th>
                    <th>Tombol</th>
                    <th>Nama Bank</th>
                    <th>No. Kartu Customer</th>
                    <th>Jumlah Debit</th>
                    <th>Tanggal</th>
                </tr>
            </thead>

            <tbody class="input-sm text-dark"></tbody>
        </table>
    </div>                             
</div>

<script>
    function TambahDebit()
    {
        $.ajax({
            url: $('#FormTambahDebit').attr('action'),
            type: "POST",
            cache: false,
            data: $('#FormTambahDebit').serialize(),
            dataType:'json',
            success: function(json){
                if(json.status == 1){ 
                    $('#FormTambahDebit').each(function(){
                        this.reset();
                    });

                    $('.modal-dialog').removeClass('modal-lg');
                    $('.modal-dialog').addClass('modal-sm');
                    $('#ModalHeader').html('Berhasil');
                    $('#ModalContent').html(json.pesan);
                    $('#ModalFooter').html("<button type='button' class='btn btn-primary' data-dismiss='modal' autofocus>Okay</button>");
                    $('#ModalGue').modal('show');

                    // $('#TotalDebit').html(to_rupiah(json.jumlah_pembayaran_debit));
                    // $('#TotalDebitHidden').val(json.jumlah_pembayaran_debit);
                }else{
                    $('#ResponseInput').html(json.pesan);
                }
            }
        });
    }

    function simpan_pembayaran_debit()
    {
        $('#SimpanDebit').text('menyimpan...');
        $('#SimpanDebit').attr('disabled',true);

        var FormData = "no_penjualan="+$('#no_penjualan').html(); 
            FormData += "&grandtotal="+$('#GrandTotalHidden').val();
            FormData += "&id_penjualan_m="+$('#id_penjualan_m').val();
            FormData += "&id_kartu_edc_pusat="+$('#kartu_edc_pusat').val();
            FormData += "&no_kartu_customer="+$('#no_kartu_customer').val();
            FormData += "&jumlah_pembayaran="+$('#pembayaran_debit_hidden').val();

        $.ajax({
            url: "<?php echo site_url('penjualan/simpan_penjualan_debit'); ?>",
            type: "POST",
            cache: false,
            data: FormData,
            dataType:'json',
            success: function(data){
                if(data.status){   
                    $('#TotalDebit').html(to_rupiah(data.total_debit));
                    $('#TotalDebitHidden').val(data.total_debit);
                    $('#UangCash').val(to_rupiah(data.total_tunai));
                    $('#UangCashHidden').val(data.total_tunai);

                    reload_table();
                    HitungTotalKembalian();
                    $('#kartu_edc_pusat').val('');
                    $('#no_kartu_customer').val('');
                    $('#pembayaran_debit').val('');
                    $('#pembayaran_debit_hidden').val('');
                    $('#SimpanDebit').text('Simpan');
                    $('#SimpanDebit').attr('disabled',false);
                    $('#kartu_edc_pusat').focus();
                }else{
                    for (var i = 0; i < data.inputerror.length; i++) 
                    {
                        $('[name="'+data.inputerror[i]+'"]').parent().parent().addClass('has-error');
                        $('[name="'+data.inputerror[i]+'"]').next().text(data.error_string[i]);
                        $('#SimpanDebit').text('Simpan');
                        $('#SimpanDebit').attr('disabled',false);
                    }
                }
            },
            error: function (jqXHR, textStatus, errorThrown){
                alert('Error adding / update data');
                $('#SimpanDebit').text('Simpan');
                $('#SimpanDebit').attr('disabled',false);

            }
        });
    }

    $(document).on('keyup', '#pembayaran_debit', function(e){
        var charCode = e.which || e.keyCode;
        if(charCode == 13){
            simpan_pembayaran_debit();
        }else{
            var Debit   = $('#pembayaran_debit').val();
                Debit   = to_angka(Debit);
                
            if(Debit > 0){
                $('#pembayaran_debit').val(to_rupiah(Debit));
                $('#pembayaran_debit_hidden').val(Debit);
            }else{
                $('#pembayaran_debit').val('');
                $('#pembayaran_debit_hidden').val('0');
            }
        }
    });

    $(document).on('click', '#SimpanDebit', function(){
        simpan_pembayaran_debit();
    });

    function edit_penjualan_debit(id_penjualan_debit)
    {
        $('.form-horizontal').removeClass('has-error');
        $('.help-block').empty();

        $.ajax({
            url : "<?php echo site_url('penjualan/ajax_edit_debit/')?>/" + id_penjualan_debit,
            type: "GET",
            dataType: "JSON",
            success: function(data){
                $('#SimpanDebit').text('Update');
                $('#kartu_edc_pusat').val(data.id_kartu_edc_pusat);
                $('#no_kartu_customer').val(data.no_kartu_customer);
                $('#pembayaran_debit').val(to_rupiah(data.jumlah_pembayaran));
                $('#pembayaran_debit_hidden').val(data.jumlah_pembayaran);
                $('#harga_satuan_tampil').val(data.jumlah_pembayaran);
            },
            error: function (jqXHR, textStatus, errorThrown){
                alert('Error get data from ajax');
            }
        });
    }

    function hapus_penjualan_detail_debit(id_penjualan_debit)
    {
        var FormData = "no_penjualan="+$('#no_penjualan').html(); 
            FormData += "&grandtotal="+$('#GrandTotalHidden').val();
            FormData += "&id_penjualan_m="+$('#id_penjualan_m').val();

        $.ajax({
            url : "<?php echo site_url('penjualan/ajax_hapus_detail_debit')?>/"+id_penjualan_debit,
            type: "POST",
            cache: false,
            data: FormData,
            dataType: "JSON",
            success: function(data){
                if(data.status == 1){
                    $('#TotalDebit').html(to_rupiah(data.total_debit));
                    $('#TotalDebitHidden').val(data.total_debit);
                    $('#UangCash').val(to_rupiah(data.total_tunai));
                    $('#UangCashHidden').val(data.total_tunai);

                    reload_table();
                    HitungTotalKembalian();
                    $('#kartu_edc_pusat').val('');
                    $('#no_kartu_customer').val('');
                    $('#pembayaran_debit').val('');
                    $('#pembayaran_debit_hidden').val('');
                    $('#kartu_edc_pusat').focus();
                }else if(data.status == 0){
                    reload_table();
                }
            }
        });
    }

    function reload_table()
    {
        table.ajax.reload();
    }

    $(document).ready(function(){
        $('.form-horizontal').removeClass('has-error');
        $('.help-block').empty();

        $("input").change(function(){
            $(this).parent().parent().removeClass('has-error');
            $(this).next().empty();
        });
        $("textarea").change(function(){
            $(this).parent().parent().removeClass('has-error');
            $(this).next().empty();
        });
        $("select").change(function(){
            $(this).parent().parent().removeClass('has-error');
            $(this).next().empty();
        });
    });

    var Tombol = "<button type='button' class='btn btn-default' data-dismiss='modal'>Tutup</button>";
    $('#ModalFooter').html(Tombol);

    $("#FormTambahDebit").find('input[type=text],textarea,select').filter(':visible:first').focus();

    $('#SimpanTambahDebit').click(function(e){
        e.preventDefault();
        TambahDebit();
    });

    $('#FormTambahDebit').submit(function(e){
        e.preventDefault();
        TambahDebit();
    });

    var no_penjualan = $('#no_penjualan').html(); 
    table            = $('#TableDebit').DataTable({ 
        processing: true,
        serverSide: true,
        order: [],
        lengthMenu: [[5, 10, 25, 50, -1], [5, 10, 25, 50, "All"]],
        pagingType: "full",

        ajax: {
            url: "<?php echo site_url('penjualan/ajax_list_debit')?>",
            type: "POST",
            data : {'no_penjualan' : no_penjualan}
        },

        columnDefs: [
            { 
                targets: [ -1 ],
                orderable: false,
            },
        ],
    });
</script>
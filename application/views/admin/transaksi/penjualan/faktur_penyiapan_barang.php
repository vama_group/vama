<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Faktur Penyiapan</title>
	<link rel="stylesheet" href="">

	<style type="text/css">
		.table_gaya {
		    font-family: sans-serif;
		    color: #444;
		    width: 50%;
		}

		.table_gaya tr th{
		    font-weight: normal;
		}

		.table_gaya, th, td {
		    text-align: center;
		}

		.th_td_gaya{
			font-weight: normal;
		    border: 1px;
		    background-color: #FFF;
		}
	</style>
</head>

<body>
	<table class='table_gaya' width="100%" style="background-color: #000;">
		<thead>
			<tr>
				<th width="5%" class="th_td_gaya" style="text-align: left; font-size: 9pt; font-weight: bold;">NO.</th>
				<th width="10%" class="th_td_gaya" style="text-align: left; font-size: 9pt; font-weight: bold;">SKU</th>
				<th width="65%" class="th_td_gaya" style="text-align: left; font-size: 9pt; font-weight: bold;">Nama Barang</th>
				<th width="10%" class="th_td_gaya" style="text-align: left; font-size: 9pt; font-weight: bold;">QTY 1</th>
				<th width="10%" class="th_td_gaya" style="text-align: left; font-size: 9pt; font-weight: bold;">QTY 2</th>
			</tr>
		</thead>
		<tbody>
			<?php $no = 1; ?>;
			<?php foreach ($data_detail AS $detail){ ?>
				<tr class="th_td_gaya">
					<td width="5%" class="th_td_gaya" style='text-align: left; font-size: 9pt;'><?php echo $no++ ?></td>
					<td width="10%" class="th_td_gaya" style='text-align: left; font-size: 9pt;'><?php echo $detail->sku ?></td>
					<td width="65%" class="th_td_gaya" style='text-align: left; font-size: 9pt;'><?php echo $detail->nama_barang ?></td>
					<td width="10%" class="th_td_gaya" style='text-align: right; font-size: 9pt;'><?php echo number_format($detail->jumlah_beli,'0',',','.') ?></td>
					<td width="10%" class="th_td_gaya" style='text-align: right; font-size: 9pt;'></td>
				</tr>
			<?php } ?> 
		</tbody>
	</table>
</body>
</html>
<?php if($access_customer == 1){ ?>
    <?php echo form_open('penjualan/form_customer', array('id' => 'FormTambahCustomer')); ?>
    <div class="row">
        <div class="col-md-6">
            <div class="form-horizontal">
                <label class="control-label col-md-3 small">Nama</label>
                <div class="col-md-9">
                    <input id="nama_customer_pusat" name="nama_customer_pusat" placeholder="Nama Customer" class="form-control" type="text">
                    <span class="help-block"></span>
                </div>
            </div>

            <div class="form-horizontal">
                <label class="control-label col-md-3 small">Email</label>
                <div class="col-md-9">
                    <input name="email" placeholder="email" class="form-control" type="email">
                    <span class="help-block"></span>
                </div>
            </div>

            <div class="form-horizontal">
                <label class="control-label col-md-3 small">HP 1 - 2</label>
                <div class="col-md-9">
                    <div class="row">
                        <div class="col-md-6">
                            <input name="handphone1" placeholder="Handphone 1" class="form-control" type="number">
                            <span class="help-block"></span>
                        </div>
                        <div class="col-md-6">
                            <input name="handphone2" placeholder="Handphone 2" class="form-control" type="number">
                            <span class="help-block"></span>
                        </div>
                    </div>
                </div>
            </div>                            

            <div class="form-horizontal">
                <label class="control-label col-md-3 small">Alamat</label>
                <div class="col-md-9">
                    <textarea name="alamat_customer_pusat" placeholder="Alamat" class="form-control"></textarea>
                    <span class="help-block"></span>                            
                </div>
            </div>                                
        </div>

        <div class="col-md-6">
            <div class="form-horizontal">
                <label class="control-label col-md-3 small">NO. SIUP</label>
                <div class="col-md-9">
                    <input name="no_siup" placeholder="NO. SIUP" class="form-control" type="text">
                    <span class="help-block"></span>
                </div>
            </div>

            <div class="form-horizontal">
                <label class="control-label col-md-3 small">Asal Customer</label>
                <div class="col-md-9">
                    <div class="row">
                        <div class="col-md-6">
                            <select id="asal_customer_pusat" name="asal_customer_pusat" class="form-control">
                                <!-- <option value="">Pilih Status</option> -->
                                <option value="OFFLINE">OFFLINE</option>
                                <option value="ONLINE">ONLINE</option>
                            </select>
                            <span class="help-block"></span>
                        </div>

                        <div class="col-md-6">
                            <select id="sumber_online" name="sumber_online" class="form-control" disabled>
                                <option value="0">Pilih</option>
                                <?php foreach ($list_sumber as $sumber) { ?>
                                    <option value="<?php echo $sumber->id_referensi ?>">
                                        <?php echo $sumber->nama_sumber_online ?>
                                    </option>
                                <?php } ?>
                            </select>
                            <span class="help-block"></span>
                        </div>
                    </div>
                </div>
            </div>

            <div class="form-horizontal">
                <label class="control-label col-md-3 small">TIPE CUTSOMER</label>
                <div class="col-md-9">
                    <select name="tipe_customer_pusat" class="form-control">
                        <!-- <option value="">Pilih Status</option> -->
                        <option value="GROSIR">GROSIR</option>
                        <option value="BENGKEL">BENGKEL</option>
                        <option value="ECERAN">ECERAN</option>
                    </select>
                    <span class="help-block"></span>
                </div>
            </div>    

            <div class="form-horizontal">
                <label class="control-label col-md-3 small">Tlpn 1 & 2</label>
                <div class="col-md-9">
                    <div class="row">
                        <div class="col-md-6">
                            <input name="telephone1" placeholder="Telephone 1" class="form-control" type="number">
                            <span class="help-block"></span>
                        </div>
                        <div class="col-md-6">
                            <input name="telephone2" placeholder="Telephone 2" class="form-control" type="number">
                            <span class="help-block"></span>
                        </div>
                    </div>
                </div>
            </div>

            <div class="form-horizontal">
                <label class="control-label col-md-3 small">Kontak & Jabatan</label>
                <div class="col-md-9">
                    <div class="row">
                        <div class="col-md-6">
                            <input name="kontak_pribadi" placeholder="Kontak" class="form-control" type="text">
                            <span class="help-block"></span>
                        </div>
                        <div class="col-md-6">
                            <input name="jabatan" placeholder="Jabatan" class="form-control" type="text">
                            <span class="help-block"></span>
                        </div>
                    </div>
                </div>
            </div>

            <div class="form-horizontal">
                <label class="control-label col-md-3 small">FAX</label>
                <div class="col-md-9">
                    <input name="fax" placeholder="Fax" class="form-control" type="number">
                    <span class="help-block"></span>
                </div>
            </div>
        </div>
    </div>                        

    <script>
        function TambahCustomer(){
            url_customer = "penjualan/transaksi/?&id=" + $('#id_penjualan_m').val();
            $('#SimpanTambahCustomer').text('menyimpan...');
            $('#SimpanTambahCustomer').attr('disabled',true);

            $.ajax({
                // url: $('#FormTambahCustomer').attr('action'),
                url: "<?php echo site_url('penjualan/tambah_customer')?>",
                type: "POST",
                cache: false,
                data: $('#FormTambahCustomer').serialize(),
                dataType:'json',
                success: function(json){
                    if(json.status == 2){
                        $('#FormTambahCustomer').each(function(){
                            this.reset();
                        });

                        $('#id_customer_hidden').html(json.id_customer_pusat);
                        $('#pencarian_customer').val(json.nama_customer_pusat);
                        $('#data_handphone_customer_pusat').html(json.handphone1);
                        $('#data_tipe_customer_pusat').html(json.tipe_customer_pusat);

                        $('#ModalGue').modal('hide');
                        swal({
                            title: "Berhasil !", 
                            text: json.pesan, 
                            type: "success", 
                            confirmButtonText: "Ok"
                        }, function(isConfirm){
                            setTimeout(function(){ 
                                $('#pencarian_kode_barang').val('').focus();
                            }, 100);
                        });
                    }else if(json.status == 1){
                        swal({
                            title: "Oops !", 
                            text: "Anda tidak di izinkan untuk menambahkan customer baru.", 
                            type: "error", 
                            confirmButtonClass: 'btn-danger btn-md waves-effect waves-light',
                            confirmButtonText: "Ok"
                        },function (isConfirm){
                            window.location.href = url_customer;
                        });
                    }else if(json.status == 0){
                        for (var i = 0; i < json.inputerror.length; i++){
                            $('[name="'+json.inputerror[i]+'"]').parent().parent().addClass('has-error');
                            $('[name="'+json.inputerror[i]+'"]').next().text(json.error_string[i]);
                            $('#SimpanTambahCustomer').text('Simpan');
                            $('#SimpanTambahCustomer').attr('disabled',false); 
                        }
                    }
                },
                error: function (jqXHR, textStatus, errorThrown){
                    swal({
                        title: "Oops !", 
                        text: "Data customer gagal disimpan.", 
                        type: "error", 
                        confirmButtonClass: 'btn-danger btn-md waves-effect waves-light',
                        confirmButtonText: "Ok"
                    },function (isConfirm){
                        window.location.href = url_customer;
                    });
                }
            });
        }

        $(document).ready(function(){
            $('#FormTambahCustomer')[0].reset();
            $('.form-horizontal').removeClass('has-error');
            $('.help-block').empty();    

            $("#asal_customer_pusat").change(function(){
                if($(this).val() == 'OFFLINE'){
                    $('#sumber_online').prop('disabled', true).val('0');
                }else if($(this).val() == 'ONLINE'){
                    $('#sumber_online').removeAttr('disabled').val('0');
                }
            });

            $("input").change(function(){
                $(this).parent().parent().removeClass('has-error');
                $(this).next().empty();
            });
            $("textarea").change(function(){
                $(this).parent().parent().removeClass('has-error');
                $(this).next().empty();
            });
            $("select").change(function(){
                $(this).parent().parent().removeClass('has-error');
                $(this).next().empty();
            });
        });

        var Tombol = "<button type='button' class='btn btn-primary' id='SimpanTambahCustomer'>Simpan</button>";
            Tombol += "<button type='button' class='btn btn-danger' data-dismiss='modal'>Batal</button>";
        $('#ModalFooter').html(Tombol);

        $("#FormTambahCustomer").find('input[type=text],textarea,select').filter(':visible:first').focus();

        $('#SimpanTambahCustomer').click(function(e){
            e.preventDefault();
            TambahCustomer();
        });

        $('#FormTambahCustomer').submit(function(e){
            e.preventDefault();
            TambahCustomer();
        });
    </script>
<?php }else{ ?>
    <h2 class="text-center text-danger">Anda tidak di izinkan untuk menambahkan data customer</h2>
<?php } ?>
<!-- Awal CSS -->
<link href="assets/plugin/zircos/plugins/datatables/jquery.dataTables.min.css" rel="stylesheet" type="text/css"/>
<link href="assets/plugin/zircos/plugins/datatables/responsive.bootstrap.min.css" rel="stylesheet" type="text/css"/>
<link href="assets/plugin/zircos/plugins/datatables/dataTables.bootstrap.min.css" rel="stylesheet" type="text/css"/>
<link href="assets/plugin/zircos/material-design/assets/css/tambahan.css" rel="stylesheet" type="text/css"/>
<!-- Akhir CSS -->

<!-- Awal JS -->
<script src="assets/plugin/zircos/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="assets/plugin/zircos/plugins/datatables/dataTables.bootstrap.js"></script>
<script src="assets/plugin/zircos/material-design/assets/js/detect.js"></script>
<script src="assets/plugin/zircos/material-design/assets/js/jquery.slimscroll.js"></script>
<script src="assets/plugin/zircos/plugins/datatables/dataTables.responsive.min.js"></script>
<script src="assets/plugin/zircos/plugins/datatables/responsive.bootstrap.min.js"></script>
<script src="assets/plugin/zircos/material-design/assets/pages/jquery.datatables.init.js"></script>
<!-- Akhir JS -->

<!-- Awal Sweet-Alert  -->
<link href="assets/plugin/zircos/plugins/bootstrap-sweetalert/sweet-alert.css" rel="stylesheet" type="text/css">
<script src="assets/plugin/zircos/plugins/bootstrap-sweetalert/sweet-alert.min.js"></script>
<!-- Akhir Sweet-Alert  -->

<script>
    var url               = "penjualan";
    var table_induk       = $('#TableTransaksi').DataTable();
    var table_batal       = $('#TableTransaksiBatal').DataTable();
    var table_cari_barang = $('#TableCariBarang').DataTable();

    $(document).ready(function(){
        $('#navbar').removeClass('navbar-default');
        $('#navbar').addClass('navbar-hijau');
        $('#JudulHalaman').html('Penjualan - VAMA');

        $("#pencarian_customer").focus();
        $("#wrapper").removeClass('forced');
        $("#wrapper").addClass('forced enlarged');

        var id_penjualan_m  = $('#id_penjualan_m').val();
        ambil_data(id_penjualan_m);

        if($('#id_penjualan_m').val() == '0'){
            $('#pencarian_customer').focus();
        }else{
            $('#pencarian_kode_barang').focus();
        }
    });

    var delay = (function () {
        var timer = 0;
        return function (callback, ms){
            clearTimeout(timer);
            timer = setTimeout(callback, ms);
        };
    })();

    function ambil_data(id_penjualan_m)
    {
        if(id_penjualan_m !=='0'){
            $.ajax({
                url : "<?php echo site_url('penjualan/ambil_data'); ?>",
                type: "POST",
                cache: false,
                data: 'id_penjualan_m=' + id_penjualan_m,
                dataType:'JSON',
                success: function(json){
                    url = "penjualan/transaksi/?&id=" + id_penjualan_m;
                    $('#id_penjualan_m').val(json.data_induk['id_penjualan_m']);
                    $('#no_penjualan').html(json.data_induk['no_penjualan']);
                    $('#pencarian_customer').val(json.data_induk['nama_customer_pusat']);
                    $('#id_customer_hidden').html(json.data_induk['id_customer_pusat']);
                    $('#no_penjualan').html(json.data_induk['no_penjualan']);
                    $('#kode_customer_pusat').val(json.data_induk['kode_customer_pusat']);
                    $('#data_handphone_customer_pusat').html(json.data_induk['handphone1']);
                    $('#data_tipe_customer_pusat').html(json.data_induk['tipe_customer_pusat']);
                    $('#jumlah_barang').html(json.data_induk['jumlah_barang']);
                    $('#TotalBayar').html(to_rupiah(json.data_induk['total']));
                    $('#TotalBayarHidden').val(json.data_induk['total']);
                    $('#TotalPpn').html(to_rupiah(json.data_induk['ppn']));
                    $('#TotalPpnHidden').val(json.data_induk['ppn']);
                    $('#BiayaLain').val(to_rupiah(json.data_induk['biaya_lain']));
                    $('#BiayaLainHidden').val(json.data_induk['biaya_lain']);
                    $('#GrandTotal').html(to_rupiah(json.data_induk['grand_total']));
                    $('#GrandTotalHidden').val(json.data_induk['grand_total']);
                    $('#UangCash').val(to_rupiah(json.data_induk['tunai']));
                    $('#UangCashHidden').val(json.data_induk['tunai']);
                    $('#TotalDebit').html(to_rupiah(json.data_induk['debit']));
                    $('#TotalDebitHidden').val(json.data_induk['debit']);
                    $('#UangKembali').val('0');
                    $('#catatan').val(json.data_induk['keterangan_lain']);

                    $('#judul_tab_induk').html("Data Penjualan : <span class='badge up bg-primary'>" +
                                                json.data_induk['jumlah_barang'] +
                                               " Barang </span>");
                    $('#judul_tab_batal').html("Data Penjualan Batal : <span class='badge up bg-danger'>" +
                                                json.data_batal['jumlah_barang'] +
                                               " Barang </span>");

                    HitungTotalKembalian();
                    tampilkan_data();
                    if(json.data_induk['tipe_customer_pusat'] == 'MRC'){
                        $('#pencarian_customer').prop('disabled', true);
                        $('#tombol_tambah_customer').prop('disabled', true);
                        $('#tombol_tambah_customer').prop('readonly', true);
                    }

                    <?php if($this->session->userdata('usergroup_name') !== 'Super Admin'){ ?>
                        $('#pencarian_customer').prop('disabled', true);
                        $('#tombol_tambah_customer').prop('disabled', true);
                        $('#tombol_tambah_customer').prop('readonly', true);
                    <?php } ?>

                },error : function(data){
                    url = 'penjualan';
                    swal({
                        title: "Oops !",
                        text: "Data penjualan gagal ditampilkan.",
                        type: "error",
                        confirmButtonClass: 'btn-danger btn-md waves-effect waves-light',
                        confirmButtonText: "Ok"
                    },function (isConfirm){
                        window.location.href = url;
                    });
                }
            });
        }
    }

    function tampilkan_data(){
        table_induk.clear();table_induk.destroy();
        table_induk = $('#TableTransaksi').DataTable({
            ordering: true,
            bFilter:true,
            processing:true,
            serverSide:true,
            order:[],
            lengthMenu: [[5, 10, 25, 50, -1], [5, 10, 25, 50, "All"]],
            pagingType: "full",

            oLanguage:{
                sLoadingRecords:'Silakan Tunggu',
                sProcessing:"<img src='assets/loader/cubeloader2.2.gif' width='50' height='50'> Harap tunggu",
                sZeroRecords:"Belum ada data untuk ditampilkan",
                sSearch : "Pencarian data"
            },
            ajax:{
                url: "<?php echo site_url('penjualan/ajax_list')?>",
                type: "POST",
                data: {'id_penjualan_m' : $('#id_penjualan_m').val()}
            },
            columnDefs: [
                            {
                                "targets": [ -1 ],
                                "orderable": false,
                            },
                        ],
        });
    }

    function tampilkan_data_batal(){
        table_batal.clear();table_batal.destroy();
        table_batal = $('#TableTransaksiBatal').DataTable({
            ordering: true,
            bFilter:true,
            processing:true,
            serverSide:true,
            order:[],
            lengthMenu: [[5, 10, 25, 50, -1], [5, 10, 25, 50, "All"]],
            pagingType: "full",

            oLanguage:{
                sLoadingRecords:'Silakan Tunggu',
                sProcessing:"<img src='assets/loader/cubeloader2.2.gif' width='50' height='50'> Harap tunggu",
                sZeroRecords:"Belum ada data untuk ditampilkan",
                sSearch : "Pencarian data"
            },
            ajax:{
                url: "<?php echo site_url('penjualan/ajax_list_batal')?>",
                type: "POST",
                data: {'id_penjualan_m' : $('#id_penjualan_m').val()}
            },
            columnDefs: [
                            {
                                "targets": [ -1 ],
                                "orderable": false,
                            },
                        ],
        });
    }

    function tampilkan_data_barang(){
        if($('#data_tipe_customer_pusat').html() == 'MRC'){
            $('#header_cari_barang').html('<th>#</th>' +
                                      '<th>SKU</th>' +
                                      '<th style="width:800px;">Nama Barang</th>' +
                                      '<th>Qty Toko</th>' +
                                      '<th>Qty Pusat</th>' +
                                      '<th>Ecr Toko</th>' +
                                      '<th>Ecr Pusat</th>' +
                                      '<th>Tombol</th>');
        }else{
            $('#header_cari_barang').html('<th>#</th>' +
                                      '<th>SKU</th>' +
                                      '<th style="width:800px;">Nama Barang</th>' +
                                      '<th>Stok</th>' +
                                      '<th>Eceran</th>' +
                                      '<th>Grosir 1</th>' +
                                      '<th>Grosir 2</th>' +
                                      '<th>Tombol</th>');
        }

        table_cari_barang.clear();table_cari_barang.destroy();
        table_cari_barang=$('#TableCariBarang').DataTable({
            ordering: true,
            bFilter:true,
            processing:true,
            serverSide:true,
            order:[],
            lengthMenu: [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
            pagingType: "full",

            oLanguage:{
                sLoadingRecords:'Silakan Tunggu',
                sProcessing:"<img src='assets/loader/cubeloader2.2.gif' width='50' height='50'> Harap tunggu",
                sZeroRecords:"Belum ada data untuk ditampilkan",
                sSearch : "Pencarian data"
            },
            ajax:{
                url: "<?php echo site_url('penjualan/ajax_list_barang')?>",
                type: "POST",
                data: {'id_penjualan_m' : $('#id_penjualan_m').val(),
                       'id_customer' : $('#id_customer_hidden').html(),}
            },
            columnDefs: [
                            {
                                "targets": [ -1 ],
                                "orderable": false,
                            },
                        ],
        });
    }

    $(document).on('click', '#tab_induk', function(e){
        tampilkan_data();
    });

    $(document).on('click', '#tab_batal', function(e){
        tampilkan_data_batal();
    });

    function reload_table_induk()
    {
        table_induk.ajax.reload();
    }

    function reload_table_batal()
    {
        table_batal.ajax.reload();
    }

    $(document).on('click', '#tombol_tambah_customer', function(e){
        e.preventDefault();
        <?php if($access_customer == 1){ ?>
            $.ajax({
                url: "<?php echo site_url('penjualan/form_customer'); ?>",
                type: "POST",
                cache: false,
                data: 'id_penjualan_m=' + $('#id_penjualan_m').val(),
                dataType:'json',
                success: function(json){
                    if(json.status == 1){
                        $('.modal-dialog').removeClass('modal-full');
                        $('.modal-dialog').removeClass('modal-sm');
                        $('.modal-dialog').removeClass('modal-lg');
                        $('.modal-dialog').addClass('modal-lg');
                        $('#ModalHeader').html('Tambah Customer Pusat');
                        $('#ModalContent').load('<?php echo site_url('penjualan/inputan_customer'); ?>');
                        $('#ModalGue').modal('show');
                    }else{
                        swal({
                            title: "Oops !",
                            text: json.pesan,
                            type: "error",
                            confirmButtonClass: 'btn-danger btn-md waves-effect waves-light',
                            confirmButtonText: "Ok"
                        },function(isConfirm){
                            window.location.href = url;
                        });
                    }
                }, error: function (jqXHR, textStatus, errorThrown){
                    swal({
                        title: "Oops !",
                        text: "Gagal membuka form tambah customer.",
                        type: "error",
                        confirmButtonClass: 'btn-danger btn-md waves-effect waves-light',
                        confirmButtonText: "Ok"
                    },function(isConfirm){
                        window.location.href = url;
                    });
                }
            });
        <?php } ?>
    });

    $(document).on('click', '#daftar-autocomplete li', function(){
        var id_barang_pusat = $(this).find('span#id_barangnya').html();
        var SKU             = $(this).find('span#skunya').html();
        var NamaBarang      = $(this).find('span#barangnya').html();
        var TipeCustomer    = $('span#data_tipe_customer_pusat').html();
        var JenisHarga      = 'ECERAN';

        $('#harga_satuan_tampil').prop('readonly', true).val('');
        $('#harga_satuan_tampil').removeAttr('disabled').val();

        if(TipeCustomer == ''){
            var Harganya    = $(this).find('span#eceran').html();
        }else if(TipeCustomer == '-'){
            var Harganya    = $(this).find('span#eceran').html();
        }else if(TipeCustomer == 'ECERAN'){
            var Harganya    = $(this).find('span#eceran').html();
        }

        if(TipeCustomer == 'BENGKEL'){
            var Harganya    = $(this).find('span#grosir1').html();
            JenisHarga      = 'GROSIR 1';
        }else if(TipeCustomer == 'GROSIR'){
            var Harganya    = $(this).find('span#grosir2').html();
            JenisHarga      = 'GROSIR 2';
        }else if(TipeCustomer == 'MRC'){
            var Harganya    = $(this).find('span#eceran_toko').html();
            JenisHarga      = 'MRC';
            // $('#harga_satuan_tampil').removeAttr('readonly').val();
            $('#harga_satuan_tampil').removeAttr('disabled').val();
        }

        $('div#hasil_pencarian_barang').hide();
        $('#id_barang_pusat').html(id_barang_pusat);
        $('#pencarian_kode_barang').val(SKU);
        $('#nama_barang').html(NamaBarang);
        $('#jenis_harga').val(JenisHarga);
        $('#harga_satuan').val(Harganya);
        $('#harga_satuan_tampil').val(to_rupiah(Harganya));
        $('#sub_total').val(Harganya);
        $('#sub_total_tampil').html(to_rupiah(Harganya));

        if(Harganya > 0){
            $('#jumlah_beli').removeAttr('readonly').val();
            $('#jumlah_beli').removeAttr('disabled').val();
            $('#SimpanDetail').removeAttr('disabled').val();
            $('#discount').removeAttr('readonly');
            $('#discount').removeAttr('disabled').val('0.00');
            $('#discount_tampil').removeAttr('readonly');
            $('#discount_tampil').removeAttr('disabled').val('Rp. 0');
            $('#jumlah_beli').focus();
        }
    });

    $(document).on('click', '#daftar_autocompleted_customer li', function(){
        var id_customer     = $(this).find('span#id_customer').html();
        var nama_customer   = $(this).find('span#nama_customer').html();
        var tipe_customer   = $(this).find('span#tipe_customer').html();
        var hp_customer     = $(this).find('span#hp_customer').html();
        var kode_customer  = $(this).find('span#kode_customer').html();

        $('#hasil_pencarian_customer').hide();
        $('#id_customer_hidden').html(id_customer);
        $('#kode_customer_pusat').val(kode_customer);
        $('#pencarian_customer').val(nama_customer);
        $('#data_tipe_customer_pusat').html(tipe_customer);
        $('#data_handphone_customer_pusat').html(hp_customer);
        $('#pencarian_kode_barang').focus();
    });

    $(document).on('click', '#daftar_autocompleted_harga li', function(){
        var harga           = $(this).find('#harga').html();
        var harga_hidden    = $(this).find('#harga_hidden').html();
        var jenis           = $(this).find('#jenis').html();

        if(jenis!= 'KHUSUS'){
            $('#jenis_harga').val(jenis);
            $('#harga_satuan').val(harga_hidden);
            $('#harga_satuan_tampil').val(harga);
            $('#sub_total').val(harga_hidden);
            $('#sub_total_tampil').html(harga);
            $('#jumlah_beli').focus();
            $('#hasil_pencarian_harga').hide();
            HitungSubtotalBarang();
        }else{
            $('.modal-dialog').removeClass('modal-lg');
            $('.modal-dialog').addClass('modal-sm');
            $('#ModalHeader').html('Input Harga Khusus');
            $('#ModalContent').html("<input type='text' class='form-control input-lg' id='harga_khusus' name='harga_khusus' placeholder='Masukan harga khusus' autofocus> <input type='hidden' class='form-control input-lg' id='harga_khusus_hidden' name='harga_khusus_hidden'>");
            $('#ModalFooter').html("<button type='button' class='btn btn-primary' onclick='masukan_harga_khusus()'>Proses</button>");
            $('#ModalGue').modal('show');
        }
    });

    $(document).on('keyup', '#harga_khusus', function(e){
        var charCode = e.which || e.keyCode;
        if(charCode == 13){
            masukan_harga_khusus();
        }else{
            var HK   = $('#harga_khusus').val();
                HK   = to_angka(HK);

            if(HK > 0){
                $('#harga_khusus').val(to_rupiah(HK));
                $('#harga_khusus_hidden').val(HK);
            }else{
                $('#harga_khusus').val('');
                $('#harga_khusus_hidden').val('0');
            }
        }
    });

    function masukan_harga_khusus()
    {
        if($('#harga_khusus').val() !== ''){
            $('#jenis_harga').val('KHUSUS');
            $('#harga_satuan').val($('#harga_khusus_hidden').val());
            $('#harga_satuan_tampil').val($('#harga_khusus').val());
            $('#sub_total').val($('#harga_khusus_hidden').val());
            $('#sub_total_tampil').html($('#harga_khusus').val());
            $('#ModalGue').modal('hide');
            $('#hasil_pencarian_harga').hide();
            $('#jumlah_beli').focus();
            HitungSubtotalBarang();
        }
    }

    $(document).on('click', '#tombol_scan_barang', function(e){
        AutoCompletedBarang($('#pencarian_kode_barang').val());
    });

    $(document).on('click', '#tombol_lihat_barang', function(e){
        tampilkan_data_barang();
        $('.modal-dialog').removeClass('modal-sm');
        $('.modal-dialog').removeClass('modal-md');
        $('.modal-dialog').addClass('modal-full');
        if($('#data_tipe_customer_pusat').html() == "MRC"){
            $('#ModalHeaderCariBarang').html('CARI BARANG TOKO : <b class="text-primary">' +
                                             $('#pencarian_customer').val() +
                                             '</b>');
        }else{
            $('#ModalHeaderCariBarang').html('CARI BARANG PUSAT');
        }
        $('#ModalCariBarang').modal('show');
        return false;
    });

    function proses_barang($sku){
        var SKU             = $sku;
        var TipeCustomer    = $('#data_tipe_customer_pusat').html();
        var JenisHarga      = 'ECERAN';

        if (TipeCustomer === 'MRC'){
            var KodeToko = $('#kode_customer_pusat').val();
        }else{
            var KodeToko = ''
        }

        $('#pencarian_kode_barang').val(SKU);
        $('div#hasil_pencarian_barang').hide();

        $.ajax({
            url: "<?php echo site_url('penjualan/ajax_cari_barang_untuk_penjualan'); ?>",
            type: "POST",
            cache: false,
            data: 'sku=' + SKU +
                  '&id_penjualan_m=' + $('#id_penjualan_m').val() +
                  '&tipe_customer=' + TipeCustomer +
                  '&kode_toko=' + KodeToko,
            dataType:'json',
            success: function(json){
                if(json.status == 1){
                    $('#harga_satuan_tampil').prop('readonly', true).val('');
                    $('#harga_satuan_tampil').removeAttr('disabled').val();

                    if(TipeCustomer == 'BENGKEL'){
                        JenisHarga      = 'GROSIR 1';
                    }else if(TipeCustomer == 'GROSIR'){
                        JenisHarga      = 'GROSIR 2';
                    }else if(TipeCustomer == 'MRC'){
                        JenisHarga      = 'MRC';
                        // $('#harga_satuan_tampil').removeAttr('readonly').val();
                    }

                    $('#id_barang_pusat').html(json.data['id_barang_pusat']);
                    $('#nama_barang').html(json.data['nama_barang']);
                    $('#jenis_harga').val(JenisHarga);
                    $('#harga_satuan').val(json.data['harga_satuan']);
                    $('#harga_satuan_tampil').val(to_rupiah(json.data['harga_satuan']));
                    $('#harga_bersih').val(to_rupiah(json.data['harga_satuan']))
                    $('#sub_total').val(json.data['harga_satuan']);
                    $('#sub_total_tampil').html(to_rupiah(json.data['harga_satuan']));

                    if($('#harga_satuan').val() >= 0){
                        $('#jumlah_beli').removeAttr('readonly');
                        $('#jumlah_beli').removeAttr('disabled').val();
                        $('#discount').removeAttr('readonly');
                        $('#discount').removeAttr('disabled').val('0.00');
                        $('#discount_tampil').removeAttr('readonly');
                        $('#discount_tampil').removeAttr('disabled').val('Rp. 0');
                        $('#SimpanDetail').removeAttr('disabled');
                        $('#jumlah_beli').focus();
                    }

                    $('#ModalCariBarang').modal('hide');
                    setTimeout(function(){
                        $('#jumlah_beli').val('').focus();
                    }, 100);
                }else if(json.status == 0){
                    $('#jumlah_beli').val('');
                    $('#sub_total').val($('#harga_satuan').val());
                    $('#sub_total_tampil').html($('#harga_satuan_tampil').val());

                    swal({
                        title: "Oops !",
                        text: json.pesan,
                        type: "error",
                        confirmButtonClass: 'btn-danger btn-md waves-effect waves-light',
                        confirmButtonText: "Ok"
                    },function(isConfirm){
                        // window.location.href = url;
                    });
                }
            }, error: function (jqXHR, textStatus, errorThrown){
                swal({
                    title: "Oops !",
                    text: "Data barang gagal ditampilkan.",
                    type: "error",
                    confirmButtonClass: 'btn-danger btn-md waves-effect waves-light',
                    confirmButtonText: "Ok"
                },function(isConfirm){
                    window.location.href = url;
                });
            }
        });
    }

    $(document).on('keyup', '#pencarian_kode_barang', function(e){
        if($(this).val() !== ''){
            var charCode = e.which || e.keyCode;
            // if(charCode == 40){
            //     if($('div#hasil_pencarian li.autocomplete_active').length > 0){
            //         var Selanjutnya = $('div#hasil_pencarian li.autocomplete_active').next();
            //         $('div#hasil_pencarian li.autocomplete_active').removeClass('autocomplete_active');

            //         Selanjutnya.addClass('autocomplete_active');
            //     }else{
            //         $('div#hasil_pencarian li:first').addClass('autocomplete_active');
            //     }
            // }else if(charCode == 38){
            //     if($('div#hasil_pencarian li.autocomplete_active').length > 0){
            //         var Sebelumnya = $('div#hasil_pencarian li.autocomplete_active').prev();
            //         $('div#hasil_pencarian li.autocomplete_active').removeClass('autocomplete_active');

            //         Sebelumnya.addClass('autocomplete_active');
            //     }else{
            //         $('div#hasil_pencarian li:first').addClass('autocomplete_active');
            //     }
            // }
            if(charCode == 13){
                var SKU             = $(this).val();
                var TipeCustomer    = $('#data_tipe_customer_pusat').html();
                var JenisHarga      = 'ECERAN';

                if (TipeCustomer === 'MRC'){
                    var KodeToko = $('#kode_customer_pusat').val();
                }else{
                    var KodeToko = ''
                }

                $('#pencarian_kode_barang').val(SKU);
                $('div#hasil_pencarian_barang').hide();
                $.ajax({
                    url: "<?php echo site_url('penjualan/ajax_cari_barang_untuk_penjualan'); ?>",
                    type: "POST",
                    cache: false,
                    data: 'sku=' + SKU +
                          '&id_penjualan_m=' + $('#id_penjualan_m').val() +
                          '&tipe_customer=' + TipeCustomer +
                          '&kode_toko=' + KodeToko,
                    dataType:'json',
                    success: function(json){
                        if(json.status == 1){
                            $('#harga_satuan_tampil').prop('readonly', true).val('');
                            $('#harga_satuan_tampil').removeAttr('disabled').val();

                            $('#id_barang_pusat').html(json.data['id_barang_pusat']);
                            $('#nama_barang').html(json.data['nama_barang']);
                            $('#harga_satuan').val(json.data['harga_satuan']);
                            $('#harga_satuan_tampil').val(to_rupiah(json.data['harga_satuan']));
                            $('#sub_total').val(json.data['harga_satuan']);
                            $('#sub_total_tampil').html(to_rupiah(json.data['harga_satuan']));

                            if(TipeCustomer == 'BENGKEL'){
                                JenisHarga      = 'GROSIR 1';
                            }else if(TipeCustomer == 'GROSIR'){
                                JenisHarga      = 'GROSIR 2';
                            }else if(TipeCustomer == 'MRC'){
                                JenisHarga      = 'MRC';
                                // $('#harga_satuan_tampil').removeAttr('readonly').val();
                            }

                            if($('#harga_satuan').val() >= 0){
                                $('#jumlah_beli').removeAttr('readonly');
                                $('#jumlah_beli').removeAttr('disabled').val('');
                                $('#discount').removeAttr('readonly');
                                $('#discount').removeAttr('disabled').val('0.00');
                                $('#discount_tampil').removeAttr('readonly');
                                $('#discount_tampil').removeAttr('disabled').val('Rp. 0');
                                $('#SimpanDetail').removeAttr('disabled');
                                $('#jumlah_beli').focus();
                            }
                        }else if(json.status == 0){
                            $('#jumlah_beli').val('');
                            $('#sub_total').val($('#harga_satuan').val());
                            $('#sub_total_tampil').html($('#harga_satuan_tampil').val());

                            swal({
                                title: "Oops !",
                                text: json.pesan,
                                type: "error",
                                confirmButtonClass: 'btn-danger btn-md waves-effect waves-light',
                                confirmButtonText: "Ok"
                            },function(isConfirm){
                                window.location.href = url;
                            });
                        }
                    }, 
                    error: function (jqXHR, textStatus, errorThrown){
                        swal({
                            title: "Oops !",
                            text: "Data barang gagal ditampilkan.",
                            type: "error",
                            confirmButtonClass: 'btn-danger btn-md waves-effect waves-light',
                            confirmButtonText: "Ok"
                        },function(isConfirm){
                            window.location.href = url;
                        });
                    }
                });
            }
        }else{
            // // Bersihkan data pemilihan barang
            // $('#id_barang_pusat').html('-');
            // $('#nama_barang').html('Belum ada barang yg dipilih');
            // $('#jumlah_beli').prop('readonly', true);
            // $('#jumlah_beli').prop('disabled', true).val('');
            // $('#SimpanDetail').prop('disabled', true);
            // $('#harga_satuan').val('0');
            // $('#harga_satuan_tampil').prop('readonly', true);
            // $('#harga_satuan_tampil').prop('disabled', true).val('Rp. 0');
            // $('#discount').prop('readonly', true);
            // $('#discount').prop('disabled', true).val('0.00');
            // $('#discount_tampil').prop('readonly', true);
            // $('#discount_tampil').prop('disabled', true).val('Rp. 0');
            // $('#discount_hidden').val('0');
            // $('#harga_bersih').val('Rp. 0');
            // $('#sub_total').val('0');
            // $('#sub_total_tampil').html('Rp. 0');

            // $('div#hasil_pencarian_barang').hide();
        }
    });

    $(document).on('keyup', '#pencarian_customer', function(e){
        var charCode = e.which || e.keyCode;
        if(charCode == 40){
            if($('div#hasil_pencarian_customer li.autocomplete_active-customer').length > 0){
                var Selanjutnya = $('div#hasil_pencarian_customer li.autocomplete_active-customer').next();
                $('div#hasil_pencarian_customer li.autocomplete_active-customer').removeClass('autocomplete_active-customer');
                Selanjutnya.addClass('autocomplete_active-customer');
            }else{
                $('div#hasil_pencarian_customer li:first').addClass('autocomplete_active-customer');
            }
        }else if(charCode == 38){
            if($('div#hasil_pencarian_customer li.autocomplete_active-customer').length > 0){
                var Sebelumnya = $('div#hasil_pencarian_customer li.autocomplete_active-customer').prev();
                $('div#hasil_pencarian_customer li.autocomplete_active-customer').removeClass('autocomplete_active-customer');
                Sebelumnya.addClass('autocomplete_active-customer');
            }else{
                $('div#hasil_pencarian_customer li:first').addClass('autocomplete_active-customer');
            }
        }else if(charCode == 13){
            // Ambil dari daftar hasl pencarian
            var id_customer     = $('div#hasil_pencarian_customer li.autocomplete_active-customer span#id_customer').html();
            var nama_customer   = $('div#hasil_pencarian_customer li.autocomplete_active-customer span#nama_customer').html();
            var tipe_customer   = $('div#hasil_pencarian_customer li.autocomplete_active-customer span#tipe_customer').html();
            var hp_customer     = $('div#hasil_pencarian_customer li.autocomplete_active-customer span#hp_customer').html();
            var kode_customer   = $('div#hasil_pencarian_customer li.autocomplete_active-customer span#kode_customer').html();

            $('#hasil_pencarian_customer').hide();
            $('#id_customer_hidden').html(id_customer);
            $('#kode_customer_pusat').val(kode_customer);
            $('#pencarian_customer').val(nama_customer);
            $('#data_tipe_customer_pusat').html(tipe_customer);
            $('#data_handphone_customer_pusat').html(hp_customer);
            $('#pencarian_kode_barang').focus();
        }else{
            AutoCompletedCustomer($(this).width(), $(this).val(), $(this).parent().parent().index());
        }
    });

    $(document).on('click', '#harga_satuan_tampil', function(e){
        if($('#harga_satuan_tampil').val() !== 'Rp. 0'){
            if($('#data_tipe_customer_pusat').html() !== 'MRC'){
                AutoCompletedHarga($(this).width());
            }
        }else{
            $('div#hasil_pencarian_harga').hide();
        }
    });

    $(document).on('keyup', '#harga_satuan_tampil', function(e){
        var charCode = e.which || e.keyCode;
        if(charCode == 13){
            $('#jumlah_beli').focus();
        }else{
            var PH   = $('#harga_satuan_tampil').val();
                PH   = to_angka(PH);

                if (PH>0){
                    $('#harga_satuan_tampil').val(to_rupiah(PH));
                    $('#harga_satuan').val(PH);
                }else{
                    $('#harga_satuan_tampil').val('');
                    $('#harga_satuan').val('0');
                }
                HitungSubtotalBarang();
        }
    });

    $(document).on('keyup', '#jumlah_beli', function(){
        // var jml = $('#jumlah_beli').val();
        //     jml = to_angka(jml);

        // if(jml > 0){
        //     $('#jumlah_beli').val('0');
        //     $('#jumlah_beli').val(to_rupiah(jml));
        // }else{
        //     $('#jumlah_beli').val('');
        // }

        HitungSubtotalBarang();
    });

    $(document).on('keydown', '#jumlah_beli', function(e){
        var charCode = e.which || e.keyCode;
        if(charCode == 9){
            $('#discount').focus();
            return false;
        }

        if(charCode == 13){
            setTimeout(function(){
                konfirmasi_simpan_barang();
            }, 100);
        }
    });

    function konfirmasi_simpan_barang(){
        if($('#jumlah_beli').val() !== ''){
            $('#jumlah_beli').val(to_angka($('#jumlah_beli').val()));
        }

        if($('#harga_satuan').val() == '0'){
            swal({
                title: "Oops !",
                text: "Harap masukan harga satuan.",
                type: "error",
                confirmButtonClass: 'btn-danger btn-md waves-effect waves-light',
                confirmButtonText: "Ok"
            },function (isConfirm){
                setTimeout(function(){
                    HitungSubtotalBarang();
                    $('#harga_satuan_tampil').focus();
                }, 100);
            });
            return;
        }

        if($('#jumlah_beli').val() == '0' || $('#jumlah_beli').val() == ''){
            swal({
                title: "Oops !",
                text: "Harap masukan jumlah beli.",
                type: "error",
                confirmButtonClass: 'btn-danger btn-md waves-effect waves-light',
                confirmButtonText: "Ok"
            },function (isConfirm){
                setTimeout(function(){
                    HitungSubtotalBarang();
                    $('#jumlah_beli').focus();
                }, 100);
            });
            return;
        }else if($('#jumlah_beli').val() !== ''){
            HitungSubtotalBarang();
            $('.modal-dialog').removeClass('modal-full');
            $('.modal-dialog').removeClass('modal-lg');
            $('.modal-dialog').addClass('modal-sm');
            $('#ModalHeader').html('Konfirmasi');
            $('#ModalContent').html(
                                        "<b class='text-dark'>Apakah anda yakin ingin menyimpan penjualan barang ini ? </b></br></br>" +
                                        "SKU : </br>" +

                                        "<b class='text-dark'>" + $('#pencarian_kode_barang').val() + "</b></br></br>" +

                                        "Nama Barang : </br>" +
                                        "<b class='text-dark'>" + $('#nama_barang').html() + "</b></br></br>" +

                                        "Jumlah Beli : </br>" +
                                        "<h2 class='text-dark'>" + $('#jumlah_beli').val() + "</h2>"
                                    );
            $('#ModalFooter').html("<button type='button' class='btn btn-primary' id='SimpanDataBarang'>Ya, saya yakin</button><button type='button' class='btn btn-default' data-dismiss='modal'>Batal</button>");
            $('#ModalGue').modal('show');

            setTimeout(function(){
                $('button#SimpanDataBarang').focus();
            }, 500);
        }else{
            swal({
                title: "Oops !",
                text: "Harap masukan jumlah beli.",
                type: "error",
                confirmButtonClass: 'btn-danger btn-md waves-effect waves-light',
                confirmButtonText: "Ok"
            },function (isConfirm){
                setTimeout(function(){
                    HitungSubtotalBarang();
                    $('#jumlah_beli').focus();
                }, 100);
            });
        }
    }

    $(document).on('click', 'button#SimpanDetail', function(){
        HitungSubtotalBarang();
        konfirmasi_simpan_barang();
    });

    $(document).on('click', 'button#SimpanDataBarang', function(){
        $('#ModalGue').modal('hide');
        simpan_detail();
    });

    $(document).on('click', 'button#info_stok', function(){
        $('#ModalGue').modal('hide');
        $('#jumlah_beli').focus();
    });

    $(document).on('keydown', '#discount', function(e){
        var charCode = e.which || e.keyCode;
        if(charCode == 9){
            $('#discount_tampil').focus();
            return false;
        }else if(charCode == 13){
            setTimeout(function(){
                HitungSubtotalBarang();
                konfirmasi_simpan_barang();
            }, 100);
        }
    });

    $(document).on('keydown', '#discount_tampil', function(e){
        var charCode = e.which || e.keyCode;
        if(charCode == 9){
            $('#jumlah_beli').focus();
            return false;
        }else if(charCode == 13){
            setTimeout(function(){
                HitungSubtotalBarang2();
                konfirmasi_simpan_barang();
            }, 100);
        }
    });

    $(document).on('keyup', '#discount', function(){
        HitungSubtotalBarang();
    });

    $(document).on('keyup', '#discount_tampil', function(){
        var hrg_disc    = $('#discount_tampil').val();
            hrg_disc    = to_angka(hrg_disc);

        if(hrg_disc > 0){
            $('#discount_tampil').val(to_rupiah(hrg_disc));
            $('#discount_hidden').val(hrg_disc);
        }else{
            $('#discount_tampil').val('');
            $('#discount_hidden').val('0');
        }

        HitungSubtotalBarang2();
    });

    $(document).on('keyup', '#BiayaLain', function(){
        var BL   = $('#BiayaLain').val();
            BL   = to_angka(BL);

        if(BL>0){
            $('#BiayaLain').val(to_rupiah(BL));
            $('#BiayaLainHidden').val(BL);
        }else{
            $('#BiayaLain').val('');
            $('#BiayaLainHidden').val('0');
        }
        HitungTotalBayar();
    });

    $(document).on('keyup', '#UangCash', function(){
        var Cash   = $('#UangCash').val();
            Cash   = to_angka(Cash);

        if (Cash>0){
            $('#UangCash').val(to_rupiah(Cash));
            $('#UangCashHidden').val(Cash);
        }else{
            $('#UangCash').val('');
            $('#UangCashHidden').val('0');
        }
        HitungTotalKembalian();
    });

    $(document).on('change', '#TotalDebit', function(){
        var Cash        = '0';
        var GrandTotal  = $('#GrandTotalHidden').val();
        var Debit       = $('#TotalDebit').html();
            Debit       = to_angka(Debit);

        if (Debit>0){
            $('#TotalDebit').html(to_rupiah(Debit));
            $('#TotalDebitHidden').val(Debit);

            var Cash = parseInt(parseInt(GrandTotal)-parseInt(Debit));
            if(Cash <=0){
                $('#UangCash').val('0');
                $('#UangCashHidden').val('0');
            }else{
                $('#UangCash').val(to_rupiah(Cash));
                $('#UangCashHidden').val(Cash);
            }

        }else{
            $('#TotalDebit').html('');
            $('#TotalDebitHidden').val('0');
            $('#UangCash').val(to_rupiah(GrandTotal));
            $('#UangCashHidden').val(GrandTotal);
        }
        HitungTotalKembalian();
    });

    $(document).on('click', '#TotalDebit', function(e){
        e.preventDefault();

        if ($('#GrandTotalHidden').val() !=='0'){
            $('.modal-dialog').removeClass('modal-full');
            $('.modal-dialog').removeClass('modal-lg');
            $('.modal-dialog').removeClass('modal-md');
            $('.modal-dialog').removeClass('modal-sm');
            $('.modal-dialog').addClass('modal-lg');
            $('#ModalHeader').html('Input Pembayaran Debit');
            $('#ModalContent').load($(this).attr('href'));
            $('#ModalGue').modal('show');
        }else{
            $('.modal-dialog').removeClass('modal-lg');
            $('.modal-dialog').addClass('modal-sm');
            $('#ModalHeader').html('Informasi Input Debit');
            $('#ModalContent').html("Belum ada barang yang anda simpan di penjualan ini");
            $('#ModalFooter').html("<button type='button' class='btn btn-default' data-dismiss='modal'>Tutup</button>");
            $('#ModalGue').modal('show');
        }
    });

    $(document).on('keyup', '#PersentasePpn', function(){
        HitungTotalBayar();
    });

    $(document).on('keydown', 'body', function(e){
        var charCode = ( e.which ) ? e.which : event.keyCode;

        if(charCode == 117) //F6
        {
            tampilkan_data_barang();
            $('.modal-dialog').removeClass('modal-full');
            $('.modal-dialog').removeClass('modal-sm');
            $('.modal-dialog').removeClass('modal-md');
            $('.modal-dialog').addClass('modal-lg');
            if($('#data_tipe_customer_pusat').html() == "MRC"){
                $('#ModalHeaderCariBarang').html('CARI BARANG TOKO : <b class="text-primary">' +
                                                 $('#pencarian_customer').val() +
                                                 '</b>');
            }else{
                $('#ModalHeaderCariBarang').html('CARI BARANG PUSAT');
            }
            $('#ModalCariBarang').modal('show');
            return false;
        }

        if(charCode == 118) //F7
        {
            $('#catatan').focus();
            return false;
        }

        if(charCode == 119) //F8
        {
            konfirmasi_tahan_transaksi();
            return false;
        }

        if(charCode == 120) //F9
        {
            konfirmasi_simpan_transaksi();
            return false;
        }

        if(charCode == 121) //F10
        {
            CetakStruk();
            return false;
        }
    });

    $(document).on('click', 'body', function(){
        $('div#hasil_pencarian_barang').hide();
        $('div#hasil_pencarian_customer').hide();
        $('div#hasil_pencarian_harga').hide();
    });

    function konfirmasi_simpan_transaksi(){
        if($('#jumlah_barang').html() > 0){
            // Validasi customer
            if($('#id_customer_hidden').html() == '' | $('#id_customer_hidden').html() == '0'){
                swal({
                    title: "Oops !",
                    text: "Harap pilih customer terlebih dahulu!",
                    type: "error",
                    confirmButtonClass: 'btn-danger btn-md waves-effect waves-light',
                    confirmButtonText: "Ok"
                }, function(){
                    setTimeout(function(){
                        $('#pencarian_customer').focus();
                    }, 100);
                });
                return;
            }
            
            // Validasi uang cash + uang debit harus sama dengan grandtotal
            var Gt          = $('#GrandTotalHidden').val();
            var Cash        = $('#UangCashHidden').val();
            var Debit       = $('#TotalDebitHidden').val();

            if(Cash==''){
                Cash = 0;
            }

            if(Debit==''){
                Debit = 0;
            }

            var Total       = parseInt(parseInt(Cash)+parseInt(Debit));
            if(Total==''){
                Total = 0;
            }

            if(Total >= Gt){
                swal({
                    title: "Apakah anda yakin ingin menyimpan penjualan ini ?",
                    type: "info",
                    showCancelButton: true,
                    confirmButtonClass: 'btn-primary btn-md waves-effect waves-light',
                    confirmButtonText: "Iya, saya yakin",
                    cancelButtonText: "Batal",
                },function (isConfirm){
                    if(isConfirm){
                        setTimeout(function(){
                            SimpanTransaksi();
                        }, 100);
                    }
                });
            }else if(Total < Gt){
                swal({
                    title: "Oops !",
                    text: "Total bayar kurang dari Grand Total. Harap periksa kembali uang cash dan debit.",
                    type: "error",
                    confirmButtonClass: 'btn-danger btn-md waves-effect waves-light',
                    confirmButtonText: "Ok"
                }, function(){
                    setTimeout(function(){
                        $('#UangCash').focus();
                    }, 100);
                });
                return;
            }else{
                swal({
                    title: "Oops !",
                    text: "Harap masukan uang cash atau debit.",
                    type: "error",
                    confirmButtonClass: 'btn-danger btn-md waves-effect waves-light',
                    confirmButtonText: "Ok"
                }, function(){
                    setTimeout(function(){
                        $('#UangCash').focus();
                    }, 100);
                });
                return;
            }
        }else{
            swal({
                title: "Oops !",
                text: "Belum ada barang yang anda simpan ditransaksi ini, harap simpan barang terlebih dahulu.",
                type: "error",
                confirmButtonClass: 'btn-danger btn-md waves-effect waves-light',
                confirmButtonText: "Ok"
            }, function(){
                setTimeout(function(){
                    $('#pencarian_kode_barang').focus();
                }, 100);
            });
        }
    }

    $(document).on('click', 'button#Simpan', function(){
        konfirmasi_simpan_transaksi();
    });

    function konfirmasi_tahan_transaksi(){
        if($('#jumlah_barang').html() > 0){
            // Validasi customer
            if($('#id_customer_hidden').html() == '' | $('#id_customer_hidden').html() == '0'){
                swal({
                    title: "Oops !",
                    text: "Harap pilih customer terlebih dahulu!",
                    type: "error",
                    confirmButtonClass: 'btn-danger btn-md waves-effect waves-light',
                    confirmButtonText: "Ok"
                }, function(){
                    setTimeout(function(){
                        $('#pencarian_customer').focus();
                    }, 100);
                });
                return;
            }

            // Validasi uang cash + uang debit harus sama dengan grandtotal
            var Gt          = $('#GrandTotalHidden').val();
            var Cash        = $('#UangCashHidden').val();
            var Debit       = $('#TotalDebitHidden').val();

            if(Cash == ''){
                Cash=0; }

            if(Debit == ''){
                Debit=0; }

            var Total       = parseInt(parseInt(Cash)+parseInt(Debit));
            if(Total == ''){
                Total=0; }

            if(Total >= Gt){
                swal({
                    title: "Apakah anda yakin ingin menahan penjualan ini ?",
                    type: "info",
                    showCancelButton: true,
                    confirmButtonClass: 'btn-primary btn-md waves-effect waves-light',
                    confirmButtonText: "Iya, saya yakin",
                    cancelButtonText: "Batal",
                },function (isConfirm){
                    if(isConfirm){
                        setTimeout(function(){
                            TahanTransaksi();
                        }, 100);
                    }
                });
            }else if(Total < Gt){
                swal({
                    title: "Oops !",
                    text: "Total bayar kurang dari Grand Total. Harap periksa kembali uang cash dan debit",
                    type: "error",
                    confirmButtonClass: 'btn-danger btn-md waves-effect waves-light',
                    confirmButtonText: "Ok"
                }, function(){
                    setTimeout(function(){
                        $('#UangCash').focus();
                    }, 100);
                });
                return;
            }else{
                swal({
                    title: "Oops !",
                    text: "Harap masukan uang cash atau debit",
                    type: "error",
                    confirmButtonClass: 'btn-danger btn-md waves-effect waves-light',
                    confirmButtonText: "Ok"
                }, function(){
                    setTimeout(function(){
                        $('#UangCash').focus();
                    }, 100);
                });
                return;
            }
        }else{
            swal({
                title: "Oops !",
                text: "Belum ada barang yang anda simpan ditransaksi ini, harap simpan barang terlebih dahulu.",
                type: "error",
                confirmButtonClass: 'btn-danger btn-md waves-effect waves-light',
                confirmButtonText: "Ok"
            }, function(){
                setTimeout(function(){
                    $('#pencarian_kode_barang').focus();
                }, 100);
            });
        }
    }

    $(document).on('click', 'button#Tahan', function(){
        konfirmasi_tahan_transaksi();
    });

    $(document).on('click', 'button#Laporan', function(){
        window.open("laporan_penjualan_pusat",'_blank');
    });

    $(document).on('click', 'button#CetakStruk', function(){
        CetakStruk();
        return false;
    });

    function pulihkan_data_detail(id_penjualan_b){
        $.ajax({
            url: "<?php echo site_url('penjualan/ajax_pulihkan_data_detail'); ?>",
            type: "POST",
            cache: false,
            data: 'id_penjualan_b=' + id_penjualan_b,
            dataType:'json',
            success: function(json){
                if(json.status == 1){
                    $('#id_barang_pusat').html(json.data['id_barang_pusat']);
                    $('#pencarian_kode_barang').val(json.data['sku']);
                    $('#nama_barang').html(json.data['nama_barang']);
                    $('#harga_satuan').val(json.data['harga_satuan']);
                    $('#harga_satuan_tampil').removeAttr('readonly').val(to_rupiah(json.data['harga_satuan']));
                    $('#harga_satuan_tampil').removeAttr('disabled');
                    $('#jenis_harga').val(json.data['jenis_harga']);
                    $('#jumlah_beli').removeAttr('disabled').val(json.data['jumlah_beli']);
                    $('#discount').removeAttr('disabled').val(json.data['discount']);
                    $('#discount_tampil').removeAttr('readonly');
                    $('#discount_tampil').removeAttr('disabled').val('0');
                    $('#sub_total').val(json.data['subtotal']);
                    $('#sub_total_tampil').html(to_rupiah(json.data['subtotal']));
                    $('#SimpanDetail').removeAttr('disabled');
                    HitungSubtotalBarang();

                    var TipeCustomer = $('#data_tipe_customer_pusat').html();
                    if(TipeCustomer == 'MRC'){
                        var Harganya    = $(this).find('span#eceran_toko').html();
                        JenisHarga      = 'MRC';
                        // $('#harga_satuan_tampil').removeAttr('readonly').val();
                        $('#harga_satuan_tampil').removeAttr('disabled').val();
                    }
                    $('#jumlah_beli').focus();
                }else if(json.status == 0){
                    swal({
                        title: "Oops !",
                        text: json.pesan,
                        type: "error",
                        confirmButtonClass: 'btn-danger btn-md waves-effect waves-light',
                        confirmButtonText: "Ok"
                    },function(isConfirm){
                        window.location.href = url;
                    });
                }
            }, error: function (jqXHR, textStatus, errorThrown){
                swal({
                    title: "Oops !",
                    text: "Data penjualan barang gagal dipulihkan.",
                    type: "error",
                    confirmButtonClass: 'btn-danger btn-md waves-effect waves-light',
                    confirmButtonText: "Ok"
                },function(isConfirm){
                    window.location.href = url;
                });
            }
        });
    }

    function simpan_detail(){
        $('#jumlah_beli').prop('disabled', true);
        $('#SimpanDetail').prop('disabled', true);
        var FormData = "no_penjualan="+$('#no_penjualan').html();
            FormData += "&id_penjualan_m="+ $('#id_penjualan_m').val();
            FormData += "&id_customer_pusat="+$('#id_customer_hidden').html();
            FormData += "&tipe_customer_pusat="+$('#data_tipe_customer_pusat').html();
            FormData += "&catatan="+encodeURI($('#catatan').val());
            FormData += "&biaya_lain="+$('#BiayaLainHidden').val();
            FormData += "&ppn="+$('#TotalPpnHidden').val();
            FormData += "&tunai="+$('#UangCashHidden').val();
            FormData += "&debit="+$('#TotalDebitHidden').val();

            FormData += "&id_barang_pusat="+ $('#id_barang_pusat').html();
            FormData += "&sku="+$('#pencarian_kode_barang').val();
            FormData += "&jenis_harga="+$('#jenis_harga').val();
            FormData += "&harga="+$('#harga_satuan').val();
            FormData += "&discount="+$('#discount').val();
            FormData += "&jumlah_beli="+to_angka($('#jumlah_beli').val());
            FormData += "&subtotal="+$('#sub_total').val();
            FormData += "&url="+ url;

        $.ajax({
            url: "<?php echo site_url('penjualan/simpan_detail'); ?>",
            type: "POST",
            cache: false,
            data: FormData,
            dataType:'json',
            success: function(data){
                if(data.status == 1){
                    if(url == 'penjualan'){
                        swal({
                            title: "Berhasil!", text: data.pesan, type: "success",
                            confirmButtonText: "Ok",
                        },function(isConfirm){
                            window.location.href = data.url;
                        });
                        return;
                    }else{
                        tampilkan_data();
                    }

                    $('#judul_tab_induk').html("Data Penjaualan : <span class='badge up bg-primary'>" +
                                                data.jumlah_barang +
                                               " Barang </span>");
                    $('#judul_tab_batal').html("Data Penjaualan Batal : <span class='badge up bg-danger'>" +
                                                data.jumlah_barang_batal +
                                               " Barang </span>");

                    // Tampilkan id penjualan master dan total bayar dll
                    if(data.tipe_customer_pusat == 'MRC'){
                        $('#pencarian_customer').prop('disabled', true);
                        $('#tombol_tambah_customer').prop('disabled', true);
                    }
                    $('#id_penjualan_m').val(data.id_pm);
                    $('#id_customer_hidden').html(data.id_customer_pusat);
                    $('#data_tipe_customer_pusat').html(data.tipe_customer_pusat);
                    $('#no_penjualan').html(data.no_penjualan);
                    $('#TotalBayar').html(to_rupiah(data.total));
                    $('#TotalBayarHidden').val(data.total);
                    $('#TotalPpn').html(to_rupiah(data.ppn));
                    $('#TotalPpnHidden').val(data.ppn);
                    $('#GrandTotal').html(to_rupiah(data.grand_total));
                    $('#GrandTotalHidden').val(parseInt(data.grand_total));
                    $('#jumlah_barang').html(data.jumlah_barang);
                    $('#UangCash').val(to_rupiah(data.grand_total));
                    $('#UangCashHidden').val(parseInt(data.grand_total));
                    $('#UangKembali').val('0');

                    // Bersihkan data pemilihan barang
                    $('#pencarian_kode_barang').val('');
                    $('#id_barang_pusat').html('-');
                    $('#nama_barang').html('Belum ada barang yg dipilih');
                    $('#harga_satuan').val('0');
                    $('#harga_satuan_tampil').prop('readonly', true);
                    $('#harga_satuan_tampil').prop('disabled', true).val('Rp. 0');
                    $('#jumlah_beli').prop('disabled', true).val('');
                    $('#discount').prop('disabled', true).val('0.00');
                    $('#discount_hidden').val('0');
                    $('#discount_tampil').prop('disabled', true).val('Rp. 0');
                    $('#discount_tampil').prop('readonly', true);
                    $('#harga_bersih').val('Rp. 0');
                    $('#sub_total').val('0');
                    $('#sub_total_tampil').html('Rp. 0');
                    $('#SimpanDetail').prop('disabled', true);

                    swal({
                        title: "Berhasil!", text: data.pesan, type: "success",
                        confirmButtonText: "Ok",
                    },function(isConfirm){
                        setTimeout(function(){
                            $('#pencarian_kode_barang').focus();
                        }, 100);
                    });

                }else if(data.status == 2){
                    swal({
                        title: "Oops !",
                        text: data.pesan,
                        type: "error",
                        confirmButtonClass: 'btn-danger btn-md waves-effect waves-light',
                        confirmButtonText: "Ok"
                    },function(isConfirm){
                        if(data.url !== ''){
                            window.location.href = data.url;
                        }
                    });

                }else if(data.status == 3){
                    $('#jumlah_beli').removeAttr('disabled');
                    $('#SimpanDetail').removeAttr('disabled');
                    $('.modal-dialog').removeClass('modal-lg');
                    $('.modal-dialog').addClass('modal-sm');
                    $('#ModalHeaderPesan').html('Oops !');
                    $('#ModalContentPesan').html(data.pesan);
                    $('#ModalFooterPesan').html("<button type='button' class='btn btn-primary' data-dismiss='modal' autofocus>Ok, Saya Mengerti</button>");
                    $('#ModalPesan').modal('show');
                }
            }, error: function (jqXHR, textStatus, errorThrown){
                swal({
                    title: "Oops !",
                    text: "Data penjualan barang gagal disimpan.",
                    type: "error",
                    confirmButtonClass: 'btn-danger btn-md waves-effect waves-light',
                    confirmButtonText: "Ok"
                },function(isConfirm){
                    window.location.href = url;
                });
            }
        });
    }

    function edit_penjualan_detail(id_penjualan_d)
    {
        $('#harga_satuan_tampil').prop('readonly', true).val('0');
        $('#harga_satuan_tampil').removeAttr('disabled');

        //Load data from ajax
        $.ajax({
            url : "<?php echo site_url('penjualan/ajax_edit'); ?>",
            type: "POST",
            cache: false,
            data: "id_penjualan_d=" + id_penjualan_d,
            dataType: "JSON",
            success: function(data){
                $('#id_barang_pusat').html(data.id_barang_pusat);
                $('#pencarian_kode_barang').val(data.sku);
                $('#nama_barang').html(data.nama_barang);
                $('#harga_satuan').val(data.harga_satuan);
                $('#harga_satuan_tampil').val(to_rupiah(data.harga_satuan));
                $('#jenis_harga').val(data.jenis_harga);
                $('#jumlah_beli').removeAttr('disabled').val(data.jumlah_beli);
                $('#discount').removeAttr('disabled').val(data.discount);
                $('#discount_tampil').removeAttr('readonly');
                $('#discount_tampil').removeAttr('disabled').val('0');
                $('#sub_total').val(data.subtotal);
                $('#sub_total_tampil').html(to_rupiah(data.subtotal));
                $('#SimpanDetail').removeAttr('disabled');
                HitungSubtotalBarang();

                var TipeCustomer = $('#data_tipe_customer_pusat').html();
                if(TipeCustomer == 'MRC'){
                    var Harganya    = $(this).find('span#eceran_toko').html();
                    JenisHarga      = 'MRC';
                    // $('#harga_satuan_tampil').removeAttr('readonly').val();
                    $('#harga_satuan_tampil').removeAttr('disabled').val();
                }
                $('#jumlah_beli').focus();
            }, error: function(jqXHR, textStatus, errorThrown){
                swal({
                    title: "Gagal!",
                    text: "Gagal mengambil data dari ajax.",
                    type: "error",
                    confirmButtonClass: 'btn-danger btn-md waves-effect waves-light',
                    confirmButtonText: "Ok"
                },function (isConfirm){
                    window.location.href=url;
                });
            }
        });
    }

    function verifikasi_hapus_penjualan_detail(id_penjualan_d)
    {
        //Ajax Load data from ajax
        $.ajax({
            url : "<?php echo site_url('penjualan/ajax_verifikasi_hapus_detail'); ?>",
            type: "POST",
            cache: false,
            data: "id_penjualan_d=" + id_penjualan_d,
            dataType: "JSON",
            success: function(data){
                if(data.status == 1){
                    $('.modal-dialog').removeClass('modal-lg');
                    $('.modal-dialog').addClass('modal-sm');
                    $('#ModalHeader').html('Informasi Hapus Barang');
                    $('#ModalContent').html(data.pesan);
                    $('#ModalFooter').html(data.footer);
                    $('#ModalGue').modal('show');
                }else if(data.status == 0){
                    swal({
                        title: 'Oops!',
                        text: data.pesan,
                        type: 'error',
                        confirmButtonClass: 'btn-danger btn-md waves-effect waves-light',
                        confirmButtonText: "Ok"
                    },function (isConfirm){
                        window.location.href = url;
                    });
                }
            },
            error: function (jqXHR, textStatus, errorThrown){
                swal({
                    title: "Gagal!",
                    text: "Data barang penjualan, gagal ditampilkan.",
                    type: "error",
                    confirmButtonClass: 'btn-danger btn-md waves-effect waves-light',
                    confirmButtonText: "Ok"
                },function (isConfirm){
                    window.location.href=url;
                });
            }
        });
    }

    function hapus_penjualan_detail(id_penjualan_d)
    {
        var FormData = "id_penjualan_d=" + id_penjualan_d;
            FormData += "&id_customer_pusat=" + $('#id_customer_hidden').html();
            FormData += "&tipe_customer_pusat=" + $('#data_tipe_customer_pusat').html();
            FormData += "&catatan=" + encodeURI($('#catatan').val());
            FormData += "&biaya_lain=" + $('#BiayaLainHidden').val();
            FormData += "&keterangan_batal=" + encodeURI($('#keterangan_batal').val());
            FormData += "&url=" + url;

        $.ajax({
            url : "<?php echo site_url('penjualan/ajax_hapus_detail'); ?>",
            type: "POST",
            data: FormData,
            dataType: "JSON",
            success: function(data){
                if(data.status == 1){
                    $('#TotalBayar').html(to_rupiah(data.total));
                    $('#TotalBayarHidden').val(data.total);
                    $('#TotalPpn').html(to_rupiah(data.ppn));
                    $('#TotalPpnHidden').val(data.ppn);
                    $('#GrandTotal').html(to_rupiah(data.grand_total));
                    $('#GrandTotalHidden').val(data.grand_total);
                    $('#jumlah_barang').html(data.jumlah_barang);
                    $('#UangCash').val(to_rupiah(data.grand_total));
                    $('#UangCashHidden').val(data.grand_total);
                    $('#UangKembali').val('0');

                    $('#judul_tab_induk').html("Data Penjualan : <span class='badge up bg-primary'>" +
                                                data.jumlah_barang +
                                               " Barang </span>");
                    $('#judul_tab_batal').html("Data Penjualan Batal : <span class='badge up bg-danger'>" +
                                                data.jumlah_barang_batal +
                                               " Barang </span>");

                    reload_table_induk();
                    swal({
                        title: "Berhasil!",
                        text: "Data penjualan barang berhasil dihapus.",
                        type: "success",
                        confirmButtonText: "Ok"
                    });
                }else if(data.status == 0){
                    swal({
                        title: data.judul,
                        text: data.pesan,
                        type: data.tipe_pesan,
                        confirmButtonClass: data.gaya_tombol,
                        confirmButtonText: "Ok"
                    },function (isConfirm){
                        window.location.href = url;
                    });
                }
            },
            error: function (jqXHR, textStatus, errorThrown){
                swal({
                    title: "Gagal!",
                    text: "Barang penjualan gagal dihapus.",
                    type: "error",
                    confirmButtonClass: 'btn-danger btn-md waves-effect waves-light',
                    confirmButtonText: "Ok"
                },function (isConfirm){
                    window.location.href = url;
                });
            }
        });
    }

    function tampilkan_total_penjualan()
    {
        var NomorNota   = $('#no_penjualan').html();

        $.ajax({
            url: "<?php echo site_url('penjualan/tampilkan_total_penjualan'); ?>",
            type: "POST",
            cache: false,
            data: '&no_penjualan=' + NomorNota,
            dataType:'json',
            success: function(data){
                if(data.status == 1){
                    // Bersihkan data
                    $('#id_penjualan_m').val(data.id_pm);
                    $('#pencarian_kode_barang').val('');
                    $('#nama_barang').html('Belum ada barang yg dipilih');
                    $('#harga_satuan').val('');
                    $('#harga_satuan_tampil').val('Rp. 0');
                    $('#jenis_harga').val('');
                    $('#jumlah_beli').val('0');
                    $('#discount').val('0.00');
                    $('#discount_hidden').val('');
                    $('#sub_total').val('');
                    $('#sub_total_tampil').html('Rp. 0');
                    $('#jumlah_beli').prop('disabled', true);
                    $('#discount').prop('disabled', true)
                }else if(data.status == 0){
                    $('.modal-dialog').removeClass('modal-lg');
                    $('.modal-dialog').addClass('modal-sm');
                    $('#ModalHeader').html('Oops !');
                    $('#ModalContent').html(data.pesan);
                    $('#ModalFooter').html("<button type='button' class='btn btn-primary' data-dismiss='modal' autofocus>Ok, Saya Mengerti</button>");
                    $('#ModalGue').modal('show');
                }
            }
        });
    }

    function SimpanTransaksi()
    {
        var FormData = "no_penjualan=" + $('#no_penjualan').html();
            FormData += "&id_penjualan_m=" + $('#id_penjualan_m').val();
            FormData += "&id_customer_pusat=" + $('#id_customer_hidden').html();
            FormData += "&catatan=" + encodeURI($('#catatan').val());
            FormData += "&total=" + $('#TotalBayarHidden').val();
            FormData += "&biaya_lain=" + $('#BiayaLainHidden').val();
            FormData += "&ppn=" + $('#TotalPpnHidden').val();
            FormData += "&grand_total=" + $('#GrandTotalHidden').val();
            FormData += "&tunai=" + $('#UangCashHidden').val();
            FormData += "&debit=" + $('#TotalDebitHidden').val();
            FormData += "&status=" +  'simpan';
            FormData += "&url=" + url;

        $.ajax({
            url: "<?php echo site_url('penjualan/simpan_transaksi'); ?>",
            type: "POST",
            cache: false,
            data: FormData,
            dataType:'json',
            success: function(data){
                if(data.status == 1){
                    swal({
                        title: "Berhasil!",
                        text: data.pesan,
                        type: "success",
                        confirmButtonText: "Iya, cetak faktur",
                        cancelButtonText: "Tidak, nanti saja",
                        showCancelButton: true
                    },function (isConfirm){
                        if(isConfirm) {
                            var FormData = "&id_penjualan_m="+$('#id_penjualan_m').val();
                            window.open("<?php echo site_url('faktur/faktur_penyiapan_barang/?'); ?>" + FormData,'_blank');
                            window.location.href = "penjualan";
                        }else{
                            window.location.href = "penjualan";
                        }
                    });
                }else if(data.status == 0){
                    swal({
                        title: "Oops !",
                        text: data.pesan,
                        type: "error",
                        confirmButtonClass: 'btn-danger btn-md waves-effect waves-light',
                        confirmButtonText: "Ok"
                    },function (isConfirm){
                        if(isConfirm){
                            window.location.href = data.url;
                        }
                    });
                }
            },
            error: function (jqXHR, textStatus, errorThrown){
                swal({
                    title: "Oops !",
                    text: "Gagal menyimpan transaksi penjualan ini.",
                    type: "error",
                    confirmButtonClass: 'btn-danger btn-md waves-effect waves-light',
                    confirmButtonText: "Ok"
                },function (isConfirm){
                    if(isConfirm){
                        window.location.href = url;
                    }
                });
            }
        });
    }

    function TahanTransaksi()
    {
        var FormData = "no_penjualan="+$('#no_penjualan').html();
            FormData += "&id_penjualan_m="+$('#id_penjualan_m').val();
            FormData += "&id_customer_pusat="+$('#id_customer_hidden').html();
            FormData += "&catatan="+encodeURI($('#catatan').val());
            FormData += "&total="+$('#TotalBayarHidden').val();
            FormData += "&biaya_lain="+$('#BiayaLainHidden').val();
            FormData += "&ppn="+$('#TotalPpnHidden').val();
            FormData += "&grand_total="+$('#GrandTotalHidden').val();
            FormData += "&tunai="+$('#UangCashHidden').val();
            FormData += "&debit="+$('#TotalDebitHidden').val();

        $.ajax({
            url: "<?php echo site_url('penjualan/tahan_transaksi'); ?>",
            type: "POST",
            cache: false,
            data: FormData,
            dataType:'json',
            success: function(data){
                if(data.status == 1){
                    swal({
                        title: "Berhasil!",
                        text: data.pesan,
                        type: "success",
                        confirmButtonText: "Ok"
                    },function (isConfirm){
                        if(isConfirm){
                            window.location.href = "penjualan";
                        }
                    });
                }else if(data.status == 0){
                    swal({
                        title: "Oops !",
                        text: data.pesan,
                        type: "error",
                        confirmButtonClass: 'btn-danger btn-md waves-effect waves-light',
                        confirmButtonText: "Ok"
                    },function (isConfirm){
                        if(isConfirm){
                            window.location.href = data.url;
                        }
                    });
                }
            },
            error: function (jqXHR, textStatus, errorThrown){
                swal({
                    title: "Oops !",
                    text: "Gagal menahan transaksi penjualan ini.",
                    type: "error",
                    confirmButtonClass: 'btn-danger btn-md waves-effect waves-light',
                    confirmButtonText: "Ok"
                },function (isConfirm){
                    if(isConfirm){
                        window.location.href = url;
                    }
                });
            }
        });
    }

    function AutoCompletedBarang(KataKunci)
    {
        $('#induk_pencarian_barang').removeClass('has-warning');
        $('#induk_pencarian_barang').addClass('has-primary');

        $('#icon_pencarian_barang').removeClass('fa fa-search');
        $('#icon_pencarian_barang').addClass('fa fa-spin fa-refresh');

        $('#tombol_scan_barang').removeClass('btn-warning');
        $('#tombol_scan_barang').addClass('btn-primary');
        $('#tombol_scan_barang').prop('disabled', true);

        $('#id_barang_pusat').html('-');
        $('#nama_barang').html('Belum ada barang yg dipilih');
        $('#harga_satuan').val('0');
        $('#harga_satuan_tampil').prop('readonly', true);
        $('#harga_satuan_tampil').prop('disabled', true).val('Rp. 0');
        $('#jumlah_beli').prop('disabled', true).val('');
        $('#discount').prop('disabled', true).val('0.00');
        $('#discount_hidden').val('0');
        $('#discount_tampil').prop('disabled', true).val('Rp. 0');
        $('#discount_tampil').prop('readonly', true);
        $('#harga_bersih').val('Rp. 0');
        $('#sub_total').val('0');
        $('#sub_total_tampil').html('Rp. 0');
        $('#SimpanDetail').prop('disabled', true);

        $('div#hasil_pencarian_barang').hide();

        delay(function () {
            var id_pm           = $('#id_penjualan_m').val();
            var TipeCustomer    = $('#data_tipe_customer_pusat').html();

            if(TipeCustomer === 'MRC'){
                var KodeToko = $('#kode_customer_pusat').val();
            }else{
                var KodeToko = ''
            }

            $.ajax({
                url: "<?php echo site_url('penjualan/ajax_kode'); ?>",
                type: "POST",
                cache: false,
                data: 'keyword=' + KataKunci +
                      '&id_pm=' + id_pm +
                      '&tipe_customer=' + TipeCustomer +
                      '&kode_toko=' + KodeToko,
                dataType:'json',
                success: function(json){
                    if(json.status == 1){
                        $('#icon_pencarian_barang').removeClass('fa fa-spin fa-refresh');
                        $('#icon_pencarian_barang').addClass('fa fa-search');
                        $('#tombol_scan_barang').removeAttr('disabled');

                        $('div#hasil_pencarian_barang').show('fast');
                        $('div#hasil_pencarian_barang').html(json.datanya);
                    }else{
                        $('#icon_pencarian_barang').removeClass('fa fa-spin fa-refresh');
                        $('#icon_pencarian_barang').addClass('fa fa-search');

                        $('#induk_pencarian_barang').removeClass('has-primary');
                        $('#induk_pencarian_barang').addClass('has-warning');

                        $('#tombol_scan_barang').removeClass('btn-primary');
                        $('#tombol_scan_barang').addClass('btn-warning');
                        $('#tombol_scan_barang').removeAttr('disabled');
                    }
                }
            });
        }, 1000);
    }

    function AutoCompletedCustomer(Lebar, KataKunci, Indexnya)
    {
        // Awal bersihkan data pemilihan barang
        $('#id_barang_pusat').html('-');
        $('#pencarian_kode_barang').val('');
        $('#nama_barang').html('Belum ada barang yg dipilih');
        $('#jumlah_beli').prop('readonly', true);
        $('#jumlah_beli').prop('disabled', true).val('');
        $('#SimpanDetail').prop('disabled', true);
        $('#harga_satuan').val('0');
        $('#harga_satuan_tampil').prop('readonly', true);
        $('#harga_satuan_tampil').prop('disabled', true).val('Rp. 0');
        $('#discount').prop('readonly', true);
        $('#discount').prop('disabled', true).val('0.00');
        $('#discount_tampil').prop('readonly', true);
        $('#discount_tampil').prop('disabled', true).val('Rp. 0');
        $('#discount_hidden').val('0');
        $('#harga_bersih').val('Rp. 0');
        $('#sub_total').val('0');
        $('#sub_total_tampil').html('Rp. 0');
        // Akhir bersihkan data pemilihan barang

        $('#tombol_tambah_customer').removeClass('btn-warning');
        $('#tombol_tambah_customer').addClass('btn-success');

        $('#induk_pencarian_customer').removeClass('has-warning');
        $('#induk_pencarian_customer').addClass('has-success');

        $('#icon_pencarian_customer').removeClass('fa fa-search');
        $('#icon_pencarian_customer').removeClass('mdi mdi-account-plus');
        $('#icon_pencarian_customer').addClass('fa fa-spin fa-refresh');

        $('div#hasil_pencarian_customer').hide();
        delay(function(){
            var Lebar   = Lebar + 25;
            $.ajax({
                url: "<?php echo site_url('penjualan/ajax_pencarian_customer'); ?>",
                type: "POST",
                cache: false,
                data: 'keyword=' + KataKunci +
                      '&id_penjualan_m=' + $('#id_penjualan_m').val(),
                dataType:'json',
                success: function(json){
                    if(json.status == 1){
                        $('#icon_pencarian_customer').removeClass('fa fa-spin fa-refresh');
                        <?php if($access_customer == 1){ ?>
                            $('#icon_pencarian_customer').addClass('mdi mdi-account-plus');
                        <?php }else{ ?>
                            $('#icon_pencarian_customer').addClass('fa fa-search');
                        <?php } ?>

                        if(json.tipe_customer_pusat == 'MRC'){
                            $('#id_customer_hidden').html(json.id_customer_pusat);
                            $('#pencarian_customer').val(json.nama_customer_pusat);
                            $('#kode_customer_pusat').val(json.kode_customer_pusat);
                            $('#data_handphone_customer_pusat').val(json.handphone_customer_pusat);
                            $('#data_tipe_customer_pusat').val(json.tipe_customer_pusat);
                            return;
                        }

                        $('div#hasil_pencarian_customer').show('fast');
                        $('div#hasil_pencarian_customer').html(json.datanya);

                        // Bersihkan data customer yang dipilih
                        $('#id_customer_hidden').html('');
                        $('#kode_customer_pusat').val('');
                        $('#data_tipe_customer_pusat').html('-');
                        $('#data_handphone_customer_pusat').html('-');
                    }else{
                        $('#tombol_tambah_customer').removeClass('btn-success');
                        $('#tombol_tambah_customer').addClass('btn-warning');

                        $('#icon_pencarian_customer').removeClass('fa fa-spin fa-refresh');
                        <?php if($access_customer == 1){ ?>
                            $('#icon_pencarian_customer').addClass('mdi mdi-account-plus');
                        <?php }else{ ?>
                            $('#icon_pencarian_customer').addClass('fa fa-search');
                        <?php } ?>

                        $('#induk_pencarian_customer').removeClass('has-success');
                        $('#induk_pencarian_customer').addClass('has-warning');
                    }
                }
            });
        }, 500);
    }

    function AutoCompletedHarga(Lebar)
    {
        $('div#hasil_pencarian_harga').hide();
        var Lebar   = Lebar + 25;
        var SKU     = $('#pencarian_kode_barang').val();

        $.ajax({
            url: "<?php echo site_url('penjualan/ajax_pencarian_harga'); ?>",
            type: "POST",
            cache: false,
            data: 'sku=' + SKU,
            dataType:'json',
            success: function(json){
                if(json.status == 1){
                    $('div#hasil_pencarian_harga').show('fast');
                    $('div#hasil_pencarian_harga').html(json.datanya);
                }
            }
        });
    }

    function HitungSubtotalBarang()
    {
        var Harga           = $('#harga_satuan').val();
        var JumlahBeli      = to_angka($('#jumlah_beli').val());
        var Disc            = $('#discount').val();
        var Harga_Discount  = Disc * parseInt(Harga);
        var Harga_Bersih    = parseInt(Harga) - parseInt(Harga_Discount);

        if(Harga_Discount > 0){
            $('#discount_tampil').val(to_rupiah(Harga_Discount));
            $('#discount_hidden').val(Harga_Discount);
            $('#harga_bersih').val(to_rupiah(Harga_Bersih));
        }else{
            $('#discount_tampil').val('Rp. 0');
            $('#discount_hidden').val('0');
            $('#harga_bersih').val(to_rupiah(Harga));
        }

        var SubTotal        = parseInt(Harga-Harga_Discount) * parseInt(JumlahBeli);
        var SabTotalRp      = 'Rp. 0';
        if(SubTotal >= '0'){
            SabTotalRp  = to_rupiah(SubTotal);
        }

        $('#sub_total').val(SubTotal);
        $('#sub_total_tampil').html(SabTotalRp);
    }

    function HitungSubtotalBarang2()
    {
        var Harga               = $('#harga_satuan').val();
        var JumlahBeli          = to_angka($('#jumlah_beli').val());
        var Disc                = $('#discount_hidden').val();
        var Persen_Discount     = Disc / parseInt(Harga);
        var Harga_Bersih        = parseInt(Harga) - parseInt(Disc);

        if (Persen_Discount > 0){
            $('#discount').val(Persen_Discount);
            $('#harga_bersih').val(to_rupiah(Harga_Bersih));
        }else{
            $('#discount').val('0.00');
            $('#discount_hidden').val('0');
            $('#harga_bersih').val('Rp. 0');
        }

        var SubTotal            = parseInt(Harga-Disc) * parseInt(JumlahBeli);
        var SabTotalRp          = 'Rp. 0';
        if(SubTotal >= '0'){
            SabTotalRp          = to_rupiah(SubTotal);
        }

        $('#sub_total').val(SubTotal);
        $('#sub_total_tampil').html(SabTotalRp);
    }

    function HitungTotalBayar()
    {
        var HitungPpn           = 0;
        var HitungGrandTotal    = 0;

        var Total                   = $('#TotalBayarHidden').val();
        if(Total==''){
            Total=0
        }

        var NilaiPpn                = $('#PersentasePpn').val();
        if(NilaiPpn==''){
            NilaiPpn=0
        }

        var NilaiBiayaLian          = $('#BiayaLainHidden').val();
        if(NilaiBiayaLian==''){
            NilaiBiayaLian=0
        }

        HitungPpn                   = parseInt(Total)*parseInt(NilaiPpn)/100;
        HitungGrandTotal            = parseInt(Total)+parseInt(HitungPpn)+parseInt(NilaiBiayaLian);

        $('#TotalBayar').html(to_rupiah(Total));
        $('#TotalBayarHidden').val(Total);
        $('#TotalPpn').html(to_rupiah(HitungPpn));
        $('#TotalPpnHidden').val(HitungPpn);
        $('#GrandTotal').html(to_rupiah(HitungGrandTotal));
        $('#GrandTotalHidden').val(HitungGrandTotal);
        $('#UangCash').val(to_rupiah(HitungGrandTotal));
        $('#UangCashHidden').val(HitungGrandTotal);
        $('#UangKembali').val('');
    }

    function HitungTotalKembalian()
    {
        var Debit               = '0';
        var Cash                = '0';

        Debit                   = $('#TotalDebitHidden').val();
        Cash                    = $('#UangCashHidden').val();

        var TotalUangBayar      = parseInt(Debit)+parseInt(Cash);
        var GrandTotalBayar     = $('#GrandTotalHidden').val();

        if(parseInt(TotalUangBayar) >= parseInt(GrandTotalBayar)){
            var Selisih = parseInt(TotalUangBayar) - parseInt(GrandTotalBayar);
            $('#UangKembali').val(to_rupiah(Selisih));
        }else{
            $('#UangKembali').val('0');
        }
    }

    function to_rupiah(angka){
        var rev     = parseInt(angka, 10).toString().split('').reverse().join('');
        var rev2    = '';
        for(var i = 0; i < rev.length; i++){
            rev2  += rev[i];
            if((i + 1) % 3 === 0 && i !== (rev.length - 1)){
                rev2 += '.';
            }
        }
        return rev2.split('').reverse().join('');

        // Pakai Simbol Rp.
        // return 'Rp. ' + rev2.split('').reverse().join('');
    }

    function to_angka(rp)
    {
        return parseInt(rp.replace(/,.*|\D/g,''),10)
    }

    function check_int(evt) {
        var charCode = ( evt.which ) ? evt.which : event.keyCode;
        return ( charCode >= 48 && charCode <= 57 || charCode == 8 );
    }

    function CetakStruk()
    {
        if($('#jumlah_barang').html() > 0){
            // Validasi uang cash + uang debit harus sama dengan grandtotal
            var Gt          = $('#GrandTotalHidden').val();
            var Cash        = $('#UangCashHidden').val();
            var Debit       = $('#TotalDebitHidden').val();

            if(Cash==''){
                Cash = 0;
            }

            if(Debit==''){
                Debit = 0;
            }

            var Total       = parseInt(parseInt(Cash)+parseInt(Debit));
            if(Total==''){
                Total = 0;
            }

            if(Total >= Gt){
                swal({
                    title: "Apakah anda yakin ingin mencetak faktur penyiapan barang ini ?",
                    type: "info",
                    showCancelButton: true,
                    confirmButtonClass: 'btn-primary btn-md waves-effect waves-light',
                    confirmButtonText: "Iya, saya yakin",
                    cancelButtonText: "Batal",
                },function (isConfirm){
                    if(isConfirm){
                        var FormData = "&id_penjualan_m="+$('#id_penjualan_m').val();
                        window.open("<?php echo site_url('faktur/faktur_penyiapan_barang/?'); ?>" + FormData,'_blank');
                    }
                });
            }else if(Total < Gt){
                swal({
                    title: "Oops !",
                    text: "Total bayar kurang dari Grand Total. Harap periksa kembali uang cash dan debit.",
                    type: "error",
                    confirmButtonClass: 'btn-danger btn-md waves-effect waves-light',
                    confirmButtonText: "Ok"
                }, function(){
                    setTimeout(function(){
                        $('#UangCash').focus();
                    }, 100);
                });
                return;
            }else{
                swal({
                    title: "Oops !",
                    text: "Harap masukan uang cash atau debit.",
                    type: "error",
                    confirmButtonClass: 'btn-danger btn-md waves-effect waves-light',
                    confirmButtonText: "Ok"
                }, function(){
                    setTimeout(function(){
                        $('#UangCash').focus();
                    }, 100);
                });
                return;
            }
        }else{
            swal({
                title: "Oops !",
                text: "Belum ada barang yang anda simpan ditransaksi ini, harap simpan barang terlebih dahulu.",
                type: "error",
                confirmButtonClass: 'btn-danger btn-md waves-effect waves-light',
                confirmButtonText: "Ok"
            }, function(){
                setTimeout(function(){
                    $('#pencarian_kode_barang').focus();
                }, 100);
            });
        }
    }

</script>
<!-- Akhir Script CRUD -->
<link href="assets/plugin/zircos/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
<link href="assets/plugin/zircos/plugins/bootstrap-select/css/bootstrap-select.min.css" rel="stylesheet" />
<script src="assets/select2/select2.min.js" type="text/javascript"></script>

<!-- Awal CSS -->
<link href="assets/plugin/zircos/plugins/datatables/jquery.dataTables.min.css" rel="stylesheet" type="text/css"/>
<link href="assets/plugin/zircos/plugins/datatables/responsive.bootstrap.min.css" rel="stylesheet" type="text/css"/>
<link href="assets/plugin/zircos/plugins/datatables/dataTables.bootstrap.min.css" rel="stylesheet" type="text/css"/>
<link href="assets/plugin/zircos/material-design/assets/css/tambahan.css" rel="stylesheet" type="text/css"/>
<!-- Akhir CSS -->

<!-- Awal JS -->
<script src="assets/plugin/zircos/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="assets/plugin/zircos/plugins/datatables/dataTables.bootstrap.js"></script>

<script src="assets/plugin/zircos/material-design/assets/js/detect.js"></script>
<script src="assets/plugin/zircos/material-design/assets/js/jquery.slimscroll.js"></script>
<script src="assets/plugin/zircos/plugins/datatables/dataTables.responsive.min.js"></script>
<script src="assets/plugin/zircos/plugins/datatables/responsive.bootstrap.min.js"></script>
<script src="assets/plugin/zircos/material-design/assets/pages/jquery.datatables.init.js"></script>
<!-- Akhir JS -->

<!-- Awal Sweet-Alert  -->
<link href="assets/plugin/zircos/plugins/bootstrap-sweetalert/sweet-alert.css" rel="stylesheet" type="text/css">
<script src="assets/plugin/zircos/plugins/bootstrap-sweetalert/sweet-alert.min.js"></script>
<!-- Akhir Sweet-Alert  -->

<script>
    var url               = "retur_pembelian";
    var table_induk       = $('#TableTransaksi').DataTable();
    var table_batal       = $('#TableTransaksiBatal').DataTable();
    var table_cari_barang = $('#TableCariBarang').DataTable();
    $('#JudulHalaman').html('Retur Pembelian - VAMA');

    $("#pencarian_supplier").select2({
        placeholder: 'Pilih supplier',
        ajax: {
            url:"<?php echo site_url('supplier/list_supplier')?>",
            dataType: 'json',
            type: "GET",
            delay: 250,
            data: function (params) {
                return {
                    ns: params.term
                };
            },
            processResults: function (data) {
                if(data !== ''){
                    var res = data.map(function (item) {
                          return {id: item.id_supplier, text: item.nama_supplier};
                        });
                }else{
                    res = '';
                }
                return {
                    results: res
                };
            }
        }
    });

    $("#pencarian_supplier").change(function(){
        $.ajax({
            url : "<?php echo site_url('supplier/ajax_lihat_supplier'); ?>",
            type: "POST",
            cache: false,
            data: 'id_supplier=' + $('#pencarian_supplier').val(),
            dataType:'JSON',
            success: function(json){
                $('#data_kode_supplier').html(json.kode_supplier);
                $('#data_tipe_supplier').html(json.tipe_supplier);
                $('#pencarian_kode_barang').focus();

            },error : function(data){ 
                url = 'retur_pembelian';
                swal({
                    title: "Oops !", 
                    text: "Data supplier gagal ditampilkan.", 
                    type: "error", 
                    confirmButtonClass: 'btn-danger btn-md waves-effect waves-light',
                    confirmButtonText: "Ok"
                },function (isConfirm){
                    window.location.href = url;
                });
                
            }
        });
    });

    $(document).ready(function(){
        $("#wrapper").removeClass('forced');
        $("#wrapper").addClass('forced enlarged');

        var id_retur_pembelian_m = $('#id_retur_pembelian_m').val(); 
        ambil_data(id_retur_pembelian_m);

        if($('#id_retur_pembelian_m').val() == '0'){
            $('#no_pembelian').focus();
        }else{
            $('#pencarian_kode_barang').focus();
        }
    });

    var delay = (function () {
        var timer = 0;
        return function (callback, ms) {
            clearTimeout(timer);
            timer = setTimeout(callback, ms);
        };
    })();

    function ambil_data(id_retur_pembelian_m)
    {
        usergroup = "<?php echo $this->session->userdata('usergroup_name') ?>";
        if (id_retur_pembelian_m !=='0'){
            $.ajax({
                url : "<?php echo site_url('retur_pembelian/ambil_data'); ?>",
                type: "POST",
                cache: false,
                data: 'id_retur_pembelian_m=' + id_retur_pembelian_m,
                dataType:'JSON',
                success: function(json){
                    url = "retur_pembelian/transaksi/?&id=" + id_retur_pembelian_m;
                    $('#no_retur_pembelian').html(json.data_induk['no_retur_pembelian']);
                    $('#no_pembelian').val(json.data_induk['no_pembelian']);

                    $('#pencarian_supplier').html('');
                    $('#pencarian_supplier').append(
                        "<option value='" + json.data_induk['id_supplier'] + "'>"
                        +json.data_induk['nama_supplier']
                        +"</option>"
                    );

                    $('#data_kode_supplier').html(json.data_induk['kode_supplier']);
                    $('#data_tipe_supplier').html(json.data_induk['tipe_supplier']);
                    
                    $('#jumlah_barang').html(json.data_induk['jumlah_barang']);

                    if(usergroup == "Super Admin"){
                        $('#TotalBayar').html(to_rupiah(json.data_induk['total']));
                        $('#TotalBayarHidden').val(json.data_induk['total']);
                        $('#TotalPpn').html(to_rupiah(json.data_induk['ppn']));
                        $('#TotalPpnHidden').val(json.data_induk['ppn']);                
                        $('#BiayaLain').val(to_rupiah(json.data_induk['biaya_lain']));
                        $('#BiayaLainHidden').val(json.data_induk['biaya_lain']);
                        $('#GrandTotal').html(to_rupiah(json.data_induk['grand_total']));
                        $('#GrandTotalHidden').val(json.data_induk['grand_total']);
                    }
                    $('#catatan').val(json.data_induk['keterangan_lain']);

                    $('#judul_tab_induk').html("Data Pembelian : <span class='badge up bg-primary'>" + 
                                                json.data_induk['jumlah_barang'] +
                                               " Barang </span>");
                    $('#judul_tab_batal').html("Data Pembelian Batal : <span class='badge up bg-danger'>" + 
                                                json.data_batal['jumlah_barang'] +
                                               " Barang </span>");

                    $('#pencarian_supplier').prop('disabled', true);
                    $('#tombol_cari_supplier').prop('disabled', true);
                    tampilkan_data();

                },error : function(data){ 
                    url = 'retur_pembelian';
                    swal({
                        title: "Oops !", 
                        text: "Data pembelian gagal ditampilkan.", 
                        type: "error", 
                        confirmButtonClass: 'btn-danger btn-md waves-effect waves-light',
                        confirmButtonText: "Ok"
                    },function (isConfirm){
                        window.location.href = url;
                    });
                }
            });
        } 
    }

    function tampilkan_data(){
        table_induk.clear();table_induk.destroy();
        table_induk=$('#TableTransaksi').DataTable(
        {
            ordering: true,
            bFilter:true,
            processing:true,
            serverSide:true,
            order:[],
            lengthMenu: [[5, 10, 25, 50, -1], [5, 10, 25, 50, "All"]],
            pagingType: "full",

            oLanguage:{
                sLoadingRecords:'Silakan Tunggu',
                sProcessing:"<img src='assets/loader/cubeloader2.2.gif' width='50' height='50'> Harap tunggu",
                sZeroRecords:"Belum ada data untuk ditampilkan",
                sSearch : "Pencarian data"
            },
            ajax:{
                url: "<?php echo site_url('retur_pembelian/ajax_list')?>",
                type: "POST",
                data: {'id_retur_pembelian_m' : $('#id_retur_pembelian_m').val()}
            },
            columnDefs: [
                            { 
                                "targets": [ -1 ],
                                "orderable": false,
                            },
                        ],
        });
    }

    function tampilkan_data_batal(){
        table_batal.clear();table_batal.destroy();
        table_batal=$('#TableTransaksiBatal').DataTable(
        {
            ordering: true,
            bFilter:true,
            processing:true,
            serverSide:true,
            order:[],
            lengthMenu: [[5, 10, 25, 50, -1], [5, 10, 25, 50, "All"]],
            pagingType: "full",

            oLanguage:{
                sLoadingRecords:'Silakan Tunggu',
                sProcessing:"<img src='assets/loader/cubeloader2.2.gif' width='50' height='50'> Harap tunggu",
                sZeroRecords:"Belum ada data untuk ditampilkan",
                sSearch : "Pencarian data"
            },
            ajax:{
                url: "<?php echo site_url('retur_pembelian/ajax_list_batal')?>",
                type: "POST",
                data: {'id_retur_pembelian_m' : $('#id_retur_pembelian_m').val()}
            },
            columnDefs: [
                            { 
                                "targets": [ -1 ],
                                "orderable": false,
                            },
                        ],
        });
    }

    function tampilkan_data_barang(){
        table_cari_barang.clear();table_cari_barang.destroy();
        table_cari_barang=$('#TableCariBarang').DataTable({
            ordering: true,
            bFilter:true,
            processing:true,
            serverSide:true,
            order:[],
            lengthMenu: [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
            pagingType: "full",

            oLanguage:{
                sLoadingRecords:'Silakan Tunggu',
                sProcessing:"<img src='assets/loader/cubeloader2.2.gif' width='50' height='50'> Harap tunggu",
                sZeroRecords:"Belum ada data untuk ditampilkan",
                sSearch : "Pencarian data"
            },
            ajax:{
                url: "<?php echo site_url('retur_pembelian/ajax_list_barang')?>",
                type: "POST",
                data: {'id_retur_pembelian_m' : $('#id_retur_pembelian_m').val(),
                       'id_supplier' : $('#pencarian_supplier').val()}
            },
            columnDefs: [
                            { 
                                "targets": [ -1 ],
                                "orderable": false,
                            },
                        ],
        });
    }

    $(document).on('click', '#tab_induk', function(e){
        tampilkan_data();
    });

    $(document).on('click', '#tab_batal', function(e){
        tampilkan_data_batal();
    });

    function reload_table_induk()
    {
        table_induk.ajax.reload();
    }

    function reload_table_batal()
    {
        table_batal.ajax.reload();
    }

    function tampilkan_total()
    {
        var NomorNota   = $('#no_pembelian').html();
        $.ajax({
            url: "<?php echo site_url('retur_pembelian/tampilkan_total'); ?>",
            type: "POST",
            cache: false,
            data: '&no_pembelian=' + NomorNota,
            dataType:'json',
            success: function(data){
                if(data.status == 1){   
                    // Bersihkan data
                    $('#id_retur_pembelian_m').val(data.id_rpm);                    
                    $('#pencarian_kode_barang').val('');
                    $('#nama_barang').html('Belum ada barang yg dipilih');
                    $('#harga_beli').val('');
                    $('#harga_beli_tampil').html('Rp. 0');
                    $('#jenis_harga').val('');
                    $('#jumlah_retur').val('0');
                    $('#discount').val('0.00');
                    $('#discount_hidden').val('');
                    $('#sub_total').val('');
                    $('#sub_total_tampil').html('Rp. 0');
                    $('#jumlah_retur').prop('disabled', true);
                    $('#discount').prop('disabled', true)
                }else if(data.status == 0){
                    $('.modal-dialog').removeClass('modal-lg');
                    $('.modal-dialog').addClass('modal-sm');
                    $('#ModalHeader').html('Oops !');
                    $('#ModalContent').html(data.pesan);
                    $('#ModalFooter').html("<button type='button' class='btn btn-primary' data-dismiss='modal' autofocus>Ok, Saya Mengerti</button>");
                    $('#ModalGue').modal('show');
                }
            }
        });
    }

    $(document).on('keydown', '#no_pembelian', function(e){
        var charCode = e.which || e.keyCode;
        if(charCode == 13 || charCode == 9){
            $('#pencarian_supplier').select2('open').select2('options'); 
        }
    });

    $(document).on('click', 'button#SimpanDetail', function(){
        simpan_detail();
    });

    $(document).on('change', '#dari_stok', function(){
        if($('#dari_stok').val() !== '-'){
            $('#jumlah_retur').focus();
        }
    });

    $(document).on('click', '#daftar-autocomplete li', function(){
        $(this).parent().parent().parent().find('input').val($(this).find('span#skunya').html());

        var id_barang_pusat     = $(this).find('span#id_barang').html();
        var nama_barang         = $(this).find('span#barangnya').html();
        var modal               = $(this).find('span#modal').html();

        $('div#hasil_pencarian').hide();
        $('#id_barang_pusat').html(id_barang_pusat);
        $('#nama_barang').html(nama_barang);

        usergroup = "<?php echo $this->session->userdata('usergroup_name') ?>";
        if(usergroup == 'Super Admin'){    
            $('#harga_beli').val(modal);
            $('#harga_beli_tampil').val(to_rupiah(modal));
            $('#sub_total').val(modal);
            $('#sub_total_tampil').html(to_rupiah(modal));
        }

        $('#dari_stok').removeAttr('disabled').val();
        $('#jumlah_retur').removeAttr('disabled').val();
        $('#keterangan_retur').removeAttr('disabled').val();
        $('#SimpanDetail').removeAttr('disabled').val();
        $('#dari_stok').focus();
    });

    $(document).on('keyup', '#pencarian_kode_barang', function(e){
        if($(this).val() !== ''){
            var charCode = e.which || e.keyCode;
            // if(charCode == 40){
            //     if($('div#hasil_pencarian li.autocomplete_active').length > 0){
            //         var Selanjutnya = $('div#hasil_pencarian li.autocomplete_active').next();
            //         $('div#hasil_pencarian li.autocomplete_active').removeClass('autocomplete_active');

            //         Selanjutnya.addClass('autocomplete_active');
            //     }else{
            //         $('div#hasil_pencarian li:first').addClass('autocomplete_active');
            //     }
            // }else if(charCode == 38){
            //     if($('div#hasil_pencarian li.autocomplete_active').length > 0){
            //         var Sebelumnya = $('div#hasil_pencarian li.autocomplete_active').prev();
            //         $('div#hasil_pencarian li.autocomplete_active').removeClass('autocomplete_active');
                
            //         Sebelumnya.addClass('autocomplete_active');
            //     }else{
            //         $('div#hasil_pencarian li:first').addClass('autocomplete_active');
            //     }
            // }
            if(charCode == 13){
                // Awal fungsi enter dan ambil data barang dengan ajax
                $('div#hasil_pencarian_barang').hide();
                $.ajax({
                    url: "<?php echo site_url('barang_pusat/ajax_cari_barang_untuk_pembelian'); ?>",
                    type: "POST",
                    cache: false,
                    data: 'sku=' + $(this).val() +
                          '&kode_supplier=' + $('#data_kode_supplier').html() +
                          '&nama_supplier=' + $('#pencarian_supplier').val(),
                    dataType:'json',
                    success: function(json){
                        if(json.status == 1){
                            $('#dari_stok').removeAttr('disabled').val();
                            $('#jumlah_retur').removeAttr('disabled').val();
                            $('#keterangan_retur').removeAttr('required').val();
                            $('#keterangan_retur').removeAttr('disabled').val();
                            $('#SimpanDetail').removeAttr('disabled').val();

                            $('#id_barang_pusat').html(json.data['id_barang_pusat']);
                            $('#nama_barang').html(json.data['nama_barang']);

                            usergroup = "<?php echo $this->session->userdata('usergroup_name') ?>";
                            if(usergroup == 'Super Admin'){
                                $('#harga_beli').val(json.data['modal']);
                                $('#harga_beli_tampil').val(to_rupiah(json.data['modal']));
                                $('#sub_total').val(json.data['modal']);
                                $('#sub_total_tampil').html(to_rupiah(json.data['modal']));
                            }
                            $('#dari_stok').focus()
                        }else if(json.status == 0){
                            $('#id_barang_pusat').html('');
                            $('#nama_barang').html('Belum ada barang yg dipilih');
                            $('#dari_stok').prop('disabled', true).val('-');
                            $('#jumlah_retur').prop('disabled', true).val('');
                            $('#keterangan_retur').prop('disabled', true).val('');                    
                            $('#SimpanDetail').prop('disabled', true).val('');

                            usergroup = "<?php echo $this->session->userdata('usergroup_name') ?>";
                            if(usergroup == 'Super Admin'){
                                $('#harga_beli').val('0');
                                $('#harga_beli_tampil').val('Rp. 0');
                                $('#sub_total').val('0');
                                $('#sub_total_tampil').html('Rp. 0');
                            }

                            $('.modal-dialog').removeClass('modal-lg');
                            $('.modal-dialog').addClass('modal-sm');
                            $('#ModalHeader').html('Oops !');
                            $('#ModalContent').html(json.pesan);
                            $('#ModalFooter').html("<button id='info_stok' type='button' class='btn btn-primary'>Ok, Saya Mengerti</button>");
                            $('#ModalGue').modal('show');
                            setTimeout(function(){ 
                                $('#info_stok').focus();
                            }, 100);
                        }
                    }
                });
            }
        }else{
            // Bersihkan data pemilihan barang
            // $('#id_barang_pusat').html('');
            // $('#nama_barang').html('Belum ada barang yg dipilih');
            // $('#dari_stok').prop('disabled', true).val('-');
            // $('#jumlah_retur').prop('disabled', true).val('');
            
            // usergroup = "<?php //echo $this->session->userdata('usergroup_name') ?>";
            // if(usergroup == 'Super Admin'){
            //     $('#harga_beli').val('0');
            //     $('#harga_beli_tampil').val('Rp. 0');
            //     $('#sub_total').val('0');
            //     $('#sub_total_tampil').html('Rp. 0');
            // }

            // $('#keterangan_retur').prop('disabled', true).val('');
            // $('#SimpanDetail').prop('disabled', true).val('');
            // $('div#hasil_pencarian').hide();
        }
    });

    $(document).on('click', '#tombol_scan_barang', function(e){
        AutoCompletedBarang($('#pencarian_kode_barang').val());
    });

    $(document).on('click', '#tombol_lihat_barang', function(e){
        tampilkan_data_barang();
        $('.modal-dialog').removeClass('modal-sm');
        $('.modal-dialog').removeClass('modal-md');
        $('.modal-dialog').addClass('modal-lg');
        $('#ModalHeaderCariBarang').html('CARI BARANG SUPPLIER : <b class="text-primary">' + 
                                         $('#pencarian_supplier').val()) +
                                         '</b>';        
        $('#ModalCariBarang').modal('show');
    });

    function proses_barang($sku){
        $.ajax({
                url: "<?php echo site_url('barang_pusat/ajax_cari_barang_untuk_pembelian'); ?>",
                type: "POST",
                cache: false,
                data: 'sku=' + $sku +
                      '&kode_supplier=' + $('#data_kode_supplier').html() +
                      '&nama_supplier=' + $('#pencarian_supplier').val(),
                dataType:'json',
                success: function(json){
                    if(json.status == 1){
                        $('#jumlah_beli').removeAttr('disabled').val('');
                        $('#SimpanDetail').removeAttr('disabled').val();
                        $('#id_barang_pusat').html(json.data['id_barang_pusat']);
                        $('#pencarian_kode_barang').val(json.data['sku']);
                        $('#nama_barang').html(json.data['nama_barang']);

                        usergroup = "<?php echo $this->session->userdata('usergroup_name') ?>";
                        if(usergroup == 'Super Admin'){
                            $('#harga_beli').val(json.data['modal']);
                            $('#harga_beli_tampil').val(to_rupiah(json.data['modal']));
                            $('#sub_total').val(json.data['modal_bersih']);
                            $('#sub_total_tampil').html(to_rupiah(json.data['modal_bersih']));
                        }

                        $('#dari_stok').removeAttr('disabled').val();
                        $('#jumlah_retur').removeAttr('disabled').val();
                        $('#keterangan_retur').removeAttr('disabled').val();
                        $('#SimpanDetail').removeAttr('disabled').val();

                        $('#ModalCariBarang').modal('hide');
                        setTimeout(function(){ 
                            $('#dari_stok').focus();
                        }, 100);
                    }else if(json.status == 0){
                        $('#ModalCariBarang').modal('hide');
                        $('#jumlah_beli').prop('disabled');
                        $('#jumlah_beli').val('0');

                        $('.modal-dialog').removeClass('modal-lg');
                        $('.modal-dialog').addClass('modal-sm');
                        $('#ModalHeader').html('Oops !');
                        $('#ModalContent').html(json.pesan);
                        $('#ModalFooter').html("<button id='info_stok' type='button' class='btn btn-primary'>Ok, Saya Mengerti</button>");
                        $('#ModalGue').modal('show');
                        setTimeout(function(){ 
                            $('#info_stok').focus();
                        }, 100);
                    }
                }
            });
    }

    $(document).on('keyup', '#jumlah_retur', function(){
        HitungSubtotalBarang();
    });

    $(document).on('keydown', '#jumlah_retur', function(e){
        var charCode = e.which || e.keyCode;
        if(charCode == 9){
            $('#keterangan_retur').focus();
            return false;   
        }
        if(charCode == 13){
            if($('#jumlah_retur').val() !== ''){
                $('#keterangan_retur').focus();
                return false;
            } 
        }
    });

    $(document).on('keydown', '#keterangan_retur', function(e){
        var charCode = e.which || e.keyCode;
        if(charCode == 13){
            simpan_detail();
        }
    });

    $(document).on('click', 'button#info_stok', function(){
        $('#ModalGue').modal('hide');
        $('#jumlah_retur').focus();
    });

    $(document).on('keyup', '#BiayaLain', function(){
        var BL   = $('#BiayaLain').val();
            BL   = to_angka(BL);
            
            if (BL>0) {
                $('#BiayaLain').val(to_rupiah(BL));
                $('#BiayaLainHidden').val(BL);
            }else{
                $('#BiayaLain').val('');
                $('#BiayaLainHidden').val('0');
            }
        HitungTotalBayar();
    });

    $(document).on('keyup', '#PersentasePpn', function(){
        HitungTotalBayar();
    });

    $(document).on('keydown', 'body', function(e){
        var charCode = ( e.which ) ? e.which : event.keyCode;

        if(charCode == 117) //F6
        {
            tampilkan_data_barang();
            $('.modal-dialog').removeClass('modal-sm');
            $('.modal-dialog').removeClass('modal-md');
            $('.modal-dialog').addClass('modal-lg');
            $('#ModalHeaderCariBarang').html('CARI BARANG SUPPLIER : <b class="text-primary">' + 
                                             $('#pencarian_supplier').val()) +
                                             '</b>';        
            $('#ModalCariBarang').modal('show');
            return false;
        }

        if(charCode == 118) //F7
        {
            $('#catatan').focus();
            return false;
        }

        if(charCode == 119) //F8
        {
            konfirmasi_tahan_transaksi();
            return false;
        }

        if(charCode == 120) //F9
        {
            konfirmasi_simpan_transaksi();
            return false;
        }
    });

    $(document).on('click', 'body', function(){
        $('div#hasil_pencarian_barang').hide();
        $('div#hasil_pencarian_supplier').hide();
    });

    function konfirmasi_tahan_transaksi(){
        if($('#no_pembelian').val() == ''){
            swal({
                    title: "Oops !", 
                    text: "Harap masuk no. pembelian terlebih dahulu", 
                    type: "error", 
                    confirmButtonClass: 'btn-danger btn-md waves-effect waves-light',
                    confirmButtonText: "Ok"
                });
            return;
        }

        if($('#jumlah_barang').html() > 0){
            swal({
                    title: "Apakah anda yakin ingin menahan pembelian ini ?",
                    type: "info",
                    showCancelButton: true,
                    confirmButtonClass: 'btn-primary btn-md waves-effect waves-light',
                    confirmButtonText: "Iya",
                    cancelButtonText: "Batal",
                },function (isConfirm){
                    if(isConfirm){
                        TahanTransaksi();
                    } 
                });
        }else{
            swal({
                    title: "Oops !", 
                    text: "Belum ada barang yang anda simpan ditransaksi ini, harap simpan barang terlebih dahulu", 
                    type: "error", 
                    confirmButtonClass: 'btn-danger btn-md waves-effect waves-light',
                    confirmButtonText: "Ok"
                });
        }
    }

    $(document).on('click', 'button#Tahan', function(){
        konfirmasi_tahan_transaksi();
    });

    function konfirmasi_simpan_transaksi(){
        if($('#no_pembelian').val() == ''){
            swal({
                    title: "Oops !", 
                    text: "Harap masuk no. pembelian terlebih dahulu", 
                    type: "error", 
                    confirmButtonClass: 'btn-danger btn-md waves-effect waves-light',
                    confirmButtonText: "Ok"
                });
            return;
        }

        if($('#jumlah_barang').html() > 0){
            swal({
                    title: "Apakah anda yakin ingin memyimpan pembelian ini ?",
                    type: "info",
                    showCancelButton: true,
                    confirmButtonClass: 'btn-primary btn-md waves-effect waves-light',
                    confirmButtonText: "Iya",
                    cancelButtonText: "Batal",
                },function (isConfirm){
                    if(isConfirm){
                        SimpanTransaksi();
                    } 
                });
        }else{
            swal({
                    title: "Oops !", 
                    text: "Belum ada barang yang anda simpan ditransaksi ini, harap simpan barang terlebih dahulu", 
                    type: "error", 
                    confirmButtonClass: 'btn-danger btn-md waves-effect waves-light',
                    confirmButtonText: "Ok"
                });
        }
    }

    $(document).on('click', 'button#Simpan', function(){
        konfirmasi_simpan_transaksi();
    });

    $(document).on('click', 'button#Laporan', function(){
        window.open("laporan_retur_pembelian",'_blank');
    });

    $(document).on('click', 'button#TidakCetak', function(){
        window.location.href="retur_pembelian";
    });

    $(document).on('click', 'button#SimpanTransaksi', function(){
        SimpanTransaksi();
    });

    function simpan_detail()
    {
        if($('#jumlah_retur').val() !== ''){
            $('#jumlah_retur').val(to_angka($('#jumlah_retur').val()));
        }
        
        if($('#dari_stok').val() == '-'){
            $('.modal-dialog').removeClass('modal-lg');
            $('.modal-dialog').addClass('modal-sm');
            $('#ModalHeader').html('Oops !');
            $('#ModalContent').html('Harap pilih dari stok mana akan diretur');
            $('#ModalFooter').html("<button type='button' class='btn btn-primary' data-dismiss='modal' autofocus>Ok</button>");
            $('#ModalGue').modal('show');
            return;
        }else if($('#jumlah_retur').val() <= 0){
            $('.modal-dialog').removeClass('modal-lg');
            $('.modal-dialog').addClass('modal-sm');
            $('#ModalHeader').html('Oops !');
            $('#ModalContent').html('Harap masukan jumlah retur');
            $('#ModalFooter').html("<button type='button' class='btn btn-primary' data-dismiss='modal' autofocus>Ok</button>");
            $('#ModalGue').modal('show');
            $('#jumlah_retur').focus();
            return;
        }else if($('#keterangan_retur').val() == ''){
            $('.modal-dialog').removeClass('modal-lg');
            $('.modal-dialog').addClass('modal-sm');
            $('#ModalHeader').html('Oops !');
            $('#ModalContent').html('Harap masukan keterangan retur');
            $('#ModalFooter').html("<button type='button' class='btn btn-primary' data-dismiss='modal' autofocus>Ok</button>");
            $('#ModalGue').modal('show');
            return;
        }else{
            var FormData = "no_retur_pembelian="+$('#no_retur_pembelian').html(); 
                FormData += "&id_retur_pembelian_m="+ $('#id_retur_pembelian_m').val();
                FormData += "&no_pembelian="+$('#no_pembelian').val();
                FormData += "&id_supplier="+$('#pencarian_supplier').val();
                FormData += "&catatan="+encodeURI($('#catatan').val());
                FormData += "&biaya_lain="+$('#BiayaLainHidden').val();
                FormData += "&ppn="+$('#TotalPpnHidden').val();

                FormData += "&id_barang_pusat="+$('#id_barang_pusat').html();
                FormData += "&sku="+encodeURI($('#pencarian_kode_barang').val());
                FormData += "&dari_stok="+$('#dari_stok').val();
                FormData += "&jumlah_retur="+$('#jumlah_retur').val();
                FormData += "&harga_beli="+$('#harga_beli').val();
                FormData += "&subtotal="+$('#sub_total').val();
                FormData += "&keterangan_retur="+$('#keterangan_retur').val();
                FormData += "&url="+ url;

            $.ajax({
                url: "<?php echo site_url('retur_pembelian/simpan_detail'); ?>",
                type: "POST",
                cache: false,
                data: FormData,
                dataType:'json',
                success: function(data){
                    if(data.status == 1){
                        if(url == 'retur_pembelian'){
                            swal({
                                title: "Berhasil!", text: data.pesan, type: "success", 
                                confirmButtonText: "Ok!",
                            },function(isConfirm){
                                window.location.href = data.url;
                            });
                            return;
                        }else{
                            tampilkan_data();
                            swal({
                                title: "Berhasil!", text: data.pesan, type: "success", 
                                confirmButtonText: "Ok!",
                            },function(isConfirm){
                                setTimeout(function(){ 
                                    $('#pencarian_kode_barang').focus();
                                }, 100);
                            });
                        }

                        $('#judul_tab_induk').html("Data Retur Pembelian : <span class='badge up bg-primary'>" + 
                                                    data.jumlah_barang +
                                                   " Barang </span>");
                        $('#judul_tab_batal').html("Data Retur Pembelian Batal : <span class='badge up bg-danger'>" + 
                                                    data.jumlah_barang_batal +
                                                   " Barang </span>");

                        $('#jumlah_barang').html(data.jumlah_barang);
                        usergroup = "<?php echo $this->session->userdata('usergroup_name') ?>";
                        if(usergroup == "Super Admin"){
                            $('#TotalBayar').html(to_rupiah(data.total));
                            $('#TotalBayarHidden').val(data.total);
                            $('#TotalPpn').html(to_rupiah(data.ppn));
                            $('#TotalPpnHidden').val(data.ppn);                
                            $('#GrandTotal').html(to_rupiah(data.grand_total));
                            $('#GrandTotalHidden').val(data.grand_total);
                        }

                        // Bersihkan data pemilihan barang
                        $('#pencarian_kode_barang').val('');
                        $('#id_barang_pusat').html('');
                        $('#nama_barang').html('Belum ada barang yg dipilih');
                        $('#dari_stok').val('-');
                        $('#jumlah_retur').val('');
                        $('#harga_beli').val('0');
                        $('#harga_beli_tampil').val('Rp. 0');
                        $('#keterangan_retur').val('');

                        $('#dari_stok').prop('disabled', true);
                        $('#jumlah_retur').prop('disabled', true);
                        $('#keterangan_retur').prop('required', true);
                        $('#keterangan_retur').prop('disabled', true);
                        $('#SimpanDetail').prop('disabled', true);

                        $('#sub_total').val('0');
                        $('#sub_total_tampil').html('Rp. 0');
                    }else if(data.status == 2){
                        swal({
                            title: "Oops !", 
                            text: data.pesan, 
                            type: "error", 
                            confirmButtonClass: 'btn-danger btn-md waves-effect waves-light',
                            confirmButtonText: "Ok"
                        },function(isConfirm){
                            window.location.href = data.url;
                        });
                    }else if(data.status == 3){
                        $('.modal-dialog').removeClass('modal-lg');
                        $('.modal-dialog').addClass('modal-sm');
                        $('#ModalHeader').html('Oops !');
                        $('#ModalContent').html(data.pesan);
                        $('#ModalFooter').html("<button type='button' class='btn btn-primary' data-dismiss='modal' autofocus>Ok, Saya Mengerti</button>");
                        $('#ModalGue').modal('show');
                    }
                },error: function (jqXHR, textStatus, errorThrown){
                    swal({
                            title: "Oops !", 
                            text: "Data pembelian barang gagal disimpan.", 
                            type: "error", 
                            confirmButtonClass: 'btn-danger btn-md waves-effect waves-light',
                            confirmButtonText: "Ok"
                    },function(isConfirm){
                        window.location.href = url;
                    });
                }
            });
        }
    }

    function edit_detail(id_retur_pembelian_d)
    {
        var FormData = "&id_retur_pembelian_d="+ id_retur_pembelian_d;
            FormData += "&url=" + url;

        $.ajax({
            url : "<?php echo site_url('retur_pembelian/ajax_edit'); ?>",
            type: "POST",
            cache: false,
            data: FormData,
            dataType: "JSON",
            success: function(json){
                $('#id_barang_pusat').html(json.data['id_barang_pusat']);
                $('#pencarian_kode_barang').val(json.data['sku']);
                $('#nama_barang').html(json.data['nama_barang']);
                $('#dari_stok').removeAttr('disabled').val(json.data['dari_stok']);
                $('#jumlah_retur').removeAttr('disabled').val(json.data['jumlah_retur']);

                usergroup = "<?php echo $this->session->userdata('usergroup_name') ?>";
                if(usergroup == "Super Admin"){
                    $('#harga_beli').val(json.data['harga_satuan']);
                    $('#harga_beli_tampil').val(to_rupiah(json.data['harga_satuan']));
                    $('#sub_total').val(json.data['subtotal']);
                    $('#sub_total_tampil').html(to_rupiah(json.data['subtotal']));
                }
                
                $('#keterangan_retur').removeAttr('required').val(json.data['keterangan']);
                $('#keterangan_retur').removeAttr('disabled').val();
                $('#SimpanDetail').removeAttr('disabled').val();
                $('#jumlah_retur').focus()
            }, error: function (jqXHR, textStatus, errorThrown){
                swal({
                    title: "Gagal!", 
                    text: "Gagal mengambil data dari ajax.", 
                    type: "error", 
                    confirmButtonClass: 'btn-danger btn-md waves-effect waves-light',
                    confirmButtonText: "Ok"
                },function (isConfirm){
                    window.location.href=url;
                });
            }
        });
    }

    function verifikasi_hapus_detail(id_retur_pembelian_d)
    {
        $.ajax({
            url : "<?php echo site_url('retur_pembelian/ajax_verifikasi_hapus_detail'); ?>",
            type: "POST",
            cache: false,
            data: "id_retur_pembelian_d=" + id_retur_pembelian_d +
                  "&url=" + url,
            dataType: "JSON",
            success: function(data){
                $('.modal-dialog').removeClass('modal-lg');
                $('.modal-dialog').addClass('modal-sm');
                $('#ModalHeader').html('Informasi Hapus Barang');
                $('#ModalContent').html(data.pesan);
                $('#ModalFooter').html(data.footer);
                $('#ModalGue').modal('show');
            },error: function (jqXHR, textStatus, errorThrown){
                swal({
                    title: "Gagal!", 
                    text: "Gagal mengambil data dari ajax.", 
                    type: "error", 
                    confirmButtonClass: 'btn-danger btn-md waves-effect waves-light',
                    confirmButtonText: "Ok"
                },function (isConfirm){
                    window.location.href=url;
                });
            }
        });
    }

    function hapus_detail(id_retur_pembelian_d)
    {
        var FormData = "&id_supplier=" + $('#pencarian_supplier').val();
            FormData += "&url=" + url;
            FormData += "&id_retur_pembelian_d=" + id_retur_pembelian_d;
            FormData += "&catatan=" + encodeURI($('#catatan').val());
            FormData += "&biaya_lain=" + $('#BiayaLainHidden').val();
            FormData += "&keterangan_batal=" + encodeURI($('#keterangan_batal').val());

        // ajax delete data to database
        $.ajax({
            url : "<?php echo site_url('retur_pembelian/ajax_hapus_detail'); ?>",
            type: "POST",
            cache: false,
            data: FormData,
            dataType: "JSON",
            success: function(data){
                if(data.status == 1){
                    $('#jumlah_barang').html(data.jumlah_barang);
                    usergroup = "<?php echo $this->session->userdata('usergroup_name') ?>";
                    if(usergroup == "Super Admin"){
                        $('#TotalBayar').html(to_rupiah(data.total));
                        $('#TotalBayarHidden').val(data.total);
                        $('#TotalPpn').html(to_rupiah(data.ppn));
                        $('#TotalPpnHidden').val(data.ppn);                
                        $('#GrandTotal').html(to_rupiah(data.grand_total));
                        $('#GrandTotalHidden').val(data.grand_total);                
                    }

                    $('#judul_tab_induk').html("Data Retur Pembelian : <span class='badge up bg-primary'>" + 
                                                data.jumlah_barang +
                                               " Barang </span>");
                    $('#judul_tab_batal').html("Data Retur Pembelian Batal : <span class='badge up bg-danger'>" + 
                                                data.jumlah_barang_batal +
                                               " Barang </span>");
                    reload_table_induk();
                    swal({
                        title: "Berhasil!", 
                        text: "Data retur pembelian barang berhasil dihapus.", 
                        type: "success", 
                        confirmButtonText: "Ok"   
                    });
                }else if(data.status == 0){
                    swal({
                        title: data.judul, 
                        text: data.pesan, 
                        type: data.tipe_pesan, 
                        confirmButtonText: "Ok"
                    },function (isConfirm){
                        window.location.href=data.url;
                    });
                }
            },error: function (jqXHR, textStatus, errorThrown){
                swal({
                    title: "Gagal!", 
                    text: "Gagal menghapus data dari ajax.", 
                    type: "error", 
                    confirmButtonClass: 'btn-danger btn-md waves-effect waves-light',
                    confirmButtonText: "Ok"
                },function (isConfirm){
                    window.location.href=url;
                });
            }
        });
    }

    function SimpanTransaksi()
    {
        var FormData = "no_retur_pembelian="+$('#no_retur_pembelian').html(); 
            FormData += "&no_pembelian="+$('#no_pembelian').val();
            FormData += "&id_retur_pembelian_m="+$('#id_retur_pembelian_m').val();
            FormData += "&tanggal="+encodeURI($('#tanggal').val());
            FormData += "&id_supplier="+$('#pencarian_supplier').val();
            FormData += "&catatan="+encodeURI($('#catatan').val());
            FormData += "&total="+$('#TotalBayarHidden').val();
            FormData += "&biaya_lain="+$('#BiayaLainHidden').val();
            FormData += "&ppn="+$('#TotalPpnHidden').val();
            FormData += "&grand_total="+$('#GrandTotalHidden').val();
            FormData += "&status="+('simpan');

        $.ajax({
            url: "<?php echo site_url('retur_pembelian/simpan_transaksi'); ?>",
            type: "POST",
            cache: false,
            data: FormData,
            dataType:'json',
            success: function(data){
                if(data.status == 1){
                    setTimeout(function(){
                        swal({
                            title: "Berhasil!", 
                            text: data.pesan, 
                            type: "success", 
                            confirmButtonText: "Iya, cetak faktur!",
                            cancelButtonText: "Tidak, nanti saja!",
                            showCancelButton: true
                        }, function (isConfirm){
                            if(isConfirm) {
                                CetakStruk();
                                window.location.href = "retur_pembelian";
                            }else{
                                window.location.href = "retur_pembelian";
                            }
                        });
                    }, 100);
                }else if(data.status == 0){
                    swal({
                        title: "Oops !", 
                        text: data.pesan, 
                        type: "error", 
                        confirmButtonClass: 'btn-danger btn-md waves-effect waves-light',
                        confirmButtonText: "Ok"
                    },function (isConfirm){
                        window.location.href = data.url;
                    });
                }   
            },error: function (jqXHR, textStatus, errorThrown){
                swal({
                    title: "Oops !", 
                    text: "Gagal menyimpan transaksi retur pembelian ini.", 
                    type: "error", 
                    confirmButtonClass: 'btn-danger btn-md waves-effect waves-light',
                    confirmButtonText: "Ok"
                },function (isConfirm){
                    // window.location.href = url;
                });
            }
        });
    }

    function TahanTransaksi()
    {
        var FormData = "no_retur_pembelian="+$('#no_retur_pembelian').html(); 
            FormData += "&no_pembelian="+$('#no_pembelian').val();
            FormData += "&id_retur_pembelian_m="+$('#id_retur_pembelian_m').val();
            FormData += "&tanggal="+encodeURI($('#tanggal').val());
            FormData += "&id_supplier="+$('#pencarian_supplier').val();
            FormData += "&catatan="+encodeURI($('#catatan').val());
            FormData += "&total="+$('#TotalBayarHidden').val();
            FormData += "&biaya_lain="+$('#BiayaLainHidden').val();
            FormData += "&ppn="+$('#TotalPpnHidden').val();
            FormData += "&grand_total="+$('#GrandTotalHidden').val();
            FormData += "&status="+('simpan');

        $.ajax({
            url: "<?php echo site_url('retur_pembelian/tahan_transaksi'); ?>",
            type: "POST",
            cache: false,
            data: FormData,
            dataType:'json',
            success: function(data){
                if(data.status == 1){
                    setTimeout(function(){
                        swal({
                            title: "Berhasil!", 
                            text: data.pesan, 
                            type: "success", 
                            confirmButtonText: "Ok"
                        },function (isConfirm){
                            window.location.href="retur_pembelian";
                        });
                    }, 100);
                }else if(data.status == 0){
                    swal({
                        title: "Oops !", 
                        text: data.pesan, 
                        type: "error", 
                        confirmButtonClass: 'btn-danger btn-md waves-effect waves-light',
                        confirmButtonText: "Ok"
                    },function (isConfirm){
                        window.location.href = data.url;
                    });
                }   
            },error: function (jqXHR, textStatus, errorThrown){
                swal({
                    title: "Oops !", 
                    text: "Gagal menahan transaksi retur pembelian ini.", 
                    type: "error", 
                    confirmButtonClass: 'btn-danger btn-md waves-effect waves-light',
                    confirmButtonText: "Ok"
                },function (isConfirm){
                    window.location.href = url;
                });
            }
        });
    }

    function AutoCompletedBarang(KataKunci)
    {    
        $('#id_barang_pusat').html('');
        $('#nama_barang').html('Belum ada barang yg dipilih');
        $('#jumlah_retur').val('');

        $('#dari_stok').prop('disabled', true).val('-');
        $('#jumlah_retur').prop('disabled', true).val('');
        $('#keterangan_retur').prop('disabled', true).val('');

        $('#harga_beli').val('0');
        $('#harga_beli_tampil').val('Rp. 0');
        $('#sub_total').val('0');
        $('#sub_total_tampil').html('Rp. 0');
                    
        $('#SimpanDetail').prop('disabled', true).val('');
            
        $('#induk_pencarian_barang').removeClass('has-warning');
        $('#induk_pencarian_barang').addClass('has-has-succes');

        $('#tombol_scan_barang').removeClass('btn-warning');
        $('#tombol_scan_barang').addClass('btn-primary');

        $('#icon_pencarian_barang').removeClass('fa-search');
        $('#icon_pencarian_barang').addClass('fa-spin fa-refresh');

        $('div#hasil_pencarian').hide();

        delay(function () {
            var id_rpm          = $('#id_retur_pembelian_m').val();
            var kode_supplier   = $('#data_kode_supplier').html();

            $.ajax({
                url: "<?php echo site_url('retur_pembelian/ajax_kode'); ?>",
                type: "POST",
                cache: false,
                data: 'keyword=' + KataKunci +
                      '&id_rpm=' + id_rpm +
                      '&kode_supplier=' + kode_supplier,
                dataType:'json',
                success: function(json){
                    if(json.status == 1){
                        $('#icon_pencarian_barang').removeClass('fa-spin fa-refresh');
                        $('#icon_pencarian_barang').addClass('fa-search');

                        $('div#hasil_pencarian_barang').show('fast');
                        $('div#hasil_pencarian_barang').html(json.datanya);

                    }else{
                        $('#icon_pencarian_barang').removeClass('fa-spin fa-refresh');
                        $('#icon_pencarian_barang').addClass('fa-search');

                        $('#induk_pencarian_barang').removeClass('has-primary');
                        $('#induk_pencarian_barang').addClass('has-warning');

                        $('#tombol_scan_barang').removeClass('btn-primary');
                        $('#tombol_scan_barang').addClass('btn-warning');
                    }            
                }
            });
        }, 500);
    }

    function AutoCompletedSupplier(KataKunci)
    {
        $('#induk_pencarian_supplier').removeClass('has-warning');
        $('#induk_pencarian_supplier').addClass('has-has-succes');

        $('#tombol_cari_supplier').removeClass('btn-warning');
        $('#tombol_cari_supplier').addClass('btn-primary');

        $('#icon_pencarian_supplier').removeClass('fa-search');
        $('#icon_pencarian_supplier').addClass('fa-spin fa-refresh');
        
        delay(function () {
            var Lebar   = Lebar + 25;
            $.ajax({
                url: "<?php echo site_url('supplier/ajax_pencarian_supplier'); ?>",
                type: "POST",
                cache: false,
                data: 'keyword=' + KataKunci +
                      '&url=' + url,
                dataType:'json',
                success: function(json){
                   if(json.status == 1){
                        $('#icon_pencarian_supplier').removeClass('fa-spin fa-refresh');
                        $('#icon_pencarian_supplier').addClass('fa-search');

                        $('div#hasil_pencarian_supplier').show('fast');
                        $('div#hasil_pencarian_supplier').html(json.datanya);

                        $('#data_kode_supplier').html('-');
                        $('#data_tipe_supplier').html('-');
                    }else{
                        $('#icon_pencarian_supplier').removeClass('fa-spin fa-refresh');
                        $('#icon_pencarian_supplier').addClass('fa-search');

                        $('#induk_pencarian_supplier').removeClass('has-primary');
                        $('#induk_pencarian_supplier').addClass('has-warning');

                        $('#tombol_cari_supplier').removeClass('btn-primary');
                        $('#tombol_cari_supplier').addClass('btn-warning');
                    } 
                }
            });
        }, 500);
    }

    function HitungSubtotalBarang()
    {
        var Harga           = $('#harga_beli').val();
        var JumlahBeli      = $('#jumlah_retur').val();
        var SubTotal        = parseInt(Harga) * parseInt(JumlahBeli);
        var SabTotalRp      = 'Rp. 0';
        
        if(SubTotal >= '0'){
            SabTotalRp  = to_rupiah(SubTotal);
        }
        
        $('#sub_total').val(SubTotal);
        $('#sub_total_tampil').html(SabTotalRp);
    }

    function HitungTotalBayar()
    {
        var HitungPpn           = 0;
        var HitungGrandTotal    = 0;

        var Total                   = $('#TotalBayarHidden').val();
        if (Total==''){
            Total=0}

        var NilaiPpn                = $('#PersentasePpn').val();
        if (NilaiPpn==''){
            NilaiPpn=0} 

        var NilaiBiayaLian          = $('#BiayaLainHidden').val();
        if (NilaiBiayaLian==''){
            NilaiBiayaLian=0}

        HitungPpn                   = parseInt(Total)*parseInt(NilaiPpn)/100;
        HitungGrandTotal            = parseInt(Total)+parseInt(HitungPpn)+parseInt(NilaiBiayaLian);

        $('#TotalBayar').html(to_rupiah(Total));
        $('#TotalBayarHidden').val(Total);
        $('#TotalPpn').html(to_rupiah(HitungPpn));
        $('#TotalPpnHidden').val(HitungPpn);
        $('#GrandTotal').html(to_rupiah(HitungGrandTotal));
        $('#GrandTotalHidden').val(HitungGrandTotal);    
        $('#UangCash').val(to_rupiah(HitungGrandTotal));
        $('#UangCashHidden').val(HitungGrandTotal);
        $('#UangKembali').val('');
    }

    function to_rupiah(angka){
        var rev     = parseInt(angka, 10).toString().split('').reverse().join('');
        var rev2    = '';
        for(var i = 0; i < rev.length; i++){
            rev2  += rev[i];
            if((i + 1) % 3 === 0 && i !== (rev.length - 1)){
                rev2 += '.';
            }
        }
        return rev2.split('').reverse().join('');

        // Pakai Simbol Rp.
        // return 'Rp. ' + rev2.split('').reverse().join('');
    }

    function to_angka(rp){
        return parseInt(rp.replace(/,.*|\D/g,''),10);
    }

    function check_int(evt){
        var charCode = ( evt.which ) ? evt.which : event.keyCode;
        return ( charCode >= 48 && charCode <= 57 || charCode == 8 );
    }

    function CetakStruk()
    {
        if($('#no_pembelian').val() == ''){
            swal({
                title: "Harap masukan no. pembelian terlebih dahulu ?",
                type: "error",
                confirmButtonClass: 'btn-danger btn-md waves-effect waves-light',
                confirmButtonText: "Ok",
            });
        return;
        }

        if($('#jumlah_barang').html() > 0){
            var FormData = "&id_retur_pembelian_m="+$('#id_retur_pembelian_m').val();
            window.open("<?php echo site_url('faktur/faktur_retur_pembelian/?'); ?>" + FormData,'_blank');
        }else{
            swal({
                title: "Harap pilih barang terlebih dahulu ?",
                type: "error",
                confirmButtonClass: 'btn-danger btn-md waves-effect waves-light',
                confirmButtonText: "Ok",
            });
        }
    }
</script>
<!-- Akhir Script CRUD -->
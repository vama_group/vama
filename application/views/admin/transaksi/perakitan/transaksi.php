<?php
	if($access_create == '1'){
		$disabled1	= '';
	}else{		
		$disabled1	= 'disabled';
	}

	if($access_update == '1'){
		$disabled2	= '';
	}else{		
		$disabled2	= 'disabled';
	}
?>

<div class="content">
	<div class="container">
	<br>		
		<div class="row">
        	<div class="col-md-12">
	        	<div class="row">
	        		<!-- Awal pilih barang perakitan -->
					<div class='col-md-4'>
						<div class="panel panel-color panel-purple">
							<!-- Default panel contents -->
							<div class="panel-heading">
								<h3 class="panel-title">No. Perakitan
									<span class="pull-right" name='no_perakitan' id='no_perakitan'>
										<b>-</b>
									</span>
								</h3>
							</div>

							<span style="display: none;">
								<input type='hidden' id="id_perakitan_m" name="id_perakitan_m" value="<?php echo $id_perakitan_m ?>">
								<input type='hidden' name='tanggal' class='form-control input-sm' id='tanggal' value="<?php echo date('d-m-Y'); ?>" <?php echo $disabled1; ?>>
							</span>

							<table class='table table-condensed table-striped table-hover' id='TablePerakitan'>
								<tbody>
									<tr>
										<td colspan="2">
	                                    	<div id="induk_pencarian_perakitan" class="input-group col-md-12">
	                                            <input id='pencarian_kode_perakitan' name="kode_barang" placeholder="SKU / Nama Barang Perakitan" class="form-control input-sm text-dark" type="text" <?php echo $disabled1 ?> autocomplete="off">
	                                            <span class="input-group-btn">
	                                                <button id="tombol_scan_perakitan" name="tombol_scan_perakitan" type="button" class="btn waves-effect waves-light btn-sm btn-purple" <?php echo $disabled1; ?>>
	                                                <i id="icon_perakitan" class="fa fa-search"></i></button>
	                                            </span>
	                                        </div>
	                                        <span id="id_barang_pusat_perakitan" style="display: none;"></span>
	                                        <div id='hasil_pencarian_perakitan' name="hasil_pencarian_barang"></div>
										</td>
									</tr>
									<tr>
										<td colspan="2">
											<small>Nama Barang</small> <br/>
											<span id="nama_barang_perakitan" name="nama_barang_perakitan" class="input-md text-dark" type="text" readonly>-</span>
										</td>
									</tr>	
									<tr>
										<td colspan="2">
											<small>Harga Eceran</small> <br/>
											<span id="harga_eceran_perakitan" name="harga_eceran_perakitan" class="input-md text-dark" type="text">-</span>
										</td>
									</tr>										
									<tr>
										<td><small>Jumlah Perakitan</small></td>
										<td class="text-left">
											<div class="row">
												<div class="col-md-10">
													<input type='number' name='jumlah_perakitan' id='jumlah_perakitan' class='form-control input-sm text-left text-dark'  onkeypress='return check_int(event)' placeholder="Jumlah perakitan" <?php echo $disabled1; ?>>
												</div>
												<div class="col-md-2">
													<button id="SimpanDetailPerakitan" type="button" class="btn btn-sm btn-purple pull-right" title="Update Qty Perakitan" <?php echo $disabled2; ?>>
														<i class="fa fa-exchange"></i>
													</button>
												</div>
											</div>
										</td>
									</tr>
									
									<tr>
										<td colspan="2">
											<textarea name='catatan' id='catatan' class='form-control input-sm' rows='2' placeholder="Catatan Transaksi (Jika Ada)" style='resize: vertical; width:100%;'></textarea>
										</td>
									</tr>
									
									<tr>
										<td colspan="2">
											<?php if($access_create == 1 or $access_update == 1){ ?>
												<button type='button' class='btn btn-primary' id='Simpan'>
													<i class='fa fa-paper-plane-o'></i>
												</button>
											<?php } ?>

											<?php if($access_create == 1 or $access_update == 1){ ?>
												<button type='button' class='btn btn-danger' id='Tahan'>
													<i class='fa fa-hand-paper-o'></i>
												</button>
											<?php } ?>

											<?php if($access_create == 1 or $access_update == 1){ ?>
												<button type='button' class='btn btn-default' id='CetakStruk'>
													<i class='fa fa-print'></i>
												</button>
											<?php } ?>

											<?php if($access_laporan == 1){ ?>
												<button id='Laporan' type='button' class='btn btn-default' title="Laporan Transaksi">
													<i class='fa fa-file-text-o'></i>
												</button>
											<?php } ?>
										</td>								
									</tr>

									<!-- Keterangan shortcut -->
									<tr>
										<td colspan="2">
											<p><i class='fa fa-keyboard-o fa-fw'></i> <b><small>Shortcut Keyboard : </small></b></p>
											<div class='row'>
												<div class='col-sm-6'><small>F6 = Cari Barang</small></div>
												<div class='col-sm-6'><small>F9 = Simpan Transaksi</small></div>
												<div class='col-sm-6'><small>F7 = Fokus ke field catatan</small></div>
												<div class='col-sm-6'><small>F10 = Cetak Struk</small></div>
												<div class='col-sm-6'><small>F8 = Tahan Transaksi</small></div>
											</div> 
										</td>
									</tr>	
								</tbody>
							</table>
						</div>
					</div>
					<!-- Akhir pilih barang perakitan --> 
					
	        		<!-- Awal pilih barang komponen dan daftar transaksi -->
		            <div class="col-md-8">
		            	<div class="row">
		            		<!-- Awal scan barang dan pilih Barang -->
		            		<div class="col-md-4">
			                    <table id='TableScan' class="table table-condensed dt-responsive nowrap card-box">
			                    	<thead class="input-sm">
			                    		<tr class="text-dark">
			                    			<th style="width:170px;">SCAN </th>
			                    		</tr>
			                    	</thead>

			                    	<tbody>
			                    		<tr class="text-dark">
			                    			<!-- Pencarian Barang : SKU / Nama Barang Komponen -->
			                    			<td>
			                                    <div id="induk_pencarian_komponen" class="input-group col-md-12">
	                                                <input id='pencarian_kode_komponen' name="kode_barang" placeholder="SKU / Nama Barang Komponen" class="form-control input-sm text-dark" type="text" <?php echo $disabled1 ?> autocomplete="off">
	                                                <span class="input-group-btn">
	                                                    <button id="tombol_scan_komponen" name="tombol_scan_komponen" type="button" class="btn waves-effect waves-light btn-sm btn-purple" <?php echo $disabled1; ?>>
	                                                    <i id="icon_komponen" class="fa fa-search"></i></button>
	                                                </span>
	                                            </div>
	                                            <span id="id_barang_pusat_komponen" style="display: none;"></span>
	                                            <div id='hasil_pencarian_komponen' name="hasil_pencarian_barang"></div>
			                    			</td>
			                    		</tr>
			                    	</tbody>
			                    </table>
						    </div>
						    <!-- Akhir scan barang dan pilih Barang -->

						    <!-- Awal detail barang -->
						    <div class='col-md-8'>
						    	<table id='TableBarang' class='table table-condensed dt-responsive nowrap card-box'>
						    		<thead class='input-sm'>
			                    		<tr class='text-dark'>
			                    			<th style="width:300px;">Nama Barang </th>
			                    			<th style="width:100px;">Harga Eceran</th>
			                    			<th style="width:10px;"></th>
			                    		</tr>
			                    	</thead>
			                    	<tbody class="input-sm">
			                    		<tr class="text-dark">
			                    			<!-- Nama Barang -->
			                    			<td>
			                    				<span id="nama_barang_komponen" name="nama_barang_komponen" class="text-dark">Belum ada barang yg dipilih</span>
			                    			</td>

			                    			<td>
			                    				<span id="harga_eceran_komponen" name="harga_eceran_komponen" class="text-dark">Rp. 0</span>
			                    			</td>

			                    			<td>
			                    				<button id='SimpanDetailKomponen' type='button' class='btn btn-purple btn-sm' title="Simpan Komponen" disabled>
													<i class='fa fa-paper-plane-o'></i>
												</button>
			                    			</td>
			                    		</tr>
			                    	</tbody>
						    	</table>
						    </div>
						    <!-- Akhir detail barang -->
						</div>

			        	<!-- Awal daftar transaksi dan catatan transaksi --> 
		            	<div class="card-box table-responsive">
		            		<!-- Awal induk tab -->
		                    <ul class="nav nav-tabs tabs-bordered" id="tab_list_pembelian">
		                        <li class="active">
		                            <a href="#tab_induk_perakitan" id="tab_induk" data-toggle="tab" aria-expanded="true">
		                                <span class="visible-xs"><i class="fa fa-check-square"></i></span>
		                                <span class="hidden-xs" id="judul_tab_induk">Data Komponen Perakitan</span>
		                            </a>
		                        </li>
		                        <li class="">
		                            <a href="#tab_perakitan_batal" id="tab_batal" data-toggle="tab" aria-expanded="true">
		                                <span class="visible-xs"><i class="typcn typcn-home"></i></span>
		                                <span class="hidden-xs" id="judul_tab_batal">Data Komponen Perakitan Batal</span>
		                            </a>
		                        </li>
		                    </ul>
		                    <!-- Akhir induk tab -->

		                    <!-- Awal isi tab -->
		                    <div class="tab-content">
								<!-- Awal daftar komponen barang -->
		                        <div class="tab-pane active" id="tab_induk_perakitan">
									<table id='TableTransaksi' class="table table-condensed table-striped table-hover dt-responsive nowrap" cellspacing="0" width="100%">
										<thead class="input-sm">
											<tr class="text-dark">
												<th>#</th>
												<th>Tombol</th>
												<th>SKU</th>
												<th>Nama Barang</th>
												<th>Harga</th>
												<th>Qty</th>

												<th>Pegawai Save</th>
												<th>Tanggal Save</th>
												<th>Pegawai Edit</th>
												<th>Tanggal Edit</th>
											</tr>
										</thead>

										<tbody class="input-sm text-dark"></tbody>
									</table>
								</div>

								<!-- Awal daftar komponen barang -->
		                        <div class="tab-pane" id="tab_perakitan_batal">
									<table id='TableTransaksiBatal' class="table table-condensed table-striped table-hover dt-responsive nowrap" cellspacing="0" width="100%">
										<thead class="input-sm">
											<tr class="text-dark">
												<th>#</th>
												<th>Tombol</th>
												<th>SKU</th>
												<th>Nama Barang</th>
												<th>Harga</th>
												<th>Qty</th>
												<th>Keterangan Batal</th>

												<th>Pegawai Batal</th>
												<th>Tanggal Batal</th>
												<th>Pegawai Save</th>
												<th>Tanggal Save</th>
												<th>Pegawai Edit</th>
												<th>Tanggal Edit</th>
											</tr>
										</thead>

										<tbody class="input-sm text-dark"></tbody>
									</table>
								</div>
							</div>
						</div>
				        <!-- Akhir daftar transaksi -->
		            </div>
		            <!-- Akhir pilih barang komponen dan daftar transaksi -->	
				</div>
            </div>
        </div>
	</div>
</div>

<div class="modal" id="ModalGue" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class='fa fa-times-circle'></i></button>
				<h4 class="modal-title" id="ModalHeader"></h4>
			</div>
			<div class="modal-body" id="ModalContent"></div>
			<div class="modal-footer" id="ModalFooter"></div>
		</div>
	</div>
</div>

<div class="modal" id="ModalPesan" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class='fa fa-times-circle'></i></button>
				<h4 class="modal-title" id="ModalHeaderPesan"></h4>
			</div>
			<div class="modal-body" id="ModalContentPesan"></div>
			<div class="modal-footer" id="ModalFooterPesan"></div>
		</div>
	</div>
</div>

<div class="modal" id="ModalCariBarang" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class='fa fa-times-circle'></i></button>
				<h5 class="modal-title" id="ModalHeaderCariBarang"></h5>
			</div>
			<div class="modal-body" id="ModalContentCariBarang">
				<table id='TableCariBarang' class="table table-condensed table-striped table-hover dt-responsive nowrap" cellspacing="0" width="100%">
					<thead class="input-sm">
						<tr class="text-dark">
							<th>#</th>
							<th>SKU</th>
							<th style="width:800px;">Nama Barang</th>
							<th>Stok</th>
							<th>Harga Eceran</th>
							<th>Tombol</th>
						</tr>
					</thead>

					<tbody class="input-sm text-dark"></tbody>
				</table>
			</div>
			<div class="modal-footer" id="ModalFooterCariBarang"></div>
		</div>
	</div>
</div>

<script>
	$('#ModalGue').on('hide.bs.modal', function () {
	   setTimeout(function(){ 
	        $('#ModalHeader, #ModalContent, #ModalFooter').html('');
	   }, 500);
	});

	$('#ModalPesan').on('hide.bs.modal', function () {
	   setTimeout(function(){ 
	        $('#ModalHeaderPesan, #ModalContentPesan, #ModalFooterPesan').html('');
	   }, 500);
	});

	$('#ModalCariBarang').on('hide.bs.modal', function () {
	   setTimeout(function(){ 
	        $('#ModalHeaderCariBarang, #ModalFooterCariBarang').html('');
	   }, 500);
	});	
</script>

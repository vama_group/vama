<!-- Awal CSS -->
<link href="assets/plugin/zircos/plugins/datatables/jquery.dataTables.min.css" rel="stylesheet" type="text/css"/>
<link href="assets/plugin/zircos/plugins/datatables/responsive.bootstrap.min.css" rel="stylesheet" type="text/css"/>
<link href="assets/plugin/zircos/plugins/datatables/dataTables.bootstrap.min.css" rel="stylesheet" type="text/css"/>
<link href="assets/plugin/zircos/material-design/assets/css/tambahan.css" rel="stylesheet" type="text/css"/>
<!-- Akhir CSS -->

<!-- Awal JS -->
<script src="assets/plugin/zircos/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="assets/plugin/zircos/plugins/datatables/dataTables.bootstrap.js"></script>

<script src="assets/plugin/zircos/material-design/assets/js/detect.js"></script>
<script src="assets/plugin/zircos/material-design/assets/js/jquery.slimscroll.js"></script>
<script src="assets/plugin/zircos/plugins/datatables/dataTables.responsive.min.js"></script>
<script src="assets/plugin/zircos/plugins/datatables/responsive.bootstrap.min.js"></script>
<script src="assets/plugin/zircos/material-design/assets/pages/jquery.datatables.init.js"></script>
<!-- Akhir JS -->

<!-- Awal Sweet-Alert  -->
<link href="assets/plugin/zircos/plugins/bootstrap-sweetalert/sweet-alert.css" rel="stylesheet" type="text/css">
<script src="assets/plugin/zircos/plugins/bootstrap-sweetalert/sweet-alert.min.js"></script>
<!-- Akhir Sweet-Alert  -->

<script>
    var url               = "perakitan";
    var table_induk       = $('#TableTransaksi').DataTable();
    var table_batal       = $('#TableTransaksiBatal').DataTable();
    var table_cari_barang = $('#TableCariBarang').DataTable();

    $(document).ready(function(){
        $('#navbar').removeClass('navbar-default');
        $('#navbar').addClass('navbar-ungu');
        $('#JudulHalaman').html('Perakitan - VAMA');

        $("#pencarian_kode_perakitan").focus();
        $("#wrapper").removeClass('forced');
        $("#wrapper").addClass('forced enlarged');

        var id_perakitan_m  = $('#id_perakitan_m').val(); 
        ambil_data(id_perakitan_m);
    });

    var delay = (function (){
        var timer = 0;
        return function (callback, ms) {
            clearTimeout(timer);
            timer = setTimeout(callback, ms);
        };
    })();

    function ambil_data(id_perakitan_m)
    {
        if(id_perakitan_m !== '0'){
            $.ajax({
                url : "<?php echo site_url('perakitan/ambil_data')?>",
                type: "POST",
                cache: false,
                data: 'id_perakitan_m=' + id_perakitan_m,
                dataType:'JSON',
                success: function(data){
                    $('#judul_tab_induk').html("Data Komponen Perakitan : <span class='badge up bg-primary'>" + 
                                                data.jumlah_barang + 
                                                " Barang </span>");
                    $('#judul_tab_batal').html("Data Komponen Perakitan Batal : <span class='badge up bg-danger'>" + 
                                                data.jumlah_barang_batal +
                                                " Barang </span>");

                    url = "perakitan/transaksi/?&id=" + id_perakitan_m;
                    $('#id_perakitan_m').val(data.id_perakitan_m);
                    $('#no_perakitan').html(data.no_perakitan);
                    $('#id_barang_pusat_perakitan').html(data.id_barang_pusat);
                    $('#pencarian_kode_perakitan').val(data.sku);
                    $('#nama_barang_perakitan').html(data.nama_barang);
                    $('#harga_eceran_perakitan').html(to_rupiah(data.harga_eceran));
                    $('#jumlah_perakitan').val(data.jumlah_perakitan);
                    $('#catatan').val(data.keterangan_lain);
                    $('#pencarian_kode_komponen').focus();
                    tampilkan_data();
                },
                error : function(data){ 
                    url = 'perakitan';
                    swal({
                        title: "Oops !", 
                        text: "Data perakitan gagal ditampilkan.", 
                        type: "error", 
                        confirmButtonClass: 'btn-danger btn-md waves-effect waves-light',
                        confirmButtonText: "Ok"
                    },function (isConfirm){
                        window.location.href = url;
                    });
                }
            });
        }else{
            url = "perakitan";
        }
    }

    function tampilkan_data(){
        table_induk.clear();table_induk.destroy();
        table_induk=$('#TableTransaksi').DataTable(
        {
            ordering: true,
            bFilter:true,
            processing:true,
            serverSide:true,
            order:[],
            lengthMenu: [[5, 10, 25, 50, -1], [5, 10, 25, 50, "All"]],
            pagingType: "full",

            oLanguage:{
                sLoadingRecords:'Silakan Tunggu',
                sProcessing:"<img src='assets/loader/cubeloader2.2.gif' width='50' height='50'> Harap tunggu",
                sZeroRecords:"Belum ada data perakitan untuk ditampilkan",
                sSearch : "Pencarian : "
            },
            ajax:{
                url: "<?php echo site_url('perakitan/ajax_list')?>",
                type: "POST",
                data: {'id_perakitan_m' : $('#id_perakitan_m').val()}
            },
            columnDefs: [
                            { 
                                "targets": [ -1 ],
                                "orderable": false,
                            },
                        ]
        });
    }

    function tampilkan_data_batal(){
        table_batal.clear();table_batal.destroy();
        table_batal=$('#TableTransaksiBatal').DataTable(
        {
            ordering: true,
            bFilter:true,
            processing:true,
            serverSide:true,
            order:[],
            lengthMenu: [[5, 10, 25, 50, -1], [5, 10, 25, 50, "All"]],
            pagingType: "full",

            oLanguage:{
                sLoadingRecords:'Silakan Tunggu',
                sProcessing:"<img src='assets/loader/cubeloader2.2.gif' width='50' height='50'> Harap tunggu",
                sZeroRecords:"Belum ada data perakitan batal untuk ditampilkan",
                sSearch : "Pencarian : "
            },
            ajax:{
                url: "<?php echo site_url('perakitan/ajax_list_batal')?>",
                type: "POST",
                data: {'id_perakitan_m' : $('#id_perakitan_m').val()}
            },
            columnDefs: [
                            { 
                                "targets": [ -1 ],
                                "orderable": false,
                            },
                        ]
        });
    }

    function tampilkan_data_barang(){
        table_cari_barang.clear();table_cari_barang.destroy();
        table_cari_barang=$('#TableCariBarang').DataTable(
        {
            ordering: true,
            bFilter:true,
            processing:true,
            serverSide:true,
            order:[],
            lengthMenu: [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
            pagingType: "full",

            oLanguage:{
                sLoadingRecords:'Silakan Tunggu',
                sProcessing:"<img src='assets/loader/cubeloader2.2.gif' width='50' height='50'> Harap tunggu",
                sZeroRecords:"Belum ada data barang untuk ditampilkan",
                sSearch : "Pencarian data"
            },
            ajax:{
                url: "<?php echo site_url('perakitan/ajax_list_barang')?>",
                type: "POST",
                data: {'id_perakitan_m' : $('#id_perakitan_m').val()}
            },
            columnDefs: [
                            { 
                                "targets": [ -1 ],
                                "orderable": false,
                            },
                        ],
        });
    }

    $(document).on('click', '#tab_induk', function(e){
        tampilkan_data();
    });

    $(document).on('click', '#tab_batal', function(e){
        tampilkan_data_batal();
    });

    function reload_table_induk()
    {
        table_induk.ajax.reload();
    }

    function reload_table_batal()
    {
        table_batal.ajax.reload();
    }

    $(document).on('click', 'button#SimpanDetailPerakitan', function(){
        if($('#jumlah_perakitan').val() == ''){
            $('.modal-dialog').removeClass('modal-md');
            $('.modal-dialog').addClass('modal-sm');
            $('#ModalHeader').html('Oops !');
            $('#ModalContent').html('Harap isikan jumlah perakitan');
            $('#ModalFooter').html("<button id='info_stok' type='button' class='btn btn-primary'>Ok, Saya Mengerti</button>");
            $('#ModalGue').modal('show');
            return false;
        }

        var id_barang_perakitan = $('#id_barang_pusat_perakitan').html();
        var jumlah_perakitan    = $('#jumlah_perakitan').val();
        var id_perakitan_m      = $('#id_perakitan_m').val();

        $.ajax({
            url: "<?php echo site_url('perakitan/update_stok_perakitan'); ?>",
            type: "POST",
            cache: false,
            data: 'id_barang_perakitan=' + id_barang_perakitan +
                  '&jumlah_perakitan=' + jumlah_perakitan +
                  '&id_perakitan_m=' + id_perakitan_m,
            dataType:'json',
            success: function(data){
                if(data.status == 0){
                    swal({
                        title: "Oops!", 
                        text: data.pesan,
                        type: "error", 
                        confirmButtonClass: 'btn-danger btn-md waves-effect waves-light',
                        confirmButtonText: "Ok"
                    }, function(isConfirm){
                        if(isConfirm){
                            tampilkan_data();
                            $('#pencarian_kode_komponen').focus();
                        }
                    });
                }else if(data.status == 1){
                    swal({
                        title: "Berhasil!", 
                        text: "Jumlah perakitan dan komponen berhasil di perbaharui.", 
                        type: "success", 
                        confirmButtonText: "Ok",
                    }, function(isConfirm){
                        tampilkan_data();
                        $('#pencarian_kode_komponen').focus();
                    });
                }else if(data.status == 2){
                    $('.modal-dialog').removeClass('modal-md');
                    $('.modal-dialog').addClass('modal-sm');
                    $('#ModalHeader').html('Oops !');
                    $('#ModalContent').html(data.pesan);
                    $('#ModalFooter').html("<button id='info_stok' type='button' class='btn btn-primary'>Ok, Saya Mengerti</button>");
                    $('#ModalGue').modal('show');

                    setTimeout(function(){ 
                        tampilkan_data();
                        $('#jumlah_perakitan').val(data.jumlah_lama);
                        $('#jumlah_perakitan').focus();
                    }, 500);
                }
            }
        });
    });

    $(document).on('click', 'button#SimpanDetailKomponen', function(){
        var pesan_simpan = "Apakah anda yakin ingin menyimpan komponen barang"; 
            pesan_simpan += "\n SKU : "+ $('#pencarian_kode_komponen').val();
            pesan_simpan += "\n Nama Barang : "+ $('#nama_barang_komponen').html();
            pesan_simpan += "\n Untuk perakitan";
            pesan_simpan += "\n SKU : "+$('#pencarian_kode_perakitan').val();
            pesan_simpan += "\n Nama Barang : "+$('#nama_barang_perakitan').html();
            pesan_simpan += "\n Jumlah Perakitan : "+$('#jumlah_perakitan').val();

        $.ajax({
            url: "<?php echo site_url('perakitan/cek_stok_komponen'); ?>",
            type: "POST",
            cache: false,
            data: 'id_barang_komponen=' + $('#id_barang_pusat_komponen').html() +
                  '&jumlah_perakitan=' + $('#jumlah_perakitan').val() +
                  '&id_perakitan_m=' + $('#id_perakitan_m').val(),
            dataType:'json',
            success: function(data){
                if(data.status == 1){
                    swal({
                        title: "Konfirmasi simpan komponen",
                        text: pesan_simpan,
                        type: "info",
                        showCancelButton: true,
                        confirmButtonClass: 'btn-primary btn-md waves-effect waves-light',
                        confirmButtonText: "Iya",
                        cancelButtonText: "Batal",
                    }, function (isConfirm){
                        if(isConfirm){
                            simpan_detail_komponen();
                        } 
                    });
                }else if(data.status == 0){
                    swal({
                        title: 'Oops!', 
                        text: data.pesan, 
                        type: "error", 
                        confirmButtonClass: 'btn-danger btn-md waves-effect waves-light',
                        confirmButtonText: "Ok"
                    }, function (isConfirm){
                        if(data.arahkan == 'jumlah_perakitan'){
                            setTimeout(function(){ 
                                $('#jumlah_perakitan').focus();
                            }, 10);
                        }else if(data.arahkan == 'halaman_utama'){
                            window.location.href = url;
                        }
                    });
                }
            }
        });
    });

    $(document).on('click', '#daftar_autocompleted_perakitan li', function(){    
        $(this).parent().parent().parent().find('input').val($(this).find('span#skunya_perakitan').html());
        var id_barang_pusat_perakitan = $(this).find('span#id_barang_perakitan').html();
        var nama_barang_perakitan     = $(this).find('span#barangnya_perakitan').html();
        var harga_barang_perakitan    = $(this).find('span#harga_perakitan').html();
        
        $('#hasil_pencarian_perakitan').hide();
        $('#id_barang_pusat_perakitan').html(id_barang_pusat_perakitan);
        $('#nama_barang_perakitan').html(nama_barang_perakitan);
        $('#harga_eceran_perakitan').html(harga_barang_perakitan);

        $('#jumlah_perakitan').removeAttr('disabled').val();
        $('#SimpanDetailPerakitan').removeAttr('disabled');
        $('#jumlah_perakitan').focus();
    });

    $(document).on('click', '#daftar_autocompleted_komponen li', function(){
        $(this).parent().parent().parent().find('input').val($(this).find('span#skunya_komponen').html());

        var id_barang_pusat_komponen     = $(this).find('span#id_barang_komponen').html();
        var nama_barang_komponen         = $(this).find('span#barangnya_komponen').html();
        var harga_barang_komponen        = $(this).find('span#harga_komponen').html();

        $('div#hasil_pencarian_komponen').hide();
        $('#id_barang_pusat_komponen').html(id_barang_pusat_komponen);
        $('#nama_barang_komponen').html(nama_barang_komponen);
        $('#harga_eceran_komponen').html(harga_barang_komponen);

        $('#SimpanDetailKomponen').removeAttr('disabled').val();
        $('#SimpanDetailKomponen').focus();
    });

    $(document).on('keyup', '#pencarian_kode_perakitan', function(e){
        if($(this).val() !== ''){
            var charCode = e.which || e.keyCode;
            if(charCode == 40){
                if($('div#hasil_pencarian_perakitan li.autocomplete_active-perakitan').length > 0){
                    var Selanjutnya = $('div#hasil_pencarian_perakitan li.autocomplete_active-perakitan').next();
                    $('div#hasil_pencarian_perakitan li.autocomplete_active-perakitan').removeClass('autocomplete_active-perakitan');
                    Selanjutnya.addClass('autocomplete_active-perakitan');
                }else{
                    $('div#hasil_pencarian_perakitan li:first').addClass('autocomplete_active-perakitan');
                }
            }else if(charCode == 38){
                if($('div#hasil_pencarian_perakitan li.autocomplete_active-perakitan').length > 0){
                    var Sebelumnya = $('div#hasil_pencarian_perakitan li.autocomplete_active-perakitan').prev();
                    $('div#hasil_pencarian_perakitan li.autocomplete_active-perakitan').removeClass('autocomplete_active-perakitan');
                    Sebelumnya.addClass('autocomplete_active-perakitan');
                }else{
                    $('div#hasil_pencarian_perakitan li:first').addClass('autocomplete_active-perakitan');
                }
            }else if(charCode == 13){
                $('div#hasil_pencarian_barang').hide();
                $.ajax({
                    url: "<?php echo site_url('perakitan/ajax_cari_barang_perakitan'); ?>",
                    type: "POST",
                    cache: false,
                    data: 'sku=' + $(this).val(),
                    dataType:'json',
                    success: function(json){
                        if(json.status == 2){
                            swal({
                                title: 'Oops!', 
                                text: json.pesan, 
                                type: 'error', 
                                confirmButtonClass: 'btn-danger btn-md waves-effect waves-light',
                                confirmButtonText: "Ok"
                            }, function(isConfirm){
                                window.location.href = url;
                            });
                        }else if(json.status == 1){
                            $('#hasil_pencarian_perakitan').hide();
                            $('#id_barang_pusat_perakitan').html(json.data['id_barang_pusat']);            
                            $('#pencarian_kode_perakitan').val(json.data['sku']);
                            $('#nama_barang_perakitan').html(json.data['nama_barang']);
                            $('#harga_eceran_perakitan').html(json.data['harga_eceran']);
                            $('#jumlah_perakitan').removeAttr('disabled').val('');
                            $('#SimpanDetailPerakitan').removeAttr('disabled');
                            $('#jumlah_perakitan').focus();
                        }else if(json.status == 0){
                            $('#hasil_pencarian_perakitan').hide();
                            $('#id_barang_pusat_perakitan').html('');            
                            $('#pencarian_kode_perakitan').val('');
                            $('#nama_barang_perakitan').html('Belum ada barang yg dipilih');
                            $('#harga_eceran_perakitan').html('Rp. 0');
                            $('#jumlah_perakitan').prop('disabled', true);
                            $('#SimpanDetailPerakitan').prop('disabled', true);

                            swal({
                                title: 'Oops!', 
                                text: json.pesan, 
                                type: 'error', 
                                confirmButtonClass: 'btn-danger btn-md waves-effect waves-light',
                                confirmButtonText: "Ok"
                            }, function(isConfirm){
                                setTimeout(function(){ 
                                    $('#pencarian_kode_perakitan').focus();
                                }, 10);
                            });
                        }
                    }
                });
            }else{
                AutoCompletedPerakitan($(this).width(), $(this).val(), $(this).parent().parent().index());
            }
        }else{
            $('#id_barang_pusat_perakitan').html('');        
            $('#nama_barang_perakitan').html('Belum ada barang yg dipilih');
            $('#harga_eceran_perakitan').html('Rp. 0');
            $('#jumlah_perakitan').prop('disabled', true).val('');
            $('#SimpanDetailPerakitan').prop('disabled', true);
            $('div#hasil_pencarian_perakitan').hide();
        }
    });

    $(document).on('keyup', '#jumlah_perakitan', function(e){
        var charCode = e.which || e.keyCode;
        if(charCode == 13){
            $('#pencarian_kode_komponen').focus();
        }
    });

    $(document).on('keyup', '#pencarian_kode_komponen', function(e){
        if($(this).val() !== ''){
            var charCode = e.which || e.keyCode;
            if(charCode == 40){
                if($('div#hasil_pencarian_komponen li.autocomplete_active-komponen').length > 0){
                    var Selanjutnya = $('div#hasil_pencarian_komponen li.autocomplete_active-komponen').next();
                    $('div#hasil_pencarian_komponen li.autocomplete_active-komponen').removeClass('autocomplete_active-komponen');

                    Selanjutnya.addClass('autocomplete_active-komponen');
                }else{
                    $('div#hasil_pencarian_komponen li:first').addClass('autocomplete_active-komponen');
                }
            }else if(charCode == 38){
                if($('div#hasil_pencarian_komponen li.autocomplete_active-komponen').length > 0){
                    var Sebelumnya = $('div#hasil_pencarian_komponen li.autocomplete_active-komponen').prev();
                    $('div#hasil_pencarian_komponen li.autocomplete_active-komponen').removeClass('autocomplete_active-komponen');
                
                    Sebelumnya.addClass('autocomplete_active-komponen');
                }else{
                    $('div#hasil_pencarian_komponen li:first').addClass('autocomplete_active-komponen');
                }
            }else if(charCode == 13){
                $('div#hasil_pencarian_barang').hide();
                $.ajax({
                    url: "<?php echo site_url('perakitan/ajax_cari_barang_perakitan'); ?>",
                    type: "POST",
                    cache: false,
                    data: 'sku=' + $(this).val(),
                    dataType:'json',
                    success: function(json){
                        if(json.status == 2){
                            swal({
                                title: 'Oops!', 
                                text: json.pesan, 
                                type: 'error', 
                                confirmButtonClass: 'btn-danger btn-md waves-effect waves-light',
                                confirmButtonText: "Ok"
                            }, function(isConfirm){
                                window.location.href = url;
                            });
                        }else if(json.status == 1){
                            $('#hasil_pencarian_komponen').hide();
                            $('#id_barang_pusat_komponen').html(json.data['id_barang_pusat']);            
                            $('#pencarian_kode_komponen').val(json.data['sku']);
                            $('#nama_barang_komponen').html(json.data['nama_barang']);
                            $('#harga_eceran_komponen').html(json.data['harga_eceran']);
                            $('#SimpanDetailKomponen').removeAttr('disabled');
                            $('#SimpanDetailKomponen').focus();
                            
                        }else if(json.status == 0){
                            $('#hasil_pencarian_komponen').hide();
                            $('#id_barang_pusat_komponen').html('');            
                            $('#nama_barang_komponen').val('');
                            $('#nama_barang_perakitan').html('Belum ada barang yg dipilih');
                            $('#harga_eceran_komponen').html('Rp. 0');
                            $('#SimpanDetailKomponen').prop('disabled', true);

                            swal({
                                title: 'Oops!', 
                                text: json.pesan, 
                                type: 'error', 
                                confirmButtonClass: 'btn-danger btn-md waves-effect waves-light',
                                confirmButtonText: "Ok"
                            }, function(isConfirm){
                                setTimeout(function(){ 
                                    $('#pencarian_kode_komponen').focus();
                                }, 10);
                            });
                        }
                    }
                });
            }else{
                AutoCompletedKomponen($(this).width(), $(this).val(), $(this).parent().parent().index());
            }
        }else{
            // Bersihkan data pemilihan barang
            $('#id_barang_pusat_komponen').html('');
            $('#nama_barang_komponen').html('Belum ada barang yg dipilih');
            $('#harga_eceran_komponen').html('Rp. 0');
            $('#SimpanDetailKomponen').prop('disabled', true).val();

            $('div#hasil_pencarian_komponen').hide();
        }
    });

    $(document).on('click', '#tombol_scan_komponen', function(e){
        tampilkan_data_barang();
        $('.modal-dialog').removeClass('modal-sm');
        $('.modal-dialog').removeClass('modal-md');
        $('.modal-dialog').addClass('modal-lg');
        $('#ModalHeaderCariBarang').html('CARI BARANG UNTUK PERAKITAN : <b class="text-primary">' + 
                                         $('#nama_barang_perakitan').html()) +
                                         '</b>';        
        $('#ModalCariBarang').modal('show');
    });

    function proses_barang($sku){
        $.ajax({
                url: "<?php echo site_url('perakitan/ajax_cari_barang_komponen'); ?>",
                type: "POST",
                cache: false,
                data: 'sku=' + $sku,
                dataType:'json',
                success: function(json){
                    if(json.status == 1){
                        $('#SimpanDetailKomponen').removeAttr('disabled').val();
                        $('#id_barang_pusat_komponen').html(json.data['id_barang_pusat']);
                        $('#pencarian_kode_komponen').val(json.data['sku']);
                        $('#harga_eceran_komponen').html(to_rupiah(json.data['harga_eceran']));
                        $('#nama_barang_komponen').html(json.data['nama_barang']);
                        $('#ModalCariBarang').modal('hide');
                        setTimeout(function(){ 
                            $('#SimpanDetailKomponen').focus();
                       }, 100);
                    }else if(json.status == 0){
                        $('#id_barang_pusat_komponen').html('');
                        $('#nama_barang_komponen').html('Belum ada barang yg dipilih');
                        $('#SimpanDetailKomponen').prop('disabled', true);

                        swal({
                            title: 'Oops!', 
                            text: data.pesan, 
                            type: 'error', 
                            confirmButtonClass: 'btn-danger btn-md waves-effect waves-light',
                            confirmButtonText: "Ok"
                        }, function(isConfirm){
                            window.location.href = url;
                        });
                    }
                },
                error: function (jqXHR, textStatus, errorThrown){
                    swal({
                        title: 'Oops!', 
                        text: 'Gagal memproses komponen perakitan.', 
                        type: 'error', 
                        confirmButtonClass: 'btn-danger btn-md waves-effect waves-light',
                        confirmButtonText: "Ok"
                    }, function(isConfirm){
                        window.location.href = url;
                    });
                }
            });
    }

    $(document).on('click', 'button#info_stok', function(){
        $('#ModalGue').modal('hide');
    });

    $(document).on('keydown', 'body', function(e){
        var charCode = ( e.which ) ? e.which : event.keyCode;

        if(charCode == 117) //F6
        {
            tampilkan_data_barang();
            $('.modal-dialog').removeClass('modal-sm');
            $('.modal-dialog').removeClass('modal-md');
            $('.modal-dialog').addClass('modal-lg');
            $('#ModalHeaderCariBarang').html('CARI KOMPONEN PERAKITAN : <b class="text-primary">' + 
                                             $('#nama_barang_perakitan').html()) +
                                             '</b>';        
            $('#ModalCariBarang').modal('show');
            return false;
        }

        if(charCode == 118) //F7
        {
            $('#catatan').focus();
            return false;
        }

        if(charCode == 119) //F8
        {
            konfirmasi_tahan_transaksi();
            return false;
        }

        if(charCode == 120) //F9
        {
            konfirmasi_simpan_transaksi();
            return false;
        }

        if(charCode == 121) //F10
        {
            CetakStruk();
            return false;
        }
    });

    $(document).on('click', 'body', function(){
        $('div#hasil_pencarian_komponen').hide();
        $('div#hasil_pencarian_perakitan').hide();
    });

    function konfirmasi_simpan_transaksi(){
        if($('#id_perakitan_m').val() > 0){
            swal({
                title: "Apakah anda yakin ingin memyimpan transaksi perakitan ini ?",
                type: "info",
                showCancelButton: true,
                confirmButtonClass: 'btn-primary btn-md waves-effect waves-light',
                confirmButtonText: "Iya",
                cancelButtonText: "Batal",
            }, function (isConfirm){
                if(isConfirm){
                    SimpanTransaksi();
                } 
            });
        }else{
            swal({
                title: "Oops !", 
                text: "Belum ada barang yang anda simpan ditransaksi ini, harap simpan komponen perakitan terlebih dahulu", 
                type: "error", 
                confirmButtonClass: 'btn-danger btn-md waves-effect waves-light',
                confirmButtonText: "Ok"
            });
        }
    }

    $(document).on('click', 'button#Simpan', function(){
        konfirmasi_simpan_transaksi();
    });

    function konfirmasi_tahan_transaksi(){
        if($('#id_perakitan_m').val() > 0){
            swal({
                title: "Apakah anda yakin ingin menahan transaksi perakitan ini ?",
                type: "info",
                showCancelButton: true,
                confirmButtonClass: 'btn-primary btn-md waves-effect waves-light',
                confirmButtonText: "Iya",
                cancelButtonText: "Batal",
            },function (isConfirm){
                if(isConfirm){
                    TahanTransaksi();
                } 
            });
        }else{
            swal({
                title: "Oops !", 
                text: "Belum ada barang yang anda simpan ditransaksi ini, harap simpan komponen perakitan terlebih dahulu", 
                type: "error", 
                confirmButtonClass: 'btn-danger btn-md waves-effect waves-light',
                confirmButtonText: "Ok"
            });
        }
    }

    $(document).on('click', 'button#Tahan', function(){
        konfirmasi_tahan_transaksi();
    });

    $(document).on('click', 'button#Laporan', function(){
        window.open("laporan_perakitan",'_blank');
    });

    $(document).on('click', 'button#IyaCetak', function(){
        CetakStruk();
        window.location.href="perakitan";
    });

    $(document).on('click', 'button#TidakCetak', function(){
        window.location.href="perakitan";
    });

    $(document).on('click', 'button#TahanTransaksi', function(){
        TahanTransaksi();
    });

    $(document).on('click', 'button#CetakStruk', function(){
        CetakStruk();
        return false;
    });

    function simpan_detail_komponen()
    {
        var FormData = "id_perakitan_m="+$('#id_perakitan_m').val(); 
            FormData += "&catatan="+encodeURI($('#catatan').val());
            FormData += "&id_barang_perakitan="+$('#id_barang_pusat_perakitan').html();
            FormData += "&id_barang_komponen="+$('#id_barang_pusat_komponen').html();
            FormData += "&jumlah_perakitan="+$('#jumlah_perakitan').val();

        $.ajax({
            url: "<?php echo site_url('perakitan/simpan_detail_komponen'); ?>",
            type: "POST",
            cache: false,
            data: FormData,
            dataType:'json',
            success: function(data){
                if(data.status == 1){
                    swal({
                        title: "Berhasil!", 
                        text: "Data komponen perakitan berhasil disimpan.", 
                        type: "success", 
                        confirmButtonText: "Ok",
                    }, function(isConfirm){
                        if(isConfirm){
                            if(url == 'perakitan'){
                                window.location.href = data.url;
                            }else{
                                tampilkan_data();
                            }    
                        }
                    });

                    $('#judul_tab_induk').html("Data Komponen Perakitan : <span class='badge up bg-primary'>" + 
                                                data.jumlah_barang + 
                                                " Barang </span>");
                    $('#judul_tab_batal').html("Data Komponen Perakitan Batal : <span class='badge up bg-danger'>" + 
                                                data.jumlah_barang_batal +
                                                " Barang </span>");

                    // Tampilkan data perakitan
                    $('#id_perakitan_m').val(data.id_pr);
                    $('#pencarian_kode_perakitan').val(data.sku_perakitan);
                    $('#nama_barang_perakitan').html(data.nama_barang_perakitan);
                    $('#harga_eceran_perakitan').html(data.harga_eceran_perakitan);
                    $('#jumlah_perakitan').val(data.jumlah_perakitan);

                    // Bersihkan data komponen
                    $('#pencarian_kode_komponen').val('');
                    $('#id_barang_pusat_komponen').html('');
                    $('#nama_barang_komponen').html('Belum ada barang yg dipilih');
                    $('#harga_eceran_komponen').html('Rp. 0');
                    $('#SimpanDetailKomponen').prop('disabled', true);
                    
                }else if(data.status == 0){
                    swal({
                        title: 'Oops!', 
                        text: data.pesan, 
                        type: 'error', 
                        confirmButtonClass: 'btn-danger btn-md waves-effect waves-light',
                        confirmButtonText: "Ok"
                    }, function(isConfirm){
                        window.location.href = url;
                    });
                }
            }
        });
    }

    function verifikasi_hapus_detail(id_perakitan_d)
    {
        $.ajax({
            url : "<?php echo site_url('perakitan/ajax_verifikasi_hapus_detail')?>",
            type: "POST",
            cache: false,
            data: 'id_perakitan_d=' + id_perakitan_d,
            dataType: "JSON",
            success: function(data){
                if(data.status == 1){
                    $('.modal-dialog').removeClass('modal-sm');
                    $('.modal-dialog').addClass('modal-md');
                    $('#ModalHeader').html('Informasi Hapus Barang');
                    $('#ModalContent').html(data.pesan);
                    $('#ModalFooter').html(data.footer);
                    $('#ModalGue').modal('show');
                }else{
                    swal({
                        title: 'Oops!', 
                        text: data.pesan, 
                        type: 'error', 
                        confirmButtonClass: 'btn-danger btn-md waves-effect waves-light',
                        confirmButtonText: "Ok"
                    }, function(isConfirm){
                        window.location.href = url;
                    });
                }
            },
            error: function (jqXHR, textStatus, errorThrown){
                swal({
                    title: 'Oops!', 
                    text: 'Gagal menghapus komponen perakitan.', 
                    type: 'error', 
                    confirmButtonClass: 'btn-danger btn-md waves-effect waves-light',
                    confirmButtonText: "Ok"
                }, function(isConfirm){
                    window.location.href = url;
                });
            }
        });
    }

    function hapus_detail(id_perakitan_d)
    {
        var FormData = "id_perakitan_d=" + id_perakitan_d;
            FormData += "&catatan=" + encodeURI($('#catatan').val());
            FormData += "&keterangan_batal=" + encodeURI($('#keterangan_batal').val());
            
        $.ajax({
            url : "<?php echo site_url('perakitan/ajax_hapus_detail')?>",
            type: "POST",
            data: FormData,
            dataType: "JSON",
            success: function(data){
                if(data.status == 1){
                    $('#judul_tab_induk').html("Data Komponen Perakitan : <span class='badge up bg-primary'>" + 
                                                data.jumlah_barang + 
                                                " Barang </span>");
                    $('#judul_tab_batal').html("Data Komponen Perakitan Batal : <span class='badge up bg-danger'>" + 
                                                data.jumlah_barang_batal +
                                                " Barang </span>");

                    swal({
                        title: "Berhasil!", 
                        text: data.pesan, 
                        type: "success", 
                        confirmButtonText: "Ok",
                    }, function(isConfirm){
                        if(isConfirm){
                            tampilkan_data();
                        }
                    });
                }else if(data.status == 0){
                    swal({
                        title: 'Oops!', 
                        text: data.pesan, 
                        type: 'error', 
                        confirmButtonClass: 'btn-danger btn-md waves-effect waves-light',
                        confirmButtonText: "Ok"
                    }, function(isConfirm){
                        window.location.href = url;
                    });
                }
            }
        });
    }

    function SimpanTransaksi()
    {
        var FormData = "id_perakitan_m="+$('#id_perakitan_m').val();
            FormData += "&id_barang_perakitan="+$('#id_barang_pusat_perakitan').html();
            FormData += "&catatan="+encodeURI($('#catatan').val());
            FormData += "&status="+('simpan');

        $.ajax({
            url: "<?php echo site_url('perakitan/simpan_transaksi'); ?>",
            type: "POST",
            cache: false,
            data: FormData,
            dataType:'json',
            success: function(data){
                if(data.status == 1){
                    setTimeout(function(){
                        swal({
                            title: "Berhasil!", 
                            text: data.pesan, 
                            type: "success", 
                            confirmButtonText: "Iya, cetak faktur!",
                            cancelButtonText: "Tidak, nanti saja!",
                            showCancelButton: true
                        }, function (isConfirm){
                            if(isConfirm) {
                                CetakStruk();
                                window.location.href = "perakitan";
                            }else{
                                window.location.href = "perakitan";
                            }
                        });
                    }, 100);
                }else if(data.status == 0){
                    setTimeout(function(){
                        swal({
                            title: "Oops !", 
                            text: data.pesan, 
                            type: "error", 
                            confirmButtonClass: 'btn-danger btn-md waves-effect waves-light',
                            confirmButtonText: "Ok"
                        }, function (isConfirm){
                            window.location.href = data.url;
                        });
                    }, 100);
                }   
            },
            error: function (jqXHR, textStatus, errorThrown){
                setTimeout(function(){
                    swal({
                        title: "Oops !", 
                        text: "Gagal menyimpan transaksi perakitan ini.", 
                        type: "error", 
                        confirmButtonClass: 'btn-danger btn-md waves-effect waves-light',
                        confirmButtonText: "Ok"
                    }, function (isConfirm){
                        window.location.href = url;
                    });
                }, 100);
            }
        });
    }

    function TahanTransaksi()
    {
        var FormData = "&id_perakitan_m="+$('#id_perakitan_m').val();
            FormData += "&id_barang_perakitan="+$('#id_barang_pusat_perakitan').html();
            FormData += "&catatan="+encodeURI($('#catatan').val());
            FormData += "&status="+('simpan');

        $.ajax({
            url: "<?php echo site_url('perakitan/tahan_transaksi'); ?>",
            type: "POST",
            cache: false,
            data: FormData,
            dataType:'json',
            success: function(data){
                if(data.status == 1){
                    setTimeout(function(){
                        swal({
                            title: "Berhasil!", 
                            text: data.pesan, 
                            type: "success", 
                            confirmButtonText: "Iya, cetak faktur!",
                            cancelButtonText: "Tidak, nanti saja!",
                            showCancelButton: true
                        }, function (isConfirm){
                            if(isConfirm) {
                                CetakStruk();
                                window.location.href = "perakitan";
                            }else{
                                window.location.href = "perakitan";
                            }
                        });
                    }, 100);
                }else if(data.status == 0){
                    setTimeout(function(){
                        swal({
                            title: "Oops !", 
                            text: data.pesan, 
                            type: "error", 
                            confirmButtonClass: 'btn-danger btn-md waves-effect waves-light',
                            confirmButtonText: "Ok"
                        }, function (isConfirm){
                            window.location.href = data.url;
                        });
                    }, 100);
                }   
            },
            error: function (jqXHR, textStatus, errorThrown){
                setTimeout(function(){
                    swal({
                        title: "Oops !", 
                        text: "Gagal menahan transaksi perakitan ini.", 
                        type: "error", 
                        confirmButtonClass: 'btn-danger btn-md waves-effect waves-light',
                        confirmButtonText: "Ok"
                    }, function (isConfirm){
                        window.location.href = url;
                    });
                }, 100);
            }
        });
    }

    function AutoCompletedPerakitan(Lebar, KataKunci, Indexnya)
    {
        $('#id_barang_pusat_perakitan').html('');
        $('#nama_barang_perakitan').html('Belum ada barang yg dipilih');
        $('#harga_eceran_perakitan').html('Rp. 0');
        $('#jumlah_perakitan').prop('disabled', true).val('');
        $('#SimpanDetailPerakitan').prop('disabled', true);
        $('#induk_pencarian_perakitan').removeClass('has-warning');
        $('#induk_pencarian_perakitan').addClass('has-has-succes');
        $('#icon_perakitan').removeClass('fa-search');
        $('#icon_perakitan').addClass('fa-spin fa-refresh');
        $('div#hasil_pencarian_perakitan').hide();
        
        delay(function(){
            var Lebar   = Lebar + 25;
            $.ajax({
                url: "<?php echo site_url('perakitan/ajax_kode_perakitan'); ?>",
                type: "POST",
                cache: false,
                data: 'keyword=' + KataKunci,
                dataType:'json',
                success: function(json){
                    if(json.status == 1){
                        $('#icon_perakitan').removeClass('fa-spin fa-refresh');
                        $('#icon_perakitan').addClass('fa-search');
                        $('div#hasil_pencarian_perakitan').show('fast');
                        $('div#hasil_pencarian_perakitan').html(json.datanya);
                    }else if(json.status == 2){
                        $('#icon_perakitan').removeClass('fa-spin fa-refresh');
                        $('#icon_perakitan').addClass('fa-search');
                        $('#induk_pencarian_perakitan').removeClass('has-succes');
                        $('#induk_pencarian_perakitan').addClass('has-warning');

                        swal({
                            title: json.judul_pesan,
                            text: json.isi_pesan,
                            type: "error",
                            confirmButtonClass: 'btn-danger btn-md waves-effect waves-light',
                            confirmButtonText: "Ok",
                        });
                    }else{
                        $('#icon_perakitan').removeClass('fa-spin fa-refresh');
                        $('#icon_perakitan').addClass('fa-search');
                        $('#induk_pencarian_perakitan').removeClass('has-succes');
                        $('#induk_pencarian_perakitan').addClass('has-warning');
                    }
                }
            });
        }, 500);
    }

    function AutoCompletedKomponen(Lebar, KataKunci, Indexnya)
    {
        $('#id_barang_pusat_komponen').html('');
        $('#nama_barang_komponen').html('Belum ada barang yg dipilih');
        $('#harga_eceran_komponen').html('Rp. 0');
        $('#SimpanDetailKomponen').prop('disabled', true).val();

        $('#induk_pencarian_komponen').removeClass('has-warning');
        $('#induk_pencarian_komponen').addClass('has-has-succes');

        $('#icon_komponen').removeClass('fa-search');
        $('#icon_komponen').addClass('fa-spin fa-refresh')

        $('div#hasil_pencarian_komponen').hide();

        delay(function(){
            var Lebar               = Lebar + 25;
            var id_pr               = $('#id_perakitan_m').val();
            var id_barang_perakitan = $('#id_barang_pusat_perakitan').html();

            $.ajax({
                url: "<?php echo site_url('perakitan/ajax_kode_komponen'); ?>",
                type: "POST",
                cache: false,
                data: 'keyword=' + KataKunci +
                      '&id_pr=' + id_pr +
                      '&id_barang_perakitan=' + id_barang_perakitan,
                dataType:'json',
                success: function(json){
                    if(json.status == 1){
                        $('#icon_komponen').removeClass('fa-spin fa-refresh');
                        $('#icon_komponen').addClass('fa-search');
                        $('div#hasil_pencarian_komponen').show('fast');
                        $('div#hasil_pencarian_komponen').html(json.datanya);
                    }else if(json.status == 2){
                        $('#icon_komponen').removeClass('fa-spin fa-refresh');
                        $('#icon_komponen').addClass('fa-search');
                        $('#induk_pencarian_komponen').removeClass('has-succes');
                        $('#induk_pencarian_komponen').addClass('has-warning');

                        swal({
                            title: json.judul_pesan,
                            text: json.isi_pesan,
                            type: "error",
                            confirmButtonClass: 'btn-danger btn-md waves-effect waves-light',
                            confirmButtonText: "Ok",
                        });
                    }else{
                        $('#icon_komponen').removeClass('fa-spin fa-refresh');
                        $('#icon_komponen').addClass('fa-search');
                        $('#induk_pencarian_komponen').removeClass('has-succes');
                        $('#induk_pencarian_komponen').addClass('has-warning');
                    }
                }
            });
        }, 500);
    }

    function to_rupiah(angka){
        var rev     = parseInt(angka, 10).toString().split('').reverse().join('');
        var rev2    = '';
        for(var i = 0; i < rev.length; i++){
            rev2  += rev[i];
            if((i + 1) % 3 === 0 && i !== (rev.length - 1)){
                rev2 += '.';
            }
        }
        return rev2.split('').reverse().join('');

        // Pakai Simbol Rp.
        // return 'Rp. ' + rev2.split('').reverse().join('');
    }

    function to_angka(rp)
    {
        return parseInt(rp.replace(/,.*|\D/g,''),10)
    }

    function check_int(evt) {
        var charCode = ( evt.which ) ? evt.which : event.keyCode;
        return ( charCode >= 48 && charCode <= 57 || charCode == 8 );
    }

    function CetakStruk()
    {
        if($('#no_faktur_supplier').val() == ''){
            swal({
                title: "Harap masukan no. faktur supplier terlebih dahulu ?",
                type: "error",
                confirmButtonClass: 'btn-danger btn-md waves-effect waves-light',
                confirmButtonText: "Ok",
            });
            return;
        }

        if($('#jumlah_barang').html() > 0){
            swal({
                title: "Apakah anda yakin ingin mencetak faktur perakitan ini ?",
                type: "info",
                showCancelButton: true,
                confirmButtonClass: 'btn-primary btn-md waves-effect waves-light',
                confirmButtonText: "Iya, saya yakin",
                cancelButtonText: "Batal",
            }, function (isConfirm){
                if(isConfirm){
                    var FormData = "&id_perakitan_m="+$('#id_perakitan_m').val();
                    window.open("<?php echo site_url('perakitan/transaksi_cetak/?'); ?>" + FormData,'_blank');
                } 
            });
        }else{
            swal({
                title: "Harap pilih barang komponen terlebih dahulu ?",
                type: "error",
                confirmButtonClass: 'btn-danger btn-md waves-effect waves-light',
                confirmButtonText: "Ok",
            });
        }
    }
</script>
<!-- Akhir Script CRUD -->
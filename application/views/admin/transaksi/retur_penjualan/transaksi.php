<?php
	if($access_create == '1' or $access_update == '1'){
		$readonly = '';
		$disabled = '';
	}else{		
		$readonly = 'readonly';
		$disabled = 'disabled';
	}
?>

<div class="content">
	<div class="container">
	<br>		
		<div class="row">
        	<div class="col-sm-12">
	        	<div class="row">
					<div class='col-sm-4'>
						<div class="panel panel-color panel-success">
							<!-- Default panel contents -->
							<div class="panel-heading">
								<h3 class="panel-title">No. Retur Penjualan
									<span class="pull-right">
										<b id='no_retur_penjualan' name='no_retur_penjualan'>-</b>
									</span>
								</h3>
							</div>

							<span>
								<input type='hidden' id="id_retur_penjualan_m" name="id_retur_penjualan_m" value="<?php echo $id_retur_penjualan_m ?>">
							</span>

							<table class='table table-condensed table-striped table-hover' id='Total'>
								<tbody>
									<tr>
										<td colspan="2">
											<div id="induk_pencarian_transaksi" class="input-group has-success col-md-12 table-responsive">
                                                <input id='pencarian_transaksi' name="pencarian_transaksi" placeholder="MASUKAN NO. PENJUALAN" class="form-control input-md text-dark" type="text" <?php echo $disabled ?> <?php echo $readonly ?> autocomplete="off">
                                                
                                                <span class="input-group-btn">
                                                	<?php if($access_create == 1 or $access_update == 1){ ?>
	                                                	<button id="tombol_cari_transaksi" name="tombol_cari_transaksi" type="button" class="btn waves-effect waves-light btn-md btn-success">
	                                                		<i id="icon_pencarian_transaksi" class="fa fa-search"></i>
	                                                	</button>
                                                	<?php }else{ ?>
	                                                	<button id="tombol_cari_transaksi" name="tombol_cari_transaksi" type="button" class="btn waves-effect waves-light btn-md btn-success">
		                                            		<i id="icon_pencarian_transaksi" class="fa fa-ban"></i>
		                                            	</button>
	                                                <?php } ?>
                                                </span>
                                            </div>
                                            <span id="id_penjualan_m" name="id_penjualan_m" style='display: none'>0</span>
                                            <div id='hasil_pencarian_transaksi' name="hasil_pencarian_transaksi"></div>
										</td>
									</tr>
									<tr>
										<td><small>Nama Customer</small></td>
										<td class="text-right">
											<span id='id_customer_pusat' name='id_customer_pusat' class="text-dark" style="display:none;">-</span>
											<span id='nama_customer' name='nama_customer' class="text-dark">-</span>
										</td>
									</tr>												
									<tr>
										<td><small>Jumlah Barang (Jual)</small></td>
										<td class="text-right">
											<span id='jumlah_barang' name='jumlah_barang' class="text-dark">0</span>
										</td>
									</tr>												
									<tr>
										<td><small>Total Penjualan</small></td>
										<td class="text-left">Rp. 
											<span id='TotalPenjualan' name="TotalPenjualan" class="text-dark pull-right">0</span>
											<input type="hidden" id='TotalPenjualanHidden' name='TotalPenjualanHidden'>
										</td>
									</tr>
									<tr>
										<td><small>Jumlah Barang (Retur)</small></td>
										<td class="text-left">Rp. 
											<span id='JumlahBarangRetur' name="JumlahBarangRetur" class="text-dark pull-right">0</span>
										</td>
									</tr>
									<tr>
										<td><small>Total Retur</small></td>
										<td class="text-left">Rp. 
											<span id='TotalRetur' name="TotalRetur" class="text-dark pull-right">0</span>
											<input type="hidden" id='TotalReturHidden' name='TotalReturHidden'>
										</td>
									</tr>
									<tr>
										<td><small>Total Potongan</small></td>
										<td class="text-left">Rp. 
											<span id='TotalPotongan' name="TotalPotongan" class="text-dark pull-right">0</span>
											<input type="hidden" id='TotalPotonganHidden' name='TotalPotonganHidden'>
										</td>
									</tr>
									<tr>
										<td><small class="text-success">Total Saldo</small></td>
										<td class="text-left">
											Rp. <span id='TotalSaldo' class="pull-right text-success">0</span></span>
											<input type="hidden" id='TotalSaldoHidden' name='TotalSaldoHidden' value="0">
										</td>
									</tr>
									<tr>
										<td colspan="2">
											<textarea name='catatan' id='catatan' class='form-control input-sm' rows='2' placeholder="Catatan Transaksi (Jika Ada)" style='resize: vertical; width:100%;'></textarea>
										</td>
									</tr>
									<tr>
										<td></td>
										<td>
											<?php if($access_create == 1 or $access_update == 1){ ?>
												<button type='button' class='btn btn-primary' id='Simpan'>
													<i class='fa fa-save'></i>
												</button>
											<?php } ?>

											<?php if($access_create == 1 or $access_update == 1){ ?>
												<button type='button' class='btn btn-danger' id='Tahan'>
													<i class='fa fa-hand-paper-o'></i>
												</button>
											<?php } ?>

											<?php if($access_laporan == 1){ ?>
												<button id='Laporan' type='button' class='btn btn-default' title="Laporan Transaksi">
													<i class='fa fa-file-text-o'></i>
												</button>
											<?php } ?>
										</td>										
									</tr>	
								</tbody>
							</table>
						</div>
					</div>
					
		            <div class="col-sm-8">
		            	<div class="row">
						    <!-- Awal input detail barang -->
						    <div class="col-sm-12">
						    	<!-- Table detail barang jual dan qty retur -->
						    	<table id='TableBarang' class="table table-condensed dt-responsive nowrap card-box">
						    		<thead class="input-sm">
			                    		<tr class="text-dark">
			                    			<th>SKU</th>
			                    			<th style="width: 330px;">Nama Barang</th>
			                    			<th>Harga</th>
			                    			<th>Potongan</th>
			                    			<th style="width: 80px;">Jml Jual</th>
			                    			<th style="width: 80px;">Jml Retur</th>
			                    		</tr>
			                    	</thead>
			                    	<tbody class="input-sm">
			                    		<tr class="text-dark">
			                    			<!-- SKU -->
			                    			<td>
			                    				<input id="id_penjualan_d" name="id_penjualan_d" type="hidden">
			                    				<input id="id_barang_pusat" name="id_barang_pusat" type="hidden">
			                    				<span id="sku" name="sku" class="form-control input-sm  text-dark">-</span>
			                    			</td>

			                    			<!-- Nama barang -->
			                    			<td>
			                    				<span class="form-control input-sm text-dark" id="nama_barang" name="nama_barang">Belum ada barang yg dipilih</span>
			                    			</td>

			                    			<!-- Harga -->
			                    			<td>
			                    				<input name='harga_satuan_hidden' id='harga_satuan_hidden' type='hidden'>
			                    				<span id="harga_satuan" name="harga_satuan" class="form-control input-sm text-dark">Rp. 0</span>
			                    			</td>

			                    			<!-- Potongan -->
			                    			<td colspan="1">
			                    				<div class="row">
			                    					<div class="col-md-4">
					                    				<input id='persen_potongan' name='persen_potongan' value="0.00" type='text' class='form-control input-sm' disabled>
					                    			</div>
					                    			<div class="col-md-8">
			                    						<input id='potongan_harga' name='potongan_harga' value="Rp. 0" type='text' class='form-control input-sm' onkeypress='return check_int(event)' disabled>
			                    						<input id='potongan_harga_hidden' name='potongan_harga_hidden' value="Rp. 0" type='hidden' class='form-control input-sm'>
			                    					</div>
			                    				</div>
			                    			</td>
			                    			
			                    			<!-- Jumlah jual -->
			                    			<td>
			                    				<span id="jumlah_beli" name="jumlah_beli" class="form-control input-sm text-dark">0</span>
			                    			</td>

			                    			<!-- Jumlah retur -->
			                    			<td>
			                    				<input id='jumlah_retur' name='jumlah_retur' type='text' class='form-control input-sm' onkeypress='return check_int(event)' disabled>
			                    			</td>
			                    		</tr>
			                    	</tbody>
						    	</table>

						    	<!-- Table subtotal -->
						    	<table id='TableSubtotalBarang' class="table table-condensed dt-responsive nowrap card-box">
						    		<thead class="input-sm">
			                    		<tr class="text-dark">
			                    			<th>Sub Potongan</th>
			                    			<th>Sub Saldo</th>
			                    			<th>Sub Retur</th>
			                    			<th>Masuk Stok</th>
			                    			<th>Keterangan</th>
			                    		</tr>
			                    	</thead>
			                    	<tbody class="input-sm">
			                    		<tr class="text-dark">
			                    			<!-- SUbtotal potongan -->
			                    			<td>
			                    				<span id='sub_potongan' name='sub_potongan' class='form-control input-sm'></span>
			                    				<input id='sub_potongan_hidden' name='sub_potongan_hidden' type='hidden'>
			                    			</td>

			                    			<!-- Subtotal saldo -->
			                    			<td>
			                    				<span id='sub_saldo' name='sub_saldo' class='form-control input-sm'></span>
			                    				<input id='sub_saldo_hidden' name='sub_saldo_hidden' type='hidden'>
			                    			</td>

			                    			<!-- Subtotal retur -->
			                    			<td>
			                    				<span id='sub_retur' name='sub_retur' class='form-control input-sm'></span>
			                    				<input id='sub_retur_hidden' name='sub_retur_hidden' type='hidden'>
			                    			</td>

			                    			<!-- Masuk stok -->
			                    			<td>
			                    				<select id='masuk_stok' name='masuk_stok' class='form-control input-sm' disabled>
			                    					<option value="-">-- pilih masuk stok --</option>
			                    					<option value="RUSAK">RUSAK</option>
			                    					<option value="JUAL">JUAL</option>
			                    				</select>
			                    			</td>

			                    			<!-- keterangan -->
			                    			<td colspan="2">
			                    				<div class="row">
				                    				<div class="col-md-10">
					                    				<input id="keterangan_retur" name='keterangan_retur' class="form-control input-sm" required	disabled>
					                    			</div>
					                    			<div class="col-md-2">
					                    				<button id="SimpanDetail" name="SimpanDetail" type="button" class="btn btn-success btn-sm pull-right" disabled><i class='fa fa-save'></i></button>
					                    			</div>
				                    			</div>
			                    			</td>
			                    		</tr>
			                    	</tbody>
						    	</table>
						    </div>
						    <!-- Akhir input detail barang -->
						</div>

			        	<!-- Awal daftar transaksi dan catatan transaksi --> 
		            	<div class="card-box table-responsive">
		            		<!-- Awal induk tab -->
		                    <ul class="nav nav-tabs tabs-bordered" id="tab_list_penjualan">
		                        <li class="active">
		                            <a href="#tab_induk_retur" id="tab_induk" data-toggle="tab" aria-expanded="true">
		                                <span class="visible-xs"><i class="fa fa-check-square"></i></span>
		                                <span class="hidden-xs" id="judul_tab_induk">Data Retur Penjualan</span>
		                            </a>
		                        </li>
		                        <li class="">
		                            <a href="#tab_retur_batal" id="tab_batal" data-toggle="tab" aria-expanded="true">
		                                <span class="visible-xs"><i class="typcn typcn-home"></i></span>
		                                <span class="hidden-xs" id="judul_tab_batal">Data Retur Penjualan Batal</span>
		                            </a>
		                        </li>
		                    </ul>
		                    <!-- Akhir induk tab -->

		                    <!-- Awal isi tab -->
		                    <div class="tab-content">
								<!-- Awal daftar retur penjualan barang -->
		                        <div class="tab-pane active" id="tab_induk_retur">
									<table id='TableTransaksi' class="table table-condensed table-striped table-hover dt-responsive nowrap" cellspacing="0" width="100%">
										<thead class="input-sm">
											<tr class="text-dark">
												<th>#</th>
												<th>Tombol</th>
												<th>SKU</th>
												<th>Nama Barang</th>
												<th>Harga</th>
												<th>Jml Beli</th>
												<th>Jml Retur</th>
												<th>Subtotal Retur</th>
												<th>Subtotal Potongan</th>
												<th>Subtotal Saldo</th>
												<th>Masuk Stok</th>
												<th>Keterangan Retur</th>
												<th>Pegawai Save</th>
												<th>Tanggal Save</th>
												<th>Pegawai Edit</th>
												<th>Tanggal Edit</th>
											</tr>
										</thead>
										<tbody class="input-sm text-dark"></tbody>
									</table>
								</div>
								<!-- Awal daftar retur penjualan barang -->

								<!-- Awal daftar retur penjualan barang batal -->
		                        <div class="tab-pane" id="tab_retur_batal">
									<table id='TableTransaksiBatal' class="table table-condensed table-striped table-hover dt-responsive nowrap" cellspacing="0" width="100%">
										<thead class="input-sm">
											<tr class="text-dark">
												<th>#</th>
												<th>SKU</th>
												<th>Nama Barang</th>
												<th>Keterangan Batal</th>
												<th>Harga</th>
												<th>Jml Beli</th>
												<th>Jml Retur</th>
												<th>Subtotal Retur</th>
												<th>Subtotal Potongan</th>
												<th>Subtotal Saldo</th>
												<th>Masuk Stok</th>
												<th>Keterangan Retur</th>
												<th>Pegawai</th>
												<th>Tanggal</th>
											</tr>
										</thead>
										<tbody class="input-sm text-dark"></tbody>
									</table>
								</div>
								<!-- Akhir daftar retur penjualan barang batal -->
							</div>

							<!-- Catatan transaksi dan keterangan shortcut -->
							<div class="row">
								<div class="col-sm-12">
									<p><i class='fa fa-keyboard-o fa-fw'></i> <b><small>Shortcut Keyboard : </small></b></p>
									<small>
										F7 = Fokus ke field catatan
										, F8 = Tahan Transaksi
										, F9 = Simpan Transaksi
									</small>
								</div>
							</div>
						</div>
				        <!-- Akhir daftar transaksi -->
		            </div>
				</div>
            </div>
        </div>
	</div>
</div>

<div class="modal" id="ModalGue" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class='fa fa-times-circle'></i></button>
				<h4 class="modal-title" id="ModalHeader"></h4>
			</div>
			<div class="modal-body" id="ModalContent"></div>
			<div class="modal-footer" id="ModalFooter"></div>
		</div>
	</div>
</div>

<script>
$('#ModalGue').on('hide.bs.modal', function () {
   setTimeout(function(){ 
        $('#ModalHeader, #ModalContent, #ModalFooter').html('');
   }, 500);
});
</script>

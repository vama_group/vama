<!-- Awal CSS -->
<link href="assets/plugin/zircos/plugins/datatables/jquery.dataTables.min.css" rel="stylesheet" type="text/css"/>
<link href="assets/plugin/zircos/plugins/datatables/responsive.bootstrap.min.css" rel="stylesheet" type="text/css"/>
<link href="assets/plugin/zircos/plugins/datatables/dataTables.bootstrap.min.css" rel="stylesheet" type="text/css"/>
<link href="assets/plugin/zircos/material-design/assets/css/tambahan.css" rel="stylesheet" type="text/css"/>
<!-- Akhir CSS -->

<!-- Awal JS -->
<script src="assets/plugin/zircos/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="assets/plugin/zircos/plugins/datatables/dataTables.bootstrap.js"></script>
<script src="assets/plugin/zircos/material-design/assets/js/detect.js"></script>
<script src="assets/plugin/zircos/material-design/assets/js/jquery.slimscroll.js"></script>
<script src="assets/plugin/zircos/plugins/datatables/dataTables.responsive.min.js"></script>
<script src="assets/plugin/zircos/plugins/datatables/responsive.bootstrap.min.js"></script>
<script src="assets/plugin/zircos/material-design/assets/pages/jquery.datatables.init.js"></script>
<!-- Akhir JS -->

<!-- Awal Sweet-Alert  -->
<link href="assets/plugin/zircos/plugins/bootstrap-sweetalert/sweet-alert.css" rel="stylesheet" type="text/css">
<script src="assets/plugin/zircos/plugins/bootstrap-sweetalert/sweet-alert.min.js"></script>
<!-- Akhir Sweet-Alert  -->

<script>
    var url               = "retur_penjualan";
    var table_induk       = $('#TableTransaksi').DataTable();
    var table_batal       = $('#TableTransaksiBatal').DataTable();
    var table_cari_barang = $('#TableCariBarang').DataTable();    

    $(document).ready(function(){
        $('#navbar').removeClass('navbar-default');
        $('#navbar').addClass('navbar-hijau');
        $('#JudulHalaman').html('Retur Penjualan - VAMA');

        $("#pencarian_transaksi").focus();
        $("#wrapper").removeClass('forced');
        $("#wrapper").addClass('forced enlarged');

        var id_retur_penjualan_m  = $('#id_retur_penjualan_m').val(); 
        ambil_data(id_retur_penjualan_m);
    });

    function ambil_data(id_retur_penjualan_m)
    {
        if(id_retur_penjualan_m !=='0'){
            $.ajax({
                url : "<?php echo site_url('retur_penjualan/ambil_data')?>",
                type: "POST",
                cache: false,
                data: 'id_retur_penjualan_m=' + id_retur_penjualan_m,
                dataType:'JSON',
                success: function(json){
                    url = "retur_penjualan/transaksi/?&id=" + id_retur_penjualan_m;
                    $('#tombol_cari_transaksi').prop('disabled', true);
                    $('#id_penjualan_m').html(json.data_induk['id_penjualan_m']);
                    $('#no_retur_penjualan').html(json.data_induk['no_retur_penjualan']);
                    $('#pencarian_transaksi').prop('disabled', true).val(json.data_induk['no_penjualan']);
                    $('#id_customer_pusat').html(json.data_induk['id_customer_pusat']);
                    $('#nama_customer').html(json.data_induk['nama_customer']);
                    $('#jumlah_barang').html(json.data_jual['jumlah_jual']);
                    $('#TotalPenjualan').html(to_rupiah(json.data_induk['grand_total']));
                    $('#TotalPenjualanHidden').val(json.data_induk['grand_total']);

                    $('#JumlahBarangRetur').html(json.data_induk['jumlah_retur']);
                    $('#TotalRetur').html(to_rupiah(json.data_induk['total_retur']));
                    $('#TotalReturHidden').val(json.data_induk['total_retur']);
                    $('#TotalPotongan').html(to_rupiah(json.data_induk['total_potongan']));
                    $('#TotalPotonganHidden').val(json.data_induk['total_potongan']);
                    $('#TotalSaldo').html(to_rupiah(json.data_induk['total_saldo']));
                    $('#TotalSaldoHidden').val(json.data_induk['total_saldo']);    
                    $('#catatan').val(json.data_induk['keterangan_lain']);

                    $('#judul_tab_induk').html("Data Retur Penjualan : <span class='badge up bg-primary'>" +
                                                json.data_induk['jumlah_retur'] +
                                               " Barang </span>");
                    $('#judul_tab_batal').html("Data Retur Penjualan Batal : <span class='badge up bg-danger'>" +
                                                json.data_batal['jumlah_barang'] +
                                               " Barang </span>");
                    tampilkan_data_induk();
                },
                error : function(data){ 
                    url = 'retur_penjualan';
                    swal({
                        title: "Oops !",
                        text: "Data retur penjualan gagal ditampilkan.",
                        type: "error",
                        confirmButtonClass: 'btn-danger btn-md waves-effect waves-light',
                        confirmButtonText: "Ok"
                    },function (isConfirm){
                        // window.location.href = url;
                    });
                }
            });
        } 
    }

    function tampilkan_data_induk(){
        table_induk.clear();table_induk.destroy();
        table_induk=$('#TableTransaksi').DataTable({
            ordering: true,
            bFilter:true,
            processing:true,
            serverSide:true,
            order:[],
            lengthMenu: [[5, 10, 25, 50, -1], [5, 10, 25, 50, "All"]],
            pagingType: "full",

            oLanguage:{
                sLoadingRecords:'Silakan Tunggu',
                sProcessing:"<img src='assets/loader/cubeloader2.2.gif' width='50' height='50'> Harap tunggu",
                sZeroRecords:"Belum ada data untuk ditampilkan",
                sSearch : "Pencarian data"
            },
            ajax:{
                url: "<?php echo site_url('retur_penjualan/ajax_list_retur')?>",
                type: "POST",
                data: {'id_penjualan_m' : $('#id_penjualan_m').html()}
            },
            columnDefs: [
                            {
                                "targets": [ -1 ],
                                "orderable": false,
                            },
                        ],
        });
    }

    function tampilkan_data_batal(){
        table_batal.clear();table_batal.destroy();
        table_batal=$('#TableTransaksiBatal').DataTable({
            ordering: true,
            bFilter:true,
            processing:true,
            serverSide:true,
            order:[],
            lengthMenu: [[5, 10, 25, 50, -1], [5, 10, 25, 50, "All"]],
            pagingType: "full",

            oLanguage:{
                sLoadingRecords:'Silakan Tunggu',
                sProcessing:"<img src='assets/loader/cubeloader2.2.gif' width='50' height='50'> Harap tunggu",
                sZeroRecords:"Belum ada data untuk ditampilkan",
                sSearch : "Pencarian data"
            },
            ajax:{
                url: "<?php echo site_url('retur_penjualan/ajax_list_retur_batal')?>",
                type: "POST",
                data: {'id_retur_penjualan_m' : $('#id_retur_penjualan_m').val()}
            },
            columnDefs: [
                            {
                                "targets": [ -1 ],
                                "orderable": false,
                            },
                        ],
        });
    }

    var delay = (function () {
        var timer = 0;
        return function (callback, ms) {
            clearTimeout(timer);
            timer = setTimeout(callback, ms);
        };
    })();

    $(document).on('click', '#tab_induk', function(e){
        tampilkan_data_induk();
    });

    $(document).on('click', '#tab_batal', function(e){
        tampilkan_data_batal();
    });

    function reload_table_induk()
    {
        table_induk.ajax.reload();
    }

    function reload_table_batal()
    {
        table_batal.ajax.reload();
    }

    $(document).on('click', '#daftar_autocompleted_transaksi li', function(){    
        var id_penjualan_m        = $(this).find('span#data_id_penjualan_m').html();
        var no_penjualan          = $(this).find('span#data_no_penjualan').html();
        var id_customer_pusat     = $(this).find('span#data_id_customer_pusat').html();
        var nama_customer         = $(this).find('span#data_nama_customer').html();
        var tanggal_penjualan     = $(this).find('span#data_tanggal_penjualan').html();
        var grand_total_penjualan = $(this).find('span#data_grand_total_penjualan').html();
        var jumlah_jual           = $(this).find('span#data_jumlah_jual').html();
        
        $('#hasil_pencarian_transaksi').hide();
        $('#id_penjualan_m').html(id_penjualan_m);   
        $('#pencarian_transaksi').val(no_penjualan);
        $('#id_customer_pusat').html(id_customer_pusat);
        $('#nama_customer').html(nama_customer);
        $('#tanggal_penjualan').html(tanggal_penjualan);
        $('#TotalPenjualan').html(to_rupiah(grand_total_penjualan));
        $('#TotalPenjualanHidden').html(grand_total_penjualan);
        $('#jumlah_barang').html(jumlah_jual);
        $('#pencarian_kode').focus();
        tampilkan_data_induk();
    });

    $(document).on('keyup', '#pencarian_transaksi', function(e){
        if($(this).val() !== ''){
            var charCode = e.which || e.keyCode;
            if(charCode == 40){
                if($('div#hasil_pencarian_transaksi li.autocomplete-active').length > 0){
                    var Selanjutnya = $('div#hasil_pencarian_transaksi li.autocomplete-active').next();
                    $('div#hasil_pencarian_transaksi li.autocomplete-active').removeClass('autocomplete-active');
                    Selanjutnya.addClass('autocomplete-active');
                }else{
                    $('div#hasil_pencarian_transaksi li:first').addClass('autocomplete-active');
                }
            }else if(charCode == 38){
                if($('div#hasil_pencarian_transaksi li.autocomplete-active').length > 0){
                    var Sebelumnya = $('div#hasil_pencarian_transaksi li.autocomplete-active').prev();
                    $('div#hasil_pencarian_transaksi li.autocomplete-active').removeClass('autocomplete-active');
                    Sebelumnya.addClass('autocomplete-active');
                }else{
                    $('div#hasil_pencarian_transaksi li:first').addClass('autocomplete-active');
                }
            }else if(charCode == 13){
                $('div#hasil_pencarian_barang').hide();
                $.ajax({
                    url: "<?php echo site_url('retur_penjualan/ajax_cari_no_penjualan'); ?>",
                    type: "POST",
                    cache: false,
                    data: 'no_penjualan=' + $('#pencarian_transaksi').val(),
                    dataType: 'json',
                    success: function(json){
                        if(json.status == 1){
                            $('#hasil_pencarian_transaksi').hide();
                            $('#id_penjualan_m').html(json.data['id_penjualan_m']);
                            $('#pencarian_transaksi').val(json.data['no_penjualan']);
                            $('#id_customer_pusat').html(json.data['id_customer_pusat']);
                            $('#nama_customer').html(json.data['nama_customer_pusat']);
                            $('#tanggal_penjualan').html(json.data['tanggal_penjualan']);
                            $('#TotalPenjualan').html(to_rupiah(json.data['grand_total']));
                            $('#TotalPenjualanHidden').html(json.data['grand_total']);
                            $('#jumlah_barang').html(json.data['jumlah_jual']);
                            $('#catatan').focus();
                            tampilkan_data_induk();
                        }else if(json.status == 0){
                            swal({
                                title: "Oops !",
                                text: json.pesan,
                                type: "error",
                                confirmButtonClass: 'btn-danger btn-md waves-effect waves-light',
                                confirmButtonText: "Ok"
                            },function(isConfirm){
                                window.location.href = url;
                            });
                        }
                    }, 
                    error: function (jqXHR, textStatus, errorThrown){
                        swal({
                            title: "Oops !",
                            text: "No. penjualan gagal dicari.",
                            type: "error",
                            confirmButtonClass: 'btn-danger btn-md waves-effect waves-light',
                            confirmButtonText: "Ok"
                        },function(isConfirm){
                            window.location.href = url;
                        });
                    }
                });
            }else{
                AutoCompletedDaftarTransaksiPenjualan($(this).val());
            }
        }else{
            $('#id_penjualan_m').html('');            
            $('#id_customer_pusat').html('-');
            $('#nama_customer').html('-');
            $('#tanggal_penjualan').html('-');
            $('#TotalPenjualan').html('0');
            $('#TotalPenjualanHidden').html('');
            $('#jumlah_barang').html('0');

            // Default detail barang
            $('#sku').html('0');
            $('#nama_barang').html('0');
            $('#jumlah_barang').html('0');
            $('#harga_satuan_hidden').val('0');
            $('#harga_satuan').html('0');
            $('#persen_potongan').prop('disabled', true).val('0.00');
            $('#potongan_harga').prop('disabled', true).val('0');
            $('#jumlah_beli').html('0');
            $('#jumlah_retur').prop('disabled', true).val('0');
            $('#sub_potongan').html('0');
            $('#sub_potongan_hidden').val('0');
            $('#sub_saldo').html('0');
            $('#sub_saldo_hidden').val('0');
            $('#sub_retur').html('0');
            $('#sub_retur_hidden').val('0');
            $('#masuk_stok').prop('disabled', true).val('-');
            $('#keterangan_retur').prop('disabled', true).val('');
            $('#SimpanDetail').prop('disabled', true);

            $('#pencarian_transaksi').focus();
        }
    });

    $(document).on('keydown', 'body', function(e){
        var charCode = ( e.which ) ? e.which : event.keyCode;

        if(charCode == 118) //F7
        {
            $('#catatan').focus();
            return false;
        }

        if(charCode == 119) //F8
        {
            konfirmasi_tahan_transaksi();
            return false;
        }

        if(charCode == 120) //F9
        {
            konfirmasi_simpan_transaksi();
            return false;
        }
    });

    $(document).on('click', 'body', function(){
        $('div#hasil_pencarian_transaksi').hide();
    });

    $(document).on('click', 'button#Tahan', function(){
        konfirmasi_tahan_transaksi();
    });

    $(document).on('click', 'button#Simpan', function(){
        konfirmasi_simpan_transaksi();
    });

    $(document).on('click', 'button#Laporan', function(){
        window.open("laporan_retur_penjualan_pusat",'_blank');
    });

    $(document).on('keyup', '#persen_potongan', function(e){
        var charCode = e.which || e.keyCode;
        if(charCode == 13){
            if($('#jumlah_retur').val() == 0){
                $('#jumlah_retur').val('');
            }
            $('#jumlah_retur').focus();
        }else{
            HitungSubtotal();
        }
    });

    $(document).on('keyup', '#potongan_harga', function(e){
        var charCode = e.which || e.keyCode;
        if(charCode == 13){
            if($('#jumlah_retur').val() == 0){
                $('#jumlah_retur').val('');
            }
            $('#jumlah_retur').focus();
        }else{
            var PH = $('#potongan_harga').val();
                PH = to_angka(PH);
            
            if(PH > 0){
                $('#potongan_harga').val(to_rupiah(PH));
                $('#potongan_harga_hidden').val(PH);
            }else{
                $('#potongan_harga').val('');
                $('#potongan_harga_hidden').val('0');
            }
            HitungSubtotal2();
        }
    });

    $(document).on('keyup', '#jumlah_retur', function(e){
        var charCode = e.which || e.keyCode;
        if(charCode == 13){
            $('#masuk_stok').focus();
        }else{
            HitungSubtotal();
        }
    });

    $(document).on('keyup', '#keterangan_retur', function(e){
        var charCode = e.which || e.keyCode;
        if(charCode == 13){
            kofirmasi_simpan_barang();
        }
    });

    function kofirmasi_simpan_barang(){
        var pesan = '';
        if($('#sku').html() > 0){
            if($('#jumlah_retur').val() > 0){
                if($('#jumlah_retur').val() > parseInt($('#jumlah_beli').html())){
                    pesan = 'Jumlah retur melebihi jumlah jual, harap kurangi jumlah retur';
                }else{
                    if($('#masuk_stok').val() !== '-'){
                        if($('#keterangan_retur').val() == ''){
                            pesan = 'Harap isikan keterangan retur terlebih dahulu';
                        }
                    }else{
                        pesan = 'Harap pilih masuk stok terlebih dahulu';
                    }                
                }
            }else{
                pesan = 'Harap isikan jumlah retur terlebih dahulu';            }
        }else{
            pesan = 'Belum ada barang yang anda simpan ditransaksi ini, harap simpan barang terlebih dahulu';
        }

        if(pesan !== ''){ 
            swal({
                title: "Oops !",
                text: pesan,
                type: "error",
                confirmButtonClass: 'btn-danger btn-md waves-effect waves-light',
                confirmButtonText: "Ok"
            });
        }else{
            $('.modal-dialog').removeClass('modal-lg');
            $('.modal-dialog').addClass('modal-sm');
            $('#ModalHeader').html('Konfirmasi');
            $('#ModalContent').html(
                                        "<b class='text-dark'>Apakah anda yakin ingin menyimpan retur penjualan barang ini ? </b></br></br>" +
                                        "SKU : </br>" +

                                        "<b class='text-dark'>" + $('#pencarian_kode_barang').val() + "</b></br></br>" +

                                        "Nama Barang : </br>" +
                                        "<b class='text-dark'>" + $('#nama_barang').html() + "</b></br></br>" +

                                        "Jumlah Jual : </br>" +
                                        "<h2 class='text-dark'>" + $('#jumlah_beli').html() + "</h2>" +

                                        "Jumlah Retur : </br>" +
                                        "<h2 class='text-danger'>" + $('#jumlah_retur').val() + "</h2>"
                                    );
            $('#ModalFooter').html("<button type='button' class='btn btn-primary' id='SimpanDataBarang' onClick='simpan_barang();'>Ya, saya yakin</button>" +
                                   "<button type='button' class='btn btn-default' data-dismiss='modal'>Batal</button>");
            $('#ModalGue').modal('show');

            setTimeout(function(){
                $('button#SimpanDataBarang').focus();
            }, 500);
        }
    }

    $(document).on('click', 'button#SimpanDetail', function(){
        kofirmasi_simpan_barang();
    });

    function simpan_barang()
    {
        $('#ModalGue').modal('hide');
        var FormData = "no_retur_penjualan="+$('#no_retur_penjualan').html(); 
            FormData += "&id_retur_penjualan_m="+$('#id_retur_penjualan_m').html();
            FormData += "&id_penjualan_m="+$('#id_penjualan_m').html();
            FormData += "&id_penjualan_d="+$('#id_penjualan_d').val();
            FormData += "&no_penjualan="+$('#pencarian_transaksi').val();
            FormData += "&id_customer_pusat="+$('#id_customer_pusat').html();
            FormData += "&id_barang_pusat="+$('#id_barang_pusat').val();
            FormData += "&sku="+$('#sku').val();
            FormData += "&harga_satuan="+$('#harga_satuan_hidden').val();
            FormData += "&potongan_harga_value="+$('#persen_potongan').val();
            FormData += "&potongan_harga="+$('#potongan_harga_hidden').val();
            FormData += "&jumlah_beli="+$('#jumlah_beli').html();
            FormData += "&jumlah_retur="+$('#jumlah_retur').val();
            FormData += "&subtotal_potongan="+$('#sub_potongan_hidden').val();
            FormData += "&subtotal_saldo="+$('#sub_saldo_hidden').val();
            FormData += "&subtotal_retur="+$('#sub_retur_hidden').val();
            FormData += "&masuk_stok="+$('#masuk_stok').val();
            FormData += "&keterangan_retur="+encodeURI($('#keterangan_retur').val());
            FormData += "&catatan="+encodeURI($('#catatan').val());

        $.ajax({
            url: "<?php echo site_url('retur_penjualan/simpan_detail'); ?>",
            type: "POST",
            cache: false,
            data: FormData,
            dataType:'json',
            success: function(data){
                if(data.status == 1){
                    if(url == 'retur_penjualan'){
                        swal({
                            title: "Berhasil!", text: data.pesan, type: "success",
                            confirmButtonText: "Ok",
                        },function(isConfirm){
                            window.location.href = data.url;
                        });
                        return;
                    }else{
                        tampilkan_data_induk();
                    }

                    $('#judul_tab_induk').html("Data Retur Penjaualan : <span class='badge up bg-primary'>" +
                                                data.jumlah_barang +
                                               " Barang </span>");
                    $('#judul_tab_batal').html("Data Retur Penjaualan Batal : <span class='badge up bg-danger'>" +
                                                data.jumlah_barang_batal +
                                               " Barang </span>");

                    // Tampilkan id penjualan master dan total bayar dll
                    $('#id_retur_penjualan_m').val(data.id_rpm);
                    $('#JumlahBarangRetur').html(to_rupiah(data.jumlah_barang));
                    $('#TotalRetur').html(to_rupiah(data.total_retur));
                    $('#TotalReturHidden').val(data.total_retur);
                    $('#TotalPotongan').html(to_rupiah(data.total_potongan));
                    $('#TotalPotonganHidden').val(data.total_potongan);
                    $('#TotalSaldo').html(to_rupiah(data.total_saldo));
                    $('#TotalSaldoHidden').val(data.total_saldo);                

                    // Bersihkan data pemilihan barang
                    $('#id_penjualan_d').val('');
                    $('#id_barang_pusat').val('');
                    $('#sku').html('');
                    $('#nama_barang').html('Belum ada barang yg dipilih');
                    $('#jumlah_beli').html('0');
                    $('#harga_satuan_hidden').val('0');
                    $('#harga_satuan').html('Rp. 0');
                    $('#persen_potongan').val('0.00');
                    $('#potongan_harga').val('Rp. 0');
                    $('#potongan_harga_hidden').val('0');
                    $('#jumlah_retur').val('0');
                    $('#sub_potongan').html('Rp. 0');
                    $('#sub_potongan_hidden').val('0');
                    $('#sub_saldo').html('Rp. 0');
                    $('#sub_saldo_hidden').val('0');
                    $('#sub_retur').html('Rp. 0');
                    $('#sub_retur_hidden').val('0');
                    $('#masuk_stok').val('-');
                    $('#keterangan_retur').val('');

                    $('#persen_potongan').prop('disabled', true);
                    $('#potongan_harga').prop('disabled', true);
                    $('#jumlah_retur').prop('disabled', true);
                    $('#masuk_stok').prop('disabled', true);
                    $('#keterangan_retur').prop('disabled', true);
                    $('#SimpanDetail').prop('disabled', true);
                    swal({
                        title: "Berhasil!", text: data.pesan, type: "success",
                        confirmButtonText: "Ok",
                    },function(isConfirm){
                        setTimeout(function(){
                            $('#catatan').focus();
                        }, 100);
                    });

                }else if(data.status == 2){
                    $('.modal-dialog').removeClass('modal-lg');
                    $('.modal-dialog').addClass('modal-sm');
                    $('#ModalHeader').html('Oops !');
                    $('#ModalContent').html(data.pesan);
                    $('#ModalFooter').html("<button type='button' class='btn btn-danger' data-dismiss='modal' autofocus>Ok, Saya Mengerti</button>");
                    $('#ModalGue').modal('show');
                }else if(data.status == 0){
                    swal({
                        title: "Oops !",
                        text: data.pesan,
                        type: "error",
                        confirmButtonClass: 'btn-danger btn-md waves-effect waves-light',
                        confirmButtonText: "Ok"
                    },function (isConfirm){
                        window.location.href = url;
                    });
                }
            },
            error: function (jqXHR, textStatus, errorThrown){
                swal({
                    title: "Oops !",
                    text: "Gagal menyimpan data retur penjualan detail.",
                    type: "error",
                    confirmButtonClass: 'btn-danger btn-md waves-effect waves-light',
                    confirmButtonText: "Ok"
                },function (isConfirm){
                    window.location.href = url;
                });
            }
        });
    }

    function edit_penjualan_detail(id_penjualan_d)
    {
        $.ajax({
            url : "<?php echo site_url('retur_penjualan/ajax_edit_retur')?>",
            type: "POST",
            cache: false,
            data: 'id_penjualan_d=' + id_penjualan_d,
            dataType: "JSON",
            success: function(json){
                if(json.status == 1){
                    $('#id_penjualan_d').val(id_penjualan_d);
                    $('#id_barang_pusat').val(json.data['id_barang_pusat']);
                    $('#sku').html(json.data['sku']);
                    $('#nama_barang').html(json.data['nama_barang']);
                    $('#jumlah_beli').html(json.data['jumlah_beli']);
                    $('#harga_satuan_hidden').val(json.data['harga_satuan']);
                    $('#harga_satuan').html(to_rupiah(json.data['harga_satuan']));
                    $('#persen_potongan').val(json.data['potongan_harga_value']);
                    $('#potongan_harga').val(to_rupiah(json.data['potongan_harga']));
                    $('#potongan_harga_hidden').val(json.data['potongan_harga']);
                    $('#jumlah_retur').val(json.data['jumlah_retur']);

                    $('#masuk_stok').val(json.data['masuk_stok']);
                    $('#keterangan').val(json.data['keterangan']);
                    $('#sub_potongan').html(to_rupiah(json.data['subtotal_potongan']));
                    $('#sub_potongan_hidden').val(json.data['subtotal_potongan']);
                    $('#sub_saldo').html(to_rupiah(json.data['subtotal_saldo']));
                    $('#sub_saldo_hidden').val(json.data['subtotal_saldo']);
                    $('#sub_retur').html(to_rupiah(json.data['subtotal_retur']));
                    $('#sub_retur_hidden').val(json.data['subtotal_retur']);
                    $('#keterangan_retur').val(json.data['keterangan']);

                    $('#persen_potongan').removeAttr('disabled').val();
                    $('#potongan_harga').removeAttr('disabled').val();
                    $('#jumlah_retur').removeAttr('disabled').val();
                    $('#masuk_stok').removeAttr('disabled').val();
                    $('#keterangan_retur').removeAttr('disabled').val();
                    $('#SimpanDetail').removeAttr('disabled').val();
                    $('#persen_potongan').focus();
                }else if(json.status == 0){
                    swal({
                        title: "Oops !",
                        text: json.pesan,
                        type: "error",
                        confirmButtonClass: 'btn-danger btn-md waves-effect waves-light',
                        confirmButtonText: "Ok"
                    },function (isConfirm){
                        window.location.href = url;
                    });
                }
            },
            error: function (jqXHR, textStatus, errorThrown){
                swal({
                    title: "Oops !",
                    text: "Gagal menampilkan data retur penjualan detail.",
                    type: "error",
                    confirmButtonClass: 'btn-danger btn-md waves-effect waves-light',
                    confirmButtonText: "Ok"
                },function (isConfirm){
                    window.location.href = url;
                });
            }
        });
    }

    function verifikasi_hapus_retur_penjualan_detail(id_retur_penjualan_d)
    {
        $.ajax({
            url : "<?php echo site_url('retur_penjualan/ajax_verifikasi_hapus_detail')?>",
            type: "POST",
            cache: false,
            data: 'id_retur_penjualan_d=' + id_retur_penjualan_d,
            dataType: "JSON",
            success: function(json){
                if(json.status == 1){
                    $('.modal-dialog').removeClass('modal-lg');
                    $('.modal-dialog').addClass('modal-sm');
                    $('#ModalHeader').html('Informasi Hapus Barang');
                    $('#ModalContent').html(json.pesan);
                    $('#ModalFooter').html(json.footer);
                    $('#ModalGue').modal('show');
                }else{
                    swal({
                        title: "Oops !",
                        text: json.pesan,
                        type: "error",
                        confirmButtonClass: 'btn-danger btn-md waves-effect waves-light',
                        confirmButtonText: "Ok"
                    },function (isConfirm){
                        window.location.href = url;
                    });
                }
            },
            error: function (jqXHR, textStatus, errorThrown){
                swal({
                    title: "Oops !",
                    text: "Gagal menampilkan data retur penjualan detail.",
                    type: "error",
                    confirmButtonClass: 'btn-danger btn-md waves-effect waves-light',
                    confirmButtonText: "Ok"
                },function (isConfirm){
                    window.location.href = url;
                });
            }
        });
    }

    function hapus_retur_penjualan_detail(id_retur_penjualan_d)
    {
        var FormData = "id_retur_penjualan_d=" + id_retur_penjualan_d;
            FormData += "&id_customer_pusat=" + encodeURI($('#id_customer_hidden').val());
            FormData += "&catatan=" + encodeURI($('#catatan').val());
            FormData += "&keterangan_batal=" + encodeURI($('#keterangan_batal').val());

        $.ajax({
            url : "<?php echo site_url('retur_penjualan/ajax_hapus_detail')?>",
            type: "POST",
            cache: false,
            data: FormData,
            dataType: "JSON",
            success: function(data){
                if(data.status == 1){
                    // Tampilkan id penjualan master dan total bayar dll
                    $('#id_retur_penjualan_m').val(data.id_rpm);
                    $('#JumlahBarangRetur').html(to_rupiah(data.jumlah_barang));
                    $('#TotalRetur').html(to_rupiah(data.total_retur));
                    $('#TotalReturHidden').val(data.total_retur);
                    $('#TotalPotongan').html(to_rupiah(data.total_potongan));
                    $('#TotalPotonganHidden').val(data.total_potongan);
                    $('#TotalSaldo').html(to_rupiah(data.total_saldo));
                    $('#TotalSaldoHidden').val(data.total_saldo);

                     $('#judul_tab_induk').html("Data Retur Penjaualan : <span class='badge up bg-primary'>" +
                                                data.jumlah_barang +
                                               " Barang </span>");
                    $('#judul_tab_batal').html("Data Retur Penjaualan Batal : <span class='badge up bg-danger'>" +
                                                data.jumlah_barang_batal +
                                               " Barang </span>");            

                    // Bersihkan data pemilihan barang
                    $('#id_barang_pusat').val('');
                    $('#sku').html('');
                    $('#nama_barang').html('Belum ada barang yg dipilih');
                    $('#jumlah_beli').html('0');
                    $('#harga_satuan_hidden').val('0');
                    $('#harga_satuan').html('Rp. 0');
                    $('#persen_potongan').val('0.00');
                    $('#potongan_harga').val('Rp. 0');
                    $('#potongan_harga_hidden').val('0');
                    $('#jumlah_retur').val('0');
                    $('#sub_potongan').html('Rp. 0');
                    $('#sub_potongan_hidden').val('0');
                    $('#sub_saldo').html('Rp. 0');
                    $('#sub_saldo_hidden').val('0');
                    $('#sub_retur').html('Rp. 0');
                    $('#sub_retur_hidden').val('0');
                    $('#masuk_stok').val('-');
                    $('#keterangan_retur').val('');

                    $('#persen_potongan').prop('disabled', true);
                    $('#potongan_harga').prop('disabled', true);
                    $('#jumlah_retur').prop('disabled', true);
                    $('#masuk_stok').prop('disabled', true);
                    $('#keterangan_retur').prop('disabled', true);
                    $('#SimpanDetail').prop('disabled', true);

                    tampilkan_data_induk();
                    swal({
                        title: "Berhasil!",
                        text: "Data retur penjualan barang berhasil dihapus.",
                        type: "success",
                        confirmButtonText: "Ok"
                    });
                }else if(data.status == 0){
                    swal({
                        title: data.judul,
                        text: data.pesan,
                        type: data.tipe_pesan,
                        confirmButtonClass: data.gaya_tombol,
                        confirmButtonText: "Ok"
                    },function (isConfirm){
                        window.location.href = url;
                    });
                }
            },
            error: function (jqXHR, textStatus, errorThrown){
                swal({
                    title: "Gagal!",
                    text: "Barang retur penjualan gagal dihapus.",
                    type: "error",
                    confirmButtonClass: 'btn-danger btn-md waves-effect waves-light',
                    confirmButtonText: "Ok"
                },function (isConfirm){
                    window.location.href = url;
                });
            }
        });
    }

    function tampilkan_total_penjualan()
    {
        $.ajax({
            url: "<?php echo site_url('penjualan/tampilkan_total_penjualan'); ?>",
            type: "POST",
            cache: false,
            data: 'no_penjualan=' + $('#no_penjualan').html(),
            dataType:'json',
            success: function(data){
                if(data.status == 1){   
                    $('#id_penjualan_m').val(data.id_pm);                    
                    $('#pencarian_kode').val('');
                    $('#nama_barang').html('Belum ada barang yg dipilih');
                    $('#harga_satuan').val('');
                    $('#harga_satuan_tampil').html('Rp. 0');
                    $('#jenis_harga').val('');
                    $('#jumlah_beli').val('0');
                    $('#discount').val('0.00');
                    $('#discount_hidden').val('');
                    $('#sub_total').val('');
                    $('#sub_total_tampil').html('Rp. 0');
                    $('#jumlah_beli').prop('disabled', true);
                    $('#discount').prop('disabled', true)
                }else if(data.status == 0){
                    $('.modal-dialog').removeClass('modal-lg');
                    $('.modal-dialog').addClass('modal-sm');
                    $('#ModalHeader').html('Oops !');
                    $('#ModalContent').html(data.pesan);
                    $('#ModalFooter').html("<button type='button' class='btn btn-primary' data-dismiss='modal' autofocus>Ok, Saya Mengerti</button>");
                    $('#ModalGue').modal('show');
                }
            }
        });
    }

    function konfirmasi_tahan_transaksi(){
        if($('#id_penjualan_m').html() == '' | $('#id_penjualan_m').html() == '0'){
            swal({
                title: "Oops !",
                text: "Belum ada transaksi penjualan yang anda pilih untuk di retur.",
                type: "error",
                confirmButtonClass: 'btn-danger btn-md waves-effect waves-light',
                confirmButtonText: "Ok"
            }, function (isConfirm){
                setTimeout(function(){
                    $('#pencarian_transaksi').focus();
                }, 100);
            });    
        }else{
            if($('#id_retur_penjualan_m').val() == '' | $('#id_retur_penjualan_m').val() == '0'){
                swal({
                    title: "Oops !",
                    text: "Belum ada barang yang anda retur ditransaksi ini.",
                    type: "error",
                    confirmButtonClass: 'btn-danger btn-md waves-effect waves-light',
                    confirmButtonText: "Ok"
                });
                return;
            }else{
                swal({
                    title: "Apakah anda yakin ingin menahan transaksi retur penjualan ini ?",
                    type: "info",
                    showCancelButton: true,
                    confirmButtonClass: 'btn-primary btn-md waves-effect waves-light',
                    confirmButtonText: "Iya, saya yakin",
                    cancelButtonText: "Batal",
                },function (isConfirm){
                    if(isConfirm){
                        TahanTransaksi();
                    }
                });
            }
        }
    }

    function TahanTransaksi()
    {
        var FormData = "&id_retur_penjualan_m="+$('#id_retur_penjualan_m').val();
            FormData += "&id_penjualan_m="+$('#id_penjualan_m').html();
            FormData += "&no_retur_penjualan="+$('#no_retur_penjualan').html();
            FormData += "&no_penjualan="+$('#pencarian_transaksi').val();
            FormData += "&id_customer_pusat="+$('#id_customer_pusat').html();
            FormData += "&catatan="+encodeURI($('#catatan').val());
            FormData += "&url=" + url;

        $.ajax({
            url: "<?php echo site_url('retur_penjualan/tahan_transaksi'); ?>",
            type: "POST",
            cache: false,
            data: FormData,
            dataType:'json',
            success: function(data){
                if(data.status == 1){
                    swal({
                        title: "Berhasil!",
                        text: data.pesan,
                        type: "success",
                        confirmButtonText: "Ok"
                    }, function (isConfirm){
                        if(isConfirm){
                            window.location.href = "retur_penjualan";
                        }
                    });
                }else if(data.status == 0){
                    swal({
                        title: "Oops !",
                        text: data.pesan,
                        type: "error",
                        confirmButtonClass: 'btn-danger btn-md waves-effect waves-light',
                        confirmButtonText: "Ok"
                    }, function (isConfirm){
                        if(isConfirm){
                            window.location.href = data.url;
                        }
                    });
                }
            },
            error: function (jqXHR, textStatus, errorThrown){
                swal({
                    title: "Oops !",
                    text: "Gagal menahan transaksi retur penjualan ini.",
                    type: "error",
                    confirmButtonClass: 'btn-danger btn-md waves-effect waves-light',
                    confirmButtonText: "Ok"
                }, function (isConfirm){
                    if(isConfirm){
                        window.location.href = url;
                    }
                });
            }
        });
    }

    function konfirmasi_simpan_transaksi(){
        if($('#id_penjualan_m').html() == '' | $('#id_penjualan_m').html() == '0'){
            swal({
                title: "Oops !",
                text: "Belum ada transaksi penjualan yang anda pilih untuk di retur.",
                type: "error",
                confirmButtonClass: 'btn-danger btn-md waves-effect waves-light',
                confirmButtonText: "Ok"
            }, function (isConfirm){
                setTimeout(function(){
                    $('#pencarian_transaksi').focus();
                }, 100);
            });    
        }else{
            if($('#id_retur_penjualan_m').val() == '' | $('#id_retur_penjualan_m').val() == '0'){
                swal({
                    title: "Oops !",
                    text: "Belum ada barang yang anda retur ditransaksi ini.",
                    type: "error",
                    confirmButtonClass: 'btn-danger btn-md waves-effect waves-light',
                    confirmButtonText: "Ok"
                });
                return;
            }else{
                swal({
                    title: "Apakah anda yakin ingin menyimpan dan menyelesaikan transaksi retur penjualan ini ?",
                    type: "info",
                    showCancelButton: true,
                    confirmButtonClass: 'btn-primary btn-md waves-effect waves-light',
                    confirmButtonText: "Iya, saya yakin",
                    cancelButtonText: "Batal",
                },function (isConfirm){
                    if(isConfirm){
                        SimpanTransaksi();
                    }
                });
            }
        }
    }

    function SimpanTransaksi()
    {
        var FormData = "&id_retur_penjualan_m="+$('#id_retur_penjualan_m').val();
            FormData += "&id_penjualan_m="+$('#id_penjualan_m').html();
            FormData += "&no_retur_penjualan="+$('#no_retur_penjualan').html();
            FormData += "&no_penjualan="+$('#pencarian_transaksi').val();
            FormData += "&id_customer_pusat="+$('#id_customer_pusat').html();
            FormData += "&catatan="+encodeURI($('#catatan').val());
            FormData += "&url=" + url;

        $.ajax({
            url: "<?php echo site_url('retur_penjualan/simpan_transaksi'); ?>",
            type: "POST",
            cache: false,
            data: FormData,
            dataType:'json',
            success: function(data){
                if(data.status == 1){
                    swal({
                        title: "Berhasil!",
                        text: data.pesan,
                        type: "success",
                        confirmButtonText: "Iya, cetak faktur",
                        cancelButtonText: "Tidak, nanti saja",
                        showCancelButton: true
                    }, function (isConfirm){
                        if(isConfirm) {
                            var FormData = "&id_retur_penjualan_m="+$('#id_retur_penjualan_m').val();
                            window.open("<?php echo site_url('faktur/faktur_retur_penjualan/?'); ?>" + FormData,'_blank');
                            window.location.href = "retur_penjualan";
                        }else{
                            window.location.href = "retur_penjualan";
                        }
                    });
                }else if(data.status == 0){
                    swal({
                        title: "Oops !",
                        text: data.pesan,
                        type: "error",
                        confirmButtonClass: 'btn-danger btn-md waves-effect waves-light',
                        confirmButtonText: "Ok"
                    },function (isConfirm){
                        if(isConfirm){
                            window.location.href = data.url;
                        }
                    });
                }
            },
            error: function (jqXHR, textStatus, errorThrown){
                swal({
                    title: "Oops !",
                    text: "Gagal menyimpan transaksi retur penjualan ini.",
                    type: "error",
                    confirmButtonClass: 'btn-danger btn-md waves-effect waves-light',
                    confirmButtonText: "Ok"
                }, function (isConfirm){
                    if(isConfirm){
                        window.location.href = url;
                    }
                });
            }
        });
    }

    function AutoCompletedDaftarTransaksiPenjualan(KataKunci)
    {
        $('#tombol_cari_transaksi').removeClass('btn-warning');
        $('#tombol_cari_transaksi').addClass('btn-success');

        $('#induk_pencarian_transaksi').removeClass('has-warning');
        $('#induk_pencarian_transaksi').addClass('has-success');

        $('#icon_pencarian_transaksi').removeClass('fa fa-search');
        $('#icon_pencarian_transaksi').removeClass('fa fa-band');
        $('#icon_pencarian_transaksi').addClass('fa fa-spin fa-refresh');
        $('div#hasil_pencarian_transaksi').hide();

        // Default detail barang
        $('#id_penjualan_d').val('');
        $('#id_barang_pusat').val('');
        $('#sku').html('0');
        $('#nama_barang').html('0');
        $('#jumlah_barang').html('0');
        $('#harga_satuan_hidden').val('0');
        $('#harga_satuan').html('0');
        $('#persen_potongan').prop('disabled', true).val('0.00');
        $('#potongan_harga').prop('disabled', true).val('0');
        $('#jumlah_beli').html('0');
        $('#jumlah_retur').prop('disabled', true).val('0');
        $('#sub_potongan').html('0');
        $('#sub_potongan_hidden').val('0');
        $('#sub_saldo').html('0');
        $('#sub_saldo_hidden').val('0');
        $('#sub_retur').html('0');
        $('#sub_retur_hidden').val('0');
        $('#masuk_stok').prop('disabled', true).val('-');
        $('#keterangan_retur').prop('disabled', true).val('');
        $('#SimpanDetail').prop('disabled', true);        

        delay(function(){
            $.ajax({
                url: "<?php echo site_url('retur_penjualan/ajax_pencarian_transaksi'); ?>",
                type: "POST",
                cache: false,
                data: 'keyword=' + KataKunci,
                dataType:'json',
                success: function(json){
                    if(json.status == 1){
                        $('div#hasil_pencarian_transaksi').show('fast');
                        $('div#hasil_pencarian_transaksi').html(json.datanya);

                        $('#icon_pencarian_transaksi').removeClass('fa fa-spin fa-refresh');
                        <?php if($access_create == 1 || $access_update == 1){ ?>
                            $('#icon_pencarian_transaksi').addClass('fa fa-search');
                        <?php }else{ ?>
                            $('#icon_pencarian_transaksi').addClass('fa fa-band');
                        <?php } ?>
                        $('#id_customer_pusat').html('-');            
                        $('#nama_customer').html('-');
                        $('#tanggal_penjualan').html('-');
                        $('#TotalPenjualan').html('0');
                        $('#TotalPenjualanHidden').html('');
                        $('#jumlah_barang').html('0');
                        $('#id_penjualan_m').html('')
                        
                        $('#id_penjualan_m').html('0');
                        tampilkan_data_induk();
                    }else if(json.status == 2){
                        swal({
                            title: "Oops !",
                            text: "Maaf anda tidak diizinkan untuk menambahkan data retur penjualan!",
                            type: "error",
                            confirmButtonClass: 'btn-danger btn-md waves-effect waves-light',
                            confirmButtonText: "Ok"
                        },function (isConfirm){
                            window.location.href = url;
                        });
                    }else if(json.status == 0){
                        $('#tombol_cari_transaksi').removeClass('btn-success');
                        $('#tombol_cari_transaksi').addClass('btn-warning');
                        $('#icon_pencarian_transaksi').removeClass('fa fa-spin fa-refresh');
                        <?php if($access_create == 1 || $access_update == 1){ ?>
                            $('#icon_pencarian_transaksi').addClass('fa fa-search');
                        <?php }else{ ?>
                            $('#icon_pencarian_transaksi').addClass('fa fa-band');
                        <?php } ?>
                        $('#induk_pencarian_transaksi').removeClass('has-success');
                        $('#induk_pencarian_transaksi').addClass('has-warning');
                        
                        $('#id_penjualan_m').html('0');
                        tampilkan_data_induk();
                    }
                }
            });
        }, 1000);
    }

    function HitungSubtotal()
    {
        var Harga              = $('#harga_satuan_hidden').val();
        var JumlahRetur        = $('#jumlah_retur').val();
        var PersentasePotongan = $('#persen_potongan').val();
        var PotonganHarga      = PersentasePotongan * parseInt(Harga);

        if(PotonganHarga > 0){
            $('#potongan_harga').val(to_rupiah(PotonganHarga));
            $('#potongan_harga_hidden').val(PotonganHarga);
        }else{
            $('#potongan_harga').val('0');
            $('#potongan_harga_hidden').val('0');
        }

        var SubPotongan   = parseInt(PotonganHarga) * parseInt(JumlahRetur);
        var SubSaldo      = parseInt(Harga-PotonganHarga) * parseInt(JumlahRetur);
        var SubRetur      = parseInt(Harga) * parseInt(JumlahRetur);
        
        var SubPotonganRp = 'Rp. 0';
        var SubSaldoRp    = 'Rp. 0';
        var SubReturRp    = 'Rp. 0';

        if(SubPotongan >= '0')
        {SubPotonganRp  = to_rupiah(SubPotongan);}

        if(SubSaldo >= '0')
        {SubSaldoRp  = to_rupiah(SubSaldo);}

        if(SubRetur >= '0')
        {SubReturRp  = to_rupiah(SubRetur);}
        
        $('#sub_potongan_hidden').val(SubPotongan);
        $('#sub_potongan').html(SubPotonganRp);

        $('#sub_saldo_hidden').val(SubSaldo);
        $('#sub_saldo').html(SubSaldoRp);

        $('#sub_retur_hidden').val(SubRetur);
        $('#sub_retur').html(SubReturRp);    
    }

    function HitungSubtotal2()
    {
        var Harga              = $('#harga_satuan_hidden').val();
        var JumlahRetur        = $('#jumlah_retur').val();
        var PotonganHarga      = $('#potongan_harga_hidden').val();
        var PersentasePotongan = (PotonganHarga/Harga);
        
        if (PersentasePotongan > 0){
            $('#persen_potongan').val(PersentasePotongan);
        }else{
            $('#persen_potongan').val('0.00');
        }
        
        var SubPotongan        = parseInt(PotonganHarga) * parseInt(JumlahRetur);
        var SubSaldo           = parseInt(Harga-PotonganHarga) * parseInt(JumlahRetur);
        var SubRetur           = parseInt(Harga) * parseInt(JumlahRetur);
        
        var SubPotonganRp      = 'Rp. 0';
        var SubSaldoRp         = 'Rp. 0';
        var SubReturRp         = 'Rp. 0';
        
        if(SubPotongan         >= '0')
        {SubPotonganRp         = to_rupiah(SubPotongan);}
        
        if(SubSaldo            >= '0')
        {SubSaldoRp            = to_rupiah(SubSaldo);}
        
        if(SubRetur            >= '0')
        {SubReturRp            = to_rupiah(SubRetur);}
        
        $('#sub_potongan_hidden').val(SubPotongan);
        $('#sub_potongan').html(SubPotonganRp);

        $('#sub_saldo_hidden').val(SubSaldo);
        $('#sub_saldo').html(SubSaldoRp);

        $('#sub_retur_hidden').val(SubRetur);
        $('#sub_retur').html(SubReturRp);    
    }

    function to_rupiah(angka){
        var rev        = parseInt(angka, 10).toString().split('').reverse().join('');
        var rev2       = '';
        for(var i      = 0; i < rev.length; i++){
            rev2       += rev[i];
            if((i + 1) % 3 === 0 && i !== (rev.length - 1)){
                rev2   += '.';
            }
        }
        return rev2.split('').reverse().join('');

        // Pakai Simbol Rp.
        // return 'Rp. ' + rev2.split('').reverse().join('');
    }

    function to_angka(rp)
    {
        return parseInt(rp.replace(/,.*|\D/g,''),10)
    }

    function check_int(evt) {
        var charCode = ( evt.which ) ? evt.which : event.keyCode;
        return ( charCode >= 48 && charCode <= 57 || charCode == 8 );
    }
</script>
<!-- Akhir Script CRUD -->
<!-- Start content -->
<div class="content">
	<div class="container">
		<div class="row">
			<div class="col-xs-12">
				<div class="page-title-box">
					<h6 class="page-title text-primary">
                        <?php echo $atribut_halaman ?>
                    </h6>
					<ol class="breadcrumb p-0 m-0">
						<li>
                            <b id="ucapan" class="text-dark"></b>    
                            <b class="text-primary"><?php echo $data_pegawai->nama_pegawai ?></b>    
                        </li>

                        <li>
                            <b id="tanggal" class="text-dark"></b>
                            <b id="tanggal_awal" class="text-dark" style="display: none;"></b>
                            <b id="tanggal_sekarang" class="text-dark" style="display: none;"></b>
						</li>
						<li>
							<b id="jam" class="text-dark"></b>
						</li>
                        <li>
                            <b id="segarkan_data"><i class="fa fa-refresh"></i></b>
                        </li>
					</ol>
					<div class="clearfix"></div>
				</div>
			</div>
		</div>
		<!-- end row -->
        <!-- <br/> -->

        <?php if($this->session->userdata('usergroup_name') == 'Super Admin' or 
                 $this->session->userdata('usergroup_name') == 'Admin'){ ?>
            <!-- Awal total  -->
            <!-- Awal baris 1 | Penjualan dan Retur Penjulaan Umum -->
            <div id="baris_1" class="row" hidden>
                <div class="col-lg-3 col-md-3 col-sm-6">
                    <div class="card-box widget-box-one">
                        <i class="ti ti-agenda widget-one-icon"></i>
                        <div class="wigdet-one-content">
                            <p class="m-0 text-uppercase font-600 font-secondary text-danger" title="Statistics">Penjualan Menunggu - UMUM</p>
                            <h2 class="text-danger">Rp.
                                <span id="total_penjualan_menunggu_umum"></span> 
                            </h2>
                            <div class="row">
                                <div class="col-md-5">
                                    <small class="text-danger"><b>Jml Transaksi</b></small><br/>
                                    <b id="jumlah_penjualan_menunggu_umum" class="text-dark"></b>
                                </div>
                                <div class="col-md-7">
                                    <small class="text-danger"><b>Periode</b></small><br/>
                                    <b id="tanggal_penjualan_menunggu_umum" class="text-dark"></b>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- <div class="col-lg-3 col-md-3 col-sm-6">
                    <div class="card-box widget-box-one">
                        <i class="ti ti-write widget-one-icon"></i>
                        <div class="wigdet-one-content">
                            <p class="m-0 text-uppercase font-600 font-secondary text-warning" title="Statistics">Penjualan Disiapkan - UMUM</p>
                            <h2 class="text-warning">Rp.
                                <span id="total_penjualan_disiapkan_umum"></span> 
                            </h2>
                            <div class="row">
                                <div class="col-md-5">
                                    <small class="text-warning"><b>Jml Transaksi</b></small><br/>
                                    <b id="jumlah_penjualan_disiapkan_umum" class="text-dark"></b>
                                </div>
                                <div class="col-md-7">
                                    <small class="text-warning"><b>Periode</b></small><br/>
                                    <b id="tanggal_penjualan_disiapkan_umum" class="text-dark"></b>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> -->

                <div class="col-lg-3 col-md-3 col-sm-6">
                    <div class="card-box widget-box-one">
                        <i class="ti-shopping-cart widget-one-icon"></i>
                        <div class="wigdet-one-content">
                            <p class="m-0 text-uppercase font-600 font-secondary text-success" title="Statistics">Penjualan Selesai - UMUM</p>
                            <h2>Rp.
                                <span id="total_penjualan_selesai_umum"></span> 
                            </h2>
                            <div class="row">
                                <div class="col-md-5">
                                    <small class="text-success"><b>Jml Transaksi</b></small><br/>
                                    <b id="jumlah_penjualan_selesai_umum" class="text-dark"></b>
                                </div>
                                <div class="col-md-7">
                                    <small class="text-success"><b>Periode</b></small><br/>
                                    <b id="tanggal_penjualan_selesai_umum" class="text-dark"></b>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-3 col-md-3 col-sm-6">
                    <div class="card-box widget-box-one">
                        <i class="ti ti-shopping-cart-full widget-one-icon"></i>
                        <div class="wigdet-one-content">
                            <p class="m-0 text-uppercase font-600 font-secondary text-success" title="Statistics">Retur Penjualan - UMUM</p>
                            <h2>Rp. 
                                <span id="total_retur_penjualan_umum"></span> 
                            </h2>
                            <div class="row">
                                <div class="col-md-5">
                                    <small class="text-success"><b>Jml Transaksi</b></small><br/>
                                    <b id="jumlah_retur_penjualan_umum" class="text-dark"></b>
                                </div>
                                <div class="col-md-7">
                                    <small class="text-success"><b>Periode</b></small><br/>
                                    <b id="tanggal_retur_penjualan_umum" class="text-dark"></b>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-3 col-md-3 col-sm-6">
                    <div class="card-box widget-box-one">
                        <i class="ti ti-agenda widget-one-icon"></i>
                        <div class="wigdet-one-content">
                            <p class="m-0 text-uppercase font-600 font-secondary text-danger" title="Statistics">Penjualan Menunggu - MRC</p>
                            <h2 class="text-danger">Rp.
                                <span id="total_penjualan_menunggu_mrc"></span> 
                            </h2>
                            <div class="row">
                                <div class="col-md-5">
                                    <small class="text-danger"><b>Jml Transaksi</b></small><br/>
                                    <b id="jumlah_penjualan_menunggu_mrc" class="text-dark"></b>
                                </div>
                                <div class="col-md-7">
                                    <small class="text-danger"><b>Periode</b></small><br/>
                                    <b id="tanggal_penjualan_menunggu_mrc" class="text-dark"></b>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Akhir baris 1 | Penjualan dan Retur Penjulaan Umum -->

            <!-- Awal baris 2 | Penjualan dan Retur Penjulaan MRC -->
            <div id="baris_2" class="row" hidden>
                <!-- <div class="col-lg-3 col-md-3 col-sm-6">
                    <div class="card-box widget-box-one">
                        <i class="ti ti-write widget-one-icon"></i>
                        <div class="wigdet-one-content">
                            <p class="m-0 text-uppercase font-600 font-secondary text-warning" title="Statistics">Penjualan Disiapkan - MRC</p>
                            <h2 class="text-warning">Rp.
                                <span id="total_penjualan_disiapkan_mrc"></span> 
                            </h2>
                            <div class="row">
                                <div class="col-md-5">
                                    <small class="text-warning"><b>Jml Transaksi</b></small><br/>
                                    <b id="jumlah_penjualan_disiapkan_mrc" class="text-dark"></b>
                                </div>
                                <div class="col-md-7">
                                    <small class="text-warning"><b>Periode</b></small><br/>
                                    <b id="tanggal_penjualan_disiapkan_mrc" class="text-dark"></b>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> -->

                <div class="col-lg-3 col-md-3 col-sm-6">
                    <div class="card-box widget-box-one">
                        <i class="ti ti-truck widget-one-icon"></i>
                        <div class="wigdet-one-content">
                            <p class="m-0 text-uppercase font-600 font-secondary text-primary" title="Statistics">Penjualan Dikirim - MRC</p>
                            <h2>Rp.
                                <span id="total_penjualan_dikirim_mrc"></span> 
                            </h2>
                            <div class="row">
                                <div class="col-md-5">
                                    <small class="text-primary"><b>Jml Transaksi</b></small><br/>
                                    <b id="jumlah_penjualan_dikirim_mrc" class="text-dark"></b>
                                </div>
                                <div class="col-md-7">
                                    <small class="text-primary"><b>Periode</b></small><br/>
                                    <b id="tanggal_penjualan_dikirim_mrc" class="text-dark"></b>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-3 col-md-3 col-sm-6">
                    <div class="card-box widget-box-one">
                        <i class="ti-shopping-cart widget-one-icon"></i>
                        <div class="wigdet-one-content">
                            <p class="m-0 text-uppercase font-600 font-secondary text-success" title="Statistics">Penjualan Selesai - MRC</p>
                            <h2>Rp.
                                <span id="total_penjualan_selesai_mrc"></span> 
                            </h2>
                            <div class="row">
                                <div class="col-md-5">
                                    <small class="text-success"><b>Jml Transaksi</b></small><br/>
                                    <b id="jumlah_penjualan_selesai_mrc" class="text-dark"></b>
                                </div>
                                <div class="col-md-7">
                                    <small class="text-success"><b>Periode</b></small><br/>
                                    <b id="tanggal_penjualan_selesai_mrc" class="text-dark"></b>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-3 col-md-3 col-sm-6">
                    <div class="card-box widget-box-one">
                        <i class="ti ti-shopping-cart-full widget-one-icon"></i>
                        <div class="wigdet-one-content">
                            <p class="m-0 text-uppercase font-600 font-secondary text-success" title="Statistics">Retur Penjualan - MRC</p>
                            <h2>Rp. 
                                <span id="total_retur_penjualan_mrc"></span> 
                            </h2>
                            <div class="row">
                                <div class="col-md-5">
                                    <small class="text-success"><b>Jml Transaksi</b></small><br/>
                                    <b id="jumlah_retur_penjualan_mrc" class="text-dark"></b>
                                </div>
                                <div class="col-md-7">
                                    <small class="text-success"><b>Periode</b></small><br/>
                                    <b id="tanggal_retur_penjualan_mrc" class="text-dark"></b>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-3 col-md-3 col-sm-6">
                    <div class="card-box widget-box-one">
                        <i class="ti ti-pencil-alt widget-one-icon"></i>
                        <div class="wigdet-one-content">
                            <p class="m-0 text-uppercase font-600 font-secondary text-primary" title="Statistics">Permintaan Barang</p>
                            <h2>Rp. 
                                <span id="total_permintaan_barang"></span> 
                            </h2>
                            <div class="row">
                                <div class="col-md-5">
                                    <small class="text-primary"><b>Jml Transaksi</b></small><br/>
                                    <b id="jumlah_permintaan_barang" class="text-dark"></b>
                                </div>
                                <div class="col-md-7">
                                    <small class="text-primary"><b>Periode</b></small><br/>
                                    <b id="tanggal_permintaan_barang" class="text-dark"></b>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Akhir baris 2 | Penjualan dan Retur Penjulaan MRC -->

            <!-- Awal baris 3 -->
            <div id="baris_3" class="row" hidden>
                <div class="col-lg-3 col-md-3 col-sm-6">
                    <div class="card-box widget-box-one">
                        <i class="ti ti-package widget-one-icon"></i>
                        <div class="wigdet-one-content">
                            <p class="m-0 text-uppercase font-600 font-secondary text-muted" title="Statistics">Pembelian Menunggu</p>
                            <h2 class="text-danger">Rp.
                                <span id="total_pembelian_menunggu"></span> 
                            </h2>
                            <div class="row">
                                <div class="col-md-5">
                                    <small class="text-danger"><b>Jml Transaksi</b></small><br/>
                                    <b id="jumlah_pembelian_menunggu" class="text-dark"></b>
                                </div>
                                <div class="col-md-7">
                                    <small class="text-danger"><b>Periode</b></small><br/>
                                    <b id="tanggal_pembelian_menunggu" class="text-dark"></b>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-3 col-md-3 col-sm-6">
                    <div class="card-box widget-box-one">
                        <i class="mdi mdi-database-plus widget-one-icon"></i>
                        <div class="wigdet-one-content">
                            <p class="m-0 text-uppercase font-600 font-secondary text-primary" title="Statistics">Pembelian Selesai</p>
                            <h2>Rp.
                                <span id="total_pembelian_selesai"></span> 
                            </h2>
                            <div class="row">
                                <div class="col-md-5">
                                    <small class="text-primary"><b>Jml Transaksi</b></small><br/>
                                    <b id="jumlah_pembelian_selesai" class="text-dark"></b>
                                </div>
                                <div class="col-md-7">
                                    <small class="text-primary"><b>Periode</b></small><br/>
                                    <b id="tanggal_pembelian_selesai" class="text-dark"></b>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-3 col-md-3 col-sm-6">
                    <div class="card-box widget-box-one">
                        <i class="mdi mdi-database-minus widget-one-icon"></i>
                        <div class="wigdet-one-content">
                            <p class="m-0 text-uppercase font-600 font-secondary text-primary" title="Statistics">Retur Pembelian</p>
                            <h2>Rp. 
                                <span id="total_retur_pembelian"></span> 
                            </h2>
                            <div class="row">
                                <div class="col-md-5">
                                    <small class="text-primary"><b>Jml Transaksi</b></small><br/>
                                    <b id="jumlah_retur_pembelian" class="text-dark"></b>
                                </div>
                                <div class="col-md-7">
                                    <small class="text-primary"><b>Periode</b></small><br/>
                                    <b id="tanggal_retur_pembelian" class="text-dark"></b>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-3 col-md-3 col-sm-6">
                    <div class="card-box widget-box-one">
                        <i class="mdi mdi-counter widget-one-icon"></i>
                        <div class="wigdet-one-content">
                            <p class="m-0 text-uppercase font-600 font-secondary text-info" title="Statistics">STOK OPNAME</p>
                            <h2>
                                <span id="total_stok_opname"></span>
                            </h2>
                            <div class="row">
                                <div class="col-md-5">
                                    <small class="text-info"><b>Qty</b></small><br/>
                                    <b id="total_qty_stok_opname" class="text-dark"></b>
                                </div>
                                <div class="col-md-7">
                                    <small class="text-info"><b>Periode</b></small><br/>
                                    <b id="tanggal_stok_opname" class="text-dark"></b>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Akhir baris 3 -->

            <!-- Awal baris 4 -->
            <div id="baris_4"  class="row" hidden>
                <div class="col-lg-3 col-md-3 col-sm-6">
                    <div class="card-box widget-box-one">
                        <i class="fa fa-cubes widget-one-icon"></i>
                        <div class="wigdet-one-content">
                            <p class="m-0 text-uppercase font-600 font-secondary text-purple" title="Statistics">Perakitan Selesai</p>
                            <h2>
                                <span id="total_perakitan_selesai"></span> 
                            </h2>
                            <div class="row">
                                <div class="col-md-5">
                                    <small class="text-purple"><b>Komponen</b></small><br/>
                                    <b id="total_komponen_selesai" class="text-dark"></b>
                                </div>
                                <div class="col-md-7">
                                    <small class="text-purple"><b>Periode</b></small><br/>
                                    <b id="tanggal_perakitan_selesai" class="text-dark"></b>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-3 col-md-3 col-sm-6">
                    <div class="card-box widget-box-one">
                        <i class="ti ti-layout-grid2-alt widget-one-icon"></i>
                        <div class="wigdet-one-content">
                            <p class="m-0 text-uppercase font-600 font-secondary text-muted" title="Statistics">Perakitan Menunggu</p>
                            <h2 class="text-danger">
                                <span id="total_perakitan_menunggu"></span> 
                            </h2>
                            <div class="row">
                                <div class="col-md-5">
                                    <small class="text-danger"><b>Komponen</b></small><br/>
                                    <b id="total_komponen_menunggu" class="text-dark"></b>
                                </div>
                                <div class="col-md-7">
                                    <small class="text-danger"><b>Periode</b></small><br/>
                                    <b id="tanggal_perakitan_menunggu" class="text-dark"></b>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="col-lg-3 col-md-3 col-sm-6">
                    <div class="card-box widget-box-one">
                        <i class="mdi mdi-gift widget-one-icon"></i>
                        <div class="wigdet-one-content">
                            <p class="m-0 text-uppercase font-600 font-secondary text-warning" title="Statistics">HADIAH SELESAI</p>
                            <h2>Rp. 
                                <span id="total_hadiah_selesai"></span> 
                            </h2>
                            <div class="row">
                                <div class="col-md-5">
                                    <small class="text-warning"><b>Jml Transaksi</b></small><br/>
                                    <b id="jumlah_hadiah_selesai" class="text-dark"></b>
                                </div>
                                <div class="col-md-7">
                                    <small class="text-warning"><b>Periode</b></small><br/>
                                    <b id="tanggal_hadiah_selesai" class="text-dark"></b>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-3 col-md-3 col-sm-6">
                    <div class="card-box widget-box-one">
                        <i class="mdi mdi-gift widget-one-icon"></i>
                        <div class="wigdet-one-content">
                            <p class="m-0 text-uppercase font-600 font-secondary text-muted" title="Statistics">HADIAH MENUNGGU</p>
                            <h2 class="text-danger">Rp. 
                                <span id="total_hadiah_menunggu"></span>
                            </h2>
                            <div class="row">
                                <div class="col-md-5">
                                    <small class="text-danger"><b>Jml Transaksi</b></small><br/>
                                    <b id="jumlah_hadiah_menunggu" class="text-dark"></b>
                                </div>
                                <div class="col-md-7">
                                    <small class="text-danger"><b>Periode</b></small><br/>
                                    <b id="tanggal_hadiah_menunggu" class="text-dark"></b>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Akhir baris 4 -->
            <!-- Akhir total -->
        <?php } ?>

		<div class="row">
            <!-- Awal daftar penjualan menunggu - umum -->
            <div class="col-lg-6">
            	<div class="card-box">
					<h4 class="m-t-0 m-b-20 header-title text-danger">
                        <i class="ti ti-agenda"></i>
                        <b>PENJUALAN MENUNGGU - UMUM</b>
                    </h4>

                    <div class="inbox-widget slimscroll-alt mx-box">
    					<div id="daftar_penjualan_menunggu_umum">
                        </div>
                    </div>
				</div>
            </div>
            <!-- Akhir daftar penjualan menunggu - umum -->

            <!-- Awal daftar penjualan disiapkan - umum -->
            <div class="col-lg-6">
                <div class="card-box">
                    <h4 class="m-t-0 m-b-20 header-title text-warning">
                        <i class="ti ti-write"></i>
                        <b>PENJUALAN DISIAPKAN - UMUM</b>
                    </h4>

                    <div class="inbox-widget slimscroll-alt mx-box">
                        <div id="daftar_penjualan_disiapkan_umum">
                        </div>
                    </div>
                </div>
            </div>
            <!-- Akhir daftar penjualan disiapkan - umum -->

            <!-- Awal daftar penjualan menunggu - mrc -->
            <div class="col-lg-6">
                <div class="card-box">
                    <h4 class="m-t-0 m-b-20 header-title text-danger">
                        <i class="ti ti-agenda"></i>
                        <b>PENJUALAN MENUNGGU - MRC</b>
                    </h4>

                    <div class="inbox-widget slimscroll-alt mx-box">
                        <div id="daftar_penjualan_menunggu_mrc">
                        </div>
                    </div>
                </div>
            </div>
            <!-- Akhir daftar penjualan menunggu - mrc -->

            <!-- Awal daftar penjualan disiapkan - mrc -->
            <div class="col-lg-6">
                <div class="card-box">
                    <h4 class="m-t-0 m-b-20 header-title text-warning">
                        <i class="ti ti-write"></i>
                        <b>PENJUALAN DISIAPKAN - MRC</b>
                    </h4>

                    <div class="inbox-widget slimscroll-alt mx-box">
                        <div id="daftar_penjualan_disiapkan_mrc">
                        </div>
                    </div>
                </div>
            </div>
            <!-- Akhir daftar penjualan disiapkan - mrc -->

            <!-- Awal daftar penjualan dikirim - mrc -->
            <div class="col-lg-6">
                <div class="card-box">
                    <h4 class="m-t-0 m-b-20 header-title text-primary">
                        <i class="ti ti-truck"></i>
                        <b>PENJUALAN DIKIRIM - MRC</b>
                    </h4>

                    <div class="inbox-widget slimscroll-alt mx-box">
                        <div id="daftar_penjualan_dikirim_mrc">
                        </div>
                    </div>
                </div>
            </div>
            <!-- Akhir daftar penjualan dikirim - mrc -->

            <!-- Awal daftar penjualan dikirim - mrc -->
            <div class="col-lg-6">
                <div class="card-box">
                    <h4 class="m-t-0 m-b-20 header-title text-primary">
                        <i class="ti ti-truck"></i>
                        <b>RETUR DARI TOKO - MRC</b>
                    </h4>

                    <div class="inbox-widget slimscroll-alt mx-box">
                        <div id="daftar_retur_barang_dari_toko">
                        </div>
                    </div>
                </div>
            </div>
            <!-- Akhir daftar penjualan dikirim - mrc -->

            <?php if($this->session->userdata('usergroup_name') == 'Super Admin' or 
                 $this->session->userdata('usergroup_name') == 'Admin'){ ?>
                <!-- Awal daftar top 10 customer -->
                <div class="col-lg-6">
                    <div class="card-box">
                        <h4 class="m-t-0 m-b-20 header-title text-success">
                            <i class="mdi mdi-account-location"></i>
                            <b>TOP 10 CUSTOMER BULAN INI</b>
                        </h4>

                        <div class="inbox-widget slimscroll-alt mx-box">
                            <div id="daftar_top_customer">
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Akhir daftar top 10 customer -->

                <!-- Awal daftar top 10 produk -->
                <div class="col-lg-6">
                    <div class="card-box">
                        <h4 class="m-t-0 m-b-20 header-title text-primary">
                            <i class="mdi mdi-cube-outline"></i>
                            <b>TOP 10 PRODUK BULAN INI</b>
                        </h4>

                        <div class="inbox-widget slimscroll-alt mx-box">
                            <div id="daftar_top_produk">
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Akhir daftar top 10 produk -->
            <?php } ?>
		</div>
		<!-- end row -->
	</div> <!-- container -->
</div> 

<div class="modal" id="ModalGue" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class='fa fa-times-circle'></i></button>
				<h4 class="modal-title" id="ModalHeader"></h4>
			</div>
			<div class="modal-body" id="ModalContent"></div>
			<div class="modal-footer" id="ModalFooter"></div>
		</div>
	</div>
</div>

<script>
$('#ModalGue').on('hide.bs.modal', function () {
   setTimeout(function(){ 
        $('#ModalHeader, #ModalContent, #ModalFooter').html('');
   }, 500);
});
</script>
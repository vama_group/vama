<div class="content">
    <div class="container">
    <br>        
        <div class="row">
            <div class="col-sm-12">
                <div class="row">
                    <div class="col-sm-12">
                        <!-- Awal daftar transaksi dan catatan transaksi --> 
                        <div class="card-box table-responsive">
                            <!-- Keterangan transaksi dan tombol tambah dan refersh -->
                            <div class="row">
                                <!-- Keterangan, tombol tambah dan refresh -->
                                <div class='col-sm-9'>
                                    <h4 class="text-success"><i class='fa fa-calculator'></i> PENJUALAN PUSAT</h4>
                                    <button class="btn btn-sm btn-primary" id="refresh_data">
                                        <i class="glyphicon glyphicon-refresh"></i> 
                                        TAMPILKAN
                                    </button>
                                </div>

                                <!-- Pemilihan tanggal awal dan akhir -->
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <!-- <div id="reportrange" class="pull-right form-control">
                                            <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>
                                            <span id="tanggal_filter" name="tanggal_filter"></span>
                                        </div> -->

                                        <!-- <div class="form-horizontal">
                                            <label class="control-label col-md-3">Tanggal Awal & Akhir</label>
                                            <div class="col-md-9"> -->
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <small>Tanggal awal</small>
                                                        <input id="tanggal_awal" type="text" class="form-control" data-date-format="yyyy-mm-dd" name="tanggal_awal" value="<?php echo $tanggal_awal ?>"/>
                                                        <span class="help-block"></span>
                                                    </div>

                                                    <!-- <div class="col-md-2">
                                                        <small></small>
                                                        <span class="btn btn-primary btn-sm">></span>
                                                    </div> -->

                                                    <div class="col-md-6">
                                                        <small>Tanggal akhir</small>
                                                        <input id="tanggal_akhir" type="text" class="form-control" data-date-format="yyyy-mm-dd" name="tanggal_akhir" value="<?php echo $tanggal_akhir ?>"/>
                                                        <span class="help-block"></span>
                                                    </div>                            
                                                </div>
                                            <!-- </div> 
                                        </div> -->
                                    </div>
                                </div>
                            </div>
                            <hr class="m-b-0 m-t-0">
                            
                            <?php if($this->session->userdata('usergroup_name') == 'Admin' or 
                                     $this->session->userdata('usergroup_name') == 'Super Admin'){ ?>
                                <!-- Awal daftar total -->
                                <div class="row">
                                    <div class="col-md-3">
                                        <table class='table tablesaw m-t-20 table m-b-0 tablesaw-stack card-box' id='Total1'>
                                            <thead>
                                            </thead>

                                            <tbody>
                                                <tr>
                                                    <td><small>Penjualan - Pusat</br></small></td>
                                                    <td class="text-left">Rp.
                                                        <span id='TotalBersih' name='TotalBersih' class="text-success pull-right">0</span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td><small>Penjualan - Toko</br></small></td>
                                                    <td class="text-left">Rp.
                                                        <span id='TotalBersihToko' name='TotalBersihToko' class="text-success pull-right">0</span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td><small>Penjualan - Online</br></small></td>
                                                    <td class="text-left">Rp.
                                                        <span id='TotalBersihOnline' name='TotalBersihOnline' class="text-success pull-right">0</span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td><small>Grand Total Penjualan</br></small></td>
                                                    <td class="text-left">Rp.
                                                        <span id='GrandTotalBersih' name='GrandTotalBersih' class="text-success pull-right">0</span>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>

                                    <?php if($this->session->userdata('usergroup_name') == 'Super Admin'){ ?>
                                    <div id="KotakModal" class="col-md-3" hidden>
                                        <table class='table tablesaw m-t-20 table m-b-0 tablesaw-stack card-box' id='Total2'>
                                            <thead>
                                            </thead>

                                            <tbody>
                                                <tr>
                                                    <td><small>Modal - Pusat</small></td>
                                                    <td class="text-left">Rp.
                                                        <span id='TotalModal' name='TotalModal' class="text-dark pull-right">0</span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td><small>Modal - Toko</small></td>
                                                    <td class="text-left">Rp.
                                                        <span id='TotalModalToko' name='TotalModalToko' class="text-dark pull-right">0</span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td><small>Modal - Online</small></td>
                                                    <td class="text-left">Rp.
                                                        <span id='TotalModalOnline' name='TotalModalOnline' class="text-dark pull-right">0</span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td><small>Grand Total Modal</small></td>
                                                    <td class="text-left">Rp.
                                                        <span id='GrandTotalModal' name='GrandTotalModal' class="text-dark pull-right">0</span>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>

                                    <div id="KotakKeuntungan" class="col-md-3" hidden>
                                        <table class='table tablesaw m-t-20 table m-b-0 tablesaw-stack card-box' id='Total3'>
                                            <thead>
                                            </thead>

                                            <tbody>
                                                <tr>
                                                    <td><small>Keuntungan - Pusat</small></td>
                                                    <td class="text-left">Rp.
                                                        <span id='TotalKeuntungan' name='TotalKeuntungan' class="text-success pull-right">0</span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td><small>Keuntungan - Toko</small></td>
                                                    <td class="text-left">Rp.
                                                        <span id='TotalKeuntunganToko' name='TotalKeuntunganToko' class="text-success pull-right">0</span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td><small>Keuntungan - Online</small></td>
                                                    <td class="text-left">Rp.
                                                        <span id='TotalKeuntunganOnline' name='TotalKeuntunganOnline' class="text-success pull-right">0</span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td><small>Grand Total Keuntungan</small></td>
                                                    <td class="text-left">Rp.
                                                        <span id='GrandTotalKeuntungan' name='GrandTotalKeuntungan' class="text-success pull-right">0</span>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>

                                    <div id="KotakAsset" class="col-md-3" hidden>
                                        <table class='table tablesaw m-t-20 table m-b-0 tablesaw-stack card-box' id='Total4'>
                                            <thead>
                                            </thead>

                                            <tbody>
                                                <tr>
                                                    <td><small>Asset - Pusat</small></td>
                                                    <td class="text-left">Rp.
                                                        <span id='TotalAsset' name='TotalKeuntungan' class="text-success pull-right">0</span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td><small>Asset - Toko</small></td>
                                                    <td class="text-left">Rp.
                                                        <span id='TotalAssetToko' name='TotalKeuntunganToko' class="text-success pull-right">0</span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td><small>Grand Total Asset</small></td>
                                                    <td class="text-left">Rp.
                                                        <span id='GrandTotalAsset' name='GrandTotalAsset' class="text-success pull-right">0</span>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <?php } ?>
                                </div>
                                <!-- Akhir daftar total -->
                            <?php } ?>
                            <!-- <hr> -->

                            <!-- Awal isi tab pusat -->
                            <div class="tab-content">
                                <!-- Awal laporan umum -->
                                <div class="tab-pane active" id="tab_penjualan_umum">
                                    <!-- Awal induk tab - pusat -->
                                    <ul class="nav nav-tabs tabs-bordered" id="tab_list_penjualan">
                                        <li class="active">
                                            <a href="#tab_induk_penjualan_perhari" id="tab_perhari_pusat" data-toggle="tab" aria-expanded="true">
                                                <span class="visible-xs"><i class="fa fa-check-square"></i></span>
                                                <span class="hidden-xs" id="judul_tab_perhari">Penjualan Perhari - PUSAT</span>
                                            </a>
                                        </li>
                                        <li class="">
                                            <a href="#tab_top_customer_pusat" id="tab_customer_pusat" data-toggle="tab" aria-expanded="true">
                                                <span class="visible-xs"><i class="typcn typcn-home"></i></span>
                                                <span class="hidden-xs" id="judul_tab_customer_pusat">Top Customer - PUSAT</span>
                                            </a>
                                        </li>
                                        <li class="">
                                            <a href="#tab_top_produk_pusat" id="tab_produk_pusat" data-toggle="tab" aria-expanded="true">
                                                <span class="visible-xs"><i class="typcn typcn-home"></i></span>
                                                <span class="hidden-xs" id="judul_tab_produk_pusat">Top Produk - PUSAT</span>
                                            </a>
                                        </li>
                                        <li class="">
                                            <a href="#tab_top_produk_percustomer" id="tab_produk_percustomer" data-toggle="tab" aria-expanded="true">
                                                <span class="visible-xs"><i class="typcn typcn-home"></i></span>
                                                <span class="hidden-xs" id="judul_tab_produk_pusat">Top Produk - Per Customer</span>
                                            </a>
                                        </li>
                                    </ul>
                                    <!-- Akhir induk tab - pusat -->

                                    <!-- Awal isi tab pusat -->
                                    <div class="tab-content">
                                        <!-- Laporan penjualan pusat - perhari -->
                                        <div class="tab-pane active" id="tab_induk_penjualan_perhari"> 
                                            <table id='TableLaporanPenjualanPusatPerhari' class="table table-condensed table-striped table-hover dt-responsive nowrap" cellspacing="0" width="100%">
                                                <thead class="input-sm">
                                                    <tr id="header_perhari" class="text-dark">
                                                        <th></th>
                                                    </tr>
                                                </thead>

                                                <tbody class="input-sm"></tbody>
                                            </table>
                                        </div>

                                        <!-- Laporan top customer - penjualan pusat -->
                                        <div class="tab-pane" id="tab_top_customer_pusat">
                                            <table id='TableLaporanTopCustomerPusat' class="table table-condensed table-striped table-hover nowrap" cellspacing="0" width="100%">
                                                <thead class="input-sm">
                                                    <tr class="text-dark">
                                                        <th>#</th>
                                                        <th>Kode Customer</th>
                                                        <th>Nama Customer</th>
                                                        <th>Periode Penjualan</th>
                                                        <th>Jml Transaksi</th>
                                                        <th>Total Jual</th>
                                                        <th>- Total Biaya Lain</th>
                                                        <th>- Total PPN</th>
                                                        <th>= Total Bersih</th>
                                                        <th>- Total Retur</th>
                                                        <th>= Total Fix</th>
                                                    </tr>
                                                </thead>

                                                <tbody class="input-sm"></tbody>
                                            </table>
                                        </div>

                                        <!-- Laporan top produk - penjualan pusat -->
                                        <div class="tab-pane" id="tab_top_produk_pusat">
                                            <table id='TableLaporanTopProdukPusat' class="table table-condensed table-striped table-hover dt-responsive nowrap nowrap" cellspacing="0" width="100%">
                                                <thead class="input-sm">
                                                    <tr class="text-dark">
                                                        <th>#</th>
                                                        <th>SKU</th>
                                                        <th>Nama Barang</th>
                                                        <th>Harga</th>
                                                        <th>Periode Penjualan</th>
                                                        <th>Transaksi</th>
                                                        <th>QTY</th>
                                                        <th>TTL Jual</th>
                                                        <th>TTL DISC</th>
                                                        <th>TTL Bersih</th>
                                                    </tr>
                                                </thead>

                                                <tbody class="input-sm"></tbody>
                                            </table>
                                        </div>

                                        <!-- Laporan top produk - penjualan per customer -->
                                        <div class="tab-pane" id="tab_top_produk_percustomer">
                                            <table id='TableLaporanTopProdukPercustomer' class="table table-condensed table-striped table-hover dt-responsive nowrap nowrap" cellspacing="0" width="100%">
                                                <thead class="input-sm">
                                                    <tr class="text-dark">
                                                        <th>#</th>
                                                        <th>Customer</th>
                                                        <th>SKU</th>
                                                        <th>Nama Barang</th>
                                                        <th>Harga</th>
                                                        <th>Periode Penjualan</th>
                                                        <th>Transaksi</th>
                                                        <th>QTY</th>
                                                        <th>TTL Jual</th>
                                                        <th>TTL DISC</th>
                                                        <th>TTL Bersih</th>
                                                    </tr>
                                                </thead>

                                                <tbody class="input-sm"></tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <!-- Akhir isi tab pusat -->
                                </div>
                                <!-- Akhir laporan pusat -->
                            </div>
                        </div>
                        <!-- Akhir daftar transaksi -->
                    </div>
                </div>
            </div>

            <div class="col-sm-12">
                <div class="row">
                    <div class="col-sm-12">
                        <!-- Awal daftar transaksi dan catatan transaksi --> 
                        <div class="card-box table-responsive">
                            <div class="row">
                                <div class='col-sm-12'>
                                    <h4 class="text-success"><i class='fa fa-home'></i> PENJUALAN TOKO</h4>
                                    <select id="kode_toko" name="kode_toko" class="btn btn-rounded btn-xs btn-default">
                                        <option value="" class="text-dark ">PILIH TOKO</option>
                                        <?php foreach ($data_toko AS $dt){ ?>
                                            <option value="<?php echo $dt->kode_customer_pusat ?>" class="text-dark">
                                                <?php echo $dt->nama_customer_pusat ?>
                                            </option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                            
                            <?php if($this->session->userdata('usergroup_name') == 'Admin' or 
                                     $this->session->userdata('usergroup_name') == 'Super Admin'){ ?>
                                <!-- Awal daftar total -->
                                <div class="row">
                                    <div class="col-md-3">
                                        <table class='table tablesaw m-t-20 table m-b-0 tablesaw-stack card-box' id='Total1'>
                                            <thead>
                                            </thead>

                                            <tbody>
                                                <tr>
                                                    <td><small>Total Penjualan</br></small></td>
                                                    <td class="text-left">Rp.
                                                        <span id='TotalBersihPerToko' name='TotalBersihPerToko' class="text-success pull-right">0</span>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>

                                    <?php if($this->session->userdata('usergroup_name') == 'Super Admin'){ ?>
                                    <div id="KotakModalPerToko" class="col-md-3" hidden>
                                        <table class='table tablesaw m-t-20 table m-b-0 tablesaw-stack card-box' id='Total2'>
                                            <thead>
                                            </thead>

                                            <tbody>
                                                <tr>
                                                    <td><small>Total Modal - Toko</small></td>
                                                    <td class="text-left">Rp.
                                                        <span id='TotalModalPerToko' name='TotalModalPerToko' class="text-dark pull-right">0</span>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>

                                    <div id="KotakKeuntunganPerToko" class="col-md-3" hidden>
                                        <table class='table tablesaw m-t-20 table m-b-0 tablesaw-stack card-box' id='Total3'>
                                            <thead>
                                            </thead>

                                            <tbody>
                                                <tr>
                                                    <td><small>Total Keuntungan - Toko</small></td>
                                                    <td class="text-left">Rp.
                                                        <span id='TotalKeuntunganPerToko' name='TotalKeuntunganPerToko' class="text-success pull-right">0</span>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>

                                    <div id="KotakAssetPerToko" class="col-md-3" hidden>
                                        <table class='table tablesaw m-t-20 table m-b-0 tablesaw-stack card-box' id='Total4'>
                                            <thead>
                                            </thead>

                                            <tbody>
                                                <tr>
                                                    <td><small>Total Asset - Toko</small></td>
                                                    <td class="text-left">Rp.
                                                        <span id='TotalAssetPerToko' name='TotalKeuntunganPerToko' class="text-success pull-right">0</span>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>                                      
                                    <?php } ?>
                                </div>
                                <!-- Akhir daftar total -->
                            <?php } ?>
                            <!-- <hr> -->

                            <!-- Awal isi tab toko -->
                            <div class="tab-content">
                                <!-- Awal laporan umum -->
                                <div class="tab-pane active" id="tab_penjualan_umum">
                                    <!-- Awal induk tab - pusat -->
                                    <ul class="nav nav-tabs tabs-bordered" id="tab_list_penjualan">
                                        <li class="active">
                                            <a href="#tab_induk_penjualan_perhari_toko" id="tab_perhari_toko" data-toggle="tab" aria-expanded="true">
                                                <span class="visible-xs"><i class="fa fa-check-square"></i></span>
                                                <span class="hidden-xs" id="judul_tab_perhari_toko">Penjualan Perhari - Toko</span>
                                            </a>
                                        </li>
                                        <li class="">
                                            <a href="#tab_top_customer_toko" id="tab_customer_toko" data-toggle="tab" aria-expanded="true">
                                                <span class="visible-xs"><i class="typcn typcn-home"></i></span>
                                                <span class="hidden-xs" id="judul_tab_customer_pusat">Top Customer - Toko</span>
                                            </a>
                                        </li>
                                        <li class="">
                                            <a href="#tab_top_produk_toko" id="tab_produk_toko" data-toggle="tab" aria-expanded="true">
                                                <span class="visible-xs"><i class="typcn typcn-home"></i></span>
                                                <span class="hidden-xs" id="judul_tab_produk_pusat">Top Produk - Toko</span>
                                            </a>
                                        </li>
                                    </ul>
                                    <!-- Akhir induk tab - toko -->

                                    <!-- Awal isi tab toko -->
                                    <div class="tab-content">
                                        <!-- Laporan penjualan toko - perhari -->
                                        <div class="tab-pane active" id="tab_induk_penjualan_perhari_toko"> 
                                            <table id='TableLaporanPenjualanTokoPerhari' class="table table-condensed table-striped table-hover dt-responsive nowrap" cellspacing="0" width="100%">
                                                <thead class="input-sm">
                                                    <tr id="header_perhari_toko" class="text-dark">
                                                        <th></th>
                                                    </tr>
                                                </thead>

                                                <tbody class="input-sm"></tbody>
                                            </table>
                                        </div>

                                        <!-- Laporan top customer - penjualan toko -->
                                        <div class="tab-pane" id="tab_top_customer_toko">
                                            <table id='TableLaporanTopCustomerToko' class="table table-condensed table-striped table-hover nowrap" cellspacing="0" width="100%">
                                                <thead class="input-sm">
                                                    <tr class="text-dark">
                                                        <th>#</th>
                                                        <th>Kode Customer</th>
                                                        <th>Nama Customer</th>
                                                        <th>Periode Penjualan</th>
                                                        <th>Jml Transaksi</th>
                                                        <th>Total Jual</th>
                                                        <th>- Total Biaya Lain</th>
                                                        <th>- Total PPN</th>
                                                        <th>= Total Bersih</th>
                                                        <th>- Total Retur</th>
                                                        <th>= Total Fix</th>
                                                    </tr>
                                                </thead>

                                                <tbody class="input-sm"></tbody>
                                            </table>
                                        </div>

                                        <!-- Laporan top produk - penjualan toko -->
                                        <div class="tab-pane" id="tab_top_produk_toko">
                                            <table id='TableLaporanTopProdukToko' class="table table-condensed table-striped table-hover dt-responsive nowrap nowrap" cellspacing="0" width="100%">
                                                <thead class="input-sm">
                                                    <tr class="text-dark">
                                                        <th>#</th>
                                                        <th>SKU</th>
                                                        <th>Nama Barang</th>
                                                        <th>Harga</th>
                                                        <th>Periode Penjualan</th>
                                                        <th>Transaksi</th>
                                                        <th>QTY</th>
                                                        <th>TTL Jual</th>
                                                        <th>TTL DISC</th>
                                                        <th>TTL Bersih</th>
                                                    </tr>
                                                </thead>

                                                <tbody class="input-sm"></tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <!-- Akhir isi tab pusat -->
                                </div>
                                <!-- Akhir laporan pusat -->
                            </div>
                        </div>
                        <!-- Akhir daftar transaksi -->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal" id="ModalGue" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class='fa fa-times-circle'></i></button>
                <h4 class="modal-title" id="ModalHeader"></h4>
            </div>
            <div class="modal-body" id="ModalContent"></div>
            <div class="modal-footer" id="ModalFooter"></div>
        </div>
    </div>
</div>

<script>
$('#ModalGue').on('hide.bs.modal', function () {
   setTimeout(function(){ 
        $('#ModalHeader, #ModalContent, #ModalFooter').html('');
   }, 500);
});
</script>
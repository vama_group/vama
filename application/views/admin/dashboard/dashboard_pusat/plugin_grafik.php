<!-- Awal CSS -->
<link href="assets/plugin/zircos/material-design/assets/css/tambahan.css" rel="stylesheet" type="text/css"/>
<link href="assets/plugin/zircos/plugins/datatables/jquery.dataTables.min.css" rel="stylesheet" type="text/css"/>
<link href="assets/plugin/zircos/plugins/datatables/buttons.bootstrap.min.css" rel="stylesheet" type="text/css"/>
<link href="assets/plugin/zircos/plugins/datatables/dataTables.colVis.css" rel="stylesheet" type="text/css"/>
<link href="assets/plugin/zircos/plugins/datatables/scroller.bootstrap.min.css" rel="stylesheet" type="text/css"/>
<link href="assets/plugin/zircos/plugins/datatables/fixedColumns.dataTables.min.css" rel="stylesheet" type="text/css"/>
<link href="assets/plugin/zircos/plugins/datatables/responsive.bootstrap.min.css" rel="stylesheet" type="text/css"/>
<link href="assets/plugin/zircos/plugins/datatables/dataTables.bootstrap.min.css" rel="stylesheet" type="text/css"/>
<link href="assets/plugin/zircos/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css" rel="stylesheet">
<link href="assets/plugin/zircos/plugins/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
<!-- Akhir CSS -->

<!-- Awal JS -->
<script src="assets/plugin/zircos/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="assets/plugin/zircos/plugins/datatables/dataTables.bootstrap.js"></script>

<script src="assets/plugin/zircos/material-design/assets/js/detect.js"></script>
<script src="assets/plugin/zircos/material-design/assets/js/jquery.slimscroll.js"></script>
<script src="assets/plugin/zircos/plugins/datatables/dataTables.colVis.js"></script>
<script src="assets/plugin/zircos/plugins/datatables/dataTables.responsive.min.js"></script>
<script src="assets/plugin/zircos/plugins/datatables/responsive.bootstrap.min.js"></script>
<script src="assets/plugin/zircos/plugins/datatables/dataTables.fixedColumns.min.js"></script>
<script src="assets/plugin/zircos/plugins/datatables/dataTables.scroller.min.js"></script>
<!-- Akhir JS -->

<!-- JS High Chart -->
<script src="assets/chart/highchart/js/highcharts.js"></script>
<script src="assets/chart/highchart/js/exporting.js"></script>

<!-- Awal Sweet-Alert  -->
<link href="assets/plugin/zircos/plugins/bootstrap-sweetalert/sweet-alert.css" rel="stylesheet" type="text/css">
<script src="assets/plugin/zircos/plugins/bootstrap-sweetalert/sweet-alert.min.js"></script>
<!-- Akhir Sweet-Alert  -->

<script>
    var tanggal_laporan;

    var delay = (function () {
        var timer = 0;
        return function (callback, ms){
            clearTimeout(timer);
            timer = setTimeout(callback, ms);
        };
    })();

    $(document).ready(function(){
        $('#navbar').removeClass('navbar-default');
        $('#navbar').addClass('navbar-hijau');

        $("#wrapper").removeClass('forced');
        $("#wrapper").addClass('forced enlarged');
        $('#JudulHalaman').html('DASHBOARD GRAFIK - VAMA');

        //datepicker
        $('#tanggal_awal').datepicker({
            autoclose: true,
            format: "yyyy-mm-dd",
            todayHighlight: true
        });

        $('#tanggal_akhir').datepicker({
            autoclose: true,
            format: "yyyy-mm-dd",
            todayHighlight: true
        });

        $('#tanggal_awal_toko_perbulan').datepicker({
            autoclose: true,
            format: "yyyy-mm-dd",
            todayHighlight: true
        });

        $('#tanggal_akhir_toko_perbulan').datepicker({
            autoclose: true,
            format: "yyyy-mm-dd",
            todayHighlight: true
        });

        $('#tanggal_awal_pusat_perbulan').datepicker({
            autoclose: true,
            format: "yyyy-mm-dd",
            todayHighlight: true
        });

        $('#tanggal_akhir_pusat_perbulan').datepicker({
            autoclose: true,
            format: "yyyy-mm-dd",
            todayHighlight: true
        });
    });

    $(document).on('click', '#refresh_data_toko_perbulan', function(){
        chart_penjualan_toko_perbulan();
    });

    function chart_penjualan_toko_perbulan(){
        // real_time();
        var tanggal_awal  = $('#tanggal_awal_toko_perbulan').val()
        var tanggal_akhir = $('#tanggal_akhir_toko_perbulan').val();

        // // Ambil jumlah transaksi dan grand total
        $.ajax({
            url: "<?php echo site_url('dashboard_grafik/chart_penjualan_toko_perbulan'); ?>",
            type: "POST",
            cache: false,
            data: 'tanggal_awal=' + tanggal_awal +
                  '&tanggal_akhir=' + tanggal_akhir,
            dataType:'json',
            success: function(hasil){
                var nama_bulan   = [];
                var total_mrc001 = [];
                var total_mrc002 = [];
                var total_mrc003 = [];
                var total_mrc004 = [];
                var total_mrc005 = [];
                var total_online = [];

                // Nama toko
                for (a in hasil.data_toko){
                    nama_toko.push(hasil.data_toko[a].nama_toko);
                }

                // TGR
                for (b in hasil.mrc001){
                    total_mrc001.push(hasil.mrc001[b].total_fix);
                }

                // BGR
                for (c in hasil.mrc002){
                    total_mrc002.push(hasil.mrc002[c].total_fix);
                }

                // TB
                for (d in hasil.mrc003){
                    total_mrc003.push(hasil.mrc003[d].total_fix);
                }

                // CSL
                for (e in hasil.mrc004){
                    total_mrc004.push(hasil.mrc004[e].total_fix);
                    nama_bulan.push(hasil.mrc004[e].nama_bulan) + ', ';                    
                }
                
                // ONLINE
                for (f in hasil.online){
                    total_online.push(hasil.online[f].total_fix);
                }

                // BGK
                for (g in hasil.mrc005){
                    total_mrc005.push(hasil.mrc005[g].total_fix);
                }

                $(function(){
                    // Penerapan chart penjualan umum
                    $("#penjualan_toko_perbulan").highcharts({
                        chart: {
                            type: 'column'
                        },
                        title: {
                            text: 'Penjualan Toko Perbulan'
                        },
                        subtitle: {
                            text: 'Sumber : www.mrcstore.co.id'
                        },
                        xAxis: {
                            categories: nama_bulan,
                            crosshair: true
                        },
                        yAxis: {
                            min: 0,
                            title: {
                                text: 'Rupiah (Juta)'
                            }
                        },
                        tooltip: {
                            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                                '<td style="padding:0"><b>{point.y:.1f} Juta</b></td></tr>',
                            footerFormat: '</table>',
                            shared: true,
                            useHTML: true
                        },
                        plotOptions: {
                            column: {
                                pointPadding: 0.2,
                                borderWidth: 0
                            }
                        },
                        
                        series: [
                            {
                                name: 'MR CLASSIC - TGR',
                                data: total_mrc001,
                                color: 'green'
                            },
                            {
                                name: 'MR CLASSIC - BGR',
                                data: total_mrc002,
                                color: 'blue'
                            }, 
                            {
                                name: 'MR CLASSIC - TB',
                                data: total_mrc003,
                                color: 'orange'
                            },
                            {
                                name: 'MR CLASSIC - MAIN STORE CSL',
                                data: total_mrc004,
                                color: 'red'
                            },
                            {
                                name: 'ONLINE',
                                data: total_online,
                                color: '#41d9f4'
                            }, 
                            {
                                name: 'MR CLASSIC - BGK',
                                data: total_mrc005,
                                color: 'purple'
                            }
                        ]
                    });
                });
                
            }, error : function(hasil){
                swal({
                    title: "Oops !",
                    text: "Data chart penjualan gagal ditampilkan.",
                    type: "error",
                    confirmButtonClass: 'btn-danger btn-md waves-effect waves-light',
                    confirmButtonText: "Ok"
                });
            }
        });  
    }

    $(document).on('click', '#refresh_data_pusat_perbulan', function(){
        chart_penjualan_pusat_perbulan();
    });

    function chart_penjualan_pusat_perbulan(){
        // real_time();
        var tanggal_awal  = $('#tanggal_awal_pusat_perbulan').val()
        var tanggal_akhir = $('#tanggal_akhir_pusat_perbulan').val();

        // // Ambil jumlah transaksi dan grand total
        $.ajax({
            url: "<?php echo site_url('dashboard_grafik/chart_penjualan_pusat_perbulan'); ?>",
            type: "POST",
            cache: false,
            data: 'tanggal_awal=' + tanggal_awal +
                  '&tanggal_akhir=' + tanggal_akhir,
            dataType:'json',
            success: function(hasil){
                var nama_bulan   = [];
                var total_grosir = [];
                var total_mrc    = [];

                // Grosir
                for (a in hasil.penjualan_grosir){
                    total_grosir.push(hasil.penjualan_grosir[a].total_fix);
                    nama_bulan.push(hasil.penjualan_grosir[a].nama_bulan) + ', ';
                }

                // MRC
                for (b in hasil.penjualan_mrc){
                    total_mrc.push(hasil.penjualan_mrc[b].total_fix);
                }

                $(function(){
                    // Penerapan chart penjualan umum
                    $("#penjualan_pusat_perbulan").highcharts({
                        chart: {
                            type: 'column'
                        },
                        title: {
                            text: 'Penjualan Pusat Perbulan'
                        },
                        subtitle: {
                            text: 'Sumber : www.mrcstore.co.id'
                        },
                        xAxis: {
                            categories: nama_bulan,
                            crosshair: true
                        },
                        yAxis: {
                            min: 0,
                            title: {
                                text: 'Rupiah (Juta)'
                            }
                        },
                        tooltip: {
                            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                                '<td style="padding:0"><b>{point.y:.1f} Juta</b></td></tr>',
                            footerFormat: '</table>',
                            shared: true,
                            useHTML: true
                        },
                        plotOptions: {
                            column: {
                                pointPadding: 0.2,
                                borderWidth: 0
                            }
                        },
                        
                        series: [
                            {
                                name: 'GROSIR',
                                data: total_grosir,
                                color: 'blue'
                            },
                            {
                                name: 'MRC',
                                data: total_mrc,
                                color: 'orange'
                            }
                        ]
                    });
                });
                
            }, error : function(hasil){
                swal({
                    title: "Oops !",
                    text: "Data chart penjualan gagal ditampilkan.",
                    type: "error",
                    confirmButtonClass: 'btn-danger btn-md waves-effect waves-light',
                    confirmButtonText: "Ok"
                });
            }
        });  
    }

    $(document).on('click', '#refresh_data_toko_perhari', function(){
        chart_penjualan_toko_perhari();
    });

    function chart_penjualan_toko_perhari(){
        // // Ambil jumlah transaksi dan grand total
        $.ajax({
            url: "<?php echo site_url('dashboard_grafik/chart_penjualan_toko_perhari'); ?>",
            type: "POST",
            cache: false,
            dataType:'json',
            success: function(hasil){
                var tanggal      = [];
                var total_mrc001 = [];
                var total_mrc002 = [];
                var total_mrc003 = [];
                var total_mrc004 = [];
                var total_mrc005 = [];
                var total_online = [];

                // Nama toko
                for (a in hasil.data_toko){
                    nama_toko.push(hasil.data_toko[a].nama_toko);
                }

                // TGR
                for (b in hasil.mrc001){
                    total_mrc001.push(hasil.mrc001[b].total_fix);
                }

                // BGR
                for (c in hasil.mrc002){
                    total_mrc002.push(hasil.mrc002[c].total_fix);
                }

                // TB
                for (d in hasil.mrc003){
                    total_mrc003.push(hasil.mrc003[d].total_fix);
                }

                // CSL
                for (e in hasil.mrc004){
                    total_mrc004.push(hasil.mrc004[e].total_fix);
                    tanggal.push(hasil.mrc004[e].hari + ', ' + hasil.mrc004[e].tanggal) + ', ';
                }

                // ONLINE
                for (f in hasil.online){
                    total_online.push(hasil.online[f].total_fix);
                }

                // BGK
                for (g in hasil.mrc005){
                    total_mrc005.push(hasil.mrc005[g].total_fix);
                }

                $(function(){
                    // Penerapan chart penjualan umum
                    $("#penjualan_toko_perhari").highcharts({
                        chart: {
                            type: 'column'
                        },
                        title: {
                            text: 'Penjualan Toko Perhari'
                        },
                        subtitle: {
                            text: 'Sumber : www.mrcstore.co.id'
                        },
                        xAxis: {
                            categories: tanggal,
                            crosshair: true
                        },
                        yAxis: {
                            min: 0,
                            title: {
                                text: 'Rupiah (Juta)'
                            }
                        },
                        tooltip: {
                            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                                '<td style="padding:0"><b>{point.y:.1f} Juta</b></td></tr>',
                            footerFormat: '</table>',
                            shared: true,
                            useHTML: true
                        },
                        plotOptions: {
                            column: {
                                pointPadding: 0.2,
                                borderWidth: 0
                            }
                        },
                        
                        series: [
                            {
                                name: 'MR CLASSIC - TGR',
                                data: total_mrc001,
                                color: 'green'
                            },
                            {
                                name: 'MR CLASSIC - BGR',
                                data: total_mrc002,
                                color: 'blue'
                            }, 
                            {
                                name: 'MR CLASSIC - TB',
                                data: total_mrc003,
                                color: 'orange'
                            },
                            {
                                name: 'MR CLASSIC - MAIN STORE CSL',
                                data: total_mrc004,
                                color: 'red'
                            },
                            {
                                name: 'ONLINE',
                                data: total_online,
                                color: '#41d9f4'
                            },
                            {
                                name: 'MR CLASSIC - BGK',
                                data: total_mrc005,
                                color: 'purple'
                            } 
                        ]
                    });
                });
                
            }, error : function(hasil){
                swal({
                    title: "Oops !",
                    text: "Data chart penjualan gagal ditampilkan.",
                    type: "error",
                    confirmButtonClass: 'btn-danger btn-md waves-effect waves-light',
                    confirmButtonText: "Ok"
                });
            }
        });  
    }

    $(document).on('click', '#refresh_data_pusat_grosir_perhari', function(){
        chart_penjualan_pusat_perhari();
    });

    $(document).on('click', '#refresh_data_pusat_mrc_perhari', function(){
        chart_penjualan_pusat_perhari();
    });

    function chart_penjualan_pusat_perhari($jenis_perintah, $status_penjualan){
        var tanggal_awal  = $('#tanggal_awal_toko_perbulan').val()
        var tanggal_akhir = $('#tanggal_akhir_toko_perbulan').val();
        
        $.ajax({
            url: "<?php echo site_url('dashboard_grafik/chart_penjualan_pusat_perhari'); ?>",
            type: "POST",
            cache: false,
            data: 'tanggal_awal=' + tanggal_awal +
                  '&tanggal_akhir=' + tanggal_akhir,
            dataType:'json',
            success: function(hasil){
                var tanggal_umum         = [];
                var total_menunggu_umum  = [];
                var total_disiapkan_umum = [];
                var total_selesai_umum   = [];
                
                var tanggal_mrc          = [];
                var total_menunggu_mrc   = [];
                var total_disiapkan_mrc  = [];
                var total_dikirim_mrc    = [];
                var total_selesai_mrc    = [];

                // Ambil data penjualan umum
                for (a in hasil.menunggu_umum){
                    total_menunggu_umum.push(hasil.menunggu_umum[a].total);
                }

                for (b in hasil.disiapkan_umum){
                    total_disiapkan_umum.push(hasil.disiapkan_umum[b].total);
                }

                for (c in hasil.selesai_umum){                
                    tanggal_umum.push(hasil.selesai_umum[c].tanggal);
                    total_selesai_umum.push(hasil.selesai_umum[c].total) + ', ';
                }

                // Ambil data penjualan mrc
                for (a in hasil.menunggu_mrc){
                    total_menunggu_mrc.push(hasil.menunggu_mrc[a].total);
                }

                for (b in hasil.disiapkan_mrc){
                    total_disiapkan_mrc.push(hasil.disiapkan_mrc[b].total);
                }

                for (c in hasil.dikirim_mrc){
                    total_dikirim_mrc.push(hasil.dikirim_mrc[c].total);
                }

                for (d in hasil.selesai_mrc){                
                    tanggal_mrc.push(hasil.selesai_mrc[d].tanggal);
                    total_selesai_mrc.push(hasil.selesai_mrc[d].total) + ', ';
                }

                $(function(){
                    // Penerapan chart penjualan umum
                    $("#penjualan_pusat_umum_perhari").highcharts({
                        chart: {
                            type: 'column'
                        },
                        title: {
                            text: 'Penjualan Pusat Perhari - Grosir'
                        },
                        subtitle: {
                            text: 'Sumber : www.vama.com'
                        },
                        xAxis: {
                            categories: tanggal_umum,
                            crosshair: true
                        },
                        yAxis: {
                            min: 0,
                            title: {
                                text: 'Rupiah (Juta)'
                            }
                        },
                        tooltip: {
                            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                                '<td style="padding:0"><b>{point.y:.1f} Juta</b></td></tr>',
                            footerFormat: '</table>',
                            shared: true,
                            useHTML: true
                        },
                        plotOptions: {
                            column: {
                                pointPadding: 0.2,
                                borderWidth: 0
                            }
                        },
                        series: [
                            {
                                name: 'Penjualan Menunggu',
                                data: total_menunggu_umum,
                                color: 'red'
                            },
                            {
                                name: 'Penjualan Disiapkan',
                                data: total_disiapkan_umum,
                                color: 'orange'
                            }, 
                            {
                                name: 'Penjualan Selesai',
                                data: total_selesai_umum,
                                color: 'green'
                            } 
                        ]
                    });

                    // Penerapan chart penjualan mrc
                    $("#penjualan_pusat_mrc_perhari").highcharts({
                        chart: {
                            type: 'column'
                        },
                        title: {
                            text: 'Penjualan Pusat Perhari - MRC'
                        },
                        subtitle: {
                            text: 'Sumber : www.vama.com'
                        },
                        xAxis: {
                            categories: tanggal_mrc,
                            crosshair: true
                        },
                        yAxis: {
                            min: 0,
                            title: {
                                text: 'Rupiah (Juta)'
                            }
                        },
                        tooltip: {
                            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                                '<td style="padding:0"><b>{point.y:.1f} Juta</b></td></tr>',
                            footerFormat: '</table>',
                            shared: true,
                            useHTML: true
                        },
                        plotOptions: {
                            column: {
                                pointPadding: 0.2,
                                borderWidth: 0
                            }
                        },
                        series: [
                            {
                                name: 'Penjualan Menunggu',
                                data: total_menunggu_mrc,
                                color: 'red'
                            },
                            {
                                name: 'Penjualan Disiapkan',
                                data: total_disiapkan_mrc,
                                color: 'orange'
                            },
                            {
                                name: 'Penjualan Dikirim',
                                data: total_dikirim_mrc,
                                color: 'blue'
                            }, 
                            {
                                name: 'Penjualan Selesai',
                                data: total_selesai_mrc,
                                color: 'green'
                            } 
                        ]
                    });
                });
                
            }, error : function(hasil){
                swal({
                    title: "Oops !",
                    text: "Data chart penjualan gagal ditampilkan.",
                    type: "error",
                    confirmButtonClass: 'btn-danger btn-md waves-effect waves-light',
                    confirmButtonText: "Ok"
                });
            }
        });  
    }

    function to_rupiah(angka){
        var rev     = parseInt(angka, 10).toString().split('').reverse().join('');
        var rev2    = '';
        for(var i = 0; i < rev.length; i++){
            rev2  += rev[i];
            if((i + 1) % 3 === 0 && i !== (rev.length - 1)){
                rev2 += '.';
            }
        }
        return rev2.split('').reverse().join('');
    }

    function to_angka(rp)
    {
        return parseInt(rp.replace(/,.*|\D/g,''),10)
    }
</script>
<!-- Akhir Script CRUD -->

<!-- Datepicker -->
<script src="assets/plugin/zircos/plugins/moment/moment.js"></script>
<script src="assets/plugin/zircos/plugins/timepicker/bootstrap-timepicker.js"></script>
<script src="assets/plugin/zircos/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js"></script>
<script src="assets/plugin/zircos/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
<script src="assets/plugin/zircos/plugins/clockpicker/js/bootstrap-clockpicker.min.js"></script>
<script src="assets/plugin/zircos/plugins/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- Datepicker -->
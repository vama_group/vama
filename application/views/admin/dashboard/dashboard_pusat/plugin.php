<!-- Awal CSS -->
<link href="assets/plugin/zircos/material-design/assets/css/tambahan.css" rel="stylesheet" type="text/css"/>
<link href="assets/plugin/zircos/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css" rel="stylesheet">
<link href="assets/plugin/zircos/plugins/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
<!-- Akhir CSS -->

<!-- Counter js  -->
<script src="assets/plugin/zircos/plugins/counterup/jquery.counterup.min.js"></script>
<script src="assets/plugin/zircos/plugins/waypoints/jquery.waypoints.min.js"></script>

<!-- Date rangepicker -->
<script src="assets/plugin/zircos/plugins/moment/moment.js"></script>
<script src="assets/plugin/zircos/plugins/timepicker/bootstrap-timepicker.js"></script>
<script src="assets/plugin/zircos/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js"></script>
<script src="assets/plugin/zircos/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
<script src="assets/plugin/zircos/plugins/clockpicker/js/bootstrap-clockpicker.min.js"></script>
<script src="assets/plugin/zircos/plugins/bootstrap-daterangepicker/daterangepicker.js"></script>
<script src="assets/plugin/zircos/material-design/assets/pages/jquery.form-pickers.init.js"></script>

<!-- JS High Chart -->
<script src="assets/chart/highchart/js/highcharts.js"></script>
<script src="assets/chart/highchart/js/exporting.js"></script>

<!-- Awal Sweet-Alert  -->
<link href="assets/plugin/zircos/plugins/bootstrap-sweetalert/sweet-alert.css" rel="stylesheet" type="text/css">
<script src="assets/plugin/zircos/plugins/bootstrap-sweetalert/sweet-alert.min.js"></script>
<!-- Akhir Sweet-Alert  -->

<script>
    $('#JudulHalaman').html('Dashboard - VAMA');
    $('#navbar').removeClass('navbar-default');
    $('#navbar').addClass('navbar-putih');

    $("#wrapper").removeClass('forced');
    $("#wrapper").addClass('forced enlarged');

    $("#TombolMenu").removeClass('button-menu-mobile');
    $("#TombolMenu").addClass('button-menu-mobile-biru');

    $("#KotakPencarian").removeClass('app-search');
    $("#KotakPencarian").addClass('app-search-biru');

    <?php if($this->session->userdata('usergroup_name') == 'Super Admin' || $this->session->userdata('usergroup_name') == 'Admin'){ ?>
        var status_total = '0';
        var pesan_total  = 'Apakah anda yakin ingin menampilkan total keseluruhan transaksi ?';

        function TampilkanKeseluruhanTransaksi(){
            if(status_total == '0'){
                $('#baris_1').removeAttr('hidden');
                $('#baris_2').removeAttr('hidden');
                $('#baris_3').removeAttr('hidden');
                $('#baris_4').removeAttr('hidden');

                status_total = '1';
                pesan_total  = "Sembunyikan total ?";
            }else if(status_total == '1'){
                $('#baris_1').prop('hidden', true);
                $('#baris_2').prop('hidden', true);
                $('#baris_3').prop('hidden', true);
                $('#baris_4').prop('hidden', true);

                status_total = '0';
                pesan_total  = "Apakah anda yakin ingin menampilkan modal ?";
            }
        }
    <?php } ?>

    $(document).on('keydown', 'body', function(e){
        var charCode = ( e.which ) ? e.which : event.keyCode;
        if(charCode == 118) //F7
        {
            ambil_data();
        }

        <?php if($this->session->userdata('usergroup_name') == 'Super Admin' || $this->session->userdata('usergroup_name') == 'Admin'){ ?>
            if(charCode == 119) //F8
            {
                swal({
                    title: pesan_total,
                    type: "info",
                    showCancelButton: true,
                    confirmButtonClass: 'btn-primary btn-md waves-effect waves-light',
                    confirmButtonText: "Iya, saya yakin",
                    cancelButtonText: "Batal",
                },function (isConfirm){
                    if(isConfirm){
                        setTimeout(function(){
                            TampilkanKeseluruhanTransaksi();
                            ambil_data();
                        }, 100);
                    }
                });
                return false;
            }
        <?php } ?>
    });

    $(document).ready(function(){
        ambil_data();
    });

    $(document).on('click', '#segarkan_data', function(){
        // e.preventDefault();
        ambil_data();
    });

    function ambil_data(){
        <?php if($this->session->userdata('usergroup_name') == 'Super Admin' || $this->session->userdata('usergroup_name') == 'Admin'){ ?>
            if(status_total == '1'){
                ambil_total();
            }
            
            ambil_top_customer();
            ambil_top_produk();
        <?php } ?>

        // Retur barang dari toko
        ambil_retur_dari_toko();

        // PENJULAN UMUM
        ambil_penjualan('!=', 'MENUNGGU');
        ambil_penjualan('!=', 'DISIAPKAN');

        // PENJULAN MRC
        ambil_penjualan('=', 'MENUNGGU');
        ambil_penjualan('=', 'DISIAPKAN');
        ambil_penjualan('=', 'DIKIRIM');
    }

    function ambil_total(){
        real_time();
        var tanggal_awal  = $('#tanggal_awal').html()
        var tanggal_akhir = $('#tanggal_sekarang').html();

        // Ambil jumlah transaksi dan grand total
        $.ajax({
            url: "<?php echo site_url('dashboard/ambil_total'); ?>",
            type: "POST",
            cache: false,
            data: 'tanggal_awal=' + tanggal_awal +
                  '&tanggal_akhir=' + tanggal_akhir,
            dataType:'json',
            success: function(hasil){
                // Baris 1 | Penjualan dan Retur Penjualan UMUM
                $('#total_penjualan_selesai_umum').html(to_rupiah(hasil.total_penjualan_selesai_umum));
                $('#jumlah_penjualan_selesai_umum').html(hasil.jumlah_penjualan_selesai_umum);
                $('#tanggal_penjualan_selesai_umum').html('(' + hasil.tl_penjualan_selesai_umum + ' > ' + hasil.tr_penjualan_selesai_umum + ')');

                $('#total_penjualan_menunggu_umum').html(to_rupiah(hasil.total_penjualan_menunggu_umum));
                $('#jumlah_penjualan_menunggu_umum').html(hasil.jumlah_penjualan_menunggu_umum);
                $('#tanggal_penjualan_menunggu_umum').html('(' + hasil.tl_penjualan_menunggu_umum + ' > ' + hasil.tr_penjualan_menunggu_umum + ')');

                // $('#total_penjualan_disiapkan_umum').html(to_rupiah(hasil.total_penjualan_disiapkan_umum));
                // $('#jumlah_penjualan_disiapkan_umum').html(hasil.jumlah_penjualan_disiapkan_umum);
                // $('#tanggal_penjualan_disiapkan_umum').html('(' + hasil.tl_penjualan_disiapkan_umum + ' > ' + hasil.tr_penjualan_disiapkan_umum + ')');

                $('#total_retur_penjualan_umum').html(to_rupiah(hasil.total_retur_penjualan_umum));
                $('#jumlah_retur_penjualan_umum').html(hasil.jumlah_retur_penjualan_umum);
                $('#tanggal_retur_penjualan_umum').html('(' + hasil.tl_retur_penjualan_umum + ' > ' + hasil.tr_retur_penjualan_umum + ')');
                
                // Baris 2 | Penjualan MRC
                $('#total_penjualan_selesai_mrc').html(to_rupiah(hasil.total_penjualan_selesai_mrc));
                $('#jumlah_penjualan_selesai_mrc').html(hasil.jumlah_penjualan_selesai_mrc);
                $('#tanggal_penjualan_selesai_mrc').html('(' + hasil.tl_penjualan_selesai_mrc + ' > ' + hasil.tr_penjualan_selesai_mrc + ')');

                $('#total_penjualan_menunggu_mrc').html(to_rupiah(hasil.total_penjualan_menunggu_mrc));
                $('#jumlah_penjualan_menunggu_mrc').html(hasil.jumlah_penjualan_menunggu_mrc);
                $('#tanggal_penjualan_menunggu_mrc').html('(' + hasil.tl_penjualan_menunggu_mrc + ' > ' + hasil.tr_penjualan_menunggu_mrc + ')');

                // $('#total_penjualan_disiapkan_mrc').html(to_rupiah(hasil.total_penjualan_disiapkan_mrc));
                // $('#jumlah_penjualan_disiapkan_mrc').html(hasil.jumlah_penjualan_disiapkan_mrc);
                // $('#tanggal_penjualan_disiapkan_mrc').html('(' + hasil.tl_penjualan_disiapkan_mrc + ' > ' + hasil.tr_penjualan_disiapkan_mrc + ')');

                $('#total_penjualan_dikirim_mrc').html(to_rupiah(hasil.total_penjualan_dikirim_mrc));
                $('#jumlah_penjualan_dikirim_mrc').html(hasil.jumlah_penjualan_dikirim_mrc);
                $('#tanggal_penjualan_dikirim_mrc').html('(' + hasil.tl_penjualan_dikirim_mrc + ' > ' + hasil.tr_penjualan_dikirim_mrc + ')');

                $('#total_retur_penjualan_mrc').html(to_rupiah(hasil.total_retur_penjualan_mrc));
                $('#jumlah_retur_penjualan_mrc').html(hasil.jumlah_retur_penjualan_mrc);
                $('#tanggal_retur_penjualan_mrc').html('(' + hasil.tl_retur_penjualan_mrc + ' > ' + hasil.tr_retur_penjualan_mrc + ')');

                $('#total_permintaan_barang').html('0');
                $('#jumlah_permintaan_barang').html('0');
                $('#tanggal_permintaan_barang').html('(-)');

                // Baris 3
                $('#total_pembelian_selesai').html(to_rupiah(hasil.total_pembelian_selesai));
                $('#jumlah_pembelian_selesai').html(hasil.jumlah_pembelian_selesai);
                $('#tanggal_pembelian_selesai').html('(' + hasil.tl_pembelian_s + ' > ' + hasil.tr_pembelian_s + ')');

                $('#total_pembelian_menunggu').html(to_rupiah(hasil.total_pembelian_menunggu));
                $('#jumlah_pembelian_menunggu').html(hasil.jumlah_pembelian_menunggu);
                $('#tanggal_pembelian_menunggu').html('(' + hasil.tl_pembelian_m + ' > ' + hasil.tr_pembelian_m + ')');

                $('#total_retur_pembelian').html(to_rupiah(hasil.total_retur_pembelian));
                $('#jumlah_retur_pembelian').html(hasil.jumlah_retur_pembelian);
                $('#tanggal_retur_pembelian').html('(' + hasil.tl_retur_pembelian + ' > ' + hasil.tr_retur_pembelian + ')');

                $('#total_stok_opname').html(to_rupiah(hasil.total_barang_opname) + ' barang');
                $('#total_qty_stok_opname').html(hasil.total_qty_opname + ' PCS');
                $('#tanggal_stok_opname').html('(' + hasil.tl_so + ' > ' + hasil.tr_so + ')');

                // Baris 4
                $('#total_perakitan_selesai').html(to_rupiah(hasil.total_barang_perakitan_selesai) + ' (Qty : ' + to_rupiah(hasil.total_qty_perakitan_selesai) + ')');
                $('#total_komponen_selesai').html(to_rupiah(hasil.total_barang_komponen_selesai) + ' (Qty : ' + to_rupiah(hasil.total_qty_komponen_selesai) + ')');
                $('#tanggal_perakitan_selesai').html('(' + hasil.tl_perakitan_s + ' > ' + hasil.tr_perakitan_s + ')');

                $('#total_perakitan_menunggu').html(to_rupiah(hasil.total_barang_perakitan_menunggu) + ' (Qty : ' + to_rupiah(hasil.total_qty_perakitan_menunggu) + ')');
                $('#total_komponen_menunggu').html(to_rupiah(hasil.total_barang_komponen_menunggu) + ' (Qty : ' + to_rupiah(hasil.total_qty_komponen_menunggu) + ')');
                $('#tanggal_perakitan_menunggu').html('(' + hasil.tl_perakitan_m + ' > ' + hasil.tr_perakitan_m + ')');

                $('#total_hadiah_selesai').html(to_rupiah(hasil.total_hadiah_selesai));
                $('#jumlah_hadiah_selesai').html(hasil.jumlah_hadiah_selesai);
                $('#tanggal_hadiah_selesai').html('(' + hasil.tl_hadiah_s + ' > ' + hasil.tr_hadiah_s + ')');

                $('#total_hadiah_menunggu').html(to_rupiah(hasil.total_hadiah_menunggu));
                $('#jumlah_hadiah_menunggu').html(hasil.jumlah_hadiah_menunggu);
                $('#tanggal_hadiah_menunggu').html('(' + hasil.tl_hadiah_m + ' > ' + hasil.tr_hadiah_m + ')');

            },
            error : function(hasil){
                // alert('error');
                // Baris 1
                $('#total_penjualan_selesai').html('0');
                $('#jumlah_penjualan_selesai').html('0 ');
                $('#tanggal_penjualan_selesai').html('(-)');

                $('#total_penjualan_menunggu').html('0');
                $('#jumlah_penjualan_menunggu').html('0');
                $('#tanggal_penjualan_menunggu').html('(-)');

                $('#total_retur_penjualan').html('0');
                $('#jumlah_retur_penjualan').html('0');
                $('#tanggal_retur_penjualan').html('(-)');

                $('#total_permintaan_barang').html('0');
                $('#jumlah_permintaan_barang').html('0');
                $('#tanggal_permintaan_barang').html('(-)');

                // Baris 2
                $('#total_pembelian_selesai').html('0');
                $('#jumlah_pembelian_selesai').html('0');
                $('#tanggal_pembelian_selesai').html('(-)');

                $('#total_pembelian_menunggu').html('0');
                $('#jumlah_pembelian_menunggu').html('0');
                $('#tanggal_pembelian_menunggu').html('(-)');

                $('#total_retur_pembelian').html('0');
                $('#jumlah_retur_pembelian').html('0');
                $('#tanggal_retur_pembelian').html('(-)');

                $('#total_stok_opname').html('0 Barang');
                $('#total_qty_stok_opname').html('0 PCS');
                $('#tanggal_stok_opname').html('(-)');

                // Baris 3
                $('#total_perakitan_selesai').html('0 (Qty : 0)');
                $('#total_komponen_selesai').html('0 (Qty : 0)');
                $('#tanggal_perakitan_selesai').html('(-)');

                $('#total_perakitan_menunggu').html('0 (Qty : 0)');
                $('#total_komponen_menunggu').html('0 (Qty : 0)');
                $('#tanggal_perakitan_menunggu').html('(-)');

                $('#total_hadiah_selesai').html('0');
                $('#jumlah_hadiah_selesai').html('0');
                $('#tanggal_hadiah_selesai').html('(-)');

                $('#total_hadiah_menunggu').html('0');
                $('#jumlah_hadiah_menunggu').html('0');
                $('#tanggal_hadiah_menunggu').html('(-)');
            }
        });     
    }

    function ambil_penjualan($jenis_perintah, $status_penjualan)
    {
        real_time();
        var tanggal_awal    = $('#tanggal_awal').html()
        var tanggal_akhir   = $('#tanggal_sekarang').html();

        // Ambil jumlah transaksi dan grand total
        $.ajax({
            url: "<?php echo site_url('dashboard/ambil_penjualan_by_perintah'); ?>",
            type: "POST",
            cache: false,
            data: 'tanggal_awal=' + tanggal_awal +
                  '&tanggal_akhir=' + tanggal_akhir +
                  '&jenis_perintah=' + $jenis_perintah +
                  '&status_penjualan=' + $status_penjualan +
                  '&<?php echo $this->security->get_csrf_token_name() ?>=<?php echo $this->security->get_csrf_hash() ?>',
            dataType:'json',
            success: function(hasil){
                if($jenis_perintah == '!='){
                    if($status_penjualan == 'MENUNGGU'){
                        $('div#daftar_penjualan_menunggu_umum').show('fast');
                        $('div#daftar_penjualan_menunggu_umum').html(hasil.datanya);
                    }else if($status_penjualan == 'DISIAPKAN'){
                        $('div#daftar_penjualan_disiapkan_umum').show('fast');
                        $('div#daftar_penjualan_disiapkan_umum').html(hasil.datanya);
                    }
                }else if($jenis_perintah == '='){
                    if($status_penjualan == 'MENUNGGU'){
                        $('div#daftar_penjualan_menunggu_mrc').show('fast');
                        $('div#daftar_penjualan_menunggu_mrc').html(hasil.datanya);
                    }else if($status_penjualan == 'DISIAPKAN'){
                        $('div#daftar_penjualan_disiapkan_mrc').show('fast');
                        $('div#daftar_penjualan_disiapkan_mrc').html(hasil.datanya);
                    }else if($status_penjualan == 'DIKIRIM'){
                        $('div#daftar_penjualan_dikirim_mrc').show('fast');
                        $('div#daftar_penjualan_dikirim_mrc').html(hasil.datanya);
                    }
                }
            },
            error : function(hasil){
                alert('Gagal menampilkan daftar penjualan');
            }
        });     
    }

    function ambil_retur_dari_toko()
    {
        real_time();
        var tanggal_awal  = $('#tanggal_awal').html()
        var tanggal_akhir = $('#tanggal_sekarang').html();

        // Ambil jumlah transaksi dan grand total
        $.ajax({
            url: "<?php echo site_url('dashboard/ambil_data_retur'); ?>",
            type: "POST",
            cache: false,
            data: 'tanggal_awal=' + tanggal_awal +
                  '&tanggal_akhir=' + tanggal_akhir,
            dataType:'json',
            success: function(json){
                $('div#daftar_retur_barang_dari_toko').show('fast');
                $('div#daftar_retur_barang_dari_toko').html(json.data_retur['datanya']);
            },
            error : function(json){
                alert('Gagal menampilkan daftar retur barang dari toko');
            }
        });     
    }

    function ambil_top_customer()
    {
        real_time();
        var tanggal_awal    = $('#tanggal_awal').html()
        var tanggal_akhir   = $('#tanggal_sekarang').html();

        // Ambil jumlah transaksi dan grand total
        $.ajax({
            url: "<?php echo site_url('dashboard/ambil_top_customer'); ?>",
            type: "POST",
            cache: false,
            data: 'tanggal_awal=' + tanggal_awal +
                  '&tanggal_akhir=' + tanggal_akhir,
            dataType:'json',
            success: function(hasil){
                $('div#daftar_top_customer').show('fast');
                $('div#daftar_top_customer').html(hasil.datanya);
            },
            error : function(hasil){
                alert('Gagal menampilkan daftar top customer');
            }
        });     
    }

    function ambil_top_produk()
    {
        real_time();
        var tanggal_awal    = $('#tanggal_awal').html()
        var tanggal_akhir   = $('#tanggal_sekarang').html();

        // Ambil jumlah transaksi dan grand total
        $.ajax({
            url: "<?php echo site_url('dashboard/ambil_top_produk'); ?>",
            type: "POST",
            cache: false,
            data: 'tanggal_awal=' + tanggal_awal +
                  '&tanggal_akhir=' + tanggal_akhir,
            dataType:'json',
            success: function(hasil){
                $('div#daftar_top_produk').show('fast');
                $('div#daftar_top_produk').html(hasil.datanya);
            },
            error : function(hasil){
                alert('Gagal menampilkan daftar top produk');
            }
        });     
    }

    function edit_retur(id_retur_pembelian_m, kode_toko)
    {
        if((id_retur_pembelian_m) !== '' || kode_toko !== ''){
            var FrmData = "&id="+id_retur_pembelian_m;
                FrmData += "&kd="+kode_toko;
            window.open("retur_barang_dari_toko/transaksi/?" + FrmData,'_blank');
        }else{
            $('.modal-dialog').removeClass('modal-lg');
            $('.modal-dialog').addClass('modal-sm');
            $('#ModalHeader').html('Oops !');
            $('#ModalContent').html('No. retur barang tidak dikenal');
            $('#ModalFooter').html("<button type='button' class='btn btn-primary' data-dismiss='modal' autofocus>Ok</button>");
            $('#ModalGue').modal('show');
        }
    }

    function edit_penjualan(id_penjualan_m)
    {
        if((id_penjualan_m) !== ''){
            var FrmData = "&id="+id_penjualan_m;
            window.open("penjualan/transaksi/?" + FrmData,'_blank');
        }else{
            $('.modal-dialog').removeClass('modal-lg');
            $('.modal-dialog').addClass('modal-sm');
            $('#ModalHeader').html('Oops !');
            $('#ModalContent').html('No. Penjualan tidak dikenal');
            $('#ModalFooter').html("<button type='button' class='btn btn-primary' data-dismiss='modal' autofocus>Ok</button>");
            $('#ModalGue').modal('show');
        }
    }

    function selesai_transaksi(id_penjualan_m, no_penjualan, nama_customer)
    {
        var pesan   = "No. Penjualan : " + no_penjualan + ", " +
                      "Nama Customer : " + nama_customer;
        swal({
            title: "Yakin ingin menyelesaikan transaksi ini ?",
            text: pesan,
            type: "info",
            showCancelButton: true,
            confirmButtonClass: 'btn-primary btn-md waves-effect waves-light',
            confirmButtonText: "Iya, saya yakin",
            cancelButtonText: "Batal",
        },function (isConfirm){
            if(isConfirm){
                $.ajax({
                    url : "<?php echo site_url('laporan_penjualan_pusat/ajax_selesai_transaksi'); ?>",
                    type: "POST",
                    cache: false,
                    data: "id_penjualan_m=" + id_penjualan_m,
                    dataType: "JSON",
                    success: function(data){
                        if(data.status == 1){
                            tanggal_laporan = $('#tanggal_filter').html();
                            ambil_data();

                            swal({
                                title: data.info_pesan,
                                text: data.pesan,
                                type: "success",
                                showCancelButton: true,
                                confirmButtonText: "Iya, saya yakin",
                                cancelButtonText: "Tidak, nantik saja"
                            }, function(){
                                var FormData        = "&id_penjualan_m=" + id_penjualan_m;
                                var url_cetak_fix   = "<?php echo site_url('faktur/'); ?>" + data.url_cetak + "/?";
                                window.open(url_cetak_fix + FormData,'_blank');
                            });
                        }else if(data.status == 0){
                            swal({
                                title: data.info_pesan,
                                text: data.pesan,
                                type: data.tipe_pesan,
                                confirmButtonClass: 'btn-danger btn-md waves-effect waves-light',
                                confirmButtonText: "Ok"
                            }, function(isConfirm){
                                window.location.href = 'laporan_penjualan_pusat';
                            });
                        }
                    }, error: function (jqXHR, textStatus, errorThrown){
                        swal({
                            title: "Oops !",
                            text: "Data penjualan gagal ditampilkan.",
                            type: "error",
                            confirmButtonClass: 'btn-danger btn-md waves-effect waves-light',
                            confirmButtonText: "Ok"
                        }, function (isConfirm){
                            window.location.href = 'laporan_penjualan_pusat';
                        });
                    }
                });
            }
        });
    }

    $(document).on('DOMSubtreeModified', '#tanggal_filter', function(){

    });

    function real_time()
    {
        // Tanggal
        var months  = ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 
                       'September', 'Oktober', 'November', 'Desember'];
        var myDays  = ['Minggu', 'Senin', 'Selasa', 'Rabu', 'Kamis', 'Jumat', 'Sabtu'];
        var date    = new Date();
        var day     = date.getDate();
        var month   = date.getMonth();
        var thisDay = date.getDay(),
        thisDay     = myDays[thisDay];
        var yy      = date.getYear();
        var year    = (yy < 1000) ? yy + 1900 : yy;
        
        document.getElementById('tanggal').innerHTML          = (thisDay + ', ' + day + ' ' + months[month] + ' ' + year);
        document.getElementById('tanggal_awal').innerHTML     = (year + '-' + (month+1) + '-01');
        document.getElementById('tanggal_sekarang').innerHTML = (year + '-' + (month+1) + '-' + day);

        // Jam
        var today = new Date();
        var jam   = today.getHours();
        var menit = today.getMinutes();
        var detik = today.getSeconds();

        if(detik < 10){
            detik = '0' + detik;
        }

        if(menit < 10){
            menit = '0' + menit;
        }

        if(jam < 10){
            jam = '0' + jam;
        }
        document.getElementById('jam').innerHTML=(jam+':'+menit+':'+detik);

        // Ucapan
        var UcapanSalam = "";
        if (jam < 4) {UcapanSalam       = "Selamat Malam !";}
        else if (jam < 11) {UcapanSalam = "Selamat Pagi !!!";}
        else if (jam < 16) {UcapanSalam = "Selamat Siang !!";}
        else if (jam < 20) {UcapanSalam = "Selamat Sore !!";}
        else {UcapanSalam               = "Selamat Malam !";}
        document.getElementById('ucapan').innerHTML=(UcapanSalam);
    }

    function to_rupiah(angka){
        var rev     = parseInt(angka, 10).toString().split('').reverse().join('');
        var rev2    = '';
        for(var i = 0; i < rev.length; i++){
            rev2  += rev[i];
            if((i + 1) % 3 === 0 && i !== (rev.length - 1)){
                rev2 += '.';
            }
        }
        return rev2.split('').reverse().join('');

        // Pakai Simbol Rp.
        // return 'Rp. ' + rev2.split('').reverse().join('');
    }

    function to_angka(rp)
    {
        return parseInt(rp.replace(/,.*|\D/g,''),10)
    }

    setInterval('real_time()', 1000);
</script>
<!-- Akhir Script CRUD -->
<!-- Awal CSS -->
<link href="assets/plugin/zircos/material-design/assets/css/tambahan.css" rel="stylesheet" type="text/css"/>
<link href="assets/plugin/zircos/plugins/datatables/jquery.dataTables.min.css" rel="stylesheet" type="text/css"/>
<link href="assets/plugin/zircos/plugins/datatables/buttons.bootstrap.min.css" rel="stylesheet" type="text/css"/>
<link href="assets/plugin/zircos/plugins/datatables/dataTables.colVis.css" rel="stylesheet" type="text/css"/>
<link href="assets/plugin/zircos/plugins/datatables/scroller.bootstrap.min.css" rel="stylesheet" type="text/css"/>
<link href="assets/plugin/zircos/plugins/datatables/fixedColumns.dataTables.min.css" rel="stylesheet" type="text/css"/>
<link href="assets/plugin/zircos/plugins/datatables/responsive.bootstrap.min.css" rel="stylesheet" type="text/css"/>
<link href="assets/plugin/zircos/plugins/datatables/dataTables.bootstrap.min.css" rel="stylesheet" type="text/css"/>
<link href="assets/plugin/zircos/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css" rel="stylesheet">
<link href="assets/plugin/zircos/plugins/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
<!-- Akhir CSS -->

<!-- Awal JS -->
<script src="assets/plugin/zircos/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="assets/plugin/zircos/plugins/datatables/dataTables.bootstrap.js"></script>

<script src="assets/plugin/zircos/material-design/assets/js/detect.js"></script>
<script src="assets/plugin/zircos/material-design/assets/js/jquery.slimscroll.js"></script>
<script src="assets/plugin/zircos/plugins/datatables/dataTables.colVis.js"></script>
<script src="assets/plugin/zircos/plugins/datatables/dataTables.responsive.min.js"></script>
<script src="assets/plugin/zircos/plugins/datatables/responsive.bootstrap.min.js"></script>
<script src="assets/plugin/zircos/plugins/datatables/dataTables.fixedColumns.min.js"></script>
<script src="assets/plugin/zircos/plugins/datatables/dataTables.scroller.min.js"></script>
<!-- Akhir JS -->

<!-- Awal datatable -->
<script src="assets/plugin/zircos/plugins/datatables/dataTables.buttons.min.js"></script>
<script src="assets/plugin/zircos/plugins/datatables/buttons.bootstrap.min.js"></script>
<script src="assets/plugin/zircos/plugins/datatables/jszip.min.js"></script>
<script src="assets/plugin/zircos/plugins/datatables/pdfmake.min.js"></script>
<script src="assets/plugin/zircos/plugins/datatables/vfs_fonts.js"></script>
<script src="assets/plugin/zircos/plugins/datatables/buttons.html5.min.js"></script>
<script src="assets/plugin/zircos/plugins/datatables/buttons.print.min.js"></script>
<!-- Akhir datatable -->

<!-- Awal date rangepicker -->
<!-- <script src="assets/plugin/zircos/plugins/moment/moment.js"></script>
<script src="assets/plugin/zircos/plugins/timepicker/bootstrap-timepicker.js"></script>
<script src="assets/plugin/zircos/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js"></script>
<script src="assets/plugin/zircos/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
<script src="assets/plugin/zircos/plugins/clockpicker/js/bootstrap-clockpicker.min.js"></script>
<script src="assets/plugin/zircos/plugins/bootstrap-daterangepicker/daterangepicker.js"></script>
<script src="assets/plugin/zircos/material-design/assets/pages/jquery.form-pickers.init.js"></script> -->
<!-- Akhir date rangepicker -->

<!-- Awal Sweet-Alert  -->
<link href="assets/plugin/zircos/plugins/bootstrap-sweetalert/sweet-alert.css" rel="stylesheet" type="text/css">
<script src="assets/plugin/zircos/plugins/bootstrap-sweetalert/sweet-alert.min.js"></script>
<!-- Akhir Sweet-Alert  -->

<script>
    var tppp = $('#TableLaporanPenjualanPusatPerhari').DataTable();
    var tptp = $('#TableLaporanPenjualanTokoPerhari').DataTable();
    // ----------------------------------------------------
    var ttcp = $('#TableLaporanTopCustomerPusat').DataTable();
    var ttct = $('#TableLaporanTopCustomerToko').DataTable();
    // ----------------------------------------------------
    var ttpp = $('#TableLaporanTopProdukPusat').DataTable();
    var ttpc = $('#TableLaporanTopProdukPercustomer').DataTable();
    var ttpt = $('#TableLaporanTopProdukToko').DataTable();
    // ----------------------------------------------------
        
    var id_tab_pusat  = "1";
    var id_tab_mrc    = "1";
    var status_tampil = '0';

    var tanggal_laporan;
    var jenis_perintah;

    var delay = (function () {
        var timer = 0;
        return function (callback, ms){
            clearTimeout(timer);
            timer = setTimeout(callback, ms);
        };
    })();

    <?php if($this->session->userdata('usergroup_name') == 'Super Admin'){ ?>
        var status_modal = '0';
        var pesan_modal  = 'Apakah anda yakin ingin menampilkan modal ?';

        function TampilkanModal(){
            if(status_modal == '0'){
                $('#KotakModal').removeAttr('hidden');
                $('#KotakKeuntungan').removeAttr('hidden');
                $('#KotakAsset').removeAttr('hidden');

                $('#KotakModalPerToko').removeAttr('hidden');
                $('#KotakKeuntunganPerToko').removeAttr('hidden');
                $('#KotakAssetPerToko').removeAttr('hidden');

                status_modal = '1';
                pesan_modal  = "Sembunyikan modal ?";
            }else if(status_modal == '1'){
                $('#KotakModal').prop('hidden', true)
                $('#KotakKeuntungan').prop('hidden', true);
                status_modal = '0';
                pesan_modal  = "Apakah anda yakin ingin menampilkan modal ?";
            }
        }
    <?php } ?>

    $(document).on('keydown', 'body', function(e){
        var charCode = ( e.which ) ? e.which : event.keyCode;

        <?php if($this->session->userdata('usergroup_name') == 'Super Admin'){ ?>
            if(charCode == 119) //F8
            {
                delay(function(){
                    swal({
                        title: pesan_modal,
                        type: "info",
                        showCancelButton: true,
                        confirmButtonClass: 'btn-primary btn-md waves-effect waves-light',
                        confirmButtonText: "Iya, saya yakin",
                        cancelButtonText: "Batal",
                    },function (isConfirm){
                        if(isConfirm){
                            if(status_tampil == '0'){
                                status_tampil = '1';
                                refresh_data_pusat();
                                refresh_data_mrc();
                            }else{
                                status_tampil = '0';
                                tampilkan_data_pusat();
                                tampilkan_data_mrc();
                            }
                            TampilkanModal();
                        }
                    });
                }, 100);
                return false;
            }
        <?php } ?>
    });

    $(document).ready(function(){
        $('#navbar').removeClass('navbar-default');
        $('#navbar').addClass('navbar-hijau');

        $("#wrapper").removeClass('forced');
        $("#wrapper").addClass('forced enlarged');
        $('#JudulHalaman').html('DASHBOARD DETAIL - VAMA');

        $('#TableLaporanTopCustomerPusat tbody').on( 'click', 'tr', function(){
            $(this).toggleClass('selected');
        });

        $('#TableLaporanCustomerToko tbody').on( 'click', 'tr', function(){
            $(this).toggleClass('selected');
        });

        $('#TableLaporanBatalUmum tbody').on( 'click', 'tr', function(){
            $(this).toggleClass('selected');
        });

        $('#TableLaporanBatalMrc tbody').on( 'click', 'tr', function(){
            $(this).toggleClass('selected');
        });

        //datepicker
        $('#tanggal_awal').datepicker({
            autoclose: true,
            format: "yyyy-mm-dd",
            todayHighlight: true
        });

        $('#tanggal_akhir').datepicker({
            autoclose: true,
            format: "yyyy-mm-dd",
            todayHighlight: true
        });
    });

    // ------------------------------------------------------------
    // Tab Penjualan Pusat
    $(document).on('click', '#tab_perhari_pusat', function(){
        id_tab_pusat = "1";
        tampilkan_data_pusat();
    });

    $(document).on('click', '#tab_customer_pusat', function(){
        id_tab_pusat = "2";
        tampilkan_data_pusat();
    });

    $(document).on('click', '#tab_produk_pusat', function(){
        id_tab_pusat = "3";
        tampilkan_data_pusat();
    });

    $(document).on('click', '#tab_produk_percustomer', function(){
        id_tab_pusat = "4";
        tampilkan_data_pusat();
    });
    // ------------------------------------------------------------

    // ------------------------------------------------------------
    // Tab Penjualan MRC
    $(document).on('change', '#kode_toko', function(){
        tampilkan_data_mrc();
    });

    $(document).on('click', '#tab_perhari_toko', function(){
        id_tab_mrc = "1";
        tampilkan_data_mrc();
    });

    $(document).on('click', '#tab_customer_toko', function(){
        id_tab_mrc = "2";
        tampilkan_data_mrc();
    });

    $(document).on('click', '#tab_produk_toko', function(){
        id_tab_mrc = "3";
        tampilkan_data_mrc();
    });
    // ------------------------------------------------------------

    // ------------------------------------------------------------
    // Awal pusat
    function tampilkan_data_perhari_pusat()
    {
        $('#header_perhari').html(
            '<th>#</th>' +
            '<th>Tanggal</th>' +
            '<th>Transaksi</th>' +
            '<th>Tunai</th>' +
            '<th>Debit</th>' +
            '<th>Total</th>' +
            '<th>Total Retur</th>' +
            '<th>Total Bersih</th>'
        );

        <?php if($this->session->userdata('usergroup_name') == 'Super Admin'){ ?>
            if(status_tampil == '1'){
                $('#header_perhari').html(
                    '<th>#</th>' +
                    '<th>Tanggal</th>' +
                    '<th>Transaksi</th>' +
                    '<th>Tunai</th>' +
                    '<th>Debit</th>' +
                    '<th>Total</th>' +
                    '<th>Total Retur</th>' +
                    '<th>Total Bersih</th>' +
                    '<th>Total Modal</th>' +
                    '<th>Total Keuntungan</th>'
                );
            }
        <?php } ?>

        jenis_perintah  = "!=";
        tppp.clear();
        tppp.destroy();
        tppp = $('#TableLaporanPenjualanPusatPerhari').DataTable({
            <?php if($this->session->userdata('usergroup_name') == 'Super Admin'){ ?>
                dom : "Blftpi",
            <?php } ?>

            buttons: [
                        {
                            extend: "copy",
                            text: "<i class='fa fa-files-o text-primary'></i>",
                            titleAttr: "Salin data ke clipboard"
                        },
                        {
                            extend: "excel",
                            text: "<i class='fa fa-file-excel-o text-success'></i>",
                            titleAttr: "Excel"
                        },
                        {
                            extend: "pdf",
                            text: "<i class='fa fa-file-pdf-o text-danger'></i>",
                            titleAttr: "PDF"
                        },
                        {
                            extend: "print",
                            text: "<i class='fa fa-print'></i>",
                            titleAttr: "Print"
                        }
                     ],
            stateSave: true,
            ordering: true,
            bFilter:true,
            processing:true,
            serverSide:true,
            order:[],
            lengthMenu: [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
            oLanguage:{
                sLoadingRecords:'Silakan Tunggu',
                sProcessing:"<img src='assets/loader/cubeloader2.2.gif' width='50' height='50'> Harap tunggu",
                sZeroRecords:"Data Tidak Ditemukan",
                sSearch : "Pencarian :  _INPUT_",
                sLengthMenu: "<i class='btn btn-primary btn-sm fa fa-folder-open-o'></i><span class='btn btn-default btn-xs'>Menampilkan _MENU_ penjualan induk</span>",
                sInfo: "<span class='btn btn-sm'>Menampilkan (_START_ sampai _END_) dari _TOTAL_ penjualan perhari</span>",
                sInfoEmpty: "<span class='btn btn-danger btn-sm'>Tidak ada penjualan induk untuk ditampilkan</span>",
                sInfoFiltered: "<span class='btn btn-default btn-sm text-primary'>(disaring dari _MAX_ total penjualan induk)</span>"
            },

            ajax: {
                url: "<?php echo site_url('dashboard2/ajax_list_penjualan_perhari_pusat')?>",
                type: "POST",
                data : {
                    'tanggal_awal' : $('#tanggal_awal').val(),
                    'tanggal_akhir': $('#tanggal_akhir').val(),
                    'jenis_perintah' : jenis_perintah,
                    'status_tampil' : status_tampil
                }
            },

            columnDefs: [
                            {
                                "targets": [ -1 ],
                                "orderable": false,
                            },
                          ],
        });
    }

    function tampilkan_data_customer_pusat()
    {
        jenis_perintah = "!=";
        ttcp.clear();
        ttcp.destroy();
        ttcp = $('#TableLaporanTopCustomerPusat').DataTable({
            <?php if($this->session->userdata('usergroup_name') == 'Super Admin'){ ?>
                dom: 'BlCftpir',
                buttons: [  {
                                extend: "copy",
                                text: "<i class='fa fa-files-o text-primary'></i>",
                                titleAttr: "Salin data ke clipboard"
                            },
                            {
                                extend: "excel",
                                text: "<i class='fa fa-file-excel-o text-success'></i>",
                                titleAttr: "Excel"
                            },
                            {
                                extend: "pdf",
                                text: "<i class='fa fa-file-pdf-o text-danger'></i>",
                                titleAttr: "PDF"
                            },
                            {
                                extend: "print",
                                text: "<i class='fa fa-print'></i>",
                                titleAttr: "Print"
                            }
                        ],
            <?php }else{ ?>
                "dom": 'Clftpir',
            <?php } ?>

            colVis: {
                        buttonText: "<i class='fa fa-eye'></i> <b>Tampilkan / sembunyikan kolom</b>"
                    },
            stateSave: true,
            ordering: true,
            bFilter:true,
            processing:true,
            serverSide:true,
            order:[],
            lengthMenu: [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
            oLanguage:{
                sLoadingRecords:'Silakan Tunggu',
                sProcessing:"<img src='assets/loader/cubeloader2.2.gif' width='50' height='50'> Harap tunggu",
                sZeroRecords:"Data Tidak Ditemukan",
                sSearch : "Pencarian :  _INPUT_",
                sLengthMenu: "<i class='btn btn-primary btn-sm fa fa-folder-open-o'></i><span class='btn btn-default btn-xs'>Menampilkan _MENU_ penjualan detail</span>",
                sInfo: "<span class='btn btn-sm'>Menampilkan (_START_ sampai _END_) dari _TOTAL_ penjualan detail</span>",
                sInfoEmpty: "<span class='btn btn-danger btn-sm'>Tidak ada penjualan detail untuk ditampilkan</span>",
                sInfoFiltered: "<span class='btn btn-default btn-sm text-primary'>(disaring dari _MAX_ total penjualan detail)</span>"
            },

            ajax: {
                url: "<?php echo site_url('dashboard2/ajax_list_top_customer_pusat')?>",
                type: "POST",
                data : {
                    'tanggal_awal' : $('#tanggal_awal').val(),
                    'tanggal_akhir': $('#tanggal_akhir').val(),
                    'jenis_perintah' : jenis_perintah
                }
            },

            columnDefs: [
                            {
                                "targets": [ -1 ],
                                "orderable": false,
                            },
                          ],

            scrollX: true,
            deferRender: true,
            scrollY: 350,
            scrollCollapse: true,
            scroller: true
        });
    }

    function tampilkan_data_produk_pusat()
    {
        jenis_perintah = "!=";
        ttpp.clear();
        ttpp.destroy();
        ttpp = $('#TableLaporanTopProdukPusat').DataTable({
            <?php if($this->session->userdata('usergroup_name') == 'Super Admin'){ ?>
                dom : "Blftpi",
            <?php } ?>

            buttons: [
                        {
                            extend: "copy",
                            text: "<i class='fa fa-files-o text-primary'></i>",
                            titleAttr: "Salin data ke clipboard"
                        },
                        {
                            extend: "excel",
                            text: "<i class='fa fa-file-excel-o text-success'></i>",
                            titleAttr: "Excel"
                        },
                        {
                            extend: "pdf",
                            text: "<i class='fa fa-file-pdf-o text-danger'></i>",
                            titleAttr: "PDF"
                        },
                        {
                            extend: "print",
                            text: "<i class='fa fa-print'></i>",
                            titleAttr: "Print"
                        }
                     ],
            stateSave: true,
            ordering: true,
            bFilter:true,
            processing:true,
            serverSide:true,
            order:[],
            lengthMenu: [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
            oLanguage:{
                sLoadingRecords:'Silakan Tunggu',
                sProcessing:"<img src='assets/loader/cubeloader2.2.gif' width='50' height='50'> Harap tunggu",
                sZeroRecords:"Data Tidak Ditemukan",
                sSearch : "Pencarian :  _INPUT_",
                sLengthMenu: "<i class='btn btn-primary btn-sm fa fa-folder-open-o'></i><span class='btn btn-default btn-xs'>Menampilkan _MENU_ penjualan perbarang</span>",
                sInfo: "<span class='btn btn-sm'>Menampilkan (_START_ sampai _END_) dari _TOTAL_ penjualan perbarang</span>",
                sInfoEmpty: "<span class='btn btn-danger btn-sm'>Tidak ada penjualan perbarang untuk ditampilkan</span>",
                sInfoFiltered: "<span class='btn btn-default btn-sm text-primary'>(disaring dari _MAX_ total penjualan perbarang)</span>"
            },

            ajax: {
                url: "<?php echo site_url('dashboard2/ajax_list_top_barang_pusat')?>",
                type: "POST",
                data : {
                    'tanggal_awal' : $('#tanggal_awal').val(),
                    'tanggal_akhir': $('#tanggal_akhir').val(),
                    'jenis_perintah' : jenis_perintah
                }
            },

            columnDefs: [
                            {
                                "targets": [ -1 ],
                                "orderable": false,
                            },
                          ],
        });
    }

    function tampilkan_data_produk_percustomer()
    {
        jenis_perintah = "!=";
        ttpc.clear();
        ttpc.destroy();
        ttpc = $('#TableLaporanTopProdukPercustomer').DataTable({
            <?php if($this->session->userdata('usergroup_name') == 'Super Admin'){ ?>
                dom : "Blftpi",
            <?php } ?>

            buttons: [
                        {
                            extend: "copy",
                            text: "<i class='fa fa-files-o text-primary'></i>",
                            titleAttr: "Salin data ke clipboard"
                        },
                        {
                            extend: "excel",
                            text: "<i class='fa fa-file-excel-o text-success'></i>",
                            titleAttr: "Excel"
                        },
                        {
                            extend: "pdf",
                            text: "<i class='fa fa-file-pdf-o text-danger'></i>",
                            titleAttr: "PDF"
                        },
                        {
                            extend: "print",
                            text: "<i class='fa fa-print'></i>",
                            titleAttr: "Print"
                        }
                     ],
            stateSave: true,
            ordering: true,
            bFilter:true,
            processing:true,
            serverSide:true,
            order:[],
            lengthMenu: [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
            oLanguage:{
                sLoadingRecords:'Silakan Tunggu',
                sProcessing:"<img src='assets/loader/cubeloader2.2.gif' width='50' height='50'> Harap tunggu",
                sZeroRecords:"Data Tidak Ditemukan",
                sSearch : "Pencarian :  _INPUT_",
                sLengthMenu: "<i class='btn btn-primary btn-sm fa fa-folder-open-o'></i><span class='btn btn-default btn-xs'>Menampilkan _MENU_ penjualan perbarang</span>",
                sInfo: "<span class='btn btn-sm'>Menampilkan (_START_ sampai _END_) dari _TOTAL_ penjualan perbarang</span>",
                sInfoEmpty: "<span class='btn btn-danger btn-sm'>Tidak ada penjualan perbarang untuk ditampilkan</span>",
                sInfoFiltered: "<span class='btn btn-default btn-sm text-primary'>(disaring dari _MAX_ total penjualan perbarang)</span>"
            },

            ajax: {
                url: "<?php echo site_url('dashboard2/ajax_list_top_barang_percustomer')?>",
                type: "POST",
                data : {
                    'tanggal_awal' : $('#tanggal_awal').val(),
                    'tanggal_akhir': $('#tanggal_akhir').val(),
                    'jenis_perintah' : jenis_perintah
                }
            },

            columnDefs: [
                            {
                                "targets": [ -1 ],
                                "orderable": false,
                            },
                          ],
        });
    }
    // Akhir pusat
    // ------------------------------------------------------------

    // ------------------------------------------------------------
    // Awal mrc
    function tampilkan_data_perhari_mrc()
    {
        $('#header_perhari_toko').html(
            '<th>#</th>' +
            '<th>Tanggal</th>' +
            '<th>Transaksi</th>' +
            '<th>Tunai</th>' +
            '<th>Debit</th>' +
            '<th>Total</th>' +
            '<th>Total Retur</th>' +
            '<th>Total Bersih</th>'
        );

        <?php if($this->session->userdata('usergroup_name') == 'Super Admin'){ ?>
            if(status_tampil == '1'){
                $('#header_perhari_toko').html(
                    '<th>#</th>' +
                    '<th>Tanggal</th>' +
                    '<th>Transaksi</th>' +
                    '<th>Tunai</th>' +
                    '<th>Debit</th>' +
                    '<th>Total</th>' +
                    '<th>Total Retur</th>' +
                    '<th>Total Bersih</th>' +
                    '<th>Total Modal</th>' +
                    '<th>Total Keuntungan</th>'
                );
            }
        <?php } ?>

        jenis_perintah  = "!=";
        tptp.clear();
        tptp.destroy();
        tptp = $('#TableLaporanPenjualanTokoPerhari').DataTable({
            <?php if($this->session->userdata('usergroup_name') == 'Super Admin'){ ?>
                dom : "Blftpi",
            <?php } ?>

            buttons: [
                        {
                            extend: "copy",
                            text: "<i class='fa fa-files-o text-primary'></i>",
                            titleAttr: "Salin data ke clipboard"
                        },
                        {
                            extend: "excel",
                            text: "<i class='fa fa-file-excel-o text-success'></i>",
                            titleAttr: "Excel"
                        },
                        {
                            extend: "pdf",
                            text: "<i class='fa fa-file-pdf-o text-danger'></i>",
                            titleAttr: "PDF"
                        },
                        {
                            extend: "print",
                            text: "<i class='fa fa-print'></i>",
                            titleAttr: "Print"
                        }
                     ],
            stateSave: true,
            ordering: true,
            bFilter:true,
            processing:true,
            serverSide:true,
            order:[],
            lengthMenu: [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
            oLanguage:{
                sLoadingRecords:'Silakan Tunggu',
                sProcessing:"<img src='assets/loader/cubeloader2.2.gif' width='50' height='50'> Harap tunggu",
                sZeroRecords:"Data Tidak Ditemukan",
                sSearch : "Pencarian :  _INPUT_",
                sLengthMenu: "<i class='btn btn-primary btn-sm fa fa-folder-open-o'></i><span class='btn btn-default btn-xs'>Menampilkan _MENU_ penjualan induk</span>",
                sInfo: "<span class='btn btn-sm'>Menampilkan (_START_ sampai _END_) dari _TOTAL_ penjualan perhari</span>",
                sInfoEmpty: "<span class='btn btn-danger btn-sm'>Tidak ada penjualan induk untuk ditampilkan</span>",
                sInfoFiltered: "<span class='btn btn-default btn-sm text-primary'>(disaring dari _MAX_ total penjualan induk)</span>"
            },

            ajax: {
                url: "<?php echo site_url('dashboard2/ajax_list_penjualan_perhari_mrc')?>",
                type: "POST",
                data : {
                    'tanggal_awal' : $('#tanggal_awal').val(),
                    'tanggal_akhir': $('#tanggal_akhir').val(),
                    'status_tampil' : status_tampil,
                    'kode_toko' : $('#kode_toko').val()
                }
            },

            columnDefs: [{
                "targets": [ -1 ],
                "orderable": false,
            },],
        });
    }

    function tampilkan_data_customer_mrc()
    {
        jenis_perintah = "!=";
        ttct.clear();
        ttct.destroy();
        ttct = $('#TableLaporanTopCustomerToko').DataTable({
            <?php if($this->session->userdata('usergroup_name') == 'Super Admin'){ ?>
                dom: 'BlCftpir',
                buttons: [  {
                                extend: "copy",
                                text: "<i class='fa fa-files-o text-primary'></i>",
                                titleAttr: "Salin data ke clipboard"
                            },
                            {
                                extend: "excel",
                                text: "<i class='fa fa-file-excel-o text-success'></i>",
                                titleAttr: "Excel"
                            },
                            {
                                extend: "pdf",
                                text: "<i class='fa fa-file-pdf-o text-danger'></i>",
                                titleAttr: "PDF"
                            },
                            {
                                extend: "print",
                                text: "<i class='fa fa-print'></i>",
                                titleAttr: "Print"
                            }
                        ],
            <?php }else{ ?>
                "dom": 'Clftpir',
            <?php } ?>

            colVis: {
                        buttonText: "<i class='fa fa-eye'></i> <b>Tampilkan / sembunyikan kolom</b>"
                    },
            stateSave: true,
            ordering: true,
            bFilter:true,
            processing:true,
            serverSide:true,
            order:[],
            lengthMenu: [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
            oLanguage:{
                sLoadingRecords:'Silakan Tunggu',
                sProcessing:"<img src='assets/loader/cubeloader2.2.gif' width='50' height='50'> Harap tunggu",
                sZeroRecords:"Data Tidak Ditemukan",
                sSearch : "Pencarian :  _INPUT_",
                sLengthMenu: "<i class='btn btn-primary btn-sm fa fa-folder-open-o'></i><span class='btn btn-default btn-xs'>Menampilkan _MENU_ penjualan detail</span>",
                sInfo: "<span class='btn btn-sm'>Menampilkan (_START_ sampai _END_) dari _TOTAL_ penjualan detail</span>",
                sInfoEmpty: "<span class='btn btn-danger btn-sm'>Tidak ada penjualan detail untuk ditampilkan</span>",
                sInfoFiltered: "<span class='btn btn-default btn-sm text-primary'>(disaring dari _MAX_ total penjualan detail)</span>"
            },

            ajax: {
                url: "<?php echo site_url('dashboard2/ajax_list_top_customer_mrc')?>",
                type: "POST",
                data : {
                    'tanggal_awal' : $('#tanggal_awal').val(),
                    'tanggal_akhir': $('#tanggal_akhir').val(),
                    'status_tampil' : status_tampil,
                    'kode_toko' : $('#kode_toko').val()
                }
            },

            columnDefs: [{
                "targets": [ -1 ],
                "orderable": false,
            },],

            scrollX: true,
            deferRender: true,
            scrollY: 350,
            scrollCollapse: true,
            scroller: true
        });
    }

    function tampilkan_data_produk_mrc()
    {
        jenis_perintah = "!=";
        ttpt.clear();
        ttpt.destroy();
        ttpt = $('#TableLaporanTopProdukToko').DataTable({
            <?php if($this->session->userdata('usergroup_name') == 'Super Admin'){ ?>
                dom : "Blftpi",
            <?php } ?>

            buttons: [
                        {
                            extend: "copy",
                            text: "<i class='fa fa-files-o text-primary'></i>",
                            titleAttr: "Salin data ke clipboard"
                        },
                        {
                            extend: "excel",
                            text: "<i class='fa fa-file-excel-o text-success'></i>",
                            titleAttr: "Excel"
                        },
                        {
                            extend: "pdf",
                            text: "<i class='fa fa-file-pdf-o text-danger'></i>",
                            titleAttr: "PDF"
                        },
                        {
                            extend: "print",
                            text: "<i class='fa fa-print'></i>",
                            titleAttr: "Print"
                        }
                     ],
            stateSave: true,
            ordering: true,
            bFilter:true,
            processing:true,
            serverSide:true,
            order:[],
            lengthMenu: [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
            oLanguage:{
                sLoadingRecords:'Silakan Tunggu',
                sProcessing:"<img src='assets/loader/cubeloader2.2.gif' width='50' height='50'> Harap tunggu",
                sZeroRecords:"Data Tidak Ditemukan",
                sSearch : "Pencarian :  _INPUT_",
                sLengthMenu: "<i class='btn btn-primary btn-sm fa fa-folder-open-o'></i><span class='btn btn-default btn-xs'>Menampilkan _MENU_ penjualan perbarang</span>",
                sInfo: "<span class='btn btn-sm'>Menampilkan (_START_ sampai _END_) dari _TOTAL_ penjualan perbarang</span>",
                sInfoEmpty: "<span class='btn btn-danger btn-sm'>Tidak ada penjualan perbarang untuk ditampilkan</span>",
                sInfoFiltered: "<span class='btn btn-default btn-sm text-primary'>(disaring dari _MAX_ total penjualan perbarang)</span>"
            },

            ajax: {
                url: "<?php echo site_url('dashboard2/ajax_list_top_barang_mrc')?>",
                type: "POST",
                data : {
                    'tanggal_awal' : $('#tanggal_awal').val(),
                    'tanggal_akhir': $('#tanggal_akhir').val(),
                    'status_tampil' : status_tampil,
                    'kode_toko' : $('#kode_toko').val()
                }
            },

            columnDefs: [{
                "targets": [ -1 ],
                "orderable": false,
            },],
        });
    }
    // Akhir mrc
    // ------------------------------------------------------------

    function ambil_total(jenis_perintah){
        $.ajax({
            url: "<?php echo site_url('dashboard2/ambil_total_penjualan'); ?>",
            type: "POST",
            cache: false,
            data: 'tanggal_awal=' + $('#tanggal_awal').val() +
                  '&tanggal_akhir=' + $('#tanggal_akhir').val() +
                  '&jenis_perintah=' + jenis_perintah +
                  '&status_tampil=' + status_tampil,
            dataType:'json',
            success: function(data){
                if(data.status == 1){
                    $('#TotalBersih').html(to_rupiah(data.total_bersih));
                    <?php if($this->session->userdata('usergroup_name') == 'Super Admin'){ ?>
                        if(status_tampil == '1'){
                            $('#TotalModal').html(to_rupiah(data.total_modal));
                            $('#TotalKeuntungan').html(to_rupiah(data.total_keuntungan));
                            $('#TotalAsset').html(to_rupiah(data.total_asset));
                        }
                    <?php } ?>

                }else if(data.status == 0){
                    $('#TotalBersih').html('0');
                    <?php if($this->session->userdata('usergroup_name') == 'Super Admin'){ ?>
                        if(status_tampil == '1'){
                            $('#TotalModal').html('0');
                            $('#TotalKeuntungan').html('0');
                            $('#TotalAsset').html('0');
                        }
                    <?php } ?>
                }
            },
            error : function(hasil){
                $('#TotalBersih').html('0');                
                <?php if($this->session->userdata('usergroup_name') == 'Super Admin'){ ?>
                    if(status_tampil == '1'){
                        $('#TotalModal').html('0');
                        $('#TotalKeuntungan').html('0');
                        $('#TotalAsset').html('0');
                    }
                <?php } ?>
            }
        });
    }

    function ambil_total_mrc(){
        $.ajax({
            url: "<?php echo site_url('dashboard2/ambil_total_penjualan_mrc'); ?>",
            type: "POST",
            cache: false,
            data: 'tanggal_awal=' + $('#tanggal_awal').val() +
                  '&tanggal_akhir=' + $('#tanggal_akhir').val() +
                  '&status_tampil=' + status_tampil +
                  '&kode_toko=' + $('#kode_toko').val(),
            dataType:'json',
            success: function(data){
                if(data.status == 1){
                    $('#TotalBersihToko').html(to_rupiah(data.total_bersih));
                    $('#TotalBersihPerToko').html(to_rupiah(data.total_bersih_toko));
                    <?php if($this->session->userdata('usergroup_name') == 'Super Admin'){ ?>
                        if(status_tampil == '1'){
                            $('#TotalModalToko').html(to_rupiah(data.total_modal));
                            $('#TotalKeuntunganToko').html(to_rupiah(data.total_keuntungan));
                            $('#TotalAssetToko').html(to_rupiah(data.total_asset));

                            $('#TotalModalPerToko').html(to_rupiah(data.total_modal_toko));
                            $('#TotalKeuntunganPerToko').html(to_rupiah(data.total_keuntungan_toko));
                            $('#TotalAssetPerToko').html(to_rupiah(data.total_asset_toko));
                        }
                    <?php } ?>

                }else if(data.status == 0){
                    $('#TotalBersih').html('0');
                    <?php if($this->session->userdata('usergroup_name') == 'Super Admin'){ ?>
                        if(status_tampil == '1'){
                            $('#TotalModalPerToko').html('0');
                            $('#TotalKeuntunganPerToko').html('0');
                            $('#TotalAssetPerToko').html('0');
                        }
                    <?php } ?>
                }
            },
            error : function(hasil){
                $('#TotalBersih').html('0');                    
                <?php if($this->session->userdata('usergroup_name') == 'Super Admin'){ ?>
                    if(status_tampil == '1'){
                        $('#TotalModalPerToko').html('0');
                        $('#TotalKeuntunganPerToko').html('0');
                        $('#TotalAssetPerToko').html('0');
                    }
                <?php } ?>
            }
        });
    }

    function ambil_total_online(){
        $.ajax({
            url: "<?php echo site_url('dashboard2/ambil_total_penjualan_online'); ?>",
            type: "POST",
            cache: false,
            data: 'tanggal_awal=' + $('#tanggal_awal').val() +
                  '&tanggal_akhir=' + $('#tanggal_akhir').val() +
                  '&status_tampil=' + status_tampil,
            dataType:'json',
            success: function(data){
                if(data.status == 1){
                    $('#TotalBersihOnline').html(to_rupiah(data.total_bersih));
                    <?php if($this->session->userdata('usergroup_name') == 'Super Admin'){ ?>
                        if(status_tampil == '1'){
                            $('#TotalModalOnline').html(to_rupiah(data.total_modal));
                            $('#TotalKeuntunganOnline').html(to_rupiah(data.total_keuntungan));
                        }
                    <?php } ?>

                }else if(data.status == 0){
                    $('#TotalBersihOnline').html('0');
                }
                HitungGrandTotal();
            },
            error : function(hasil){
                $('#TotalBersihOnline').html('0');
                HitungGrandTotal();                  
            }
        });
    }

    function refresh_data_pusat(){
        jenis_perintah = "!=";
        tampilkan_data_pusat()
    }

    function tampilkan_data_pusat(){
        if(id_tab_pusat == 1){
            tampilkan_data_perhari_pusat();
        }else if(id_tab_pusat == 2){
            tampilkan_data_customer_pusat();
        }else if(id_tab_pusat == 3){
            tampilkan_data_produk_pusat();
        }else if(id_tab_pusat == 4){
            tampilkan_data_produk_percustomer();
        }

        <?php if($this->session->userdata('usergroup_name') == 'Admin' or
                 $this->session->userdata('usergroup_name') == 'Super Admin'){ ?>
            ambil_total(jenis_perintah);
        <?php } ?>
    }

    function refresh_data_mrc(){
        jenis_perintah = "!=";
        tampilkan_data_mrc();
    }

    function tampilkan_data_mrc(){
        if($('#kode_toko').val() == ''){
            // swal({
            //     title: "Oops!", 
            //     text: "Harap pilih toko terlebih dahulu.", 
            //     type: "info", 
            //     confirmButtonClass: 'btn-primary btn-md waves-effect waves-light',
            //     confirmButtonText: "Ok"
            // }, function(){
            //     $('#kode_toko').focus();
            // });
        }else{
            if(id_tab_mrc == 1){
                tampilkan_data_perhari_mrc();
            }else if(id_tab_mrc == 2){
                tampilkan_data_customer_mrc();
            }else if(id_tab_mrc == 3){
                tampilkan_data_produk_mrc();
            }
        }

        <?php if($this->session->userdata('usergroup_name') == 'Admin' or
                 $this->session->userdata('usergroup_name') == 'Super Admin'){ ?>
            ambil_total_mrc();
            ambil_total_online();
        <?php } ?>
    }

    $(document).on('DOMSubtreeModified', '#tanggal_awal', function(){
        tanggal_awal  = $('#tanggal_awal').val();
        tanggal_akhir = $('#tanggal_akhir').val();
        if(tanggal_awal != '' || tanggal_akhir !== ''){
            tampilkan_data_pusat();
            tampilkan_data_mrc();
        }
    });

    $(document).on('DOMSubtreeModified', '#tanggal_awal', function(){
        tanggal_awal  = $('#tanggal_awal').val();
        tanggal_akhir = $('#tanggal_akhir').val();
        if(tanggal_awal != '' || tanggal_akhir !== ''){
            tampilkan_data_pusat();
            tampilkan_data_mrc();
        }
    });

    $(document).on('change', '#tanggal_awal', function(){
        tanggal_awal  = $('#tanggal_awal').val();
        tanggal_akhir = $('#tanggal_akhir').val();
        if(tanggal_awal != '' || tanggal_akhir !== ''){
            tampilkan_data_pusat();
            tampilkan_data_mrc();
        }
    });

    $(document).on('change', '#tanggal_akhir', function(){
        tanggal_awal  = $('#tanggal_awal').val();
        tanggal_akhir = $('#tanggal_akhir').val();
        if(tanggal_awal != '' || tanggal_akhir !== ''){
            tampilkan_data_pusat();
            tampilkan_data_mrc();
        }
    });

    $(document).on('click', '#refresh_data', function(){
        refresh_data_pusat();
        refresh_data_mrc();
    });

    function to_rupiah(angka){
        var rev     = parseInt(angka, 10).toString().split('').reverse().join('');
        var rev2    = '';
        for(var i = 0; i < rev.length; i++){
            rev2  += rev[i];
            if((i + 1) % 3 === 0 && i !== (rev.length - 1)){
                rev2 += '.';
            }
        }
        return rev2.split('').reverse().join('');
    }

    function to_angka(rp)
    {
        return parseInt(rp.replace(/,.*|\D/g,''),10)
    }

    function HitungGrandTotal()
    {
        delay(function(){
            TPP = to_angka($('#TotalBersih').html());        
            TPT = to_angka($('#TotalBersihToko').html());        
            TPO = to_angka($('#TotalBersihOnline').html());        

            GTP = parseInt(TPP + TPT + TPO);
            $('#GrandTotalBersih').html(to_rupiah(GTP));
            
            <?php if($this->session->userdata('usergroup_name') == 'Super Admin'){ ?>
                if(status_tampil == '1'){
                    TMP = to_angka($('#TotalModal').html());
                    TMT = to_angka($('#TotalModalToko').html());
                    TMO = to_angka($('#TotalModalOnline').html());

                    GTM = parseInt(TMP + TMT + TMO);
                    $('#GrandTotalModal').html(to_rupiah(GTM));

                    TKP = to_angka($('#TotalKeuntungan').html());
                    TKT = to_angka($('#TotalKeuntunganToko').html());
                    TKO = to_angka($('#TotalKeuntunganOnline').html());

                    GTK = parseInt(TKP + TKT + TKO);
                    $('#GrandTotalKeuntungan').html(to_rupiah(GTK));

                    TAP = to_angka($('#TotalAsset').html());
                    TAT = to_angka($('#TotalAssetToko').html());

                    GTA = parseInt(TAP + TAT);
                    $('#GrandTotalAsset').html(to_rupiah(GTA));
                }
            <?php } ?>
        }, 500);
    }
</script>
<!-- Akhir Script CRUD -->

<!-- Datepicker -->
<script src="assets/plugin/zircos/plugins/moment/moment.js"></script>
<script src="assets/plugin/zircos/plugins/timepicker/bootstrap-timepicker.js"></script>
<script src="assets/plugin/zircos/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js"></script>
<script src="assets/plugin/zircos/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
<script src="assets/plugin/zircos/plugins/clockpicker/js/bootstrap-clockpicker.min.js"></script>
<script src="assets/plugin/zircos/plugins/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- Datepicker -->
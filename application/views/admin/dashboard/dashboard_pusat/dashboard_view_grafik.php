<div class="content">
    <div class="container">
        <br>
        <!-- Penjualan toko perbulan -->
        <div class="row">
            <div class="col-sm-12">
                <div class="card-box table-responsive">
                    <div class="row">
                        <div class="col-md-9">
                            <h4 class="header-title m-t-0">PENJUALAN TOKO PERBULAN</h4>
                            <button class="btn btn-sm btn-primary" id="refresh_data_toko_perbulan">
                                <i class="glyphicon glyphicon-refresh"></i> 
                                TAMPILKAN
                            </button>
                        </div>
                    
                        <div class="col-md-3">
                            <div class="pull-right">
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <small>Tanggal awal</small>
                                            <input id="tanggal_awal_toko_perbulan" type="text" class="form-control" data-date-format="yyyy-mm-dd" name="tanggal_awal_toko_perbulan" value="<?php echo $tanggal_awal ?>"/>
                                            <span class="help-block"></span>
                                        </div>

                                        <div class="col-sm-6">
                                            <small>Tanggal akhir</small>
                                            <input id="tanggal_akhir_toko_perbulan" type="text" class="form-control" data-date-format="yyyy-mm-dd" name="tanggal_akhir_toko_perbulan" value="<?php echo $tanggal_akhir ?>"/>
                                            <span class="help-block"></span>
                                        </div>                            
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="clearfix"></div>

                    <div id="penjualan_toko_perbulan"></div>
                </div>
            </div>
        </div>

        <!-- Penjualan toko perhari -->
        <div class="row">
            <div class="col-sm-12">
                <div class="card-box table-responsive">
                    <div class="row">
                        <div class='col-sm-12'>
                            <h4 class="text-dark">PENJUALAN TOKO PERHARI - (<?php echo $bulan ?>)</h4>
                            <button class="btn btn-sm btn-primary" id="refresh_data_toko_perhari">
                                <i class="glyphicon glyphicon-refresh"></i> 
                                TAMPILKAN
                            </button>
                        </div>
                    </div>
                    <!-- <hr> -->
                    <div class="clearfix"></div>
                    <div id="penjualan_toko_perhari"></div>
                </div>
            </div>
        </div>

        <!-- Penjualan pusat grosir perhari -->
        <div class="row">
            <div class="col-sm-12">
                <div class="card-box table-responsive">
                    <div class="row">
                        <div class='col-sm-12'>
                            <h4 class="text-dark">PENJUALAN PUSAT PERHARI - GROSIR - (<?php echo $bulan ?>)</h4>
                            <button class="btn btn-sm btn-primary" id="refresh_data_pusat_grosir_perhari">
                                <i class="glyphicon glyphicon-refresh"></i> 
                                TAMPILKAN
                            </button>
                        </div>
                    </div>
                    <!-- <hr> -->
                    <div class="clearfix"></div>
                    <div id="penjualan_pusat_umum_perhari"></div>
                </div>
            </div>
        </div>

        <!-- Penjualan pusat mrc perhari -->
        <div class="row">
            <div class="col-sm-12">
                <div class="card-box table-responsive">
                    <div class="row">
                        <div class='col-sm-12'>
                            <h4 class="text-dark">PENJUALAN PUSAT PERHARI - MRC - (<?php echo $bulan ?>)</h4>
                            <button class="btn btn-sm btn-primary" id="refresh_data_pusat_mrc_perhari">
                                <i class="glyphicon glyphicon-refresh"></i> 
                                TAMPILKAN
                            </button>
                        </div>
                    </div>
                    <!-- <hr> -->
                    <div class="clearfix"></div>
                    <div id="penjualan_pusat_mrc_perhari"></div>
                </div>
            </div>
        </div>

        <!-- Penjualan pusat perbulan -->
        <div class="row">
            <div class="col-sm-12">
                <div class="card-box table-responsive">
                    <div class="row">
                        <div class="col-md-9">
                            <h4 class="header-title m-t-0">PENJUALAN PUSAT PERBULAN</h4>
                            <button class="btn btn-sm btn-primary" id="refresh_data_pusat_perbulan">
                                <i class="glyphicon glyphicon-refresh"></i> 
                                TAMPILKAN
                            </button>
                        </div>
                    
                        <div class="col-md-3">
                            <div class="pull-right">
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <small>Tanggal awal</small>
                                            <input id="tanggal_awal_pusat_perbulan" type="text" class="form-control" data-date-format="yyyy-mm-dd" name="tanggal_awal_pusat_perbulan" value="<?php echo $tanggal_awal ?>"/>
                                            <span class="help-block"></span>
                                        </div>

                                        <div class="col-sm-6">
                                            <small>Tanggal akhir</small>
                                            <input id="tanggal_akhir_pusat_perbulan" type="text" class="form-control" data-date-format="yyyy-mm-dd" name="tanggal_akhir_pusat_perbulan" value="<?php echo $tanggal_akhir ?>"/>
                                            <span class="help-block"></span>
                                        </div>                            
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="clearfix"></div>

                    <div id="penjualan_pusat_perbulan"></div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal" id="ModalGue" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class='fa fa-times-circle'></i></button>
                <h4 class="modal-title" id="ModalHeader"></h4>
            </div>
            <div class="modal-body" id="ModalContent"></div>
            <div class="modal-footer" id="ModalFooter"></div>
        </div>
    </div>
</div>

<script>
$('#ModalGue').on('hide.bs.modal', function () {
   setTimeout(function(){ 
        $('#ModalHeader, #ModalContent, #ModalFooter').html('');
   }, 500);
});
</script>
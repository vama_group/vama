<div class="content">
	<div class="container">
	<br>		
		<div class="row">
        	<div class="col-sm-12">
	        	<div class="row">
		            <div class="col-sm-12">
			        	<!-- Awal daftar transaksi dan catatan transaksi --> 
		            	<div class="card-box table-responsive">
		            		<!-- Keterangan transaksi dan tombol tambah dan refersh -->
							<div class="row">
								<div class='col-sm-3'>
									<h4 class="text-pink"><i class='mdi mdi-gift'></i>
										LAPORAN HADIAH
									</h4>

									<?php if($access_create == 1){ ?>
										<button class="btn btn-sm btn-pink" id="tambah_transaksi">
											<i class="glyphicon glyphicon-plus"></i> 
											TAMBAH
										</button>
									<?php } ?>
                            		<button class="btn btn-sm btn-primary" id="refresh_data">
                            			<i class="glyphicon glyphicon-refresh"></i> 
                            			REFRESH
                            		</button>
								</div>

								<div class="col-sm-6">
									<div class="row">
										<div class="col-sm-4">
											<label>Jumlah</label><br/>
											<span id='JmlTransaksi' name="JmlTransaksi" class="form-control text-dark"><b>0</b></span>
										</div>
										<div class="col-sm-4">
											<label>Grand Total Hadiah</label><br/>
											<span id='GrandTotalHadiah' name='GrandTotalHadiah' class="form-control text-dark">Rp. 0</span>
										</div>
									</div>
								</div>
								<!-- Pemilihan tanggal awal dan akhir -->
								<div class="col-sm-3">
									<div class="form-group">
                                        <label>Pilih tanggal awal dan akhir</label>
                                        <div id="reportrange" class="pull-right form-control">
                                            <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>
                                            <span id="tanggal_filter" name="tanggal_filter"></span>
                                        </div>
                                    </div>
								</div>
							</div>
							<hr>
							<!-- <br> -->


							<!-- Awal induk tab -->
		                    <ul class="nav nav-tabs tabs-bordered" id="tab_list_hadiah">
		                        <li class="active">
		                            <a href="#tab_induk_hadiah" id="tab_induk" data-toggle="tab" aria-expanded="true">
		                                <span class="visible-xs"><i class="fa fa-check-square"></i></span>
		                                <span class="hidden-xs" id="judul_tab_induk">Data Hadiah Induk</span>
		                            </a>
		                        </li>
		                        <li class="">
		                            <a href="#tab_detail_hadiah" id="tab_detail" data-toggle="tab" aria-expanded="true">
		                                <span class="visible-xs"><i class="typcn typcn-home"></i></span>
		                                <span class="hidden-xs" id="judul_tab_batal">Data Hadiah Detail</span>
		                            </a>
		                        </li>
		                        <li class="">
		                            <a href="#tab_batal_hadiah" id="tab_batal" data-toggle="tab" aria-expanded="true">
		                                <span class="visible-xs"><i class="typcn typcn-home"></i></span>
		                                <span class="hidden-xs" id="judul_tab_batal">Data Hadiah Batal</span>
		                            </a>
		                        </li>
		                    </ul>
		                    <!-- Akhir induk tab -->

							<!-- Awal isi tab -->
		                    <div class="tab-content">
								<!-- Awal Daftar Laporan induk -->
		                        <div class="tab-pane active" id="tab_induk_hadiah">
									<table id='TableLaporanInduk' class="table table-condensed table-striped table-hover dt-responsive nowrap" cellspacing="0" width="100%">
										<thead class="input-sm">
											<tr>
												<th>#</th>
												<th>Tombol</th>
												<th>No. Hadiah</th>
												<th>Nama Customer</th>
												<th>Nama Hadiah</th>
												<th>Status</th>
												<th>Tanggal</th>
												<th>JML Barang</th>
												<th>Total Qty</th>
												<th>Total Harga</th>					
												<th>Catatan</th>

												<th>Pegawai Save</th>
												<th>Tanggal Save</th>
												<th>Pegawai Edit</th>
												<th>Tanggal Edit</th>
											</tr>
										</thead>

										<tbody class="input-sm"></tbody>
									</table>
								</div>

								<!-- Awal Daftar Laporan detail -->
		                        <div class="tab-pane" id="tab_detail_hadiah">
									<table id='TableLaporanDetail' class="table table-condensed table-striped table-hover dt-responsive nowrap" cellspacing="0" width="100%">
										<thead class="input-sm">
											<tr>
												<th>#</th>
												<th>Tombol</th>
												<th>No. Hadiah</th>
												<th>Nama Hadiah</th>
												<th>Status</th>
												<th>Tanggal</th>
												<th>SKU</th>
												<th>Nama Barang</th>
												<th>Harga</th>
												<th>Qty</th>
												<th>Subtotal</th>												

												<th>Nama Customer</th>
												<th>Catatan</th>
												<th>Pegawai Save</th>
												<th>Tanggal Save</th>
												<th>Pegawai Edit</th>
												<th>Tanggal Edit</th>
											</tr>
										</thead>

										<tbody class="input-sm"></tbody>
									</table>
								</div>

								<!-- Awal Daftar Laporan batal -->
		                        <div class="tab-pane" id="tab_batal_hadiah">
									<table id='TableLaporanBatal' class="table table-condensed table-striped table-hover dt-responsive nowrap" cellspacing="0" width="100%">
										<thead class="input-sm">
											<tr>
												<th>#</th>
												<th>No. Hadiah</th>
												<th>Nama Hadiah</th>
												<th>SKU</th>
												<th>Nama Barang</th>
												<th>Harga</th>
												<th>Qty</th>
												<th>Subtotal</th>

												<th>Nama Customer</th>
												<th>Keterangan Batal</th>
												<th>Pegawai Batal</th>
												<th>Tanggal Batal</th>
												<th>Pegawai Save</th>
												<th>Tanggal Save</th>
												<th>Pegawai Edit</th>
												<th>Tanggal Edit</th>
											</tr>
										</thead>

										<tbody class="input-sm"></tbody>
									</table>
								</div>
							</div>
						</div>
				        <!-- Akhir daftar transaksi -->
		            </div>
				</div>
            </div>
        </div>
	</div>
</div>

<div class="modal" id="ModalGue" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class='fa fa-times-circle'></i></button>
				<h4 class="modal-title" id="ModalHeader"></h4>
			</div>
			<div class="modal-body" id="ModalContent"></div>
			<div class="modal-footer" id="ModalFooter"></div>
		</div>
	</div>
</div>

<script>
$('#ModalGue').on('hide.bs.modal', function () {
   setTimeout(function(){ 
        $('#ModalHeader, #ModalContent, #ModalFooter').html('');
   }, 500);
});
</script>
<!-- Awal CSS -->
<link href="assets/plugin/zircos/material-design/assets/css/tambahan.css" rel="stylesheet" type="text/css"/>
<link href="assets/plugin/zircos/plugins/datatables/jquery.dataTables.min.css" rel="stylesheet" type="text/css"/>
<link href="assets/plugin/zircos/plugins/datatables/buttons.bootstrap.min.css" rel="stylesheet" type="text/css"/>
<link href="assets/plugin/zircos/plugins/datatables/dataTables.colVis.css" rel="stylesheet" type="text/css"/>
<link href="assets/plugin/zircos/plugins/datatables/scroller.bootstrap.min.css" rel="stylesheet" type="text/css"/>
<link href="assets/plugin/zircos/plugins/datatables/fixedColumns.dataTables.min.css" rel="stylesheet" type="text/css"/>
<link href="assets/plugin/zircos/plugins/datatables/responsive.bootstrap.min.css" rel="stylesheet" type="text/css"/>
<link href="assets/plugin/zircos/plugins/datatables/dataTables.bootstrap.min.css" rel="stylesheet" type="text/css"/>
<link href="assets/plugin/zircos/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css" rel="stylesheet">
<link href="assets/plugin/zircos/plugins/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
<!-- Akhir CSS -->

<!-- Awal JS -->
<script src="assets/plugin/zircos/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="assets/plugin/zircos/plugins/datatables/dataTables.bootstrap.js"></script>

<script src="assets/plugin/zircos/material-design/assets/js/detect.js"></script>
<script src="assets/plugin/zircos/material-design/assets/js/jquery.slimscroll.js"></script>
<script src="assets/plugin/zircos/plugins/datatables/dataTables.colVis.js"></script>
<script src="assets/plugin/zircos/plugins/datatables/dataTables.responsive.min.js"></script>
<script src="assets/plugin/zircos/plugins/datatables/responsive.bootstrap.min.js"></script>
<script src="assets/plugin/zircos/plugins/datatables/dataTables.fixedColumns.min.js"></script>
<script src="assets/plugin/zircos/plugins/datatables/dataTables.scroller.min.js"></script>
<!-- Akhir JS -->

<!-- Awal datatable -->
<script src="assets/plugin/zircos/plugins/datatables/dataTables.buttons.min.js"></script>
<script src="assets/plugin/zircos/plugins/datatables/buttons.bootstrap.min.js"></script>
<script src="assets/plugin/zircos/plugins/datatables/jszip.min.js"></script>
<script src="assets/plugin/zircos/plugins/datatables/pdfmake.min.js"></script>
<script src="assets/plugin/zircos/plugins/datatables/vfs_fonts.js"></script>
<script src="assets/plugin/zircos/plugins/datatables/buttons.html5.min.js"></script>
<script src="assets/plugin/zircos/plugins/datatables/buttons.print.min.js"></script>
<!-- Akhir datatable -->

<!-- Awal date rangepicker -->
<script src="assets/plugin/zircos/plugins/moment/moment.js"></script>
<script src="assets/plugin/zircos/plugins/timepicker/bootstrap-timepicker.js"></script>
<script src="assets/plugin/zircos/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js"></script>
<script src="assets/plugin/zircos/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
<script src="assets/plugin/zircos/plugins/clockpicker/js/bootstrap-clockpicker.min.js"></script>
<script src="assets/plugin/zircos/plugins/bootstrap-daterangepicker/daterangepicker.js"></script>
<script src="assets/plugin/zircos/material-design/assets/pages/jquery.form-pickers.init.js"></script>
<!-- Akhir date rangepicker -->

<!-- Awal Sweet-Alert  -->
<link href="assets/plugin/zircos/plugins/bootstrap-sweetalert/sweet-alert.css" rel="stylesheet" type="text/css">
<script src="assets/plugin/zircos/plugins/bootstrap-sweetalert/sweet-alert.min.js"></script>
<!-- Akhir Sweet-Alert  -->

<script>
    var tiu  = $('#TableLaporanIndukUmum').DataTable();
    var tdu  = $('#TableLaporanDetailUmum').DataTable();
    var tbu  = $('#TableLaporanBatalUmum').DataTable();
    var tdbu = $('#TableLaporanDebitUmum').DataTable();
    var tpbu = $('#TableLaporanPerbarangUmum').DataTable();
    // ----------------------------------------------------
        
    var id_tab      = "1";
    var id_tab_umum = "1";
    var tanggal_laporan;
    var jenis_perintah;

    <?php if($this->session->userdata('usergroup_name') == 'Super Admin'){ ?>
        var status_modal = '0';
        var pesan_modal  = 'Apakah anda yakin ingin menampilkan modal ?';

        function TampilkanModal(){
            if(status_modal == '0'){
                $('#total_modal').removeClass('hidden');
                $('#total_keuntungan').removeClass('hidden');
                status_modal = '1';
                pesan_modal  = "Sembunyikan modal ?";
            }else if(status_modal == '1'){
                $('#total_modal').addClass('hidden');
                $('#total_keuntungan').addClass('hidden');
                status_modal = '0';
                pesan_modal  = "Apakah anda yakin ingin menampilkan modal ?";
            }
        }
    <?php } ?>

    $(document).on('keydown', 'body', function(e){
        var charCode = ( e.which ) ? e.which : event.keyCode;
        <?php if($this->session->userdata('usergroup_name') == 'Super Admin'){ ?>
            if(charCode == 119) //F8
            {
                swal({
                    title: pesan_modal,
                    type: "info",
                    showCancelButton: true,
                    confirmButtonClass: 'btn-primary btn-md waves-effect waves-light',
                    confirmButtonText: "Iya, saya yakin",
                    cancelButtonText: "Batal",
                },function (isConfirm){
                    if(isConfirm){
                        TampilkanModal();
                    }
                });
                return false;
            }
        <?php } ?>
    });

    $(document).ready(function(){
        $('#navbar').removeClass('navbar-default');
        $('#navbar').addClass('navbar-hijau');

        $("#wrapper").removeClass('forced');
        $("#wrapper").addClass('forced enlarged');
        $('#JudulHalaman').html('Laporan Penjualan');

        $('#TableLaporanDetailUmum tbody').on( 'click', 'tr', function(){
            $(this).toggleClass('selected');
        });

        $('#TableLaporanDetailMrc tbody').on( 'click', 'tr', function(){
            $(this).toggleClass('selected');
        });

        $('#TableLaporanBatalUmum tbody').on( 'click', 'tr', function(){
            $(this).toggleClass('selected');
        });

        $('#TableLaporanBatalMrc tbody').on( 'click', 'tr', function(){
            $(this).toggleClass('selected');
        });

        $('#status_penjualan').html('<option value="" class="text-dark ">ALL</option> ' +
                                    '<option value="MENUNGGU" class="text-dark ">MENUNGGU</option> ' +
                                    '<option value="SELESAI" class="text-dark ">SELESAI</option>');
    });

    // Tab Penjualan UMUM
    $(document).on('click', '#tab_umum', function(){
        $('#total_dikirim').addClass('hidden');
        $('#status_penjualan').html('<option value="" class="text-dark ">ALL</option> ' +
                                    '<option value="MENUNGGU" class="text-dark ">MENUNGGU</option> ' +
                                    '<option value="SELESAI" class="text-dark ">SELESAI</option>');

        id_tab = "1";
        filter_data();
    });

    $(document).on('click', '#tab_induk_umum', function(){
        tanggal_laporan = $('#tanggal_filter').html();
        id_tab          = "1";
        id_tab_umum     = "1";
        tampilkan_data(tanggal_laporan);
    });

    $(document).on('click', '#tab_detail_umum', function(){
        tanggal_laporan = $('#tanggal_filter').html();
        id_tab          = "1";
        id_tab_umum     = "2";
        tampilkan_data(tanggal_laporan);
    });

    $(document).on('click', '#tab_batal_umum', function(){
        tanggal_laporan = $('#tanggal_filter').html();
        id_tab          = "1";
        id_tab_umum     = "3";
        tampilkan_data(tanggal_laporan);
    });

    $(document).on('click', '#tab_debit_umum', function(){
        tanggal_laporan = $('#tanggal_filter').html();
        id_tab          = "1";
        id_tab_umum     = "4";
        tampilkan_data(tanggal_laporan);
    });

    $(document).on('click', '#tab_perbarang_umum', function(){
        tanggal_laporan = $('#tanggal_filter').html();
        id_tab          = "1";
        id_tab_umum     = "5";
        tampilkan_data(tanggal_laporan);
    });
    // -----------------------------------------------------

    $(document).on('change', '#kode_toko', function(){
        tampilkan_data(tanggal_laporan);
    });

    function tampilkan_data_induk(tanggal_laporan)
    {
        jenis_perintah  = "!=";
        tiu.clear();
        tiu.destroy();
        tiu = $('#TableLaporanIndukUmum').DataTable({
            <?php if($this->session->userdata('usergroup_name') == 'Kepala Toko'){ ?>
                dom : "Blftpi",
            <?php } ?>

            buttons: [
                        {
                            extend: "copy",
                            text: "<i class='fa fa-files-o text-primary'></i>",
                            titleAttr: "Salin data ke clipboard"
                        },
                        {
                            extend: "excel",
                            text: "<i class='fa fa-file-excel-o text-success'></i>",
                            titleAttr: "Excel"
                        },
                        {
                            extend: "pdf",
                            text: "<i class='fa fa-file-pdf-o text-danger'></i>",
                            titleAttr: "PDF"
                        },
                        {
                            extend: "print",
                            text: "<i class='fa fa-print'></i>",
                            titleAttr: "Print"
                        }
                     ],
            stateSave: true,
            ordering: true,
            bFilter:true,
            processing:true,
            serverSide:true,
            order:[],
            lengthMenu: [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
            oLanguage:{
                sLoadingRecords:'Silakan Tunggu',
                sProcessing:"<img src='assets/loader/cubeloader2.2.gif' width='50' height='50'> Harap tunggu",
                sZeroRecords:"Data Tidak Ditemukan",
                sSearch : "Pencarian :  _INPUT_",
                sLengthMenu: "<i class='btn btn-primary btn-sm fa fa-folder-open-o'></i><span class='btn btn-default btn-xs'>Menampilkan _MENU_ penjualan induk</span>",
                sInfo: "<span class='btn btn-sm'>Menampilkan (_START_ sampai _END_) dari _TOTAL_ penjualan induk</span>",
                sInfoEmpty: "<span class='btn btn-danger btn-sm'>Tidak ada penjualan induk untuk ditampilkan</span>",
                sInfoFiltered: "<span class='btn btn-default btn-sm text-primary'>(disaring dari _MAX_ total penjualan induk)</span>"
            },

            ajax: {
                url: "<?php echo site_url('laporan_penjualan_toko/ajax_list')?>",
                type: "POST",
                data : {'kode_toko' : $('#kode_toko').val(),
                        'tanggal_filter' : tanggal_laporan,
                        'jenis_perintah' : jenis_perintah,
                        'status_penjualan' : $('#status_penjualan').val()}
            },

            columnDefs: [
                            {
                                "targets": [ -1 ],
                                "orderable": false,
                            },
                          ],
        });

        <?php if($this->session->userdata('usergroup_name') == 'Admin' or
                 $this->session->userdata('usergroup_name') == 'Super Admin'){ ?>
            ambil_total(tanggal_laporan);
        <?php } ?>
    }

    function tampilkan_data_detail(tanggal_laporan)
    {
        jenis_perintah = "!=";
        tdu.clear();
        tdu.destroy();
        tdu = $('#TableLaporanDetailUmum').DataTable({
            <?php if($this->session->userdata('usergroup_name') == 'Kepala Toko'){ ?>
                dom: 'BlCftpir',
                buttons: [  {
                                extend: "copy",
                                text: "<i class='fa fa-files-o text-primary'></i>",
                                titleAttr: "Salin data ke clipboard"
                            },
                            {
                                extend: "excel",
                                text: "<i class='fa fa-file-excel-o text-success'></i>",
                                titleAttr: "Excel"
                            },
                            {
                                extend: "pdf",
                                text: "<i class='fa fa-file-pdf-o text-danger'></i>",
                                titleAttr: "PDF"
                            },
                            {
                                extend: "print",
                                text: "<i class='fa fa-print'></i>",
                                titleAttr: "Print"
                            }
                        ],
            <?php }else{ ?>
                "dom": 'Clftpir',
            <?php } ?>

            colVis: {
                        buttonText: "<i class='fa fa-eye'></i> <b>Tampilkan / sembunyikan kolom</b>"
                    },
            stateSave: true,
            ordering: true,
            bFilter:true,
            processing:true,
            serverSide:true,
            order:[],
            lengthMenu: [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
            oLanguage:{
                sLoadingRecords:'Silakan Tunggu',
                sProcessing:"<img src='assets/loader/cubeloader2.2.gif' width='50' height='50'> Harap tunggu",
                sZeroRecords:"Data Tidak Ditemukan",
                sSearch : "Pencarian :  _INPUT_",
                sLengthMenu: "<i class='btn btn-primary btn-sm fa fa-folder-open-o'></i><span class='btn btn-default btn-xs'>Menampilkan _MENU_ penjualan detail</span>",
                sInfo: "<span class='btn btn-sm'>Menampilkan (_START_ sampai _END_) dari _TOTAL_ penjualan detail</span>",
                sInfoEmpty: "<span class='btn btn-danger btn-sm'>Tidak ada penjualan detail untuk ditampilkan</span>",
                sInfoFiltered: "<span class='btn btn-default btn-sm text-primary'>(disaring dari _MAX_ total penjualan detail)</span>"
            },

            ajax: {
                url: "<?php echo site_url('laporan_penjualan_toko/ajax_list_detail')?>",
                type: "POST",
                data : {'kode_toko' : $('#kode_toko').val(),
                        'tanggal_filter' : tanggal_laporan,
                        'jenis_perintah' : jenis_perintah,
                        'status_penjualan' : $('#status_penjualan').val()}
            },

            columnDefs: [
                            {
                                "targets": [ -1 ],
                                "orderable": false,
                            },
                          ],

            scrollX: true,
            deferRender: true,
            scrollY: 350,
            scrollCollapse: true,
            scroller: true
        });

        <?php if($this->session->userdata('usergroup_name') == 'Admin' or
                 $this->session->userdata('usergroup_name') == 'Super Admin'){ ?>
            ambil_total(tanggal_laporan);
        <?php } ?>
    }

    function tampilkan_data_batal(tanggal_laporan)
    {
        jenis_perintah  = "!=";

        tbu.clear();
        tbu.destroy();
        tbu = $('#TableLaporanBatalUmum').DataTable({
            <?php if($this->session->userdata('usergroup_name') == 'Kepala Toko'){ ?>
                dom: 'BlCftpir',
                buttons: [  {
                                extend: "copy",
                                text: "<i class='fa fa-files-o text-primary'></i>",
                                titleAttr: "Salin data ke clipboard"
                            },
                            {
                                extend: "excel",
                                text: "<i class='fa fa-file-excel-o text-success'></i>",
                                titleAttr: "Excel"
                            },
                            {
                                extend: "pdf",
                                text: "<i class='fa fa-file-pdf-o text-danger'></i>",
                                titleAttr: "PDF"
                            },
                            {
                                extend: "print",
                                text: "<i class='fa fa-print'></i>",
                                titleAttr: "Print"
                            }
                        ],
            <?php }else{ ?>
                "dom": 'Clftpir',
            <?php } ?>

            colVis: {
                        buttonText: "<i class='fa fa-eye'></i> <b>Tampilkan / sembunyikan kolom</b>"
                    },
            stateSave: true,
            ordering: true,
            bFilter:true,
            processing:true,
            serverSide:true,
            order:[],
            lengthMenu: [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
            oLanguage:{
                sLoadingRecords:'Silakan Tunggu',
                sProcessing:"<img src='assets/loader/cubeloader2.2.gif' width='50' height='50'> Harap tunggu",
                sZeroRecords:"Data Tidak Ditemukan",
                sSearch : "Pencarian :  _INPUT_",
                sLengthMenu: "<i class='btn btn-primary btn-sm fa fa-folder-open-o'></i><span class='btn btn-default btn-xs'>Menampilkan _MENU_ penjualan batal</span>",
                sInfo: "<span class='btn btn-sm'>Menampilkan (_START_ sampai _END_) dari _TOTAL_ penjualan batal</span>",
                sInfoEmpty: "<span class='btn btn-danger btn-sm'>Tidak ada penjualan batal untuk ditampilkan</span>",
                sInfoFiltered: "<span class='btn btn-default btn-sm text-primary'>(disaring dari _MAX_ total penjualan batal)</span>"
            },

            ajax: {
                url: "<?php echo site_url('laporan_penjualan_toko/ajax_list_batal')?>",
                type: "POST",
                data : {'kode_toko' : $('#kode_toko').val(),
                        'tanggal_filter' : tanggal_laporan,
                        'jenis_perintah' : jenis_perintah}
            },

            columnDefs: [
                            {
                                "targets": [ -1 ],
                                "orderable": false,
                            },
                        ],
        });

        <?php if($this->session->userdata('usergroup_name') == 'Admin' or
                 $this->session->userdata('usergroup_name') == 'Super Admin'){ ?>
            ambil_total(tanggal_laporan);
        <?php } ?>
    }

    function tampilkan_data_debit(tanggal_laporan)
    {
        jenis_perintah  = "!=";

        tdbu.clear();
        tdbu.destroy();
        tdbu = $('#TableLaporanDebitUmum').DataTable({
            <?php if($this->session->userdata('usergroup_name') == 'Kepala Toko'){ ?>
                dom : "Blftpi",
            <?php } ?>

            buttons: [
                        {
                            extend: "copy",
                            text: "<i class='fa fa-files-o text-primary'></i>",
                            titleAttr: "Salin data ke clipboard"
                        },
                        {
                            extend: "excel",
                            text: "<i class='fa fa-file-excel-o text-success'></i>",
                            titleAttr: "Excel"
                        },
                        {
                            extend: "pdf",
                            text: "<i class='fa fa-file-pdf-o text-danger'></i>",
                            titleAttr: "PDF"
                        },
                        {
                            extend: "print",
                            text: "<i class='fa fa-print'></i>",
                            titleAttr: "Print"
                        }
                     ],
            stateSave: true,
            ordering: true,
            bFilter:true,
            processing:true,
            serverSide:true,
            order:[],
            lengthMenu: [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
            oLanguage:{
                sLoadingRecords:'Silakan Tunggu',
                sProcessing:"<img src='assets/loader/cubeloader2.2.gif' width='50' height='50'> Harap tunggu",
                sZeroRecords:"Data Tidak Ditemukan",
                sSearch : "Pencarian :  _INPUT_",
                sLengthMenu: "<i class='btn btn-primary btn-sm fa fa-folder-open-o'></i><span class='btn btn-default btn-xs'>Menampilkan _MENU_ penjualan debit</span>",
                sInfo: "<span class='btn btn-sm'>Menampilkan (_START_ sampai _END_) dari _TOTAL_ penjualan debit</span>",
                sInfoEmpty: "<span class='btn btn-danger btn-sm'>Tidak ada penjualan debit untuk ditampilkan</span>",
                sInfoFiltered: "<span class='btn btn-default btn-sm text-primary'>(disaring dari _MAX_ total penjualan debit)</span>"
            },

            ajax: {
                url: "<?php echo site_url('laporan_penjualan_toko/ajax_list_debit')?>",
                type: "POST",
                data : {'kode_toko' : $('#kode_toko').val(),
                        'tanggal_filter' : tanggal_laporan,
                        'jenis_perintah' : jenis_perintah,
                        'status_penjualan' : $('#status_penjualan').val()}
            },

            columnDefs: [
                            {
                                "targets": [ -1 ],
                                "orderable": false,
                            },
                          ],
        });

        <?php if($this->session->userdata('usergroup_name') == 'Admin' or
                 $this->session->userdata('usergroup_name') == 'Super Admin'){ ?>
            ambil_total(tanggal_laporan);
        <?php } ?>
    }

    function tampilkan_data_perbarang(tanggal_laporan)
    {
        jenis_perintah  = "!=";

        tpbu.clear();
        tpbu.destroy();
        tpbu = $('#TableLaporanPerbarangUmum').DataTable({
            <?php if($this->session->userdata('usergroup_name') == 'Kepala Toko'){ ?>
                dom : "Blftpi",
            <?php } ?>

            buttons: [
                        {
                            extend: "copy",
                            text: "<i class='fa fa-files-o text-primary'></i>",
                            titleAttr: "Salin data ke clipboard"
                        },
                        {
                            extend: "excel",
                            text: "<i class='fa fa-file-excel-o text-success'></i>",
                            titleAttr: "Excel"
                        },
                        {
                            extend: "pdf",
                            text: "<i class='fa fa-file-pdf-o text-danger'></i>",
                            titleAttr: "PDF"
                        },
                        {
                            extend: "print",
                            text: "<i class='fa fa-print'></i>",
                            titleAttr: "Print"
                        }
                     ],
            stateSave: true,
            ordering: true,
            bFilter:true,
            processing:true,
            serverSide:true,
            order:[],
            lengthMenu: [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
            oLanguage:{
                sLoadingRecords:'Silakan Tunggu',
                sProcessing:"<img src='assets/loader/cubeloader2.2.gif' width='50' height='50'> Harap tunggu",
                sZeroRecords:"Data Tidak Ditemukan",
                sSearch : "Pencarian :  _INPUT_",
                sLengthMenu: "<i class='btn btn-primary btn-sm fa fa-folder-open-o'></i><span class='btn btn-default btn-xs'>Menampilkan _MENU_ penjualan perbarang</span>",
                sInfo: "<span class='btn btn-sm'>Menampilkan (_START_ sampai _END_) dari _TOTAL_ penjualan perbarang</span>",
                sInfoEmpty: "<span class='btn btn-danger btn-sm'>Tidak ada penjualan perbarang untuk ditampilkan</span>",
                sInfoFiltered: "<span class='btn btn-default btn-sm text-primary'>(disaring dari _MAX_ total penjualan perbarang)</span>"
            },

            ajax: {
                url: "<?php echo site_url('laporan_penjualan_toko/ajax_list_perbarang')?>",
                type: "POST",
                data : {'kode_toko' : $('#kode_toko').val(),
                        'tanggal_filter' : tanggal_laporan,
                        'jenis_perintah' : jenis_perintah,
                        'status_penjualan' : $('#status_penjualan').val()}
            },

            columnDefs: [
                            {
                                "targets": [ -1 ],
                                "orderable": false,
                            },
                          ],
        });

        <?php if($this->session->userdata('usergroup_name') == 'Admin' or
                 $this->session->userdata('usergroup_name') == 'Super Admin'){ ?>
            ambil_total(tanggal_laporan);
        <?php } ?>
    }

    function ambil_total(tanggal_laporan){
        $.ajax({
            url: "<?php echo site_url('laporan_penjualan_toko/ambil_total'); ?>",
            type: "POST",
            cache: false,
            data: 'tanggal_filter=' + tanggal_laporan +
                  '&kode_toko=' + $('#kode_toko').val(),
            dataType:'json',
            success: function(data){
                if(data.status == 1){
                    $('#TotalTransaksiMenunggu').html(data.total_menunggu);
                    $('#TotalTransaksiDisiapkan').html(data.total_disiapkan);
                    $('#TotalTransaksiDikirim').html(data.total_dikirim);
                    $('#TotalDebit').html(data.total_debit);
                    $('#TotalTunai').html(data.total_tunai);
                    $('#TotalUangMasuk').html(data.total_uang_masuk);
                    $('#TotalRetur').html(data.total_retur);
                    $('#TotalPPN').html(data.total_ppn);
                    $('#TotalBiayaLain').html(data.total_biaya_lain);
                    $('#TotalBersih').html(data.total_bersih);
                    $('#TotalToko').html(data.total_toko);
                    $('#TotalOnline').html(data.total_online);
                    <?php if($this->session->userdata('usergroup_name') == 'Super Admin'){ ?>
                        $('#TotalModal').html(data.total_modal);
                        $('#TotalKeuntungan').html(data.total_keuntungan);
                    <?php } ?>
                }else if(data.status == 0){
                    $('#TotalTransaksiMenunggu').html('0');
                    $('#TotalTransaksiDisiapkan').html('0');
                    $('#TotalTransaksiDikirim').html('0');
                    $('#TotalDebit').html('0');
                    $('#TotalTunai').html('0');
                    $('#TotalTunai').html('0');
                    $('#TotalRetur').html('0');
                    $('#TotalPPN').html('0');
                    $('#TotalBiayaLain').html('0');
                    $('#TotalBersih').html('0');
                    $('#TotalModal').html('0');
                    $('#TotalKeuntungan').html('0');
                }
            },
            error : function(hasil){
                $('#JmlTransaksi').html('0');
                $('#TotalDebit').html('0');
                $('#TotalTunai').html('0');
                $('#TotalTunai').html('0');
                $('#TotalRetur').html('0');
                $('#TotalPPN').html('0');
                $('#TotalBiayaLain').html('0');
                $('#TotalBersih').html('0');
            }
        });
    }

    function refresh_data(tanggal_laporan){
        if($('#kode_toko').val() == ''){
            swal({
                title: 'Upsss!',
                text: 'Harap pilih toko terlebih dahulu',
                type: 'info',
                confirmButtonClass: 'btn-primary btn-md waves-effect waves-light',
                confirmButtonText: "Ok"
            }, function(isConfirm){
                setTimeout(function(){ 
                    $('#kode_toko').focus();
                }, 100);
            });
            return false;
        }

        if(id_tab == 1){
            if(id_tab_umum == 1){
                tampilkan_data_induk(tanggal_laporan);
            }else if(id_tab_umum == 2){
                tampilkan_data_detail(tanggal_laporan);
            }else if(id_tab_umum == 3){
                tampilkan_data_batal(tanggal_laporan);
            }else if(id_tab_umum == 4){
                tampilkan_data_debit(tanggal_laporan);
            }else if(id_tab_umum == 5){
                tampilkan_data_perbarang(tanggal_laporan);
            }
        }
    }

    function tampilkan_data(tanggal_laporan){
        if($('#kode_toko').val() == ''){
            swal({
                title: 'Upsss!',
                text: 'Harap pilih toko terlebih dahulu',
                type: 'info',
                confirmButtonClass: 'btn-primary btn-md waves-effect waves-light',
                confirmButtonText: "Ok"
            }, function(isConfirm){
                setTimeout(function(){ 
                    $('#kode_toko').focus();
                }, 100);
            });
            return false;
        }
        
        if(id_tab == 1){
            if(id_tab_umum == 1){
                tampilkan_data_induk(tanggal_laporan);
            }else if(id_tab_umum == 2){
                tampilkan_data_detail(tanggal_laporan);
            }else if(id_tab_umum == 3){
                tampilkan_data_batal(tanggal_laporan);
            }else if(id_tab_umum == 4){
                tampilkan_data_debit(tanggal_laporan);
            }else if(id_tab_umum == 5){
                tampilkan_data_perbarang(tanggal_laporan);
            }
        }
    }

    function filter_data(){
        $('#status_penjualan').removeClass('btn-default');
        $('#status_penjualan').removeClass('btn-danger');
        $('#status_penjualan').removeClass('btn-warning');
        $('#status_penjualan').removeClass('btn-primary');
        $('#status_penjualan').removeClass('btn-success');

        if($('#status_penjualan').val() == 'ALL'){
            $('#status_penjualan').addClass('btn-default');
        }else if($('#status_penjualan').val() == 'MENUNGGU'){
            $('#status_penjualan').addClass('btn-danger');
            $('#status_penjualan').next().removeClass('text-dark');
            $('#status_penjualan').next().addClass('text-warning');
        }else if($('#status_penjualan').val() == 'SELESAI'){
            $('#status_penjualan').addClass('btn-success');
        }

        tanggal_laporan = $('#tanggal_filter').html();
        if(tanggal_laporan != ''){
            tampilkan_data(tanggal_laporan);
        }
    }

    $(document).on('DOMSubtreeModified', '#tanggal_filter', function(){
        tanggal_laporan = $('#tanggal_filter').html();
        if(tanggal_laporan != ''){
            if($('#kode_toko').val() !== ''){
                tampilkan_data(tanggal_laporan);
            }
        }
    });

    $(document).on('click', '#refresh_data', function(){
        tanggal_laporan = $('#tanggal_filter').html();
        refresh_data(tanggal_laporan);
    });

    $(document).on('click', 'button#tambah_transaksi', function(){
        window.open("penjualan",'_blank');
    });

    function edit_penjualan(id_penjualan_m)
    {
        if((id_penjualan_m) !== ''){
            var FrmData = "&id="+id_penjualan_m;
            window.open("penjualan/transaksi/?" + FrmData,'_blank');
        }else{
            $('.modal-dialog').removeClass('modal-lg');
            $('.modal-dialog').addClass('modal-sm');
            $('#ModalHeader').html('Oops !');
            $('#ModalContent').html('No. Penjualan tidak dikenal');
            $('#ModalFooter').html("<button type='button' class='btn btn-primary' data-dismiss='modal' autofocus>Ok</button>");
            $('#ModalGue').modal('show');
        }
    }

    function verifikasi_hapus_penjualan($id_penjualan_m)
    {
        $.ajax({
            url : "<?php echo site_url('laporan_penjualan_toko/ajax_verifikasi_hapus_transaksi'); ?>",
            type: "POST",
            cache: false,
            data: "id_penjualan_m=" + $id_penjualan_m,
            dataType: "JSON",
            success: function(data){
                if(data.status == 1){
                    $('.modal-dialog').removeClass('modal-lg');
                    $('.modal-dialog').addClass('modal-sm');
                    $('#ModalHeader').html('Informasi Hapus Penjualan');
                    $('#ModalContent').html(data.pesan);
                    $('#ModalFooter').html(data.footer);
                    $('#ModalGue').modal('show');
                }else if(data.status == 0){
                    swal({
                        title: data.info_pesan, 
                        text: data.pesan, 
                        type: data.tipe_pesan, 
                        confirmButtonClass: 'btn-danger btn-md waves-effect waves-light',
                        confirmButtonText: "Ok"
                    }, function(isConfirm){
                        tanggal_laporan = $('#tanggal_filter').html();
                        tampilkan_data(tanggal_laporan);
                    });    
                }
            }, 
            error: function (jqXHR, textStatus, errorThrown){
                swal({
                    title: "Oops !", 
                    text: "Data penjualan gagal ditampilkan.", 
                    type: "error", 
                    confirmButtonClass: 'btn-danger btn-md waves-effect waves-light',
                    confirmButtonText: "Ok"
                }, function (isConfirm){
                    tanggal_laporan = $('#tanggal_filter').html();
                    tampilkan_data(tanggal_laporan);
                });
            }
        });
    }

    function hapus_penjualan($id_penjualan_m)
    {
        var FormData = "id_penjualan_m="+ $id_penjualan_m;
            FormData += "&keterangan_batal="+ encodeURI($('#keterangan_batal').val());

        $.ajax({
            url : "<?php echo site_url('laporan_penjualan_toko/ajax_hapus_transaksi'); ?>",
            type: "POST",
            cache: false,
            data: FormData,
            dataType: "JSON",
            success: function(data){
                if(data.status == 1){
                    swal({
                        title: data.info_pesan, 
                        text: data.pesan, 
                        type: 'success', 
                        confirmButtonClass: 'btn-success btn-md waves-effect waves-light',
                        confirmButtonText: "Ok"
                    }, function (isConfirm){
                        tanggal_laporan = $('#tanggal_filter').html();
                        tampilkan_data(tanggal_laporan);
                    });                    
                }else if(data.status == 0){
                    swal({
                        title: data.info_pesan, 
                        text: data.pesan, 
                        type: 'error', 
                        confirmButtonClass: 'btn-danger btn-md waves-effect waves-light',
                        confirmButtonText: "Ok"
                    }, function (isConfirm){
                        tanggal_laporan = $('#tanggal_filter').html();
                        tampilkan_data(tanggal_laporan);
                    });
                }
            },
            error: function (jqXHR, textStatus, errorThrown){
                swal({
                    title: "Oops !", 
                    text: "Data penjualan gagal dihapus.", 
                    type: "error", 
                    confirmButtonClass: 'btn-danger btn-md waves-effect waves-light',
                    confirmButtonText: "Ok"
                }, function (isConfirm){
                    tanggal_laporan = $('#tanggal_filter').html();
                    tampilkan_data_induk(tanggal_laporan);
                });
            }
        });
    }

    function selesai_transaksi(id_penjualan_m, no_penjualan, nama_customer)
    {
        var pesan   = "No. Penjualan : " + no_penjualan + ", " +
                      "Nama Customer : " + nama_customer;
        swal({
            title: "Yakin ingin menyelesaikan transaksi ini ?",
            text: pesan,
            type: "info",
            showCancelButton: true,
            confirmButtonClass: 'btn-primary btn-md waves-effect waves-light',
            confirmButtonText: "Iya, saya yakin",
            cancelButtonText: "Batal",
        },function (isConfirm){
            if(isConfirm){
                $.ajax({
                    url : "<?php echo site_url('laporan_penjualan_toko/ajax_selesai_transaksi'); ?>",
                    type: "POST",
                    cache: false,
                    data: "id_penjualan_m=" + id_penjualan_m,
                    dataType: "JSON",
                    success: function(data){
                        if(data.status == 1){
                            tanggal_laporan = $('#tanggal_filter').html();
                            refresh_data(tanggal_laporan);

                            swal({
                                title: data.info_pesan,
                                text: data.pesan,
                                type: "success",
                                showCancelButton: true,
                                confirmButtonText: "Iya, saya yakin",
                                cancelButtonText: "Tidak, nantik saja"
                            }, function(){
                                var FormData        = "&id_penjualan_m=" + id_penjualan_m;
                                var url_cetak_fix   = "<?php echo site_url('faktur/'); ?>" + data.url_cetak + "/?";
                                window.open(url_cetak_fix + FormData,'_blank');
                            });
                        }else if(data.status == 0){
                            swal({
                                title: data.info_pesan,
                                text: data.pesan,
                                type: data.tipe_pesan,
                                confirmButtonClass: 'btn-danger btn-md waves-effect waves-light',
                                confirmButtonText: "Ok"
                            }, function(isConfirm){
                                window.location.href = 'laporan_penjualan';
                            });
                        }
                    }, error: function (jqXHR, textStatus, errorThrown){
                        swal({
                            title: "Oops !",
                            text: "Data penjualan gagal ditampilkan.",
                            type: "error",
                            confirmButtonClass: 'btn-danger btn-md waves-effect waves-light',
                            confirmButtonText: "Ok"
                        }, function (isConfirm){
                            window.location.href = 'laporan_penjualan';
                        });
                    }
                });
            }
        });
    }

    function cetak_faktur(id_penjualan_m, pesan, url_cetak)
    {
        if((id_penjualan_m) !== ''){
            swal({
                title: pesan,
                type: "info",
                showCancelButton: true,
                confirmButtonClass: 'btn-primary btn-md waves-effect waves-light',
                confirmButtonText: "Iya, saya yakin",
                cancelButtonText: "Batal",
            },function (isConfirm){
                if(isConfirm){
                    var FormData        = "&id_penjualan_m=" + id_penjualan_m;
                    var url_cetak_fix   = "<?php echo site_url('faktur/'); ?>" + url_cetak + "/?";
                    window.open(url_cetak_fix + FormData,'_blank');
                }
            });
        }else{
            swal({
                title: "Oops !",
                text: "No. penjualan tidak terdaftar.",
                type: "error",
                confirmButtonClass: 'btn-danger btn-md waves-effect waves-light',
                confirmButtonText: "Ok"
            });
        }
    }
</script>
<!-- Akhir Script CRUD -->
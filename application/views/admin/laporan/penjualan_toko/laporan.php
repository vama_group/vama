<div class="content">
	<div class="container">
	<br>		
		<div class="row">
        	<div class="col-sm-12">
	        	<div class="row">
		            <div class="col-sm-12">
			        	<!-- Awal daftar transaksi dan catatan transaksi --> 
		            	<div class="card-box table-responsive">
		            		<!-- Keterangan transaksi dan tombol tambah dan refersh -->
							<div class="row">
								<!-- Keterangan, tombol tambah dan refresh -->
								<div class='col-sm-7'>
									<h4 class="text-success">
										<i class='fa fa-cart-plus'></i> 
										LAPORAN PENJUALAN TOKO
									</h4>
									<select id="kode_toko" name="kode_toko" class="btn btn-rounded btn-xs btn-default">
	                                    <option value="" class="text-dark ">PILIH TOKO</option>
	                                    <?php foreach ($data_toko AS $dt){ ?>
	                                        <option value="<?php echo $dt->kode_customer_pusat ?>" class="text-dark">
	                                            <?php echo $dt->nama_customer_pusat ?>
	                                        </option>
	                                    <?php } ?>
	                                </select>
                            		<button class="btn btn-xs btn-primary btn-rounded" id="refresh_data">
                            			<i class="glyphicon glyphicon-refresh"></i> 
                            			REFRESH
                            		</button>
								</div>
								
								<!-- Pemilihan status penjualan -->
								<div class="col-sm-2">
									<label>Status penjualan</label></br>
		                            <select id="status_penjualan" name="status_penjualan" class="btn btn-rounded btn-block btn-xs btn-default"
		                            onchange="filter_data()">
		                            </select>	
								</div>

								<!-- Pemilihan tanggal awal dan akhir -->
								<div class="col-sm-3">
									<div class="form-group">
                                        <label>Pilih tanggal awal dan akhir</label>
                                        <div id="reportrange" class="pull-right form-control">
                                            <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>
                                            <span id="tanggal_filter" name="tanggal_filter"></span>
                                        </div>
                                    </div>
								</div>
							</div>
							<!-- <hr> -->

							<?php if($this->session->userdata('usergroup_name') == 'Admin' or 
									 $this->session->userdata('usergroup_name') == 'Super Admin'){ ?>
								<!-- Awal daftar total -->
								<div class="row">
									<div class="col-md-3">
										<table class='table tablesaw m-t-20 table m-b-0 tablesaw-stack card-box' id='Total1'>
											<thead>
											</thead>

											<tbody>
												<tr>
													<td><small>Total Tunai</small></td>
													<td class="text-left">Rp.
														<span id='TotalTunai' name='TotalTunai' class="text-dark pull-right">0</span>
													</td>
												</tr>
												<tr>
													<td><small>Total Debit</small></td>
													<td class="text-left">Rp.
														<span id='TotalDebit' name='TotalDebit' class="text-dark pull-right">0</span>
													</td>
												</tr>
												<tr>
													<td><small>Total Uang Masuk
													</small></td>
													<td class="text-left">Rp.
														<span id='TotalUangMasuk' name='TotalUangMasuk' class="text-dark pull-right">0</span>
													</td>
												</tr>
											</tbody>
										</table>
									</div>

									<div class="col-md-3">
										<table class='table tablesaw m-t-20 table m-b-0 tablesaw-stack card-box' id='Total2'>
											<thead>
											</thead>

											<tbody>
												<tr>
													<td><small>Total PPN</small></td>
													<td class="text-left">Rp.
														<span id='TotalPPN' name='TotalPPN' class="text-danger pull-right">0</span>
													</td>
												</tr>
												<tr>
													<td><small>Total Retur</small></td>
													<td class="text-left">Rp.
														<span id='TotalRetur' name="TotalRetur" class="text-danger pull-right">0</span>
													</td>
												</tr>
												<tr>
													<td><small>Total Biaya Lain</small></td>
													<td class="text-left">Rp.
														<span id='TotalBiayaLain' name='TotalBiayaLain' class="text-danger pull-right">0</span>
													</td>
												</tr>
											</tbody>
										</table>
									</div>

									<div class="col-md-3">
										<table class='table tablesaw m-t-20 table m-b-0 tablesaw-stack card-box' id='Total3'>
											<thead>
											</thead>

											<tbody>
												<tr>
													<td><small>Total Transaksi Menunggu</br></small></td>
													<td>Rp. 
														<span id="TotalTransaksiMenunggu" name="TotalTransaksiMenunggu" 
														class="text-danger pull-right">0</span> 
				                        			</td>
												</tr>
												<tr>
													<td><small>Total Transaksi Disiapkan</br></small></td>
													<td>Rp. 
														<span id="TotalTransaksiDisiapkan" name="TotalTransaksiMenunggu" 
														class="text-warning pull-right">0</span> 
				                        			</td>
												</tr>
												<tr id="total_dikirim" class="hidden">
													<td><small>Total Transaksi Dikirim</br></small></td>
													<td>Rp. 
														<span id="TotalTransaksiDikirim" name="TotalTransaksiMenunggu" 
														class="text-primary pull-right">0</span> 
				                        			</td>
												</tr>
												<tr>
													<td><small>Total Bersih Uang Masuk</br></small></td>
													<td class="text-left">Rp.
														<span id='TotalBersih' name='TotalBersih' class="text-success pull-right">0</span>
													</td>
												</tr>
											</tbody>
										</table>
									</div>

									
									<div class="col-md-3" id="KotakBagian">
										<table class='table tablesaw m-t-20 table m-b-0 tablesaw-stack card-box' id='Total4'>
											<thead></thead>
											<tbody>
												<tr>
													<td><small>Penjualan Toko</small></td>
													<td class="text-left">Rp.
														<span id='TotalToko' name='TotalToko' class="text-dark pull-right">0</span>
													</td>
												</tr>
												<tr>
													<td><small>Penjualan Online</small></td>
				                        			<td class="text-left">Rp.
														<span id='TotalOnline' name='TotalOnline' class="text-success pull-right">0</span>
													</td>
												</tr>
												<?php if($this->session->userdata('usergroup_name') == 'Super Admin'){ ?>
												<tr id="total_modal" class="hidden">
													<td><small>Total Modal</small></td>
													<td class="text-left">Rp.
														<span id='TotalModal' name='TotalModal' class="text-dark pull-right">0</span>
													</td>
												</tr>
												<tr id="total_keuntungan" class="hidden">
													<td><small>Total Keuntungan</small></td>
				                        			<td class="text-left">Rp.
														<span id='TotalKeuntungan' name='TotalKeuntungan' class="text-success pull-right">0</span>
													</td>
												</tr>
												<?php } ?>
											</tbody>
										</table>
									</div>
									
								</div>
								<!-- Akhir daftar total -->
							<?php } ?>
							<hr>

							<!-- Awal laporan umum -->
	                        <div class="tab-pane active" id="tab_penjualan_umum">
								<!-- Awal induk tab - umum -->
			                    <ul class="nav nav-tabs tabs-bordered" id="tab_list_penjualan">
			                        <li class="active">
			                            <a href="#tab_induk_penjualan_umum" id="tab_induk_umum" data-toggle="tab" aria-expanded="true">
			                                <span class="visible-xs"><i class="fa fa-check-square"></i></span>
			                                <span class="hidden-xs" id="judul_tab_induk">Penjualan Induk</span>
			                            </a>
			                        </li>
			                        <li class="">
			                            <a href="#tab_detail_penjualan_umum" id="tab_detail_umum" data-toggle="tab" aria-expanded="true">
			                                <span class="visible-xs"><i class="typcn typcn-home"></i></span>
			                                <span class="hidden-xs" id="judul_tab_batal">Penjualan Detail</span>
			                            </a>
			                        </li>
			                        <li class="">
			                            <a href="#tab_batal_penjualan_umum" id="tab_batal_umum" data-toggle="tab" aria-expanded="true">
			                                <span class="visible-xs"><i class="typcn typcn-home"></i></span>
			                                <span class="hidden-xs" id="judul_tab_batal">Penjualan Batal</span>
			                            </a>
			                        </li>
			                        <li class="">
			                            <a href="#tab_debit_penjualan_umum" id="tab_debit_umum" data-toggle="tab" aria-expanded="true">
			                                <span class="visible-xs"><i class="typcn typcn-home"></i></span>
			                                <span class="hidden-xs" id="judul_tab_debit">Penjualan Debit</span>
			                            </a>
			                        </li>
			                        <li class="">
			                            <a href="#tab_penjualan_perbarang_umum" id="tab_perbarang_umum" data-toggle="tab" aria-expanded="true">
			                                <span class="visible-xs"><i class="typcn typcn-home"></i></span>
			                                <span class="hidden-xs" id="judul_tab_perbarang">Penjualan Perbarang</span>
			                            </a>
			                        </li>
			                    </ul>
			                    <!-- Akhir induk tab - umum -->

			                    <!-- Awal isi tab umum -->
			                    <div class="tab-content">
									<!-- Laporan induk umum -->
			                        <div class="tab-pane active" id="tab_induk_penjualan_umum">	
										<table id='TableLaporanIndukUmum' class="table table-condensed table-striped table-hover dt-responsive nowrap" cellspacing="0" width="100%">
											<thead class="input-sm">
												<tr class="text-dark">
													<th>#</th>
													<!-- <th>Tombol</th> -->
													<th>No. Penjualan</th>
													<th>No. Retur</th>
													<th>Status</th>
													<th>Tanggal</th>
													<th>Nama Customer</th>
													<th>Jml Barang</th>

													<th>Tunai</th>
													<th>Debit</th>
													<th>Total</th>
													<th>Total Retur</th>
													<th>Total Bersih</th>
													
													<th>Catatan</th>
													<th>Pegawai Save</th>
													<th>Tanggal Save</th>
													<th>Pegawai Edit</th>
													<th>Tanggal Edit</th>
												</tr>
											</thead>

											<tbody class="input-sm"></tbody>
										</table>
									</div>

									<!-- Laporan detail umum -->
									<div class="tab-pane" id="tab_detail_penjualan_umum">
										<table id='TableLaporanDetailUmum' class="table table-condensed table-striped table-hover nowrap" cellspacing="0" width="100%">
											<thead class="input-sm">
												<tr class="text-dark">
													<th>#</th>
													<!-- <th>Tombol</th> -->
													<th>No. Penjualan</th>
													<th>No. Retur</th>
													<th>Status</th>
													<th>Tanggal</th>
													<th>Nama Customer</th>
													
													<th>Nama Barang</th>
													<th>Hrg Satuan</th>
													<th>Discount</th>
													<th>Hrg Bersih</th>
													
													<th>Jml Beli</th>
													<th>Jml Retur</th>
													<th>Jml Bersih</th>
													
													<th>Subtotal Beli</th>
													<th>Subtotal Retur</th>
													<th>Subtotal Bersih</th>

													<th>Pegawai Save</th>
													<th>Tanggal Save</th>
													<th>Pegawai Edit</th>
													<th>Tanggal Edit</th>
												</tr>
											</thead>

											<tbody class="input-sm"></tbody>
										</table>
									</div>

									<!-- Laporan detail batal umum -->
									<div class="tab-pane" id="tab_batal_penjualan_umum">
										<table id='TableLaporanBatalUmum' class="table table-condensed table-striped table-hover dt-responsive nowrap nowrap" cellspacing="0" width="100%">
											<thead class="input-sm">
												<tr class="text-dark">
													<th>#</th>
													<th>No. Penjualan</th>
													<th>Nama Customer</th>
													
													<th>Nama Barang</th>
													<th>Hrg Satuan</th>
													<th>Discount</th>
													<th>Hrg Bersih</th>
													
													<th>Jml Beli</th>														
													<th>Subtotal</th>
													<th>Keterangan Batal</th>

													<th>Pegawai Batal</th>
													<th>Tanggal Batal</th>
													<th>Pegawai Save</th>
													<th>Tanggal Save</th>
													<th>Pegawai Edit</th>
													<th>Tanggal Edit</th>
												</tr>
											</thead>

											<tbody class="input-sm"></tbody>
										</table>
									</div>

									<!-- Laporan debit umum -->
									<div class="tab-pane" id="tab_debit_penjualan_umum">
										<table id='TableLaporanDebitUmum' class="table table-condensed table-striped table-hover dt-responsive nowrap" cellspacing="0" width="100%">
											<thead class="input-sm">
												<tr class="text-dark">
													<th>#</th>
													<th>No. Penjualan</th>
													<th>Tanggal</th>
													<th>Nama Customer</th>
													
													<th>No. Kartu EDC</th>
													<th>Nama Bank</th>
													<th>No. Kartu Customer</th>
													<th>Jumlah Debit</th>
													
													<th>Pegawai Save</th>
													<th>Tanggal Save</th>
													<th>Pegawai Edit</th>
													<th>Tanggal Edit</th>
												</tr>
											</thead>

											<tbody class="input-sm"></tbody>
										</table>
									</div>

									<!-- Laporan perbarang umum -->
									<div class="tab-pane" id="tab_penjualan_perbarang_umum">
										<table id='TableLaporanPerbarangUmum' class="table table-condensed table-striped table-hover dt-responsive nowrap" cellspacing="0" width="100%">
											<thead class="input-sm">
												<tr class="text-dark">
													<th>#</th>
													<th>SKU</th>
													<th>Nama Barang</th>
													<th>Transaksi Jual</th>
													<th>Transaksi Retur</th>
													<th>Transaksi Fix</th>
													
													<th>Qty Jual</th>
													<th>Qty Retur</th>
													<th>Qty Fix</th>

													<th>Total Jual</th>
													<th>Total Retur</th>
													<th>Total Fix</th>
												</tr>
											</thead>

											<tbody class="input-sm"></tbody>
										</table>
									</div>
								</div>
								<!-- Akhir isi tab umum -->
							</div>
							<!-- Akhir laporan umum -->
						</div>
				        <!-- Akhir daftar transaksi -->
		            </div>
				</div>
            </div>
        </div>
	</div>
</div>

<div class="modal" id="ModalGue" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class='fa fa-times-circle'></i></button>
				<h4 class="modal-title" id="ModalHeader"></h4>
			</div>
			<div class="modal-body" id="ModalContent"></div>
			<div class="modal-footer" id="ModalFooter"></div>
		</div>
	</div>
</div>

<script>
$('#ModalGue').on('hide.bs.modal', function () {
   setTimeout(function(){ 
        $('#ModalHeader, #ModalContent, #ModalFooter').html('');
   }, 500);
});
</script>
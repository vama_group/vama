<!-- Awal CSS -->
<link href="assets/plugin/zircos/plugins/datatables/jquery.dataTables.min.css" rel="stylesheet" type="text/css"/>
<link href="assets/plugin/zircos/plugins/datatables/responsive.bootstrap.min.css" rel="stylesheet" type="text/css"/>
<link href="assets/plugin/zircos/plugins/datatables/dataTables.bootstrap.min.css" rel="stylesheet" type="text/css"/>
<link href="assets/plugin/zircos/material-design/assets/css/tambahan.css" rel="stylesheet" type="text/css"/>
<link href="assets/plugin/zircos/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css" rel="stylesheet">
<link href="assets/plugin/zircos/plugins/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
<!-- Akhir CSS -->

<!-- Awal JS -->
<script src="assets/plugin/zircos/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="assets/plugin/zircos/plugins/datatables/dataTables.bootstrap.js"></script>

<script src="assets/plugin/zircos/material-design/assets/js/detect.js"></script>
<script src="assets/plugin/zircos/material-design/assets/js/jquery.slimscroll.js"></script>
<script src="assets/plugin/zircos/plugins/datatables/dataTables.responsive.min.js"></script>
<script src="assets/plugin/zircos/plugins/datatables/responsive.bootstrap.min.js"></script>
<script src="assets/plugin/zircos/material-design/assets/pages/jquery.datatables.init.js"></script>
<!-- Akhir JS -->

<!-- Date rangepicker -->
<script src="assets/plugin/zircos/plugins/moment/moment.js"></script>
<script src="assets/plugin/zircos/plugins/timepicker/bootstrap-timepicker.js"></script>
<script src="assets/plugin/zircos/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js"></script>
<script src="assets/plugin/zircos/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
<script src="assets/plugin/zircos/plugins/clockpicker/js/bootstrap-clockpicker.min.js"></script>
<script src="assets/plugin/zircos/plugins/bootstrap-daterangepicker/daterangepicker.js"></script>
<script src="assets/plugin/zircos/material-design/assets/pages/jquery.form-pickers.init.js"></script>

<!-- Awal Sweet-Alert  -->
<link href="assets/plugin/zircos/plugins/bootstrap-sweetalert/sweet-alert.css" rel="stylesheet" type="text/css">
<script src="assets/plugin/zircos/plugins/bootstrap-sweetalert/sweet-alert.min.js"></script>
<!-- Akhir Sweet-Alert  -->

<script>
    var id_tab       = '1';
    var table_induk  = $('#TableLaporanInduk').DataTable();
    var table_detail = $('#TableLaporanDetail').DataTable();
    var table_batal  = $('#TableLaporanBatal').DataTable();

    $(document).ready(function(){
        $('#navbar').removeClass('navbar-default');
        $('#navbar').addClass('navbar-hijau');

        $("#wrapper").removeClass('forced');
        $("#wrapper").addClass('forced enlarged');
        $('#JudulHalaman').html('Laporan Retur Penjualan - VAMA');
    });

    function reload_table(tanggal_laporan)
    {
        if(id_tab == '1'){
            tampilkan_data_induk(tanggal_laporan);
        }else if(id_tab == '2'){
            tampilkan_data_detail(tanggal_laporan);
        }else if(id_tab == '3'){
            tampilkan_data_batal(tanggal_laporan);
        }

        <?php if($this->session->userdata('usergroup_name') == 'Super Admin'){ ?>
            ambil_total(tanggal_laporan);
        <?php }else if($this->session->userdata('usergroup_name') == 'Admin'){ ?>
            ambil_total(tanggal_laporan);
        <?php } ?>  
    }

    function refresh_table(){
        if(id_tab == '1'){
            table_induk.ajax.reload();
        }else if(id_tab == '2'){
            table_detail.ajax.reload();
        }else if(id_tab == '3'){
            table_batal.ajax.reload();
        }

        <?php if($this->session->userdata('usergroup_name') == 'Super Admin'){ ?>
            ambil_total(tanggal_laporan);
        <?php }else if($this->session->userdata('usergroup_name') == 'Admin'){ ?>
            ambil_total(tanggal_laporan);
        <?php } ?>
    }

    function tampilkan_data(tanggal_laporan){
        if(id_tab == '1'){
            tampilkan_data_induk(tanggal_laporan);
        }else if(id_tab == '2'){
            tampilkan_data_detail(tanggal_laporan);
        }else if(id_tab == '3'){
            tampilkan_data_batal(tanggal_laporan);
        }

        <?php if($this->session->userdata('usergroup_name') == 'Super Admin'){ ?>
            ambil_total(tanggal_laporan);
        <?php }else if($this->session->userdata('usergroup_name') == 'Admin'){ ?>
            ambil_total(tanggal_laporan);
        <?php } ?>
    }

    function tampilkan_data_induk(tanggal_laporan){
        table_induk.clear();table_induk.destroy();
        table_induk = $('#TableLaporanInduk').DataTable({ 
            <?php if($this->session->userdata('usergroup_name') == 'Super Admin'){ ?>
                dom : "Blftpi",
            <?php } ?>

            buttons: [
                        {
                            extend: "copy",
                            text: "<i class='fa fa-files-o text-primary'></i>",
                            titleAttr: "Salin data ke clipboard"
                        }, 
                        {
                            extend: "excel",
                            text: "<i class='fa fa-file-excel-o text-success'></i>",
                            titleAttr: "Excel"
                        }, 
                        {
                            extend: "pdf",
                            text: "<i class='fa fa-file-pdf-o text-danger'></i>",
                            titleAttr: "PDF"
                        }, 
                        {
                            extend: "print",
                            text: "<i class='fa fa-print'></i>",
                            titleAttr: "Print"
                        }
                     ],
            ordering: true,
            bFilter:true,
            processing:true,
            serverSide:true,
            order:[],
            lengthMenu: [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
            oLanguage:{
                sLoadingRecords:'Silakan Tunggu',
                sProcessing:"<img src='assets/loader/cubeloader2.2.gif' width='50' height='50'> Harap tunggu",
                sZeroRecords:"Data Tidak Ditemukan",
                sSearch : "Pencarian :  _INPUT_",
                sLengthMenu: "<i class='btn btn-primary btn-sm fa fa-folder-open-o'></i><span class='btn btn-default btn-xs'>Menampilkan _MENU_ retur penjualan induk</span>",
                sInfo: "<span class='btn btn-sm'>Menampilkan (_START_ sampai _END_) dari _TOTAL_ retur penjualan induk</span>",
                sInfoEmpty: "Tidak ada retur penjualan induk untuk ditampilkan",
                sInfoFiltered: " - (disaring dari _MAX_ total retur penjualan induk)"
            },

            ajax: {
                url: "<?php echo site_url('laporan_retur_penjualan_pusat/ajax_list')?>",
                type: "POST",
                data : {'tanggal_filter' : tanggal_laporan}
            },

            columnDefs: [
                            { 
                                "targets": [ -1 ],
                                "orderable": false,
                            },
                          ],
        });        
    }

    function tampilkan_data_detail(tanggal_laporan){
        table_detail.clear();table_detail.destroy();
        table_detail = $('#TableLaporanDetail').DataTable({ 
            <?php if($this->session->userdata('usergroup_name') == 'Super Admin'){ ?>
                dom : "Blftpi",
            <?php } ?>

            buttons: [
                        {
                            extend: "copy",
                            text: "<i class='fa fa-files-o text-primary'></i>",
                            titleAttr: "Salin data ke clipboard"
                        }, 
                        {
                            extend: "excel",
                            text: "<i class='fa fa-file-excel-o text-success'></i>",
                            titleAttr: "Excel"
                        }, 
                        {
                            extend: "pdf",
                            text: "<i class='fa fa-file-pdf-o text-danger'></i>",
                            titleAttr: "PDF"
                        }, 
                        {
                            extend: "print",
                            text: "<i class='fa fa-print'></i>",
                            titleAttr: "Print"
                        }
                     ],
            ordering: true,
            bFilter:true,
            processing:true,
            serverSide:true,
            order:[],
            lengthMenu: [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
            oLanguage:{
                sLoadingRecords:'Silakan Tunggu',
                sProcessing:"<img src='assets/loader/cubeloader2.2.gif' width='50' height='50'> Harap tunggu",
                sZeroRecords:"Data Tidak Ditemukan",
                sSearch : "Pencarian :  _INPUT_",
                sLengthMenu: "<i class='btn btn-primary btn-sm fa fa-folder-open-o'></i><span class='btn btn-default btn-xs'>Menampilkan _MENU_ retur penjualan detail</span>",
                sInfo: "<span class='btn btn-sm'>Menampilkan (_START_ sampai _END_) dari _TOTAL_ retur penjualan detail</span>",
                sInfoEmpty: "Tidak ada retur penjualan detail untuk ditampilkan",
                sInfoFiltered: " - (disaring dari _MAX_ total retur penjualan detail)"
            },

            ajax: {
                url: "<?php echo site_url('laporan_retur_penjualan_pusat/ajax_list_detail')?>",
                type: "POST",
                data : {'tanggal_filter' : tanggal_laporan}
            },

            columnDefs: [   { 
                                "targets": [ -1 ],
                                "orderable": false,
                            },
                        ],
        });        
    }

    function tampilkan_data_batal(tanggal_laporan){
        table_batal.clear();table_batal.destroy();
        table_batal = $('#TableLaporanBatal').DataTable({ 
            <?php if($this->session->userdata('usergroup_name') == 'Super Admin'){ ?>
                dom : "Blftpi",
            <?php } ?>

            buttons: [
                        {
                            extend: "copy",
                            text: "<i class='fa fa-files-o text-primary'></i>",
                            titleAttr: "Salin data ke clipboard"
                        }, 
                        {
                            extend: "excel",
                            text: "<i class='fa fa-file-excel-o text-success'></i>",
                            titleAttr: "Excel"
                        }, 
                        {
                            extend: "pdf",
                            text: "<i class='fa fa-file-pdf-o text-danger'></i>",
                            titleAttr: "PDF"
                        }, 
                        {
                            extend: "print",
                            text: "<i class='fa fa-print'></i>",
                            titleAttr: "Print"
                        }
                     ],
            ordering: true,
            bFilter:true,
            processing:true,
            serverSide:true,
            order:[],
            lengthMenu: [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
            oLanguage:{
                sLoadingRecords:'Silakan Tunggu',
                sProcessing:"<img src='assets/loader/cubeloader2.2.gif' width='50' height='50'> Harap tunggu",
                sZeroRecords:"Data Tidak Ditemukan",
                sSearch : "Pencarian :  _INPUT_",
                sLengthMenu: "<i class='btn btn-primary btn-sm fa fa-folder-open-o'></i><span class='btn btn-default btn-xs'>Menampilkan _MENU_ retur penjualan batal</span>",
                sInfo: "<span class='btn btn-sm'>Menampilkan (_START_ sampai _END_) dari _TOTAL_ retur penjualan batal</span>",
                sInfoEmpty: "Tidak ada retur penjualan batal untuk ditampilkan",
                sInfoFiltered: " - (disaring dari _MAX_ total retur penjualan batal)"
            },

            ajax: {
                url: "<?php echo site_url('laporan_retur_penjualan_pusat/ajax_list_detail_batal')?>",
                type: "POST",
                data : {'tanggal_filter' : tanggal_laporan}
            },

            columnDefs: [   { 
                                "targets": [ -1 ],
                                "orderable": false,
                            },
                        ],
        });        
    }

    function ambil_total(tanggal_laporan){
        $.ajax({
            url: "<?php echo site_url('laporan_retur_penjualan_pusat/ambil_total'); ?>",
            type: "POST",
            cache: false,
            data: 'tanggal_filter=' + tanggal_laporan,
            dataType:'json',
            success: function(data){
                if(data.status == 1){
                    $('#kotak_total').removeAttr('hidden');
                    $('span[name="JmlTransaksi"]').html(data.jml_transaksi+' Transaksi');
                    $('span[name="GrandTotalPotongan"]').html(data.grand_total_potongan);                            
                    $('span[name="GrandTotalRetur"]').html(data.grand_total_retur);
                }else if(data.status == 0){
                    $('#kotak_total').prop('hidden', true);
                    $('span[name="JmlTransaksi"]').html('0');
                    $('span[name="GrandTotalPotongan"]').html('Rp. 0');                            
                    $('span[name="GrandTotalRetur"]').html('Rp. 0');
                }
            },
            error : function(hasil){
                $('#kotak_total').prop('hidden', true);
                $('span[name="JmlTransaksi"]').html('0');
                $('span[name="GrandTotalPotongan"]').html('Rp. 0');                            
                $('span[name="GrandTotalRetur"]').html('Rp. 0');
            }
        });
    }

    $(document).on('click', '#tab_induk', function(){
        id_tab              = '1';
        var tanggal_laporan = $('#tanggal_filter').html();
        reload_table(tanggal_laporan);
    });

    $(document).on('click', '#tab_detail', function(){
        id_tab              = '2';
        var tanggal_laporan = $('#tanggal_filter').html();
        reload_table(tanggal_laporan);
    });

    $(document).on('click', '#tab_batal', function(){
        id_tab              = '3';
        var tanggal_laporan = $('#tanggal_filter').html();
        reload_table(tanggal_laporan);
    });

    $(document).on('DOMSubtreeModified', '#tanggal_filter', function(){
        var tanggal_laporan = $('#tanggal_filter').html();

        if(tanggal_laporan != ''){
            $('#TableLaporan').DataTable().clear();
            $('#TableLaporan').DataTable().destroy();
            reload_table(tanggal_laporan);
        }
    });

    $(document).on('click', '#refresh_data', function(){
        var tanggal_laporan = $('#tanggal_filter').html();
        $('#TableLaporan').DataTable().destroy();
        reload_table(tanggal_laporan);
    });

    $(document).on('click', 'button#tambah_transaksi', function(){
        window.open("retur_penjualan");
    });

    function edit_retur_penjualan(id_retur_penjualan_m)
    {
        if((id_retur_penjualan_m) !== ''){
            var FrmData = "&id="+id_retur_penjualan_m;
            window.open("retur_penjualan/transaksi/?" + FrmData,'_blank');
        }else{
            $('.modal-dialog').removeClass('modal-lg');
            $('.modal-dialog').addClass('modal-sm');
            $('#ModalHeader').html('Oops !');
            $('#ModalContent').html('No. retur penjualan tidak dikenal');
            $('#ModalFooter').html("<button type='button' class='btn btn-primary' data-dismiss='modal' autofocus>Ok</button>");
            $('#ModalGue').modal('show');
        }
    }

    function verifikasi_hapus_transaksi($id_retur_penjualan_m)
    {
        $.ajax({
            url : "<?php echo site_url('laporan_retur_penjualan_pusat/ajax_verifikasi_hapus_transaksi'); ?>",
            type: "POST",
            cache: false,
            data: 'id_retur_penjualan_m=' + $id_retur_penjualan_m,
            dataType: "JSON",
            success: function(data){
                if(data.status == 1){
                    $('.modal-dialog').removeClass('modal-lg');
                    $('.modal-dialog').addClass('modal-sm');
                    $('#ModalHeader').html('Informasi Hapus Retur Penjualan');
                    $('#ModalContent').html(data.pesan);
                    $('#ModalFooter').html(data.footer);
                    $('#ModalGue').modal('show');
                }else if(data.status == 0){
                    swal({
                        title: data.info_pesan, 
                        text: data.pesan, 
                        type: data.tipe_pesan, 
                        confirmButtonClass: 'btn-danger btn-md waves-effect waves-light',
                        confirmButtonText: "Ok"
                    }, function(isConfirm){
                        tanggal_laporan = $('#tanggal_filter').html();
                        tampilkan_data(tanggal_laporan);
                    });    
                }
            }, error: function (jqXHR, textStatus, errorThrown){
                swal({
                    title: "Oops !", 
                    text: "Data retur penjualan gagal ditampilkan.", 
                    type: "error", 
                    confirmButtonClass: 'btn-danger btn-md waves-effect waves-light',
                    confirmButtonText: "Ok"
                }, function (isConfirm){
                    tanggal_laporan = $('#tanggal_filter').html();
                    tampilkan_data(tanggal_laporan);
                });
            }
        }
    );}

    function hapus_transaksi($id_retur_penjualan_m)
    {
        var FormData = "tanggal=" + encodeURI($('#tanggal').val());
            FormData += "&id_retur_penjualan_m=" + $id_retur_penjualan_m;
            FormData += "&keterangan_batal=" + encodeURI($('#keterangan_batal').val());

        // ajax delete data to database
        $.ajax({
            url : "<?php echo site_url('laporan_retur_penjualan_pusat/ajax_hapus_transaksi'); ?>",
            type: "POST",
            cache: false,
            data: FormData,
            dataType: "JSON",
            success: function(data){
                swal({
                    title: data.info_pesan, 
                    text: data.pesan, 
                    type: data.tipe_pesan,
                    confirmButtonClass: data.gaya_tombol,
                    confirmButtonText: "Ok"
                }, function (isConfirm){
                    if(data.status == 1){
                        var tanggal_laporan = $('#tanggal_filter').html();
                        reload_table(tanggal_laporan);
                    }else if(data.status == 0){
                        tanggal_laporan = $('#tanggal_filter').html();
                        tampilkan_data(tanggal_laporan);
                    }
                });                
            }, error: function (jqXHR, textStatus, errorThrown){
                swal({
                    title: "Oops !", 
                    text: "Data retur penjualan gagal dihapus.", 
                    type: "error", 
                    confirmButtonClass: 'btn-danger btn-md waves-effect waves-light',
                    confirmButtonText: "Ok"
                }, function (isConfirm){
                    tanggal_laporan = $('#tanggal_filter').html();
                        tampilkan_data(tanggal_laporan);
                });
            }
        });
    }

    function cetak_faktur(id_retur_penjualan_m)
    {
        if((id_retur_penjualan_m) !== ''){
            var FormData = "&id_retur_penjualan_m="+id_retur_penjualan_m;
            window.open("faktur/faktur_retur_penjualan/?" + FormData,'_blank');
        }else{
            swal({
                title: "Oops !", 
                text: "No. retur penjualan tidak terdaftar.", 
                type: "error", 
                confirmButtonClass: 'btn-danger btn-md waves-effect waves-light',
                confirmButtonText: "Ok"
            },function (isConfirm){
                window.location.href = 'laporan_retur_penjualan_pusat';
            });
        }
    }
</script>
<!-- Akhir Script CRUD -->

<!-- Awal datatable -->
<link href="assets/plugin/zircos/plugins/datatables/buttons.bootstrap.min.css" rel="stylesheet" type="text/css"/>
<link href="assets/plugin/zircos/plugins/datatables/dataTables.colVis.css" rel="stylesheet" type="text/css"/>
<script src="assets/plugin/zircos/plugins/datatables/dataTables.buttons.min.js"></script>
<script src="assets/plugin/zircos/plugins/datatables/buttons.bootstrap.min.js"></script>
<script src="assets/plugin/zircos/plugins/datatables/jszip.min.js"></script>
<script src="assets/plugin/zircos/plugins/datatables/pdfmake.min.js"></script>
<script src="assets/plugin/zircos/plugins/datatables/vfs_fonts.js"></script>
<script src="assets/plugin/zircos/plugins/datatables/buttons.html5.min.js"></script>
<script src="assets/plugin/zircos/plugins/datatables/buttons.print.min.js"></script>
<!-- Akhir datatable -->
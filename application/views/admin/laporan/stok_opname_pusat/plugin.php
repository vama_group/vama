<!-- Awal CSS -->
<link href="assets/plugin/zircos/plugins/datatables/jquery.dataTables.min.css" rel="stylesheet" type="text/css"/>
<link href="assets/plugin/zircos/plugins/datatables/responsive.bootstrap.min.css" rel="stylesheet" type="text/css"/>
<link href="assets/plugin/zircos/plugins/datatables/dataTables.bootstrap.min.css" rel="stylesheet" type="text/css"/>
<link href="assets/plugin/zircos/material-design/assets/css/tambahan.css" rel="stylesheet" type="text/css"/>
<!-- Akhir CSS -->

<!-- Awal JS -->
<script src="assets/plugin/zircos/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="assets/plugin/zircos/plugins/datatables/dataTables.bootstrap.js"></script>
<script src="assets/plugin/zircos/material-design/assets/js/detect.js"></script>
<script src="assets/plugin/zircos/material-design/assets/js/jquery.slimscroll.js"></script>
<script src="assets/plugin/zircos/plugins/datatables/dataTables.responsive.min.js"></script>
<script src="assets/plugin/zircos/plugins/datatables/responsive.bootstrap.min.js"></script>
<script src="assets/plugin/zircos/material-design/assets/pages/jquery.datatables.init.js"></script>
<!-- Akhir JS -->

<!-- Awal Sweet-Alert  -->
<link href="assets/plugin/zircos/plugins/bootstrap-sweetalert/sweet-alert.css" rel="stylesheet" type="text/css">
<script src="assets/plugin/zircos/plugins/bootstrap-sweetalert/sweet-alert.min.js"></script>
<!-- Akhir Sweet-Alert  -->

<script>
    $('#navbar').removeClass('navbar-default');
    $('#navbar').addClass('navbar-biru-muda');
    $('#JudulHalaman').html('Laporan Stok Opname Pusat - VAMA');

    var table_so         = $('#TableTransaksi').DataTable();
    var table_riwayat_so = $('#table_riwayat_stok_opname_pusat').DataTable();
    var table_batal_so   = $('#table_batal_stok_opname_pusat').DataTable();
    var id_tab           = 1;

    $(document).ready(function(){
        $("#pencarian_kode").focus();
        $("#wrapper").removeClass('forced');
        $("#wrapper").addClass('forced enlarged');
        TampilkanDataStokOpname();
    });

    $(document).on('click', 'button#tambah_transaksi', function(){
        window.open("stok_opname_pusat");
    });

    $(document).on('change', '#pilih_periode_stok_pusat', function(){
        if(id_tab == 1){
            TampilkanDataStokOpname();
        }else if(id_tab == 2){
            TampilkanDataRiwayatStokOpname();
        }else if(id_tab == 3){
            TampilkanDataStokOpnameBatal();
        }
    });

    function TampilkanDataStokOpname(){
        var id_periode_stok_pusat  = $('#id_periode_stok_pusat').val();

        table_so.clear();table_so.destroy();
        table_so=$('#TableTransaksi').DataTable(
        {
            ordering: true,
            bFilter:true,
            processing:true,
            serverSide:true,
            order:[],
            lengthMenu: [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
            pagingType: "full",

            oLanguage:{
                sLoadingRecords:'Silakan Tunggu',
                sProcessing:"<img src='assets/loader/cubeloader2.2.gif' width='50' height='50'> Harap tunggu",
                sZeroRecords:"Belum ada data untuk ditampilkan",
                sSearch : "Pencarian data"
            },
            ajax:{
                url: "<?php echo site_url('laporan_stok_opname_pusat/ajax_list')?>",
                type: "POST",
                data: {'id_periode_stok_pusat' : $('#pilih_periode_stok_pusat').val()}
            },
            columnDefs: [
                            { 
                                "targets": [ -1 ],
                                "orderable": false,
                            },
                        ],
        });
    }

    function TampilkanDataRiwayatStokOpname(){
        var id_periode_stok_pusat  = $('#id_periode_stok_pusat').val();

        table_riwayat_so.clear();table_riwayat_so.destroy();
        table_riwayat_so=$('#table_riwayat_stok_opname_pusat').DataTable(
        {
            ordering: true,
            bFilter:true,
            processing:true,
            serverSide:true,
            order:[],
            lengthMenu: [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
            pagingType: "full",

            oLanguage:{
                sLoadingRecords:'Silakan Tunggu',
                sProcessing:"<img src='assets/loader/cubeloader2.2.gif' width='50' height='50'> Harap tunggu",
                sZeroRecords:"Belum ada data untuk ditampilkan",
                sSearch : "Pencarian data"
            },
            ajax:{
                url: "<?php echo site_url('laporan_stok_opname_pusat/ajax_list_riwayat')?>",
                type: "POST",
                data: {'id_periode_stok_pusat' : $('#pilih_periode_stok_pusat').val()}
            },
            columnDefs: [
                            { 
                                "targets": [ -1 ],
                                "orderable": false,
                            },
                        ],
        });
    }
    
    function TampilkanDataStokOpnameBatal(){
        var id_periode_stok_pusat  = $('#id_periode_stok_pusat').val();

        table_batal_so.clear();table_batal_so.destroy();
        table_batal_so=$('#table_batal_stok_opname_pusat').DataTable(
        {
            ordering: true,
            bFilter:true,
            processing:true,
            serverSide:true,
            order:[],
            lengthMenu: [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
            pagingType: "full",

            oLanguage:{
                sLoadingRecords:'Silakan Tunggu',
                sProcessing:"<img src='assets/loader/cubeloader2.2.gif' width='50' height='50'> Harap tunggu",
                sZeroRecords:"Belum ada data untuk ditampilkan",
                sSearch : "Pencarian data"
            },
            ajax:{
                url: "<?php echo site_url('laporan_stok_opname_pusat/ajax_list_batal')?>",
                type: "POST",
                data: {'id_periode_stok_pusat' : $('#pilih_periode_stok_pusat').val()}
            },
            columnDefs: [
                            { 
                                "targets": [ -1 ],
                                "orderable": false,
                            },
                        ],
        });
    }

    var delay = (function (){
        var timer = 0;
        return function (callback, ms) {
            clearTimeout(timer);
            timer = setTimeout(callback, ms);
        };
    })();

    $(document).on('click', '#tab_so', function(){
        id_tab = 1;
        TampilkanDataStokOpname();
    });

    $(document).on('click', '#tab_riwayat_so', function(){
        id_tab = 2;
        TampilkanDataRiwayatStokOpname();
    });

    $(document).on('click', '#tab_batal_so', function(){
        id_tab = 3;
        TampilkanDataStokOpnameBatal();
    });

    function reload_table_so()
    {
        table_so.ajax.reload();
    }

    function reload_table_riwayat_so()
    {
        table_riwayat_so.ajax.reload();
    }

    function reload_table_batal_so()
    {
        table_batal_so.ajax.reload();
    }

    function check_int(evt) {
        var charCode = ( evt.which ) ? evt.which : event.keyCode;
        return ( charCode >= 48 && charCode <= 57 || charCode == 8 );
    }

    function cetak_excel(){
        window.open("<?php echo site_url('laporan_stok_opname_pusat/ajax_cetak_ke_excel?'); ?>",'_blank');
    }
</script>
<!-- Akhir Script CRUD -->

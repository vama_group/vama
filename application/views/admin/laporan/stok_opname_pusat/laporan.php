<!-- Start content -->
<div class="content">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="page-title-box">
                    <h4 class="page-title text-info"><i class='mdi mdi-cube-outline'></i><b>  Laporan Stok Opname Pusat</b></h4>
                    <ol class="breadcrumb p-0 m-0">
                        <li>
                            <input type='hidden' id="id_periode_stok_pusat" name="id_periode_stok_pusat" 
                            value="<?php echo $data_periode_sekarang->id_periode_stok_pusat ?>">

                            <b class="small text-muted">Periode Stok : </b>
                            <select id="pilih_periode_stok_pusat" name="pilih_periode_stok_pusat" class="btn btn-rounded btn-xs" onchange="">
                                <?php foreach ($data_periode AS $dp){ ?>
                                    <option value="<?php echo $dp->id_periode_stok_pusat ?>" class="text-dark">
                                        <?php echo $dp->kode_periode_stok_pusat, ' (',
                                        $dp->tanggal_periode_awal, '>', 
                                        $dp->tanggal_periode_akhir, ')' ?>
                                    </option>
                                <?php } ?>
                            </select>

                            <?php if($access_create == '1'){ ?>
                                <button id="tambah_transaksi" class="btn btn-rounded btn-xs btn-success">
                                    <i class="fa fa-plus"></i> TAMBAH
                                </button>
                            <?php } ?>
                            
                            <button id="ReferesData" class="btn btn-rounded btn-xs btn-primary" onclick="refresh_data()">
                                <i class="fa fa-refresh"></i> 
                                REFRESH
                            </button>    

                            <?php if($this->session->userdata('usergroup_name') == 'Super Admin'){ ?>
                                <button id="CetakExcel" class="btn btn-rounded btn-xs btn-success" onclick="cetak_excel()">
                                    <i class="fa fa-file-excel-o"></i> 
                                    Cetak Ke Excel
                                </button>
                            <?php } ?>                                                                            
                        </li>                        
                    </ol>

                    <div class="clearfix"></div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12">
                <div class="card-box table-responsive">
                    <!-- Awal induk tab -->
                    <ul class="nav nav-tabs tabs-bordered">
                        <li class="active">
                            <a id="tab_so" href="#tab_stok_opname_pusat" data-toggle="tab" aria-expanded="false">
                                <span class="visible-xs"><i class="fa fa-check-square"></i></span>
                                <span class="hidden-xs">Stok Opname Pusat</span>
                            </a>
                        </li>
                        <li class="">
                            <a id="tab_riwayat_so" href="#tab_riwayat_stok_opname_pusat" data-toggle="tab" aria-expanded="true">
                                <span class="visible-xs"><i class="fa fa-recycle"></i></span>
                                <span class="hidden-xs">Riwayat Stok Opname Pusat</span>
                            </a>
                        </li>
                        <li class="">
                            <a id="tab_batal_so" href="#tab_batal_stok_opname_pusat" data-toggle="tab" aria-expanded="true">
                                <span class="visible-xs"><i class="fa fa-recycle"></i></span>
                                <span class="hidden-xs">Stok Opname Pusat Batal</span>
                            </a>
                        </li>
                    </ul>
                    <!-- Akhir induk tab -->
                    
                    <div class="tab-content">
                    	<!-- Awal data stok opname -->
                		<div class="tab-pane active" id="tab_stok_opname_pusat">
							<!-- Daftar transaksi -->
							<table id='TableTransaksi' class="table table-condensed table-striped table-hover dt-responsive nowrap" cellspacing="0" width="100%">
								<thead class="input-sm">
									<tr class="text-dark">
										<th>#</th>
										<!-- <th>Tombol</th> -->
										<!-- <th>Foto</th> -->
										<th>SKU</th>
										<th>Nama Barang</th>
										<th>Harga</th>
										<th>QTY SB</th>
										<th>QTY SO</th>
										<th>STOK</th>
										<th>Catatan</th>
										<th>Pegawai Save</th>
										<th>Tanggal Save</th>
										<th>Pegawai Edit</th>
										<th>Tanggal Edit</th>
									</tr>
								</thead>

								<tbody class="input-sm text-dark"></tbody>
							</table>
						</div>
						<!-- Akhir data stok opname -->

						<!-- Awal data riwayat stok opname -->
						<div class="tab-pane" id="tab_riwayat_stok_opname_pusat">
                            <table id="table_riwayat_stok_opname_pusat" class="table table-condensed table-striped table-hover dt-responsive nowrap" cellspacing="0" width="100%">
                                <thead class="input-sm">
                                    <tr>
                                    	<th>#</th>
                                        <th>SKU</th>
                                        <th style='width: 1350px;'>NAMA BARANG</th>
                                        <th>QTY SB</th>
                                        <th>QTY SO</th>
                                        <th>STOK</th>
                                        <th>CATATAN</th>
                                        <th>PEGAWAI</th>
                                        <th>TANGGAL</th>
                                        <!-- <th style="width:125px;">Action</th> -->
                                    </tr>
                                </thead>

                                <tbody class="input-sm">
                                </tbody>
                            </table>
                        </div>
                        <!-- Akhir data riwayat stok opname -->

                        <!-- Awal data batal stok opname -->
						<div class="tab-pane" id="tab_batal_stok_opname_pusat">
                            <table id="table_batal_stok_opname_pusat" class="table table-condensed table-striped table-hover dt-responsive nowrap" cellspacing="0" width="100%">
                                <thead class="input-sm">
                                    <tr>
                                    	<th>#</th>
                                        <th>SKU</th>
                                        <th>NAMA BARANG</th>
                                        <th>QTY SO</th>
                                        <th>STOK</th>
                                        <th>KETERANGAN</th>
                                        <th>Pegawai Batal</th>
                                        <th>Tanggal Batal</th>
                                        <th>Pegawai Save</th>
                                        <th>Tanggal Save</th>
                                        <th>Pegawai Edit</th>
                                        <th>Tanggal Edit</th>
                                    </tr>
                                </thead>

                                <tbody class="input-sm">
                                </tbody>
                            </table>
                        </div>
                        <!-- Akhir data batal stok opname -->
					</div>
                </div>
            </div>
        </div>
    </div> <!-- container -->
</div> 
<!-- content -->

<div class="modal" id="ModalGue" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class='fa fa-times-circle'></i></button>
                <h4 class="modal-title" id="ModalHeader"></h4>
            </div>
            <div class="modal-body" id="ModalContent"></div>
            <div class="modal-footer" id="ModalFooter"></div>
        </div>
    </div>
</div>

<div class="modal" id="ModalDetailTransaksi" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class='fa fa-times-circle'></i></button>
                <h5 class="modal-title" id="ModalHeaderDetailTransaksi"></h5>
            </div>
            <div class="modal-body" id="ModalContentDetailTransaksi">
                <div class="row">
                    <!-- Awal detail barang -->
                    <div class="col-md-6">
                        <div class="row">
                            <div class="col-md-2">
                                <small>Kode Periode</small><br>
                                <span class="text-dark" id="kode_periode_stok_pusat">-</span>
                            </div>

                            <div class="col-md-3">
                                <small>Tanggal Awal Periode</small><br>
                                <span class="text-dark" id="tanggal_periode_awal">-</span>
                            </div>

                            <div class="col-md-3">
                                <small>Tanggal Akhir Periode</small><br>
                                <span class="text-dark" id="tanggal_periode_akhir">-</span>
                            </div>

                            <div class="col-md-2 list_data">
                                <div id="foto_barang_kecil" name="foto_barang_kecil" class="inbox-item">
                                </div>
                            </div>                            
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-md-2">
                                <span id="id_barang_pusat" style="display: none;"></span>
                                <small>SKU</small><br>
                                <span class="text-dark" id="sku">-</span>
                            </div>

                            <div class="col-md-3">
                                <small>Kode Barang</small><br>
                                <span class="text-dark" id="kode_barang">-</span>
                            </div>

                            <div class="col-md-2">
                                <small>Harga Eceran</small><br>
                                <span class="text-dark" id="harga_eceran">Rp. 0</span>
                            </div>

                            <div class="col-md-2">
                                <small>Harga Grosir 1</small><br>
                                <span class="text-dark" id="harga_grosir1">Rp. 0</span>
                            </div>

                            <div class="col-md-2">
                                <small>Harga Grosir 2</small><br>
                                <span class="text-dark" id="harga_grosir2">Rp. 0</span>
                            </div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-md-3">
                                <span class="text-muted">STOK JUAL : </span>
                                <h2 class="text-primary" id="stok_jual">9.999</h2>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-2">
                                <small>SO</small><br>
                                <span class="btn btn-xs btn-default btn-block text-primary" id="so">9.999</span>
                            </div>

                            <div class="col-md-2">
                                <small>+ PB</small><br>
                                <span class="btn btn-xs btn-default btn-block text-primary" id="pb">9.999</span>
                            </div>

                            <div class="col-md-2">
                                <small>+ DP</small><br>
                                <span class="btn btn-xs btn-default btn-block text-primary" id="dp">9.999</span>
                            </div>

                            <div class="col-md-2">
                                <small>+ RPT</small><br>
                                <span class="btn btn-xs btn-default btn-block text-primary" id="rpt">9.999</span>
                            </div>

                            <div class="col-md-2">
                                <small>+ RBDTT</small><br>
                                <span class="btn btn-xs btn-default btn-block text-primary" id="rbdtt">9.999</span>
                            </div>

                            <div class="col-md-2">
                                <small>- UP</small><br>
                                <span class="btn btn-xs btn-default btn-block text-danger" id="up">9.999</span>
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-md-2">
                                <small>- PJU</small><br>
                                <span class="btn btn-xs btn-default btn-block text-danger" id="pju">9.999</span>
                            </div>

                            <div class="col-md-2">
                                <small>- PJT</small><br>
                                <span class="btn btn-xs btn-default btn-block text-danger" id="pjt">9.999</span>
                            </div>

                            <div class="col-md-2">
                                <small>- RPBT</small><br>
                                <span class="btn btn-xs btn-default btn-block text-danger" id="rpbt">9.999</span>
                            </div>

                            <div class="col-md-2">
                                <small>- PP</small><br>
                                <span class="btn btn-xs btn-default btn-block text-danger" id="pp">9.999</span>
                            </div>

                            <div class="col-md-2">
                                <small>- HD</small><br>
                                <span class="btn btn-xs btn-default btn-block text-danger" id="hd">9.999</span>
                            </div>

                            <div class="col-md-2">
                                <small>- PS</small><br>
                                <span class="btn btn-xs btn-default btn-block text-danger" id="ps1">9.999</span>
                            </div>
                        </div>
                        <hr>

                        <div class="row">
                            <div class="col-md-3">
                                <span class="text-muted">STOK RUSAK : </span>
                                <h2 class="text-danger" id="stok_rusak">9.999</h2>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-2">
                                <small>+ RPR</small><br>
                                <span class="btn btn-xs btn-default btn-block text-primary" id="rpr">9.999</span>
                            </div>

                            <div class="col-md-2">
                                <small>+ RBDTR</small><br>
                                <span class="btn btn-xs btn-default btn-block text-primary" id="rbdtr">9.999</span>
                            </div>

                            <div class="col-md-2">
                                <small>+ PS</small><br>
                                <span class="btn btn-xs btn-default btn-block text-primary" id="ps2">9.999</span>
                            </div>

                            <div class="col-md-2">
                                <small>- RPBR</small><br>
                                <span class="btn btn-xs btn-default btn-block text-danger" id="rpbr">9.999</span>
                            </div>
                        </div>                    
                    </div>
                    <!-- Akhir detail barang -->
                    
                    <!-- Awal detail transaksi -->
                    <div class="col-md-6">
                        <h3 id="judul_transaksi" class="text-dark">Transaksi</h3>
                        <table id='TableDetailTransaksi' class="table table-condensed table-striped table-hover dt-responsive nowrap" cellspacing="0" width="100%">
                            <thead class="input-sm">
                                <tr id="header_detail_transaksi" class="text-dark">
                                    <th></th>
                                </tr>
                            </thead>

                            <tbody class="input-sm text-dark"></tbody>
                        </table>
                    </div>
                    <!-- Akhir detail transaksi -->
                </div>
            </div>
            <div class="modal-footer" id="ModalFooterDetailTransaksi"></div>
        </div>
    </div>
</div>

<script>
    $('#ModalGue').on('hide.bs.modal', function () {
       setTimeout(function(){ 
            $('#ModalHeader, #ModalContent, #ModalFooter').html('');
       }, 500);
    });

    $('#ModalDetailTransaksi').on('hide.bs.modal', function () {
       setTimeout(function(){ 
            $('#ModalHeaderDetailTransaksi, #ModalFooterDetailTransaksi').html('');
       }, 500);
    }); 
</script>

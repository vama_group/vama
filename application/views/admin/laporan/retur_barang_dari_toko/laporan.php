<?php
	$level    = $this->session->userdata('usergroup_name');
	$readonly = 'readonly';
	$disabled = 'disabled';
?>

<div class="content">
	<div class="container">
	<br>		
		<div class="row">
        	<div class="col-sm-12">
	        	<div class="row">
		            <div class="col-sm-12">
			        	<!-- Awal daftar transaksi dan catatan transaksi --> 
		            	<div class="card-box table-responsive">
		            		<!-- Keterangan transaksi dan tombol tambah dan refersh -->
							<div class="row">
								<!-- Tombol -->
								<div class='col-sm-9'>
									<h4 class="text-warning">
										<i class='fa fa-truck'></i>
										LAPORAN RETUR DARI TOKO
									</h4>
									<select id="kode_toko" name="kode_toko" class="btn btn-rounded btn-xs btn-default">
	                                    <option value="" class="text-dark ">PILIH TOKO</option>
	                                    <?php foreach ($data_toko AS $dt){ ?>
	                                        <option value="<?php echo $dt->kode_customer_pusat ?>" class="text-dark">
	                                            <?php echo $dt->nama_customer_pusat ?>
	                                        </option>
	                                    <?php } ?>
	                                </select>
                            		<button class="btn btn-xs btn-primary btn-rounded" id="refresh_data">
                            			<i class="glyphicon glyphicon-refresh"></i> 
                            			REFRESH
                            		</button>
								</div>

								<!-- Pemilihan tanggal awal dan akhir -->
								<div class="col-sm-3">
									<div class="form-group">
                                        <label>Pilih tanggal awal dan akhir</label>
                                        <div id="reportrange" class="pull-right form-control">
                                            <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>
                                            <span id="tanggal_filter" name="tanggal_filter"></span>
                                        </div>
                                    </div>
								</div>
							</div>
							<hr>
							<!-- <br> -->

							<!-- Awal induk tab -->
		                    <ul class="nav nav-tabs tabs-bordered" id="tab_list_pembelian">
		                        <li class="active">
		                            <a href="#tab_induk_retur" id="tab_induk" data-toggle="tab" aria-expanded="true">
		                                <span class="visible-xs"><i class="fa fa-check-square"></i></span>
		                                <span class="hidden-xs" id="judul_tab_induk">Data Retur Barang Induk</span>
		                            </a>
		                        </li>
		                        <li class="">
		                            <a href="#tab_detail_retur" id="tab_detail" data-toggle="tab" aria-expanded="true">
		                                <span class="visible-xs"><i class="typcn typcn-home"></i></span>
		                                <span class="hidden-xs" id="judul_tab_batal">Data Retur Barang Detail</span>
		                            </a>
		                        </li>
		                        <li class="">
		                            <a href="#tab_batal_retur" id="tab_batal" data-toggle="tab" aria-expanded="true">
		                                <span class="visible-xs"><i class="typcn typcn-home"></i></span>
		                                <span class="hidden-xs" id="judul_tab_batal">Data Retur Barang Batal</span>
		                            </a>
		                        </li>
		                    </ul>
		                    <!-- Akhir induk tab -->

		                    <!-- Awal isi tab -->
		                    <div class="tab-content">
								<!-- Awal Daftar Laporan induk -->
		                        <div class="tab-pane active" id="tab_induk_retur">
									<table id='TableLaporanInduk' class="table table-condensed table-striped table-hover dt-responsive nowrap" cellspacing="0" width="100%">
										<thead class="input-sm">
											<tr>
												<th>#</th>
												<th>Tombol</th>
												<th>Toko</th>
												<th>No. Retur</th>
												<th>Tanggal</th>
												<th>Status</th>
												<th>Jml Retur</th>
												<th>Jml Masuk</th>

												<th>Catatan</th>
												<th>Pegawai Save</th>
												<th>Tanggal Save</th>
												<th>Pegawai Edit</th>
												<th>Tanggal Edit</th>
												<th>Pegawai Masuk</th>
												<th>Tanggal Masuk</th>
											</tr>
										</thead>

										<tbody class="input-sm"></tbody>
									</table>
								</div>
								<!-- Akhir Daftar Laporan induk -->

								<!-- Awal Daftar Laporan detail -->
		                        <div class="tab-pane" id="tab_detail_retur">
									<table id='TableLaporanDetail' class="table table-condensed table-striped table-hover dt-responsive nowrap" cellspacing="0" width="100%">
										<thead class="input-sm">
											<tr>
												<th>#</th>
												<th>Tombol</th>
												<th>Toko</th>
												<th>No. Retur</th>
												<th>Tanggal</th>
												<th>Status</th>
												<th>SKU</th>
												<th>Nama Barang</th>
												<th>Harga</th>
												<th>Jml Retur</th>
												<th>Jml Masuk</th>
												<th>Dari Stok</th>
												<th>Kondisi</th>
												
												<th>Keterangan</th>
												<th>Pegawai Save</th>
												<th>Tanggal Save</th>
												<th>Pegawai Edit</th>
												<th>Tanggal Edit</th>
												<th>Pegawai Masuk</th>
												<th>Tanggal Masuk</th>
											</tr>
										</thead>

										<tbody class="input-sm"></tbody>
									</table>
								</div>
								<!-- Akhir Daftar Laporan detail -->

								<!-- Awal Daftar Laporan batal -->
		                        <div class="tab-pane" id="tab_batal_retur">
									<table id='TableLaporanBatal' class="table table-condensed table-striped table-hover dt-responsive nowrap" cellspacing="0" width="100%">
										<thead class="input-sm">
											<tr>
												<th>#</th>
												<th>Toko</th>
												<th>No. Retur</th>
												<th>SKU</th>
												<th>Nama Barang</th>
												<th>Harga</th>
												<th>Dari Stok</th>
												<th>Kondisi</th>
												<th>Jml Retur</th>
												<th>Jml Masuk</th>

												<th>Keterangan Retur</th>
												<th>Keterangan Batal</th>
												<th>Pegawai Batal</th>
												<th>Tanggal Batal</th>
												<th>Pegawai Save</th>
												<th>Tanggal Save</th>
												<th>Pegawai Edit</th>
												<th>Tanggal Edit</th>
												<th>Pegawai Masuk</th>
												<th>Tanggal Masuk</th>
											</tr>
										</thead>

										<tbody class="input-sm"></tbody>
									</table>
								</div>
								<!-- Akhir Daftar Laporan batal -->
							</div>
						</div>
				        <!-- Akhir daftar transaksi -->
		            </div>
				</div>
            </div>
        </div>
	</div>
</div>

<div class="modal" id="ModalGue" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class='fa fa-times-circle'></i></button>
				<h4 class="modal-title" id="ModalHeader"></h4>
			</div>
			<div class="modal-body" id="ModalContent"></div>
			<div class="modal-footer" id="ModalFooter"></div>
		</div>
	</div>
</div>

<script>
$('#ModalGue').on('hide.bs.modal', function () {
   setTimeout(function(){ 
        $('#ModalHeader, #ModalContent, #ModalFooter').html('');
   }, 500);
});
</script>
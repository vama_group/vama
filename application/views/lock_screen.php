<!DOCTYPE html>
<html>
<head>
    <meta name="theme-color" content="#337ab7" /> <!-- URL Theme Color untuk Chrome, Firefox OS, Opera dan Vivaldi -->
    <meta name="msapplication-navbutton-color" content="#337ab7" /> <!-- URL Theme Color untuk Windows Phone -->
    
    <meta name="apple-mobile-web-app-capable" content="yes" /><!-- URL Theme Color untuk iOS Safari -->
    <meta name="apple-mobile-web-app-status-bar-style" content="#337ab7" /><!-- URL Theme Color untuk iOS Safari -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="A fully featured admin theme which can be used to build CRM, CMS, etc.">
    <meta name="author" content="Coderthemes">

    <!-- App favicon -->
    <link rel="shortcut icon" href="assets/images/favicon.ico">

    <!-- App title -->
    <title><?php echo $title ?></title>

    <!-- App css -->
    <link href="<?php echo base_url() ?>assets/plugin/zircos/material-design/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url() ?>assets/plugin/zircos/material-design/assets/css/core.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url() ?>assets/plugin/zircos/material-design/assets/css/components.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url() ?>assets/plugin/zircos/material-design/assets/css/icons.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url() ?>assets/plugin/zircos/material-design/assets/css/pages.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url() ?>assets/plugin/zircos/material-design/assets/css/menu.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url() ?>assets/plugin/zircos/material-design/assets/css/responsive.css" rel="stylesheet" type="text/css" />

    <!-- HTML5 Shiv and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
    <script src="<?php echo base_url() ?>assets/plugin/zircos/material-design/assets/js/modernizr.min.js"></script>
</head>


<body>

<!-- Loader -->
<div id="preloader">
<div id="status">
<div class="spinner">
<div class="spinner-wrapper">
<div class="rotator">
<div class="inner-spin"></div>
<div class="inner-spin"></div>
</div>
</div>
</div>
</div>
</div>

<!-- HOME -->
<section>
<div class="container-alt">
<div class="row">
<div class="col-sm-12">

<div class="wrapper-page">
<div class="m-t-40 account-pages">
<div class="text-center account-logo-box">
<h2 class="text-uppercase">
    <a href="index.html" class="text-success">
        <span><img src="<?php echo base_url() ?>assets/upload/logo/vama_logo_putih.png"  alt="" height="36"></span>
    </a>
</h2>
<!--<h4 class="text-uppercase font-bold m-b-0">Sign In</h4>-->
</div>
    <div class="account-content">
        <?php
        // Notifikasi dan  Error login
        if ($this->session->flashdata('sukses')){
            echo '<div class="alert alert-success">';
            echo $this->session->flashdata('sukses');
            echo '</div>';
            }

        // Error input
        echo validation_errors('<div class="alert alert-success">','</div>');

        ?>
        <form class="form-horizontal" action="<?php echo base_url('login/lock?redirect='.$_GET['redirect'])?>" method="post">
            <div class="form-group">
                <div class="col-xs-12">
                    <label>Masukan Password</label>
                    <input class="form-control" type="password" required="required" name="password" placeholder="Masukan password bro">
                </div>
            </div>

            <div class="form-group account-btn text-center m-t-10">
                <div class="col-xs-12">
                    <button class="btn w-md btn-bordered btn-primary waves-effect waves-light" type="submit">Masuk</button>
                </div>
            </div>
        </form>
    </div>
</div>
<!-- end card-box-->

</div>
<!-- end wrapper -->

</div>
</div>
</div>
</section>
<!-- END HOME -->

<script>
var resizefunc = [];
</script>

<!-- jQuery  -->
<script src="<?php echo base_url() ?>assets/plugin/zircos/material-design/assets/js/jquery.min.js"></script>
<script src="<?php echo base_url() ?>assets/plugin/zircos/material-design/assets/js/bootstrap.min.js"></script>
<script src="<?php echo base_url() ?>assets/plugin/zircos/material-design/assets/js/detect.js"></script>
<script src="<?php echo base_url() ?>assets/plugin/zircos/material-design/assets/js/fastclick.js"></script>
<script src="<?php echo base_url() ?>assets/plugin/zircos/material-design/assets/js/jquery.blockUI.js"></script>
<script src="<?php echo base_url() ?>assets/plugin/zircos/material-design/assets/js/waves.js"></script>
<script src="<?php echo base_url() ?>assets/plugin/zircos/material-design/assets/js/jquery.slimscroll.js"></script>
<script src="<?php echo base_url() ?>assets/plugin/zircos/material-design/assets/js/jquery.scrollTo.min.js"></script>

<!-- App js -->
<script src="<?php echo base_url() ?>assets/plugin/zircos/material-design/assets/js/jquery.core.js"></script>
<script src="<?php echo base_url() ?>assets/plugin/zircos/material-design/assets/js/jquery.app.js"></script>

</body>
</html>